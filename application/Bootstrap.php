<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    protected function _initAutoload() {
        date_default_timezone_set('Asia/Jakarta');
        $autoLoader = Zend_Loader_Autoloader::getInstance();
        $autoLoader->registerNamespace('CMS_');
        $autoLoader->registerNamespace('REST_');
        $resourceLoader = new Zend_Loader_Autoloader_Resource(array('basePath' => APPLICATION_PATH, 'namespace' => '', 'resourceTypes' => array('form' => array('path' => 'forms/', 'namespace' => 'Form_'), 'model' => array('path' => 'models/', 'namespace' => 'Model_'))));
        return $autoLoader;
    }

    protected function _initView() {
        $view = new Zend_View();
        //        $view->doctype('XHTML1_STRICT');
        //        if(isset($_SERVER['REQUEST_URI'])) {
        //            $uri = explode('/', $_SERVER['REQUEST_URI']);
        //        }
        $view->headTitle('PraBAC-2015');
        //        //$view->skin = 'default';
        $config = new Zend_Config_Ini(APPLICATION_PATH . 
                                      '/configs/application.ini', 'production');
        $theme =($config->theme->name);
        Zend_Registry::set('theme', $theme);
        $view->setScriptPath(APPLICATION_PATH . 
                             '/views/theme/' . 
                             $theme . 
                             '/');
        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
        $viewRenderer->setView($view);
        return $view;
    }

    protected function _initLog() {
        $options = $this->getOption('resources');
        $partitionConfig = $this->getOption('log');
        $logOptions = $options['log'];
        $logger = Zend_Log::factory($logOptions);
        Zend_Registry::set('logger', $logger);
    }

    protected function _initCalasan() {
        $vA = explode("/", $_SERVER['REQUEST_URI']);
        if($vA[1] == 'calasan') {
            $this->bootstrap('db');
            $registry = Zend_Registry::getInstance();
            $db = $this->getResource('db');
            $cc = new Model_Configs();
            $registry->set('xconfig', $cc->get_all());
            
        }
    }

    protected function _initTranslate() {
        
        /*
         APPLICATION_PATH . '/../public/assets/core/fonts/elephant.ttf', $translate = new Zend_Translate(array('adapter' => 'gettext', 'content' => APPLICATION_PATH . '/../locale/en-US.mo', 'locale' => 'en'));*/
        
        /*
         Zend_Registry::set('translate', $translate);
         Zend_Form::setDefaultTranslator($translate);
         $this->bootstrap('view');
         $view = $this->getResource('view');
         $view->translate = $translate;
         */
    }
}
