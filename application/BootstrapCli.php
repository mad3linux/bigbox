<?php

class BootstrapCli extends Zend_Application_Bootstrap_Bootstrap
{


	protected function _initRouter ()
	{
		
	
		$this->bootstrap ('frontcontroller');

		$this->getResource ('frontcontroller')
			->setResponse(new Zend_Controller_Response_Cli())
			->setRouter (new Application_Router_Cli ())
			->setRequest (new Zend_Controller_Request_Simple ());
	}


	protected function _initError ()
	{
		$frontcontroller = $this->getResource ('frontcontroller');

		$error = new Zend_Controller_Plugin_ErrorHandler ();
		$error->setErrorHandlerController ('error');

		$frontcontroller->registerPlugin ($error, 100);

		return $error;
	}
	  
    protected function _initAutoload ()
    {
		
		$autoLoader = Zend_Loader_Autoloader::getInstance();
   
        $resourceLoader = new Zend_Loader_Autoloader_Resource(array('basePath' => APPLICATION_PATH , 'namespace' => '' , 'resourceTypes' => array('form' => array('path' => 'forms/' , 'namespace' => 'Form_') , 'model' => array('path' => 'models/' , 'namespace' => 'Model_'))));
        return $autoLoader;
    }
}

