<?php

/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
#ini_set("memory_limit","80M");
@ ini_set("memory_limit", "-1");
date_default_timezone_set("Asia/Bangkok");

class CronController extends Zend_Controller_Action {
    
    /*
     *  $params = $this->getRequest()->getParams();
     $i = 0;
     foreach($params['awal'] as $v) {
     $input[$v] = $params['akhir'][$i];
     $i ++;
     }
     // $input['indexurl']='http://news.detik.com/indeks';
     // $input['medianame']='detik';
     $new = new Model_Zprabawf();
     $out = $new->get_wf_process($params['id'], $input, true, $params['debug']);*/
    public function trainAction() {
        //die("xxx");
        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($params); die();
        $upload = APPLICATION_PATH . '/../public/predictive/' . $params['stype'] . "/";
        //echo $upload;
        $mode = 0755;
        if(!is_dir($upload)) {
            mkdir($upload, 0777);
        }
        $lang_detect = new Domas_Model_Predictive();
        $ret = $lang_detect->train($params['tid'], $params['stype']);
        $lang_detect->saveToFile($ret, APPLICATION_PATH . 
                                 '/../public/predictive/' . 
                                 $params['stype'] . 
                                 "/" . 
                                 $params['tid']);
        die("ok");
    }

    public function execAction() {
        
        /*
         file_get_contents("https://api.telegram.org/bot212490757:AAG7YHJXfyURWrGfr4GICRPTbiOCRo8nhDY/sendMessage?chat_id=@SMSLaporGubChannel&text=test"); die(); */
        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($params); die();
        $i = 0;

        foreach($params['awal'] as $v) {
            $input[$v] = $params['akhir'][$i];
            $i ++;
        }
        $new = new Model_Zprabawf();
        $out = $new->get_wf_process($params['id'], $input, true, $params['debug']);
        Zend_Debug::dump($out);
        die("end_exec");
    }

    public function execrunningcronAction() {
        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($params); die();
        $new = new Model_Crontab();
        $data = $new->get_cron4exec($params['id']);
        //Zend_Debug::dump($data); die("ok");
    }

    public function crawlAction() {
        $params = $this->getRequest()->getParams();
        $i = 0;

        foreach($params['awal'] as $v) {
            $input[$v] = $params['akhir'][$i];
            $i ++;
        }
        // $input['medianame'] ='kompas';
        // $input['indexurl'] ='http://indeks.kompas.com/?tanggal=7&bulan=01&tahun=2016&pos=indeks';
        // $input['startpage'] =11;
        $new = new Model_Zprabawf();
        $out = $new->get_wf_process($params['id'], $input, true, $params['debug']);
        Zend_Debug::dump($out);
        die();
        
        /*
         $cc= new Pmas_Model_Pmascrawl();
         $var = $cc->get_detik();
         Zend_Debug::dump($var); die();
         */
    }

    public function testAction() {
        $loop = React\EventLoop\Factory::create();
        $i = 0;

        $loop->addPeriodicTimer(1, function()use(&$i) {
            die("ss");
            echo ++ $i, PHP_EOL;
        }
       );
        $loop->run();
        die();
    }

    public function twitAction() {
        die("xx");
        $dd = new Pmas_Model_Pubtwitter();
        $dd->pub_get_twitter_bin();
        die("c");
    }
}
