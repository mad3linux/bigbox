<?php


/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class ErrorController extends Zend_Controller_Action {

    public function errorAction() {
        $errors = $this->_getParam('error_handler');
        switch($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE : case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER : case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION : // 404 error -- controller or action not found
            $this->getResponse()->setRawHeader('HTTP/1.1 404 Not Found');
            $content =<<<EOH
<h1>Error!</h1>
<p>The page you requested was not found.</p>
EOH
           ;
            die();
            break;
            default : // application error
            $content =<<<EOH
<h1>Error!</h1>
<p>An unexpected error occurred. Please try again later.</p>
EOH
           ;
            die();
            break;
        }
        // Clear previous content
        $this->getResponse()->clearBody();
        $this->view->content = $content;
    }
}
