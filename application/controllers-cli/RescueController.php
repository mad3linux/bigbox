<?php


/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
#ini_set("memory_limit","80M");
@ini_set("memory_limit", "-1");
date_default_timezone_set("Asia/Bangkok");
ini_set("display_errors", "On");

class RescueController extends Zend_Controller_Action {

    public function runAction() {
        //require (APPLICATION_PATH) . '/models/Resc.php'; 
        require_once(APPLICATION_PATH). '/../bin/resque';
    }

    public function queAction() {
        //require (APPLICATION_PATH) . '/models/Resc.php'; 
        require_once(APPLICATION_PATH). '/../lib/Resque.php';
        $args = array('time' => time(),
                      'array' => array('test' => 'test',
                     ),
                     );
        $jobId = Resque::enqueue('default', 'Model_Resc', $args, true);
        echo "Queued job " . $jobId . "\n\n";
        die();
    }

    public function quetelegramAction() {
        //require (APPLICATION_PATH) . '/models/Resc.php'; 
        require_once(APPLICATION_PATH). '/../lib/Resque.php';
        $args = array('time' => time(),
                      'array' => array('test' => 'test',
                     ),
                     );
        $jobId = Resque::enqueue('telegram', 'Model_Apitelegram', $args, true);
        echo "Queued job " . $jobId . "\n\n";
        die();
    }
}
