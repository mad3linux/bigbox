<?php


/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
#ini_set("memory_limit","80M");
@ ini_set("memory_limit", "-1");
date_default_timezone_set("Asia/Bangkok");
ini_set("display_errors", "On");

class SocketController extends Zend_Controller_Action {

    public function runAction() {
        //$this->_helper->layout->disableLayout ();
        //error_reporting(E_ALL);
        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($params); die();
        set_time_limit(0);
        $address = $params['host'];
        $port = $params['port'];
        //$verboseMode = $params['verbose'];
        $address = '127.0.0.1';
        $port = 5001;
        $server = new \PushWebSocket\Server($address, $port, false);
        $n = new Model_System();
        $data = $n->call_api($params['id']);
        //Zend_Debug::dump($data); die();
        $server->runningdata($data['data']);
        //die();		
    }
}
