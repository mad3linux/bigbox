<?php

@ ini_set("memory_limit", "-1");
date_default_timezone_set("Asia/Bangkok");
ini_set('display_errors', 1);
error_reporting(E_ALL);
//error_reporting(0);

class Socketserver2Controller extends Zend_Controller_Action {

    public function run4Action() {
        set_time_limit(0);
        $params = $this->getRequest()->getParams();
        try {
            $loop = React\EventLoop\Factory::create();
            $app = new Ratchet\App('103.16.199.56', 9992, '0.0.0.0', $loop);
            $class = new CMS_Chat();

            $loop->addPeriodicTimer(5, function(React\EventLoop\Timer\Timer $timer)use($class, $loop) {
                //Zend_Debug::dump($class->clients); die();

                foreach($class->clients as $client) {
                    $client->send(json_encode(array('data' => 1, 'time' => mktime())));
                }
            }
           );
            $app->route('/chat', $class, array('*'));
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
        //die("ss");
        $app->run();
        //die();
    }

    public function run3Action() {
        set_time_limit(0);
        $params = $this->getRequest()->getParams();
        try {
            $loop = React\EventLoop\Factory::create();
            //Zend_Debug::dump($loop); die("s");
            $app = new Ratchet\App('103.16.199.56', 9998, '0.0.0.0', $loop);
            $class = new CMS_MySocket();

            $loop->addPeriodicTimer(1, function(React\EventLoop\Timer\Timer $timer)use($class, $loop) {
                //Zend_Debug::dump($class->clients); die();

                foreach($class->clients as $client) {
                    $client->send(json_encode(array('data' => 1, 'time' => mktime())));
                }
            }
           );
            $app->route('/test', $class, array('*'));
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
        //die("ss");
        $app->run();
        //die();
    }

    public function run2Action() {
        $loop = React\EventLoop\Factory::create();
        $socket = new React\Socket\Server($loop);
        //die("zzzzzzzzz");		
        $socket->on('connection', function($conn) {
            $conn->pipe($conn);
        }
       );
        echo "Socket server listening on port 4000.\n";
        echo "You can connect to it by running: telnet localhost 4000\n";
        $socket->listen(9998, '103.16.199.56');
        //die("ss");
        $loop->run();
        //die();
    }

    public function runAction() {
        set_time_limit(0);
        $params = $this->getRequest()->getParams();
        $input = unserialize(base64_decode($params['data']));
        try {
            $loop = React\EventLoop\Factory::create();
            //$app = new Ratchet\App($params['host'], $params['port'],'127.0.0.1',$loop);
            $app = new Ratchet\App($params['host'], $params['port'], '0.0.0.0', $loop);
            $cc = new Rboco_Model_Rbocosystem();
            $data = $cc->get_socket_router_by_sid($params['id4socket']);
            //Zend_Debug::dump ($data); die(); 

            foreach($data as $k => $v) {
                $zvar = array();
                $class[$k] = new CMS_MySocket();
                //die("cc");
                $loop->addPeriodicTimer($v['interval_time'], function(React\EventLoop\Timer\Timer $timer)use($v, $k, $class, $loop) {
                    $c = new Model_System();
                    $zvar = unserialize(base64_decode($v['socket_attrs']));
                    //Zend_Debug::dump ($zvar); die(); 
                    $new = array();
                    $v1 = explode(",", $zvar['arrayinput']);

                    foreach($v1 as $vz) {
                        $cf2 = explode("=>", trim($vz));
                        $new[trim($cf2[0])] = trim($cf2[1]);
                    }
                    if(count($new)> 0) {

                        foreach($new as $idx => $key) {
                            $input['loop'] = array();
                            $input["loop"][$idx] = $key;
                            $result = $c->call_api($v['id'], $input);
                            //Zend_Debug::dump($result); die("1");

                            foreach($class[$k]->clients as $client) {
                                $client->send(json_encode($result));
                            }
                        }
                    } else {
                        $result = $c->call_api($v['id'], $input);
                        //Zend_Debug::dump($result); die("2");

                        foreach($class[$k]->clients as $client) {
                            $client->send(json_encode($result));
                        }
                    }
                }
               );
                //$app->route($v['router_socket'], $class[$k],array('*'));
                $app->route($v['router_socket'], $class[$k], array('*'));
            }
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            //die();
        }
        $app->run();
        //die("ok");
    }
}
//register_shutdown_function('shutdownFunction');
//function shutDownFunction() { 
//$error = error_get_last();
//// fatal error, E_ERROR === 1
//if ($error['type'] === E_ERROR) { 
//echo "EEEEEEEEEE"; die();
//} 
//}
