<?php


/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
@ ini_set("memory_limit", "-1");
date_default_timezone_set("Asia/Bangkok");
ini_set('display_errors', 1);
error_reporting(E_ALL);

class Socketserver3Controller extends Zend_Controller_Action {

    public function runAction() {
        set_time_limit(0);
        $params = $this->getRequest()->getParams();
        $input = unserialize(base64_decode($params['data']));
        $tweetid = 0;
        $firsttweet = true;
        $rediskey = array('webdomain' => 'INSTANT_OFFERING:HIT:WEBDOMAIN',
                          'webcategory' => 'INSTANT_OFFERING:HIT:WEBCATEGORY',
                          'speedy' => 'INSTANT_OFFERING:HIT:SPEEDY',
                          'f5webdomain' => 'INSTANT_OFFERING:F5:HIT:WEBDOMAIN',
                          'f5webcategory' => 'INSTANT_OFFERING:F5:HIT:WEBCATEGORY',
                          'f5speedy' => 'INSTANT_OFFERING:F5:HIT:SPEEDY',
                          'cfswebdomain' => 'INSTANT_OFFERING:CFS:HIT:WEBDOMAIN',
                          'cfswebcategory' => 'INSTANT_OFFERING:CFS:HIT:WEBCATEGORY',
                          'cfsspeedy' => 'INSTANT_OFFERING:CFS:HIT:SPEEDY');
        try {
            $loop = React\EventLoop\Factory::create();
            $app = new Ratchet\App('10.2.27.49', 9988, '0.0.0.0', $loop);
            $cc = new Rboco_Model_Rbocosystem();
            $data = $cc->get_socket_router();

            foreach($data as $k => $v) {
                $class[$k] = new CMS_MySocket();

                $loop->addPeriodicTimer(1, function(React\EventLoop\Timer\Timer $timer)use($rediskey, $v, $k, $class, $loop) {
                    $redis = new Redis();
                    $redis->connect('10.62.29.105', 6379, 60);
                    $redis->auth("zaKNGP8P6B8bj8gFCbXzwk5RDeBtuq8H");
                    $redis->select(date('w'));

                    foreach($rediskey as $idx => $key) {
                        $red = $redis->zRevRangeByScore($key, '+inf', '-inf', array('withscores' => TRUE, 'limit' => array(0, 5)));
                        // $this->stdout(count($red));
                        $datax = array();
                        $i = count($red);

                        foreach($red as $kx => $vx) {
                            $datax[$idx . 
                                $i] = array($kx, $vx);
                            $i --;
                        }
                        $result = array("data" => $datax, "dtime" => date("Y-m-d H:i:s"));

                        foreach($class[$k]->clients as $client) {
                            $client->send(json_encode($result));
                        }
                    }
                    $redis->close();
                }
               );
                $app->route($v['router_socket'], $class[$k]);
            }
           ;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
        //die("ss");
        $app->run();
        //die();
    }
}
