<?php

@ ini_set("memory_limit", "-1");
date_default_timezone_set("Asia/Bangkok");
ini_set("display_errors", "On");

class SocketserverController extends Zend_Controller_Action {

    public function runAction() {
        set_time_limit(0);
        $params = $this->getRequest()->getParams();
        $data = unserialize(base64_decode($params['data']));
        //die();
        $new = array();
        $v1 = explode(",", $data['arrayinput']);

        foreach($v1 as $vz) {
            $cf2 = explode("=>", trim($vz));
            $new[trim($cf2[0])] = trim($cf2[1]);
        }
        //Zend_Debug::dump($new); die();
        $address = $params['host'];
        $port = $params['port'];
        $echo = new CMS_Websocket_webrun("0.0.0.0", $port);
        try {
            $echo->run($params['id4socket'], array(), $new);
        }
        catch(Exception $e) {
            $echo->stdout($e->getMessage());
        }
        die();
    }

    public function runsAction() {
        $params = $this->getRequest()->getParams();
        set_time_limit(0);
        $address = $params['host'];
        $port = $params['port'];
        //$verboseMode = $params['verbose'];
        //$address = '127.0.0.1';
        //$port = 5001;
        $server = new \PushWebSocket\Server($address, $port, false);
        $n = new Model_System();
        $data = $n->call_api($params['id4socket']);
        //Zend_Debug::dump($data); die();
        $server->runningdata($data['data']);
        //die();		
    }
}
