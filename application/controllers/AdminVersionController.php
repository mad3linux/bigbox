<?php

class AdminVersionController extends Zend_Controller_Action
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */ {

    public function init() {
        /* Initialize action controller here */
    }

    public function testRevAction() {
        die("ss");
        $dd = new Model_Zprabametadata(1);
        $dd->tes();
        die("asd");
    }

    public function manageusersAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/table-ajax.js');

        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/ui-extended-modals.js');


        //$this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        //$this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css');
        //$this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datetimepicker/css/datetimepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');

        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        //$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        //$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
        //$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-daterangepicker/moment.min.js');
        //$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-daterangepicker/daterangepicker.js');
        //$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js');
        //$this->view->headScript()->appendFile('/assets/core/plugins/date-id-ID.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        //$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-touchspin/bootstrap.touchspin.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        //Zend_Debug::dump($identity);die();

        $mdl_sys = new Model_System();
        $roles = $mdl_sys->get_roles();
        //Zend_Debug::dump($roles); die();
        $this->view->roles = $roles;

        $mdl_gsys = new Model_Generalsystem();
        $ubis = $mdl_gsys->get_raw_ubis();
        //Zend_Debug::dump($ubis); die();
        $this->view->ubisarr = $ubis;

        $sub_ubis = array();
        //Zend_Debug::dump($data);
        $sub_ubis = $mdl_gsys->get_raw_sub_ubis($ubis[0]['id']);
        $this->view->sububisarr = $sububis;
    }

    public function manageuserAction() {
        //$this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($params);die();
        if (!isset($params['uname']) || $params['uname'] == '') {
            $this->_redirect('/admin/manageusers');
        }

        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');

        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        //Zend_Debug::dump($identity);die();
        $this->view->me = $identity;

        $mdl_sys = new Model_System();
        $roles = $mdl_sys->get_roles();
        //Zend_Debug::dump($roles); die();
        $this->view->roles = $roles;

        $mdl_zusr = new Model_Zusers();
        $data = $mdl_zusr->getuser($params['uname']);
        //Zend_Debug::dump($data);die();

        $mdl_gsys = new Model_Generalsystem();
        $ubis = $mdl_gsys->get_raw_ubis();
        //Zend_Debug::dump($ubis); die();
        $this->view->ubisarr = $ubis;

        $sub_ubis = array();
        //Zend_Debug::dump($data);
        if (isset($data['ubis_id']) && $data['ubis_id'] != '') {
            $sub_ubis = $mdl_gsys->get_raw_sub_ubis($data['ubis_id']);
            //Zend_Debug::dump($sub_ubis); die();
        } else {
            $sub_ubis = $mdl_gsys->get_raw_sub_ubis($ubis[0]['id']);
            //Zend_Debug::dump($sub_ubis); die();
        }

        $this->view->sububisarr = $sub_ubis;

        //Zend_Debug::dump($data);
        $rols = $mdl_sys->get_roles_by_uid($data['id']);
        //Zend_Debug::dump($rols); die('xxx');
        $rolearr2 = array();
        foreach ($rols as $k => $v) {
            array_push($rolearr2, $v['gid']);
        }

        $this->view->params = $params;
        $this->view->data = $data;
        $this->view->rolearr = $rolearr2;
    }

    public function managemenusAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/ui-extended-modals.js');

        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');

        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/fuelux/js/spinner.min.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');

        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-toastr/toastr.min.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-toastr/toastr.min.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/ui-toastr.js');

        $mdl_sys = new Model_System();
        if ($_POST) {
            Zend_Debug::dump($_POST);
            die();
        }

        $menu = $mdl_sys->get_menus_by_app(); //die();
        //Zend_Debug::dump($menu); die();

        $menus = array();
        $unmenus = array();
        $main = array();
        $main2 = array();
        $childs1 = array();
        $child1 = array();
        $childs2 = array();
        $child2 = array();

        //level1
        foreach ($menu as $k => $v) {
            if ($v['parent_id'] == "" || $v['parent_id'] == "0") {
                $menus[$v['id']] = $v;
                $main[] = $v['id'];
            } else {
                $childs1[$v['parent_id']][$v['id']] = $v;
            }
        }
        ksort($menus);
        ksort($childs1);
        //Zend_Debug::dump($menus);//die();
        //echo '<hr>';
        //Zend_Debug::dump($childs1);//die();
        //level2
        foreach ($childs1 as $k => $v) {
            if (in_array($k, $main)) {
                foreach ($v as $kk => $vv) {
                    $child1[$vv['id']] = $vv;
                    $main2[] = $vv['id'];
                }
            } else {
                $childs2[$k] = $v;
            }
        }
        //Zend_Debug::dump($child1);//die();
        //Zend_Debug::dump($main2);//die();
        //echo '<hr>';
        ksort($child1);
        ksort($childs2);
        //Zend_Debug::dump($child1);die();
        //echo '<hr>';
        //Zend_Debug::dump($childs2);die();
        //echo '<hr>';
        //level3
        foreach ($childs2 as $k => $v) {
            if (in_array($k, $main2)) {
                $child1[$k]['child'] = $v;
                //Zend_Debug::dump($child1[$k]);
                //echo $k.'<hr>';
            } else {
                foreach ($v as $kk => $vv) {
                    $unmenus[] = $vv;
                }
            }
        }
        //Zend_Debug::dump($child1);die();
        //level1
        foreach ($child1 as $k => $v) {
            $menus[$v['parent_id']]['child'][] = $v;
        }

        //Zend_Debug::dump($menus);//die();
        //Zend_Debug::dump($unmenus);die();

        $class_icon = array(
            "fa fa-rub" => "fa-rub",
            "fa fa-ruble" => "fa-ruble",
            "fa fa-rouble" => "fa-rouble",
            "fa fa-pagelines" => "fa-pagelines",
            "fa fa-stack-exchange" => "fa-stack-exchange",
            "fa fa-arrow-circle-o-right" => "fa-arrow-circle-o-right",
            "fa fa-arrow-circle-o-left" => "fa-arrow-circle-o-left",
            "fa fa-caret-square-o-left" => "fa-caret-square-o-left",
            "fa fa-toggle-left" => "fa-toggle-left",
            "fa fa-dot-circle-o" => "fa-dot-circle-o",
            "fa fa-wheelchair" => "fa-wheelchair",
            "fa fa-vimeo-square" => "fa-vimeo-square",
            "fa fa-try" => "fa-try",
            "fa fa-turkish-lira" => "fa-turkish-lira",
            "fa fa-adjust" => "fa-adjust",
            "fa fa-anchor" => "fa-anchor",
            "fa fa-archive" => "fa-archive",
            "fa fa-asterisk" => "fa-asterisk",
            "fa fa-ban" => "fa-ban",
            "fa fa-bar-chart-o" => "fa-bar-chart-o",
            "fa fa-barcode" => "fa-barcode",
            "fa fa-beer" => "fa-beer",
            "fa fa-bell" => "fa-bell",
            "fa fa-bell-o" => "fa-bell-o",
            "fa fa-bolt" => "fa-bolt",
            "fa fa-book" => "fa-book",
            "fa fa-bookmark" => "fa-bookmark",
            "fa fa-bookmark-o" => "fa-bookmark-o",
            "fa fa-briefcase" => "fa-briefcase",
            "fa fa-bug" => "fa-bug",
            "fa fa-building" => "fa-building",
            "fa fa-bullhorn" => "fa-bullhorn",
            "fa fa-bullseye" => "fa-bullseye",
            "fa fa-calendar" => "fa-calendar",
            "fa fa-calendar-o" => "fa-calendar-o",
            "fa fa-camera" => "fa-camera",
            "fa fa-camera-retro" => "fa-camera-retro",
            "fa fa-caret-square-o-down" => "fa-caret-square-o-down",
            "fa fa-caret-square-o-left" => "fa-caret-square-o-left",
            "fa fa-caret-square-o-right" => "fa-caret-square-o-right",
            "fa fa-caret-square-o-up" => "fa-caret-square-o-up",
            "fa fa-certificate" => "fa-certificate",
            "fa fa-check" => "fa-check",
            "fa fa-check-circle" => "fa-check-circle",
            "fa fa-check-circle-o" => "fa-check-circle-o",
            "fa fa-check-square" => "fa-check-square",
            "fa fa-check-square-o" => "fa-check-square-o",
            "fa fa-circle" => "fa-circle",
            "fa fa-circle-o" => "fa-circle-o",
            "fa fa-clock-o" => "fa-clock-o",
            "fa fa-cloud" => "fa-cloud",
            "fa fa-cloud-download" => "fa-cloud-download",
            "fa fa-cloud-upload" => "fa-cloud-upload",
            "fa fa-code" => "fa-code",
            "fa fa-code-fork" => "fa-code-fork",
            "fa fa-coffee" => "fa-coffee",
            "fa fa-cog" => "fa-cog",
            "fa fa-cogs" => "fa-cogs",
            "fa fa-collapse-o" => "fa-collapse-o",
            "fa fa-comment" => "fa-comment",
            "fa fa-comment-o" => "fa-comment-o",
            "fa fa-comments" => "fa-comments",
            "fa fa-comments-o" => "fa-comments-o",
            "fa fa-compass" => "fa-compass",
            "fa fa-credit-card" => "fa-credit-card",
            "fa fa-crop" => "fa-crop",
            "fa fa-crosshairs" => "fa-crosshairs",
            "fa fa-cutlery" => "fa-cutlery",
            "fa fa-dashboard" => "fa-dashboard",
            "fa fa-desktop" => "fa-desktop",
            "fa fa-dot-circle-o" => "fa-dot-circle-o",
            "fa fa-download" => "fa-download",
            "fa fa-edit" => "fa-edit",
            "fa fa-ellipsis-horizontal" => "fa-ellipsis-horizontal",
            "fa fa-ellipsis-vertical" => "fa-ellipsis-vertical",
            "fa fa-envelope" => "fa-envelope",
            "fa fa-envelope-o" => "fa-envelope-o",
            "fa fa-eraser" => "fa-eraser",
            "fa fa-exchange" => "fa-exchange",
            "fa fa-exclamation" => "fa-exclamation",
            "fa fa-exclamation-circle" => "fa-exclamation-circle",
            "fa fa-exclamation-triangle" => "fa-exclamation-triangle",
            "fa fa-expand-o" => "fa-expand-o",
            "fa fa-external-link" => "fa-external-link",
            "fa fa-external-link-square" => "fa-external-link-square",
            "fa fa-eye" => "fa-eye",
            "fa fa-eye-slash" => "fa-eye-slash",
            "fa fa-female" => "fa-female",
            "fa fa-fighter-jet" => "fa-fighter-jet",
            "fa fa-film" => "fa-film",
            "fa fa-filter" => "fa-filter",
            "fa fa-fire" => "fa-fire",
            "fa fa-fire-extinguisher" => "fa-fire-extinguisher",
            "fa fa-flag" => "fa-flag",
            "fa fa-flag-checkered" => "fa-flag-checkered",
            "fa fa-flag-o" => "fa-flag-o",
            "fa fa-flash" => "fa-flash",
            "fa fa-flask" => "fa-flask",
            "fa fa-folder" => "fa-folder",
            "fa fa-folder-o" => "fa-folder-o",
            "fa fa-folder-open" => "fa-folder-open",
            "fa fa-folder-open-o" => "fa-folder-open-o",
            "fa fa-frown-o" => "fa-frown-o",
            "fa fa-gamepad" => "fa-gamepad",
            "fa fa-gavel" => "fa-gavel",
            "fa fa-gear" => "fa-gear",
            "fa fa-gears" => "fa-gears",
            "fa fa-gift" => "fa-gift",
            "fa fa-glass" => "fa-glass",
            "fa fa-globe" => "fa-globe",
            "fa fa-group" => "fa-group",
            "fa fa-hdd-o" => "fa-hdd-o",
            "fa fa-headphones" => "fa-headphones",
            "fa fa-heart" => "fa-heart",
            "fa fa-heart-o" => "fa-heart-o",
            "fa fa-home" => "fa-home",
            "fa fa-inbox" => "fa-inbox",
            "fa fa-info" => "fa-info",
            "fa fa-info-circle" => "fa-info-circle",
            "fa fa-key" => "fa-key",
            "fa fa-keyboard-o" => "fa-keyboard-o",
            "fa fa-laptop" => "fa-laptop",
            "fa fa-leaf" => "fa-leaf",
            "fa fa-legal" => "fa-legal",
            "fa fa-lemon-o" => "fa-lemon-o",
            "fa fa-level-down" => "fa-level-down",
            "fa fa-level-up" => "fa-level-up",
            "fa fa-lightbulb-o" => "fa-lightbulb-o",
            "fa fa-location-arrow" => "fa-location-arrow",
            "fa fa-lock" => "fa-lock",
            "fa fa-magic" => "fa-magic",
            "fa fa-magnet" => "fa-magnet",
            "fa fa-mail-forward" => "fa-mail-forward",
            "fa fa-mail-reply" => "fa-mail-reply",
            "fa fa-mail-reply-all" => "fa-mail-reply-all",
            "fa fa-male" => "fa-male",
            "fa fa-map-marker" => "fa-map-marker",
            "fa fa-meh-o" => "fa-meh-o",
            "fa fa-microphone" => "fa-microphone",
            "fa fa-microphone-slash" => "fa-microphone-slash",
            "fa fa-minus" => "fa-minus",
            "fa fa-minus-circle" => "fa-minus-circle",
            "fa fa-minus-square" => "fa-minus-square",
            "fa fa-minus-square-o" => "fa-minus-square-o",
            "fa fa-mobile" => "fa-mobile",
            "fa fa-mobile-phone" => "fa-mobile-phone",
            "fa fa-money" => "fa-money",
            "fa fa-moon-o" => "fa-moon-o",
            "fa fa-move" => "fa-move",
            "fa fa-music" => "fa-music",
            "fa fa-pencil" => "fa-pencil",
            "fa fa-pencil-square" => "fa-pencil-square",
            "fa fa-pencil-square-o" => "fa-pencil-square-o",
            "fa fa-phone" => "fa-phone",
            "fa fa-phone-square" => "fa-phone-square",
            "fa fa-picture-o" => "fa-picture-o",
            "fa fa-plane" => "fa-plane",
            "fa fa-plus" => "fa-plus",
            "fa fa-plus-circle" => "fa-plus-circle",
            "fa fa-plus-square" => "fa-plus-square",
            "fa fa-power-off" => "fa-power-off",
            "fa fa-print" => "fa-print",
            "fa fa-puzzle-piece" => "fa-puzzle-piece",
            "fa fa-qrcode" => "fa-qrcode",
            "fa fa-question" => "fa-question",
            "fa fa-question-circle" => "fa-question-circle",
            "fa fa-quote-left" => "fa-quote-left",
            "fa fa-quote-right" => "fa-quote-right",
            "fa fa-random" => "fa-random",
            "fa fa-refresh" => "fa-refresh",
            "fa fa-reorder" => "fa-reorder",
            "fa fa-reply" => "fa-reply",
            "fa fa-reply-all" => "fa-reply-all",
            "fa fa-resize-horizontal" => "fa-resize-horizontal",
            "fa fa-resize-vertical" => "fa-resize-vertical",
            "fa fa-retweet" => "fa-retweet",
            "fa fa-road" => "fa-road",
            "fa fa-rocket" => "fa-rocket",
            "fa fa-rss" => "fa-rss",
            "fa fa-rss-square" => "fa-rss-square",
            "fa fa-search" => "fa-search",
            "fa fa-search-minus" => "fa-search-minus",
            "fa fa-search-plus" => "fa-search-plus",
            "fa fa-share" => "fa-share",
            "fa fa-share-square" => "fa-share-square",
            "fa fa-share-square-o" => "fa-share-square-o",
            "fa fa-shield" => "fa-shield",
            "fa fa-shopping-cart" => "fa-shopping-cart",
            "fa fa-sign-in" => "fa-sign-in",
            "fa fa-sign-out" => "fa-sign-out",
            "fa fa-signal" => "fa-signal",
            "fa fa-sitemap" => "fa-sitemap",
            "fa fa-smile-o" => "fa-smile-o",
            "fa fa-sort" => "fa-sort",
            "fa fa-sort-alpha-asc" => "fa-sort-alpha-asc",
            "fa fa-sort-alpha-desc" => "fa-sort-alpha-desc",
            "fa fa-sort-amount-asc" => "fa-sort-amount-asc",
            "fa fa-sort-amount-desc" => "fa-sort-amount-desc",
            "fa fa-sort-asc" => "fa-sort-asc",
            "fa fa-sort-desc" => "fa-sort-desc",
            "fa fa-sort-down" => "fa-sort-down",
            "fa fa-sort-numeric-asc" => "fa-sort-numeric-asc",
            "fa fa-sort-numeric-desc" => "fa-sort-numeric-desc",
            "fa fa-sort-up" => "fa-sort-up",
            "fa fa-spinner" => "fa-spinner",
            "fa fa-square" => "fa-square",
            "fa fa-square-o" => "fa-square-o",
            "fa fa-star" => "fa-star",
            "fa fa-star-half" => "fa-star-half",
            "fa fa-star-half-empty" => "fa-star-half-empty",
            "fa fa-star-half-full" => "fa-star-half-full",
            "fa fa-star-half-o" => "fa-star-half-o",
            "fa fa-star-o" => "fa-star-o",
            "fa fa-subscript" => "fa-subscript",
            "fa fa-suitcase" => "fa-suitcase",
            "fa fa-sun-o" => "fa-sun-o",
            "fa fa-superscript" => "fa-superscript",
            "fa fa-tablet" => "fa-tablet",
            "fa fa-tachometer" => "fa-tachometer",
            "fa fa-tag" => "fa-tag",
            "fa fa-tags" => "fa-tags",
            "fa fa-tasks" => "fa-tasks",
            "fa fa-terminal" => "fa-terminal",
            "fa fa-thumb-tack" => "fa-thumb-tack",
            "fa fa-thumbs-down" => "fa-thumbs-down",
            "fa fa-thumbs-o-down" => "fa-thumbs-o-down",
            "fa fa-thumbs-o-up" => "fa-thumbs-o-up",
            "fa fa-thumbs-up" => "fa-thumbs-up",
            "fa fa-ticket" => "fa-ticket",
            "fa fa-times" => "fa-times",
            "fa fa-clock-o" => "fa-clock-o",
            "fa fa-tint" => "fa-tint",
            "fa fa-toggle-down" => "fa-toggle-down",
            "fa fa-toggle-left" => "fa-toggle-left",
            "fa fa-toggle-right" => "fa-toggle-right",
            "fa fa-toggle-up" => "fa-toggle-up",
            "fa fa-trash-o" => "fa-trash-o",
            "fa fa-trophy" => "fa-trophy",
            "fa fa-truck" => "fa-truck",
            "fa fa-umbrella" => "fa-umbrella",
            "fa fa-unlock" => "fa-unlock",
            "fa fa-unlock-o" => "fa-unlock-o",
            "fa fa-unsorted" => "fa-unsorted",
            "fa fa-upload" => "fa-upload",
            "fa fa-user" => "fa-user",
            "fa fa-video-camera" => "fa-video-camera",
            "fa fa-volume-down" => "fa-volume-down",
            "fa fa-volume-off" => "fa-volume-off",
            "fa fa-volume-up" => "fa-volume-up",
            "fa fa-warning" => "fa-warning",
            "fa fa-wheelchair" => "fa-wheelchair",
            "fa fa-wrench" => "fa-wrench",
            "fa fa-check-square" => "fa-check-square",
            "fa fa-check-square-o" => "fa-check-square-o",
            "fa fa-circle" => "fa-circle",
            "fa fa-circle-o" => "fa-circle-o",
            "fa fa-dot-circle-o" => "fa-dot-circle-o",
            "fa fa-minus-square" => "fa-minus-square",
            "fa fa-minus-square-o" => "fa-minus-square-o",
            "fa fa-square" => "fa-square",
            "fa fa-square-o" => "fa-square-o",
            "fa fa-bitcoin" => "fa-bitcoin",
            "fa fa-btc" => "fa-btc",
            "fa fa-cny" => "fa-cny",
            "fa fa-dollar" => "fa-dollar",
            "fa fa-eur" => "fa-eur",
            "fa fa-euro" => "fa-euro",
            "fa fa-gbp" => "fa-gbp",
            "fa fa-inr" => "fa-inr",
            "fa fa-jpy" => "fa-jpy",
            "fa fa-krw" => "fa-krw",
            "fa fa-money" => "fa-money",
            "fa fa-rmb" => "fa-rmb",
            "fa fa-rouble" => "fa-rouble",
            "fa fa-rub" => "fa-rub",
            "fa fa-ruble" => "fa-ruble",
            "fa fa-rupee" => "fa-rupee",
            "fa fa-try" => "fa-try",
            "fa fa-turkish-lira" => "fa-turkish-lira",
            "fa fa-usd" => "fa-usd",
            "fa fa-won" => "fa-won",
            "fa fa-yen" => "fa-yen",
            "fa fa-align-center" => "fa-align-center",
            "fa fa-align-justify" => "fa-align-justify",
            "fa fa-align-left" => "fa-align-left",
            "fa fa-align-right" => "fa-align-right",
            "fa fa-bold" => "fa-bold",
            "fa fa-chain" => "fa-chain",
            "fa fa-chain-broken" => "fa-chain-broken",
            "fa fa-clipboard" => "fa-clipboard",
            "fa fa-columns" => "fa-columns",
            "fa fa-copy" => "fa-copy",
            "fa fa-cut" => "fa-cut",
            "fa fa-dedent" => "fa-dedent",
            "fa fa-eraser" => "fa-eraser",
            "fa fa-file" => "fa-file",
            "fa fa-file-o" => "fa-file-o",
            "fa fa-file-text" => "fa-file-text",
            "fa fa-file-text-o" => "fa-file-text-o",
            "fa fa-files-o" => "fa-files-o",
            "fa fa-floppy-o" => "fa-floppy-o",
            "fa fa-font" => "fa-font",
            "fa fa-indent" => "fa-indent",
            "fa fa-italic" => "fa-italic",
            "fa fa-link" => "fa-link",
            "fa fa-list" => "fa-list",
            "fa fa-list-alt" => "fa-list-alt",
            "fa fa-list-ol" => "fa-list-ol",
            "fa fa-list-ul" => "fa-list-ul",
            "fa fa-outdent" => "fa-outdent",
            "fa fa-paperclip" => "fa-paperclip",
            "fa fa-paste" => "fa-paste",
            "fa fa-repeat" => "fa-repeat",
            "fa fa-rotate-left" => "fa-rotate-left",
            "fa fa-rotate-right" => "fa-rotate-right",
            "fa fa-save" => "fa-save",
            "fa fa-scissors" => "fa-scissors",
            "fa fa-strikethrough" => "fa-strikethrough",
            "fa fa-table" => "fa-table",
            "fa fa-text-height" => "fa-text-height",
            "fa fa-text-width" => "fa-text-width",
            "fa fa-th" => "fa-th",
            "fa fa-th-large" => "fa-th-large",
            "fa fa-th-list" => "fa-th-list",
            "fa fa-underline" => "fa-underline",
            "fa fa-undo" => "fa-undo",
            "fa fa-unlink" => "fa-unlink",
            "fa fa-angle-double-down" => "fa-angle-double-down",
            "fa fa-angle-double-left" => "fa-angle-double-left",
            "fa fa-angle-double-right" => "fa-angle-double-right",
            "fa fa-angle-double-up" => "fa-angle-double-up",
            "fa fa-angle-down" => "fa-angle-down",
            "fa fa-angle-left" => "fa-angle-left",
            "fa fa-angle-right" => "fa-angle-right",
            "fa fa-angle-up" => "fa-angle-up",
            "fa fa-arrow-circle-down" => "fa-arrow-circle-down",
            "fa fa-arrow-circle-left" => "fa-arrow-circle-left",
            "fa fa-arrow-circle-o-down" => "fa-arrow-circle-o-down",
            "fa fa-arrow-circle-o-left" => "fa-arrow-circle-o-left",
            "fa fa-arrow-circle-o-right" => "fa-arrow-circle-o-right",
            "fa fa-arrow-circle-o-up" => "fa-arrow-circle-o-up",
            "fa fa-arrow-circle-right" => "fa-arrow-circle-right",
            "fa fa-arrow-circle-up" => "fa-arrow-circle-up",
            "fa fa-arrow-down" => "fa-arrow-down",
            "fa fa-arrow-left" => "fa-arrow-left",
            "fa fa-arrow-right" => "fa-arrow-right",
            "fa fa-arrow-up" => "fa-arrow-up",
            "fa fa-caret-down" => "fa-caret-down",
            "fa fa-caret-left" => "fa-caret-left",
            "fa fa-caret-right" => "fa-caret-right",
            "fa fa-caret-square-o-down" => "fa-caret-square-o-down",
            "fa fa-caret-square-o-left" => "fa-caret-square-o-left",
            "fa fa-caret-square-o-right" => "fa-caret-square-o-right",
            "fa fa-caret-square-o-up" => "fa-caret-square-o-up",
            "fa fa-caret-up" => "fa-caret-up",
            "fa fa-chevron-circle-down" => "fa-chevron-circle-down",
            "fa fa-chevron-circle-left" => "fa-chevron-circle-left",
            "fa fa-chevron-circle-right" => "fa-chevron-circle-right",
            "fa fa-chevron-circle-up" => "fa-chevron-circle-up",
            "fa fa-chevron-down" => "fa-chevron-down",
            "fa fa-chevron-left" => "fa-chevron-left",
            "fa fa-chevron-right" => "fa-chevron-right",
            "fa fa-chevron-up" => "fa-chevron-up",
            "fa fa-hand-o-down" => "fa-hand-o-down",
            "fa fa-hand-o-left" => "fa-hand-o-left",
            "fa fa-hand-o-right" => "fa-hand-o-right",
            "fa fa-hand-o-up" => "fa-hand-o-up",
            "fa fa-long-arrow-down" => "fa-long-arrow-down",
            "fa fa-long-arrow-left" => "fa-long-arrow-left",
            "fa fa-long-arrow-right" => "fa-long-arrow-right",
            "fa fa-long-arrow-up" => "fa-long-arrow-up",
            "fa fa-toggle-down" => "fa-toggle-down",
            "fa fa-toggle-left" => "fa-toggle-left",
            "fa fa-toggle-right" => "fa-toggle-right",
            "fa fa-toggle-up" => "fa-toggle-up",
            "fa fa-backward" => "fa-backward",
            "fa fa-eject" => "fa-eject",
            "fa fa-fast-backward" => "fa-fast-backward",
            "fa fa-fast-forward" => "fa-fast-forward",
            "fa fa-forward" => "fa-forward",
            "fa fa-fullscreen" => "fa-fullscreen",
            "fa fa-pause" => "fa-pause",
            "fa fa-play" => "fa-play",
            "fa fa-play-circle" => "fa-play-circle",
            "fa fa-play-circle-o" => "fa-play-circle-o",
            "fa fa-resize-full" => "fa-resize-full",
            "fa fa-resize-small" => "fa-resize-small",
            "fa fa-step-backward" => "fa-step-backward",
            "fa fa-step-forward" => "fa-step-forward",
            "fa fa-stop" => "fa-stop",
            "fa fa-youtube-play" => "fa-youtube-play",
            "fa fa-adn" => "fa-adn",
            "fa fa-android" => "fa-android",
            "fa fa-apple" => "fa-apple",
            "fa fa-bitbucket" => "fa-bitbucket",
            "fa fa-bitbucket-square" => "fa-bitbucket-square",
            "fa fa-bitcoin" => "fa-bitcoin",
            "fa fa-btc" => "fa-btc",
            "fa fa-css3" => "fa-css3",
            "fa fa-dribbble" => "fa-dribbble",
            "fa fa-dropbox" => "fa-dropbox",
            "fa fa-facebook" => "fa-facebook",
            "fa fa-facebook-square" => "fa-facebook-square",
            "fa fa-flickr" => "fa-flickr",
            "fa fa-foursquare" => "fa-foursquare",
            "fa fa-github" => "fa-github",
            "fa fa-github-alt" => "fa-github-alt",
            "fa fa-github-square" => "fa-github-square",
            "fa fa-gittip" => "fa-gittip",
            "fa fa-google-plus" => "fa-google-plus",
            "fa fa-google-plus-square" => "fa-google-plus-square",
            "fa fa-html5" => "fa-html5",
            "fa fa-instagram" => "fa-instagram",
            "fa fa-linkedin" => "fa-linkedin",
            "fa fa-linkedin-square" => "fa-linkedin-square",
            "fa fa-linux" => "fa-linux",
            "fa fa-maxcdn" => "fa-maxcdn",
            "fa fa-pagelines" => "fa-pagelines",
            "fa fa-pinterest" => "fa-pinterest",
            "fa fa-pinterest-square" => "fa-pinterest-square",
            "fa fa-renren" => "fa-renren",
            "fa fa-skype" => "fa-skype",
            "fa fa-stack-exchange" => "fa-stack-exchange",
            "fa fa-stack-overflow" => "fa-stack-overflow",
            "fa fa-trello" => "fa-trello",
            "fa fa-tumblr" => "fa-tumblr",
            "fa fa-tumblr-square" => "fa-tumblr-square",
            "fa fa-twitter" => "fa-twitter",
            "fa fa-twitter-square" => "fa-twitter-square",
            "fa fa-vimeo-square" => "fa-vimeo-square",
            "fa fa-vk" => "fa-vk",
            "fa fa-weibo" => "fa-weibo",
            "fa fa-windows" => "fa-windows",
            "fa fa-xing" => "fa-xing",
            "fa fa-xing-square" => "fa-xing-square",
            "fa fa-youtube" => "fa-youtube",
            "fa fa-youtube-play" => "fa-youtube-play",
            "fa fa-youtube-square" => "fa-youtube-square",
            "fa fa-ambulance" => "fa-ambulance",
            "fa fa-h-square" => "fa-h-square",
            "fa fa-hospital" => "fa-hospital",
            "fa fa-medkit" => "fa-medkit",
            "fa fa-plus-square" => "fa-plus-square",
            "fa fa-stethoscope" => "fa-stethoscope",
            "fa fa-user-md" => "fa-user-md",
            "fa fa-wheelchair" => "fa-wheelchair");

        //Zend_Debug::dump($class_icon);die();
        $this->view->icon = $class_icon;
        $this->view->menus = $menus;
        $this->view->unmenus = $unmenus;
        //Zend_Debug::dump($armenu);die();
    }

    public function managerolesAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/ui-extended-modals.js');

        $mdl_sys = new Model_System();
        if ($_POST) {
            Zend_Debug::dump($_POST);
            die();
            if (count($_POST['selected']) >= 1) {
                $id = implode(',', $_POST['selected']);
                //Zend_Debug::dump($id); die();
                $delete = $mdl_sys->delete_menus($id);
                if ($delete['success']) {
                    
                } else {
                    
                }

                $this->_redirect('/kpu/system/manage4menus');
            }
        }

        $roles = $mdl_sys->get_roles(); //die();
        //Zend_Debug::dump($roles); die();

        $this->view->roles = $roles;
    }

    public function manageroleAction() {
        $params = $this->getRequest()->getParams();
        $mdl_sys = new Model_System();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        if ($_POST) {
            Zend_Debug::dump($_POST);
            die();
        }

        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-nestable/jquery.nestable.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-nestable/jquery.nestable.js');
        $app = $mdl_sys->get_controllers(true);
        //Zend_Debug::dump($app); die();

        $menu = $mdl_sys->get_menus_by_app(); //die();
        //Zend_Debug::dump($menu); die();

        $menus = array();
        $unmenus = array();
        $main = array();
        $main2 = array();
        $childs1 = array();
        $child1 = array();
        $childs2 = array();
        $child2 = array();

        //level1
        foreach ($menu as $k => $v) {
            if ($v['parent_id'] == "" || $v['parent_id'] == "0") {
                $menus[$v['id']] = $v;
                $main[] = $v['id'];
            } else {
                $childs1[$v['parent_id']][$v['id']] = $v;
            }
        }
        ksort($menus);
        ksort($childs1);
        //Zend_Debug::dump($menus);//die();
        //echo '<hr>';
        //Zend_Debug::dump($childs1);//die();
        //level2
        foreach ($childs1 as $k => $v) {
            if (in_array($k, $main)) {
                foreach ($v as $kk => $vv) {
                    $child1[$vv['id']] = $vv;
                    $main2[] = $vv['id'];
                }
            } else {
                $childs2[$k] = $v;
            }
        }
        //Zend_Debug::dump($child1);//die();
        //Zend_Debug::dump($main2);//die();
        //echo '<hr>';
        ksort($child1);
        ksort($childs2);
        //Zend_Debug::dump($child1);die();
        //echo '<hr>';
        //Zend_Debug::dump($childs2);die();
        //echo '<hr>';
        //level3
        foreach ($childs2 as $k => $v) {
            if (in_array($k, $main2)) {
                $child1[$k]['child'] = $v;
                //Zend_Debug::dump($child1[$k]);
                //echo $k.'<hr>';
            } else {
                foreach ($v as $kk => $vv) {
                    $unmenus[] = $vv;
                }
            }
        }
        //Zend_Debug::dump($child1);die();
        //level1
        foreach ($child1 as $k => $v) {
            $menus[$v['parent_id']]['child'][] = $v;
        }

        //Zend_Debug::dump($menus);//die();
        //Zend_Debug::dump($unmenus);die();

        $role = array();
        if (isset($params['gid'])) {
            $role = $mdl_sys->get_role_by_id($params['gid']);
            //Zend_Debug::dump($role);die();
        }

        $activemenus = array();
        if (isset($params['gid'])) {
            $actmenu = $mdl_sys->get_menus_by_role($params['gid']);
            foreach ($actmenu as $k => $v) {
                $activemenus[] = $v['id'];
            }
            //Zend_Debug::dump($activemenus);die();
        }

        $roles = $mdl_sys->get_roles(); //die();
        //Zend_Debug::dump($roles); die();

        $this->view->roles = $roles;

        $this->view->role = $role;
        $this->view->menus = $menus;
        $this->view->unmenus = $unmenus;
        $this->view->activemenus = $activemenus;
        $this->view->app = $app;
        $this->view->params = $params;
    }

}
