<?php

/**
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>,Jul 15, 2014
 *
 */

class AjaxController extends Zend_Controller_Action {

    public function init() {
        
        /* Initialize action controller here */
    }

    public function callapiAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $params = $this->getRequest()->getParams();
        $this->_helper->layout->disableLayout();
        //$params['api']
        $dd = new Model_Zprabapage();
        $m1 = new Model_Zpraba4api();
        $varC = $dd->get_api($params['id']);
        $conn = unserialize($varC['conn_params']);
        $result = $m1->execute_api($conn, $varC, $params);
        // Zend_Debug::dump($result); die();
        switch($params['type']) {

            case 'options' : foreach($result['data'] as $v) {
                echo "<option value='" . $v['xkey'] . "'>" . $v['xvalue'] . "</option>";
            }
            break;

            case 'radios' : foreach($result['data'] as $v) {
                echo "<option value='" . $v['xkey'] . "'>" . $v['xvalue'] . "</option>";
            }
        }
        die();
    }

    public function deleteportletAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            // Zend_Debug::dump($_POST); die();
            $mdl_zusr = new Model_Prabasystem();
            $delete = $mdl_zusr->delete_portlet($_POST['uid']);
            if($delete['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$delete['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $delete = array('retCode' => '11',
                                'retMsg' =>$delete['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function notifAction() {
        $d = new CMS_General();
        $cc = new Model_Minbox();
        $data = $cc->notreadinbox('task');
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        switch($params['layout']) {
            default : echo '<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                                <i class="icon-calendar"></i>
                                <span class="badge badge-default"> ' . count($data). ' </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="external">
                                    <h3>You have
                                        <span class="bold">' . count($data). ' New</span> Messages</h3>
                                    <a href="app_inbox.html">view all</a>
                                </li>';
            if(count($data)> 0) {

                foreach($data as $v) {
                    echo '<li>
                                    <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                       
                                        <li>
                                            <a href="#">
                                                <span class="photo">
                                                    <img src="/assets/core/layouts/layout3/img/avatar.png" class="img-circle" alt=""> </span>
                                                <span class="subject">
                                                    <span class="from"> Richard Doe </span>
                                                    <span class="time">' . $d->time_elapsed_string(strtotime($v['incoming_date'])). ' </span>
                                                </span>
                                                <span class="message"> ' . strip_tags($v['message']). '</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>';
                }
            }
            echo '</ul>';
            die();
            case '5' : echo '<button type="button" class="btn btn-sm md-skip dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                        <i class="icon-wallet"></i>
                                        <span class="badge">' . count($data). '</span>
                                    </button>
                                    
                                    <ul class="dropdown-menu-v2">
                                        <li class="external">
                                            <h3>
                                                <span class="bold">' . count($data). ' New</span> Messages</h3>
                                            <a href="#">view all</a>
                                        </li>
                                        <li>
                                            <ul class="dropdown-menu-list scroller" style="height: 250px; padding: 0;" data-handle-color="#637283">';
            if(count($data)> 0) {

                foreach($data as $v) {
                    echo ' <li>
                                                    <a href="javascript:;">
                                                        <span class="details">
                                                            <span class="label label-sm label-icon label-success md-skip">
                                                              <span class="from"> Richard Doe </span>
                                                            </span> ' . strip_tags($v['message']). ' </span>
                                                        <span class="time">' . $d->time_elapsed_string(strtotime($v['incoming_date'])). '</span>
                                                    </a>
                                                </li>';
                }
            }
            echo '</ul>
                                        </li>
                                    </ul>';
            die();
            break;
        }
    }

    public function inboxAction() {
        $d = new CMS_General();
        $cc = new Model_Minbox();
        $data = $cc->notreadinbox('inbox');
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        switch($params['layout']) {
            default : echo '<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
							<i class="icon-envelope-open"></i>
							<span class="badge badge-default"> ' . count($data). ' </span>
						</a>
						<ul class="dropdown-menu">
							<li class="external">
								<h3>You have
									<span class="bold">' . count($data). ' New</span> Messages</h3>
									<!--<a href="app_inbox.html">view all</a>-->
							</li>';
            if(count($data)> 0) {

                foreach($data as $v) {
                    echo '<li>
								<ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
								   
									<li>
										<a href="#">
											<span class="photo">
												<img src="/assets/core/layouts/layout3/img/avatar.png" class="img-circle" alt=""> </span>
											<span class="subject">
												<span class="from"> Richard Doe </span>
												<span class="time">' . $d->time_elapsed_string(strtotime($v['incoming_date'])). ' </span>
											</span>
											<span class="message"> ' . strip_tags($v['message']). '</span>
										</a>
									</li>
								</ul>
							</li>';
                }
            }
            echo '</ul>';
            die();
            break;
            case '5' : echo '<button type="button" class="btn btn-sm md-skip dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                        <i class="icon-wallet"></i>
                                        <span class="badge">' . count($data). '</span>
                                    </button>
                                    
                                    <ul class="dropdown-menu-v2">
                                        <li class="external">
                                            <h3>
                                                <span class="bold">' . count($data). ' New</span> Messages</h3>
                                            <a href="#">view all</a>
                                        </li>
                                        <li>
                                            <ul class="dropdown-menu-list scroller" style="height: 250px; padding: 0;" data-handle-color="#637283">';
            if(count($data)> 0) {

                foreach($data as $v) {
                    echo ' <li>
                                                    <a href="javascript:;">
                                                        <span class="details">
                                                            <span class="label label-sm label-icon label-success md-skip">
                                                              <span class="from"> Richard Doe </span>
                                                            </span> ' . strip_tags($v['message']). ' </span>
                                                        <span class="time">' . $d->time_elapsed_string(strtotime($v['incoming_date'])). '</span>
                                                    </a>
                                                </li>';
                }
            }
            echo '</ul>
                                        </li>
                                    </ul>';
            die();
            break;
        }
    }

    public function listportletjsonAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $mdl_admin = new Model_Prabasystem();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params); die();
        $list = $mdl_admin->listapiportletjson($params);
        // Zend_Debug::dump($list); die();
        $zz = 1;

        foreach($list as $k => $v) {
            $records['total'] = 10;
            $records['movies'][0]['id'] = "";
            $records['movies'][0]['title'] = "";
            if(isset($params['id'])) {
                $records['total'] = 1;
            }
            $records['movies'][$zz]['id'] = $v['id'];
            if(is_numeric($v['id'])) {
                $records['movies'][$zz]['title'] = $v['id'] . '_' . $v['portlet_name'];
            } else {
                $records['movies'][$zz]['title'] = $v['portlet_name'];
            }
            $zz ++;
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        // echo json_encode($records);
        $jsonp_callback = isset($_GET['callback'])? $_GET['callback'] : null;
        $json = json_encode($records);
        print $jsonp_callback ? "$jsonp_callback($json)" : $json;
        die();
    }

    public function tableappAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $this->_helper->layout->disableLayout();
        // $this->_helper->viewRenderer->setNoRender(true);
        $mdl_admin = new Model_Admin();
        $params = $this->getRequest()->getParams();
        $countgraph = $mdl_admin->count_listuser($params['app']);
        // Zend_Debug::dump($countgraph);die();
        $listgraph = $mdl_admin->get_listuser($params['app']);
        // Zend_Debug::dump($listgraph);die();
        $iTotalRecords = intval($countgraph);
        $iDisplayLength = intval($_GET['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_GET['iDisplayStart']);
        $sEcho = intval($_GET['sEcho']);
        $records = array();
        $records["aaData"] = array();
        $end = $iDisplayStart + $iDisplayLength;

        foreach($listgraph as $k => $v) {
            $records["aaData"][] = array($iDisplayStart+$k+ 1,
                                        $v['id'],
                                        $v['uname'],
                                        $v['fullname'],
                                        $v['email'],
                                        $v['sub_ubis'],
                                        $v['ubis'],
                                        $v['jabatan'],
                                        $v['mobile_phone'],
                                        $v['fixed_phone'],
                                         '<div style="text-align:center"><a href="/admin/manageuser/uname/' .$v['uname']. '" data-id="' .$v['id']. '" data-uname="' .$v['uname']. '" data-fullname="' .$v['fullname']. '" data-email="' .$v['email']. '" data-ubis="' .$v['ubis']. '" data-sububis="' .$v['sub_ubis']. '" data-sububisid="' .$v['sub_ubis_id']. '" data-position="' .$v['jabatan']. '" data-mobilephone="' .$v['mobile_phone']. '" data-fixedphone="' .$v['fixed_phone']. '" class="btn btn-xs green btn-edit"><i class="fa fa-search"></i> View User</a>' .  '<a href="javascript:;" data-id="'.$v['id'].'" data-uname="'.$v['uname'].'" data-fullname="'.$v['fullname'].'" data-email="'.$v['email'].'" data-ubis="'.$v['ubis'].'" data-sububis="'.$v['sub_ubis'].'" data-sububisid="'.$v['sub_ubis_id'].'" data-position="'.$v['jabatan'].'" data-mobilephone="'.$v['mobile_phone'].'" data-fixedphone="'.$v['fixed_phone'].'" class="btn btn-xs blue btn-edit"><i class="fa fa-pencil"></i> Edit User</a>'. '<a href="javascript:;" data-id="' .$v['id']. '" data-uname="' .$v['uname']. '" data-fullname="' .$v['fullname']. '" data-email="' .$v['email']. '" data-ubis="' .$v['ubis']. '" data-sububis="' .$v['sub_ubis']. '" data-sububisid="' .$v['sub_ubis_id']. '" data-position="' .$v['jabatan']. '" data-mobilephone="' .$v['mobile_phone']. '" data-fixedphone="' .$v['fixed_phone']. '" class="btn btn-xs red btn-delete"><i class="fa fa-times"></i> Delete User</a></div>');
        }
        // Zend_Debug::dump($records);die();
        $records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;
        
        /*
         * $gg = new Escort_Model_Kpu(); $res = $gg->get_datakpu($data2['witel']); //var_dump($res);die();
         */
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }

    public function table1Action() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $params = $this->getRequest()->getParams();
        $this->_helper->layout->disableLayout();
        // $this->_helper->viewRenderer->setNoRender(true);
        $mdl_admin = new Model_Admin();
        $countgraph = $mdl_admin->count_listuser($params['app'], $_GET);
        // Zend_Debug::dump($countgraph);die();
        //die("xxx");
        $listgraph = $mdl_admin->get_listuser($params['app'], $_GET);
        // Zend_Debug::dump($listgraph);die();
        $iTotalRecords = intval($countgraph);
        $iDisplayLength = intval($_GET['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_GET['iDisplayStart']);
        $sEcho = intval($_GET['sEcho']);
        $records = array();
        $records["aaData"] = array();
        $end = $iDisplayStart + $iDisplayLength;

        foreach($listgraph as $k => $v) {
            $records["aaData"][] = array($iDisplayStart+$k+ 1,
                                        $v['id'],
                                        $v['uname'],
                                        $v['fullname'],
                                        $v['email'],
                                        $v['ubis'],
                                        $v['sub_ubis'],
                                        $v['jabatan'],
                                        $v['mobile_phone'],
                                        $v['fixed_phone'],
                                         '<div style="text-align:center"><a href="/admin/manageuser/uname/' .$v['uname']. '" data-id="' .$v['id']. '" data-uname="' .$v['uname']. '" data-fullname="' .$v['fullname']. '" data-email="' .$v['email']. '" data-ubis="' .$v['ubis']. '" data-sububis="' .$v['sub_ubis']. '" data-sububisid="' .$v['sub_ubis_id']. '" data-position="' .$v['jabatan']. '" data-mobilephone="' .$v['mobile_phone']. '" data-fixedphone="' .$v['fixed_phone']. '" class="btn btn-xs green btn-edit"><i class="fa fa-search"></i> View User</a>' .  '<a href="javascript:;" data-id="'.$v['id'].'" data-uname="'.$v['uname'].'" data-fullname="'.$v['fullname'].'" data-email="'.$v['email'].'" data-ubis="'.$v['ubis'].'" data-sububis="'.$v['sub_ubis'].'" data-sububisid="'.$v['sub_ubis_id'].'" data-position="'.$v['jabatan'].'" data-mobilephone="'.$v['mobile_phone'].'" data-fixedphone="'.$v['fixed_phone'].'" class="btn btn-xs blue btn-edit"><i class="fa fa-pencil"></i> Edit User</a>'. '<a href="javascript:;" data-id="' .$v['id']. '" data-uname="' .$v['uname']. '" data-fullname="' .$v['fullname']. '" data-email="' .$v['email']. '" data-ubis="' .$v['ubis']. '" data-sububis="' .$v['sub_ubis']. '" data-sububisid="' .$v['sub_ubis_id']. '" data-position="' .$v['jabatan']. '" data-mobilephone="' .$v['mobile_phone']. '" data-fixedphone="' .$v['fixed_phone']. '" class="btn btn-xs red btn-delete"><i class="fa fa-times"></i> Delete User</a></div>');
        }
        // Zend_Debug::dump($records);die();
        $records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;
        
        /*
         * $gg = new Escort_Model_Kpu(); $res = $gg->get_datakpu($data2['witel']); //var_dump($res);die();
         */
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }

    public function table2Action() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $this->_helper->layout->disableLayout();
        // $this->_helper->viewRenderer->setNoRender(true);
        $mdl_admin = new Model_Admin();
        $mdl_device = new Model_Device();
        // $ubis = $mdl_admin->get_userubis($identity->uname);
        $ubis = $mdl_admin->get_userubis('720200');
        // Zend_Debug::dump($ubis);die();
        $countdevice = 0;
        $listdevice = array();
        if($ubis != false && isset($ubis['name'])) {
            // $countdevice = $mdl_device->count_listdeviceuser($ubis['name']);
            $countdevice = $mdl_device->count_listdeviceuser('NAD (ACEH)');
            // Zend_Debug::dump($countdevice);die();
            // $listdevice = $mdl_device->get_listdeviceuser($ubis['name']);
            $listdevice = $mdl_device->get_listdeviceuser('NAD (ACEH)');
            // Zend_Debug::dump($listdevice);die();
        }
        $iTotalRecords = intval($countdevice);
        $iDisplayLength = intval($_GET['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_GET['iDisplayStart']);
        $sEcho = intval($_GET['sEcho']);
        $records = array();
        $records["aaData"] = array();
        $end = $iDisplayStart + $iDisplayLength;

        foreach($listdevice as $k => $v) {
            $records["aaData"][] = array($iDisplayStart+$k+ 1,
                                        $v['kawasan'],
                                        $v['reg'],
                                        $v['wil_id'],
                                        $v['witel'],
                                        $v['datel_id'],
                                        $v['kandatel'],
                                        $v['sero_serv_id'],
                                        $v['device_type'],
                                        $v['sero_sert_abbreviation'],
                                        $v['cust_name'],
                                         // $v['cust_type'],$v['port'],
                                        $v['ip_address'],
                                        $v['street'],
                                        $v['city'],
                                        $v['user_cat'],
                                         '<a class="edit" href="javascript:;">Edit</a><a class="delete" href="javascript:;">Delete</a>');
        }
        // Zend_Debug::dump($records);die();
        $records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }

    public function getsububisAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        $mdl_gsys = new Model_Generalsystem();
        $sub_ubis = $mdl_gsys->get_raw_sub_ubis($params['id']);
        $sububisarr = array();
        $html = '<option value="" >none</option>';

        foreach($sub_ubis as $k => $v) {
            $html .= '<option value="' . $v['id'] . '" ' .((isset($params['sub'])&& $params['sub'] == $v['id'])? 'selected' : ''). ' >' . $v['name'] . '</option>';
        }
        echo $html;
        die();
    }

    public function adduserAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $this->_helper->layout->disableLayout();
        // $this->_helper->viewRenderer->setNoRender(true);
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            preg_match_all('!\d+!', $_POST['mainrole'], $matches);
            $_POST['mainrole'] = $matches[0][0];
            $mdl_zusr = new Model_Zusers();
            $update = $mdl_zusr->update_insert_user($_POST);
            if($update['result']) {
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$update['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function deleteuserAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $this->_helper->layout->disableLayout();
        // $this->_helper->viewRenderer->setNoRender(true);
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            // Zend_Debug::dump($_POST); die();
            $mdl_zusr = new Model_Zusers();
            $delete = $mdl_zusr->delete_user($_POST['uid']);
            if($delete['result']) {
                $result = array('retCode' => '00',
                                'retMsg' =>$delete['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $delete = array('retCode' => '11',
                                'retMsg' =>$delete['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function addmenuAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $this->_helper->layout->disableLayout();
        // $this->_helper->viewRenderer->setNoRender(true);
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            // Zend_Debug::dump($_POST); die();
            $mdl_sys = new Model_System();
            $update = $mdl_sys->insert_update_menu($_POST);
            if($update['result']) {
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$update['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function deletemenuAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $this->_helper->layout->disableLayout();
        // $this->_helper->viewRenderer->setNoRender(true);
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if(isset($_POST['data'])) {
            // Zend_Debug::dump($_POST['data']); die();
            $mdl_sys = new Model_System();
            $update = $mdl_sys->delete_menus($_POST['data']);
            if($update['result']) {
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$update['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function activatemenuAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $this->_helper->layout->disableLayout();
        // $this->_helper->viewRenderer->setNoRender(true);
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            // Zend_Debug::dump($_POST['data']); die();
            $mdl_sys = new Model_System();
            $update = $mdl_sys->activate_menu($_POST['id'], $_POST['newstat']);
            if($update['result']) {
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$update['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function insertdevicehistoryAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $this->_helper->layout->disableLayout();
        // $this->_helper->viewRenderer->setNoRender(true);
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            $column = array('kawasan',
                            'reg',
                            'wil_id',
                            'witel',
                            'datel_id',
                            'kandatel',
                            'sero_serv_id',
                            'device_type',
                            'sero_sert_abbreviation',
                            'cust_name',
                            'port',
                            'ip_address',
                            'street',
                            'city',
                            'user_cat');
            $column = array_flip($column);
            // Zend_Debug::dump($_POST); die();
            // Zend_Debug::dump($column); //die();
            $mdl_device = new Model_Device();
            $before = $mdl_device->get_deviceuser($_POST[6], $_POST[7], $_POST[10]);
            // Zend_Debug::dump($before); die();
            $check = false;
            $datetime = date('Y-m-d H:i:s');
            $msg = $identity->uname . ' - ' . $datetime . ', [SERO SERV ID:' . $_POST[6] . ';DEVICE TYPE:' . $_POST[7] . ';PORT:' . $_POST[10] . '] ';
            $after = array();
            $before['updatedatetime'] = $datetime;
            $before['updateby'] = $identity->uname;

            foreach($before as $k => $v) {
                // echo $k.'<br>';
                // echo $column[$k].'<br>';
                if(isset($column[$k])&& isset($_POST[$column[$k]])&& $_POST[$column[$k]] != $v) {
                    $after[$k] = $_POST[$column[$k]];
                    $check = true;
                    $msg .= $k . ' ' . $v . ' berubah menjadi ' . $_POST[$column[$k]] . ', ';
                }
            }
            // Zend_Debug::dump($msg); die();
            // Zend_Debug::dump($after); die();
            if($check) {
                $insert = $mdl_device->insert_device_history($before);
                // Zend_Debug::dump($insert); die();
                if($insert['result']) {
                    $update = $mdl_device->update_device($after, $_POST[6], $_POST[7], $_POST[10]);
                    // Zend_Debug::dump($update); die();
                    if($update['result']) {
                        $mdl_zwall = new Model_Zwall();
                        $data = array('xhpc_message' =>$msg,
                                      'gid' => 107);
                        // Zend_Debug::dump($data);die();
                        $mdl_zwall->insert_wall($data, $identity->uid, false, 'Info');
                        $result = array('retCode' => '00',
                                        'retMsg' =>$update['message'],
                                        'result' => true,
                                        'data' =>$_POST);
                    } else {
                        $result = array('retCode' => '11',
                                        'retMsg' =>$update['message'],
                                        'result' => false,
                                        'data' =>$_POST);
                    }
                } else {
                    $result = array('retCode' => '11',
                                    'retMsg' =>$insert['message'],
                                    'result' => false,
                                    'data' =>$_POST);
                }
            } else {
                $result = array('retCode' => '12',
                                'retMsg' => "No change found.",
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function updateroleAction() {
        // $auth = Zend_Auth::getInstance();
        // $identity = $auth->getIdentity();
        // Zend_Debug::dump($identity);die();
        $this->_helper->layout->disableLayout();
        // $this->_helper->viewRenderer->setNoRender(true);
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            // Zend_Debug::dump($_POST); die();
            $perm = array();
            if(isset($_POST['perm'])&& count($_POST['perm'])> 0) {

                foreach($_POST['perm'] as $k => $v) {

                    foreach($v as $kk => $vv) {
                        $perm[$k][] = $kk;
                    }
                }
            }
            // Zend_Debug::dump($perm); die();
            $_POST['perms'] = $perm;
            $mdl_sys = new Model_System();
            $remove = $mdl_sys->insert_update_role($_POST);
            if($remove['result']) {
                $result = array('retCode' => '00',
                                'retMsg' =>$remove['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$remove['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function deleterolesAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $this->_helper->layout->disableLayout();
        // $this->_helper->viewRenderer->setNoRender(true);
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if(isset($_POST['data'])) {
            // Zend_Debug::dump($_POST['data']); die();
            $mdl_sys = new Model_System();
            $update = $mdl_sys->delete_roles($_POST['data']);
            if($update['result']) {
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$update['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function changepriorityAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $this->_helper->layout->disableLayout();
        // $this->_helper->viewRenderer->setNoRender(true);
        $mdl_zmon = new Model_Zmap();
        $cat =(isset($_GET['cat']))? $_GET['cat'] : - 1;
        $cat2 =(isset($_GET['cluster']))? $_GET['cluster'] : - 1;
        $loc = $mdl_zmon->get_priority_options($cat);
        // Zend_Debug::dump($loc);die();
        header("Access-Control-Allow-Origin: *");
        // header('Content-Type: application/json');
        // echo json_encode($result);
        echo '<option value="-">ALL</option>';

        foreach($loc as $k => $v) {
            echo '<option value="' . $v['cluster'] . '">' . $v['cluster'] . '</option>';
        }
        die();
    }
}
