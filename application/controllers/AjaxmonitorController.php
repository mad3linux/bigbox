<?php

/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class AjaxmonitorController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    public function gettabpaneAction() {

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $params = $this->getRequest()->getParams();
// Zend_Debug::dump($params);die();
// if($params['tpanename']=='collaboration'){
// }

        $mdl_monitor = new Model_Monitor();

//-=== SURVEILLANCE
        if ($params['tpanename'] == 'surveillance') {

            $tmp_masiv = $mdl_monitor->getMASIVloc();
            // Zend_Debug::dump($tmp_masiv);die();
            $tmp_status_layanan = $mdl_monitor->getStatusLayanan();
            $tmp_maxstatus_location = $mdl_monitor->getMaxStatusLocation();
            // Zend_Debug::dump($tmp_maxstatus_location);die();

            $maxstatus_location = array();
            foreach ($tmp_maxstatus_location as $k => $v) {
                $maxstatus_location[$v['LOC_ID']]['MAX_STATUS'] = $v['MAX_STATUS'];
            }
            // Zend_Debug::dump($maxstatus_location);die();

            $status_layanan = array();
            foreach ($tmp_status_layanan as $k => $v) {
                $status_layanan[$v['LOC_ID']][$v['SERVICE_TYPE']] = $v;
            }
            // Zend_Debug::dump($status_layanan);die();

            $arr_masiv = array(
                "M" => "edia Center",
                "A" => "komodasi",
                "S" => "ecurity",
                "I" => "nternational Airport",
                "V" => "enue",
            );

            $arr_col = array(
                "M" => 3,
                "A" => 9,
                "S" => 3,
                "I" => 4,
                "V" => 5,
            );

            $data = array();
            foreach ($tmp_masiv as $k => $v) {
                $data[$v['FLAG']][$v['LOC_ID']] = $v;

                foreach ($status_layanan[$v['LOC_ID']] as $k2 => $v2) {
                    $data[$v['FLAG']][$v["LOC_ID"]][$k2] = $v2;
                }

                foreach ($maxstatus_location[$v['LOC_ID']] as $k3 => $v3) {
                    $data[$v['FLAG']][$v["LOC_ID"]][$k3] = $v3;
                }
            }
            // die();
            // Zend_Debug::dump($data);die();


            $arr_color_status = array(
                "3" => "bwhite_fred.png",
                "2" => "bwhite_fyellow.png",
                "1" => "bwhite_fgreen.png",
                "0" => "bwhite_fgrey.png",
            );



            echo '<div class="row">';
            foreach ($data as $k => $v) {
                echo '<div class="col-md-' . $arr_col[$k] . '">
			<div class="panel panel-default" style="margin-bottom:10px;">
				<div class="panel-heading">
					<h3 class="panel-title"><b style="color:red;">' . $k . '</b>' . $arr_masiv[$k] . '</h3>
				</div>
				<div class="panel-body square1-body" style="background-color:#404040;color:white;padding:10px 0px 0px 0px;text-align:center;vertical-align:middle;min-height:155px;">';
                foreach ($v as $k2 => $v2) {
                    // Zend_Debug::dump($v2);
                    $img = '/images/ICON/CIRCLE/circle_' . (count($v2['MAX_STATUS']) > 0 ? $arr_color_status[$v2['MAX_STATUS']] : 'bwhite_fgrey') . '';
                    // if($v2['ALIAS_NAME']=='SOETTA'){
                    // $img = '/images/ICON/EVENT/event_bwhite_fblue.png';
                    // }

                    echo '<div style="position:relative; border:0px solid yellow; width:65px; height:65px; text-align:center; display:inline-block; margin:2px;">';




                    echo'
					<!-- LOCATION EVENT -->					
					<span style="position:absolute; width:100%; border:0px solid green; left:0px;  top:0px;">
						<a class="csr" href="#' . $v2['LOC_ID'] . '"" data-tpanename="detailsurveillance" data-event_id="' . $v2['EVENT_ID'] . '" data-loc_id="' . $v2['LOC_ID'] . '" onclick="getTabPane(this);"><img src="' . $img . '" style="width:40px;"></a>
					</span>
					
					<!-- LAYANAN -->';

                    if (count($v2['WIFI']) > 0) {
                        echo '<span style="position:absolute; border:0px solid green; left:4px;  top:-2px;">
						<a class="csr" href="javascript:;" data-tpanename="detailsurveillance" data-event_id="' . $v2['EVENT_ID'] . '" data-loc_id="' . $v2['LOC_ID'] . '" data-service="wifi" onclick="getTabPane(this);"><img src="/images/ICON/ACCESS POINT/access_' . $arr_color_status[$v2['WIFI']['STATUS']] . '" style="width:18px;"></a>
					</span>';
                    }


                    if (count($v2['TELKOMSEL']) > 0) {
                        echo '<span style="position:absolute; border:0px solid green; right:4px;  top:-2px;">
						<a class="csr" href="javascript:;" data-tpanename="detailsurveillance" data-event_id="' . $v2['EVENT_ID'] . '" data-loc_id="' . $v2['LOC_ID'] . '" data-service="telkomsel" onclick="getTabPane(this);"><img src="/images/ICON/TELKOMSEL/tsel_' . $arr_color_status[$v2['TELKOMSEL']['STATUS']] . '" style="width:18px;"></a>
					</span>';
                    }


                    if (count($v2['DATIN']) > 0) {
                        echo '<span style="position:absolute; border:0px solid green; left:5px;  bottom:26px;">
						<a class="csr" href="javascript:;" data-tpanename="detailsurveillance" data-event_id="' . $v2['EVENT_ID'] . '" data-loc_id="' . $v2['LOC_ID'] . '" data-service="datin" onclick="getTabPane(this);"><img src="/images/ICON/SQUARE/square_' . $arr_color_status[$v2['DATIN']['STATUS']] . '" style="width:15px;"></a>
					</span>';
                    }

                    if (count($v2['CCTV']) > 0) {
                        echo '<span style="position:absolute; border:0px solid green; right:5px;  bottom:26px;">
						<a class="csr" href="javascript:;" data-tpanename="detailsurveillance" data-event_id="' . $v2['EVENT_ID'] . '" data-loc_id="' . $v2['LOC_ID'] . '" data-service="cctv" onclick="getTabPane(this);"><img src="/images/ICON/CCTV/cctv_' . $arr_color_status[$v2['CCTV']['STATUS']] . '" style="width:15px;"></a>
					</span>';
                    }


                    echo'
					<span style="position:absolute; width:100%; left:0px; top:42px; text-align:center; font-size:8px;">
						' . $v2["ALIAS_NAME"] . '
					</span>
				</div>';
                }

                echo '	</div>
			</div>
		</div>';

                if ($k == 'A') {
                    echo '</div>';
                    echo '<div class="row">';
                }
            }
            echo '</div>';


            $tmp_data = $mdl_monitor->getCountStatusLayananPerLocation();
            // Zend_Debug::dump($data);die();
            $data = array();
            foreach ($tmp_data as $k => $v) {
                $data[$v['LOC_ID']][$v['SERVICE_TYPE']] = $v;
            }
            // Zend_Debug::dump($data);die();

            $data_tables = array();
            foreach ($tmp_masiv as $k => $v) {
                $data_tables[$v['FLAG']][$v['LOC_ID']] = $v;
                foreach ($data[$v['LOC_ID']] as $k2 => $v2) {
                    $data_tables[$v['FLAG']][$v['LOC_ID']]['DATA'][$k2] = $v2;
                }
            }
            // Zend_Debug::dump($data_tables["M"]);//die();

            echo '<div class="row">';
            echo '	<div class="col-md-12">';
            echo '		<div class="portlet box grey" style="margin-bottom:10px;">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-table"></i>Summary Perangkat KAA
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse"></a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">';
            echo '					<div class="col-md-6">';
            foreach ($data_tables as $k => $v) {
                echo '						<div class="table-responsive">';
                echo '						<table class="table table-bordered table-hover" style="font-size:10px;">';
                echo '							<thead>
											<tr style="background-color:#404040; color:white;">';
                echo '									<th class="padd-tab" style="vertical-align:middle; font-size:10px;">' . strtoupper('<span style="color:red; font-weight:bold; font-size:16px;">' . $k . '</span> ' . $arr_masiv[$k]) . ' (' . count($v) . ')</th>';
                echo '									<th class="padd-tab" style="text-align:center; font-size:10px;"><img src="/images/ICON/ACCESS POINT/access_bwhite_fgrey.png" style="width:18px"> WIFI<br>Up|D|Un|T</th>
											<th class="padd-tab" style="text-align:center; font-size:10px;"><img src="/images/ICON/TELKOMSEL/tsel_bwhite_fgrey.png" style="width:18px"> TSEL<br>Up|D|Un|T</th>
											<th class="padd-tab" style="text-align:center; font-size:10px;"><img src="/images/ICON/SQUARE/square_bwhite_fgrey.png" style="width:16px"> DATIN<br>Up|D|Un|T</th>
											<th class="padd-tab" style="text-align:center; font-size:10px;"><img src="/images/ICON/CCTV/cctv_bwhite_fgrey.png" style="width:16px"> CCTV<br>Up|D|Un|T</th>
										</tr>
									</thead>
									<tbody>';


                $i = 1;
                foreach ($v as $k2 => $v2) {
                    // Zend_Debug::dump($v2);

                    $wifi_up = (int) $v2['DATA']['WIFI']['STATUS_UP'];
                    $wifi_down = (int) $v2['DATA']['WIFI']['STATUS_DOWN'];
                    $wifi_unknown = (int) $v2['DATA']['WIFI']['STATUS_UNKNOWN'];
                    $wifi_total = $wifi_up + $wifi_down + $wifi_unknown;
                    $col_wifi = '<span class="' . ($wifi_up > 0 ? 'badge badge-success' : 'badge badge-default') . '">' . $wifi_up . '</span> <span class="' . ($wifi_down > 0 ? 'badge badge-danger' : 'badge badge-default') . '">' . $wifi_down . '</span> <span class="badge badge-default">' . $wifi_unknown . '</span>  <span class="' . ($wifi_total > 0 ? 'badge badge-blacks' : 'badge badge-default') . '">' . $wifi_total . '</span>';

                    $tsel_up = (int) $v2['DATA']['TELKOMSEL']['STATUS_UP'];
                    $tsel_down = (int) $v2['DATA']['TELKOMSEL']['STATUS_DOWN'];
                    $tsel_unknown = (int) $v2['DATA']['TELKOMSEL']['STATUS_UNKNOWN'];
                    $tsel_total = $tsel_up + $tsel_down + $tsel_unknown;
                    $col_tsel = '<span class="' . ($tsel_up > 0 ? 'badge badge-success' : 'badge badge-default') . '">' . $tsel_up . '</span> <span class="' . ($tsel_down > 0 ? 'badge badge-danger' : 'badge badge-default') . '">' . $tsel_down . '</span> <span class="badge badge-default">' . $tsel_unknown . '</span> <span class="' . ($tsel_total > 0 ? 'badge badge-blacks' : 'badge badge-default') . '">' . $tsel_total . '</span>';

                    $astinet_up = (int) $v2['DATA']['DATIN']['STATUS_UP'];
                    $astinet_down = (int) $v2['DATA']['DATIN']['STATUS_DOWN'];
                    $astinet_unknown = (int) $v2['DATA']['DATIN']['STATUS_UNKNOWN'];
                    $astinet_total = $astinet_up + $astinet_down + $astinet_unknown;
                    $col_astinet = '<span class="' . ($astinet_up > 0 ? 'badge badge-success' : 'badge badge-default') . '">' . $astinet_up . '</span> <span class="' . ($astinet_down > 0 ? 'badge badge-danger' : 'badge badge-default') . '">' . $astinet_down . '</span> <span class="badge badge-default">' . $astinet_unknown . '</span> <span class="' . ($astinet_total > 0 ? 'badge badge-blacks' : 'badge badge-default') . '">' . $astinet_total . '</span>';

                    $cctv_up = (int) $v2['DATA']['CCTV']['STATUS_UP'];
                    $cctv_down = (int) $v2['DATA']['CCTV']['STATUS_DOWN'];
                    $cctv_unknown = (int) $v2['DATA']['CCTV']['STATUS_UNKNOWN'];
                    $cctv_total = $cctv_up + $cctv_down + $cctv_unknown;
                    $col_cctv = '<span class="' . ($cctv_up > 0 ? 'badge badge-success' : 'badge badge-default') . '">' . $cctv_up . '</span> <span class="' . ($cctv_down > 0 ? 'badge badge-danger' : 'badge badge-default') . '">' . $cctv_down . '</span> <span class="badge badge-default">' . $cctv_unknown . '</span> <span class="' . ($cctv_total > 0 ? 'badge badge-blacks' : 'badge badge-default') . '">' . $cctv_total . '</span>';

                    echo '							<tr style="cursor:pointer;" data-tpanename="detailsurveillance" data-event_id="' . $v2['EVENT_ID'] . '" data-loc_id="' . $v2['LOC_ID'] . '" onclick="getTabPane(this);">';
                    // echo '								<td class="padd-tab">'.$i.'. '.$v2['ALIAS_NAME'].'</td>';		
                    echo '								<td class="padd-tab">' . $v2['ALIAS_NAME'] . '</td>';
                    echo '								<td class="padd-tab text-center">' . $col_wifi . '</td>';
                    echo '								<td class="padd-tab text-center">' . $col_tsel . '</td>';
                    echo '								<td class="padd-tab text-center">' . $col_astinet . '</td>';
                    echo '								<td class="padd-tab text-center">' . $col_cctv . '</td>';
                    echo '							</tr>';

                    $i++;
                }
                echo '						</tbody>
							</table>
						</div>';
                if ($k == 'A') {
                    echo '</div>
		<div class="col-md-6">
			<div class="table-responsive">';
                }
            }
            echo '				</div>
					</div>
				</div>
			</div>';




            echo '</div>';
        }
//-=== DETAIL SURVEILLANCE
        else if ($params['tpanename'] == 'detailsurveillance') {

            $list_event = $mdl_monitor->getListEvent();
            $tmp_data = $mdl_monitor->getListLocEvent($params['event_id']);

            $arr_masiv = array(
                "M" => "edia Center",
                "A" => "komodasi",
                "S" => "ecurity",
                "I" => "nternational Airport",
                "V" => "enue",
            );

            $list_location = array();
            foreach ($tmp_data as $k => $v) {
                $list_location[$v['FLAG'] . $arr_masiv[$v['FLAG']]][] = $v;
            }

            $list_sublocation = $mdl_monitor->getSubLocation($params);

            $html = "";
            $html .='
<div class="row">
	<div class="col-md-12">
		<form class="form-horizontal" role="form" id="form">
			<input type="hidden" id="service" name="service" value="' . $params['service'] . '">
			<div class="form-body">
				<div class="row">
					<div class="col-md-4 hidden">
						<div class="form-group">
							<label class="col-md-3 control-label">Event:</label>
							<div class="col-md-9">
								<select class="form-control select2me" id="event_id" name="event_id" onchange="changeLocID();">';
            foreach ($list_event as $k => $v) {
                $html .='<option value="' . $v['VALUE'] . '" ' . ($params['event_id'] ? 'selected' : '') . '>' . $v['DISPLAY'] . '</option>';
            }
            $html .='						</select>
								<span class="help-block"></span>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="col-md-3 control-label">Location:</label>
							<div class="col-md-9">
								<select class="form-control select2me" id="loc_id" name="loc_id" placeholder="Select Location..." onchange="changeSubLocID();">
									<option value="">Select Location...</option>';
            foreach ($list_location as $k => $v) {
                $html .='<optgroup label="' . strtoupper($k) . '">';
                foreach ($v as $k2 => $v2) {
                    $html .='<option value="' . $v2['VALUE'] . '" ' . ($params['loc_id'] == $v2['VALUE'] ? 'selected' : '') . '>' . $v2['DISPLAY'] . '</option>';
                }
                $html .='</optgroup>';
            }
            $html .='						</select>
								<span class="help-block"></span>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<label class="col-md-4 control-label">Sub Location:</label>
							<div class="col-md-8">
								<select class="form-control select2me" id="sub_loc_id" name="sub_loc_id" placeholder="Select Sub Location...">
									<option value="">All Sub Location</option>';
            foreach ($list_sublocation as $k => $v) {
                $html .='<option value="' . $v['SUB_LOC_ID'] . '" ' . ($params['sub_loc_id'] == $v['SUB_LOC_ID'] ? 'selected' : '') . '>' . $v['SUB_LOC_NAME'] . '</option>';
            }
            $html .='						</select>
								<span class="help-block"></span>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<button type="button" class="btn blue" id="submit" onclick="detailContent($(\'#event_id\').val(),$(\'#loc_id\').val(),$(\'#sub_loc_id\').val());">Submit</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<hr>
';



            $tmp_sublocation = $mdl_monitor->getSubLocation2($params);
// Zend_Debug::dump($tmp_sublocation);die();

            $tmp_data_perangkat = $mdl_monitor->getListDeviceByService($params);
// Zend_Debug::dump($tmp_data_perangkat);die();


            $data_perangkat = array();
            foreach ($tmp_data_perangkat as $k => $v) {
                $data_perangkat[$v['SUB_LOC_NAME']][$v['SERVICE_TYPE']][] = $v;
            }


            $tmp_datatables = $mdl_monitor->getSummaryBySubLocation($params);
// Zend_Debug::dump($tmp_datatables);die();

            $datatables = array();
            foreach ($tmp_datatables as $k => $v) {
                $datatables[$v['SUB_LOC_NAME']][] = $v;
            }

// Zend_Debug::dump($datatables);die();

            $data = array();
            foreach ($tmp_sublocation as $k => $v) {
                // Zend_Debug::dump(count($data_perangkat[$v['SUB_LOC_NAME']]));
                // $data_perangkat = array();
                if ($params['service'] != '') {
                    $data[$v['SUB_LOC_NAME']][strtoupper($params['service'])] = array();
                } else {
                    // $data[$v['SUB_LOC_NAME']]['WIFI'] = array();
                    // $data[$v['SUB_LOC_NAME']]['DATIN'] = array();
                    // $data[$v['SUB_LOC_NAME']]['CCTV'] = array();
                    // $data[$v['SUB_LOC_NAME']]['TELKOMSEL'] = array();
                }

                if (count($data_perangkat[$v['SUB_LOC_NAME']]) > 0) {
                    foreach ($data_perangkat[$v['SUB_LOC_NAME']] as $k2 => $v2) {
                        // Zend_Debug::dump($k2);
                        $data[$v['SUB_LOC_NAME']][$k2] = $v2;
                    }

                    foreach ($datatables[$v['SUB_LOC_NAME']] as $k3 => $v3) {
                        $data[$v['SUB_LOC_NAME']]['DATATABLES'][] = $v3;
                    }
                }
            }


            $tmp_datatables = $mdl_monitor->getSummaryBySubLocation($params);
// Zend_Debug::dump($tmp_datatables);die();
// foreach($tmp_datatables as $k=>$v){
            // $data_perangkat[$v['SUB_LOC_NAME']]['DATATABLES'][] = $v;
// }
// die();
// Zend_Debug::dump($data);die();


            $arr_status = array(
                "5" => "UP",
                "1" => "DOWN",
                "0" => "UNKNOWN",
            );

            $arr_color_status = array(
                "5" => "bwhite_fgreen",
                "4" => "bwhite_fgrey",
                "3" => "bwhite_fgreen",
                "1" => "bwhite_fred",
                "0" => "bwhite_fgrey",
                "" => "bwhite_fgrey",
            );

            $arr_location_icon = array(
                "WIFI" => "ACCESS POINT/access",
                "DATIN" => "SQUARE/square",
                "TELKOMSEL" => "TELKOMSEL/tsel",
                "CCTV" => "CCTV/cctv",
            );


            $html .='<div id="detail_content">';
            foreach ($data as $k => $v) {
// Zend_Debug::dump($v);die();
                $html .='<div class="row">';
                $html .='	<div class="col-md-offset-4 col-md-4">';
                $html .='		<div class="row corner-all text-center" style="background-color:#404040;color:white;padding:5px;text-align:center;vertical-align:middle;margin-bottom:10px;">';
// $html .='		<span class="bold" style="font-size:16px;">SUB LOCATION: </span>';
                $html .='			<span class="bold gold" style="font-size:16px;">' . $k . '</span>';
                $html .='		</div>';
                $html .='	</div>';
                $html .='</div>';


                $html .='<div class="row">';
                $html .='	<div class="col-md-offset-4 col-md-4">';
                $html .='		<table class="table table-bordered table-striped">
				<thead>
				<tr>
					<th>LAYANAN</th>
					<th style="text-align:center;">UP|D|UN</th>
					<th style="text-align:center;">USER ONLINE</th>
				</tr>
				</thead>
				<tbody>';
                foreach ($v as $k2 => $v2) {
                    // Zend_Debug::dump($v2);die();
                    if (strtoupper($k2) == 'DATATABLES') {
                        foreach ($v2 as $kk2 => $vv2) {
                            // Zend_Debug::dump($vv2);die();
                            $html .='
			<tr>
				<td>' . $vv2['SERVICE_TYPE'] . '</td>
				<td style="text-align:center;"><span class="badge badge-success">' . $vv2['STATUS_UP'] . '</span> <span class="badge badge-danger">' . $vv2['STATUS_DOWN'] . '</span> <span class="badge badge-default">' . $vv2['STATUS_UNKNOWN'] . '</span></td>
				<td style="text-align:center;">' . ($vv2['SERVICE_TYPE'] != 'WIFI' ? '-' : '<span class="badge badge-primary">' . (int) $vv2['CLIENTCOUNT']) . '</span></td>
			</tr>
			';
                        }
                    }
                }

                $html .='			</tbody>
				</table>';
                $html .='	</div>';
                $html .='</div>';

                foreach ($v as $k2 => $v2) {
                    // Zend_Debug::dump($v2);die();
                    if (strtoupper($k2) != 'DATATABLES') {

                        $html .='<div class="row text-center">';
                        $html .='	<div class="col-md-offset-4 col-md-4" style="padding:15px;">';
                        $html .='		<span class="bold" style="font-size:16px;">' . strtoupper($k2) . '</span>';
                        $html .='	</div>';
                        $html .='</div>';


                        $html .='<div class="row text-center">';

                        if (count($v2) > 0) {


                            $server = array('10.62.8.126', '10.62.8.132', '10.62.8.135', '10.62.8.136');

                            foreach ($v2 as $k3 => $v3) {
                                // Zend_Debug::dump($v3);die();

                                $key = md5('telkomcare');
                                $string = $v3['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $v3['RRD_NAME']);
                                //echo $string.'<br>';
                                //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
                                $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
                                //echo $encrypted."<br>";
                                $encrypted2 = base64_encode($encrypted);



                                $html .='	<div class="" style="display:inline-block; vertical-align:top; width:150px;font-size:10px;text-align:center">';

                                $html .='		<a style="display:inline-block; margin:5px 5px;text-decoration:none;text-align:center;" href="' . ($v3['SERVICE_TYPE'] == 'DATIN' || $v3['SERVICE_TYPE'] == 'CCTV' ? '' : '#modal_perangkat') . '" data-toggle="modal" data-event_id="' . $v3['EVENT_ID'] . '" data-loc_id="' . $v3['LOC_ID'] . '" data-sid="' . $v3['SERVICE_NAME'] . '" data-sub_loc_id="' . $v3['SUB_LOC_ID'] . '" data-link="' . $v3['LINK_ID'] . '" data-event_id="' . $v3['EVENT_ID'] . '" data-service_type="' . $v3['SERVICE_TYPE'] . '" data-id_perangkat="' . $v3['ID_PERANGKAT'] . '" data-nama_perangkat="' . $v3['NAMA_PERANGKAT'] . '"  data-ethernetmac="' . $v3['ETHERNETMAC'] . '" data-id="' . ($v3['SERVICE_TYPE'] != 'TELKOMSEL' ? $encrypted2 : $v3['DEVICE_ID']) . '" data-title="' . $v3['SERVICE_ID'] . '" data-cust="' . $v3['SERVICE_NAME'] . '" class="square1' . ($v3['SERVICE_TYPE'] == 'DATIN' || $v3['SERVICE_TYPE'] == 'CCTV' ? ' btn-graph' : '') . '" title="' . $v3['NAMA_PERANGKAT'] . '" ' . ($v3['SERVICE_TYPE'] == 'DATIN' || $v3['SERVICE_TYPE'] == 'CCTV' ? '' : 'onclick="getModalPerangkat(this);"') . ' >';
                                if ($v3['SERVICE_TYPE'] == 'WIFI') {
                                    $html .= '<span style="font-weight:bold; font-size:9px;">UserOnline: ' . $v3['CLIENTCOUNT'] . '</span><br>';
                                }
                                $html .='			<img src="/images/ICON/' . $arr_location_icon[$k2] . '_' . $arr_color_status[$v3['STATUS']] . '.png" style="width:40px;height:40px;"><br>';
                                $html .='			<span class="bold greys">' . $v3['NAMA_PERANGKAT'] . '</span><br>';
                                $html .='			<span class="bold blues" style="margin-left:0px">' . $arr_status[$v3['STATUS']] . '</span>';
                                $html .='		</a>';
                                $html .='	</div>';
                            }
                        }
                        // else {
// $html .='	<div class="" style="display:block; width:100%;font-size:10px;font-weight:bold;text-align:center">';
// $html .='		this Location doesn\'t have '.$k2.' data.';
// $html .='	</div>';
                        // }

                        $html .='</div>';
                        if ($params['service'] == '') {
                            $html .='<hr>';
                        }
                    }
                }


                $html .='</div>';
            }
            $html .='</div>';

            $html .="
<script>
function detailContent(event_id,loc_id,sub_loc_id){
	
	// console.log(me);
	
	$('#tab_content').html('');
	$('#loader_tab_content').show();
	
	
	$.get(
		'/ajaxmonitor/gettabpane',
		{tpanename:'detailsurveillance',event_id:event_id,loc_id:loc_id,sub_loc_id:sub_loc_id},
		function(data){
			$('#tab_content').html(data);
			$('#loader_tab_content').hide();
		},
		'html');
}
</script>";

            echo $html;
        }
//-=== INFORMATION
        else if ($params['tpanename'] == 'information') {

            $arr_acc = array(
                "0" => "Agenda Kegiatan KAA ke 60",
                "1" => "Materi Konferensi Asia Afrika 2015",
                "2" => "Layanan Masiv KAA 2015",
            );

            $arr_dir = array(
                "0" => "/images/Agenda Kegiatan",
                "1" => "/images/Agenda Kegiatan",
                // "1"=>"/images/KAA2015",
                "2" => "/images/LAYANANMASIVKAA2015",
            );

            echo '<div class="panel-group accordion" id="accordion_information">';
            for ($i = 0; $i < 3; $i++) {
                echo '<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a class="accordion-toggle' . ($i > 0 ? ' collapsed' : '') . '" data-toggle="collapse" data-parent="#accordion_information" href="#collapse_' . $i . '">
					' . $arr_acc[$i] . ' </a>
					</h4>
				</div>
				<div id="collapse_' . $i . '" class="panel-collapse' . ($i > 0 ? ' collapsed' : ' in') . '" style="height: ' . ($i > 0 ? '0px' : 'auto') . ';">
					<div class="panel-body">
						
					<div class="row">
						<div class="col-md-12">
							<div class="block-carousel">
								<div id="promo_carousel' . $i . '" class="carousel slide">
									<div class="container">
										<div class="carousel-inner">';

                if ($i == 0) {
                    for ($ii = 7; $ii < 9; $ii++) {
                        echo '<div class="' . ($ii == 7 ? 'active ' : '') . 'item text-center" style="margin-bottom:0px;">';
                        echo '	<img src="' . $arr_dir[$i] . '/Slide' . $ii . '.PNG" alt="" class="img-responsive" style="display:inline-block !important;width:100%;">';
                        echo '</div>';
                    }
                } else if ($i == 1) {
                    for ($ii = 1; $ii < 31; $ii++) {
                        echo '<div class="' . ($ii == 1 ? 'active ' : '') . 'item text-center" style="margin-bottom:0px;">';
                        echo '	<img src="' . $arr_dir[$i] . '/Slide' . $ii . '.PNG" alt="" class="img-responsive" style="display:inline-block !important;width:100%;">';
                        echo '</div>';
                    }
                } else if ($i == 2) {
                    for ($ii = 1; $ii < 19; $ii++) {
                        echo '<div class="' . ($ii == 1 ? 'active ' : '') . 'item text-center" style="margin-bottom:0px;">';
                        echo '	<img src="' . $arr_dir[$i] . '/Slide' . $ii . '.PNG" alt="" class="img-responsive" style="display:inline-block !important;width:100%;">';
                        echo '</div>';
                    }
                }

                echo '
										</div>
										<a class="carousel-control left" href="#promo_carousel' . $i . '" data-slide="prev">
										<i class="m-icon-big-swapleft" style="position:absolute;top:40%;left:0px;"></i>
										</a>
										<a class="carousel-control right" href="#promo_carousel' . $i . '" data-slide="next">
										<i class="m-icon-big-swapright" style="position:absolute;top:40%;right:0px;"></i>
										</a>
									</div>
								</div>
							</div>

						</div>
					</div>
					</div>
				</div>
			</div>';
            }
            echo'	
		</div>';
        }
//-=== COLLABORATION
        else if ($params['tpanename'] == 'collaboration') {

            // Zend_Debug::dump($identity->uname);die();

            $data_chats = $mdl_monitor->getCollaborationEvent();
            $data_comment = $mdl_monitor->count_comment();
            // if($identity->uname="him"){
            // Zend_Debug::dump($data_chats);die();

            $comment = array();
            foreach ($data_comment as $k => $v) {
                $comment[$v['PARENT_LEVEL']]['JML_COMMENT'] = $v['JML_COMMENT'];
            }
            // Zend_Debug::dump($comment);die();
            // }
            // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-slimscroll/jquery.slimscroll.min.js');
            // $this->view->headScript()->appendFile('/assets/core/scripts/index.js');
            echo '
<div class="row">
	<div class="col-md-12">		
			<!-- BEGIN PORTLET-->
			<div class="portlet">
				<div class="portlet-title line">
					<div class="caption">
						<i class="fa fa-comments"></i>Chats
					</div>
					<div class="tools">
						<a href="" class="reload"></a>
						<a href="" class="collapse"></a>
					</div>
				</div>
				<div class="portlet-body" id="chats">
					<form id="form_chat" enctype="multipart/form-data" method="post" role="form">
						<div class="row">
							<div class="col-md-12">
								<div class="chat-form" style="margin-top: 0px;">
									<div class="input-cont" style="margin-right:35px">
										<textarea id="text_message" name="text_message" class="form-control require" type="text" placeholder="Type a message here..." style="width:100%;height:75px;min-width:100%;min-height:75px;max-width:100%;max-height:75px"></textarea>
									</div>
									<div class="btn-cont" style="margin-top:-82px">
										<span class="arrow" style="margin-right:10px;">
										</span>
										<a href="#" class="btn blue icn-only" style="height:74px; margin-left:-10px;" onclick="insertChats()"><i class="fa fa-check icon-white" style="margin-top:25px"></i></a>
									</div>
									<span class="help-block" style="margin-bottom: 0px; display: none;">
										<span class="label label-danger" id="walllabel"><i class="fa fa-warning"> </i>This is inline help</span>
									</span>
									
									<!-- file upload -->
									
									<div class="fileupload fileupload-new" data-provides="fileupload" style="display:inline">
										<span class="btn default btn-file" style="margin-top: 10px;">
											<span class="fileupload-new">
												<i class="fa fa-paperclip"></i> 
											</span>
											<span class="fileupload-exists">
												<i class="fa fa-undo"></i> Change
											</span>
											<input id="wallimg" name="wallimg" type="file" class="default">
										</span>
										<span class="fileupload-preview" style="margin-left:5px;"></span>
										<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>							
									</div>		
									
								</div>
							</div>
						</div>
						<!--div class="row">
							<div class="col-md-2">
								<button type="button" class="btn red" onclick="test();">Test</button>
							</div>
						</div-->
					</form>
					<div id="content_chats" class="scrollers" style="min-height: 300px; overflow:auto; margin-top:10px;" data-always-visible="1" data-rail-visible1="1">
						<ul class="chats">';
            foreach ($data_chats as $k => $v) {
                echo'<li class="' . ($identity->fullname == $v['NIK'] ? 'out' : 'in') . '">
								<img class="avatar img-responsive" alt="" src="/assets/core/skins/default/img/avatar.png"/>
								<div class="message">
									<span class="arrow">
									</span>
									' . ($identity->fullname == $v['NIK'] ? '<button class="btn red btn-xs" data-id="' . $v['ID'] . '" style="font-size:9px;" onclick="deleteChat(this);">Delete</button>' : '') . '
									<a href="#" class="name">' . $v['NIK'] . ' / ' . $v['NAMA'] . '</a>
									<br>
									<span class="datetime" style="color:grey; font-size:10px;">
										' . date('D, d F Y G:ia', strtotime($v['DATETIME'])) . '
									</span>
									<span class="body">
											<pre style="border:0px; background-color:transparent; font-family:\'Open Sans\', sans-serif; padding:0px;">' . $v['MESSAGE'] . '</pre>';
                if ($v['FILENAME']) {
                    echo '
												<!--a class="mix-preview fancybox-button" href="/uploads/kaa2015/' . $v['FILENAME'] . '" data-rel="fancybox-button"-->
													<img class="img-responsive" alt="" src="/uploads/kaa2015/' . $v['FILENAME'] . '" style="display:inline-block;">
												<!--/a-->';
                }
                echo '
									</span>';

                // if($identity->uname=='him'){
                echo '
										<a href="" class="comments" id="comment' . $v['ID'] . '" data-toggle="modal" data-parent_level="' . $v['ID'] . '"-->
											Comment (' . (int) $comment[$v['ID']]['JML_COMMENT'] . ')
										</a>';
                // }
                echo '</div>
							</li>';
            }
            echo'
						</ul>
					</div>
				</div>
			</div>
			<!-- END PORTLET-->
	</div>
</div>


<script>
// IndexKAA.initChat();
var el2 = jQuery("#chats").first();

var files = {};
// var files = jQuery("#wallimg");

$(".reload").on("click", reloadChat);

$("input[type=file]").on("change", prepareUpload);

function prepareUpload(event)
{
  files = event.target.files;
event.stopPropagation(); // Stop stuff happening
event.preventDefault(); // Totally stop stuff happening
}



function insertChats(){
	
	App.blockUI(el2);
	
	$("input[type=file]").on("change", prepareUpload);
	var dataform = $("#form_chat").serializeArray()
	// console.log(dataform[0].value);
	
	var formData = new FormData();
	formData.append("text_message", dataform[0].value);
	formData.append("action", "previewImg");
	// Main magic with files here
	formData.append("image", $("input[type=file]")[0].files[0]); 
	
	// console.log(formData);
	$.ajax({
        url: "/ajaxmonitor/insertchat",
        type: "POST",
        data: formData,
        cache: false,
        dataType: "json",
        processData: false, // Don"t process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
		success: function(data){
			App.unblockUI(el2);
			 xhr = $.get(
				"/ajaxmonitor/gettabpane",
				{tpanename:"collaboration"},
				function(data){
					$("#tab_content").html(data);
					// $("#loader_tab_content").hide();
				},
			"html");
		},
    });
}

function reloadChat(){
	App.unblockUI(el2);
	 xhr = $.get(
		"/ajaxmonitor/gettabpane",
		{tpanename:"collaboration"},
		function(data){
			$("#tab_content").html(data);
			// $("#loader_tab_content").hide();
		},
	"html");
}


function deleteChat(me){
	
	if (xhr_chats != undefined && xhr_chats != null){
		xhr_chats.abort();
	}
	
	
	App.blockUI(el2);
	// console.log($("#text_message").val());
	
	 xhr_chats = $.get(
		"/ajaxmonitor/deletechat",
		{id:$(me).data("id")},
		function(data){
			App.unblockUI(el2);
			 xhr = $.get(
				"/ajaxmonitor/gettabpane",
				{tpanename:"collaboration"},
				function(data){
					$("#tab_content").html(data);
					// $("#loader_tab_content").hide();
				},
			"html");
			// $("#tab_content").html(data);
			// $("#loader_tab_content").hide();
		},
		"html");
	
}

</script>
';
        }
//-=== TOPOLOGI MEDIA CENTER
        else if ($params['tpanename'] == 'topologimc') {
            echo '<div style="width:1020px; height:745px; position:relative; border:0px solid yellow;">';
            echo '	<span style="position:absolute;	top:0px; left:0px;">';
            echo '		<img src="/images/TOPOLOGI_MEDIA_CENTER.png" style="width:1020px; height:745px; opacity:0.2;">';
            echo '	</span>';

            echo '	<span style="position:absolute;	top:280px; left:174px;">';
            echo '		<img src="/images/ICON/CIRCLE/circle_bgrey_fred.png" style="width:20px;opacity:;">';
            echo '	</span>';
            echo '	<span style="position:absolute;	top:380px; left:274px;">';
            echo '		<img src="/images/ICON/CIRCLE/circle_bgrey_fred.png" style="width:20px;opacity:;">';
            echo '	</span>';
            echo '</div>';
        }
//-=== WORKFOCE MGT
        else if ($params['tpanename'] == 'workforcemgt') {

            $arr_acc = array(
                "0" => "Jadwal Posko",
            );


            echo '<div class="panel-group accordion" id="accordion_workforcemgt">';
            for ($i = 0; $i < 1; $i++) {
                echo '<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a class="accordion-toggle' . ($i > 0 ? ' collapsed' : '') . '" data-toggle="collapse" data-parent="#accordion_workforcemgt" href="#collapse_' . $i . '">
					' . $arr_acc[$i] . ' </a>
					</h4>
				</div>
				<div id="collapse_' . $i . '" class="panel-collapse' . ($i > 0 ? ' collapsed' : ' in') . '" style="height: ' . ($i > 0 ? '0px' : 'auto') . ';">
					<div class="panel-body">';

                echo '<div class="row">';
                echo '	<div class="col-md-12">';
                echo '		<form class="form-horizontal" role="form" id="form_jadwal">
										<div class="form-body">
											<div class="form-group">
												<!--label class="col-md-3 control-label">Text</label-->
												<div class="col-md-4">
													<input type="text" class="form-control" placeholder="NIK / NAME" id="nikname" name="nikname">
													<span class="help-block"></span>
												</div>
												<div class="col-md-2">
													<button class="btn blue" type="button" onclick="filterJadwal();" id="btn_filter_jadwalposko">Filter</button>
												</div>
											</div>
										</div>
									</form>';
                echo '	</div>';
                echo '</div>';

                echo '<div class="row">';
                echo '	<div class="col-md-12">';
                echo '		<div id="content_jadwal"></div>';
                echo '	</div>';
                echo '</div>';



                // if($i==0){
                // echo '<div class="panel-group accordion" id="accordion_workforcemgt">';
                // echo '<div class="row" >
                // <div class="col-md-12" id="content">
                // <embed width="100%" height="370px" name="plugin" src="/uploads/Jadwal_Posko1.pdf" type="application/pdf">
                // </div>
                // </div>';
                // echo '</div>';
                // }


                echo'
					</div>
					</div>
				</div>
			</div>';
            }
            echo'	
		</div>';

            // echo '<div class="panel-group accordion" id="accordion_information">';
            // echo '<div class="row" >
            // <div class="col-md-12" id="content">
            // <embed width="100%" height="470px" name="plugin" src="/uploads/Jadwal_Posko.pdf" type="application/pdf">
            // </div>
            // </div>';
            // echo '</div>';
        }
//-=== LINK MONITORING
        else if ($params['tpanename'] == 'linkmonitoring') {


            //-================================== LINK MONITORING MEDIA CENTER
            echo '<div class="row">';
            echo '	<div class="col-md-12">';
            echo '		<div class="portlet box grey" style="margin-bottom:10px;">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-reorder"></i>LINK MONITORING MEDIA CENTER
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse"></a>
						</div>
					</div>
					<div class="portlet-body" id="linkmediacenter">
						<div class="row">';

            $tmpdatax = $mdl_monitor->getStatusLinkMediaCenter();
// Zend_Debug::dump($tmpdatax[0]);//die();

            $datax = array();
            foreach ($tmpdatax as $k => $v) {
                $datax[$v['IMAGE']][$v['COL']][$v['SEQ_ID']]['STATUS'] = $v['STATUS'];
                $datax[$v['IMAGE']][$v['COL']][$v['SEQ_ID']]['GRAPH_ID'] = $v['GRAPH_ID'];
                $datax[$v['IMAGE']][$v['COL']][$v['SEQ_ID']]['RRD_NAME'] = $v['RRD_NAME'];
                $datax[$v['IMAGE']][$v['COL']][$v['SEQ_ID']]['GRAPH_TITLE'] = $v['SERVICE_NAME'];
                $datax[$v['IMAGE']][$v['COL']][$v['SEQ_ID']]['LINK'] = $v['LINK_ID'];
                $datax[$v['IMAGE']][$v['COL']][$v['SEQ_ID']]['SID'] = $v['SERVICE_ID'];
                $datax[$v['IMAGE']][$v['COL']][$v['SEQ_ID']]['TRAFFIC'] = $v['CURRENT_TRAFFIC'];
            }
// Zend_Debug::dump($datax);die();

            $arr_status_mc = array(
                "" => "bgrey_fwhite",
                "0" => "bgrey_fwhite",
                "2" => "bgrey_fred",
                "3" => "bgrey_fgreen",
                "4" => "bgrey_fwhite",
            );

            $server = array('10.62.8.126', '10.62.8.132', '10.62.8.135', '10.62.8.136');
            $key = md5('telkomcare');

            for ($i = 1; $i <= 5; $i++) {



                echo '					<div class="' . ($i == 5 ? 'col-md-offset-3 ' : '') . 'col-md-6" style="postion:relative; border:0px solid yellow; padding:5px;">';
                echo '						
								<img src="/images/LINK_ASTINET_MEDIA_CENTER/' . $i . '.png" alt="" class="img-responsive">
								
								<!-- 11 -->';
                $string = $datax[$i][1][1]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[$i][1][1]['RRD_NAME']);
                //echo $string.'<br>';
                //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
                $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
                //echo $encrypted."<br>";
                $encrypted2 = base64_encode($encrypted);
                echo '
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[$i][1][1]['GRAPH_TITLE'] . '" data-sid="' . $datax[$i][1][1]['SID'] . '" data-cust="' . $datax[$i][1][1]['GRAPH_TITLE'] . '" data-link="' . $datax[$i][1][1]['LINK'] . '" style="position:absolute; top:20%; left:32%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_mc[$datax[$i][1][1]['STATUS']] . '.png" style="max-width:20px;">';
                echo '</a>';


                // if($identity->uname="him"){
                // echo '<span style="position:absolute; top:17%; left:39%; font-weight:bold;">';
                // $traffic = 0;
                // if($datax[$i][1][1]['TRAFFIC']<1024){
                // $traffic = round($datax[$i][1][1]['TRAFFIC'],2)." Byte";
                // }
                // else if($datax[$i][1][1]['TRAFFIC']>1024){
                // $traffic = round(($datax[$i][1][1]['TRAFFIC']/1024),2)." KB";
                // }
                // else if($datax[$i][1][1]['TRAFFIC']>1024*1024){
                // $traffic = round(($datax[$i][1][1]['TRAFFIC']/1024/1024),2)." KB";
                // }
                // echo $traffic;
                // echo '</span>';
                // }
                // if($i==1){
                // echo '	<!-- 12 -->';
                // $string = $datax[$i][1][2]['GRAPH_ID'].'|0|'.((isset($server[0]))?$server[0]:"UNKNOWN").'|'.str_replace("<path_rra>","",$datax[$i][1][2]['RRD_NAME']);
                // //echo $string.'<br>';
                // //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
                // $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
                // //echo $encrypted."<br>";
                // $encrypted2 = base64_encode($encrypted);
                // echo '
                // <a class="btn-graph" data-toggle="modal" data-id="'.$encrypted2.'" data-title="'.$datax[$i][1][2]['GRAPH_TITLE'].'" data-sid="'.$datax[$i][1][2]['SID'].'" data-cust="'.$datax[$i][1][2]['GRAPH_TITLE'].'" data-link="'.$datax[$i][1][2]['LINK'].'" style="position:absolute; top:37%; left:32%; cursor:pointer;">
                // <img class="img-responsive" src="/images/ICON/CIRCLE/circle_'.$arr_status_mc[$datax[$i][1][2]['STATUS']].'.png" style="max-width:20px;">
                // </a>';
                // }


                echo'	<!-- 13 -->';
                $string = $datax[$i][1][3]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[$i][1][3]['RRD_NAME']);
                //echo $string.'<br>';
                //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
                $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
                //echo $encrypted."<br>";
                $encrypted2 = base64_encode($encrypted);
                echo '
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[$i][1][3]['GRAPH_TITLE'] . '" data-sid="' . $datax[$i][1][3]['SID'] . '" data-cust="' . $datax[$i][1][3]['GRAPH_TITLE'] . '" data-link="' . $datax[$i][1][3]['LINK'] . '" style="position:absolute; top:' . ($i == 5 ? 50 : 64) . '%; left:32%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_mc[$datax[$i][1][3]['STATUS']] . '.png" style="max-width:20px;">
								</a>';

                // echo'		<!-- 14 -->';
                // $string = $datax[$i][1][4]['GRAPH_ID'].'|0|'.((isset($server[0]))?$server[0]:"UNKNOWN").'|'.str_replace("<path_rra>","",$datax[$i][1][4]['RRD_NAME']);
                // //echo $string.'<br>';
                // //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
                // $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
                // //echo $encrypted."<br>";
                // $encrypted2 = base64_encode($encrypted);
                // echo '
                // <a class="btn-graph" data-toggle="modal" data-id="'.$encrypted2.'" data-title="'.$datax[$i][1][4]['GRAPH_TITLE'].'" data-sid="'.$datax[$i][1][4]['SID'].'" data-cust="'.$datax[$i][1][4]['GRAPH_TITLE'].'" data-link="'.$datax[$i][1][4]['LINK'].'" style="position:absolute; top:86%; left:32%; cursor:pointer;">
                // <img class="img-responsive" src="/images/ICON/CIRCLE/circle_'.$arr_status_mc[$datax[$i][1][4]['STATUS']].'.png" style="max-width:20px;">
                // </a>';
                // echo'	<!-- 24 -->';
                // $string = $datax[$i][2][4]['GRAPH_ID'].'|0|'.((isset($server[0]))?$server[0]:"UNKNOWN").'|'.str_replace("<path_rra>","",$datax[$i][2][4]['RRD_NAME']);
                // //echo $string.'<br>';
                // //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
                // $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
                // //echo $encrypted."<br>";
                // $encrypted2 = base64_encode($encrypted);
                // echo '
                // <a class="btn-graph" data-toggle="modal" data-id="'.$encrypted2.'" data-title="'.$datax[$i][2][4]['GRAPH_TITLE'].'" data-sid="'.$datax[$i][2][4]['SID'].'" data-cust="'.$datax[$i][2][4]['GRAPH_TITLE'].'" data-link="'.$datax[$i][2][4]['LINK'].'" style="position:absolute; top:86%; left:62%; cursor:pointer;">
                // <img class="img-responsive" src="/images/ICON/CIRCLE/circle_'.$arr_status_mc[$datax[$i][2][4]['STATUS']].'.png" style="max-width:20px;">
                // </a>';

                echo '	<!-- 23 -->';
                $string = $datax[$i][2][3]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[$i][2][3]['RRD_NAME']);
                //echo $string.'<br>';
                //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
                $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
                //echo $encrypted."<br>";
                $encrypted2 = base64_encode($encrypted);
                echo '
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[$i][2][3]['GRAPH_TITLE'] . '" data-sid="' . $datax[$i][2][3]['SID'] . '" data-cust="' . $datax[$i][2][3]['GRAPH_TITLE'] . '" data-link="' . $datax[$i][2][3]['LINK'] . '" style="position:absolute; top:63%; left:62%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_mc[$datax[$i][2][3]['STATUS']] . '.png" style="max-width:20px;">
								</a>';

                // echo'	<!-- 22 -->';
                // $string = $datax[$i][2][2]['GRAPH_ID'].'|0|'.((isset($server[0]))?$server[0]:"UNKNOWN").'|'.str_replace("<path_rra>","",$datax[$i][2][2]['RRD_NAME']);
                // //echo $string.'<br>';
                // //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
                // $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
                // //echo $encrypted."<br>";
                // $encrypted2 = base64_encode($encrypted);
                // echo '
                // <a class="btn-graph" data-toggle="modal" data-id="'.$encrypted2.'" data-title="'.$datax[$i][2][2]['GRAPH_TITLE'].'" data-sid="'.$datax[$i][2][2]['SID'].'" data-cust="'.$datax[$i][2][2]['GRAPH_TITLE'].'" data-link="'.$datax[$i][2][2]['LINK'].'" style="position:absolute; top:37%; left:62%; cursor:pointer;">
                // <img class="img-responsive" src="/images/ICON/CIRCLE/circle_'.$arr_status_mc[$datax[$i][2][2]['STATUS']].'.png" style="max-width:20px;">
                // </a>';

                echo'	<!-- 21 -->';
                $string = $datax[$i][2][1]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[$i][2][1]['RRD_NAME']);
                //echo $string.'<br>';
                //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
                $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
                //echo $encrypted."<br>";
                $encrypted2 = base64_encode($encrypted);
                echo '
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[$i][2][1]['GRAPH_TITLE'] . '" data-sid="' . $datax[$i][2][1]['SID'] . '" data-cust="' . $datax[$i][2][1]['GRAPH_TITLE'] . '" data-link="' . $datax[$i][2][1]['LINK'] . '" style="position:absolute; top:20%; left:62%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_mc[$datax[$i][2][1]['STATUS']] . '.png" style="max-width:20px;">
								</a>
		';


                // if($identity->uname="him"){
                // echo '<span style="position:absolute; top:25%; left:50%; font-weight:bold;">';
                // $traffic = 0;
                // if($datax[$i][2][1]['TRAFFIC']<1024){
                // $traffic = round(($datax[$i][2][1]['TRAFFIC']/1024),2)." Byte";
                // }
                // else if($datax[$i][2][1]['TRAFFIC']>1024){
                // $traffic = round(($datax[$i][2][1]['TRAFFIC']/1024),2)." KB";
                // }
                // else if($datax[$i][2][1]['TRAFFIC']>1024*1024){
                // $traffic = round(($datax[$i][2][1]['TRAFFIC']/1024/1024),2)." KB";
                // }
                // echo $traffic;
                // echo '</span>';
                // }
                echo '					</div>';

                if ($i == 4) {
                    echo '</div>
		<div class="row">';
                }
            }

            echo '				</div>
					</div>
				</div>';
            echo '	</div>';
            echo '</div>';


            $arr_status_cctv = array(
                "" => "bblack_fwhite",
                "0" => "bblack_fwhite",
                "1" => "bblack_fred",
                "2" => "bblack_fred",
                "3" => "bblack_fgreen",
                "4" => "bblack_fwhite",
            );

            $tmpdatax = $mdl_monitor->getStatusLinkMetroCCTVBandung();
            $datax = array();
            foreach ($tmpdatax as $k => $v) {
                $datax[$v['LINK_ID']] = $v;
            }

            // Zend_Debug::dump($datax[544]['STATUS']);die();
            //-================================== LINK METRO ETHERNET CCTV BANDUNG
            echo '<div class="row">';
            echo '	<div class="col-md-12">';
            echo '		<div class="portlet box grey" style="margin-bottom:10px;">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-table"></i>LINK METRO ETHERNET CCTV BANDUNG
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse"></a>
						</div>
					</div>
					<div class="portlet-body" id="linkmetrocctvbdg">
						<div class="row">';
            echo '					<div class="col-md-12" style="postion:relative; border:0px solid yellow; padding:5px;">
								<img src="/images/LINK_METRO_ETHERNET_CCTV_BANDUNG/1.png" alt="" class="img-responsive">';

            $string = $datax[73]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[73]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--1-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[73]['SERVICE_NAME'] . '" data-sid="' . $datax[73]['SERVICE_ID'] . '" data-cust="' . $datax[73]['SERVICE_NAME'] . '" data-link="' . $datax[73]['LINK_ID'] . '" style="position:absolute; top:9%; left:17%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_cctv[$datax[73]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            $string = $datax[76]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[76]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--4-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[76]['SERVICE_NAME'] . '" data-sid="' . $datax[76]['SERVICE_ID'] . '" data-cust="' . $datax[76]['SERVICE_NAME'] . '" data-link="' . $datax[76]['LINK_ID'] . '" style="position:absolute; top:35%; left:17%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_cctv[$datax[76]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            $string = $datax[74]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[74]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--5-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[74]['SERVICE_NAME'] . '" data-sid="' . $datax[74]['SERVICE_ID'] . '" data-cust="' . $datax[74]['SERVICE_NAME'] . '" data-link="' . $datax[74]['LINK_ID'] . '" style="position:absolute; top:45%; left:17%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_cctv[$datax[74]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            $string = $datax[543]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[543]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--7-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[543]['SERVICE_NAME'] . '" data-sid="' . $datax[543]['SERVICE_ID'] . '" data-cust="' . $datax[543]['SERVICE_NAME'] . '" data-link="' . $datax[543]['LINK_ID'] . '" style="position:absolute; top:61%; left:17%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_cctv[$datax[543]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            $string = $datax[72]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[72]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--9-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[72]['SERVICE_NAME'] . '" data-sid="' . $datax[72]['SERVICE_ID'] . '" data-cust="' . $datax[72]['SERVICE_NAME'] . '" data-link="' . $datax[72]['LINK_ID'] . '" style="position:absolute; top:83%; left:16%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_cctv[$datax[72]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            $string = $datax[75]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[75]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--10-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[75]['SERVICE_NAME'] . '" data-sid="' . $datax[75]['SERVICE_ID'] . '" data-cust="' . $datax[75]['SERVICE_NAME'] . '" data-link="' . $datax[75]['LINK_ID'] . '" style="position:absolute; top:83%; left:30%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_cctv[$datax[75]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            $string = $datax[79]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[79]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--11-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[79]['SERVICE_NAME'] . '" data-sid="' . $datax[79]['SERVICE_ID'] . '" data-cust="' . $datax[79]['SERVICE_NAME'] . '" data-link="' . $datax[79]['LINK_ID'] . '" style="position:absolute; top:83%; left:43%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_cctv[$datax[79]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            $string = $datax[80]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[80]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--13-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[80]['SERVICE_NAME'] . '" data-sid="' . $datax[80]['SERVICE_ID'] . '" data-cust="' . $datax[80]['SERVICE_NAME'] . '" data-link="' . $datax[80]['LINK_ID'] . '" style="position:absolute; top:83%; left:57%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_cctv[$datax[80]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            $string = $datax[81]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[81]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--14-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[81]['SERVICE_NAME'] . '" data-sid="' . $datax[81]['SERVICE_ID'] . '" data-cust="' . $datax[81]['SERVICE_NAME'] . '" data-link="' . $datax[81]['LINK_ID'] . '" style="position:absolute; top:83%; left:69%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_cctv[$datax[81]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            $string = $datax[82]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[82]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--15-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[82]['SERVICE_NAME'] . '" data-sid="' . $datax[82]['SERVICE_ID'] . '" data-cust="' . $datax[82]['SERVICE_NAME'] . '" data-link="' . $datax[82]['LINK_ID'] . '" style="position:absolute; top:83%; left:83%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_cctv[$datax[82]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            $string = $datax[83]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[83]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--17-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[83]['SERVICE_NAME'] . '" data-sid="' . $datax[83]['SERVICE_ID'] . '" data-cust="' . $datax[83]['SERVICE_NAME'] . '" data-link="' . $datax[83]['LINK_ID'] . '" style="position:absolute; top:62%; left:82%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_cctv[$datax[83]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            $string = $datax[84]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[84]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--18-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[84]['SERVICE_NAME'] . '" data-sid="' . $datax[84]['SERVICE_ID'] . '" data-cust="' . $datax[84]['SERVICE_NAME'] . '" data-link="' . $datax[84]['LINK_ID'] . '" style="position:absolute; top:53%; left:82%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_cctv[$datax[84]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            $string = $datax[78]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[78]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--19-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[78]['SERVICE_NAME'] . '" data-sid="' . $datax[78]['SERVICE_ID'] . '" data-cust="' . $datax[78]['SERVICE_NAME'] . '" data-link="' . $datax[78]['LINK_ID'] . '" style="position:absolute; top:45%; left:82%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_cctv[$datax[78]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            $string = $datax[539]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[539]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--20-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[539]['SERVICE_NAME'] . '" data-sid="' . $datax[539]['SERVICE_ID'] . '" data-cust="' . $datax[539]['SERVICE_NAME'] . '" data-link="' . $datax[539]['LINK_ID'] . '" style="position:absolute; top:17%; left:82%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_cctv[$datax[539]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            $string = $datax[71]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[71]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--21-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[71]['SERVICE_NAME'] . '" data-sid="' . $datax[71]['SERVICE_ID'] . '" data-cust="' . $datax[71]['SERVICE_NAME'] . '" data-link="' . $datax[71]['LINK_ID'] . '" style="position:absolute; top:26%; left:82%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_cctv[$datax[71]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            // $string = $datax[539]['GRAPH_ID'].'|0|'.((isset($server[0]))?$server[0]:"UNKNOWN").'|'.str_replace("<path_rra>","",$datax[539]['RRD_NAME']);
            // //echo $string.'<br>';
            // //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            // $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            // //echo $encrypted."<br>";
            // $encrypted2 = base64_encode($encrypted);
            // echo '						<!--22-->
            // <a class="btn-graph" data-toggle="modal" data-id="'.$encrypted2.'" data-title="'.$datax[539]['SERVICE_NAME'].'" data-sid="'.$datax[539]['SERVICE_ID'].'" data-cust="'.$datax[539]['SERVICE_NAME'].'" data-link="'.$datax[539]['LINK_ID'].'" style="position:absolute; top:17%; left:82%; cursor:pointer;">
            // <img class="img-responsive" src="/images/ICON/CIRCLE/circle_'.$arr_status_cctv[$datax[539]['STATUS']].'.png" style="max-width:16px;">
            // </a>';

            $string = $datax[538]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[538]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--23-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[538]['SERVICE_NAME'] . '" data-sid="' . $datax[538]['SERVICE_ID'] . '" data-cust="' . $datax[538]['SERVICE_NAME'] . '" data-link="' . $datax[538]['LINK_ID'] . '" style="position:absolute; top:9%; left:82%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_cctv[$datax[538]['STATUS']] . '.png" style="max-width:16px;">
								</a>
								
							</div>
		';

            echo '				</div>
					</div>
				</div>';
            echo '	</div>';
            echo '</div>';

            //-================================== LINK DATIN SECURITY
            echo '<div class="row">';
            echo '	<div class="col-md-12">';
            echo '		<div class="portlet box grey" style="margin-bottom:10px;">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-table"></i>LINK DATIN SECURITY
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse"></a>
						</div>
					</div>
					<div class="portlet-body" id="linkdatinsecurity">
						<div class="row">';



            $tmpdatax = $mdl_monitor->getStatusLinkDatinSecurity();
            $datax = array();
            foreach ($tmpdatax as $k => $v) {
                $datax[$v['LINK_ID']][$v['SEQ_ID']] = $v;
            }

            // Zend_Debug::dump($datax);die();
            $arr_status_datin = array(
                "" => "bblack_fwhite",
                "0" => "bblack_fwhite",
                "1" => "bblack_fred",
                "2" => "bblack_fred",
                "3" => "bblack_fgreen",
                "4" => "bblack_fwhite",
            );

            echo '					<div class="col-md-6" style="postion:relative; border:0px solid yellow; padding:5px;">
								<img src="/images/LINK_DATIN_SECURITY/1.png" alt="" class="img-responsive">';

            //-==== GEDUNG JCC

            $string = $datax[85][1]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[85][1]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--1-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[85][1]['SERVICE_NAME'] . '" data-sid="' . $datax[85][1]['SERVICE_ID'] . '" data-cust="' . $datax[85][1]['SERVICE_NAME'] . '" data-link="' . $datax[85][1]['LINK_ID'] . '" style="position:absolute; top:30%; left:9%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[85][1]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            // echo '						<!--2-->
            // <a class="btn-graph-un" style="position:absolute; top:43%; left:9%; cursor:pointer;">
            // <img class="img-responsive" src="/images/ICON/CIRCLE/circle_bblack_fwhite.png" style="max-width:16px;">
            // </a>';								

            $string = $datax[85][3]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[85][3]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--3-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[85][3]['SERVICE_NAME'] . '" data-sid="' . $datax[85][3]['SERVICE_ID'] . '" data-cust="' . $datax[85][3]['SERVICE_NAME'] . '" data-link="' . $datax[85][3]['LINK_ID'] . '" style="position:absolute; top:58%; left:9%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[85][3]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            // echo '						<!--4-->
            // <a class="btn-graph-un" style="position:absolute; top:70%; left:9%; cursor:pointer;">
            // <img class="img-responsive" src="/images/ICON/CIRCLE/circle_bblack_fwhite.png" style="max-width:16px;">
            // </a>';						
            // echo '						<!--5-->
            // <a class="btn-graph-un" style="position:absolute; top:82%; left:9%; cursor:pointer;">
            // <img class="img-responsive" src="/images/ICON/CIRCLE/circle_bblack_fwhite.png" style="max-width:16px;">
            // </a>';
            //-==== HYATT BANDUNG
            $string = $datax[86][1]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[86][1]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--1-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[86][1]['SERVICE_NAME'] . '" data-sid="' . $datax[86][1]['SERVICE_ID'] . '" data-cust="' . $datax[86][1]['SERVICE_NAME'] . '" data-link="' . $datax[86][1]['LINK_ID'] . '" style="position:absolute; top:30%; left:33%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[86][1]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            // echo '						<!--2-->
            // <a class="btn-graph-un" style="position:absolute; top:43%; left:33%; cursor:pointer;">
            // <img class="img-responsive" src="/images/ICON/CIRCLE/circle_bblack_fwhite.png" style="max-width:16px;">
            // </a>';								

            $string = $datax[86][3]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[86][3]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--3-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[86][3]['SERVICE_NAME'] . '" data-sid="' . $datax[86][3]['SERVICE_ID'] . '" data-cust="' . $datax[86][3]['SERVICE_NAME'] . '" data-link="' . $datax[86][3]['LINK_ID'] . '" style="position:absolute; top:58%; left:33%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[86][3]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            // echo '						<!--4-->
            // <a class="btn-graph-un" style="position:absolute; top:70%; left:33%; cursor:pointer;">
            // <img class="img-responsive" src="/images/ICON/CIRCLE/circle_bblack_fwhite.png" style="max-width:16px;">
            // </a>';						
            // echo '						<!--5-->
            // <a class="btn-graph-un" style="position:absolute; top:82%; left:33%; cursor:pointer;">
            // <img class="img-responsive" src="/images/ICON/CIRCLE/circle_bblack_fwhite.png" style="max-width:16px;">
            // </a>';		
            //-==== BINDA JABAR
            $string = $datax[87][1]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[87][1]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--1-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[87][1]['SERVICE_NAME'] . '" data-sid="' . $datax[87][1]['SERVICE_ID'] . '" data-cust="' . $datax[87][1]['SERVICE_NAME'] . '" data-link="' . $datax[87][1]['LINK_ID'] . '" style="position:absolute; top:30%; left:58%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[87][1]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            // echo '						<!--2-->
            // <a class="btn-graph-un" style="position:absolute; top:43%; left:58%; cursor:pointer;">
            // <img class="img-responsive" src="/images/ICON/CIRCLE/circle_bblack_fwhite.png" style="max-width:16px;">
            // </a>';		
            $string = $datax[87][3]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[87][3]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);

            echo '						<!--3-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[87][3]['SERVICE_NAME'] . '" data-sid="' . $datax[87][3]['SERVICE_ID'] . '" data-cust="' . $datax[87][3]['SERVICE_NAME'] . '" data-link="' . $datax[87][3]['LINK_ID'] . '" style="position:absolute; top:58%; left:58%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[87][3]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            // echo '						<!--4-->
            // <a class="btn-graph-un" style="position:absolute; top:70%; left:58%; cursor:pointer;">
            // <img class="img-responsive" src="/images/ICON/CIRCLE/circle_bblack_fwhite.png" style="max-width:16px;">
            // </a>';						
            // echo '						<!--5-->
            // <a class="btn-graph-un" style="position:absolute; top:82%; left:58%; cursor:pointer;">
            // <img class="img-responsive" src="/images/ICON/CIRCLE/circle_bblack_fwhite.png" style="max-width:16px;">
            // </a>';						
            //-==== IBIS BANDUNG
            $string = $datax[88][1]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[88][1]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--1-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[88][1]['SERVICE_NAME'] . '" data-sid="' . $datax[88][1]['SERVICE_ID'] . '" data-cust="' . $datax[88][1]['SERVICE_NAME'] . '" data-link="' . $datax[88][1]['LINK_ID'] . '" style="position:absolute; top:30%; left:82%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[88][1]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            $string = $datax[88][3]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[88][3]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);

            echo '						<!--2-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[88][3]['SERVICE_NAME'] . '" data-sid="' . $datax[88][3]['SERVICE_ID'] . '" data-cust="' . $datax[88][3]['SERVICE_NAME'] . '" data-link="' . $datax[88][3]['LINK_ID'] . '" style="position:absolute; top:51%; left:82%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[88][3]['STATUS']] . '.png" style="max-width:16px;">
								</a>';
            // echo '						<!--3-->
            // <a class="btn-graph" data-toggle="modal" style="position:absolute; top:70%; left:82%; cursor:pointer;">
            // <img class="img-responsive" src="/images/ICON/CIRCLE/circle_bblack_fwhite.png" style="max-width:16px;">
            // </a>';						
            // echo '						<!--4-->
            // <a class="btn-graph-un" style="position:absolute; top:82%; left:82%; cursor:pointer;">
            // <img class="img-responsive" src="/images/ICON/CIRCLE/circle_bblack_fwhite.png" style="max-width:16px;">
            // </a>';

            echo '					</div>
		';

            echo '					<div class="col-md-6" style="postion:relative; border:0px solid yellow; padding:5px;">
								<img src="/images/LINK_DATIN_SECURITY/2.png" alt="" class="img-responsive">';
            //-==== GEDUNG MERDERKA
            $string = $datax[89][1]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[89][1]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--1-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[89][1]['SERVICE_NAME'] . '" data-sid="' . $datax[89][1]['SERVICE_ID'] . '" data-cust="' . $datax[89][1]['SERVICE_NAME'] . '" data-link="' . $datax[89][1]['LINK_ID'] . '" style="position:absolute; top:30%; left:9%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[89][1]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            $string = $datax[89][3]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[89][3]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);

            echo '						<!--2-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[89][3]['SERVICE_NAME'] . '" data-sid="' . $datax[89][3]['SERVICE_ID'] . '" data-cust="' . $datax[89][3]['SERVICE_NAME'] . '" data-link="' . $datax[89][3]['LINK_ID'] . '" style="position:absolute; top:51%; left:9%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[89][3]['STATUS']] . '.png" style="max-width:16px;">
								</a>';
            // echo '						<!--3-->
            // <a class="btn-graph" data-toggle="modal" style="position:absolute; top:70%; left:9%; cursor:pointer;">
            // <img class="img-responsive" src="/images/ICON/CIRCLE/circle_bblack_fwhite.png" style="max-width:16px;">
            // </a>';						
            // echo '						<!--4-->
            // <a class="btn-graph-un" style="position:absolute; top:82%; left:9%; cursor:pointer;">
            // <img class="img-responsive" src="/images/ICON/CIRCLE/circle_bblack_fwhite.png" style="max-width:16px;">
            // </a>';
            //-==== MOBILE SPRINTER
            $string = $datax[92][1]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[92][1]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--1-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[92][1]['SERVICE_NAME'] . '" data-sid="' . $datax[92][1]['SERVICE_ID'] . '" data-cust="' . $datax[92][1]['SERVICE_NAME'] . '" data-link="' . $datax[92][1]['LINK_ID'] . '" style="position:absolute; top:30%; left:33%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[92][1]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            $string = $datax[92][3]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[92][3]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);

            echo '						<!--2-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[92][3]['SERVICE_NAME'] . '" data-sid="' . $datax[92][3]['SERVICE_ID'] . '" data-cust="' . $datax[92][3]['SERVICE_NAME'] . '" data-link="' . $datax[92][3]['LINK_ID'] . '" style="position:absolute; top:51%; left:33%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[92][3]['STATUS']] . '.png" style="max-width:16px;">
								</a>';
            // echo '						<!--3-->
            // <a class="btn-graph" data-toggle="modal" style="position:absolute; top:70%; left:33%; cursor:pointer;">
            // <img class="img-responsive" src="/images/ICON/CIRCLE/circle_bblack_fwhite.png" style="max-width:16px;">
            // </a>';						
            // echo '						<!--4-->
            // <a class="btn-graph-un" style="position:absolute; top:82%; left:33%; cursor:pointer;">
            // <img class="img-responsive" src="/images/ICON/CIRCLE/circle_bblack_fwhite.png" style="max-width:16px;">
            // </a>';
            //-==== GEDUNG MUSEUM
            $string = $datax[90][1]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[90][1]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--1-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[90][1]['SERVICE_NAME'] . '" data-sid="' . $datax[90][1]['SERVICE_ID'] . '" data-cust="' . $datax[90][1]['SERVICE_NAME'] . '" data-link="' . $datax[90][1]['LINK_ID'] . '" style="position:absolute; top:30%; left:58%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[90][1]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            $string = $datax[90][3]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[90][3]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);

            echo '						<!--2-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[90][3]['SERVICE_NAME'] . '" data-sid="' . $datax[90][3]['SERVICE_ID'] . '" data-cust="' . $datax[90][3]['SERVICE_NAME'] . '" data-link="' . $datax[90][3]['LINK_ID'] . '" style="position:absolute; top:51%; left:58%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[90][3]['STATUS']] . '.png" style="max-width:16px;">
								</a>';
            // echo '						<!--3-->
            // <a class="btn-graph" data-toggle="modal" style="position:absolute; top:70%; left:58%; cursor:pointer;">
            // <img class="img-responsive" src="/images/ICON/CIRCLE/circle_bblack_fwhite.png" style="max-width:16px;">
            // </a>';						
            // echo '						<!--4-->
            // <a class="btn-graph-un" style="position:absolute; top:82%; left:58%; cursor:pointer;">
            // <img class="img-responsive" src="/images/ICON/CIRCLE/circle_bblack_fwhite.png" style="max-width:16px;">
            // </a>';		
            //-==== PEMKOT BANDUNG
            $string = $datax[91][1]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[91][1]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--1-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[91][1]['SERVICE_NAME'] . '" data-sid="' . $datax[91][1]['SERVICE_ID'] . '" data-cust="' . $datax[91][1]['SERVICE_NAME'] . '" data-link="' . $datax[91][1]['LINK_ID'] . '" style="position:absolute; top:30%; left:82%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[91][1]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            // echo '						<!--2-->
            // <a class="btn-graph-un" style="position:absolute; top:43%; left:82%; cursor:pointer;">
            // <img class="img-responsive" src="/images/ICON/CIRCLE/circle_bblack_fwhite.png" style="max-width:16px;">
            // </a>';		
            $string = $datax[91][3]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[91][3]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);

            echo '						<!--3-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[91][3]['SERVICE_NAME'] . '" data-sid="' . $datax[91][3]['SERVICE_ID'] . '" data-cust="' . $datax[91][3]['SERVICE_NAME'] . '" data-link="' . $datax[91][3]['LINK_ID'] . '" style="position:absolute; top:58%; left:82%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[91][3]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            // echo '						<!--4-->
            // <a class="btn-graph-un" style="position:absolute; top:70%; left:82%; cursor:pointer;">
            // <img class="img-responsive" src="/images/ICON/CIRCLE/circle_bblack_fwhite.png" style="max-width:16px;">
            // </a>';						
            // echo '						<!--5-->
            // <a class="btn-graph-un" style="position:absolute; top:82%; left:82%; cursor:pointer;">
            // <img class="img-responsive" src="/images/ICON/CIRCLE/circle_bblack_fwhite.png" style="max-width:16px;">
            // </a>';	

            echo '
							</div>
		';

            echo '				</div>
					</div>
				</div>';
            echo '	</div>';
            echo '</div>';


            //-================================== LINK DATIN VENUE KEMENLU
            echo '<div class="row">';
            echo '	<div class="col-md-12">';
            echo '		<div class="portlet box grey" style="margin-bottom:10px;">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-table"></i>LINK DATIN VENUE KEMENLU
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse"></a>
						</div>
					</div>
					<div class="portlet-body" id="linkdatinvenue">
						<div class="row">';

            $tmpdatax = $mdl_monitor->getStatusLinkDatinVenue();
            $datax = array();
            foreach ($tmpdatax as $k => $v) {
                $datax[$v['LINK_ID']][$v['SEQ_ID']] = $v;
            }

            // Zend_Debug::dump($datax);die();
            $arr_status_datin = array(
                "" => "bblack_fwhite",
                "0" => "bblack_fwhite",
                "1" => "bblack_fred",
                "2" => "bblack_fred",
                "3" => "bblack_fgreen",
                "4" => "bblack_fwhite",
            );

            echo '					<div class="col-md-6" style="postion:relative; border:0px solid yellow;">
								<img src="/images/LINK_DATIN_VENUE/1.png" alt="" class="img-responsive">';


            //-==== EXHIBITION HALL A  JCC
            $string = $datax[589][1]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[589][1]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--1-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[589][1]['SERVICE_NAME'] . '" data-sid="' . $datax[589][1]['SERVICE_ID'] . '" data-cust="' . $datax[589][1]['SERVICE_NAME'] . '" data-link="' . $datax[589][1]['LINK_ID'] . '" style="position:absolute; top:30%; left:10%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[589][1]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            $string = $datax[589][3]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[589][3]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--2-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[589][3]['SERVICE_NAME'] . '" data-sid="' . $datax[589][3]['SERVICE_ID'] . '" data-cust="' . $datax[589][3]['SERVICE_NAME'] . '" data-link="' . $datax[589][3]['LINK_ID'] . '" style="position:absolute; top:58%; left:10%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[589][3]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            //-==== PLENARY HALL JCC
            $string = $datax[587][1]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[587][1]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--1-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[587][1]['SERVICE_NAME'] . '" data-sid="' . $datax[587][1]['SERVICE_ID'] . '" data-cust="' . $datax[587][1]['SERVICE_NAME'] . '" data-link="' . $datax[587][1]['LINK_ID'] . '" style="position:absolute; top:30%; left:34%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[587][1]['STATUS']] . '.png" style="max-width:16px;">
								</a>';


            $string = $datax[587][3]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[587][3]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--2-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[587][3]['SERVICE_NAME'] . '" data-sid="' . $datax[587][3]['SERVICE_ID'] . '" data-cust="' . $datax[587][3]['SERVICE_NAME'] . '" data-link="' . $datax[587][3]['LINK_ID'] . '" style="position:absolute; top:58%; left:34%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[587][3]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            //-==== R. SEKRETARIAT SENAYAN
            $string = $datax[583][1]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[583][1]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--1-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[583][1]['SERVICE_NAME'] . '" data-sid="' . $datax[583][1]['SERVICE_ID'] . '" data-cust="' . $datax[583][1]['SERVICE_NAME'] . '" data-link="' . $datax[583][1]['LINK_ID'] . '" style="position:absolute; top:30%; left:57%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[583][1]['STATUS']] . '.png" style="max-width:16px;">
								</a>';


            $string = $datax[583][3]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[583][3]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--2-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[583][3]['SERVICE_NAME'] . '" data-sid="' . $datax[583][3]['SERVICE_ID'] . '" data-cust="' . $datax[583][3]['SERVICE_NAME'] . '" data-link="' . $datax[583][3]['LINK_ID'] . '" style="position:absolute; top:58%; left:57%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[583][3]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            //-====  RFID JCC 1 (Cempaka 3)
            $string = $datax[591][1]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[591][1]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--1-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[591][1]['SERVICE_NAME'] . '" data-sid="' . $datax[591][1]['SERVICE_ID'] . '" data-cust="' . $datax[591][1]['SERVICE_NAME'] . '" data-link="' . $datax[591][1]['LINK_ID'] . '" style="position:absolute; top:30%; left:80%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[591][1]['STATUS']] . '.png" style="max-width:16px;">
								</a>';


            $string = $datax[591][3]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[591][3]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--2-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[591][3]['SERVICE_NAME'] . '" data-sid="' . $datax[591][3]['SERVICE_ID'] . '" data-cust="' . $datax[591][3]['SERVICE_NAME'] . '" data-link="' . $datax[591][3]['LINK_ID'] . '" style="position:absolute; top:51%; left:80%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[591][3]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            echo '					</div>
		';
            echo '					<div class="col-md-6">
								<img src="/images/LINK_DATIN_VENUE/2.png" alt="" class="img-responsive">';


            //-==== RFID JCC 1 (Lobby Melati)
            $string = $datax[593][1]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[593][1]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--1-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[593][1]['SERVICE_NAME'] . '" data-sid="' . $datax[593][1]['SERVICE_ID'] . '" data-cust="' . $datax[593][1]['SERVICE_NAME'] . '" data-link="' . $datax[593][1]['LINK_ID'] . '" style="position:absolute; top:30%; left:10%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[593][1]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            $string = $datax[593][3]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[593][3]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--2-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[593][3]['SERVICE_NAME'] . '" data-sid="' . $datax[593][3]['SERVICE_ID'] . '" data-cust="' . $datax[593][3]['SERVICE_NAME'] . '" data-link="' . $datax[593][3]['LINK_ID'] . '" style="position:absolute; top:58%; left:10%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[593][3]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            //-==== RFID JCC 4 JCC
            $string = $datax[595][1]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[595][1]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--1-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[595][1]['SERVICE_NAME'] . '" data-sid="' . $datax[595][1]['SERVICE_ID'] . '" data-cust="' . $datax[595][1]['SERVICE_NAME'] . '" data-link="' . $datax[595][1]['LINK_ID'] . '" style="position:absolute; top:30%; left:34%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[595][1]['STATUS']] . '.png" style="max-width:16px;">
								</a>';


            $string = $datax[595][3]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[595][3]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--2-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[595][3]['SERVICE_NAME'] . '" data-sid="' . $datax[595][3]['SERVICE_ID'] . '" data-cust="' . $datax[595][3]['SERVICE_NAME'] . '" data-link="' . $datax[595][3]['LINK_ID'] . '" style="position:absolute; top:58%; left:34%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[595][3]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            //-==== RFID JCC 5 JCC
            $string = $datax[597][1]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[597][1]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--1-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[597][1]['SERVICE_NAME'] . '" data-sid="' . $datax[597][1]['SERVICE_ID'] . '" data-cust="' . $datax[597][1]['SERVICE_NAME'] . '" data-link="' . $datax[597][1]['LINK_ID'] . '" style="position:absolute; top:30%; left:57%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[597][1]['STATUS']] . '.png" style="max-width:16px;">
								</a>';


            $string = $datax[597][3]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[597][3]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--2-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[597][3]['SERVICE_NAME'] . '" data-sid="' . $datax[597][3]['SERVICE_ID'] . '" data-cust="' . $datax[597][3]['SERVICE_NAME'] . '" data-link="' . $datax[597][3]['LINK_ID'] . '" style="position:absolute; top:58%; left:57%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[597][3]['STATUS']] . '.png" style="max-width:16px;">
								</a>';

            //-====  RFID JCC 6 JCC
            $string = $datax[599][1]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[599][1]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--1-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[599][1]['SERVICE_NAME'] . '" data-sid="' . $datax[599][1]['SERVICE_ID'] . '" data-cust="' . $datax[599][1]['SERVICE_NAME'] . '" data-link="' . $datax[599][1]['LINK_ID'] . '" style="position:absolute; top:30%; left:80%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[599][1]['STATUS']] . '.png" style="max-width:16px;">
								</a>';


            $string = $datax[599][3]['GRAPH_ID'] . '|0|' . ((isset($server[0])) ? $server[0] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $datax[599][3]['RRD_NAME']);
            //echo $string.'<br>';
            //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
            $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
            //echo $encrypted."<br>";
            $encrypted2 = base64_encode($encrypted);
            echo '						<!--2-->
								<a class="btn-graph" data-toggle="modal" data-id="' . $encrypted2 . '" data-title="' . $datax[599][3]['SERVICE_NAME'] . '" data-sid="' . $datax[599][3]['SERVICE_ID'] . '" data-cust="' . $datax[599][3]['SERVICE_NAME'] . '" data-link="' . $datax[599][3]['LINK_ID'] . '" style="position:absolute; top:51%; left:80%; cursor:pointer;">
									<img class="img-responsive" src="/images/ICON/CIRCLE/circle_' . $arr_status_datin[$datax[599][3]['STATUS']] . '.png" style="max-width:16px;">
								</a>';


            echo '					</div>
		';

            echo '				</div>
					</div>
				</div>';
            echo '	</div>';
            echo '</div>';


            echo '<div class="row">';
            echo '	<div class="col-md-12">';
            echo '		<div class="portlet box grey" style="margin-bottom:10px;">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-reorder"></i>LAN KAA BANDUNG
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse"></a>
						</div>
					</div>
					<div class="portlet-body" id="linkmediacenter">
						<div class="row">';

            echo '<div class="col-md-12" style="height:560px;">';
            echo '	<iframe src="http://guest@180.250.230.10:4646/map.asp?map=KAA-PGN.wup" frameborder="0" style="overflow: hidden; height: 100%;
			width: 100%; height="100%"></iframe>';
            echo '</div>';

            echo '</div>';
            echo '</div>';
            echo '</div>';
            echo '</div>';
            echo '</div>';
        }
//-=== REPORT
        else if ($params['tpanename'] == 'report') {


            $dm = $mdl_monitor->getDMposko();


            $msg = "";
            $msg .= "Pak Awal Ykh\n";
            $msg .= "\n";
            $msg .= "Kami laporkan status fastel VIP TelkomGroup di area MASIV lokasi Jakarta dan Bandung pada AACC 2015 posisi tgl " . date("d F") . " pukul <input id=\"input1\" name=\"input1\"> wib sbb :\n";
            $msg .= "\n";
            $msg .= "1. Secara umum fastel berfungsi <input id=\"input2\" name=\"input2\"> dengan rincian terlampir\n";
            $msg .= "\n";
            $msg .= "2. Trafik pada MRTG normal dengan pemakaian tertinggi terjadi pada pukul <input id=\"input3\" name=\"input3\">  sebesar <input id=\"input4\" name=\"input4\"> . \n";
            $msg .= " \n";
            $msg .= "3. Aktivitas Posko hari ini difokuskan :\n";
            $msg .= "Jakarta : <textarea id=\"input5\" name=\"input5\"></textarea>\n";
            $msg .= "Bandung : <textarea id=\"input6\" name=\"input6\"></textarea>\n";
            $msg .= " \n";
            $msg .= "4. Petugas Posko : \n";
            $msg .= "Hari ini, " . date("l, d F Y") . " :\n";
            $msg .= "DM Posko Semanggi : <input id=\"input7\" name=\"input7\">\n";
            $msg .= "DM Posko Bandung : <input id=\"input8\" name=\"input8\">\n";
            $msg .= "\n";
            $msg .= "Besok : " . date('l, d F Y', strtotime(date('Y/m/d') . " +1 days")) . " :\n";
            $msg .= "DM Posko Semanggi : <input id=\"input9\" name=\"input9\">\n";
            $msg .= "DM Posko Bandung : <input id=\"input10\" name=\"input10\">\n";
            $msg .= "\n";
            $msg .= "\n";
            $msg .= "5. Related Info :\n";
            $msg .= "<textarea id=\"input11\" name=\"input11\"></textarea>\n";
            $msg .= "\n";
            $msg .= "\n";
            $msg .= "\n";
            $msg .= "Demikian. Tks\n";
            $msg .= "\n";
            $msg .= "\n";
            $msg .= "\n";
            $msg .= "----------------------\n";
            $msg .= "Lampiran :\n";
            $msg .= "\n";
            $msg .= "Status Fastel TGroup posisi " . date("d F Y") . " jam <input id=\"input12\" name=\"input12\"> wib\n";
            $msg .= "\n";
            $msg .= "1. Rekap Fastel di lokasi MASIV\n";
            $msg .= "\n";
            $msg .= "Lokasi Jakarta\n";
            $msg .= "\n";
            $msg .= "M Center/JCC Hall B/Astinet 10 Gbps/ Ok\n";
            $msg .= "M Center/JCC Hall B/ Wifi 8 AP 1 Gbps/Ok\n";
            $msg .= "M Center/JCC Hall B/ Sinyal Tsel/Ok\n";
            $msg .= "M Center/JCC Hall B/SGN 1 Unit/ok\n";
            $msg .= "\n";
            $msg .= "Akomodasi/Hotel Sultan/wifi ID 1 Gbps/ok\n";
            $msg .= "Akomodasi/Hotel Sultan/Sinyal TSel/ok\n";
            $msg .= "Akomodasi/Hotel pendukung (18)/ Wifi id/ok\n";
            $msg .= "Akomodasi/Hotel pendukung (18)/ Sinyal Tsel/ok\n";
            $msg .= "\n";
            $msg .= "Security/Puskodal/Astinet 20 Mbps/ok\n";
            $msg .= "Security/cctv Puskodal/Metro, VPN 100 Mbps/ok\n";
            $msg .= "Security/Paspampres/ Astinet 20 Mbps/ok\n";
            $msg .= "Security/TNI AD/AStinet 20 Mbps/ok\n";
            $msg .= "Security/ CCTV Puskotis/ VPN IP 3 titik 25 Mbps/ok\n";
            $msg .= "\n";
            $msg .= "\n";
            $msg .= "Int. Airport/Bandara Soeta/ Wifi Id 160 AP/ Ok\n";
            $msg .= "Int. Airport/Bandara Soeta/ Sinyal Tsel/ Ok\n";
            $msg .= "Int. Airport/Bandara Halim/ Wifi id 45 Ap/Ok\n";
            $msg .= "Int. Airport/Bandara Halim/ Sinyal Tsel/Ok\n";
            $msg .= "\n";
            $msg .= "Venue/JCC Meeting Room/Astinet 50 Mbps/ok\n";
            $msg .= "Venue/JCC Hall A/Astinet 100 Mbps/ok\n";
            $msg .= "Venue/JCC-all/Wifi ID 1 Gbps/ok\n";
            $msg .= "Venue/JCC-all/Sinyal Tsel/ok\n";
            $msg .= "\n";
            $msg .= "\n";
            $msg .= "Lokasi Bandung\n";
            $msg .= "\n";
            $msg .= "M Centre/Hotel Ibis/Astinet 2 Gbps/ok\n";
            $msg .= "M Centre/Hotel Ibis/Wifi id 7 AP 1 Gbps/ok\n";
            $msg .= "M Centre/Hotel Ibis/Sinyal Tsel/ok\n";
            $msg .= "M Centre/Hotel Ibis/SNG 1 Unit/ok\n";
            $msg .= "M Centre/New Majestik/Astinet 2 Gbps/ok\n";
            $msg .= "M Centre/New Majestik/Wifi id 6 AP 1 Gbps/ok\n";
            $msg .= "M Centre/New Majestik/Sinyal Tsel/ok\n";
            $msg .= "M Centre/PGN/ Astinet 2 Gbps/ok\n";
            $msg .= "M Centre/PGN/ Wifi id 13 AP 1 Gbps/ok\n";
            $msg .= "M Centre/PGN/ Sinyal Tsel/ok\n";
            $msg .= "\n";
            $msg .= "\n";
            $msg .= "Accomodation/Hotel Savoy/wifi 10 Ap 500 Mbps/ok\n";
            $msg .= "Accomodation/Hotel Savoy/Sinyal Tsel/ok\n";
            $msg .= "\n";
            $msg .= "Security/Paspampres/Astinet 20 Mbps/ok\n";
            $msg .= "Security/ BIN/ Astinet 7 link 120 Mbps/ok\n";
            $msg .= "Security/ CCTV Puskotis/ VPN IP 4 titik 95 Mbps/ok\n";
            $msg .= "\n";
            $msg .= "\n";
            $msg .= "Int. Airport/Husein/wifi id 6 AP/ok\n";
            $msg .= "Int.Airport/Husein/Sinyal Tsel/ok\n";
            $msg .= "\n";
            $msg .= "Venue/Savoy Homan/Metro 50 Mbps/ok\n";
            $msg .= "Venue/Balai Pakuan/ Metro 50 Mbps/ok\n";
            $msg .= "Venue/Majestik/Metro 50 Mbps/ok\n";
            $msg .= "Venue/Area Historical Walk/wifi id 1 Gbps/ok\n";
            $msg .= "Venue/Gedung Merdeka/Astinet 1 Gbps/ok\n";
            $msg .= "Venue/All site /Sinyal Tsel/ok\n";
            $msg .= "\n";
            $msg .= "\n";
            $msg .= "2. REKAP FASTEL TELKOMSEL\n";
            $msg .= "KAA Critical Network Incident Update\n";
            $msg .= "Type/Status/Reg/Loc/Start/Duration/End/Impact/Actions/Estimated End\n";
            $msg .= "<textarea id=\"input13\" name=\"input13\"></textarea>\n";
            $msg .= "\n";
            $msg .= "\n";
            $msg .= "3. REKAP FASTEL TELIN \n";
            $msg .= "DATIN: <input id=\"input14\" name=\"input14\"> M of 10 G\n";
            $msg .= "VOICE: <input id=\"input15\" name=\"input15\">; ACD : <input id=\"input16\" name=\"input16\">; ASR: <input id=\"input17\" name=\"input17\">\n";
            $msg .= "TOTAL BACKBONE INTERNATIONAL <input id=\"input18\" name=\"input18\"> of <input id=\"input19\" name=\"input19\">\n";
            $msg .= "\n";
            $msg .= "GBR: <input id=\"input20\" name=\"input20\">\n";
            $msg .= "DMI: <input id=\"input21\" name=\"input21\">\n";
            $msg .= "BTM: <input id=\"input22\" name=\"input22\">\n";
            $msg .= "SBY: <input id=\"input23\" name=\"input23\">\n";
            $msg .= "SMG: <input id=\"input24\" name=\"input24\">\n";
            $msg .= "\n";
            $msg .= "\n";
            $msg .= "Demikian kami sampaikan\n";
            $msg .= "Duty Manager :<input id=\"input25\" name=\"input25\">\n";
            $msg .= "Deputy DM : <input id=\"input26\" name=\"input26\">\n";
            $msg .= "Telp posko: 021-5740400";



            echo '<div class="alert alert-success alert-dismissable hidden" id="alertinfo">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
	<span id="alertmsg"><strong>Warning!</strong> Something went wrong. Please check.</span>
</div>

<div style="margin:0px auto;text-align:center" id="processbar" class="hidden">
	<div class="progress progress-striped active" style="margin:0px auto;text-align:center">
		<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
			<span class="sr-only">
				On Process
			</span>
		</div>
	</div>
	<span style="margin:0px auto;">Please wait...processing request</span>
	<br>
</div>';


            echo '<div class="panel-group accordion" id="accordion_workforcemgt">';
            echo '<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
				<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion_workforcemgt" href="#collapse_1">
				Laporan Piket KAA 2015 </a>
				</h4>
			</div>
			<div id="collapse_1" class="panel-collapse collapse" style="height:0px;">
				<div class="panel-body">';
            echo '				<div class="row">';
            echo '					<div class="col-md-12">';

            echo '						<form role="form" id="form_report">
								<div class="form-body">';
            echo "								<pre>" . $msg . "</pre>
								</div>";
            echo '							<div class="form-actions">
									<button type="button" class="btn blue" onclick="sendTelegram();">Submit</button>
								</div>
						</form>';
            echo '				</div>';
            echo '			</div>';
            echo '		</div>';
            echo '	</div>';
            echo '</div>';
        } else if ($params['tpanename'] == 'tickets') {


            $mdl_monitor = new Model_Monitor();

            $tmp_data_ap = $mdl_monitor->getInboxTicketAP();
// Zend_Debug::dump($tmp_data_ap);die();

            $datas = array();
            foreach ($tmp_data_ap as $k => $v) {
                $datas[$v['STATUS_GGN']][] = $v;
            }

//Zend_Debug::dump($paginator);die();
// $this->view->paginator = $paginator;

            echo '<div class="row">';
            echo '<div class="col-md-12">';
            echo '<div class="portlet box grey">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-ticket"></i> WIFI; <span style="color:white;">' . count($datas[0]) . '</span> <span style="color:red;">Tickets OPEN;</span> <span style="color:white;">' . count($datas[1]) . '</span> <span style="color:#40FF00;">Tickets CLOSE;</span>
					</div>
					<div class="tools">
						<a href="javascript:;" class="expand"></a>
					</div>
				</div>
				<div class="portlet-body" id="portlet_ticket" style="display: none;">
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">';

            echo '<table class="table table-bordered table-striped" style="font-size: 10px;" id="ticketap_datatable">
			<thead>
			<tr>
				<th>#</th>
				<th>LOCATION</th>
				<th>NMS</th>
				<th>AP LOCATION</th>
				<th>ETHERNETMAC</th>
				<th>STATUS_GGN</th>
				<th>OPEN DATE</th>
			</tr>
			</thead>
			<tbody>
			';

            $arr_status = array(
                "0" => "OPEN",
                "1" => "CLOSE",
            );
            foreach ($tmp_data_ap as $k => $v) {
                $closed = "CLOSED\nby " . $v['USER_ID'] . "\n[" . $v['DATE_CLOSED'] . "]";
                echo '	<tr>
				<td>' . ($k + 1) . '</td>
				<td>' . $v['ALIAS_NAME'] . '</td>
				<td>' . $v['NMS_NAME'] . '</td>
				<td>' . $v['AP_LOCATION'] . '</td>
				<td>' . $v['ETHERNETMAC'] . '</td>
				<!--td><a class="btn default btn-mod_ticket" data-toggle="modal" data-layanan="WIFI" data-id="' . $v['ID'] . '">' . $arr_status[$v['STATUS_GGN']] . '</a></td-->
				<td style="text-align:center;">' . ($v['STATUS_GGN'] == 0 ? '<a href="#modal_edit" class="btn default" data-toggle="modal" data-layanan="WIFI" data-id="' . $v['ID'] . '" onclick="getRowTicket(this);">' . $arr_status[$v['STATUS_GGN']] . '</a>' : "<pre>" . $closed . "</pre>") . '</td>
				<td>' . $v['DATE_ID'] . '</td>
			</tr>';
            }
            echo '		</tbody>
		</table>';

            echo '					</div>
						</div>
					</div>	
				</div>
		</div>
		';
            echo '</div>';
            echo '</div>';



            $tmp_data_router = $mdl_monitor->getInboxTicketRouter();
// Zend_Debug::dump($tmp_data_router);die();
// if($identity->uname="him"){
// Zend_Debug::dump($tmp_data_router);die();

            $data_router = array();
            $data_ggn = array();
            foreach ($tmp_data_router as $k => $v) {
                $data_router[$v["SERVICE_TYPE"]][] = $v;
                $data_ggn[$v["SERVICE_TYPE"]][$v["STATUS_GGN"]][] = $v;
            }
// Zend_Debug::dump($data_ggn);die();


            $arr_service_type = array(
                "TELKOMSEL", "DATIN", "CCTV"
            );
// Zend_Debug::dump($arr_service_type);die();



            foreach ($arr_service_type as $k => $v) {
                // Zend_Debug::dump($data_router[$v]);die();

                echo '<div class="row">';
                echo '<div class="col-md-12">';
                echo '<div class="portlet box grey">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-ticket"></i> ' . $v . '; <span style="color:white;">' . count($data_ggn[$v][0]) . '</span> <span style="color:red;">Tickets OPEN;</span> <span style="color:white;">' . count($data_ggn[$v][1]) . '</span> <span style="color:#40FF00;">Tickets CLOSE;</span>
					</div>
					<div class="tools">
						<a href="javascript:;" class="expand"></a>
					</div>
				</div>
				<div class="portlet-body" id="portlet_ticket" style="display: none;">
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">';

                echo '<table class="table table-bordered table-striped" style="font-size: 10px;" id="ticketap_datatable">
			<thead>
			<tr>
				<th>#</th>
				<th>LOCATION</th>
				<th>SERVICE ID</th>
				<th>SERVICE NAME</th>
				<th>HOSTNAME</th>
				<th>IP ADDRESS</th>
				<th>PORT</th>
				<th>VLAN ID</th>
				<th>STATUS_GGN</th>
				<th>OPEN DATE</th>
			</tr>
			</thead>
			<tbody>
			';

                if (count($data_router[$v]) > 0) {
                    foreach ($data_router[$v] as $k2 => $v2) {


                        $arr_status = array(
                            "0" => "OPEN",
                            "1" => "CLOSE",
                        );

                        $closed = "CLOSED\nby " . $v2['USER_ID'] . "\n[" . $v2['SEND_DATE'] . "]";
                        echo '	<tr>
				<td>' . ($k2 + 1) . '</td>
				<td>' . $v2['ALIAS_NAME'] . '</td>
				<td>' . $v2['SERVICE_ID'] . '</td>
				<td>' . $v2['SERVICE_NAME'] . '</td>
				<td>' . $v2['HOST_NAME'] . '</td>
				<td>' . $v2['IP_ADDRESS'] . '</td>
				<td>' . $v2['PORT'] . '</td>
				<td>' . $v2['VLAN_ID'] . '</td>
				<!--td><a class="btn default btn-mod_ticket" data-toggle="modal" data-layanan="WIFI" data-id="' . $v2['ID'] . '">' . $arr_status[$v2['STATUS_GGN']] . '</a></td-->
				<td style="text-align:center;">' . ($v2['STATUS_GGN'] == 0 ? '<a href="#modal_edit" class="btn default" data-toggle="modal" data-layanan="' . $v . '" data-id="' . $v2['ID'] . '" onclick="getRowTicket(this);">' . $arr_status[$v2['STATUS_GGN']] . '</a>' : "<pre>" . $closed . "</pre>") . '</td>
				<td>' . $v2['CREATED_DATE'] . '</td>
			</tr>';
                    }
                } else {
                    echo '<tr>';
                    echo '	<td colspan="10">no data available.</td>';
                    echo '</tr>';
                }
                echo '		</tbody>
		</table>';
                echo '					</div>
						</div>
					</div>
				</div>
			</div>
			</div>
			</div>
	';
            }
        } else {
//$serosert = $mdl_monitor->get_kaa_serosertabbr();
            $neighbor = $mdl_monitor->get_kaa_neighbor();
//Zend_Debug::dump($neighbor); die();

            echo '
	<div class="row">
		<div class="col-md-12">
			<div class="portlet">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-reorder"></i> AP TRAFFIC
					</div>
					<div class="tools">
						<a href="" class="collapse"></a>
					</div>
				</div>
				<div class="portlet-body" id="body_aptraffic">';

            echo '			</div>
			</div>
		</div>
	</div>';

            echo '
	<div class="row">
		<div class="col-md-12">
			<div class="portlet">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-reorder"></i> ROUTER TRAFFIC
					</div>
					<div class="tools">
						<a href="" class="collapse"></a>
					</div>
				</div>
				<div class="portlet-body">';

            echo '
<div class="row">';

            echo '
<div class="col-md-4">
	<div class="row">
		<div class="col-md-12">
			<div class="portlet box grey">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-reorder"></i> FILTER
					</div>
					<div class="tools">
						<a href="" class="collapse"></a>
					</div>
				</div>
				<div class="portlet-body form">
					<form role="form" id="form_router">
						<div class="form-body">
							<!--div class="form-group">
								<label>Text</label>
								<input class="form-control" id="exampleInputEmail1" placeholder="Enter text">
								<span class="help-block"></span>
							</div-->
							<div class="form-group">
								<label>FLAG</label>
								<select class="form-control" id="flag" name="flag">
									<option value="">MASIV</option>
									<option value="M">MEDIA CENTER</option>
									<option value="A">AKOMODASI</option>
									<option value="S">SECURITY</option>
									<option value="I">INTERNATIONAL AIRPORT</option>
									<option value="V">VENUE</option>
								</select>
								<span class="help-block"></span>
							</div>
							<div class="form-group">
								<label>LOCATION</label>
								<select class="form-control" id="loc_id" name="loc_id" onchange="changeSubLocID_router();">';
            $mdl_monitorwifi = new Model_Monitorwifi();

            $location = $mdl_monitorwifi->getLocations();

            $arr_loc = array();
            foreach ($location as $k => $v) {
                $arr_loc[$v["FLAGS"]][] = $v;
            }
            // Zend_Debug::dump($arr_loc);die();
            echo '<option value="">All Location</option>';
            foreach ($arr_loc as $k => $v) {
                echo '<optgroup label="' . $k . '"></optgroup>';
                foreach ($v as $k2 => $v2) {
                    echo '<option value="' . $v2["LOC_ID"] . '">' . $v2["LOC_NAME"] . '</option>';
                }
            }
            echo '						</select>
								<span class="help-block"></span>
							</div>
							<div class="form-group">
								<label>Sub Location:</label>
								<select class="form-control select2me" id="sub_loc_id" name="sub_loc_id_router"" placeholder="Select Sub Location...">
									<option value="">All Sub Location</option>
								</select>
								<span class="help-block"></span>
							</div>
							<div class="form-group">
								<label>Date Range</label>
											<div class="input-group" id="defaultrange">
												<input type="text" class="form-control" id="sss" name="sss">
												<span class="input-group-btn">
													<button class="btn default date-range-toggle" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
								<!-- /input-group -->
								<span class="help-block">
									Select date range
								</span>
							</div>
							<div class="form-group">
								<label>Device</label>
								<select onchange="requestData2(this)" class="form-control" id="device" name="device">';
            foreach ($neighbor as $k => $v) {
                $tmp = explode(".", $v['snmp_index']);
                $type = 0;
                if (count($tmp) > 1) {
                    $type = 1;
                }
                $rrd = str_replace("<path_rra>", "", $v['data_rrd']);
                echo '<option value="' . $rrd . '|' . $type . '|' . $v['id'] . '|' . $v['neighbor'] . ' ' . $v['port'] . ' [' . $v['sero_serv_id'] . ']" data-type="' . $type . '" data-id="' . $v['id'] . '" data-title="' . $v['neighbor'] . ' ' . $v['port'] . ' [' . $v['sero_serv_id'] . ']">' . $v['neighbor'] . ' ' . $v['port'] . ' [' . $v['sero_serv_id'] . ']</option>';
            }
            echo '								</select>
								<span class="help-block"></span>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
';

            echo '
	<div class="col-md-8">
		<div class="portlet box grey">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-chart"></i> TRAFFIC CONSUMPTION GRAPH
				</div>
				<div class="tools">
					<a href="" class="collapse"></a>
				</div>
			</div>
			<div class="portlet-body" id="contain">
				<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>		
			</div>
		</div>
	</div>';
            echo '
</div>
';
            echo '
</div>
</div>
</div>
</div>
';

            echo '<script>
initchart();

function getbody_aptraffic(){
	$.get(
		"/monitorwifi/traffic",
		function(data){
			$("#body_aptraffic").html(data);
		},
		"html");
}

getbody_aptraffic();

</script>';
        }

        die();
    }

    public function getlistloceventAction() {

        $params = $this->getRequest()->getParams();
// Zend_Debug::dump($params);die();

        $mdl_monitor = new Model_Monitor();
        $tmp_data = $mdl_monitor->getListLocEvent($params['event_id']);

        $arr_masiv = array(
            "M" => "edia Center",
            "A" => "komodasi",
            "S" => "ecurity",
            "I" => "nternational Airport",
            "V" => "enue",
        );

        $data = array();
        foreach ($tmp_data as $k => $v) {
            $data[$v['FLAG'] . $arr_masiv[$v['FLAG']]][] = $v;
        }
// Zend_Debug::dump($data);die();
        echo '<option value="">Select Location...</option>';
        foreach ($data as $k => $v) {
            echo '<optgroup label="' . strtoupper($k) . '">';
            foreach ($v as $k2 => $v2) {
                echo '<option value="' . $v2['VALUE'] . '" ' . ($params['loc_id'] == $v2['VALUE'] ? 'selected' : '') . '>' . $v2['DISPLAY'] . '</option>';
            }
            echo '</optgroup>';
        }

        die();
    }

    public function getlistsubloceventAction() {

        $params = $this->getRequest()->getParams();
// Zend_Debug::dump($params);die();

        $mdl_monitor = new Model_Monitor();
        $tmp_data = $mdl_monitor->getSubLocation($params);
// Zend_Debug::dump($tmp_data);//die();

        echo '<option value="">All Sub Location</option>';
        foreach ($tmp_data as $k => $v) {
            echo '<option value="' . $v['SUB_LOC_ID'] . '" ' . ($params['SUB_LOC_ID'] == $v['SUB_LOC_ID'] ? 'selected' : '') . '>' . $v['SUB_LOC_NAME'] . '</option>';
        }

        die();
    }

    public function detailcontentAction() {

        $params = $this->getRequest()->getParams();
// Zend_Debug::dump($params);die();

        $mdl_monitor = new Model_Monitor();

        $tmp_sublocation = $mdl_monitor->getSubLocation($params);
// Zend_Debug::dump($tmp_sublocation);die();

        $tmp_data_perangkat = $mdl_monitor->getListDeviceByService($params);
// Zend_Debug::dump($tmp_data_perangkat);die();
        $data_perangkat = array();
        foreach ($tmp_data_perangkat as $k => $v) {
            $data_perangkat[$v['SUB_LOC_NAME']][$v['SERVICE_TYPE']][] = $v;
        }
// Zend_Debug::dump($data_perangkat);die();
        // Zend_Debug::dump(count($data_wifi['BANDARA SOEKARNO-HATTA TERMINAL 2D']));die();

        $data = array();
        foreach ($tmp_sublocation as $k => $v) {
            // Zend_Debug::dump(count($data_perangkat[$v['SUB_LOC_NAME']]));
            // $data_perangkat = array();
            if ($params['service'] != '') {
                $data[$v['SUB_LOC_NAME']][strtoupper($params['service'])] = array();
            } else {
                $data[$v['SUB_LOC_NAME']]['WIFI'] = array();
                $data[$v['SUB_LOC_NAME']]['ASTINET'] = array();
                $data[$v['SUB_LOC_NAME']]['CCTV'] = array();
            }

            if (count($data_perangkat[$v['SUB_LOC_NAME']]) > 0) {
                foreach ($data_perangkat[$v['SUB_LOC_NAME']] as $k2 => $v2) {
                    // Zend_Debug::dump($k2);
                    $data[$v['SUB_LOC_NAME']][$k2] = $v2;
                }
            }
        }
// die();
// Zend_Debug::dump($data);die();


        $arr_status = array(
            "5" => "UP",
            "1" => "DOWN",
        );

        $arr_color_status = array(
            "5" => "bgreen_fgreen",
            "1" => "bred_fred",
        );

        $arr_location_icon = array(
            "WIFI" => "ACCESS POINT/access",
            "ASTINET" => "SQUARE/square",
            "CCTV" => "CCTV/cctv",
        );

        $html = "";

        foreach ($data as $k => $v) {
// Zend_Debug::dump($v);die();
            $html .='<div class="row">';
            $html .='	<div class="col-md-offset-4 col-md-4">';
            $html .='		<div class="row corner-all text-center" style="background-color:#404040;color:white;padding:5px;text-align:center;vertical-align:middle;margin-bottom:10px;">';
// $html .='		<span class="bold" style="font-size:16px;">SUB LOCATION: </span>';
            $html .='			<span class="bold gold" style="font-size:16px;">' . $k . '</span>';
            $html .='		</div>';
            $html .='	</div>';
            $html .='</div>';

            foreach ($v as $k2 => $v2) {
                // Zend_Debug::dump($k2);die();
                $html .='<div class="row text-center">';
                $html .='	<div class="col-md-offset-4 col-md-4" style="padding:15px;">';
                $html .='		<span class="bold" style="font-size:16px;">' . strtoupper($k2) . '</span>';
                $html .='	</div>';
                $html .='</div>';

                $html .='<div class="row text-center">';

                if (count($v2) > 0) {
                    foreach ($v2 as $k3 => $v3) {
                        // Zend_Debug::dump($k3);die();



                        $html .='	<div class="" style="display:inline-block; width:150px;font-size:10px;text-align:center">';
                        $html .='		<a style="display:inline-block; margin:5px 5px;text-decoration:none;text-align:center;" href="javascript:;" class="square1" title="' . $v3['NAMA_PERANGKAT'] . '">';
                        $html .='			<img src="/images/ICON/' . $arr_location_icon[$k2] . '_' . $arr_color_status[$v3['STATUS']] . '.png" style="width:40px;height:40px;"><br>';
                        $html .='			<span class="bold grey">' . $v3['NAMA_PERANGKAT'] . '</span><br>';
                        $html .='			<span class="bold blue" style="margin-left:0px">' . $arr_status[$v3['STATUS']] . '</span>';
                        $html .='		</a>';
                        $html .='	</div>';
                    }
                } else {
                    $html .='	<div class="" style="display:block; width:100%;font-size:10px;font-weight:bold;text-align:center">';
                    $html .='		this Location doesn\'t have ' . $k2 . ' data.';
                    $html .='	</div>';
                }

                $html .='</div>';
                $html .='<hr>';
            }


            $html .='</div>';
        }

        echo $html;




        die();
    }

    public function getmodalperangkatAction() {

        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
// Zend_Debug::dump($params);die();

        $mdl_monitor = new Model_Monitor();
        $tmp_data = $mdl_monitor->getPerangkat($params['data']);
        $tmp_chart = $mdl_monitor->getUserOnlineTraffic($params['data']);
// Zend_Debug::dump($tmp_chart);die();

        $chart = array();
        $data = array();

        $data_client = "";
        $data_traffic = "";

        foreach ($tmp_chart as $k => $v) {
            $per = explode('-', $v['PERIODE']);
            $periode .= "'" . $per[1] . ":00',";
            $chart['xaxis'] = rtrim($periode, ',');
            $chart['periode'] = $v['DATE_ID'];

            $data_client .= $v['CLIENTCOUNT'] . ',';
            $data_traffic .= round(($v['TRAFFIC'] / 1024 / 1024), 2) . ',';
        }
// Zend_Debug::dump($data_client);die();

        $chart['series'] = array(
            array("name" => "UserOnline", "data" => rtrim($data_client, ',')),
            array("name" => "Traffic", "data" => rtrim($data_traffic, ',')),
        );
// Zend_Debug::dump($chart);die();

        $arr_color_status = array(
            "5" => "bwhite_fgreen",
            "4" => "bwhite_fgrey",
            "3" => "bwhite_fgreen",
            "1" => "bwhite_fred",
            "0" => "bwhite_fgrey",
            "" => "bwhite_fgrey",
        );

        $arr_valSuff = array("UserOnline", "MByte");


// $arr_color_status = array(
        // "0"=>"bwhite_fred",
        // "1"=>"bwhite_fred",
        // "3"=>"bwhite_fgreen",
        // "5"=>"bwhite_fgreen",
// );

        $html = '';
        if ($params['data']['service_type'] == 'WIFI') {
            $html .='<div class="row">';
            $html .='	<div class="col-md-12">';
            $html .='		<div class="" style="width: 100%; max-width: 100%; display:block">
						<div class="text-center" style="position: relative;">
							<p style="color:#000; font-weight:bold; margin-bottom:0px;">UserOnline: ' . $tmp_data['CLIENTCOUNT'] . '</p>
							<img src="/images/ICON/ACCESS POINT/access_' . $arr_color_status[$tmp_data['STATUS']] . '.png" style="width: 64px">
						</div>
						<div class="text-center" style="font-weight: bold;margin-bottom: 5px; font-size: 11px; color: #000;">
							NAME : <span style="color: #5CBDE0;">' . $tmp_data['NMS_NAME'] . '</span><br>
							ETHERNETMAC : <span style="color: #5CBDE0;">' . $tmp_data['MACADDRESS'] . '</span><br>
							IPADDRESS : <span style="color: #5CBDE0;">' . $tmp_data['IPADDRESS'] . '</span><br>
							AP LOCATION : <span style="color: #5CBDE0;">' . $tmp_data['AP_LOCATION'] . '</span><br>
							LOCATION : <span style="color: #5CBDE0;">' . $tmp_data['LOC_NAME'] . '</span><br>
							ROOM : <span style="color: #5CBDE0;">' . $tmp_data['SUB_LOC_NAME'] . '</span><br>
						</div>
					</div>';
            $html .='	</div>';
            $html .='</div>';

            $html .='<hr>';


            $html .='<div class="row">';
            $html .='	<div class="col-md-12">';
            $html .='		<div id="container" style="min-width:310px; height: 200px; margin: 0 auto;"></div>';
            $html .='	</div>';
            $html .='</div>';

            $html .='<script>';
            $html .="$('#container').highcharts({
		chart:{
			type:'spline',
			zoomType: 'x,y'
		},
        title: {
            text: 'Traffic & UserOnline',
            // x: -20 //center
        },
		credits: {
			text: ''
		},
        subtitle: {
            text: '" . date('D, d F Y', strtotime($chart['periode'])) . "',
            // x: -20
        },

		tooltip: {
			shared: true,
			crosshairs: true
		},
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        xAxis: {
            categories: [" . $chart['xaxis'] . "]
        },
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value} MByte', 
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: 'Traffic',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }, { // Secondary yAxis
            title: {
                text: 'UserOnline',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            opposite: true
        }],
        // tooltip: {
            // valueSuffix: ''
        // },
        legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom',
            borderWidth: 0
        },
        series: [";

            foreach ($chart['series'] as $k => $v) {
                // if($k==0){
                $html .="{
					name: '" . $v['name'] . "',
					data: [" . $v['data'] . "],
					tooltip: {
						valueSuffix: ' " . $arr_valSuff[$k] . "'
					},
					";


                if ($k == "0") {
                    $html .="yAxis: 1,";
                }
                if ($k == "0") {
                    $html .="type: 'column',";
                }

                $html .="
				},";
                // }
            }

            $html .="]
    });";
            $html .='</script>';
        } else if ($params['data']['service_type'] == 'TELKOMSEL') {
            $html .='<div class="row">';
            $html .='	<div class="col-md-12">';
            $html .='		<div class="" style="width: 100%; max-width: 100%; display:block">
						<div class="text-center" style="position: relative;">
							<img src="/images/ICON/TELKOMSEL/tsel_' . $arr_color_status[$tmp_data['STATUS']] . '.png" style="width: 64px">
						</div>
						<div class="text-center" style="font-weight: bold;margin-bottom: 5px; font-size: 11px; color: #000;">
							TITLE : <span style="color: #5CBDE0;">' . $tmp_data['SERVICE_NAME'] . '</span><br>
							SID : <span style="color: #5CBDE0;">' . $tmp_data['SERVICE_ID'] . '</span><br>
							ALIAS : <span style="color: #5CBDE0;">' . $tmp_data['HOST_NAME_ALIAS'] . '</span><br>
							HOSTNAME : <span style="color: #5CBDE0;">' . $tmp_data['HOST_NAME'] . '</span><br>
							PORT : <span style="color: #5CBDE0;">' . $tmp_data['PORT'] . '</span><br>
							IP ADDRESS : <span style="color: #5CBDE0;">' . $tmp_data['IP_ADDRESS'] . '</span><br>
						</div>
					</div>';
            $html .='	</div>';
            $html .='</div>';
        } else {
            $html .='detail perangkat.';
        }



// $html = "sss";

        echo $html;
// Zend_Debug::dump($params);die();
        die();
    }

    public function insertchatAction() {

        $params = $this->getRequest()->getParams();
// Zend_Debug::dump($_FILES);die();	

        $filenames = "";
        if ($_FILES) {
            // die('sss');
            $swap = "";
            define("MAX_SIZE", "600");
            $errors = 0;
            $image = $_FILES["image"]["name"];

            //die($image);
            $uploadedfile = $_FILES['image']['tmp_name'];

            $filename = stripslashes($_FILES['image']['name']);
            $filenames = stripslashes($_FILES['image']['name']);
            $extension = pathinfo($filename, PATHINFO_EXTENSION);
            $extension = strtolower($extension);

            // Zend_Debug::dump($extension);die();

            if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) {
                $swap = 'Unknown Image extension';
                $errors = 1;
            } else {
                // Zend_Debug::dump($errors);die();
                $size = filesize($_FILES['image']['tmp_name']);
                $size = $size / 1024;
                // Zend_Debug::dump($size);die();
                if ($size > MAX_SIZE * 1024) {
                    // if ($size > 1024){
                    $errors = 1;
                }
                // Zend_Debug::dump($errors);die();

                if ($extension == "jpg" || $extension == "jpeg") {
                    $uploadedfile = $_FILES['image']['tmp_name'];
                    $src = imagecreatefromjpeg($uploadedfile);
                } else if ($extension == "png") {
                    $uploadedfile = $_FILES['image']['tmp_name'];
                    $src = imagecreatefrompng($uploadedfile);
                } else {
                    $src = imagecreatefromgif($uploadedfile);
                }

                list($width, $height) = getimagesize($uploadedfile);

                $newwidth = 300;
                $newheight = ($height / $width) * $newwidth;
                $tmp = imagecreatetruecolor($newwidth, $newheight);
                imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

                $filename = constant('APPLICATION_PATH') . '/../public/uploads/kaa2015/' . $_FILES['image']['name'];
                // Zend_Debubg::dump($filename);die();
                //$filename = "/var/www/zendrafinaru/public/uploads/images/". $_FILES[0]['name'];

                imagejpeg($tmp, $filename, 100);
                imagedestroy($src);
                imagedestroy($tmp);
            }
        }

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $params['filename'] = $filenames;
        $params['uname'] = $identity->uname;
        $params['fullname'] = $identity->fullname;


        $mdl_monitor = new Model_Monitor();

        $ins = $mdl_monitor->ins_chats($params);



        die();
    }

    public function insertcommentAction() {

        $params = $this->getRequest()->getParams();
// Zend_Debug::dump($_FILES);die();	

        $filenames = "";
        if ($_FILES) {
            // die('sss');
            $swap = "";
            define("MAX_SIZE", "600");
            $errors = 0;
            $image = $_FILES["image"]["name"];

            //die($image);
            $uploadedfile = $_FILES['image']['tmp_name'];

            $filename = stripslashes($_FILES['image']['name']);
            $filenames = stripslashes($_FILES['image']['name']);
            $extension = pathinfo($filename, PATHINFO_EXTENSION);
            $extension = strtolower($extension);

            // Zend_Debug::dump($extension);die();

            if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) {
                $swap = 'Unknown Image extension';
                $errors = 1;
            } else {
                // Zend_Debug::dump($errors);die();
                $size = filesize($_FILES['image']['tmp_name']);
                $size = $size / 1024;
                // Zend_Debug::dump($size);die();
                if ($size > MAX_SIZE * 1024) {
                    // if ($size > 1024){
                    $errors = 1;
                }
                // Zend_Debug::dump($errors);die();

                if ($extension == "jpg" || $extension == "jpeg") {
                    $uploadedfile = $_FILES['image']['tmp_name'];
                    $src = imagecreatefromjpeg($uploadedfile);
                } else if ($extension == "png") {
                    $uploadedfile = $_FILES['image']['tmp_name'];
                    $src = imagecreatefrompng($uploadedfile);
                } else {
                    $src = imagecreatefromgif($uploadedfile);
                }

                list($width, $height) = getimagesize($uploadedfile);

                $newwidth = 300;
                $newheight = ($height / $width) * $newwidth;
                $tmp = imagecreatetruecolor($newwidth, $newheight);
                imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

                $filename = constant('APPLICATION_PATH') . '/../public/uploads/kaa2015/' . $_FILES['image']['name'];
                // Zend_Debubg::dump($filename);die();
                //$filename = "/var/www/zendrafinaru/public/uploads/images/". $_FILES[0]['name'];

                imagejpeg($tmp, $filename, 100);
                imagedestroy($src);
                imagedestroy($tmp);
            }
        }

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $params['filename'] = $filenames;
        $params['uname'] = $identity->uname;
        $params['fullname'] = $identity->fullname;


        $mdl_monitor = new Model_Monitor();

        $ins = $mdl_monitor->ins_comment($params);



        die();
    }

    public function deletechatAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $params = $this->getRequest()->getParams();
// Zend_Debug::dump($params);die();	

        $mdl_monitor = new Model_Monitor();

        $del = $mdl_monitor->del_chats($params);
// Zend_Debug::dump($ins);



        die();
    }

    public function testAction() {

        $params = $this->getRequest()->getParams();
// Zend_Debug::dump($params);die();	

        $filenames = "";
        if ($_FILES) {
            // die('sss');

            $filename = stripslashes($_FILES['image']['name']);
            $filenames = stripslashes($_FILES['image']['name']);
            $extension = pathinfo($filename, PATHINFO_EXTENSION);
            $extension = strtolower($extension);

            if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) {
                $swap = 'Unknown Image extension';
                $errors = 1;
            } else {
                $size = filesize($_FILES['image']['tmp_name']);
                // echo MAX_SIZE;
                // Zend_Debug::dump($size);die();
                if ($size > MAX_SIZE * 1024) {
                    $errors = 1;
                }

                // Zend_Debug::dump($extension);die();

                if ($extension == "jpg" || $extension == "jpeg") {
                    $uploadedfile = $_FILES['image']['tmp_name'];
                    $src = imagecreatefromjpeg($uploadedfile);
                } else if ($extension == "png") {
                    $uploadedfile = $_FILES['image']['tmp_name'];
                    $src = imagecreatefrompng($uploadedfile);
                } else {
                    $src = imagecreatefromgif($uploadedfile);
                }
                // Zend_Debug::dump($src);

                list($width, $height) = getimagesize($uploadedfile);

                $newwidth = 300;
                $newheight = ($height / $width) * $newwidth;
                $tmp = imagecreatetruecolor($newwidth, $newheight);
                imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

                $filename = constant('APPLICATION_PATH') . '/../public/uploads/kaa2015/' . $_FILES[0]['name'];
                // Zend_Debug::dump($filename);die();
                //$filename = "/var/www/zendrafinaru/public/uploads/images/". $_FILES[0]['name'];

                imagejpeg($tmp, $filename, 100);
                imagedestroy($src);
                imagedestroy($tmp);
            }
        }

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $params['filename'] = $filenames;


        $mdl_monitor = new Model_Monitor();

        $ins = $mdl_monitor->ins_chats($identity, $params);



        die();
    }

    public function gettabticket2Action() {

        $params = $this->getRequest()->getParams();
// Zend_Debug::dump($params);die();

        $mdl_monitor = new Model_Monitor();

        $tmp_data = $mdl_monitor->getListLocEvent($params['event_id']);
// Zend_Debug::dump($tmp_data);die();

        $arr_masiv = array(
            "M" => "edia Center",
            "A" => "komodasi",
            "S" => "ecurity",
            "I" => "nternational Airport",
            "V" => "enue",
        );

        $list_location = array();
        foreach ($tmp_data as $k => $v) {
            $list_location[$v['FLAG'] . $arr_masiv[$v['FLAG']]][] = $v;
        }
// Zend_Debug::dump($list_location);die();

        echo '<div class="tab-pane active">';
        if ($params["tticket"] == 'create') {

            echo '<div class="alert alert-success alert-dismissable hidden" id="alertinfo">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
				<span id="alertmsg"><strong>Warning!</strong> Something went wrong. Please check.</span>
			</div>
			
			<div style="margin:0px auto;text-align:center" id="processbar" class="hidden">
				<div class="progress progress-striped active" style="margin:0px auto;text-align:center">
					<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
						<span class="sr-only">
							On Process
						</span>
					</div>
				</div>
				<span style="margin:0px auto;">Please wait...processing request</span>
				<br>
			</div>';

            echo '<div class="row">';
            echo '	<div class="col-md-12">';
            echo '			<form class="form-horizontal" role="form" id="form_ticket">
						<div class="form-body">
							<div class="form-group">
								<label class="col-md-2 control-label">Location:</label>
								<div class="col-md-4">
									<select class="form-control" id="loc" name="loc">
										<option value="">Select Location...</option>';
            foreach ($list_location as $k => $v) {
                echo '<optgroup label="' . $k . '"></optgroup>';
                foreach ($v as $k2 => $v2) {
                    echo '<option value="' . $v2['VALUE'] . '">' . $v2['DISPLAY'] . '</option>';
                }
            }
            echo '							</select>
									</select>
									<span class="help-block"></span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Layanan:</label>
								<div class="col-md-4">
									<select class="form-control" id="layanan" name="layanan">
										<option value="">Select Layanan...</option>
										<option value="WIFI">WIFI</option>
										<option value="TELKOMSEL">TELKOMSEL</option>
										<option value="DATIN">DATIN</option>
										<option value="CCTV">CCTV</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Prioritas:</label>
								<div class="col-md-4">
									<select class="form-control" id="prioritas" name="prioritas">
										<option value="1">URGENT</option>
										<option value="2">MIDDLE</option>
										<option value="3">EASY</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Description:</label>
								<div class="col-md-10">
									<textarea class="form-control" id="description" name="description" rows="3"></textarea>
								</div>
							</div>
						</div>
						<div class="form-actions fluid">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-offset-2 col-md-10">
										<button type="button" class="btn green" onclick="insertTicket();"><i class="fa fa-check"></i> Submit</button>
										<!--button type="button" class="btn default">Cancel</button-->
									</div>
								</div>
							</div>
						</div>
					</form>
		';
            echo '	</div>';
            echo '</div>';
        } else if ($params["tticket"] == 'inbox') {

            $data = $mdl_monitor->getInbox();

            echo '<div class="row">';
            echo '<div class="col-md-12">';
            echo '	<div class="table-responsive">
				<table class="table table-bordered table-striped table-condensed" style="font-size:11px;">
					<thead>
						<tr>
							<th style="width:30px;">#</th>
							<th>LOCATION</th>
							<th>LAYANAN</th>
							<th>PRIORITAS</th>
							<th>STATUS</th>
							<th>DESCRIPTION</th>
							<th>CREATED_DATE</th>
							<!--th style="width:60px;"></th-->
						</tr>
					</thead>
					<tbody>';
            if (count($data) > 0) {

                $arr_prioratas = array(
                    "1" => "EASY",
                    "2" => "MIDDLE",
                    "3" => "URGENT",
                );

                $arr_status = array(
                    "1" => "OPEN",
                    "2" => "INPROGRESS",
                    "3" => "CLOSED",
                );

                foreach ($data as $k => $v) {
                    echo '			<tr>
								<td>' . ($k + 1) . '</td>
								<td>' . $v['ALIAS_NAME'] . '</td>
								<td>' . $v['LAYANAN'] . '</td>
								<td>' . $arr_prioratas[$v['PRIORITAS']] . '</td>
								<td>' . $arr_status[$v['STATUS']] . '</td>
								<td><pre>' . $v['DESCRIPTION'] . '</pre></td>
								<td>' . $v['CREATED_DATE'] . '</td>
								<!--td><a class="btn yellow btn-xs" href="#modal_update_tiket" data-toggle="modal" data-id="' . $v['ID'] . '" data-prioritas="' . $v['PRIORITAS'] . '" data-status="' . $v['STATUS'] . '" onclick="getModalUpdTicket(this);" ><i class=" fa fa-pencil"></i> Update</a></td-->
								<!--td></td-->
							</tr>
			';
                }
            } else {
                echo '				<tr>
								<td colspan="8">no data available.</td>
							</tr>
		';
            }

            echo '			</tbody>
				</table>
			</div>';
            echo '</div>';
            echo '</div>';
        } else {
            echo 'no data available.';
        }
        echo '</div>';





        die();
    }

    public function gettabticketAction() {

        $params = $this->getRequest()->getParams();
// Zend_Debug::dump($params);die();

        $mdl_monitor = new Model_Monitor();

        $tmp_data_ap = $mdl_monitor->getInboxTicketAP($params['event_id']);
// Zend_Debug::dump($tmp_data);die();

        echo '<div class="row">';
        echo '<div class="col-md-12">';
        echo '	<div class="portlet box grey">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-reorder"></i> Default Form
					</div>
					<div class="tools">
						<a href="" class="collapse"></a>
						<a href="#portlet-config" data-toggle="modal" class="config"></a>
						<a href="" class="reload"></a>
						<a href="" class="remove"></a>
					</div>
				</div>
				<div class="portlet-body">';
        echo '			<div class="table-responsive">
						<table class="table table-bordered table-striped table-condensed" style="font-size:11px;">
							<thead>
								<tr>
									<th style="width:30px;">#</th>
									<th>LOCATION</th>
									<th>LAYANAN</th>
									<th>PRIORITAS</th>
									<th>STATUS</th>
									<th>DESCRIPTION</th>
									<th>CREATED_DATE</th>
									<!--th style="width:60px;"></th-->
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>';
        echo '	</div>';
        echo '</div>';





        die();
    }

    public function getmodalpicAction() {

        $params = $this->getRequest()->getParams();
// Zend_Debug::dump($params);die();

        $mdl_monitor = new Model_Monitor();

        $data = $mdl_monitor->getPICLokasi($params['data']);

        echo '<div class="row">';
        echo '<div class="col-md-12">';
        echo '	<div class="portlet-body">
		<div class="table-responsive">
			<table class="table table-bordered table-striped table-condensed" style="font-size:11px; margin-bottom:0px;">
				<thead>
				<tr>
					<th>#</th>
					<th>NIK</th>
					<th>NAMA</th>
					<th>JABATAN</th>
					<th>UNIT</th>
					<th>NO HP</th>
					<th>JADWAL</th>
				</tr>
				</thead>
				<tbody>';
        foreach ($data as $k => $v) {
            echo '		<tr>
					<td>' . ($k + 1) . '</td>
					<td>' . (strtoupper($v['NIK']) != '' ? str_replace(strtoupper($params['data']['filter']), '<b style="color:red;">' . strtoupper($params['data']['filter']) . '</b>', strtoupper($v['NIK'])) : '-') . '</td>
					<td>' . (strtoupper($v['NAMA']) != '' ? str_replace(strtoupper($params['data']['filter']), '<b style="color:red;">' . strtoupper($params['data']['filter']) . '</b>', strtoupper($v['NAMA'])) : '-') . '</td>
					<td>' . ($v['JABATAN'] != '' ? $v['JABATAN'] : '-') . '</td>
					<td>' . $v['UNIT'] . '</td>
					<td>' . ($v['NO_HP'] != '' ? $v['NO_HP'] : '-') . '</td>
					<td>' . $v['JADWAL'] . '</td>
				</tr>
	';
        }
        echo '			</tbody>
			</table>
		</div>
		</div>
	 ';
        echo '</div>';
        echo '</div>';

        die();
    }

    public function insertticketAction() {

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $params = $this->getRequest()->getParams();
// Zend_Debug::dump($_POST);die();

        $mdl_monitor = new Model_Monitor();

        $ins = $mdl_monitor->insert_ticket($_POST, $identity->uid);
        if ($ins['result']) {

            $result = array(
                'retCode' => '11',
                'retMsg' => $ins['message'],
                'result' => true,
                'data' => null
            );
        } else {

            $result = array(
                'retCode' => '00',
                'retMsg' => $ins['message'],
                'result' => false,
                'data' => null
            );
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function getmodalupdateticketAction() {

        $params = $this->getRequest()->getParams();
// Zend_Debug::dump($params);die();

        echo '<div class="row">
	<div class="col-md-12">
		<form class="form-horizontal" role="form" id="form_ticket">
			<div class="form-body">
				<div class="form-group">
					<label class="col-md-2 control-label">Prioritas:</label>
					<div class="col-md-4">
						<select class="form-control" id="prioritas" name="prioritas">
							<option value="1">URGENT</option>
							<option value="2">MIDDLE</option>
							<option value="3">EASY</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">Status:</label>
					<div class="col-md-4">
						<select class="form-control" id="status" name="status">
							<option value="1">URGENT</option>
							<option value="2">MIDDLE</option>
							<option value="3">EASY</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">Description:</label>
					<div class="col-md-10">
						<textarea class="form-control" id="description" name="description" rows="3"></textarea>
					</div>
				</div>
			</div>
			<div class="form-actions fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-offset-2 col-md-10">
							<button type="button" class="btn green" onclick="insertTicket();"><i class="fa fa-check"></i> Submit</button>
							<!--button type="button" class="btn default">Cancel</button-->
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>';



        die();
    }

    public function getjadwalposkoAction() {

        $params = $this->getRequest()->getParams();
// Zend_Debug::dump($params);die();


        $mdl_monitor = new Model_Monitor();

        $tanggal = $mdl_monitor->getTanggalDanLokasiPosko($params['nikname']);
// Zend_Debug::dump($tanggal);die();

        $data = array();
        $jml = 0;
        foreach ($tanggal as $k => $v) {
            $data[$v['TGL_POSKO']]['LOKASI'][] = $v['LOKASI'];
            $data[$v['TGL_POSKO']]['KOTA'][] = $v['KOTA'];
            $data[$v['TGL_POSKO']]['PIC'][] = $v['JML'];

            // $jml += $v['JML'];
            // $data[$v['TGL_POSKO']]['TOTAL'] = $jml;
        }
// Zend_Debug::dump($data['20150418']);die();

        echo '<div class="row">';
        echo '	<div class="col-md-12">';
        echo '		<div class="table-responsive">';
        echo '		<table class="table table-bordered table-striped table-condensed" style="font-size:11px;">
				<thead>
				<tr>
					<th style="width:20px;">#</th>
					<th>LOKASI</th>
					<th style="width:100px;">KOTA</th>
					<th style="width:30px;"></th>
				</tr>
				</thead>
				<tbody>';

        $today = date("Ymd");
// $today = '20150419';

        if ($data) {
            $no = 1;
            foreach ($data as $k => $v) {
                // Zend_Debug::dump($v);
                echo '			<tr class="bg_grey ' . ($today == $k ? 'today' : 'not_today') . '" style="cursor:pointer;" data-td_id="td_' . $k . '" onclick="colex(this)">
							<td colspan="4" style="background-color:#D3D3D3; ' . ($today == $k ? 'color:red; font-size:14px;' : 'color:black;') . '; font-weight:bold;"><span>-</span> ' . date('l, d M Y', strtotime($k)) . ' [' . count($v['LOKASI']) . ' Location]</td>
						</tr>';

                // $count = count($v);
                // Zend_Debug::dump($v);

                $arr_kota = array(
                    "Posko Bandung Lembong" => "Bandung",
                    "Posko Bandung On Site" => "Bandung",
                    "Posko Bandung" => "Bandung",
                    "Posko JCC" => "Jakarta",
                    "Istora Senayan" => "Jakarta",
                    "Posko Utama" => "Jakarta",
                    "Airport SOETTA" => "Jakarta",
                    "Hotel Sultan" => "Jakarta",
                );
                foreach ($v as $k2 => $v2) {
                    $count = count($v2);
                }

                for ($i = 0; $i < $count; $i++) {
                    echo '	<tr>';
                    echo '		<td>' . ($i + 1) . '</td>';
                    echo '		<td>' . $v['LOKASI'][$i] . ' [' . $v['PIC'][$i] . ' PIC]</td>';
                    echo '		<td>' . $arr_kota[$v['LOKASI'][$i]] . '</td>';
                    echo '		<td style="text-align:center;">
							<a class="btn blue btn-xs fa fa-info-circle" href="#modal_pic" data-toggle="modal" data-format_tanggal="' . date('l, d M Y', strtotime($k)) . '" data-tanggal="' . $k . '" data-lokasi="' . $v['LOKASI'][$i] . '" data-kota="' . $v['KOTA'][$i] . '" data-filter="' . $params['nikname'] . '" onclick="getModalPIC(this);"></a>
						</td>';
                    echo '	</tr>';
                }
                $no++;
            }
        } else {
            echo '<tr>';
            echo '	<td colspan="4">no data available.</td>';
            echo '</tr>';
        }

        ';</tbody>
			</table>
	';
        echo '		</div>';
        echo '	</div>';
        echo '</div>';

        echo'<script>';
        if (date('Ymd')) {
            echo'$(".not_today").click();';
        }
        echo'function colex(me){
		$(me).find("span").text(function(_, value){return value=="-"?"+":"-"});
		$(me).nextUntil("tr.bg_grey").slideToggle(100, function(){
		});
	}';
        echo'</script>';



        die();
    }

    public function getcommentsAction() {

        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
// Zend_Debug::dump($params);die();

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $mdl_monitor = new Model_Monitor();

        $data = $mdl_monitor->getCommentLevel1($params['parent_level']);
// Zend_Debug::dump($data);die();

        $this->view->data = $data;
        $this->view->params = $params;
        $this->view->identity = $identity;

// die();
    }

    public function inputlaporan2Action() {

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $params = $this->getRequest()->getParams();
        Zend_Debug::dump($params);
        die();

        $mdl_monitor = new Model_Monitor();

        $msg = "";

        $msg .="Dh,\n";
        $msg .="Kami sampaikan monitoring fastel KAA 2015.\n";
        $msg .="Date: " . date("d M y") . "\n";
        $msg .="Time: " . date("H:i") . "\n\n";

        $msg .="A. Summary\n";
        $msg .="Fastel Telkom\n";
        $msg .=$params["fastel_telkom"] . "\n";

        $msg .="\nFastel Telkomsel\n";
        $msg .=$params["fastel_telkomsel"] . "\n";

        $msg .="\nFastel Telin\n";
        $msg .=$params["fastel_telin"] . "\n";

        $msg .="\n\nDetail terlampir\n\n";

        $msg .="B. ISSUE PENTING\n";
        $msg .=$params["issue_penting"] . "\n";

        $msg .="\nC. NEXT AGENDA\n";
        $msg .=$params["som_meeting"] . "\n";

        $msg .="\nD. LAMPIRAN\n";
        $msg .="\n1. REKAP FASTEL TELKOM KAA\n";

        $msg .="\nAREA|LAYANAN|DOWN|UP|Total\n\n";

        $data_rekap = $mdl_monitor->getDataRekap();
// Zend_Debug::dump($data_rekap);die();

        $data = array();
        foreach ($data_rekap as $k => $v) {
            $data[$v['AREA']][] = $v;
        }
// Zend_Debug::dump($data);die();


        $total_down = 0;
        $total_up = 0;
        $total_total = 0;
        foreach ($data as $k => $v) {

            foreach ($v as $k2 => $v2) {
                $total_down += $v2['DOWN'];
                $total_up += $v2['UP'];
                $total_total += $v2['TOTAL'];

                $msg .=$v2['AREA'] . "|" . $v2['LAYANAN'] . "|" . $v2['DOWN'] . "|" . $v2['UP'] . "|" . $v2['TOTAL'] . "\n";
            }
            $msg .="\n";
            // $msg .=$v['AREA']."|".$v['LAYANAN']."|".$v['DOWN']."|".$v['UP']."|".$v['TOTAL']."\n";
        }

        $msg .="Grand Total|" . $total_down . "|" . $total_up . "|" . $total_total . "\n\n";

        $msg .="2. REKAP FASTEL TELKOMSEL\n";
        $msg .="KAA Critical Network Incident Update\n";
        $msg .="Type/Status/Reg/Loc/Start/Duration/End/Impact/Actions/Estimated End\n";
        $msg .=$params["rekap_fastel_telkomsel"] . "\n";

        $msg .="\n\n3. REKAP FASTEL TELIN\n";
        $msg .="DATIN: " . $params["datin"] . "\n";
        $msg .="VOICE: " . $params["voice"] . "\n";
        $msg .="TOTAL BACKBONE INTERNATIONAL " . $params["total_backbone"] . "\n";

        $msg .="\nDemikian kami sampaikan\n";
        $msg .="Koordinator POSKO TELKOM GROUP KAA 2015\n";
        $msg .="YANTO SETIAWAN\n";
        $msg .="021-5740400";

        $params["message"] = $msg;
        $params["nik"] = $identity->uname;
// Zend_Debug::dump($params["message"]);die();


        $ins = $mdl_monitor->inputlaporan($params);
// Zend_Debug::dump($ins);die();

        if ($ins['result']) {



            $gname = 'KAA_ALERT';
            $url = 'http://10.62.8.133/api/telegram/action.php?act=sendmsgtxt&target=' . $gname . '&msg=' . urlencode($params["message"]);
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result = curl_exec($ch);
            curl_close($ch);

// echo '<br><br>message has been send to '.$gname.' (Telegram).';

            $result = array(
                'retCode' => '11',
                'retMsg' => $ins['message'],
                'result' => true,
                'data' => null
            );
        } else {

            $result = array(
                'retCode' => '00',
                'retMsg' => $ins['message'],
                'result' => false,
                'data' => null
            );
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function inputlaporanAction() {

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $params = $this->getRequest()->getParams();
// Zend_Debug::dump($params);die();

        $mdl_monitor = new Model_Monitor();
        $dm = $mdl_monitor->getDMposko();


        $msg = "";
        $msg .= "Pak Awal Ykh\n";
        $msg .= "\n";
        $msg .= "Kami laporkan status fastel VIP TelkomGroup di area MASIV lokasi Jakarta dan Bandung pada AACC 2015 posisi tgl " . date("d F") . " pukul " . $params["input1"] . " wib sbb :\n";
        $msg .= "\n";
        $msg .= "1. Secara umum fastel berfungsi " . $params["input2"] . " dengan rincian terlampir\n";
        $msg .= "\n";
        $msg .= "2. Trafik pada MRTG normal dengan pemakaian tertinggi terjadi pada pukul " . $params["input3"] . "  sebesar " . $params["input4"] . ". \n";
        $msg .= " \n";
        $msg .= "3. Aktivitas Posko hari ini difokuskan :\n";
        $msg .= "Jakarta : " . $params["input5"] . "\n";
        $msg .= "Bandung : " . $params["input6"] . "\n";
        $msg .= " \n";
        $msg .= "4. Petugas Posko : \n";
        $msg .= "Hari ini, " . date("l, d F Y") . " :\n";
        $msg .= "DM Posko Semanggi : " . $params["input7"] . "\n";
        $msg .= "DM Posko Bandung : " . $params["input8"] . "\n";
        $msg .= "\n";
        $msg .= "Besok : " . date('l, d F Y', strtotime(date('Y/m/d') . " +1 days")) . " :\n";
        $msg .= "DM Posko Semanggi : " . $params["input9"] . "\n";
        $msg .= "DM Posko Bandung : " . $params["input10"] . "\n";
        $msg .= "\n";
        $msg .= "\n";
        $msg .= "5. Related Info :\n";
        $msg .= "" . $params["input11"] . "\n";
        $msg .= "\n";
        $msg .= "\n";
        $msg .= "\n";
        $msg .= "Demikian. Tks\n";
        $msg .= "\n";
        $msg .= "\n";
        $msg .= "\n";
        $msg .= "----------------------\n";
        $msg .= "Lampiran :\n";
        $msg .= "\n";
        $msg .= "Status Fastel TGroup posisi " . date("d F Y") . " jam " . $params["input12"] . " wib\n";
        $msg .= "\n";
        $msg .= "1. Rekap Fastel di lokasi MASIV\n";
        $msg .= "\n";
        $msg .= "Lokasi Jakarta\n";
        $msg .= "\n";
        $msg .= "M Center/JCC Hall B/Astinet 10 Gbps/ Ok\n";
        $msg .= "M Center/JCC Hall B/ Wifi 8 AP 1 Gbps/Ok\n";
        $msg .= "M Center/JCC Hall B/ Sinyal Tsel/Ok\n";
        $msg .= "M Center/JCC Hall B/SGN 1 Unit/ok\n";
        $msg .= "\n";
        $msg .= "Akomodasi/Hotel Sultan/wifi ID 1 Gbps/ok\n";
        $msg .= "Akomodasi/Hotel Sultan/Sinyal TSel/ok\n";
        $msg .= "Akomodasi/Hotel pendukung (18)/ Wifi id/ok\n";
        $msg .= "Akomodasi/Hotel pendukung (18)/ Sinyal Tsel/ok\n";
        $msg .= "\n";
        $msg .= "Security/Puskodal/Astinet 20 Mbps/ok\n";
        $msg .= "Security/cctv Puskodal/Metro, VPN 100 Mbps/ok\n";
        $msg .= "Security/Paspampres/ Astinet 20 Mbps/ok\n";
        $msg .= "Security/TNI AD/AStinet 20 Mbps/ok\n";
        $msg .= "Security/ CCTV Puskotis/ VPN IP 3 titik 25 Mbps/ok\n";
        $msg .= "\n";
        $msg .= "\n";
        $msg .= "Int. Airport/Bandara Soeta/ Wifi Id 160 AP/ Ok\n";
        $msg .= "Int. Airport/Bandara Soeta/ Sinyal Tsel/ Ok\n";
        $msg .= "Int. Airport/Bandara Halim/ Wifi id 45 Ap/Ok\n";
        $msg .= "Int. Airport/Bandara Halim/ Sinyal Tsel/Ok\n";
        $msg .= "\n";
        $msg .= "Venue/JCC Meeting Room/Astinet 50 Mbps/ok\n";
        $msg .= "Venue/JCC Hall A/Astinet 100 Mbps/ok\n";
        $msg .= "Venue/JCC-all/Wifi ID 1 Gbps/ok\n";
        $msg .= "Venue/JCC-all/Sinyal Tsel/ok\n";
        $msg .= "\n";
        $msg .= "\n";
        $msg .= "Lokasi Bandung\n";
        $msg .= "\n";
        $msg .= "M Centre/Hotel Ibis/Astinet 2 Gbps/ok\n";
        $msg .= "M Centre/Hotel Ibis/Wifi id 7 AP 1 Gbps/ok\n";
        $msg .= "M Centre/Hotel Ibis/Sinyal Tsel/ok\n";
        $msg .= "M Centre/Hotel Ibis/SNG 1 Unit/ok\n";
        $msg .= "M Centre/New Majestik/Astinet 2 Gbps/ok\n";
        $msg .= "M Centre/New Majestik/Wifi id 6 AP 1 Gbps/ok\n";
        $msg .= "M Centre/New Majestik/Sinyal Tsel/ok\n";
        $msg .= "M Centre/PGN/ Astinet 2 Gbps/ok\n";
        $msg .= "M Centre/PGN/ Wifi id 13 AP 1 Gbps/ok\n";
        $msg .= "M Centre/PGN/ Sinyal Tsel/ok\n";
        $msg .= "\n";
        $msg .= "\n";
        $msg .= "Accomodation/Hotel Savoy/wifi 10 Ap 500 Mbps/ok\n";
        $msg .= "Accomodation/Hotel Savoy/Sinyal Tsel/ok\n";
        $msg .= "\n";
        $msg .= "Security/Paspampres/Astinet 20 Mbps/ok\n";
        $msg .= "Security/ BIN/ Astinet 7 link 120 Mbps/ok\n";
        $msg .= "Security/ CCTV Puskotis/ VPN IP 4 titik 95 Mbps/ok\n";
        $msg .= "\n";
        $msg .= "\n";
        $msg .= "Int. Airport/Husein/wifi id 6 AP/ok\n";
        $msg .= "Int.Airport/Husein/Sinyal Tsel/ok\n";
        $msg .= "\n";
        $msg .= "Venue/Savoy Homan/Metro 50 Mbps/ok\n";
        $msg .= "Venue/Balai Pakuan/ Metro 50 Mbps/ok\n";
        $msg .= "Venue/Majestik/Metro 50 Mbps/ok\n";
        $msg .= "Venue/Area Historical Walk/wifi id 1 Gbps/ok\n";
        $msg .= "Venue/Gedung Merdeka/Astinet 1 Gbps/ok\n";
        $msg .= "Venue/All site /Sinyal Tsel/ok\n";
        $msg .= "\n";
        $msg .= "\n";
        $msg .= "2. REKAP FASTEL TELKOMSEL\n";
        $msg .= "KAA Critical Network Incident Update\n";
        $msg .= "Type/Status/Reg/Loc/Start/Duration/End/Impact/Actions/Estimated End\n";
        $msg .= "" . $params["input13"] . "\n";
        $msg .= "\n";
        $msg .= "\n";
        $msg .= "3. REKAP FASTEL TELIN \n";
        $msg .= "DATIN: " . $params["input14"] . " M of 10 G\n";
        $msg .= "VOICE: " . $params["input15"] . "; ACD : " . $params["input16"] . "; ASR: " . $params["input17"] . "\n";
        $msg .= "TOTAL BACKBONE INTERNATIONAL " . $params["input18"] . " of " . $params["input19"] . "\n";
        $msg .= "\n";
        $msg .= "GBR: " . $params["input20"] . "\n";
        $msg .= "DMI: " . $params["input21"] . "\n";
        $msg .= "BTM: " . $params["input22"] . "\n";
        $msg .= "SBY: " . $params["input23"] . "\n";
        $msg .= "SMG: " . $params["input24"] . "\n";
        $msg .= "\n";
        $msg .= "\n";
        $msg .= "Demikian kami sampaikan\n";
        $msg .= "Duty Manager :" . $params["input25"] . "\n";
        $msg .= "Deputy DM : " . $params["input26"] . "\n";
        $msg .= "Telp posko: 021-5740400";

        $params["message"] = $msg;
        $params["nik"] = $identity->uname;
// Zend_Debug::dump($params["message"]);die();


        $ins = $mdl_monitor->inputlaporan($params);
// Zend_Debug::dump($ins);die();

        if ($ins['result']) {


            $func = new CMS_Himfunctions();

            $target = "KAA_ALERT";

            $func->send_telegram($target, $msg);

            $result = array(
                'retCode' => '11',
                'retMsg' => $ins['message'],
                'result' => true,
                'data' => null
            );
        } else {

            $result = array(
                'retCode' => '00',
                'retMsg' => $ins['message'],
                'result' => false,
                'data' => null
            );
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function changestatusticketAction() {

        $this->_helper->layout->disableLayout();

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $params = $this->getRequest()->getParams();
// Zend_Debug::dump($params);die();	

        $this->view->params = $params;
    }

    public function closeticketAction() {

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $params = $this->getRequest()->getParams();
// Zend_Debug::dump($params);die();

        $mdl_monitor = new Model_Monitor();

        $ins = $mdl_monitor->closeticket($params, $identity);
// Zend_Debug::dump($ins);die();

        if ($ins['result']) {


            $result = array(
                'retCode' => '11',
                'retMsg' => $ins['message'],
                'result' => true,
                'data' => null
            );
        } else {

            $result = array(
                'retCode' => '00',
                'retMsg' => $ins['message'],
                'result' => false,
                'data' => null
            );
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function tresholdAction() {
        $this->_helper->layout->disableLayout();
        //$this->_helper->viewRenderer->setNoRender(true);
        try {
            $auth = Zend_Auth::getInstance();
            $identity = $auth->getIdentity();
        } catch (Exception $e) {
            
        }
        //Zend_Debug::dump($identity);die();
        $sEcho = intval($_GET['sEcho']);
        ;
        $iTotalRecords = 0;
        $iDisplayLength = intval($_GET['iDisplayLength']);
        $iDisplayStart = intval($_GET['iDisplayStart']);
        $records = array();
        $records["aaData"] = array();

        $params = $this->getRequest()->getParams();
        $mdl_monitor2 = new Model_Monitor2();
        $countgraph = $mdl_monitor2->count_detailcustomer($params);
        //Zend_Debug::dump($countgraph);die();
        $listgraph = $mdl_monitor2->get_detailcustomer($params);
        //Zend_Debug::dump($listgraph);die();

        $iTotalRecords = intval($countgraph);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;

        $end = $iDisplayStart + $iDisplayLength;


        $server = array('10.62.8.126', '10.62.8.132', '10.62.8.135', '10.62.8.136');
        $act = "";

        foreach ($listgraph as $k => $v) {

            if ($v['graph_id'] != null && $v['graph_id'] != '') {

                $key = md5('telkomcare');
                $string = $v['graph_id'] . '|' . $v['server_id'] . '|' . ((isset($server[$v['server_id']])) ? $server[$v['server_id']] : "UNKNOWN") . '|' . str_replace("<path_rra>", "", $v['rrd_name']);
                //echo $string.'<br>';
                //$string = "210625|3|10.62.8.136|/1170/210555.rrd";
                $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key)));
                //echo $encrypted."<br>";
                $encrypted2 = base64_encode($encrypted);

                $act = '<div style="text-align:center;vertical-align:top;min-width:30px">
						<a href="javascript:;" data-title="' . $v['graph_title'] . '" data-sid="' . $v['serv_id'] . '" data-cust="' . $v['sub_ubis_name'] . '" data-addr="" data-id="' . $encrypted2 . '" class="btn btn-xs blue btn-graph" style="display:inline-block;padding: 2px 5px;float: left;">
							<i class="fa fa-bar-chart-o"></i>
						</a>
					</div>';

                $records["aaData"][] = array(
                    $act,
                    ($iDisplayStart + $k + 1),
                    $v['sub_ubis_name'],
                    $v['serv_id'],
                    $v['alamat'],
                    $v['layanan'],
                    $v['bandwidth'],
                    $v['utilisation'] . '%'
                );
            }
        }

        //Zend_Debug::dump($records);die();

        $records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }

}
