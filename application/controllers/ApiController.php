<?php

/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class ApiController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    public function managewhatsappAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/table-ajax.js');

        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/ui-extended-modals.js');


//$this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
//$this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
// $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css');
//$this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datetimepicker/css/datetimepicker.css');
        /* $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
          $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');

          $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
          //$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
          //$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
          //$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-daterangepicker/moment.min.js');
          //$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-daterangepicker/daterangepicker.js');
          //$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js');
          //$this->view->headScript()->appendFile('/assets/core/plugins/date-id-ID.js');
          $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
          //$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-touchspin/bootstrap.touchspin.js');
          $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
          $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
          $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
          $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js'); */

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
//Zend_Debug::dump($identity);die();

        $mdl_wa = new Model_Whatsapp();
        $num = $mdl_wa->get_listnumber();
//Zend_Debug::dump($num); die();
        $this->view->num = $num;
    }

    public function whatsappwizardAction() {
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-validation/dist/additional-methods.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/js/api-wawizard-form-wizard.js');

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
//Zend_Debug::dump($identity);die();

        $this->view->identity = $identity;
    }

    public function telegramwizardAction() {
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-validation/dist/additional-methods.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/js/api-tgwizard-form-wizard.js');

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
//Zend_Debug::dump($identity);die();

        $this->view->identity = $identity;
    }

    public function savewaAction() {
        $this->_helper->layout->disableLayout();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
//Zend_Debug::dump($identity);die();

        $mdl_wa = new Model_Whatsapp();
        $mdl_wa->add_listnumber($identity, $_POST);
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode(array('retCode' => '00', 'result' => true));
        die();
    }

    public function savetgAction() {
        $this->_helper->layout->disableLayout();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
//Zend_Debug::dump($identity);die();

        $mdl_tg = new Model_Telegram();
        $mdl_tg->add_listnumber($identity, $_POST);
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode(array('retCode' => '00', 'result' => true));
        die();
    }

    public function checknumberAction() {
        $this->_helper->layout->disableLayout();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
//Zend_Debug::dump($identity);die();

        $mdl_wa = new Model_Whatsapp();
        $ret = $mdl_wa->check_number($_GET['phone']);
//Zend_Debug::dump($ret);die();
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode(array('result' => $ret));
        die();
    }

    public function checkprofileAction() {
        $this->_helper->layout->disableLayout();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
//Zend_Debug::dump($identity);die();

        $ret = file_exists('/home/prahu/.telegram-cli/profile/' . $_GET['profile']);
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode(array('result' => $ret));
        die();
    }

    public function updatepassAction() {
        $this->_helper->layout->disableLayout();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
//Zend_Debug::dump($identity);die();

        $mdl_wa = new Model_Whatsapp();
        $ret = $mdl_wa->update_pass($identity, $_GET);
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode(array('result' => $ret));
        die();
    }

    public function wachatAction() {
        $params = $this->getRequest()->getParams();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $mdl_wa = new Model_Whatsapp();
        $num = $mdl_wa->get_listnumber();
//Zend_Debug::dump($num); die();
        $this->view->num = $num;

//Zend_Debug::dump($identity);die();
        if (!isset($params['phone'])) {
            $conf = array(
                'username' => '6285222399213',
                'identity' => strtolower(urlencode(sha1($username, true))),
                'password' => "fPOKZ7MN0/WAswh7Xsed8KvVt3U=",
                'nickname' => 'Telkom'
            );
            $params['phone'] = '6285222399213';
        } else {
            $number = $mdl_wa->get_number($params['phone']);
            //Zend_Debug::dump($number); die();

            if ($number != false && count($number) > 0) {
                $conf = array(
                    'username' => $params['phone'],
                    'identity' => strtolower(urlencode(sha1($username, true))),
                    'password' => $number["password"],
                    'nickname' => $number["nickname"]
                );
            } else {
                $conf = array(
                    'username' => $params['phone'],
                    'identity' => strtolower(urlencode(sha1($username, true))),
                    'password' => "9817234kjhwqewffwqe",
                    'nickname' => 'fqwi8fnqwhf'
                );
            }
        }

        $this->view->identity = $identity;
        $this->view->params = $params;
        $this->view->conf = $conf;
        $this->view->res = array();
    }

    public function managewagroupAction() {
        require_once (APPLICATION_PATH) . '/../public/api/wa/waWhatsProt.php';
        require_once (APPLICATION_PATH) . '/../public/api/wa/wafunction.php';

        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-tags-input/jquery.tagsinput.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-tags-input/jquery.tagsinput.min.js');

        $params = $this->getRequest()->getParams();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $mdl_wa = new Model_Whatsapp();
        $num = $mdl_wa->get_listnumber();
//Zend_Debug::dump($num); die();
        $this->view->num = $num;

        if (isset($_POST) && count($_POST) > 0) {
            //Zend_Debug::dump($_POST);die();
            if (isset($_POST['act']) && $_POST['act'] == 'addgrp') {
                $number = $mdl_wa->get_number($_POST['phone']);
                //Zend_Debug::dump($number); die();
                if ($number != false && count($number) > 0) {
                    $username = $params['phone'];
                    $identity = strtolower(urlencode(sha1($username, true)));
                    $password = $number["password"];
                    $nickname = $number["nickname"];

                    $w = new WhatsProt($username, $identity, $nickname, false, true);
                    $wcon = $w->Connect();

                    if (isset($wcon) && $wcon['result'] == false) {
                        $this->view->msg = "Group " . $_POST['grpsubject'] . " failed to create.";
                    } else {
                        $wlogin = $w->LoginWithPassword($password);
                        if (isset($wlogin) && $wlogin['result'] == false) {
                            $this->view->msg = "Group " . $_POST['grpsubject'] . " failed to create.";
                        } else {
                            $participants = explode(',', $_POST['grppar']);
                            $gid = $w->sendGroupsChatCreate($_POST['grpsubject'], $participants);
                            $w->disconnect();
                            //my_dump_exit($gid);
                            if ($gid != null && $gid != '' && $gid != false) {
                                $this->view->msg = "Create group " . $_POST['grpsubject'] . " has been requested to WhatsApp Server.";
                            } else {
                                $this->view->msg = "Group " . $_POST['grpsubject'] . " failed to create.";
                            }
                        }
                    }
                }
            } else if (isset($_POST['act']) && $_POST['act'] == 'delgrp') {
                $number = $mdl_wa->get_number($_POST['phone']);
                //Zend_Debug::dump($number); die();
                if ($number != false && count($number) > 0) {
                    $username = $params['phone'];
                    $identity = strtolower(urlencode(sha1($username, true)));
                    $password = $number["password"];
                    $nickname = $number["nickname"];

                    $w = new WhatsProt($username, $identity, $nickname, false, true);
                    $wcon = $w->Connect();

                    if (isset($wcon) && $wcon['result'] == false) {
                        $this->view->msg = "Group failed to delete.";
                    } else {
                        $wlogin = $w->LoginWithPassword($password);
                        if (isset($wlogin) && $wlogin['result'] == false) {
                            $this->view->msg = "Group failed to delete.";
                        } else {
                            $w->sendGroupsChatEnd($_POST['id']);
                            $w->sendGetGroupsParticipants($_POST['id']);
                            //my_dump_exit($gid);
                            $this->view->msg = "Delete group has been requested to WhatsApp Server.";
                        }
                    }
                    $w->disconnect();
                }
            } else if (isset($_POST['act']) && $_POST['act'] == 'addpar') {
                $number = $mdl_wa->get_number($_POST['phone']);
                //Zend_Debug::dump($number); die();
                if ($number != false && count($number) > 0) {
                    $username = $params['phone'];
                    $identity = strtolower(urlencode(sha1($username, true)));
                    $password = $number["password"];
                    $nickname = $number["nickname"];

                    $w = new WhatsProt($username, $identity, $nickname, false, true);
                    $wcon = $w->Connect();

                    if (isset($wcon) && $wcon['result'] == false) {
                        $this->view->msg = "Participants failed to add.";
                    } else {
                        $wlogin = $w->LoginWithPassword($password);
                        if (isset($wlogin) && $wlogin['result'] == false) {
                            $this->view->msg = "Participants failed to add.";
                        } else {
                            $wlogin = $w->LoginWithPassword($password);
                            $participants = explode(',', $_POST['grppar']);
                            $w->sendGroupsParticipantsAdd($_POST['id'], $participants);
                            //my_dump_exit($gid);
                            $this->view->msg = "Add participants has been requested to WhatsApp Server.";
                        }
                    }
                    $w->disconnect();
                }
            } else if (isset($_POST['act']) && $_POST['act'] == 'sendmsg') {
                $number = $mdl_wa->get_number($_POST['phone']);
                //Zend_Debug::dump($number); die();
                if ($number != false && count($number) > 0) {
                    $username = $params['phone'];
                    $identity = strtolower(urlencode(sha1($username, true)));
                    $password = $number["password"];
                    $nickname = $number["nickname"];

                    $w = new WhatsProt($username, $identity, $nickname, false, true);
                    $w->eventManager()->bind("onGetError", "ongetError");
                    $wcon = $w->Connect();

                    if (isset($wcon) && $wcon['result'] == false) {
                        $this->view->msg = "Message failed to send.";
                    } else {
                        $wlogin = $w->LoginWithPassword($password);
                        if (isset($wlogin) && $wlogin['result'] == false) {
                            $this->view->msg = "Message failed to send.";
                        } else {
                            //Zend_Debug::dump($wlogin);//die();
                            if (isset($_POST['sendck']) && $_POST['sendck'] == 'true' && $_POST['id'] != '') {
                                $send = $w->sendMessage($_POST['id'], $_POST['wall']);
                                $this->view->msg = "Send message to " . $_POST['id'] . " has been requested to WhatsApp Server.";
                            } else {
                                $send = $w->sendMessage($_POST['sendphn'], $_POST['wall']);
                                $this->view->msg = "Send message to " . $_POST['sendphn'] . " has been requested to WhatsApp Server.";
                            }
                        }
                    }
                    //Zend_Debug::dump($send); die();
                    $w->disconnect();
                    //my_dump_exit($gid);
                }
            }
        }

//Zend_Debug::dump($identity);die();
        if (!isset($params['phone'])) {
            $this->_redirect('/api/managewhatsapp');
        } else {
            $number = $mdl_wa->get_number($params['phone']);
            //Zend_Debug::dump($number); die();

            if ($number != false && count($number) > 0) {
                $conf = array(
                    'username' => $params['phone'],
                    'identity' => strtolower(urlencode(sha1($params['phone'], true))),
                    'password' => $number["password"],
                    'nickname' => $number["nickname"]
                );
            } else {
                $conf = array(
                    'username' => $params['phone'],
                    'identity' => strtolower(urlencode(sha1($params['phone'], true))),
                    'password' => "9817234kjhwqewffwqe",
                    'nickname' => 'fqwi8fnqwhf'
                );
            }
        }

        $this->view->identity = $identity;
        $this->view->params = $params;
        $this->view->conf = $conf;
        $this->view->res = array();
    }

    public function managewagroup2Action() {
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-tags-input/jquery.tagsinput.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-tags-input/jquery.tagsinput.min.js');

        $params = $this->getRequest()->getParams();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $mdl_wa = new Model_Whatsapp();
        $num = $mdl_wa->get_listnumber(true);
//Zend_Debug::dump($num); die();
        $this->view->num = $num;
        $this->view->identity = $identity;
        $this->view->params = $params;
    }

    public function telegramgroupAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-tags-input/jquery.tagsinput.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-tags-input/jquery.tagsinput.min.js');
    }

    public function senduiAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');

        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-toastr/toastr.min.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-toastr/toastr.min.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/ui-toastr.js');
        //die('www');
        $CMS_himfunc = new CMS_Himfunctions();
        $send = array(
            array(
                'name' => "Fikri Rohim",
                'target' => "rohimfikri@gmail.com",
                'subject' => "Test API",
                'msg' => "<h3>HELLO FIKRI</h3><br><hr>Selamat Datang...."
            )
        );
        //Zend_Debug::dump($send);die();
        //$sendemail = $CMS_himfunc->sendEmail($send,true);
        //Zend_Debug::dump($sendemail);//die();

        $send = array(
            array(
                'target' => "Budi_Codet",
                'msg' => "
			HELLO FIKRI
			===========
			Selamat Datang...."
            )
        );
        //Zend_Debug::dump($send);die();
        //$sendtelegram = $CMS_himfunc->sendTelegram($send,true);
        //Zend_Debug::dump($sendtelegram);//die();

        $send = array(
            array(
                'target' => "6285793133614",
                'msg' => "
			HELLO Budi
			===========
			Selamat Datang...."
            )
        );
        //Zend_Debug::dump($send);die();
        //$sendwhatsapp = $CMS_himfunc->sendWhatsapp($send,true);
        //Zend_Debug::dump($sendwhatsapp);die();

        $send = array(
            array(
                'target' => "made.linux@gmail.com",
                'msg' => "
			HELLO Mang Akhmad
			===========
			Selamat Datang...."
            )
        );
        //Zend_Debug::dump($send);die();
        //$sendgtalk = $CMS_himfunc->sendGtalk($send,true);
        //Zend_Debug::dump($sendgtalk);die();


        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        if (isset($_POST) && count($_POST) > 0) {
            //Zend_Debug::dump($_POST);die();
            $mdl_api = new Model_Api();
            $update = $mdl_api->insertupdate_attr($_POST, $identity->uid);
            $this->view->msg = $update;
        }
        //$this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        //$this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        //$this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        //$mdl_api = new Model_Api();
        //$user = $mdl_api->getApiUser();
        //Zend_Debug::dump($user);die();
        //echo json_encode($user);die();
        //$this->view->user = $user;
    }

    public function getlistAction() {
        $this->_helper->layout->disableLayout();
        $mdl_api = new Model_Api();
        $user = $mdl_api->getApiUser2($_GET);
        //Zend_Debug::dump($user);die();
        $ret = array();
        foreach ($user as $k => $v) {
            $type = '';
            if ($v['uname'] == $v['fullname'] && $v['ubis'] == "Group" && $v['sub_ubis'] == "User Group") {
                $type = 'group';
            } else {
                $type = 'user';
            }

            if (!isset($ret[$type . '_' . $v['uname']])) {
                $ret[$type . '_' . $v['uname']] = array(
                    'id' => $v['id'],
                    'uname' => $v['uname'],
                    'fullname' => $v['fullname'],
                    'ubis_id' => $v['ubis_id'],
                    'ubis' => $v['ubis'],
                    'sub_ubis_id' => $v['sub_ubis_id'],
                    'sub_ubis' => $v['sub_ubis'],
                    'attr' => array()
                );
            }

            if ($v['attr_code'] != null && $v['attr_code'] != '') {
                $ret[$type . '_' . $v['uname']]['attr'][$v['attr_code']] = $v['attr_val'];
            }
        }
        //Zend_Debug::dump($ret);die();

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($ret);
        die();
    }

    public function getlist2Action() {
        $this->_helper->layout->disableLayout();
        $mdl_api = new Model_Api();
        $user = $mdl_api->getApiUser3($_GET);
        //Zend_Debug::dump($user);die();
        $ret = array();
        foreach ($user as $k => $v) {
            $type = '';
            if ($v['uname'] == $v['fullname'] && $v['ubis'] == "Group" && $v['sub_ubis'] == "User Group") {
                $type = 'group';
            } else {
                $type = 'user';
            }

            if (!isset($ret[$type . '_' . $v['uname']])) {
                $ret[$type . '_' . $v['uname']] = array(
                    'id' => $v['id'],
                    'uname' => $v['uname'],
                    'fullname' => $v['fullname'],
                    'ubis_id' => $v['ubis_id'],
                    'ubis' => $v['ubis'],
                    'sub_ubis_id' => $v['sub_ubis_id'],
                    'sub_ubis' => $v['sub_ubis'],
                    'attr' => array()
                );
            }

            if ($v['attr_code'] != null && $v['attr_code'] != '') {
                $ret[$type . '_' . $v['uname']]['attr'][$v['attr_code']] = $v['attr_val'];
            }
        }
        //Zend_Debug::dump($ret);die();

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($ret);
        die();
    }

    public function sendmsgAction() {
    }

    public function managefilesAction() {
        if (isset($_FILES['upl']) && $_FILES['upl']['error'] == 0) {
            // A list of permitted file extensions
            $allowed = array(
                'image' => array('png', 'jpg', 'gif', 'jpeg'),
                'audio' => array('mp3', 'wav', 'amr'),
                'video' => array('3gp', 'flv', 'mp4'),
                'text' => array('txt', 'log'),
                'document' => array('doc', 'docx', 'pdf', 'ppt', 'pptx'),
                'other' => array('zip')
            );
            //Zend_Debug::dump($_FILES);//die();

            $size = $_FILES['upl']['size'];
            if ((int) $size > 1000000) {
                header("Access-Control-Allow-Origin: *");
                header('Content-type: application/json');
                echo json_encode(array('status' => 'error', 'return' => false, 'msg' => "size too large [max 1 Mb]"));
                exit;
            }

            $extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);
            //Zend_Debug::dump($extension);die();
            $type = null;
            $ext = null;
            foreach ($allowed as $k => $v) {
                foreach ($v as $kx => $vx) {
                    if (strtolower($extension) == $vx) {
                        $type = $k;
                        $ext = $vx;
                        break;
                    }
                }
            }
            if ($type == null || $ext == null) {
                header("Access-Control-Allow-Origin: *");
                header('Content-type: application/json');
                echo json_encode(array('status' => 'error', 'return' => false, 'msg' => "format file not allowed"));
                exit;
            }

            if (move_uploaded_file($_FILES['upl']['tmp_name'], '/var/www/html/newprabacore/public/prahu/upload/' . $type . '/' . $_FILES['upl']['name'])) {
                chmod('/var/www/html/newprabacore/public/prahu/upload/' . $type . '/' . $_FILES['upl']['name'], 0777);
                chgrp('/var/www/html/newprabacore/public/prahu/upload/' . $type . '/' . $_FILES['upl']['name'], 54323);
                header("Access-Control-Allow-Origin: *");
                header('Content-type: application/json');
                echo json_encode(array('status' => 'success', 'return' => true, 'msg' => "file has been saved"));
                exit;
            }
            header("Access-Control-Allow-Origin: *");
            header('Content-type: application/json');
            echo json_encode(array('status' => 'error', 'return' => false, 'msg' => "failed to upload"));
            exit;
        }

        if (isset($_GET['act']) && $_GET['act'] == 'getfiles') {
            $list = array();
            $type = (isset($_GET['type']) && $_GET['type'] != '') ? $_GET['type'] : '*';
            $files = glob("/var/www/html/newprabacore/public/prahu/upload/" . $type . "/*", GLOB_MARK);
            foreach ($files as $v) {
                //Zend_Debug::dump(stat($v));die();
                $tmp = explode('/', $v);
                $size = filesize($v);
                $tmp2 = array();
                if ($size < 1000) {
                    $tmp2 = array(
                        'size' => floor($size),
                        'in' => 'b'
                    );
                } else if ($size >= 1000 && $size < 1000000) {
                    $tmp2 = array(
                        'size' => number_format((float) ($size / 1000), 2, '.', ''),
                        'in' => 'Kb'
                    );
                } else if ($size >= 1000000 && $size < 1000000000) {
                    $tmp2 = array(
                        'size' => number_format((float) ($size / 1000000), 2, '.', ''),
                        'in' => 'Mb'
                    );
                } else if ($size >= 1000000000 && $size < 1000000000000) {
                    $tmp2 = array(
                        'size' => number_format((float) ($size / 1000000000), 2, '.', ''),
                        'in' => 'Gb'
                    );
                }
                $list[] = array(
                    'path' => $v,
                    'size_bytes' => $size,
                    //'uid'=>fileowner($v),
                    //'gid'=>filegroup($v),
                    'last_access' => date("Y-m-d H:i:s", fileatime($v)),
                    'last_modified' => date("Y-m-d H:i:s", filemtime($v)),
                    //'permissions'=>sprintf('%o', fileperms($v)),
                    //'file_type'=>filetype($v),
                    //'realpath'=>realpath($v),
                    'basename' => basename($v),
                    'dirname' => dirname($v),
                    'ext' => pathinfo(basename($v), PATHINFO_EXTENSION),
                    'type' => $tmp[count($tmp) - 2],
                    'size' => $tmp2
                );
            }
            //Zend_Debug::dump($list);die();
            //$this->view->files = $list;
            header("Access-Control-Allow-Origin: *");
            header('Content-type: application/json');
            echo json_encode($list);
            exit;
        }

        $this->view->headLink()->appendStylesheet('/assets/core/plugins/mini-upload-form/assets/css/style.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-toastr/toastr.min.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/mini-upload-form/assets/js/jquery.knob.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/mini-upload-form/assets/js/jquery.ui.widget.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/mini-upload-form/assets/js/jquery.iframe-transport.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/mini-upload-form/assets/js/jquery.fileupload.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-toastr/toastr.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/mini-upload-form/assets/js/script.js');

        $params = $this->getRequest()->getParams();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
    }

}
