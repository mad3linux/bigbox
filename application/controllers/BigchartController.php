<?php
class BigchartController extends Zend_Controller_Action {

public function adminajaxAction() {
	$this->_helper->layout->disableLayout();
	$data = array(
		"error"=>true,
		"message"=>""
	);
	// Zend_Debug::dump($_POST); die();
	if($_POST['action']=='amcharts_editor_get_current_user'){
		try {
			$authAdapter = Zend_Auth::getInstance();
			$identity = $authAdapter->getIdentity();
		}
		catch(Exception $e) {
		}
		// Zend_Debug::dump($identity); die();
		$data['error'] = false;
		$data['id'] = $identity->uid;
		// Zend_Debug::dump($data); die();
		// Zend_Debug::dump($this->restorearray((array)$identity)); die();
		$data = array_merge($data,$this->restorearray((array)$identity));
		// Zend_Debug::dump($data); die();
	// }
	// else if($_POST['action']=='amcharts_editor_get_chart_list'){
		// echo '{"error":true,"message":"You need to be logged in to get chart list","login_url":"https:\/\/www.amcharts.com\/sign-in\/editor\/?skinny=1&redirect_to=http%3A%2F%2Flive.amcharts.com%2Fauth%2F%3Faction%3Ddone"}';
		// die();
	}else if($_POST['action']=='amcharts_editor_save_chart'){
		// Zend_Debug::dump($_POST); die();
		$POST = $this->restorearray((array)$_POST);
		$ns = Zend_Session::namespaceGet('bigchart');
		// Zend_Debug::dump($ns);//die();
		// Zend_Debug::dump($POST); die();
		// if(isset($ns['report']) && isset($ns['sql']) && isset($ns['apiid']) && isset($ns['apiparam']) && isset($ns['dbfile'])){
			// $ns['report'] = json_encode($ns['report']);
			$p = array(
				'name'=>(isset($POST['title']))?$POST['title']:"bigchart_".date("YmdHis"),
				'title'=>(isset($POST['title']))?$POST['title']:"bigchart_".date("YmdHis"),
				'weight'=>"",
				'pcolor'=>"",
				'type'=>"",
				'width'=>""
			);
			$par = array_merge_recursive($ns,$POST);
			// Zend_Debug::dump($par); die();
			// if(isset($ns['pid'])){
			$c = new Model_Prabasystem();
			if(isset($par['pid']) && $par['pid']!=""){
				$par['tpl'] = str_replace("{{pid}}",$par['pid'],$par['tpl']);
				$par['tpl2'] = str_replace("{{pid}}",$par['pid'],$par['tpl2']);
				$par["chart_id"] = $par['pid'];
				$ret = $c->add_update_portlet_bigchart($par,$p);
				$data['id'] = $ret['id'];
				$data['error'] = false;
			}else{
				$ret = $c->add_update_portlet_bigchart($par,$p);
				if($ret['result'] && isset($ret['id'])){
					$par['pid'] = $ret['id'];
					$par['tpl'] = str_replace("{{pid}}",$par['pid'],$par['tpl']);
					$par['tpl2'] = str_replace("{{pid}}",$par['pid'],$par['tpl2']);
					$par["chart_id"] = $par['pid'];
					// Zend_Debug::dump($par); die();
					$c->add_update_portlet_bigchart($par,$p);
					$data['id'] = $ret['id'];
					$data['error'] = false;
				}
			}
			// Zend_Debug::dump($ret); die();
		// }
	}
	header("Access-Control-Allow-Origin: *");
	header('Content-Type: application/json');
	// header('Content-Type: plain/text');
	$tmp = $this->restorearray((array)$data);
	// unset($tmp['code']['legend']);
	$data = json_decode(json_encode($tmp), true);
	// Zend_Debug::dump(json_last_error());die();
	echo json_encode($data);
	die();
}

public function importmodalAction() {
	$this->_helper->layout->disableLayout();
	header('Content-Type: application/vnd.groove-tool-template');
}

public function databuilderAction() {
	ini_set("memory_limit",-1);
	$this->_helper->layout->disableLayout();
	$result = array('retCode' => '10',
					'retMsg' => 'Invalid Parameter',
					'result' => false,
					'data' => null);
	$cc = new Model_System();
	if($_POST) {
		// Zend_Debug::dump($this);
		$POST = $this->restorearray($_POST);
		// Zend_Debug::dump($POST);
		// die();
		$data_api = array();
		foreach($POST as $k=>$v) {
			// Zend_Debug::dump($k);
			// Zend_Debug::dump($v);die();
			$tmp = $cc->exec_api($v['api'],$v['param']);
			// Zend_Debug::dump($tmp);die();
			// Zend_Debug::dump($v);die();
			$cnt = 0;
			if($tmp['transaction']){
				foreach ($tmp['data'] as $k2 => $v2) {
					// Zend_Debug::dump($v);
					// Zend_Debug::dump($v2);die();
					$data_api[$k2][$k] = isset($v2[$v['field']])?$v2[$v['field']]:null;
					// Zend_Debug::dump($data_api);die();
					$cnt++;
					if($cnt==10){
						break;
					}
				}
			}else{
				$result['retCode'] = 11;
				$result['retMsg'] = "Failed Call API";
				break;
			}
		}
	}
	header("Access-Control-Allow-Origin: *");
	header('Content-Type: application/json');
	$tmp = $this->restorearray((array)$data_api);
	$data_api = json_decode(json_encode($tmp), true);             
	echo json_encode($data_api);
	die();
}

public function editorAction() {
	$params = $this->getRequest()->getParams();
	// Zend_Debug::dump($params);// die();
	$ns = Zend_Session::namespaceGet('bigchart');
	// Zend_Debug::dump($ns); die();
	
	if(isset($params['pid'])) {
		$ns = new Zend_Session_Namespace('bigchart');
		$ns->id = mktime();
		$ns->pid = $params['pid'];
		$pid = $ns->pid;

        $cd = new Model_Zprabapage();
        $csci = $cd->get_portlet($ns->pid);

		// Zend_Debug::dump($ns);
		// Zend_Debug::dump($csci); die();
		if(isset($csci['portlet_type']) && $csci['portlet_type']!="bigchart"){
			Zend_Session::namespaceUnset('bigchart');
			$this->_redirect("/prabaeditor/listportlet");
		}else if(isset($csci['portlet_type']) && $csci['portlet_type']=="bigchart"){
			$new = false;
			// Zend_Debug::dump($csci); die();
			if ($csci['content_attrs'] == "" || $csci['content_attrs'] == null){
				$ns->on_edit = 0;
			}else{
				$dt = $csci['content_attrs'];
				// Zend_Debug::dump($dt); die();
				if(isset($dt['code']) && isset($dt['css']) && isset($dt['template']) && isset($dt['title']) && isset($dt['description']) && isset($dt['int_save']) && isset($dt['form_api'])){
					$new = false;
					$ns->cfg = array(
						"code"=>$dt['code'],
						"css"=>$dt['css'],
						"template"=>$dt['template'],
						"title"=>$dt['title'],
						"description"=>$dt['description'],
						"int_save"=>$dt['int_save'],
						"form_api"=>$dt['form_api']
					);
					$ns->on_edit = 1;
					$ns = Zend_Session::namespaceGet('bigchart');
					// Zend_Debug::dump($ns); die();
				}else{
					$ns->on_edit = 0;
					$ns = Zend_Session::namespaceGet('bigchart');
				}
			}
		}else {
			Zend_Session::namespaceUnset('bigchart');
			$this->_redirect("/prabaeditor/listportlet");
		}
	}else if($ns && isset($ns['pid'])){
		$pid = $ns['pid'];
        $cd = new Model_Zprabapage();
        $csci = $cd->get_portlet($ns['pid']);

		$uri = $this->getRequest()->getrequestUri();
		// Zend_Debug::dump($uri); die();
		// Zend_Debug::dump($csci); die();
		if(isset($csci['portlet_type']) && $csci['portlet_type']!="bigchart"){
			Zend_Session::namespaceUnset('bigchart');
			$this->_redirect("/prabaeditor/listportlet");
		}else if(isset($csci['portlet_type']) && $csci['portlet_type']=="bigchart"){   
			$new = false;               
			// Zend_Debug::dump($uri); die('www');
			if ($csci['content_attrs'] == "" || $csci['content_attrs'] == null){
				// Zend_Debug::dump($uri); die('qqq');
				$ns = new Zend_Session_Namespace('bigchart');
				$ns->on_edit = 0;
			}else if($csci['content_attrs'] != "" && $csci['content_attrs'] != null){
				// Zend_Debug::dump($uri); die('eeee');
				$dt = $csci['content_attrs'];
				if(isset($dt['code']) && isset($dt['css']) && isset($dt['template']) && isset($dt['title']) && isset($dt['description']) && isset($dt['int_save']) && isset($dt['form_api'])){
					$ns = new Zend_Session_Namespace('bigchart');
					$ns->cfg = array(
						"code"=>$dt['code'],
						"css"=>$dt['css'],
						"template"=>$dt['template'],
						"title"=>$dt['title'],
						"description"=>$dt['description'],
						"int_save"=>$dt['int_save'],
						"form_api"=>$dt['form_api']
					);
					$ns->on_edit = 1;
					$ns = Zend_Session::namespaceGet('bigchart');
				}else{
					$ns = new Zend_Session_Namespace('bigchart');
					$ns->on_edit = 0;
					$ns = Zend_Session::namespaceGet('bigchart');
				}
			}
		}else {
			Zend_Session::namespaceUnset('bigchart');
			$this->_redirect("/prabaeditor/listportlet");
		}
	}else{
		// Zend_Debug::dump($params); die();
		// die("qqq");
		// Zend_Session::namespaceUnset('bigchart');
		$new = true;
		Zend_Session::namespaceUnset('bigchart');
		$ns = new Zend_Session_Namespace('bigchart');
		$ns->id = mktime();
		$ns->listapi = array();
		$ns->cfg = array();
		$ns->on_edit = 0;
		$ns = Zend_Session::namespaceGet('bigchart');
	}
	// Zend_Debug::dump($ns); die();
	$ns = json_decode(json_encode($ns),true);
	// Zend_Debug::dump($ns); die();
	$this->view->ns = $ns;
	$dd = new Model_Prabasystemaddon();
	$apis = $dd->get_api_grouping();
	$this->data->listapi = $apis;
	$tmp = $this->restorearray((array)$apis);
	$data_api = json_decode(json_encode($tmp), true);  
	$this->view->headScript()->appendScript("var listapi = ".json_encode($data_api).";" );
	$this->view->headLink()->appendStylesheet('/live.bigcharts/vendor/jquery/css/ui-amcharts/jquery-ui-1.10.4.custom.css?d='.date("YmdHis"));
	$this->view->headLink()->appendStylesheet('/live.bigcharts/vendor/jquery/plugins/chosen/jquery.chosen.css?d='.date("YmdHis"));
	$this->view->headLink()->appendStylesheet('/live.bigcharts/vendor/jquery/plugins/spectrum/jquery.spectrum.css?d='.date("YmdHis"));
	$this->view->headLink()->appendStylesheet('/live.bigcharts/vendor/jquery/plugins/handsontable/jquery.handsontable.css?d='.date("YmdHis"));
	$this->view->headLink()->appendStylesheet('/live.bigcharts/vendor/jquery/plugins/handsontable/jquery.handsontable.removeRow.css?d='.date("YmdHis"));
	$this->view->headLink()->appendStylesheet('/live.bigcharts/vendor/bootstrap/css/bootstrap.css?d='.date("YmdHis"));
	$this->view->headLink()->appendStylesheet('/live.bigcharts/vendor/font-awesome/css/font-awesome.css?d='.date("YmdHis"));
	$this->view->headLink()->appendStylesheet('//fonts.googleapis.com/css?family=Covered+By+Your+Grace');

	$this->view->headLink()->appendStylesheet('/live.bigcharts/dist/css/ameditor.core.css?d='.date("YmdHis"));
	$this->view->headLink()->appendStylesheet('/live.bigcharts/dist/css/ameditor.icons.css?d='.date("YmdHis"));
	$this->view->headLink()->appendStylesheet('/live.bigcharts/dist/css/ameditor.table.css?d='.date("YmdHis"));
	$this->view->headLink()->appendStylesheet('/live.bigcharts/dist/css/ameditor.share.css?d='.date("YmdHis"));

	$this->view->headScript()->appendFile('/live.bigcharts/vendor/amcharts/amcharts.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/vendor/amcharts/funnel.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/vendor/amcharts/gauge.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/vendor/amcharts/pie.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/vendor/amcharts/radar.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/vendor/amcharts/serial.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/vendor/amcharts/xy.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/vendor/amcharts/themes/chalk.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/vendor/amcharts/themes/dark.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/vendor/amcharts/themes/light.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/vendor/amcharts/themes/patterns.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/vendor/amcharts/themes/black.js?d='.date("YmdHis"));

	$this->view->headLink()->appendStylesheet('/live.bigcharts/vendor/amcharts/plugins/export/export.css?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/vendor/amcharts/plugins/export/export.min.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/vendor/amcharts/plugins/dataloader/dataloader.min.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/vendor/amcharts/plugins/responsive/responsive.min.js?d='.date("YmdHis"));

	// $this->view->headScript()->appendFile('/live.bigcharts/vendor/jquery/jquery-1.10.2.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/vendor/jquery/jquery-ui-1.10.4.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/vendor/jquery/plugins/router/jquery.router.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/vendor/jquery/plugins/layout/jquery.layout.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/vendor/jquery/plugins/chosen/jquery.chosen.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/vendor/jquery/plugins/spectrum/jquery.spectrum.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/vendor/jquery/plugins/handsontable/jquery.handsontable.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/vendor/jquery/plugins/handsontable/jquery.handsontable.removeRow.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/vendor/modernizr/modernizr-2.8.1.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/vendor/bootstrap/js/bootstrap.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/vendor/clipboard/clipboard.js?d='.date("YmdHis"));

	$this->view->headScript()->appendFile('/live.bigcharts/dist/ameditor.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/dist/ameditor.config.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/dist/ameditor.core.js?d='.date("YmdHis"));
	$this->view->headScript()->appendScript("
		AmCharts.Editor.CONSTANT.URL_AMCHARTS = '/bigchart/';
		AmCharts.Editor.CONSTANT.URL_BASE  = '/live.bigcharts/';
		AmCharts.Editor.CONSTANT.URL_CDN = '/live.bigcharts/';
		AmCharts.Editor.CONSTANT.URL_CDN_AMCHARTS = '/live.bigcharts/';
		//AmCharts.Editor.CONSTANT.URL_API =  '/live.bigcharts/wp-admin/admin-ajax.php';
		AmCharts.Editor.CONSTANT.URL_API =  '/bigchart/adminajax';
		AmCharts.Editor.CONSTANT.URL_PATH = '/bigchart/';
		AmCharts.Editor.CONSTANT.RND_KEY  = '".date('Ymd-His')."';
		AmCharts.Editor.CONSTANT.COVER = '/live.bigcharts/static/img/cover/9.JPG';"
	);
	$this->view->headScript()->appendFile('/live.bigcharts/dist/ameditor.handler.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/dist/ameditor.router.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/dist/ameditor.samples.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/dist/lang/ameditor.en-US.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/dist/section/ameditor.landing.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/dist/section/ameditor.editor.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.bigcharts/dist/section/ameditor.share.js?d='.date("YmdHis"));

	$this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
	$this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
	$this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
	$this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.min.js');
	$this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
	$this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css');
	$this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js');
}

public function restorearray($data = array()){
	$dup = array();
	foreach ($data as $key => $value) {
		if(is_array($value)){
			$tmp = $this->restorearray($value);
			$dup[$key] = $tmp;
		}else if(is_object($value)){
			$tmp = $this->restorearray((array)$value);
			$dup[$key] = $tmp;
		}else{
			if(is_numeric($value)){
				if(substr($value,0,1)=="0"){
					// Zend_Debug::dump(strpos($value,".",1));
					if(strpos($value,".",1)==false){
						$dup[$key] = $value;
					}else{
						$dup[$key] = $value+0;
					}
				}else{
					$dup[$key] = $value+0;
				}
			}else if($value=="true"){
				$dup[$key] = true;
			}else if($value=="false"){
				$dup[$key] = false;
			}else{
				$dup[$key] = $value;
			}
			// Zend_Debug::dump($dup[$key]);
		}
	}
	return $dup;
}

}