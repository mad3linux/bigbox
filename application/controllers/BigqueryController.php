<?php

/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class BigqueryController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction(){

        // $text = explode(' ', $text);
        // $text = array_map('trim',array_filter($text));
        // $tmp = preg_grep('~fa-~', $text);
        // // Zend_Debug::dump($text);die();

        // $text = array();
        // foreach($tmp as $val){
        //     if(substr($val, 0,3)=='fa-'){
        //         // $text[] = substr($val, 0,-6);
        //         echo "'fa ".substr($val, 0,-6)."',\n";
        //     }
        // }
        // die();

        $params = $this->getRequest()->getParams();

        $z = new Model_Cache();

        if($_POST){
            $this->_helper->layout->disableLayout();
            $exe = new Model_Zpraba4api ();
            $dd = new Model_Zprabapage ();
            if( isset($params['ajax']) && (int)$params['ajax']==1){
                $arr = array(
                    230=>'getDescribes',
                    231=>'getProperties',
                    232=>'getSamples',
                    233=>'getCount',
                );
                $result = array();
                foreach ($arr as $api_id => $func) {
                    $varC = $dd->get_api($api_id);
                    $conn = unserialize($varC ['conn_params']);
                    $result[$api_id] = $exe->execute_api($conn, $varC, array());
                    
                    $input = array(
                        'function'=>$func,
                        'database_name'=>$_POST['database_name'],
                        'table_name'=>$_POST['table_name'],
                    );
                    $cid = $z->get_id('Hive',$input);
                    $file = APPLICATION_PATH.'/../cache/zend_cache---'.$cid;
                    if(file_exists($file)){
                        // Zend_Debug::dump(date('Y-m-d H:i:s',filemtime($file)));die();
                        $result[$api_id]['last_cache'] = date('Y-m-d H:i:s',filemtime($file));
                    }   



                    if($api_id==231){

                        $prpt_name = array_column($result[231]['data'], 'prpt_name');
                        $key = array_search('transient_lastDdlTime',$prpt_name);
                        // Zend_Debug::dump($result[231]['data'][$key]['prpt_value']);

                        $lastDdlTime = (int)trim($result[231]['data'][$key]['prpt_value']);
                        $lastDdlTime = date('Y-m-d H:i:s',$lastDdlTime);
                        $result[231]['data'][$key]['prpt_value'] = $lastDdlTime;
                        // die();
                    } else if($api_id==233){
                        $result[233]['data'] = number_format($result[233]['data'],0,'.',',');
                    }
                }
                header("Access-Control-Allow-Origin: *");
                header('Content-Type: application/json');
                echo json_encode($result);
                die();
            } else if(isset($params['ajax']) && (int)$params['ajax']==2){

                // Zend_Debug::dump($_POST);//die();

                // $qryLower = strtolower($_POST['qry']);
                // Zend_Debug::dump($_POST);//die();

                // $api_id = 244;
                // if(strpos($qryLower, 'select')!==FALSE){
                    $api_id = 234;
                // }

                // Zend_Debug::dump($api_id);die();

                $varC = $dd->get_api($api_id);
                $conn = unserialize($varC ['conn_params']);
                $result = $exe->execute_api($conn, $varC, array());
                $tmpAoColumns = array_keys($result['data'][0]);
                // Zend_Debug::dump($tmpAoColumns);die();

                $alias = (strpos($aoColumns[0],'a.')!==FALSE?TRUE:FALSE);

                $aoColumns = $tmpAoColumns;
                if($alias){
                    $aoColumns = array();
                    foreach ($tmpAoColumns as $column) {
                        $aoColumns[] = substr($column, 2);
                    }
                }

                $aaData = array();
                foreach ($result['data'] as $k => $v) {
                    foreach ($aoColumns as $key => $value) {
                        $aaData[$k][$key] = ($v[$value]?$v[$value]:$v['a.'.$value]);
                    }
                }

                // Zend_Debug::dump($aaData);die();

                $result['tabId'] = (int)$_POST['tabId'];
                $result['parseData']['aaData'] = $aaData;
                $result['parseData']['aoColumns'] = $aoColumns;
                $result['parseData']['displayStart'] = (int)$_POST['offset'];

                header("Access-Control-Allow-Origin: *");
                header('Content-Type: application/json');
                echo json_encode($result);
                die();
            } else if(isset($params['ajax']) && (int)$params['ajax']==3){
                $varC = $dd->get_api(235);
                $conn = unserialize($varC ['conn_params']);
                $result = $exe->execute_api($conn, $varC, array());
                // Zend_Debug::dump($result);die();

                header("Access-Control-Allow-Origin: *");
                header('Content-Type: application/json');
                echo json_encode($result);
                die();
            }

            $result = array(
                'transaction'=>false,
                'message'=>'invalid identifier',
            );
            header("Access-Control-Allow-Origin: *");
            header('Content-Type: application/json');
            echo json_encode($result);
            die();   
        }


        $allow_datatypes = array(
            'INT',
            'TINYINT',
            'SMALLIINT',
            'BIGINT',
            'BOOLEAN',
            'FLOAT',
            'DOUBLE',
            'STRING',
            'TIMESTAMP',
            'BINARY',
            'ARRAY',
            'MAP',
            'STRUCT',
            'UNION',
            'DECIMAL',
            'CHAR',
            'VARCHAR',
            'DATE',
        );

        $allow_datatypes = array_map('strtolower', $allow_datatypes);
        
        $cid = '279b1958a0062c5dddfa96292bc3cb84';
        
        $cache = $z->cachefunc(86400);
        $tree = $z->get_cache($cache, $cid);
        // Zend_Debug::dump($tree);die();  

        $hintOptions = array(
            'tables'=>array('SELECT * FROM'=>array()),
        );

        foreach ($tree as $k => $v) {
            foreach ($v['tables'] as $k1 => $v1) {
                $hintOptions['tables']['FROM '.$v['database_name'].'.'] = array();
                $hintOptions['tables'][$v['database_name']] = array();
                $hintOptions['tables'][$v['tab_name']] = array();
                foreach ($v1['columns'] as $k2 => $v2) {
                    $hintOptions['tables'][$v['tab_name']][] = $v2['col_name'];
                    $hintOptions['tables'][$v['database_name'].'.'.$v1['tab_name']][] = $v2['col_name'];
                }
            }
        }

        $this->view->allow_datatypes = $allow_datatypes;
        $this->view->tree = $tree;
        $this->view->hintOptions = json_encode($hintOptions);
        $this->view->dbtbcl = json_encode($dbtbcl);



        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        $this->view->headLink ()->appendStylesheet ( '/assets/core/global/plugins/select2/css/select2.min.css');
        $this->view->headLink ()->appendStylesheet ( '/assets/core/global/plugins/select2/css/select2-bootstrap.min.css');
        $this->view->headLink ()->appendStylesheet ( '/assets/bigbox/plugins/codemirror-5.33.0/lib/codemirror.css');
        $this->view->headLink ()->appendStylesheet ( '/assets/bigbox/plugins/codemirror-5.33.0/addon/hint/show-hint.css');
        $this->view->headLink ()->appendStylesheet ( '/assets/bigbox/css/bigquery_index.css');

        $this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery-slimscroll/jquery.slimscroll.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/scripts/datatable.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/select2/js/select2.full.min.js');
        $this->view->headScript ()->appendFile ( '/assets/bigbox/plugins/codemirror-5.33.0/lib/codemirror.js');
        $this->view->headScript ()->appendFile ( '/assets/bigbox/plugins/codemirror-5.33.0/mode/sql/sql.js');
        $this->view->headScript ()->appendFile ( '/assets/bigbox/plugins/codemirror-5.33.0/addon/display/placeholder.js');
        $this->view->headScript ()->appendFile ( '/assets/bigbox/plugins/codemirror-5.33.0/addon/hint/show-hint.js');
        $this->view->headScript ()->appendFile ( '/assets/bigbox/plugins/codemirror-5.33.0/addon/hint/sql-hint.js');
        $this->view->headScript ()->appendFile ( '/assets/bigbox/plugins/codemirror-5.33.0/addon/selection/active-line.js');
        $this->view->headScript ()->appendFile ( '/assets/bigbox/js/bigquery_index.js');
    }

    public function zeppelinAction(){
        // $username = 'admin';
        // $password = 'admin';
        // $url = 'http://10.2.147.127:9995/#/notebook/2D69EMKVA/paragraph/20180221-130241_2039629991?asIframe';
         
        // $context = stream_context_create(array(
        //     'http' => array(
        //         'header'  => "Authorization: Basic " . base64_encode("$username:$password")
        //     )
        // ));
        // $data = file_get_contents($url, false, $context);
        // $data = str_replace('="styles/', '="http://10.2.147.127:9995/styles/', $data);
        // $data = str_replace('src="scripts/', 'src="http://10.2.147.127:9995/scripts/', $data);
        // $data = str_replace('src="\'components/', 'src="\'http://10.2.147.127:9995/components/', $data);
        // Zend_Debug::dump($data);die();

        // $this->view->data = $data;
    }

    public function solrAction(){
        // die('slr');
    }
}
