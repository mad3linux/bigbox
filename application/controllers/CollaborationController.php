<?php

/*
 * Copyright (C) Prabtech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>,
 */

class CollaborationController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    function __getExtension($str) {
        $i = strrpos($str, ".");
        if (!$i) {
            return "";
        }
        $l = strlen($str) - $i;
        $ext = substr($str, $i + 1, $l);
        return $ext;
    }

    public function discussandshareAction() {
        $params = $this->getRequest()->getParams();

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        //Zend_Debug::dump($identity);die();

        $mdl_zwall = new Model_Zwall();
        $mdl_gparam = new Model_Gparams();

        if (isset($_POST) && count($_POST) > 0) {
            //Zend_Debug::dump($_FILES);
            //Zend_Debug::dump($_POST);die();

            $data = array(
                'xhpc_message' => '',
                'gid' => $_POST['gid']
            );
            if (isset($_FILES["wallimg"])) {
                $swap = "";
                define("MAX_SIZE", "600");
                $errors = 0;
                $image = $_FILES["wallimg"]["name"];

                //die($image);
                $uploadedfile = $_FILES['wallimg']['tmp_name'];
                if ($image) {
                    $filename = stripslashes($_FILES['wallimg']['name']);
                    $extension = $this->__getExtension($filename);
                    $extension = strtolower($extension);
                    //die($extension);

                    if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) {
                        $swap = '<div class="msgdiv">Unknown Image extension </div> ';
                        $errors = 1;
                    } else {
                        $size = filesize($_FILES['wallimg']['tmp_name']);
                        if ($size > MAX_SIZE * 1024) {
                            $errors = 1;
                        }

                        if ($extension == "jpg" || $extension == "jpeg") {
                            $uploadedfile = $_FILES['wallimg']['tmp_name'];
                            $src = imagecreatefromjpeg($uploadedfile);
                        } else if ($extension == "png") {
                            $uploadedfile = $_FILES['wallimg']['tmp_name'];
                            $src = imagecreatefrompng($uploadedfile);
                        } else {
                            $src = imagecreatefromgif($uploadedfile);
                        }

                        list($width, $height) = getimagesize($uploadedfile);

                        $newwidth = 300;
                        $newheight = ($height / $width) * $newwidth;
                        $tmp = imagecreatetruecolor($newwidth, $newheight);
                        imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                        $filename = "/var/www/zendrafinaru/public/uploads/images/" . $_FILES['wallimg']['name'];

                        imagejpeg($tmp, $filename, 100);
                        imagedestroy($src);
                        imagedestroy($tmp);
                        $data['xhpc_message'].='<div style="text-align:center;margin:0px auto">
							<img src="http://telkomcare.telkom.co.id/uploads/images/' . $_FILES['wallimg']['name'] . '" style="text-align:center;margin:0px auto"/>
						</div><br><hr>';
                    }
                }
            }

            if (isset($_POST['wall'])) {
                $data['xhpc_message'].=$_POST['wall'];
            }
            //Zend_Debug::dump($data);die();
            $mdl_zwall->insert_wall($data, $identity->uid, true);

            $url = $_SERVER['REQUEST_URI'];
            $this->_redirect('/collaboration/discussandshare/gid/' . (int) $_POST['gid'] . '/gname/' . $_POST['group_name']);
        }

        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-fileupload/bootstrap-fileupload.css');
        $this->view->headLink()->appendStylesheet('/assets/core/skins/default/css/pages/timeline.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-fileupload/bootstrap-fileupload.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');

        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/ui-extended-modals.js');

        $groups = $mdl_zwall->get_allgroups_by_uid($identity->uid);
        //Zend_Debug::dump($groups);die();
        $gparam = $mdl_gparam->getall_by_param("WALL MSG TYPE");
        //Zend_Debug::dump($gparam);die();
        $gpar = array();
        foreach ($gparam as $k => $v) {
            $gpar[$v['display_param']] = $v;
        }
        if (!isset($params['gid'])) {
            $this->_redirect('/collaboration/discussandshare/gid/' . (int) $groups[0]['gid'] . '/gname/' . $groups[0]['group_name']);
        }

        $walls = array();
        $walls = $mdl_zwall->get_wallgroups($params['gid']);
        //Zend_Debug::dump($walls);die();
        //Zend_Debug::dump($gpar);die();
        $this->view->groups = $groups;
        $this->view->walls = $walls;
        $this->view->params = $params;
        $this->view->gpar = $gpar;
        $this->view->me = $identity;
    }

    public function commentsAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $mdl_zwall = new Model_Zwall();
        $mdl_gparam = new Model_Gparams();
        $comments = array();
        if (isset($params['wid'])) {
            $comments = $mdl_zwall->get_comments($params['wid']);
            //Zend_Debug::dump($comments);die();
        }

        if (isset($_POST) && count($_POST) > 0) {
            //Zend_Debug::dump($_FILES);
            //Zend_Debug::dump($_POST);die();

            $data = array(
                'comment' => '',
                'mgid' => $_POST['mgid']
            );

            if (isset($_POST['comment'])) {
                $data['comment'].=$_POST['comment'];
            }
            //Zend_Debug::dump($data);die('wwww');
            $mdl_zwall->insert_wall_comment($data, $identity->uid, true);

            header("Access-Control-Allow-Origin: *");
            header('Content-Type: application/json');
            echo json_encode(array('result' => true));
            die();
        }

        $gparam = $mdl_gparam->getall_by_param("WALL MSG TYPE");
        //Zend_Debug::dump($gparam);die();
        $gpar = array();
        foreach ($gparam as $k => $v) {
            $gpar[$v['display_param']] = $v;
        }

        $this->view->comments = $comments;
        $this->view->params = $params;
        $this->view->gpar = $gpar;
    }

    public function commentsajaxAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $mdl_zwall = new Model_Zwall();
        $mdl_gparam = new Model_Gparams();
        $comments = array();
        if (isset($params['wid'])) {
            $comments = $mdl_zwall->get_comments($params['wid']);
            //Zend_Debug::dump($comments);die();
        }
        $gparam = $mdl_gparam->getall_by_param("WALL MSG TYPE");
        //Zend_Debug::dump($gparam);die();
        $gpar = array();
        foreach ($gparam as $k => $v) {
            $gpar[$v['display_param']] = $v;
        }
        foreach ($comments as $k => $v) {
            echo '<li class="' . (($v['uid'] == $params['uid']) ? 'in' : 'out') . '">
					<img class="avatar img-responsive" alt="" src="/assets/core/skins/default/img/avatar.png"/>
					<div class="message">
						<span class="arrow">
						</span>
						<a href="#" class="name">' . $v['fullname'] . '</a>
						<span class="datetime">
							 at ' . $v['created_date'] . '
						</span>
						<span class="body">
							' . $v['comment_text'] . '
						</span>
					</div>
				</li>';
        }
        die();
    }

}
