<?php

/*
 * Copyright (C) Prabtech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>,
 */

@ini_set("memory_limit", "-1");

class Pm_CronController extends Zend_Controller_Action {

    public function performansiweekAction() {

        $date = date("d/m/y G:i");
        $cc = new Pm_Model_Pmperformance();
        $data = $cc->getPerformanceWeek();

        //Zend_Debug::dump($data);die(); 

        foreach ($data as $k => $dat) {


            $lnwitel = $cc->get_sender_by_witel($k);
            //Zend_Debug::dump($lnwitel);die();
            foreach ($lnwitel as $dat2) {

                //$message= "WITEL: ";
                $message = "WITEL: " . $k . ", Posisi Tgl: " . $date . ", Minggu Ini, Total Closed :" . $dat["total_closed"] . ", MTTP Alpro Ready: " . $dat["mttp_nonjt"] . " Hari, MTTP Alpro Not Ready: " . $dat["mttp_jt"] . " Hari, TTP Comply: " . $dat["ttp_nonjt"] . "%, MTTI: " . $dat["mtti"] . " Hari, TTI Comply: " . $dat["ttic"] . "%, Detail order via email dan aplikasi SPEED (speed.telkom.co.id) ";


                $mail = "WITEL: " . $k . ", Posisi Tgl: " . $date . " Untuk Minggu Ini <br> MTTP Alpro Ready: " . $dat["mttp_nonjt"] . " Hari <br> Total Closed :" . $dat["total_closed"] . "<br>MTTP Alpro Not Ready: " . $dat["mttp_jt"] . " Hari <br>TTP Comply: " . $dat["ttp_nonjt"] . "% <br> MTTI: " . $dat["mtti"] . " Hari <br> TTI Comply: " . $dat["ttic"] . "% <br>";


                $this->sending_sms($dat2['NOTEL'], $message);

                $this->sending_mail_raw($dat2['EMAIL'], $dat2['NAMA'], $mail, 'ALERT PERFORMANSI  MINGGUAN [WITEL :' . $k . ']');
            }
            //die ($mtti);
        }

        die("SENT");
    }

    public function performansimonthAction() {


        $date = date("d/m/y G:i");
        $cc = new Pm_Model_Pmperformance();
        $data = $cc->getPerformancemonth();

        //Zend_Debug::dump($data); die();		


        foreach ($data as $k => $dat) {


            $lnwitel = $cc->get_sender_by_witel($k);
            //Zend_Debug::dump($lnwitel);die();
            foreach ($lnwitel as $dat2) {

                //$message= "WITEL: ";
                $message = "WITEL: " . $k . ", Posisi Tgl: " . $date . ", Bulan Ini,Total Closed: " . $dat["total_closed"] . "; MTTP Alpro Ready: " . $dat["mttp_nonjt"] . " Hari, MTTP Alpro Not Ready: " . $dat["mttp_jt"] . " Hari, TTP Comply: " . $dat["ttp_nonjt"] . "%, MTTI: " . $dat["mtti"] . " Hari, TTI Comply: " . $dat["ttic"] . "%, Detail order via email dan aplikasi SPEED (speed.telkom.co.id) ";

                $mail = "WITEL: " . $k . ", Posisi Tgl: " . $date . ", Bulan Ini <br>Total Closed: " . $dat["total_closed"] . "<br> MTTP Alpro Ready: " . $dat["mttp_nonjt"] . " Hari<br>MTTP Alpro Not Ready: " . $dat["mttp_jt"] . " Hari<br> TTP Comply: " . $dat["ttp_nonjt"] . "%<br> MTTI: " . $dat["mtti"] . " Hari<br> TTI Comply: " . $dat["ttic"] . "%<br>";


                $this->sending_sms($dat2['NOTEL'], $message);

                $this->sending_mail_raw($dat2['EMAIL'], $dat2['NAMA'], $mail, 'ALERT PERFORMANSI  BULANAN [WITEL :' . $k . ']');
                //$this->sending_mail_raw($dat2['EMAIL'], $dat2['NAMA'], $message);
                //echo " To:".$dat2['NOTEL']." text: ".$message;
            }
            die("SENT");
        }
    }

    public function performansi3monthsAction() {

        $date = date("d/m/y G:i");
        $cc = new Pm_Model_Pmperformance();
        //die("s");
        $data = $cc->getPerformance3Months();

        //Zend_Debug::dump($data); die();		

        foreach ($data as $k => $dat) {


            $lnwitel = $cc->get_sender_by_witel($k);

            foreach ($lnwitel as $dat2) {

                //$message= "WITEL: ";
                $message = "WITEL: " . $k . ", Posisi Tgl: " . $date . ", Minggu Ini, Total Closed : " . $dat["total_closed"] . ", MTTP Alpro Ready: " . $dat["mttp_nonjt"] . " Hari, MTTP Alpro Not Ready: " . $dat["mttp_jt"] . " Hari, TTI Comply: " . $dat["ttp_nonjt"] . "%, MTTI: " . $dat["mtti"] . " Hari, TTI Comply: " . $dat["ttic"] . "%, Detail order via email dan aplikasi SPEED (speed.telkom.co.id) ";
                //echo ($message); die();

                $mail = "WITEL: " . $k . ", Posisi Tgl: " . $date . ", Triwulan Ini<br>Total Closed : " . $dat["total_closed"] . "<br> MTTP Alpro Ready: " . $dat["mttp_nonjt"] . " Hari<br> MTTP Alpro Not Ready: " . $dat["mttp_jt"] . " Hari<br>TTI Comply: " . $dat["ttp_nonjt"] . "%<br> MTTI: " . $dat["mtti"] . " Hari<br>TTI Comply: " . $dat["ttic"] . "%<br>";



                $this->sending_sms($dat2['NOTEL'], $message);


                $this->sending_mail_raw($dat2['EMAIL'], $dat2['NAMA'], $mail, 'ALERT PERFORMANSI 3 BULANAN [WITEL :' . $k . ']');
            }
        }

        die("SENT");
    }

    public function performansiweekallAction() {

        $date = date("d/m/y G:i");
        $cc = new Pm_Model_Pmperformance();
        $data = $cc->getPerformanceWeekAll();

        Zend_Debug::dump($data);
        die();

        foreach ($data as $dat) {


            $lnwitel = $cc->get_sender_by_witel($dat['witel']);
            //Zend_Debug::dump($lnwitel);die();
            foreach ($lnwitel as $dat2) {

                //$message= "WITEL: ";
                $message = "WITEL: " . $dat['witel'] . ", Posisi Tgl: " . $date . ", Minggu Ini, MTTP Alpro Ready: " . $dat["mttp_nonjt"] . " Hari, MTTP Alpro Not Ready: " . $dat["mttp_jt"] . " Hari, TTP Comply: " . $dat["ttp_nonjt"] . "%, MTTI: " . $dat["mtti"] . " Hari, TTI Comply: " . $dat["ttic"] . "%, Detail order via email dan aplikasi SPEED (speed.telkom.co.id) ";


                $mail = "WITEL: " . $dat['witel'] . ", Posisi Tgl: " . $date . " Untuk Minggu Ini <br> MTTP Alpro Ready: " . $dat["mttp_nonjt"] . " Hari <br> MTTP Alpro Not Ready: " . $dat["mttp_jt"] . " Hari <br>TTP Comply: " . $dat["ttp_nonjt"] . "% <br> MTTI: " . $dat["mtti"] . " Hari <br> TTI Comply: " . $dat["ttic"] . "% <br>";


                $this->sending_sms($dat2['NOTEL'], $message);

                $this->sending_mail_raw($dat2['EMAIL'], $dat2['NAMA'], $mail, 'ALERT PERFORMANSI  MINGGUAN [WITEL :' . $dat['witel'] . ']');
                //$this->sending_mail_raw($dat2['EMAIL'], $dat2['NAMA'], $message);
                //echo " To:".$dat2['NOTEL']." text: ".$message;
            }
            //die ($mtti);
        }

        die("SENT");
    }

    public function eskalasikawasanAction() {

        $date = date("d/m/y G:i");
        $cc = new Pm_Model_Pmeskalasi();
        $sum_kawasan = $cc->summary_kawasan();
        $datakawasan = $cc->get_kawasan();
        $detilkawasan = $cc->get_detail_kawasan();
        $vfh = array();
        foreach ($sum_kawasan as $k => $v) {
            $vfh[$v['WITEL_ALIAS']] = $v;
        }
        $datax = array();
        foreach ($datakawasan as $kk => $vv) {
            $datax[$vv['WITEL']][$vv['GROUPING_UNIT']] = $vv;
        }

        foreach ($datax as $kk => $vj) {

            if ($vj['BELUM ADA ALOKASI JARINGAN']['JML'] == "") {
                $vj['BELUM ADA ALOKASI JARINGAN']['JML'] = 0;
            }
            if ($vj['JT']['JML'] == "") {
                $vj['JT']['JML'] = 0;
            }
            if ($vj['BELUM INSTALL MODEM']['JML'] == "") {
                $vj['BELUM INSTALL MODEM']['JML'] = 0;
            }


            $mes[$kk] = 'ALERT OGP Order DATIN posisi witel ' . $kk . ' ' . $date . '; Total Order : ' . $vfh[$kk]['TOTAL'] . ';WORKGROUP : BELUM ADA ALOKASI JARINGAN=' . $vj['BELUM ADA ALOKASI JARINGAN']['JML'] . '; JT=' . $vj['JT']['JML'] . '; BELUM INSTALL MODEM=' . $vj['BELUM INSTALL MODEM']['JML'] . ';  Durasi : 0-5 hari =' . $vfh[$kk]['KURANGLIMA'] . '; 5-10 hari =' . $vfh[$kk]['LIMASEPULUH'] . '; >10 hari=' . $vfh[$kk]['LEBIHSEPULUH'] . '; Detail order via email dan aplikasi SPEED (speed.telkom.co.id)';

            $mail[$kk] = '<b>ALERT OGP ORDER DATIN POSISI WITEL ' . $kk . ' ' . $date . '</b> <br> Total Order : ' . $vfh[$kk]['TOTAL'] . '<br><br> <b>WORKGROUP :</b><br> BELUM ADA ALOKASI JARINGAN=' . $vj['BELUM ADA ALOKASI JARINGAN']['JML'] . '<br>JT=' . $vj['JT']['JML'] . '<br> BELUM INSTALL MODEM=' . $vj['BELUM INSTALL MODEM']['JML'] . '<br> <br><b>DURASI :</b><br>0-5 hari =' . $vfh[$kk]['KURANGLIMA'] . '<br>5-10 hari =' . $vfh[$kk]['LIMASEPULUH'] . '<br> >10 hari=' . $vfh[$kk]['LEBIHSEPULUH'] . '<br><br><b>DETAIL ORDER </b> <br>';


            $mail[$kk] .= '<table border="1" cellpadding="0" cellspacing="0" ><tr><td>NO</td><td>NO ORDER </td><td>LOCATION </td><td>CUSTOMER LOCATION</td><td>SO NUMBER</td><td>DURASI (HARI)</td><td>WORKGROUP TENOSS</td><td>WORKGROUP TASK</td> </tr>';
            $i = 1;

            foreach ($detilkawasan[$kk] as $k => $vv) {

                $mail[$kk] .='<tr><td>' . $i . '</td><td>' . $vv['NO_ORDER'] . '</td><td>' . $vv['CUSTOMER_LOCATION'] . '</td><td>' . $vv['CUSTOMER_NAME'] . '</td><td>' . $vv['SO_NUMBER'] . '</td><td>' . number_format($vv['LAMA_HARI'], 2, ',', '') . '</td><td>' . $vv['WORKGROUP'] . '</td><td>' . $vv['GROUPING_UNIT'] . '</td></tr>';

                $i++;
            }
            $mail[$kk] .='</table>';
            $vgf = $cc->get_sender_by_witel($kk);

            foreach ($vgf as $f) {
                $this->sending_sms($f['NOTEL'], $mes[$kk]);
                $this->sending_mail_raw($f['EMAIL'], $f['NAMA'], $mail[$kk], 'ESKALASI SPEED [WITEL :' . $kk . ']');
            }
        }

        die("SENT");
    }

    public function eskalasinetbroAction() {

        $date = date("d/m/y G:i");
        $cc = new Pm_Model_Pmeskalasi();

        $data2 = $cc->get_non_kawasan();
        $sum_netbro = $cc->summary_netbro();

        $detilnetbro = $cc->get_detail_netbro();

        foreach ($data2 as $kk => $vv) {
            $data3[$vv['GROUPING_UNIT']] = $vv;
        }


        if ($data3['ICM']['JML'] == "") {
            $data3['ICM']['JML'] = 0;
        }

        if ($data3['RCS']['JML'] == "") {
            $data3['RCS']['JML'] = 0;
        }
        if ($data3['EOC']['JML'] == "") {
            $data3['EOC']['JML'] = 0;
        }

        $total_netbro = $data3['EOC']['JML'] + $data3['RCS']['JML'] + $data3['ICM']['JML'];
        $mess['netbro'] = 'ALERT OGP Order DATIN posisi NETBRO ' . $date . '; Total Order : ' . $total_netbro . '; Workgroup : ICM=' . $data3['ICM']['JML'] . '; RCS=' . $data3['RCS']['JML'] . '; EOC=' . $data3['EOC']['JML'] . '; Durasi : 0-1 hari =' . $sum_netbro['KURANGSATU'] . '; 1-2 hari =' . $sum_netbro['SATUDAN2'] . '; >2 hari=' . $sum_netbro['LEBIHDUA'] . '; Detail order via email dan aplikasi SPEED (speed.telkom.co.id)';



        $mail['netbro'] = '<b>ALERT OGP Order DATIN posisi NETBRO ' . $date . '</b> <br> Total Order : ' . $total_netbro . ' <br> <br> Workgroup :<br> ICM=' . $data3['ICM']['JML'] . ' <br> RCS=' . $data3['RCS']['JML'] . ' <br> EOC=' . $data3['EOC']['JML'] . ' <br> <br> Durasi : <br> 0-1 hari =' . $sum_netbro['KURANGSATU'] . ' <br> 1-2 hari =' . $sum_netbro['SATUDAN2'] . ' <br> >2 hari=' . $sum_netbro['LEBIHDUA'] . ' <br><br>  <b>DETAIL ORDER</b> <br>';


        $mail['netbro'] .= '<table border="1" cellpadding="0" cellspacing="0" ><tr><td>NO</td><td>NO ORDER </td><td>LOCATION </td><td>CUSTOMER LOCATION</td><td>SO NUMBER</td><td>DURASI (HARI)</td><td>WORKGROUP TENOSS</td><td>WORKGROUP TASK</td><td>JENIS LAYANAN</td> </tr>';
        $i = 1;
        foreach ($detilnetbro as $kk => $vv) {

            $mail['netbro'] .='<tr><td>' . $i . '</td><td>' . $vv['NO_ORDER'] . '</td><td>' . $vv['CUSTOMER_LOCATION'] . '</td><td>' . $vv['CUSTOMER_NAME'] . '</td><td>' . $vv['SO_NUMBER'] . '</td><td>' . number_format($vv['LAMA_HARI'], 2, ',', '') . '</td><td>' . $vv['WORKGROUP'] . '</td><td>' . $vv['GROUPING_UNIT'] . '</td><td>' . $vv['SERVICE_TYPE'] . '</td></tr>';

            $i++;
        }
        $mail['netbro'] .='</table>';



        $dnb = $cc->get_sender_by_unit('DNB');
        foreach ($dnb as $kk => $vv) {

            $this->sending_sms($vv['NOTEL'], $mess['netbro']);
            $this->sending_mail_raw($vv['EMAIL'], $vv['NAMA'], $mail['netbro'], 'ESKALASI SPEED [NETBRO]');
        }



        die("SENT");
    }

    public function eskalasidesAction() {
        $date = date("d/m/y G:i");
        $cc = new Pm_Model_Pmeskalasi();
        $data2 = $cc->get_non_kawasan();
        $sum_des = $cc->summary_des();
        $detildes = $cc->get_detail_des();
        foreach ($data2 as $kk => $vv) {
            $data3[$vv['GROUPING_UNIT']] = $vv;
        }

        //sending DWS

        $mess['dws'] = 'ALERT OGP Order DATIN posisi DES-FULL ' . $date . '; Total Order : ' . $sum_des['TOTAL'] . '; Workgroup : LOCAL ACCESS REQUEST=' . $data3['LOCAL ACCESS REQUEST']['JML'] . '; CP ALLOCATION=' . $data3['CP ALLOCATION']['JML'] . '; ACTIVATION ORDER=' . $data3['ACTIVATION ORDER']['JML'] . ';IKG CUSTOMER=' . $data3['IKG CUSTOMER']['JML'] . ';  Durasi : 0-1 hari =' . $sum_des['KURANGSATU'] . '; 1-2 hari =' . $sum_des['SATUDUA'] . '; 2->10 hari=' . $sum_des['DUASEPULUH'] . '; >10 hari=' . $sum_des['LEBIHSEPULUH'] . '; Detail order via email dan aplikasi SPEED (speed.telkom.co.id)';


        $mail['dws'] = '<b>ALERT OGP Order DATIN posisi DES-FULL ' . $date . '</b> <br>Total Order : ' . $sum_des['TOTAL'] . '<br><br> WORKGROUP :<br> LOCAL ACCESS REQUEST=' . $data3['LOCAL ACCESS REQUEST']['JML'] . '<br>CP ALLOCATION=' . $data3['CP ALLOCATION']['JML'] . '<br> ACTIVATION ORDER=' . $data3['ACTIVATION ORDER']['JML'] . '<br>IKG CUSTOMER=' . $data3['IKG CUSTOMER']['JML'] . '<br><br>DURASI :<br>0-1 hari =' . $sum_des['KURANGSATU'] . '<br>1-2 hari =' . $sum_des['SATUDUA'] . '<br> 2->10 hari=' . $sum_des['DUASEPULUH'] . '<br> >10 hari=' . $sum_des['LEBIHSEPULUH'] . '<br><br> <b>DETAIL ORDER</b>';




        $mail['dws'] .= '<table border="1" cellpadding="0" cellspacing="0" ><tr><td>NO</td><td>NO ORDER </td><td>LOCATION </td><td>CUSTOMER LOCATION</td><td>SO NUMBER</td><td>DURASI (HARI)</td><td>WORKGROUP TENOSS</td><td>WORKGROUP TASK</td><td>JENIS LAYANAN</td> </tr>';
        $i = 1;
        foreach ($detildes as $kk => $vv) {

            $mail['dws'] .='<tr><td>' . $i . '</td><td>' . $vv['NO_ORDER'] . '</td><td>' . $vv['CUSTOMER_LOCATION'] . '</td><td>' . $vv['CUSTOMER_NAME'] . '</td><td>' . $vv['SO_NUMBER'] . '</td><td>' . number_format($vv['LAMA_HARI'], 2, ',', '') . '</td><td>' . $vv['WORKGROUP'] . '</td><td>' . $vv['GROUPING_UNIT'] . '</td><td>' . $vv['SERVICE_TYPE'] . '</td></tr>';

            $i++;
        }
        $mail['dws'] .='</table>';



        $dws = $cc->get_sender_by_unit('DWS');
        foreach ($dws as $kk => $vv) {

            $this->sending_sms($vv['NOTEL'], $mess['dws']);

            $this->sending_mail_raw($vv['EMAIL'], $vv['NAMA'], $mail['dws'], 'ESKALASI SPEED [DES]');
        }

        $iss = $cc->get_sender_by_unit('ISS');
        foreach ($iss as $kk => $ll) {

            $this->sending_sms($ll['NOTEL'], $mess['dws']);

            $this->sending_mail_raw($ll['EMAIL'], $ll['NAMA'], $mail['dws'], 'ESKALASI SPEED [DES]');
        }



        die("SENT");
    }

    public function alertsystemAction() {
        $c = new Pm_Model_Pmsystem();
        $params = $this->getRequest()->getParams();

        //Zend_Debug::dump($params); die();
        $ll = $c->getalert((int) $params[ID]);
        //Zend_Debug::dump($ll); die();
        $sendingalert = false;
        switch ($ll['CHECKING_TYPE']) {
            case 'check tabel log' :
                $formula = $ll['ALERT_FORMULA'];
                if ($ll['CONNECTION'] == "") {
                    $status = $c->runformula($formula);
                    //Zend_Debug::dump($status); die();
                    //($status);
                    if ($status['STATUS'] != '1') {
                        $sendingalert = true;
                    }
                } else {
                    $connArr = explode('~', $ll['CONNECTION']);
                    $pArr = array();
                    foreach ($connArr as $Val) {
                        $vK = explode("|", $Val);
                        $pArr[$vK[0]] = $vK[1];
                    }

                    $pparam = array(
                        'dbname' => $pArr['dbname'],
                        'username' => $pArr['username'],
                        'password' => $pArr['password'],
                    );
                    if ($pArr['host']) {
                        $pparam = array(
                            'dbname' => $pArr['dbname'],
                            'username' => $pArr['username'],
                            'password' => $pArr['password'],
                            'host' => $pArr['host'],
                        );
                    }
                    $dbz = Zend_Db::factory($pArr['adapter'], $pparam);
                    //Zend_Debug::Dump($pArr['adapter']); die($formula);

                    try {
                        $status = $dbz->fetchRow($formula);
                    } catch (Exception $e) {
                        Zend_Debug::Dump($e);
                        die($formula);
                    }

                    if ($status['STATUS'] != '1') {
                        $sendingalert = true;
                    }
                }

                break;
            case 'check file log' :
                $content = file_get_contents($ll['LOG_OBJECT_NAME']);
                $pos = strpos($content, $ll['ALERT_FORMULA']);
                if ($pos === false) {
                    $sendingalert = true;
                } else {
                    $sendingalert = false;
                }


                break;
            case 'check port' :

                $sendingalert = false;
                if ($ll['CONNECTION'] != "") {
                    $fd = explode(":", $ll['CONNECTION']);
                    $data = array();
                    $data[HOST] = $fd[0];
                    $data[PORT] = $fd[1];

                    if (!$this->check_port($data)) {
                        $sendingalert = true;
                    }
                }

                break;
            case 'check connection' :

                $sendingalert = false;
                if ($ll['CONNECTION'] != "") {
                    $connArr = explode('~', $ll['CONNECTION']);
                    $pArr = array();
                    foreach ($connArr as $Val) {
                        $vK = explode("|", $Val);
                        $pArr[$vK[0]] = $vK[1];
                    }

                    $pparam = array(
                        'dbname' => $pArr['dbname'],
                        'username' => $pArr['username'],
                        'password' => $pArr['password'],
                    );
                    if ($pArr['host']) {
                        $pparam = array(
                            'dbname' => $pArr['dbname'],
                            'username' => $pArr['username'],
                            'password' => $pArr['password'],
                            'host' => $pArr['host'],
                        );
                    }

                    try {
                        //Zend_Debug::dump($pArr['adapter']); die();
                        $dbz = Zend_Db::factory($pArr['adapter'], $pparam);
                        $var = $dbz->getConnection();
                        //Zend_Debug::dump($var);

                        $sendingalert = false;
                    } catch (Zend_Exception $e) {
                        //  Zend_Debug::dump($e); die();
                        $sendingalert = true;
                    }
                }

                //return false;

                break;
        }

        //echo $sendingalert; die();
        if ($sendingalert) {
            $cara = explode(',', $ll['ALERT_TYPE']);
        }
        //Zend_Debug::dump($ll);
        //Zend_Debug::dump($cara);die("s");
        if (in_array('EMAIL', $cara)) {
            //$this->sending_mail($ll['ALERT_TEXT'], $ll['ZROLES']);	
        }

        //Zend_Debug::dump($cara); die();

        if (in_array('SMS', $cara)) {
            $mobes = $c->get_contact($ll['ZROLES'], 'MOBILE');
            //Zend_Debug::dump($mobes); die();
            foreach ($mobes as $mobe) {

                //  $this->sending_sms($mobe['MOBILE'], $ll['ALERT_TEXT']);
            }
        }
        die("sss");
    }

    public function sending_mail_raw($email = NULL, $name = NULL, $text = NULL, $subject = NULL) {
        //return true;

        $config = array('auth' => 'login',
            'username' => 'myusername',
            'password' => 'password'
        );
        $config = array(
            'auth' => 'login',
            'username' => (USER_SMTP),
            'password' => (PASS_SMTP),
            // 'ssl' => (SSL_SMTP),
            'port' => (PORT_SMTP));

        //Zend_Debug::dump($config); die();
        $transport = new Zend_Mail_Transport_Smtp((MAIL_SMTP), $config);
        $mail = new Zend_Mail();
        $mail->setDefaultTransport($transport);
        $mail->setBodyHtml($text);
        $mail->setFrom('speed@telkom.co.id', 'Admin Speed');
        //Zend_Debug::dump($roles); die();
        //Zend_Debug::dump($ddd); die();

        $mail->addTo($email, $name);


        $mail->setSubject($subject);
        try {
            $mail->send();
            $sent = true;
        } catch (Exception $e) {
            $sent = false;
        }

        //die("oke");
        return $sent;
    }

    public function sending_mail($text = NULL, $roles = NULL) {
        //return true;
        $c = new Pm_Model_Pmsystem();
        $config = array('auth' => 'login',
            'username' => 'myusername',
            'password' => 'password'
        );
        $config = array(
            'auth' => 'login',
            'username' => (USER_SMTP),
            'password' => (PASS_SMTP),
            // 'ssl' => (SSL_SMTP),
            'port' => (PORT_SMTP));

        //Zend_Debug::dump($config); die();
        $transport = new Zend_Mail_Transport_Smtp((MAIL_SMTP), $config);
        $mail = new Zend_Mail();
        $mail->setDefaultTransport($transport);
        $mail->setBodyText($text);
        $mail->setFrom('speed@telkom.co.id', 'Admin Speed');
        //Zend_Debug::dump($roles); die();
        $ddd = $c->get_contact($roles);

        //Zend_Debug::dump($ddd); die();
        foreach ($ddd as $k => $d) {
            $mail->addTo($d[EMAIL], $d[NAME]);
        }


        $mail->setSubject('ALERT SYSTEM');
        try {
            $mail->send();
            $sent = true;
        } catch (Exception $e) {
            $sent = false;
        }

        //die("oke");
        return $sent;
    }

    function sending_sms($no, $text) {
        if ($no != "" && $text != "") {

            $url = "http://servicebus.telkom.co.id:9001/TelkomSystem/SMSGateway/Services/ProxyService/smsBulk?msisdn=" . $no . "&message=" . urlencode($text);
            //echo $url; die();

            $data = file_get_contents($url);
            if ($data = 'SUCCESS<br>') {
                return true;
            } else {

                return false;
            }
        }
    }

    private function check_port($data) {
        $checkconn = fsockopen($data[HOST], $data[PORT], $errno, $errstr, 5);
        //echo $checkconn; die();
        if (!$checkconn) {
            return false;
        } else {
            return true;
        }
    }

    public function pmupdatepmorderAction() {
        $cserv = new Pm_Model_Pmservices();
        $cc = new Pm_Model_General();
        $arr = $cserv->getservicenull();
        //Zend_Debug::dump($arr); die("sss");
        foreach ($arr as $a) {
            $ao = NULL;
            $v1 = $cserv->cekticares($a[NO_ORDER]);
            /* if($a["NO_ORDER"]=='8000459497'){
              Zend_Debug::dump($v1); die("sss");
              } */

            if ($v1['PROCESS_TYPE'] == 'ZSVQ' || $v1['PROCESS_TYPE'] == 'ZMOS') {
                ($v1['PROCESS_TYPE'] == 'ZMOS') ? $zmos = 1 : $zmos = 'NULL';
                if ($v1['PROCESS_TYPE'] == 'ZSVQ') {
                    $ao = $cserv->get_ao_ticares($a[NO_ORDER]);
                }
            }

            if ($ao) {
                $cserv->updateaoorder($a[NO_ORDER], $ao, $zmos);
            }
        }


        $za = new Pm_Model_General();
        $dataz = array(
            'CRON_NAME' => 'pmupdatepmorder',
            'CRON_LENGTH' => '10 menit'
        );
        $za->create_cronlogs($dataz);



        die("OK");
    }

    public function pmupdateservicestatusweeklybckAction() {
        $c = new Pm_Model_Pmscurve();
        $li = $c->service_cek_open();
        $lis = $c->list_task();
        $lid = $c->list_task_toid();
        $lisnname = $c->list_task_name();
        $arr = array();
        $arr2 = array();
        $sum = array();
        $jum = NULL;
        foreach ($li as $ll) {

            if ($ll['GROUP_IMP_TASK_ID'] != '999' && $ll['GROUP_IMP_TASK_ID'] != '777') {
                if ($ll['GROUP_IMP_TASK_ID'] == '30') {
                    $c->updatestatusopen_toclosed_week($ll['NID'], $ll[GROUP_IMP_TASK_ID], $ll[PRODACTDATE]);
                } else {
                    $c->updatestatusopen_week($ll['NID'], $ll[GROUP_IMP_TASK_ID]);
                }
            } elseif ($ll[GROUP_IMP_TASK_ID] == '777') {
                $c->updatecancelled_week($ll['NID']);
            } else {
                $arr2[] = "'" . $ll['NO_ORDER'] . "'";
            }
        }

        if ($arr2) {
            $varam = implode(',', $arr2);

            $ll = $c->getTenos($varam);
            //print_r($ll); die("ss");


            $arr2 = array();
            foreach ($ll as $l) {
                $arr2[$l['NO_ORDER']][$l[IMPLEMENTATION_TASK]] = $lid[$l[IMPLEMENTATION_TASK]];
                $c->updatestatusopen_byorder_week($l['NO_ORDER'], $lid[$l[IMPLEMENTATION_TASK]]);
            }
        }

        $li_ao = $c->service_cek_ao_open();
        // Zend_Debug::dump($li_ao); die();

        $jum3 = NULL;
        foreach ($li_ao as $ll) {
            if ($ll['GROUP_IMP_TASK_ID'] != '999' && $ll['GROUP_IMP_TASK_ID'] != '777') {

                $arrx[$ll['ACTIVATION_ORDER']][$ll['GROUP_IMP_TASK_ID']] = $lis[$ll[GROUP_IMP_TASK_ID]];
                if ($ll['GROUP_IMP_TASK_ID'] == '30') {
                    $c->updatestatusopen_toclosed_week($ll['NID'], $ll[GROUP_IMP_TASK_ID], $ll[PRODACTDATE]);
                }
            } elseif ($ll['GROUP_IMP_TASK_ID'] == '777') {

                //die($ll['NID']);
                $c->updatecancelled_week($ll['NID']);
            } else {

                //die($ll['NID']);
                $arr4[] = "'" . $ll['ACTIVATION_ORDER'] . "'";
            }
        }

        $jum4 = NULL;
        if ($arr4) {
            $varam = implode(',', $arr4);
            $ll = $c->getTenos($varam);
            //	Zend_Debug::dump($varam); die();
            foreach ($ll as $l) {

                $c->updatestatusopen_byorder_ao_week($l['NO_ORDER'], $lid[$l[IMPLEMENTATION_TASK]]);
            }
        }

        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array('PM_SERVICE'));
        die("ok");
    }

    public function bypasuserAction() {
        $c = new Pm_Model_Pmscurve();
        $li = $c->service_all_sin();
        //Zend_Debug::dump($li); die("xx");
        if ($li) {
            foreach ($li as $ll) {
                $c->updatetrx($ll['NID'], $ll[DIVRE]);
            }
        }

        $li2 = $c->service_all_sin_xx();
        //Zend_Debug::dump($li); die("xx");
        if ($li2) {
            foreach ($li2 as $ll) {
                $c->updatetrx($ll['NID'], $ll[DIVRE]);
            }
        }


        $li3 = $c->service_all_sin_y();
        //Zend_Debug::dump($li); die("xx");
        if ($li3) {
            foreach ($li3 as $ll) {
                $c->updatetrx($ll['NID'], $ll[DIVRE]);
            }
        }

        $li4 = $c->service_all_sin_yy();
        //Zend_Debug::dump($li); die("xx");
        if ($li4) {
            foreach ($li4 as $ll) {
                $c->updatetrx($ll['NID'], $ll[DIVRE]);
            }
        }
        $li5 = $c->service_all_sin_z();
        //Zend_Debug::dump($li); die("xx");
        if ($li5) {
            foreach ($li5 as $ll) {
                $c->updatetrx($ll['NID'], $ll[DIVRE]);
            }
        }

        $li6 = $c->service_all_sin_zz();
        //Zend_Debug::dump($li); die("xx");
        if ($li6) {
            foreach ($li6 as $ll) {
                $c->updatetrx($ll['NID'], $ll[DIVRE]);
            }
        }

        $za = new Pm_Model_General();

        $dataz = array(
            'CRON_NAME' => 'bypasuser--get divre',
            'CRON_LENGTH' => '10 menit'
        );
        $za->create_cronlogs($dataz);
        die("ss");
    }

    public function pmupdateservicestatusAction() {
        /*
          require_once APPLICATION_PATH . '/fixed_libraries/phpchartdir.php';

          $c = new Pm_Model_Pmscurve();
          $list = $c->get_scurve('168');
          $var = array();
          foreach ($list as $kk=> $l) {
          //$var[$kk] = $l['REAL_WEIGHT'];
          //Zend_Debug::dump($l['REAL_WEIGHT']);
          }
          if ($l['REAL_WEIGHT']==null ||$l['REAL_WEIGHT']=='NULL'){
          die('insert');
          $c->createscurve($th, $pid, $plan, $actdate, $real);
          }
          Zend_Debug::dump($l['REAL_WEIGHT']);
          die("ok");
         * 
         */
        $c = new Pm_Model_Pmscurve();
        //$c->updated_closed();
        //query exclude waitress yang di OGP 
        $zax = $c->cek_allwaitress_exclude();
        foreach ($zax as $zz) {
            $zex[] = $zz['ID'];
        }
        //Zend_Debug::dump($zex); die(); 

        $c->delproductNull();
        $li = $c->service_cek_open();
        $clo = $c->get_pm_service_close();
        //Zend_Debug::dump($li); die();
        // $lis = $c->list_task();
        $lid = $c->list_task_toid();
        $lis = $c->list_imptask_tenoss();
        //Zend_Debug::dump($lis); die();
        $lisnname = $c->list_task_name();
        $arr = array();
        $arr2 = array();
        $sum = array();
        $jum = NULL;
        //$li=array();
        foreach ($clo as $llc) {
            //$is_open_tenoss = $c->getTenos2($llc['ACTIVATION_ORDER']);
            //if(is_array($is_open_tenoss) && count($is_open_tenoss[0])>0){
            //$at_tenoss = $is_open_tenoss[0];
            //die($llc['ACTIVATION_ORDER']);
            //Zend_Debug::dump($at_tenoss);
            //Zend_Debug::dump($at_tenoss[IMPLEMENTATION_TASK]);
            //Zend_Debug::dump($lid);
            //Zend_Debug::dump($llc['NO_ORDER']);
            //Zend_Debug::dump($lid[$at_tenoss[IMPLEMENTATION_TASK]]);die('here');
            //$c->updatestatusopen_byorder_ao2($llc['ACTIVATION_ORDER'], $lid[$at_tenoss[IMPLEMENTATION_TASK]]);
            //}else{
            $is_closed_tenoss = $c->cek_tenos_closed($llc['ACTIVATION_ORDER']);
            //Zend_Debug::dump($is_closed_tenoss);die('here');


            if ($is_closed_tenoss != "") {
                //die('hr');
                $c->updatestatusopen_toclosed($llc['ID'], '30', $is_closed_tenoss);

                // Zend_Debug::dump($is_closed_tenoss); die($ll['ACTIVATION_ORDER']);
            }
            //}
        }
        foreach ($li as $ll) {

            /* start : tambahan bypass cek ke ticares, jika ada TENOSS_CLOSED_DATE, by  himawijaya 27 feb2013, dedicated to ibu elis yang lagi hamil gede
             */

            if ($ll['TENOSS_CLOSED_DATE'] != "") {
                //Zend_Debug::dump($ll);
                if ($ll['MO'] == 1) {
                    $c->updatestatusopen_toclosed($ll['NID'], '30', $ll[TENOSS_CLOSED_DATE]);
                } else {
                    $c->updatestatusopen($ll['NID'], '14');
                }
            }
            //end     
            /*
              if($ll['NO_ORDER']=='8000433049'){
              Zend_Debug::dump($ll); die();
              } */

            if ($ll['GROUP_IMP_TASK_ID'] != '999' && $ll['GROUP_IMP_TASK_ID'] != '777' && $ll['GROUP_IMP_TASK_ID'] != NULL && $ll['GROUP_IMP_TASK_ID'] != '1000') {
                /* if($ll['NO_ORDER']=='8000433049'){
                  Zend_Debug::dump($ll); die('aaa');
                  } */
                if (($ll['GROUP_IMP_TASK_ID'] == '30' || $ll['GROUP_IMP_TASK_ID'] == '888') && $ll['TENOSS_CLOSED_DATE'] == "") {
                    $c->updatestatusopen_toclosed($ll['NID'], $ll[GROUP_IMP_TASK_ID], $ll[TENOSS_CLOSED_DATE]);
                } else {
                    //update ke PM_SERVICE dga IMP_TASK_ID   

                    if ($ll['GROUP_IMP_TASK_ID'] == '30' && $ll['TENOSS_CLOSED_DATE'] != "") {
                        $c->updatestatusopen_toclosed($ll['NID'], $ll[GROUP_IMP_TASK_ID], $ll[TENOSS_CLOSED_DATE]);
                    } else if (!in_array($ll['NID'], $zex)) {
                        $c->updatestatusopen($ll['NID'], $ll[GROUP_IMP_TASK_ID]);
                    }
                }
            } elseif ($ll[GROUP_IMP_TASK_ID] == '777') {
                $c->updatecancelled($ll['NID']);
            } elseif ($ll[GROUP_IMP_TASK_ID] == NULL) {
                $arr0[] = "'" . $ll['NO_ORDER'] . "'";
            }/*

              elseif($ll['GROUP_IMP_TASK_ID'] == '1000'){
              $lx = $c->getServiceCancel($ll['NO_ORDER']);
              //Zend_Debug::dump($lx); die();

              if((int)$lx['COUNT']>0){
              //echo $ll['NO_ORDER'];die();
              $c->updatestatusopen_byorder($ll['NO_ORDER'], '777');
              //die("x");
              }
              else{
              $c->updatestatusopen_byorder($ll['NO_ORDER'], '1000');
              }
              } */ else {
                //yang GROUP_IMP_TASK_ID=999, cek ke TENOS
                $arr2[] = "'" . $ll['NO_ORDER'] . "'";
            }
        }

        //Zend_Debug::dump($arr2);die('xxx'); 
        //Zend_Debug::dump($arr0); die();
        //die("x");

        if ($arr0) {
            $varam = implode(',', $arr0);
        }
        $arr2 = (array_chunk($arr2, 100, true));
        //Zend_Debug::dump($arr2); die();
        foreach ($arr2 as $k => $v) {
            $ll = array();
            if ($v) {
                $varam = implode(',', $v);
                $ll = $c->getTenos($varam);
                //Zend_Debug::dump($ll); die();
                $arr2 = array();
                foreach ($ll as $l) {
                    //$arr2[$l['NO_ORDER']][$l[IMPLEMENTATION_TASK]] = $lis[$l[IMPLEMENTATION_TASK]];
                    //die($lis[$l[IMPLEMENTATION_TASK]]);
                    $c->updatestatusopen_byorder($l['NO_ORDER'], $lis[$l[IMPLEMENTATION_TASK]]);
                    //die("x");
                }
            }
        }
        //die("x");

        $li_ao = $c->service_cek_ao_open();
        //Zend_Debug::dump($li_ao); die('AO');
        foreach ($li_ao as $ll) {

            if ($ll['GROUP_IMP_TASK_ID'] != '999' && $ll['GROUP_IMP_TASK_ID'] != '777' && $ll['GROUP_IMP_TASK_ID'] != '1000') {

                $arrx[$ll['ACTIVATION_ORDER']][$ll['GROUP_IMP_TASK_ID']] = $lis[$ll[GROUP_IMP_TASK_ID]];
                if ($ll['GROUP_IMP_TASK_ID'] == '30' || $ll['GROUP_IMP_TASK_ID'] == '888') {
                    $c->updatestatusopen_toclosed($ll['NID'], $ll[GROUP_IMP_TASK_ID], $ll[TENOSS_CLOSED_DATE]);
                }
            } elseif ($ll['GROUP_IMP_TASK_ID'] == '777') {

                //die($ll['NID']);
                $c->updatecancelled($ll['NID']);
            } elseif ($ll['GROUP_IMP_TASK_ID'] == '1000') {
                $lx = $c->getServiceCancel($ll['NID']);
                //Zend_Debug::dump($lx); die();

                if ((int) $lx['COUNT'] > 0) {
                    //echo $ll['NID'];die();
                    $c->updatestatusopen_byorder($ll['NID'], '777');
                    //die("x");
                } else {
                    $c->updatestatusopen_byorder($ll['NID'], '1000');
                }
            } else {

                //cek ke TENOS_CLOSED
                $is_closed_tenoss = $c->cek_tenos_closed($ll['ACTIVATION_ORDER']);
                //Zend_Debug::dump($is_closed_tenoss); 


                if ($is_closed_tenoss != "") {
                    $c->updatestatusopen_toclosed($ll['NID'], '30', $is_closed_tenoss);

                    // Zend_Debug::dump($is_closed_tenoss); die($ll['ACTIVATION_ORDER']);
                }/* else{
                  $lx = $c->getServiceCancel($ll['NID']);
                  //Zend_Debug::dump($lx); die();

                  if((int)$lx['COUNT']>0){
                  //echo $ll['NID'];die();
                  $c->updatestatusopen_byorder($ll['NID'], '777');
                  //die("x");
                  }
                  else{
                  $c->updatestatusopen_byorder($ll['NID'], '1000');
                  }
                  } */ else {
                    //utk dicek ke PM_TENOS_SERVICE		
                    $arr4[] = "'" . $ll['ACTIVATION_ORDER'] . "'";
                }
            }
        }


        $jum4 = NULL;

        //utk dicek ke PM_TENOS_SERVICE
        if ($arr4) {

            $arr4 = (array_chunk($arr4, 100, true));

            //Zend_Debug::dump($arr4); die();
            foreach ($arr4 as $k => $v) {
                $ll = array();

                //  if($k==10){
                $varam = implode(',', $v);
                $ll = $c->getTenos($varam);
                //Zend_Debug::dump($ll); die("s");
                if ($ll) {
                    foreach ($ll as $l) {
                        /* if($l['NO_ORDER']=='8000459254'){
                          Zend_Debug::dump($l);
                          Zend_Debug::dump($lid);
                          $c->updatestatusopen_byorder_ao($l['NO_ORDER'], $lid[$l[IMPLEMENTATION_TASK]]);
                          die("sss");
                          } */
                        $c->updatestatusopen_byorder_ao($l['NO_ORDER'], $lid[$l[IMPLEMENTATION_TASK]]);
                    }
                } else {
                    $lc = $c->getserviceclosed($varam);
                    foreach ($lc as $l) {
                        $c->updatestatusopen_byorder_ao($l['NO_ORDER'], $lis[$l[SO_STATUS]]);
                    }
                }

                //}
            }
        }
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array('PM_SERVICE'));
        $za = new Pm_Model_General();

        $dataz = array(
            'CRON_NAME' => 'pmupdateservicestatus',
            'CRON_LENGTH' => '10 menit'
        );

        $za->create_cronlogs($dataz);

        die("ok");
    }

    public function pmupdateserviceatributesAction() {
        $c = new Pm_Model_Pmscurve();
        #UPDATE ATRIBUT DARI DTA TENOSS OGP
        $li = $c->service_atributes_open_for_open();
        //Zend_Debug::dump($li); die();

        foreach ($li as $ll) {

            $c->update_pm_service_attrs_ogp($ll);
        }
        #die('OK 1');  
        #UPDATE ATRIBUT DARI DTA TENOSS CLOSED
        $li2 = $c->service_atributes_open_for_closed();

        //Zend_Debug::dump($li); die();

        foreach ($li2 as $ll2) {

            $c->update_pm_service_attrs($ll2);
        }



        die("ok");
        //Zend_Debug::dump($li); 
    }

    public function pmupdateservicestatusweeklyver0Action() {
        $c = new Pm_Model_Pmscurve();
        $li = $c->service_cek_open(TRUE);
        //Zend_Debug::dump($li); die();
        $lis = $c->list_task();
        $lid = $c->list_task_toid();
        $lis = $c->list_imptask_tenoss();
        //Zend_Debug::dump($lis); die();
        $lisnname = $c->list_task_name();
        $arr = array();
        $arr2 = array();
        $sum = array();
        $jum = NULL;
        foreach ($li as $ll) {

            if ($ll['GROUP_IMP_TASK_ID'] != '999' && $ll['GROUP_IMP_TASK_ID'] != '777' && $ll['GROUP_IMP_TASK_ID'] != NULL) {

                if ($ll['GROUP_IMP_TASK_ID'] == '30') {
                    $c->updatestatusopen_toclosed_week($ll['NID'], $ll[GROUP_IMP_TASK_ID], $ll[PRODACTDATE]);
                } else {
                    $c->updatestatusopen_week($ll['NID'], $ll[GROUP_IMP_TASK_ID]);
                }
            } elseif ($ll[GROUP_IMP_TASK_ID] == '777') {
                $c->updatecancelled_week($ll['NID']);
            } elseif ($ll[GROUP_IMP_TASK_ID] == NULL) {
                $arr0[] = "'" . $ll['NO_ORDER'] . "'";
            } else {

                $arr2[] = "'" . $ll['NO_ORDER'] . "'";
            }
        }

        //Zend_Debug::dump($li); 
        //Zend_Debug::dump($arr0); die();

        if ($arr0) {
            $varam = implode(',', $arr0);
        }


        if ($arr2) {
            $varam = implode(',', $arr2);
            $ll = $c->getTenos($varam);
            $arr2 = array();
            foreach ($ll as $l) {
                $arr2[$l['NO_ORDER']][$l[IMPLEMENTATION_TASK]] = $lis[$l[IMPLEMENTATION_TASK]];
                $c->updatestatusopen_byorder_week($l['NO_ORDER'], $lis[$l[IMPLEMENTATION_TASK]]);
            }
        }

        $li_ao = $c->service_cek_ao_open();
        //Zend_Debug::dump($li_ao); die();

        $jum3 = NULL;
        foreach ($li_ao as $ll) {
            if ($ll['GROUP_IMP_TASK_ID'] != '999' && $ll['GROUP_IMP_TASK_ID'] != '777') {

                $arrx[$ll['ACTIVATION_ORDER']][$ll['GROUP_IMP_TASK_ID']] = $lis[$ll[GROUP_IMP_TASK_ID]];
                if ($ll['GROUP_IMP_TASK_ID'] == '30') {
                    $c->updatestatusopen_toclosed_week($ll['NID'], $ll[GROUP_IMP_TASK_ID], $ll[PRODACTDATE]);
                }
            } elseif ($ll['GROUP_IMP_TASK_ID'] == '777') {

                //die($ll['NID']);
                $c->updatecancelled_week($ll['NID']);
            } else {
                $arr4[] = "'" . $ll['ACTIVATION_ORDER'] . "'";
            }
        }
        //Zend_Debug::dump($arr4); die("s");
        $jum4 = NULL;
        if ($arr4) {
            $varam = implode(',', $arr4);
            $ll = $c->getTenos($varam);

            if ($ll) {
                foreach ($ll as $l) {
                    $c->updatestatusopen_byorder_ao_week($l['NO_ORDER'], $lid[$l[IMPLEMENTATION_TASK]]);
                }
            } else {
                $lc = $c->getserviceclosed($varam);
                //Zend_Debug::dump($lc); die("s");
                foreach ($lc as $l) {
                    $c->updatestatusopen_byorder_ao_week($l['NO_ORDER'], $lis[$l[SO_STATUS]]);
                }
            }
        }

        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array('PM_SERVICE'));
        die("ok");
    }

    public function cekticaresAction() {

        $params = $this->getRequest()->getParams();
        $c = new Pm_Model_Pmscurve();
        $var = mktime();
        $now = date('d/m/Y', $var);
        $ll = $c->getdate($params['PID']);
        $dth = ($ll[$now]);
        $li = $c->service_cek($params['PID']);
        $lis = $c->list_task();
        $lisnname = $c->list_task_name();
        $arr = array();
        $arr2 = array();
        $sum = array();
        $jum = NULL;
        foreach ($li as $ll) {
            if ($ll[GROUP_IMP_TASK_ID] != '999') {
                $arr[$ll['NO_ORDER']][$ll[GROUP_IMP_TASK_ID]] = $lis[$ll[GROUP_IMP_TASK_ID]];
                $jum = $jum + $lis[$ll[GROUP_IMP_TASK_ID]];
            } elseif ($ll[GROUP_IMP_TASK_ID] == '777') {
                $c->updatecancelled($ll['NID']);
            } else {
                $arr2[] = "'" . $ll['NO_ORDER'] . "'";
            }
        }
        $jum2 = null;
        if ($arr2) {
            $varam = implode(',', $arr2);
            $ll = $c->getTenos($varam);
            $arr2 = array();
            foreach ($ll as $l) {
                $arr2[$l['NO_ORDER']][$l[IMPLEMENTATION_TASK]] = $lisnname[$l[IMPLEMENTATION_TASK]];
                $jum2 = $jum2 + $lisnname[$l[IMPLEMENTATION_TASK]];
            }
        }
        $li_ao = $c->service_cek_ao($params['PID']);
        $jum3 = NULL;
        foreach ($li_ao as $ll) {
            if ($ll[GROUP_IMP_TASK_ID] != '999') {
                $arrx[$ll['ACTIVATION_ORDER']][$ll[GROUP_IMP_TASK_ID]] = $lis[$ll[GROUP_IMP_TASK_ID]];
                $jum3 = $jum3 + $lis[$ll[GROUP_IMP_TASK_ID]];
            } elseif ($ll[GROUP_IMP_TASK_ID] == '777') {
                $c->updatecancelled($ll['NID']);
            } else {
                $arr4[] = "'" . $ll['ACTIVATION_ORDER'] . "'";
            }
        }
        $jum4 = NULL;
        if ($arr4) {
            $varam = implode(',', $arr4);
            $ll = $c->getTenos($varam);
            foreach ($ll as $l) {
                $arr5[$l['NO_ORDER']][$l[IMPLEMENTATION_TASK]] = $lisnname[$l[IMPLEMENTATION_TASK]];
                $jum4 = $jum4 + $lisnname[$l[IMPLEMENTATION_TASK]];
            }
        }
        $total = $jum + $jum2 + $jum3 + $jum4;
        $jum = $c->totalservice($params['PID'], NULL, TRUE);
        $weigt = $total / $jum;
        if ($dth) {
            $c->update_scurve_riil($weigt, $params['PID'], $dth);
        }
        $this->view->weight = $weigt;
    }

    public function cleaningAction() {
        die("s");
        //$c = new Pm_Model_General();
        //$c->run_proc_PM_STG_MAPPING_TQAO_INSR_TRNC();
        //$li = $c->get_double();
        ////Zend_Debug::dump($li); die(); 
        //if($li){
        //foreach ($li as $ll) {
        //$li2 = 	$c->selectdouble($ll[NSORDER]);
        ////Zend_Debug::dump($li2); die(); 
        //if(count($li2)>1){
        //foreach($li2 as $k=>$ll) {
        //if($k>0)
        //{
        //$c->deldouble($ll[NSORDER],$ll[CHARDAT]);
        //}
        //}
        //} else {
        //$li2 = 	$c->selectdouble_step2($ll[NSORDER]);
        //foreach($li2 as $k=>$ll) {
        //if($k>0)
        //{
        //$c->deldouble_step2($ll[NSORDER]);
        //}
        //}
        //}
        //}
        //}
        //die("ss");  
    }

    public function crondailyAction() {

        $a = new Pm_Model_General();
        $c = new Pm_Model_Pmscurve();

        $vv = $a->get_all_project_open();
        #nyoba satu ID : $vv = $a->get_project_329();
        //$vv = $a->get_all_projectxx();
        //Zend_Debug::dump($vv);die();
        foreach ($vv as $v) {

            $total = $c->totalservice_define($v[PROJECT_ID], NULL, TRUE);

            $cnt = $c->count_vsat($v[PROJECT_ID]);

            if ($cnt > 0) {
                $weigt = $c->total_scurve($v[PROJECT_ID], TRUE, $total, $cnt);
            } else {
                $weigt = $c->total_scurve($v[PROJECT_ID], FALSE, $total);
            }



            $var = mktime();
            $var2 = mktime(0, 0, 0, date("m"), date("d") + 1, date("y"));
            $var3 = mktime(0, 0, 0, date("m"), date("d") - 1, date("y"));
            $now = date('d/m/Y', $var);

            $tomorrow = date('d/m/Y', $var2);
            $yesterday = date('d/m/Y', $var3);

            $cek_yesterday = $c->cek_yesterday($yesterday, $v[PROJECT_ID]);
            $ll = $c->getdate($v[PROJECT_ID]);

            $dth = ($ll[$now]);
            $dth2 = ($ll[$tomorrow]);

            if ($dth) {
                $c->update_scurve_riil($weigt, $v[PROJECT_ID], $dth);
                $c->update_scurve_riil($weigt, $v[PROJECT_ID], $dth2);
            } else {

                $c->insert_scurve_riil($cek_yesterday, $v[PROJECT_ID], $now);
            }
        }


        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array('PM_SERVICE'));

        $za = new Pm_Model_General();

        $dataz = array(
            'CRON_NAME' => 'crondaily',
            'CRON_LENGTH' => 'daily'
        );

        $za->create_cronlogs($dataz);

        die("ok");
    }

    public function pmupdateservicestatusweeklyAction() {
        $a = new Pm_Model_General();
        $d = new Pm_Model_Pmscurve();
        $vv = $a->get_all_project_open();
        foreach ($vv as $v) {

            $d->updateserviceweekly($v['PROJECT_ID']);
        }


        $za = new Pm_Model_General();

        $dataz = array(
            'CRON_NAME' => 'pmupdateservicestatusweekly',
            'CRON_LENGTH' => 'setiap minggu malam, 24.00'
        );
        $za->create_cronlogs($dataz);
        die("ss");


        die("s");
    }

    public function recekcancelledAction() {

        $c = new Pm_Model_Pmscurve();
        $cserv = new Pm_Model_Pmservices();

        $li = $c->cek_allcancelled();
        $d = $c->cek_allcancelled_closed_ao();

        //Zend_Debug::dump($li); die();
        //$e = $c->cek_allcancelled_ogp_tq();
        $f = $c->cek_allcancelled_ogp_ao();

        foreach ($d as $dd) {
            $aao[] = $dd['ACTIVATION_ORDER'];
            $var[$dd['ACTIVATION_ORDER']] = $dd;
        }
        foreach ($e as $ee) {
            $atq[] = $ee['NO_ORDER'];
            $vartq[$ee['NO_ORDER']] = $ee;
        }

        foreach ($f as $ff) {
            $ato[] = $ff['ACTIVATION_ORDER'];
            $varto[$ff['ACTIVATION_ORDER']] = $ff;
        }


        //Zend_Debug::dump($var); die();
        //Zend_Debug::dump($varto); die();
        foreach ($li as $a) {
            if ($a[ACTIVATION_ORDER] != "") {
                $v1 = $cserv->cekticares($a[NO_ORDER]);
                if ($v1['PROCESS_TYPE'] == 'ZSVQ' || $v1['PROCESS_TYPE'] == 'ZMOS') {
                    ($v1['PROCESS_TYPE'] == 'ZMOS') ? $zmos = 1 : $zmos = 'NULL';
                    if ($v1['PROCESS_TYPE'] == 'ZSVQ') {

                        $ao = $cserv->get_ao_ticares($a[NO_ORDER]);
                    }
                }


                if (isset($ao) && $ao != $a['ACTIVATION_ORDER']) {
                    $cserv->updateaoorder_valid($a[NO_ORDER], $ao, $zmos);
                } else {
                    
                }
            }
            //if(in_array($ao, $aao))
            if (isset($var[$ao])) {
                //die("1");
                $c->updatestatusopen_toclosed($a['ID'], '30', $var[$ao]['TENOSS_CLOSED_DATE']);
            }

            if (isset($varto[$a['ACTIVATION_ORDER']])) {
                #	die("2");
                $c->updatestatusopen_byorder_for_cancelled_ao($a['ACTIVATION_ORDER'], $varto[$a['ACTIVATION_ORDER']]['GROUP_IMP_TASK_ID']);
            }

            if (isset($vartq[$a['NO_ORDER']])) {
                #	die("3");

                $c->updatestatusopen_byorder_for_cancelled($a['NO_ORDER'], $vartq[$a['NO_ORDER']]['GROUP_IMP_TASK_ID']);
            }
        }



        $za = new Pm_Model_General();
        $dataz = array(
            'CRON_NAME' => 'recekcancelled',
            'CRON_LENGTH' => 'jam 10:00 dan jam 18:00 sore tiap hari'
        );
        $za->create_cronlogs($dataz);

        //Zend_Debug::dump($li2); 
        die("ok");
        //Zend_Debug::dump($li); die();
    }

    public function updatevalidasitqAction() {
        $cserv = new Pm_Model_Pmservices();
        $cc = new Pm_Model_General();
        $arr = $cserv->getservice_valid_tq();
        //Zend_Debug::dump($arr); die("sss");
        foreach ($arr as $a) {
            $ao = NULL;
            $v1 = $cserv->cekticares($a[NO_ORDER]);

            // Zend_Debug::dump($v1); die();

            if ($v1['PROCESS_TYPE'] == 'ZSVQ' || $v1['PROCESS_TYPE'] == 'ZMOS') {
                ($v1['PROCESS_TYPE'] == 'ZMOS') ? $zmos = 1 : $zmos = 'NULL';
                if ($v1['PROCESS_TYPE'] == 'ZSVQ') {
                    $ao = $cserv->get_ao_ticares($a[NO_ORDER]);
                    //die($ao);
                }
            }

            if (isset($ao) && $ao != $a['ACTIVATION_ORDER']) {
                echo $ao . "<br>";
                //die($a['ACTIVATION_ORDER']);
                $cserv->updateaoorder_valid($a[NO_ORDER], $ao, $zmos);
            } else {

                $cserv->update_valid($a[NO_ORDER]);
            }
        }


        $za = new Pm_Model_General();
        $dataz = array(
            'CRON_NAME' => 'pmupdatepmorder',
            'CRON_LENGTH' => '10 menit'
        );
        //$za->create_cronlogs($dataz); 



        die("OK");
    }

    public function testAction() {
        $cserv = new Pm_Model_Pmscurve();
        //  die("OK");
        $arr = $cserv->cek_service_2();
        Zend_Debug::dump($arr);
        die("sss");




        die("OK");
    }

    public function recekwaitressAction() {

        $c = new Pm_Model_Pmscurve();
        $cserv = new Pm_Model_Pmservices();

        $e = $c->cek_allwaitress_ogp_tq();
        //Zend_Debug::dump($e); die();
        foreach ($e as $ee) {
            $c->updatestatusopen_byorder($ee['NO_ORDER'], $ee['GROUP_IMP_TASK_ID']);
        }

        $za = new Pm_Model_General();
        $dataz = array(
            'CRON_NAME' => 'recekwaitress',
            'CRON_LENGTH' => '20 menitan'
        );
        $za->create_cronlogs($dataz);

        //Zend_Debug::dump($li2); 
        die("ok");
        //Zend_Debug::dump($li); die();
    }

    public function sapAction() {
        //die("ok");
        $c = new Pm_Model_Pmscurve();
        $ll = $c->cek_sap_open();
        $c_o = count($ll);
        $c_l = $c->cek_count_log();
        if ($c_o != $c_l) {
            $c->dropinsertsap($ll);
        }
        //Zend_Debug::dump($ll); die();
        foreach ($ll as $kk => $vk) {
            //if($kk==0){	
            $param = array('OBJECT_ID' => $vk['VORDER']);
            $client = new SoapClient("/usr/local/apache2/htdocs/pmis/application/fixed_libraries/sap.wsdl", array('login' => 'WMUSER', 'password' => 'diponegoro24', 'trace' => 1, 'exceptions' => 1));
            try {
                $result = $client->ZRFC_GET_STATUS($param);
            } catch (SoapFault $f) {
                //	var_dump($f);
            }
            //Zend_Debug::dump($result); die();

            if (isset($result->STATUS) && $result->STATUS != "") {
                $c->updateordersap($vk['VORDER'], $result->STATUS);
            }
            //}
        }

        $za = new Pm_Model_General();
        $dataz = array(
            'CRON_NAME' => 'SAP for NSORDER',
            'CRON_LENGTH' => ''
        );
        $za->create_cronlogs($dataz);
        die("ok");
    }

    public function sap4aoAction() {
        //die("ok");
        $c = new Pm_Model_Pmscurve();
        $ll = $c->cek_sap_open(true);
        $c_o = count($ll);
        $c_l = $c->cek_count_log(true);
        if ($c_o != $c_l) {
            $c->dropinsertsap($ll, true);
        }
        //die("s");
        foreach ($ll as $kk => $vk) {
            $param = array('OBJECT_ID' => $vk['VORDER']);
            $client = new SoapClient("/usr/local/apache2/htdocs/pmis/application/fixed_libraries/sap.wsdl", array('login' => 'WMUSER', 'password' => 'diponegoro24', 'trace' => 1, 'exceptions' => 1));
            try {
                $result = $client->ZRFC_GET_STATUS($param);
            } catch (SoapFault $f) {
                //var_dump($f);
            }
            if (isset($result->STATUS) && $result->STATUS != "") {
                $c->updateordersap($vk['VORDER'], $result->STATUS);
            }
        }
        $za = new Pm_Model_General();
        $dataz = array(
            'CRON_NAME' => 'SAP for AO',
            'CRON_LENGTH' => ''
        );
        $za->create_cronlogs($dataz);
        die("ok");
    }

    public function revalidsapAction() {
        $c = new Pm_Model_Pmscurve();
        $ll = $c->revalid(true);

        die("ok");
    }

    //created by him 01 Juli 2013 untuk tes
    public function testhimAction() {
        $mdl_general = new Pm_Model_General();
        $result = null;

        $sql = "";
        $result = $mdl_general->tes_query($sql);
        echo '<pre>';
        var_dump($result);
        echo '</pre>';
        die('him');
    }

    public function tescronAction() {
        //die("ok");
        $c = new Pm_Model_Pmscurve();
        //$c->updated_closed();
        //query exclude waitress yang di OGP 
        $zax = $c->cek_allwaitress_exclude();
        foreach ($zax as $zz) {
            $zex[] = $zz['ID'];
        }
        //Zend_Debug::dump($zex); die(); 

        $c->delproductNull();
        $li = $c->service_cek_open();
        //Zend_Debug::dump($li); die();
        // $lis = $c->list_task();
        $lid = $c->list_task_toid();
        $lis = $c->list_imptask_tenoss();
        // Zend_Debug::dump($lis); die();
        $lisnname = $c->list_task_name();
        $arr = array();
        $arr2 = array();
        $sum = array();
        $jum = NULL;
        //$li=array();
        foreach ($li as $ll) {

            /* start : tambahan bypass cek ke ticares, jika ada TENOSS_CLOSED_DATE, by  himawijaya 27 feb2013, dedicated to ibu elis yang lagi hamil gede
             */

            if ($ll['TENOSS_CLOSED_DATE'] != "") {
                $c->updatestatusopen($ll['NID'], '14');
            }
            //end     
            if ($ll['GROUP_IMP_TASK_ID'] != '999' && $ll['GROUP_IMP_TASK_ID'] != '777' && $ll['GROUP_IMP_TASK_ID'] != NULL) {
                if (($ll['GROUP_IMP_TASK_ID'] == '30' || $ll['GROUP_IMP_TASK_ID'] == '888') && $ll['TENOSS_CLOSED_DATE'] == "") {
                    $c->updatestatusopen_toclosed($ll['NID'], $ll[GROUP_IMP_TASK_ID], $ll[TENOSS_CLOSED_DATE]);
                } else {
                    //update ke PM_SERVICE dga IMP_TASK_ID   
                    if (!in_array($ll['NID'], $zex)) {
                        $c->updatestatusopen($ll['NID'], $ll[GROUP_IMP_TASK_ID]);
                    }
                }
            } elseif ($ll[GROUP_IMP_TASK_ID] == '777') {
                $c->updatecancelled($ll['NID']);
            } elseif ($ll[GROUP_IMP_TASK_ID] == NULL) {
                $arr0[] = "'" . $ll['NO_ORDER'] . "'";
            } else {
                //yang GROUP_IMP_TASK_ID=999, cek ke TENOS
                $arr2[] = "'" . $ll['NO_ORDER'] . "'";
            }
        }

        //Zend_Debug::dump($arr2);die('xxx'); 
        //Zend_Debug::dump($arr0); die();
        //die("x");

        if ($arr0) {
            $varam = implode(',', $arr0);
        }
        $arr2 = (array_chunk($arr2, 100, true));
        //Zend_Debug::dump($arr2); die();
        foreach ($arr2 as $k => $v) {
            $ll = array();
            if ($v) {
                $varam = implode(',', $v);
                $ll = $c->getTenos($varam);
                //Zend_Debug::dump($ll); die();
                $arr2 = array();
                foreach ($ll as $l) {
                    //$arr2[$l['NO_ORDER']][$l[IMPLEMENTATION_TASK]] = $lis[$l[IMPLEMENTATION_TASK]];
                    //die($lis[$l[IMPLEMENTATION_TASK]]);
                    $c->updatestatusopen_byorder($l['NO_ORDER'], $lis[$l[IMPLEMENTATION_TASK]]);
                    //die("x");
                }
            }
        }
        //die("x");

        $li_ao = $c->service_cek_ao_open();
        //Zend_Debug::dump($li_ao); die('AO');
        foreach ($li_ao as $ll) {

            if ($ll['GROUP_IMP_TASK_ID'] != '999' && $ll['GROUP_IMP_TASK_ID'] != '777') {

                $arrx[$ll['ACTIVATION_ORDER']][$ll['GROUP_IMP_TASK_ID']] = $lis[$ll[GROUP_IMP_TASK_ID]];
                if ($ll['GROUP_IMP_TASK_ID'] == '30' || $ll['GROUP_IMP_TASK_ID'] == '888') {
                    $c->updatestatusopen_toclosed($ll['NID'], $ll[GROUP_IMP_TASK_ID], $ll[TENOSS_CLOSED_DATE]);
                }
            } elseif ($ll['GROUP_IMP_TASK_ID'] == '777') {

                //die($ll['NID']);
                $c->updatecancelled($ll['NID']);
            } else {

                //cek ke TENOS_CLOSED
                $is_closed_tenoss = $c->cek_tenos_closed($ll['ACTIVATION_ORDER']);
                //Zend_Debug::dump($is_closed_tenoss); 


                if ($is_closed_tenoss != "") {
                    $c->updatestatusopen_toclosed($ll['NID'], '30', $is_closed_tenoss);

                    // Zend_Debug::dump($is_closed_tenoss); die($ll['ACTIVATION_ORDER']);
                } else {
                    //utk dicek ke PM_TENOS_SERVICE		
                    $arr4[] = "'" . $ll['ACTIVATION_ORDER'] . "'";
                }
            }
        }


        $jum4 = NULL;

        //utk dicek ke PM_TENOS_SERVICE
        if ($arr4) {

            $arr4 = (array_chunk($arr4, 100, true));

            //Zend_Debug::dump($arr4); die();
            foreach ($arr4 as $k => $v) {
                $ll = array();

                //  if($k==10){
                $varam = implode(',', $v);
                $ll = $c->getTenos($varam);
                //Zend_Debug::dump($ll); die("s");
                if ($ll) {
                    foreach ($ll as $l) {
                        $c->updatestatusopen_byorder_ao($l['NO_ORDER'], $lid[$l[IMPLEMENTATION_TASK]]);
                    }
                } else {
                    $lc = $c->getserviceclosed($varam);
                    foreach ($lc as $l) {
                        $c->updatestatusopen_byorder_ao($l['NO_ORDER'], $lis[$l[SO_STATUS]]);
                    }
                }

                //}
            }
        }
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array('PM_SERVICE'));
        $za = new Pm_Model_General();

        $dataz = array(
            'CRON_NAME' => 'pmupdateservicestatus',
            'CRON_LENGTH' => '10 menit'
        );

        $za->create_cronlogs($dataz);

        die("ok");
    }

    public function updatecloseddateAction() {

        $d = new Pm_Model_Pmscurve();

        $li = $d->mo_closed_date();
        foreach ($li as $l) {
            $d->updatestatusopen_toclosed($l['ID'], $l[IMP_TASK_ID], $l[TENOSS_CLOSED_DATE]);
        }
        die("z");
        //Zend_Debug::dump($li); die();
    }

}
