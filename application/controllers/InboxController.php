<?php

/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class InboxController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    public function ininboxAction() {

        $params = $this->getRequest()->getParams();
        $inb = new Model_Minbox();
        $data = $inb->getInbox();

        $this->view->data = $data;
        $this->view->params = $params;


        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
    }

    public function inAction() {


        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
    }

    public function indexAction() {

#die("me");	

        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-1.10.2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-migrate-1.2.1.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap/js/bootstrap.min.js');

        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-slimscroll/jquery.slimscroll.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.blockui.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.cokie.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/fancybox/source/jquery.fancybox.pack.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js');



        //$this->view->headScript()->appendFile('/assets/core/scripts/app.js');
        //$this->view->headScript()->appendFile('/assets/core/scripts/inbox.js');



        $params = $this->getRequest()->getParams();


        $inb = new Model_Minbox();
        $data = $inb->getInbox();

        #		Zend_Debug::dump($data);die();

        $this->view->data = $data;
        $this->view->params = $params;
    }

    public function messageAction() {

#die("me");	

        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-1.10.2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-migrate-1.2.1.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap/js/bootstrap.min.js');

        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-slimscroll/jquery.slimscroll.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.blockui.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.cokie.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/fancybox/source/jquery.fancybox.pack.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js');

        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-file-upload/js/vendor/tmpl.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-file-upload/js/vendor/load-image.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-file-upload/js/vendor/canvas-to-blob.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-file-upload/js/jquery.iframe-transport.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-file-upload/js/jquery.fileupload.js');

        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-file-upload/js/jquery.fileupload-process.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-file-upload/js/jquery.fileupload-image.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-file-upload/js/jquery.fileupload-audio.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-file-upload/js/jquery.fileupload-video.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-file-upload/js/jquery.fileupload-validate.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-file-upload/js/jquery.fileupload-ui.js');

        $this->view->headScript()->appendFile('/assets/core/scripts/app.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/inbox.js');

#	$this->view->headLink()->appendStylesheet('/assets/nextone/plugins/select2/select2_metro.css');
#	$this->view->headLink()->appendStylesheet('/assets/nextone/plugins/data-tables/DT_bootstrap.css');



        $params = $this->getRequest()->getParams();

        $inb = new Model_Minbox();
        $data = $inb->getMessage($params);

#		Zend_Debug::dump($xx);die();

        $this->view->data = $data;
        $this->view->params = $params;
    }

    public function prahuAction() {

#die("me");	

        $params = $this->getRequest()->getParams();

        $inb = new Model_Minbox();
        $data = $inb->getPrahu();

#		Zend_Debug::dump($xx);die();

        $this->view->data = $data;
        $this->view->params = $params;
    }

    public function addprahuAction() {

#die("me");	
        $params = $this->getRequest()->getParams();

        $inb = new Model_Minbox();

        if ($_POST) {

#		Zend_Debug::dump($_POST);die();

            $exe = $inb->insertPrahu($_POST);

            $this->_redirect('/inbox/prahu');
        }



        $data = $inb->dataPrahu($params['id']);


        $this->view->data = $data;
        $this->view->params = $params;
    }

    public function deleteprahuAction() {

#die("me");	
        $params = $this->getRequest()->getParams();

        $inb = new Model_Minbox();
        $exe = $inb->deletePrahu($params['id']);

        if ($_POST) {

#		Zend_Debug::dump($_POST);die();
        }

        $this->_redirect('/inbox/prahu');


#	$data = $inb->dataPrahu($params['id']);
#		$this->view->data = $data;
#		$this->view->params = $params;
    }

}
