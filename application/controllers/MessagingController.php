<?php

/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class MessagingController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    public function sendalertAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
//Zend_Debug::dump($identity);die();
//Zend_Debug::dump($_POST);die();
        if ($_POST && count($_POST["selecteduser"]) > 0 && count($_POST["sendto"]) > 0 && count($_POST["sch"]) > 0 && $_POST["tgl1"] != '' && $_POST["tgl2"] != '' && $_POST["sendtxt"] != '') {
            //Zend_Debug::dump($_POST);die('www');
            $mdl_crn = new Model_Crontab();
            $mdl_api = new Model_Api();
            foreach ($_POST["sch"] as $k => $v) {
                $cronconf = '';
                switch ($v) {
                    case 'hourly':
                        //die('hour');
                        //Zend_Debug::dump($_POST["tgl1"].':00');die();
                        //Zend_Debug::dump($_POST["tgl2"].':00');die();
                        $cronconf = $mdl_crn->getcroninterval($_POST["tgl1"] . ':00', $_POST["tgl2"] . ':00', 'hour');
                        break;
                    case 'daily':
                        $cronconf = $mdl_crn->getcroninterval($_POST["tgl1"] . ':00', $_POST["tgl2"] . ':00', 'day');
                        break;
                    case 'weekly':
                        $cronconf = $mdl_crn->getcroninterval($_POST["tgl1"] . ':00', $_POST["tgl2"] . ':00', 'week');
                        break;
                    case 'monthly':
                        $cronconf = $mdl_crn->getcroninterval($_POST["tgl1"] . ':00', $_POST["tgl2"] . ':00', 'month');
                        break;
                    default:
                        break;
                }

                if ($cronconf == '') {
                    break;
                }
                //Zend_Debug::dump($cronconf.' - '.$v);die();

                foreach ($_POST["selecteduser"] as $k2 => $v2) {
                    $send = explode("_", $v2);
                    //Zend_Debug::dump($send);die();
                    if (count($send) > 1) {
                        $send = $mdl_api->getAttr($send[1], $send[0]);
                        //Zend_Debug::dump($send);die();
                        $telegram = '';
                        $email = '';
                        $wa = '';
                        $gtalk = '';
                        $sms = '';
                        if ($send && count($send) > 0) {
                            foreach ($send as $k3 => $v3) {
                                switch ($v3["attr_code"]) {
                                    case 'telegram':
                                        $telegram = $v3["attr_val"];
                                        break;
                                    case 'gtalk':
                                        $gtalk = $v3["attr_val"];
                                        break;
                                    case 'email':
                                        $email = $v3["attr_val"];
                                        break;
                                    case 'whatsapp':
                                        $wa = $v3["attr_val"];
                                        break;
                                    case 'sms':
                                        $sms = $v3["attr_val"];
                                        break;
                                    default:
                                        break;
                                }
                            }
                            $intcron = $mdl_crn->addcronalert($identity->uid, $_POST["sendtxt"], $telegram, $email, $gtalk, $wa, $sms, $cronconf, null, str_replace(" ", "_", $_POST["tgl1"]) . ':00', str_replace(" ", "_", $_POST["tgl2"]) . ':00', $v);
                            //Zend_Debug::dump($intcron);//die();
                            //die('create');
                        }
                    }
                }
                //die();
            }
            //die();
            $this->_redirect('/messaging/managealerts');
        }

        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-toastr/toastr.min.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-toastr/toastr.min.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/ui-toastr.js');


        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datetimepicker/css/datetimepicker.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-daterangepicker/moment.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-daterangepicker/daterangepicker.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js');

//die('www');
    }

    public function managegroupsAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        $this->view->headScript()->appendFile('/assets/core/js/messaging-managegroups.js');
        //die('manage users');

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        //Zend_Debug::dump($identity);die();

        $mdl_msg = new Model_Messaging();
        $types = $mdl_msg->getAlertTypes();
        //Zend_Debug::dump($types);die();
        /* $grps = $mdl_msg->getAlertGroups();
          //Zend_Debug::dump($grps);//die();

          foreach($grps as $k=>$v){
          foreach($types as $k2=>$v2){
          //die($v2);
          if(!isset($grps[$k][$v2])){
          $grps[$k][$v2] = "";
          }
          }
          } */
        //Zend_Debug::dump($grps);die();
        $this->view->types = $types;
    }

    public function manageusersAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        $this->view->headScript()->appendFile('/assets/core/js/messaging-manageusers.js');
        //die('manage users');

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        //Zend_Debug::dump($identity);die();

        $mdl_msg = new Model_Messaging();
        $types = $mdl_msg->getAlertTypes();
        //Zend_Debug::dump($types);die();
        $grps = $mdl_msg->getAlertUsers();
        //Zend_Debug::dump($grps);die();

        foreach ($grps as $k => $v) {
            foreach ($types as $k2 => $v2) {
                //die($v2);
                if (!isset($grps[$k][$v2])) {
                    $grps[$k][$v2] = "";
                }
            }
        }
        //Zend_Debug::dump($grps);die();
        $this->view->types = $types;
    }

    public function editgroupAction() {
        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($params);die();
        if (!isset($params['gid']) || !isset($params['gname']) || $params['gid'] == '' || $params['gname'] == '') {
            $this->_redirect('/messaging/managegroups');
        }

        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        $this->view->headScript()->appendFile('/assets/core/js/messaging-managegroups.js');
        //die('manage users');

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        //Zend_Debug::dump($identity);die();

        $mdl_msg = new Model_Messaging();
        $types = $mdl_msg->getAlertTypes();
        //Zend_Debug::dump($types);die();
        $grps = $mdl_msg->getAlertGroups($params['gid']);
        //Zend_Debug::dump($grps);die();
        if (count($grps) < 0 || $grps[$params['gid']]['name'] != $params['gname']) {
            $this->_redirect('/messaging/managegroups');
        }

        $group = array();
        $group['gid'] = $params['gid'];
        $group['gname'] = $params['gname'];
        foreach ($grps as $k => $v) {
            foreach ($types as $k2 => $v2) {
                //die($v2);
                if (!isset($grps[$k][$v2])) {
                    $group['type'][$v2] = "";
                } else {
                    $group['type'][$v2] = $grps[$k][$v2];
                }
            }
        }
        //Zend_Debug::dump($group);die();
        $this->view->types = $types;
        $this->view->group = $group;
        $this->view->params = $params;
    }

    public function edituserAction() {
        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($params);die();
        if (!isset($params['uid']) || !isset($params['uname']) || $params['uid'] == '' || $params['uname'] == '') {
            $this->_redirect('/messaging/manageusers');
        }

        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        $this->view->headScript()->appendFile('/assets/core/js/messaging-manageusers.js');
        //die('manage users');

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        //Zend_Debug::dump($identity);die();

        $mdl_msg = new Model_Messaging();
        $types = $mdl_msg->getAlertTypes();
        //Zend_Debug::dump($types);die();
        $grps = $mdl_msg->getAlertUsers($params['uid']);
        //Zend_Debug::dump($grps);die();
        if (count($grps) < 0 || $grps[$params['uid']]['name'] != $params['uname']) {
            $this->_redirect('/messaging/manageusers');
        }

        $group = array();
        $group['uid'] = $params['uid'];
        $group['uname'] = $params['uname'];
        foreach ($grps as $k => $v) {
            foreach ($types as $k2 => $v2) {
                //die($v2);
                if (!isset($grps[$k][$v2])) {
                    $group['type'][$v2] = "";
                } else {
                    $group['type'][$v2] = $grps[$k][$v2];
                }
            }
        }
        //Zend_Debug::dump($group);die();
        $this->view->types = $types;
        $this->view->group = $group;
        $this->view->params = $params;
    }

    public function addgroupAction() {
        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($params);die();


        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        $this->view->headScript()->appendFile('/assets/core/js/messaging-managegroups.js');
        //die('manage users');
        //===================
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');

        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-toastr/toastr.min.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-toastr/toastr.min.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/ui-toastr.js');

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        //Zend_Debug::dump($identity);die();

        $mdl_msg = new Model_Messaging();
        $types = $mdl_msg->getAlertTypes();
        //Zend_Debug::dump($types);die();
        $params['gid'] = (isset($params['gid'])) ? $params['gid'] : null;
        $params['gname'] = (isset($params['gname'])) ? $params['gname'] : null;
        $grps = $mdl_msg->getAlertGroups($params['gid']);
        //Zend_Debug::dump($grps);die();

        $group = array();
        $group['gid'] = $params['gid'];
        $group['gname'] = $params['gname'];
        foreach ($grps as $k => $v) {
            foreach ($types as $k2 => $v2) {
                //die($v2);
                $group['type'][$v2] = "";
            }
        }

        $this->view->group = $group;
        $this->view->params = $params;
    }

    public function adduserAction() {
        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($params);die();


        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        $this->view->headScript()->appendFile('/assets/core/js/messaging-manageusers.js');

        //===================
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');

        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-toastr/toastr.min.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-toastr/toastr.min.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/ui-toastr.js');

        //die('manage users');

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        //Zend_Debug::dump($identity);die();

        $mdl_msg = new Model_Messaging();
        $types = $mdl_msg->getAlertTypes();
        //Zend_Debug::dump($types);die();
        foreach ($types as $k2 => $v2) {
            //die($v2);

            $group['type'][$v2] = "";
        }

        //Zend_Debug::dump($group);die();

        $this->view->group = $group;
        $this->view->params = $params;
    }

    public function ajaxaddgroupAction() {
        $this->_helper->layout->disableLayout();
        $result = array(
            'ret' => false,
            'retmsg' => 'Invalid Parameters'
        );

        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($_POST);die();
        if (isset($_POST['gid']) && isset($_POST['gname']) && $_POST['gname'] != '' && $_POST['gid'] != '') {
            $mdl_msg = new Model_Messaging();
            foreach ($_POST as $k => $v) {
                if ($k != 'gid' && $k != 'gname' && $k != 'filtercheck') {
                    $mdl_msg->insertgroupattr($_POST['gid'], $k, $v);
                }
            }
            $result = array(
                'ret' => true,
                'retmsg' => 'Group Profile has been added'
            );
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function ajaxadduserAction() {
        $params = $this->getRequest()->getParams();
//Zend_Debug::dump($_POST);die();

        $result = array(
            'ret' => false,
            'retmsg' => 'Invalid request'
        );
        if (isset($_POST['uid']) && isset($_POST['uname']) && $_POST['uname'] != '' && $_POST['uid'] != '') {

            $mdl_msg = new Model_Messaging();
            foreach ($_POST as $k => $v) {
                if ($k != 'uid' && $k != 'uname' && $k != 'filtercheck') {
                    $mdl_msg->insertuserattr($_POST['uid'], $k, $v);
                }
            }
            $result = array(
                'ret' => true,
                'retmsg' => 'User Profile has been added'
            );
        }


        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function ajaxupdateuserAction() {
        $this->_helper->layout->disableLayout();
        $result = array(
            'ret' => false,
            'retmsg' => 'Invalid Parameters'
        );

        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($_POST);die();
        if (isset($_POST['uid']) && isset($_POST['uname']) && $_POST['uname'] != '' && $_POST['uid'] != '') {
            $mdl_msg = new Model_Messaging();
            foreach ($_POST as $k => $v) {
                if ($k != 'uid' && $k != 'uname' && $k != 'filtercheck') {
                    //$mdl_msg->updateuserattr($_POST['uid'],$k,$v);
                    $mdl_msg->insertuserattr($_POST['uid'], $k, $v);
                }
            }
            $result = array(
                'ret' => true,
                'retmsg' => 'User Profile has been updates'
            );
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function ajaxupdategroupAction() {
        $this->_helper->layout->disableLayout();
        $result = array(
            'ret' => false,
            'retmsg' => 'Invalid Parameters'
        );

        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($_POST);die();
        if (isset($_POST['gid']) && isset($_POST['gname']) && $_POST['gname'] != '' && $_POST['gid'] != '') {
            $mdl_msg = new Model_Messaging();
            foreach ($_POST as $k => $v) {
                if ($k != 'gid' && $k != 'gname' && $k != 'filtercheck') {
                    //$mdl_msg->updategroupattr($_POST['gid'],$k,$v);
                    $mdl_msg->insertgroupattr($_POST['gid'], $k, $v);
                }
            }
            $result = array(
                'ret' => true,
                'retmsg' => 'Group Profile has been updates'
            );
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function ajaxgroupslistAction() {
        $this->_helper->layout->disableLayout();
        //$this->_helper->viewRenderer->setNoRender(true);
        try {
            $auth = Zend_Auth::getInstance();
            $identity = $auth->getIdentity();
        } catch (Exception $e) {
            
        }
        //Zend_Debug::dump($identity);die();
        $sEcho = intval($_GET['sEcho']);
        ;
        $iTotalRecords = 0;
        $iDisplayLength = intval($_GET['iDisplayLength']);
        $iDisplayStart = intval($_GET['iDisplayStart']);
        $records = array();
        $records["aaData"] = array();

        $params = $this->getRequest()->getParams();
        $mdl_msg = new Model_Messaging();
        $countgroup = $mdl_msg->count_listgroup($params);
        //Zend_Debug::dump($countgroup);die();
        $listgroup = $mdl_msg->get_listgroup($params);
        //Zend_Debug::dump($listgroup);die();

        $iTotalRecords = intval($countgroup);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;

        $end = $iDisplayStart + $iDisplayLength;

        $idx = 0;
        foreach ($listgroup as $k => $v) {
            $act = '<div style="text-align:center;vertical-align:top;min-width:30px">
					<a href="/messaging/editgroup/gid/' . $k . '/gname/' . $v['name'] . '" class="btn btn-xs blue btn-editgroups" style="display:inline-block;padding: 2px 5px;float: left;" data-id="' . $k . '" ';
            foreach ($v as $k2 => $v2) {
                $act .='data-' . $k2 . '="' . $v2 . '" ';
            }
            $act .= '><i class="fa fa-edit"></i></a>
				</div>';

            $records["aaData"][$idx] = array(
                $act,
                ($iDisplayStart + $idx + 1) . '<input type="hidden" name="gid[' . $k . ']" id="gid' . $k . '" value="' . json_encode($v) . '"/>',
                $v['name'],
                $v['telegram'],
                $v['gtalk'],
                $v['email'],
                $v['whatsapp']
            );

            $idx++;
        }
        //Zend_Debug::dump($records);die();

        $records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }

    public function ajaxuserslistAction() {
        $this->_helper->layout->disableLayout();
        //$this->_helper->viewRenderer->setNoRender(true);
        try {
            $auth = Zend_Auth::getInstance();
            $identity = $auth->getIdentity();
        } catch (Exception $e) {
            
        }
        //Zend_Debug::dump($identity);die();
        $sEcho = intval($_GET['sEcho']);
        ;
        $iTotalRecords = 0;
        $iDisplayLength = intval($_GET['iDisplayLength']);
        $iDisplayStart = intval($_GET['iDisplayStart']);
        $records = array();
        $records["aaData"] = array();

        $params = $this->getRequest()->getParams();
        $mdl_msg = new Model_Messaging();
        $countgroup = $mdl_msg->count_listuser($params);
        //Zend_Debug::dump($countgroup);die();
        $listgroup = $mdl_msg->get_listuser($params);
        //Zend_Debug::dump($listgroup);die();

        $iTotalRecords = intval($countgroup);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;

        $end = $iDisplayStart + $iDisplayLength;

        $idx = 0;
        foreach ($listgroup as $k => $v) {
            $act = '<div style="text-align:center;vertical-align:top;min-width:30px">
					<a href="/messaging/edituser/uid/' . $k . '/uname/' . $v['uname'] . '" class="btn btn-xs blue btn-editusers" style="display:inline-block;padding: 2px 5px;float: left;" data-id="' . $k . '" ';
            foreach ($v as $k2 => $v2) {
                $act .='data-' . $k2 . '="' . $v2 . '" ';
            }
            $act .= '><i class="fa fa-edit"></i></a>
				</div>';

            $records["aaData"][$idx] = array(
                $act,
                ($iDisplayStart + $idx + 1) . '<input type="hidden" name="uid[' . $k . ']" id="uid' . $k . '" value="' . json_encode($v) . '"/>',
                $v['uname'],
                $v['fullname'],
                $v['telegram'],
                $v['gtalk'],
                $v['email'],
                $v['whatsapp']
            );

            $idx++;
        }
        //Zend_Debug::dump($records);die();

        $records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }

    public function addcronalertAction() {
        
    }

    public function managealertsAction() {
        try {
            $auth = Zend_Auth::getInstance();
            $identity = $auth->getIdentity();
        } catch (Exception $e) {
            
        }
        //Zend_Debug::dump($identity);die();
        $mdl_crn = new Model_Crontab();

        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($params);die();
        if (isset($params['id']) && $params['id'] != '' && isset($params['act']) && $params['act'] != '') {
            $check = $mdl_crn->checkIDCrontab($params['id']);
            //Zend_Debug::dump($check);die();
            if ($check) {
                switch ($params['act']) {
                    case 'deleted':
                        $check = $mdl_crn->deleteCrontab($params['id']);
                        break;
                    case 'disabled':
                        $check = $mdl_crn->deactivateCrontab($params['id']);
                        break;
                    case 'enabled':
                        $check = $mdl_crn->activateCrontab($params['id']);
                        break;
                    default:
                        break;
                }
            }
        }

        $cronalert = $mdl_crn->getallAlertCrontab();
        //Zend_Debug::dump($cronalert);die();
        $alert = array();
        foreach ($cronalert as $k => $v) {
            $tmp1 = explode(" ", $v['info']);
            //Zend_Debug::dump($tmp1);die();
            $tmp2 = explode("|", $tmp1[1]);
            //Zend_Debug::dump($tmp2);die();
            $id = $tmp2[0];
            $user = $tmp2[1];
            $created_at = $tmp2[2];
            $start = $tmp2[3];
            $end = $tmp2[4];
            $interval = $tmp2[5];
            $tmp1 = explode(" ", $v['cron']);
            $status = 'enable';
            $tmp2 = explode("/", $tmp1[5]);
            //$config = $tmp1[0]." ".$tmp1[1]." ".$tmp1[2]." ".$tmp1[3]." ".$tmp1[4];
            //$minute = $tmp1[0];
            //$hour = $tmp1[1];
            //$dayofmonth = $tmp1[2];
            //$month = $tmp1[3];
            //$dayofweek = $tmp1[4];
            if ($tmp1[0] == '#disable') {
                $status = 'disable';
                $tmp2 = explode("/", $tmp1[6]);
                //$config = $tmp1[1]." ".$tmp1[2]." ".$tmp1[3]." ".$tmp1[4]." ".$tmp1[5];
                //$minute = $tmp1[1];
                //$hour = $tmp1[2];
                //$dayofmonth = $tmp1[3];
                //$month = $tmp1[4];
                //$dayofweek = $tmp1[5];
            }
            //Zend_Debug::dump($tmp2);die();
            $file = $tmp2[7];
            //$msg = file_get_contents("http://10.62.8.133/cronalert/msg/".str_replace(".sh",".php",$file)."?get=msg");
            //$msg = json_decode($msg);
            //Zend_Debug::dump($msg);die();
            //$target = file_get_contents("http://10.62.8.133/cronalert/msg/".str_replace(".sh",".php",$file)."?get=target");
            //$target = json_decode($target,true);
            //Zend_Debug::dump($target);die();
            $alert[$id] = array(
                'id' => $id,
                'user' => $user,
                'created_at' => $created_at,
                'status' => $status,
                'start' => $start,
                'end' => $end,
                'interval' => $interval,
                //'config'=>$config,
                //'minute'=>$minute,
                //'hour'=>$hour,
                //'dayofmonth'=>$dayofmonth,
                //'month'=>$month,
                //'dayofweek'=>$dayofweek,
                'file' => $file,
                    //'msg'=>$msg->msg,
                    //'target'=>$target
            );
            //Zend_Debug::dump($alert[$id]);die();
        }
        //Zend_Debug::dump($alert);die();
        $this->view->alert = $alert;
    }

    public function detailalertAction() {
        try {
            $auth = Zend_Auth::getInstance();
            $identity = $auth->getIdentity();
        } catch (Exception $e) {
            
        }
        //Zend_Debug::dump($identity);die();
        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($params);die();
        if (!isset($params['id']) || $params['id'] == '') {
            #	$this->_redirect('/messaging/managealerts');
        }
        $mdl_crn = new Model_Crontab();
        $cronalert = $mdl_crn->getAlertCrontab($params['id']);
        //Zend_Debug::dump($cronalert);die();
        if (!isset($cronalert['info'])) {
            #$this->_redirect('/messaging/managealerts');
        }
        $alert = array();
        $tmp1 = explode(" ", $cronalert['info']);
        $tmp2 = explode("|", $tmp1[1]);
        //Zend_Debug::dump($tmp2);die();
        $id = $tmp2[0];
        $user = $tmp2[1];
        $created_at = $tmp2[2];
        $tmp1 = explode(" ", $cronalert['cron']);
        $status = 'enable';
        $tmp2 = explode("/", $tmp1[5]);
        $config = $tmp1[0] . " " . $tmp1[1] . " " . $tmp1[2] . " " . $tmp1[3] . " " . $tmp1[4];
        $minute = $tmp1[0];
        $hour = $tmp1[1];
        $dayofmonth = $tmp1[2];
        $month = $tmp1[3];
        $dayofweek = $tmp1[4];
        if ($tmp1[0] == '#disable') {
            $status = 'disable';
            $tmp2 = explode("/", $tmp1[6]);
            $config = $tmp1[1] . " " . $tmp1[2] . " " . $tmp1[3] . " " . $tmp1[4] . " " . $tmp1[5];
            $minute = $tmp1[1];
            $hour = $tmp1[2];
            $dayofmonth = $tmp1[3];
            $month = $tmp1[4];
            $dayofweek = $tmp1[5];
        }
        //Zend_Debug::dump($tmp2);die();
        $file = $tmp2[7];
#	$msg = file_get_contents("http://10.62.8.133/cronalert/msg/".str_replace(".sh",".php",$file)."?get=msg");
        $msg = json_decode($msg);
        //Zend_Debug::dump($msg);die();
#	$target = file_get_contents("http://10.62.8.133/cronalert/msg/".str_replace(".sh",".php",$file)."?get=target");
        $target = json_decode($target, true);
        //Zend_Debug::dump($target);die();
        $alert = array(
            'id' => $id,
            'created by' => $user,
            'created at' => $created_at,
            'status' => $status,
            'message' => $msg->msg,
            'file' => $file,
            'config' => $config,
            'minute' => $minute,
            'hour' => $hour,
            'day of month' => $dayofmonth,
            'month' => $month,
            'day of week' => $dayofweek,
            'target' => $target
        );
        //Zend_Debug::dump($alert);die();
        $this->view->alert = $alert;
    }

}
