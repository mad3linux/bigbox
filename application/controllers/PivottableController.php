<?php
class PivottableController extends Zend_Controller_Action {

public function flexAction() {
	$auth = Zend_Auth::getInstance ();
	$identity = $auth->getIdentity ();
	$params = $this->getRequest()->getParams ();

	$dd = new Model_Prabasystemaddon();
	$apis = $dd->get_api_grouping();
	$this->data->listapi = $apis;
	$tmp = $this->restorearray((array)$apis);
	$data_api = json_decode(json_encode($tmp), true);  
	$this->view->headScript()->appendScript("var listapi = ".json_encode($data_api).";" );
	
	$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/flexmonster/flexmonster.min.css?d='.date("YmdHis"));
	
	// $this->view->headScript ()->appendFile ( '/assets/core/plugins/react/prop-types.js?d='.date("YmdHis"));
	// $this->view->headScript()->appendScript("import PropTypes from 'prop-types';");
	// $this->view->headScript ()->appendFile ( '/assets/core/plugins/react/react.development.js?d='.date("YmdHis"));
	// $this->view->headScript ()->appendFile ( '/assets/core/plugins/react/react-dom.development.js?d='.date("YmdHis"));
	$this->view->headScript ()->appendFile ( '/pivottable/flexmonster/toolbar/flexmonster.toolbar.js?d='.date("YmdHis"));
	$this->view->headScript ()->appendFile ( '/pivottable/flexmonster/flexmonster.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
	$this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
	$this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
	$this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
	$this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.min.js');
	$this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css');
	$this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js');
	// die("www");
}

public function databuilderAction() {
	ini_set("memory_limit",-1);
	$this->_helper->layout->disableLayout();
	$result = array('retCode' => '10',
					'retMsg' => 'Invalid Parameter',
					'result' => false,
					'data' => null);
	$cc = new Model_System();
	if($_POST) {
		$POST = $this->restorearray($_POST);
		// Zend_Debug::dump($POST);die('www');
		$data_api = array();
		$dbpath = "/var/www/html/pylib/bigbox/api/";
		$flname = "prabacapi_".$POST['api'].'_'.md5($POST['param']).'.csv';
		// Zend_Debug::dump($flname);//die('www');
		$hasdb = file_exists($dbpath.$flname);
		// Zend_Debug::dump($hasdb);die('www');
		try{
			if($hasdb){
				function str_getcsvtab($n){
					return str_getcsv($n,"\t");
				}
				function str_getcsvdel($n){
					return str_getcsv($n,"|");
				}
				function array_combine2($arr1, $arr2) {
					$count = min(count($arr1), count($arr2));
					return array_combine(array_slice($arr1, 0, $count), array_slice($arr2, 0, $count));
				}
				$out = array();
				$rows = array_map('str_getcsvdel', file($dbpath.$flname));
				$header = array_shift($rows);
				// Zend_Debug::dump($header);//die('www');
				$data = array();
				// Zend_Debug::dump($rows);die();
				foreach ($rows as $row) {
					$tmp = array();
					foreach($row as $k=>$v){
						// Zend_Debug::dump($v);die('www');
						if($v=="~NULL~"){
							$tmp[$k] = null;
						}else{
							$tmp[$k] = str_replace("{newline}","\r\n",str_replace("#","|",$v));
						}
					}
					// Zend_Debug::dump($row);die('www');
				  $data[] = array_combine($header, $tmp);
				}
				
				$result['retCode'] = '00';
				$result['retMsg'] = "Success Call API";
				$result['result'] = true;
				$tmpx = $this->restorearray((array)$data);
				$data_api = json_decode(json_encode($tmpx), true);  
				$result['dbfile'] = $dbpath.$flname;
				$result['data'] = $data_api;
			}
		}catch(Exception $e){
			$hasdb = false;
		}
		
		if(!$hasdb){
			// Zend_Debug::dump($dbpath.$flname);die();
			// Zend_Debug::dump($POST);die();
			$tmp = $cc->exec_api($POST['api'],$POST['param']);
			// Zend_Debug::dump($tmp);die();
			$_POST['internal'] = true;
			$this->flatdbbuilderAction();
			// Zend_Debug::dump($tmp);die();
			if($tmp['transaction']){
				$result['retCode'] = '00';
				$result['retMsg'] = "Success Call API";
				$result['result'] = true;
				$tmpx = $this->restorearray((array)$tmp['data']);
				$data_api = json_decode(json_encode($tmpx), true);  
				$result['dbfile'] = $dbpath.$flname;
				$result['data'] = $data_api;
			}else{
				$result['retCode'] = '11';
				$result['retMsg'] = "Failed Call API";
			}
		}
		// Zend_Debug::dump($data_api);die();
	}
	header("Access-Control-Allow-Origin: *");
	header('Content-Type: application/json');           
	echo json_encode($result);
	die();
}

public function flatdbbuilderAction() {
	ini_set("memory_limit",-1);
	$this->_helper->layout->disableLayout();
	$params = $this->getRequest()->getParams ();
	$result = array('retCode' => '10',
					'retMsg' => 'Invalid Parameter',
					'result' => false,
					'data' => null);
	// Zend_Debug::dump($params); die();
	if($params && isset($params['api'])) {
		$POST = $this->restorearray($params);
		$cc = new Model_System();
		$tmp = $cc->exec_api($POST['api'],$POST['param']);
		if($tmp['transaction']){
			$tmpx = $this->restorearray((array)$tmp['data']);
			$data_api = json_decode(json_encode($tmpx), true);
			// Zend_Debug::dump($data_api[0]); die();
			$dbpath = "/var/www/html/pylib/bigbox/api/";
			$flname = "prabacapi_".$POST['api'].'_'.md5($POST['param']).'.csv';
			// unlink($dbpath.$flname.'.csv');
			// Zend_Debug::dump($data_api); die();
			if(count($data_api)>0){
				$col = array();
				foreach($data_api[0] as $k=>$v){
					$col[trim(str_replace(" ","_",str_replace(".","_",$k)))] = gettype($v);
				}
				// Zend_Debug::dump($col); die();
				try{
					// Zend_Debug::dump(is_writable($dbpath.$flname)); die();
					// Zend_Debug::dump($dbpath.$flname); die();
					$fp = fopen($dbpath.$flname, 'w') or die('Could not open file');
					fputcsv($fp, array_keys($col), "|");
					
					foreach($data_api as $k=>$v){
						$val = array();
						foreach($v as $k2=>$v2){
							$val[] = trim(str_replace(array("\r\n", "\r", "\n"),"{newline}",str_replace("|","#",($v2==null)?"~NULL~":$v2)));
						}
						// Zend_Debug::dump($val); die();
						fputcsv($fp, $val, "|");
					}
					fclose($fp);
					
					$result['retCode'] = '00';
					$result['retMsg'] = "Success Create FlatDB";
					$result['result'] = true;
					$result['data']['dbpath'] = $dbpath;
					$result['data']['flname'] = $flname;
					$result['data']['total_data'] = count($data_api);
					// Zend_Debug::dump($result); die();
				}catch(Exception $e){
					// die("failed: ".$e->getMessage());
					$result['retCode'] = '12';
					$result['retMsg'] = "Failed Create FlatDB";
				}
			}else{
				$result['retCode'] = '01';
				$result['retMsg'] = "No Data";
			}
		}else{
			$result['retCode'] = '11';
			$result['retMsg'] = "Failed Call API";
		}
	}
	if(isset($_POST['internal']) && $_POST['internal']==true){
		return $result;
	}
	header("Access-Control-Allow-Origin: *");
	header('Content-Type: application/json');           
	echo json_encode($result);
	die();
}

public function adminajaxAction() {
	$this->_helper->layout->disableLayout();
	$data = array(
		"error"=>true,
		"message"=>""
	);
	// Zend_Debug::dump($_POST); die();
	if($_POST['action']=='amcharts_editor_get_current_user'){
		try {
			$authAdapter = Zend_Auth::getInstance();
			$identity = $authAdapter->getIdentity();
		}
		catch(Exception $e) {
		}
		// Zend_Debug::dump($identity); die();
		$data['error'] = false;
		$data['id'] = $identity->uid;
		// Zend_Debug::dump($data); die();
		// Zend_Debug::dump($this->restorearray((array)$identity)); die();
		$data = array_merge($data,$this->restorearray((array)$identity));
		// Zend_Debug::dump($data); die();
	// }
	// else if($_POST['action']=='amcharts_editor_get_chart_list'){
		// echo '{"error":true,"message":"You need to be logged in to get chart list","login_url":"https:\/\/www.amcharts.com\/sign-in\/editor\/?skinny=1&redirect_to=http%3A%2F%2Flive.amcharts.com%2Fauth%2F%3Faction%3Ddone"}';
		// die();
	}else if($_POST['action']=='amcharts_editor_save_chart'){
		// Zend_Debug::dump($_POST); die();
		$POST = $this->restorearray((array)$_POST);
		$ns = Zend_Session::namespaceGet('pivotamchart');
		// Zend_Debug::dump($ns);die();
		// Zend_Debug::dump($POST); die();
		if(isset($ns['report']) && isset($ns['sql']) && isset($ns['apiid']) && isset($ns['apiparam']) && isset($ns['dbfile'])){
			$ns['report'] = json_encode($ns['report']);
			$p = array(
				'name'=>(isset($POST['title']))?$POST['title']:"amchartpivot".$ns['apiid']."_".date("YmdHis"),
				'title'=>(isset($POST['title']))?$POST['title']:"amchartpivot".$ns['apiid']."_".date("YmdHis"),
				'weight'=>"",
				'pcolor'=>"",
				'type'=>"",
				'width'=>""
			);
			$par = array_merge_recursive($ns,$POST);
			// Zend_Debug::dump($par); die();
			// if(isset($ns['pid'])){
			$c = new Model_Prabasystem();
			if(isset($par['pid']) && $par['pid']!=""){
				$par['tpl'] = str_replace("{{pid}}",$par['pid'],$par['tpl']);
				$par['tpl2'] = str_replace("{{pid}}",$par['pid'],$par['tpl2']);
				$par["chart_id"] = $par['pid'];
				$ret = $c->add_update_portlet_pivotamchart($par,$p);
				$data['id'] = $ret['id'];
				$data['error'] = false;
			}else{
				$ret = $c->add_update_portlet_pivotamchart($par,$p);
				if($ret['result'] && isset($ret['id'])){
					$par['pid'] = $ret['id'];
					$par['tpl'] = str_replace("{{pid}}",$par['pid'],$par['tpl']);
					$par['tpl2'] = str_replace("{{pid}}",$par['pid'],$par['tpl2']);
					$par["chart_id"] = $par['pid'];
					// Zend_Debug::dump($par); die();
					$c->add_update_portlet_pivotamchart($par,$p);
					$data['id'] = $ret['id'];
					$data['error'] = false;
				}
			}
			// Zend_Debug::dump($ret); die();
		}
	}
	// else if($_POST['action']=='amcharts_editor_get_chart'){
		// if(Zend_Session::namespaceGet('rboco')) {
			// $ns = Zend_Session::namespaceGet('rboco');
			// // Zend_Debug::dump($ns); die();
			// $page = $this->cache()->load("graph_".$_POST['chart_id']);
			// // Zend_Debug::dump($page); die();
			// if(isset($ns['graphic']) && isset($page[$_POST['chart_id']])){
				// $conf = array();
				// $grp = array();
				// $grp = $page[$_POST['chart_id']]['p_attr']['graphic'];
				// $conf = $grp['config'];
				// // Zend_Debug::dump($conf); die();
				// $data['error'] = false;
				// $data['code'] = (
					// (isset($conf['chartCFG']))?$conf['chartCFG']:(
						// (isset($conf['script']))?$conf['script']:array())
				// );
				// // if(isset($conf['form'])){
				// // Zend_Debug::dump($data); die();
				// $data['form'] = (
					// (isset($grp['form']))?$grp['form']:(
						// (isset($grp['attr']))?$grp['attr']:array())
				// );
				// // }
				// $data['template'] = (
					// (isset($conf['chartTPL']))?$conf['chartTPL']:(
						// (isset($conf['template']))?$conf['template']:array())
				// );
				// $data['css'] = (
					// (isset($conf['chartCSS']))?$conf['chartCSS']:(
						// (isset($conf['css']))?$conf['css']:array())
				// );
				// $data['provdata'] = (
					// (isset($grp['data']))?$grp['data']:(
						// (isset($data['code']['dataProvider']))?$data['code']['dataProvider']:array())
				// );
				// $data['owner'] = true;
			// }else if($ns['graphic']['edit'] == 1) {
				// $cx = new Prabacontent_Model_Builder();
				// $csci = $cx->get_a_portlet($_POST['chart_id']);
				// // Zend_Debug::dump($csci['attr1']); die();
				// if($csci['attr1'] != "" && $params['edit'] == "") {
					// $data['error'] = false;
					// $attr = json_decode(json_encode(unserialize($csci['attr1'])), true);
					// $attr2 = $this->restorearray((array)$attr);
					// // $code = json_decode($attr['script']);
					// // Zend_Debug::dump($attr2); die();
					// // $data['code'] = json_decode($attr2['script'], true);
					// // $data['template'] = json_decode($attr2['template'], true);
					// // $data['css'] = json_decode($attr2['css'], true);
					// // $data['provdata'] =  $data['code']['dataProvider'];
					// $cfg = (
						// (isset($attr2['chartCFG']))?$attr2['chartCFG']:(
							// (isset($attr2['script']))?$attr2['script']:"{}")
					// );
					// $data['code'] = json_decode($cfg, true);
					// // if(isset($conf['form'])){
					// $form = (isset($attr2['form']))?$attr2['form']:"{}";
					// $data['form'] = json_decode($form, true);
					// $form2 = (isset($attr2['form2']))?$attr2['form2']:"{}";
					// $data['form2'] = json_decode($form2, true);
					// // }
					// $tpl= (
						// (isset($attr2['chartTPL']))?$attr2['chartTPL']:(
							// (isset($attr2['template']))?$attr2['template']:"{}")
					// );
					// $data['template'] = json_decode($tpl, true);
					// $css = (
						// (isset($attr2['chartCSS']))?$attr2['chartCSS']:(
							// (isset($attr2['css']))?$attr2['css']:"{}")
					// );
					// $data['css'] = json_decode($css, true);
					// $provdata = (
						// (isset($attr2['data']))?$attr2['data']:(
							// (isset($data['code']['dataProvider']))?json_encode($data['code']['dataProvider']):"[]")
					// );
					// $data['provdata'] = json_decode($provdata, true);
					// $data['owner'] = true;
				// }
			// }
		// }
		// // echo '{"error":false,"message":"","id":0,"login_url":"https:\/\/www.amcharts.com\/sign-in\/editor\/?skinny=1&redirect_to=http%3A%2F%2Flive.amcharts.com%2Fauth%2F%3Faction%3Ddone"}';
	// }else{
		// echo '{"error":false,"message":"","id":0,"login_url":"https:\/\/www.amcharts.com\/sign-in\/editor\/?skinny=1&redirect_to=http%3A%2F%2Flive.amcharts.com%2Fauth%2F%3Faction%3Ddone"}';
		// die();
	// }
	header("Access-Control-Allow-Origin: *");
	header('Content-Type: application/json');
	// header('Content-Type: plain/text');
	$tmp = $this->restorearray((array)$data);
	// unset($tmp['code']['legend']);
	$data = json_decode(json_encode($tmp), true);
	// Zend_Debug::dump(json_last_error());die();
	echo json_encode($data);
	die();
}

public function getamchartAction(){
	$this->_helper->layout->disableLayout();
	$params = $this->getRequest()->getParams();
	$data_api = array();
	if(isset($params['id'])){
        $cd = new Model_Zprabapage();
        $csci = $cd->get_portlet($params['id']);
		if(isset($csci['portlet_type']) && $csci['portlet_type']=="amchartpivot"){
			$dt = $csci['content_attrs'];
			$grp = array();
			function str_getcsvtab($n){
				return str_getcsv($n,"\t");
			}
			function str_getcsvdel($n){
				return str_getcsv($n,"|");
			}
			function array_combine2($arr1, $arr2) {
				$count = min(count($arr1), count($arr2));
				return array_combine(array_slice($arr1, 0, $count), array_slice($arr2, 0, $count));
			}
			$ns = array();
			$ns['sql'] = $dt['sql'];
			$out = array();
			exec('/var/www/html/pylib/qfile -H -d "|" -O -D "|" "'.$ns['sql'].' "', $out);
			$rows = array_map('str_getcsvdel', $out);
			$header = array_shift($rows);
			$data = array();
			foreach ($rows as $row) {
				$tmp = array();
				foreach($row as $k=>$v){
					if($v=="~NULL~"){
						$tmp[$k] = null;
					}else{
						$tmp[$k] = str_replace("{newline}","\r\n",str_replace("#","|",$v));
					}
				}
			  $data[] = array_combine($header, $tmp);
			}
			
			$grp = $this->restorearray((array)$data);
			
			$ns['report'] = json_decode($dt['report'],true);
			$row = $ns['report']["rows"];
			$col = $ns['report']["columns"];
			$mea = $ns['report']["measures"];
			
			$rows = array();
			foreach($row as $v){
				$rows[] = str_replace(".","_",$v['uniqueName']);
			}
			$cols = array();
			foreach($col as $v){
				if($v['uniqueName']!="[Measures]"){
					$cols[] = str_replace(".","_",$v['uniqueName']);
				}
			}
			
			$meas = array();
			foreach($mea as $v){
				$meas[] = $v["aggregation"]."_of_".str_replace(".","_",$v['uniqueName']);
			}
			
			foreach($grp as $k=>$v){
				$rowfield = array();
				$rowval = array();
				foreach($rows as $y){
					$rowfield[] = $v[$y];
				}
				$rowfield = implode("::",$rowfield);
				if(!isset($grpdata[$rowfield])){
					$grpdata[$rowfield] = array();
				}
				
				$colfield = array();
				foreach($cols as $y){
					$colfield[] = $v[$y];
				}
				$colfield = implode("::",$colfield);
				if(!isset($grpdata[$rowfield][$colfield])){
					$grpdata[$rowfield][$colfield] = array();
				}
				
				foreach($meas as $k3=>$v3){
					$grpdata[$rowfield][$colfield][$v3] = $v[$v3];
				}
			}
			function cmp($a, $b){
				return strcasecmp($a, $b);
			}
			uksort($grpdata, "cmp");
			
			$keys = array_keys($grpdata);
			foreach($grpdata as $k=>$v){
				uksort($v, "cmp");
				foreach($v as $k2=>$v2){
					$idx++;
					$tmp = array(
						"no"=>$idx,
						"rows"=>$k,
						"columns"=>$k2
					);
					foreach($v2 as $k3=>$v3){
						$tmp[$k3] = $v3;
					}
					$grpdata2[] = $tmp;
				}
			}
		}
	}
	header("Access-Control-Allow-Origin: *");
	header('Content-Type: application/json');
	$tmp = $this->restorearray((array)$grpdata2);
	$data_api = json_decode(json_encode($tmp), true);             
	echo json_encode($data_api);
	die();
}

public function amchartAction() {
	ini_set("memory_limit",-1);
	$params = $this->getRequest()->getParams();
	// Zend_Debug::dump($params);// die();
	$ns = Zend_Session::namespaceGet('pivotamchart');
	// Zend_Debug::dump($ns); die();
	
	if(isset($params['pid'])) {
		$ns = new Zend_Session_Namespace('pivotamchart');
		$ns->id = mktime();
		$ns->pid = $params['pid'];
		$pid = $ns->pid;

        $cd = new Model_Zprabapage();
        $csci = $cd->get_portlet($ns->pid);

		// Zend_Debug::dump($csci); die();
		if(isset($csci['portlet_type']) && $csci['portlet_type']!="amchartpivot"){
			Zend_Session::namespaceUnset('pivotamchart');
			$this->_redirect("/prabaeditor/listportlet");
		}else if(isset($csci['portlet_type']) && $csci['portlet_type']=="amchartpivot"){
			// Zend_Debug::dump($csci); die();
			if ($csci['content_attrs'] == "" || $csci['content_attrs'] == null){
				Zend_Session::namespaceUnset('pivotamchart');
				$this->_redirect("/pivottable/flex/pid/".$pid);
			}else{
				$dt = $csci['content_attrs'];
				// Zend_Debug::dump($dt); die();
				if(isset($dt['report']) && isset($dt['sql']) && isset($dt['apiid']) && isset($dt['apiparam']) && isset($dt['code']) && isset($dt['dbfile'])){
					$new = false;
					$ns->report = json_decode($dt['report'],true);
					$ns->sql = $dt['sql'];
					$ns->apiid = $dt['apiid'];
					$ns->apiparam = $dt['apiparam'];
					$ns->dbfile = $dt['dbfile'];
					$ns->chartopt = $dt['code'];
					$ns->cfg = array(
						"code"=>$dt['code'],
						"css"=>$dt['css'],
						"template"=>$dt['template'],
						"title"=>$dt['title'],
						"description"=>$dt['description'],
						"int_save"=>$dt['int_save']
					);
					$ns->on_edit = 1;
					// $var['ns'] = json_decode(json_encode($ns),true);
					// $this->cache()->save($var, "amchartpivot_".$ns['pid'], array(
						// 'port_' . $ns['pid'], 'pg_' . $ns['pid']
					// ));
					// Zend_Debug::dump($ns); die();
					$ns = Zend_Session::namespaceGet('pivotamchart');
					// Zend_Debug::dump($ns); die();
				}else{
					Zend_Session::namespaceUnset('pivotamchart');
					$this->_redirect("/pivottable/flex/pid/".$pid);
				}
			}
		}else {
			Zend_Session::namespaceUnset('pivotamchart');
			$this->_redirect("/pivottable/flex");
		}
	}else if($ns && isset($ns['pid'])){
		$pid = $ns['pid'];
        $cd = new Model_Zprabapage();
        $csci = $cd->get_portlet($ns['pid']);

		$uri = $this->getRequest()->getrequestUri();
		// Zend_Debug::dump($uri); die();
		// Zend_Debug::dump($csci); die();
		if(isset($csci['portlet_type']) && $csci['portlet_type']!="amchartpivot"){
			Zend_Session::namespaceUnset('pivotamchart');
			$this->_redirect("/prabaeditor/listportlet");
		}else if(isset($csci['portlet_type']) && $csci['portlet_type']=="amchartpivot"){                      
			// Zend_Debug::dump($uri); die('www');
			if ($csci['content_attrs'] == "" || $csci['content_attrs'] == null){
				// Zend_Debug::dump($uri); die('qqq');
				Zend_Session::namespaceUnset('pivotamchart');
				$this->_redirect("/pivottable/flex/pid/".$pid);
			}else if($csci['content_attrs'] != "" && $csci['content_attrs'] != null){
				// Zend_Debug::dump($uri); die('eeee');
				$dt = $csci['content_attrs'];
				if(isset($dt['report']) && isset($dt['sql']) && isset($dt['apiid']) && isset($dt['apiparam']) && isset($dt['code']) && isset($dt['dbfile'])){
					$new = false;
					$ns = new Zend_Session_Namespace('pivotamchart');
					$ns->report = json_decode($dt['report'],true);
					$ns->sql = $dt['sql'];
					$ns->apiid = $dt['apiid'];
					$ns->apiparam = $dt['apiparam'];
					$ns->chartopt = $dt['code'];
					$ns->dbfile = $dt['dbfile'];
					$ns->cfg = array(
						"code"=>$dt['code'],
						"css"=>$dt['css'],
						"template"=>$dt['template'],
						"title"=>$dt['title'],
						"description"=>$dt['description'],
						"int_save"=>$dt['int_save']
					);
					$ns->on_edit = 1;
					// $this->_redirect("/pivottable/amchart/edit/".$ns->pid);
				}else{
					Zend_Session::namespaceUnset('pivotamchart');
					$this->_redirect("/pivottable/flex/pid/".$pid);
				}
			}
		}else {
			// Zend_Debug::dump($csci); die();
			Zend_Session::namespaceUnset('pivotamchart');
			$this->_redirect("/pivottable/flex");
		}
	}else{
		// Zend_Debug::dump($params); die();
		// die("qqq");
		// Zend_Session::namespaceUnset('pivotamchart');
		$new = true;
		if(!isset($params['report']) || !isset($params['sql']) || !isset($params['apiid']) || !isset($params['apiparam']) || !isset($params['dbfile'])){
			// Zend_Debug::dump($ns); die();
			if(!isset($ns['report']) || !isset($ns['sql']) || !isset($ns['apiid']) || !isset($ns['apiparam']) || !isset($ns['dbfile'])){
				$this->_redirect("/pivottable/flex");
			}
			$new = false;
			// Zend_Debug::dump($new); die();
		}else{
			Zend_Session::namespaceUnset('pivotamchart');
			$ns = new Zend_Session_Namespace('pivotamchart');
			$ns->id = mktime();
			$ns->report = json_decode($params['report'],true);
			$ns->sql = $params['sql'];
			$ns->apiid = $params['apiid'];
			$ns->apiparam = $params['apiparam'];
			$ns->dbfile = $params['dbfile'];
			$ns->on_edit = 0;
		}
		$ns = Zend_Session::namespaceGet('pivotamchart');
	}
	// Zend_Debug::dump($ns); die();
	$ns = json_decode(json_encode($ns),true);
	// Zend_Debug::dump($flname); die();
	// Zend_Debug::dump($ns); //die();
	$hasdb = file_exists($ns['dbfile']);
	// Zend_Debug::dump($hasdb); die();
	if(!$hasdb){
		Zend_Session::namespaceUnset('pivotamchart');
		$this->_redirect("/pivottable/flex");
	}
	// Zend_Debug::dump($params); die();
	
	$grp = array();
	try{
		function str_getcsvtab($n){
			return str_getcsv($n,"\t");
		}
		function str_getcsvdel($n){
			return str_getcsv($n,"|");
		}
		function array_combine2($arr1, $arr2) {
			$count = min(count($arr1), count($arr2));
			return array_combine(array_slice($arr1, 0, $count), array_slice($arr2, 0, $count));
		}
		$out = array();
		// Zend_Debug::dump('/var/www/html/pylib/qfile -H -d "|" -O -D "|" "'.$ns['sql'].' "');die('www');
		exec('/var/www/html/pylib/qfile -H -d "|" -O -D "|" "'.$ns['sql'].' "', $out);
		// Zend_Debug::dump($out);die('www');
		$rows = array_map('str_getcsvdel', $out);
		// Zend_Debug::dump($rows);die('www');
		$header = array_shift($rows);
		// Zend_Debug::dump($header);//die('www');
		$data = array();
		// Zend_Debug::dump($rows);die();
		foreach ($rows as $row) {
			$tmp = array();
			foreach($row as $k=>$v){
				// Zend_Debug::dump($v);die('www');
				if($v=="~NULL~"){
					$tmp[$k] = null;
				}else{
					$tmp[$k] = str_replace("{newline}","\r\n",str_replace("#","|",$v));
				}
			}
			// Zend_Debug::dump($row);die('www');
		  $data[] = array_combine($header, $tmp);
		}
		
		$grp = $this->restorearray((array)$data);
		// Zend_Debug::dump($grp);die('www');
	}catch(Exception $e){
		// Zend_Debug::dump($e);die('www');
		$this->_redirect("/pivottable/flex");
	}
	
	$chartopt = '{
		"titles": [{"text": "Chart Title","size": 15}],"type": "serial","theme": "light",
		"legend": {"horizontalGap": 10,"position": "bottom","useGraphSettings": true,"markerSize": 10},
		"valueAxes": [{"axisAlpha": 0.3,"gridAlpha": 0}],
		"graphs": {{graphs}},
		"export": {"enabled": true},"categoryField": "no",
		"categoryAxis": {"gridPosition": "start","axisAlpha": 0.5,"gridAlpha": 0,"labelRotation": 30,"position": "left","fontSize": 12,
			"guides": {{guides}},
			"labelFunction" : {}
		},
		"dataProvider": []
	}';
	
	// Zend_Debug::dump($chartopt);//die('www');
	$row = $ns['report']["rows"];
	// $chart["categoryField"] = $row[count($row)-1]["uniqueName"];
	$mainrow = $row[count($row)-1]["uniqueName"];
	// $chartopt = str_replace("{{category}}",$mainrow,$chartopt);
	// Zend_Debug::dump($chartopt);die('www');
	
	// Zend_Debug::dump($ns['report']);//die();
	$col = $ns['report']["columns"];
	$maincol = $col[count($col)-1]["uniqueName"];
	$mea = $ns['report']["measures"];
	// Zend_Debug::dump($mea);die();
	
	$rows = array();
	foreach($row as $v){
		$rows[] = str_replace(".","_",$v['uniqueName']);
	}
	// Zend_Debug::dump($rows);die();
	$cols = array();
	foreach($col as $v){
		if($v['uniqueName']!="[Measures]"){
			$cols[] = str_replace(".","_",$v['uniqueName']);
		}
	}
	$maincol = $row[count($cols)-1]["uniqueName"];
	// Zend_Debug::dump($cols);
	$meas = array();
	$graph = array();
	foreach($mea as $v){
		$uniqueName = str_replace(".","_",$v['uniqueName']);
		$meas[] = $v["aggregation"]."_of_".$uniqueName;
		$graph[] = array(
			"valueField"=> $v["aggregation"]."_of_".$uniqueName,
			"fillAlphas"=> 0.8,
			"labelText"=> "[[value]]",
			"lineAlpha"=> 0.3,
			"showHandOnHover"=> true,
			"title"=> $v["aggregation"]."_of_".$uniqueName,
			"type"=> "column",
			"color"=> "#000000",
			"balloonText"=> "<b>[[title]]</b> : [[value]]"
		);
	}
	// Zend_Debug::dump($graph);die();
	$chartopt = str_replace("{{graphs}}",json_encode($graph),$chartopt);
	// Zend_Debug::dump($chartopt);die('www');
	// Zend_Debug::dump($cols);
	// Zend_Debug::dump($meas);
	// Zend_Debug::dump($grp);die();
	$grpdata = array();
	$guide = array();
	foreach($grp as $k=>$v){
		$rowfield = array();
		$rowval = array();
		foreach($rows as $y){
			$rowfield[] = $v[$y];
		}
		$rowfield = implode("::",$rowfield);
		if(!isset($grpdata[$rowfield])){
			$grpdata[$rowfield] = array();
		}
		
		$colfield = array();
		foreach($cols as $y){
			$colfield[] = $v[$y];
		}
		$colfield = implode("::",$colfield);
		if(!isset($grpdata[$rowfield][$colfield])){
			$grpdata[$rowfield][$colfield] = array();
		}
		
		foreach($meas as $k3=>$v3){
			$grpdata[$rowfield][$colfield][$v3] = $v[$v3];
		}
	}
	// Zend_Debug::dump($grpdata);//die();
	
	$grpdata2 = array();
	$column = array();
	$guide = array();
	$key = null;
	$start = null;
	$end = null;
	$idx = 0;
	function cmp($a, $b){
		return strcasecmp($a, $b);
	}
	uksort($grpdata, "cmp");
	// Zend_Debug::dump($grpdata);die();
	$keys = array_keys($grpdata);
	foreach($grpdata as $k=>$v){
		// Zend_Debug::dump($v);//die();
		uksort($v, "cmp");
		// Zend_Debug::dump($v);die();
		foreach($v as $k2=>$v2){
			$idx++;
			if($key==null){
				$start = $idx;
				$end = $idx;
				$key = $k;
			}else if($key!=$k){
				$guide[] = array(
					"category"=> $start,
					"toCategory"=> $end,
					"lineAlpha"=> 0.5,
					"tickLength"=> strlen($k2)+50,
					"color"=>"red",
					"boldLabel"=>true,
					"balloonColor"=>"red",
					"balloonText"=>"row : ".implode(",",$rows)."<br>column : ".implode(",",$cols)."<br>measure : ".implode(",",$meas),
					// "above"=>true,
					"expand"=> true,
					"label"=> $key,
					"labelRotation"=> 0
				);
				$start = $idx;
				$end = $idx;
				$key = $k;
			}else{
				$end = $idx;
			}
			// Zend_Debug::dump($keys);
			// Zend_Debug::dump($k);
			// Zend_Debug::dump(array_search($k, $keys));die();
			$tmp = array(
				"no"=>$idx,
				// "section"=>$k2
				"rows"=>$k,
				"columns"=>$k2
			);
			foreach($v2 as $k3=>$v3){
				$tmp[$k3] = $v3;
			}
			$grpdata2[] = $tmp;
		}
	}
	$guide[] = array(
		"category"=> $start,
		"toCategory"=> $end,
		"lineAlpha"=> 0.5,
		"tickLength"=> strlen($k2)+50,
		"color"=>"red",
		"boldLabel"=>true,
		"balloonColor"=>"red",
		"balloonText"=>"row : ".implode(",",$rows)."<br>column : ".implode(",",$cols)."<br>measure : ".implode(",",$meas),
		// "above"=>true,
		"expand"=> true,
		"label"=> $key,
		"labelRotation"=> 0
	);
	// Zend_Debug::dump($guide);//die('www');
	$chartopt = str_replace("{{guides}}",json_encode($guide),$chartopt);
	$chart = json_decode($chartopt,true);
	// Zend_Debug::dump($grpdata2);die('www');
	$grpdata2 = $this->restorearray($grpdata2);
	// Zend_Debug::dump($chartopt);
	$chartopt = str_replace('"labelFunction" : {}','"labelFunction" : function( label, item ) {return String( item.dataContext.columns);}',$chartopt);
	$chartopt = str_replace('"dataProvider": []','"dataProvider": '.json_encode($grpdata2),$chartopt);
	// Zend_Debug::dump($chartopt);die('www');
	// Zend_Debug::dump(json_decode($chartopt,true));die('www');
	// $chart = json_decode($chartopt,true);
	$chart["dataProvider"] = $grpdata2;
	if($new){
		$ns = new Zend_Session_Namespace('pivotamchart');
		// Zend_Debug::dump($ns); die();
		$ns->grpdata = $grpdata2;
		$ns->chart = $chart;
		$ns->chartopt = $chartopt;
		// $this->_redirect("/pivottable/amchart");
		$ns = Zend_Session::namespaceGet('pivotamchart');
		// Zend_Debug::dump($ns); die();
	}
	// Zend_Debug::dump($ns);die('www');
	// Zend_Debug::dump($grpdata2);die('www');
	
	$this->view->ns = $ns;
	$this->view->chart = $chart;
	$this->view->chartopt = $chartopt;
	$this->view->grpdata = $grpdata2;
	$this->view->headLink()->appendStylesheet('/live.amcharts.com/vendor/jquery/css/ui-amcharts/jquery-ui-1.10.4.custom.css?d='.date("YmdHis"));
	$this->view->headLink()->appendStylesheet('/live.amcharts.com/vendor/jquery/plugins/chosen/jquery.chosen.css?d='.date("YmdHis"));
	$this->view->headLink()->appendStylesheet('/live.amcharts.com/vendor/jquery/plugins/spectrum/jquery.spectrum.css?d='.date("YmdHis"));
	$this->view->headLink()->appendStylesheet('/live.amcharts.com/vendor/jquery/plugins/handsontable/jquery.handsontable.css?d='.date("YmdHis"));
	$this->view->headLink()->appendStylesheet('/live.amcharts.com/vendor/jquery/plugins/handsontable/jquery.handsontable.removeRow.css?d='.date("YmdHis"));
	$this->view->headLink()->appendStylesheet('/live.amcharts.com/vendor/bootstrap/css/bootstrap.css?d='.date("YmdHis"));
	$this->view->headLink()->appendStylesheet('/live.amcharts.com/vendor/font-awesome/css/font-awesome.css?d='.date("YmdHis"));
	$this->view->headLink()->appendStylesheet('//fonts.googleapis.com/css?family=Covered+By+Your+Grace');

	$this->view->headLink()->appendStylesheet('/live.amcharts.com/dist/css/ameditor.core.css?d='.date("YmdHis"));
	$this->view->headLink()->appendStylesheet('/live.amcharts.com/dist/css/ameditor.icons.css?d='.date("YmdHis"));
	$this->view->headLink()->appendStylesheet('/live.amcharts.com/dist/css/ameditor.table.css?d='.date("YmdHis"));
	$this->view->headLink()->appendStylesheet('/live.amcharts.com/dist/css/ameditor.share.css?d='.date("YmdHis"));

	$this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/amcharts.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/funnel.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/gauge.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/pie.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/radar.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/serial.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/xy.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/themes/chalk.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/themes/dark.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/themes/light.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/themes/patterns.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/themes/black.js?d='.date("YmdHis"));

	$this->view->headLink()->appendStylesheet('/live.amcharts.com/vendor/amcharts/plugins/export/export.css?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/plugins/export/export.min.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/plugins/dataloader/dataloader.min.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/plugins/responsive/responsive.min.js?d='.date("YmdHis"));

	// $this->view->headScript()->appendFile('/live.amcharts.com/vendor/jquery/jquery-1.10.2.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/vendor/jquery/jquery-ui-1.10.4.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/vendor/jquery/plugins/router/jquery.router.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/vendor/jquery/plugins/layout/jquery.layout.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/vendor/jquery/plugins/chosen/jquery.chosen.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/vendor/jquery/plugins/spectrum/jquery.spectrum.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/vendor/jquery/plugins/handsontable/jquery.handsontable.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/vendor/jquery/plugins/handsontable/jquery.handsontable.removeRow.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/vendor/modernizr/modernizr-2.8.1.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/vendor/bootstrap/js/bootstrap.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/vendor/clipboard/clipboard.js?d='.date("YmdHis"));

	$this->view->headScript()->appendFile('/live.amcharts.com/dist/ameditor.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/dist/ameditor.config.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/dist/ameditor.core.js?d='.date("YmdHis"));
	$this->view->headScript()->appendScript("
		AmCharts.Editor.CONSTANT.URL_AMCHARTS = '/pivottable/amchart/';
		AmCharts.Editor.CONSTANT.URL_BASE  = '/live.amcharts.com/';
		AmCharts.Editor.CONSTANT.URL_CDN = '/live.amcharts.com/';
		AmCharts.Editor.CONSTANT.URL_CDN_AMCHARTS = '/live.amcharts.com/';
		//AmCharts.Editor.CONSTANT.URL_API =  '/live.amcharts.com/wp-admin/admin-ajax.php';
		AmCharts.Editor.CONSTANT.URL_API =  '/pivottable/adminajax';
		AmCharts.Editor.CONSTANT.URL_PATH = '/pivottable/';
		AmCharts.Editor.CONSTANT.RND_KEY  = '".date('Ymd-His')."';
		AmCharts.Editor.CONSTANT.COVER = '/live.amcharts.com/static/img/cover/9.JPG';"
	);
	$this->view->headScript()->appendFile('/live.amcharts.com/dist/ameditor.handler.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/dist/ameditor.router.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/dist/ameditor.samples.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/dist/lang/ameditor.en-US.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/dist/section/ameditor.landing.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/dist/section/ameditor.editor.js?d='.date("YmdHis"));
	$this->view->headScript()->appendFile('/live.amcharts.com/dist/section/ameditor.share.js?d='.date("YmdHis"));

	$this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
	$this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
	$this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
	$this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.min.js');
	$this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
	$this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css');
	$this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js');
}

public function restorearray($data = array()){
	$dup = array();
	foreach ($data as $key => $value) {
		if(is_array($value)){
			$tmp = $this->restorearray($value);
			$dup[$key] = $tmp;
		}else if(is_object($value)){
			$tmp = $this->restorearray((array)$value);
			$dup[$key] = $tmp;
		}else{
			if(is_numeric($value)){
				if(substr($value,0,1)=="0"){
					// Zend_Debug::dump(strpos($value,".",1));
					if(strpos($value,".",1)==false){
						$dup[$key] = $value;
					}else{
						$dup[$key] = $value+0;
					}
				}else{
					$dup[$key] = $value+0;
				}
			}else if($value=="true"){
				$dup[$key] = true;
			}else if($value=="false"){
				$dup[$key] = false;
			}else{
				$dup[$key] = $value;
			}
			// Zend_Debug::dump($dup[$key]);
		}
	}
	return $dup;
}

}