<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class PrabaconstructController extends Zend_Controller_Action {
	public function init() {
	}
	
	public function updatesocketAction( ) {
	try {
			$authAdapter = Zend_Auth::getInstance ();
			$identity = $authAdapter->getIdentity ();
		} catch ( Exception $e ) {
		}
			$this->_helper->layout->disableLayout ();
		
			if ($_POST) {
				$c = new Model_Socket();
				if (isset($_POST['running']) &&$_POST['running']==1 ) {
						
						$c->running_socket($_POST);
						
					
					$result=  array (
						'retCode' => '00',
						'retMsg' => 'socket server activated',
						'result' => true,
						'data' => $_POST 
						)	;
				
				} else {
					$c->stop_socket($_POST);
						
					
					$result=  array (
						'retCode' => '00',
						'retMsg' => 'socket server killed ',
						'result' => true,
						'data' => $_POST 
						)	;	
					
					
					}
						
		header ( "Access-Control-Allow-Origin: *" );
		header ( 'Content-Type: application/json' );
		echo json_encode ( $result );
		die ();
					
			}	
			
			
	
	}	
	public function listapiAction() {
		
		// $mm = new Model_Prabasystem();
		// Zend_Debug::dump($mm->get_models_class()); die();
		// $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/data-tables/DT_bootstrap.css' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css' );
		
		// $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/data-tables/jquery.dataTables.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/data-tables/DT_bootstrap.js' );
		// $this->view->headScript()->appendFile('/assets/core/scripts/table-ajax.js');
		
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js' );
		// $this->view->headScript()->appendFile('/assets/core/scripts/ui-extended-modals.js');
		
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css' );
		// $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
		
		$this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
		
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js' );
		
		// $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
		// $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
		// $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
		// $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
		
		try {
			$authAdapter = Zend_Auth::getInstance ();
			$identity = $authAdapter->getIdentity ();
		} catch ( Exception $e ) {
		}
		// Zend_Debug::dump($identity);die();
		$sys = new Model_Prabasystem ();
		$page = new Model_Zprabapage ();
		
		$userapp = $page->list_users_api ();
		// Zend_Debug::dump($userapp); die();
		$params = $sys->get_params ( 'api_type' );
		$modes = $sys->get_params ( 'api_mode' );
		// Zend_Debug::dump($modes); die();
		$conns = $sys->get_conn ();
		$this->view->modes = $modes;
		$this->view->params = $params;
		$this->view->conns = $conns;
		$this->view->userapp = $userapp;
	}
	
	public function socketAction() {
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery.numeric.js' );
		
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-datepicker/css/datepicker.css' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css' );
		// <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/select2/select2_metro.css' );
		
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css' );
		// $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js' );
		// $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
		// $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js' );
		// $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
		$this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/select2/select2.min.js' );
		
		$params = $this->getRequest ()->getParams ();
		
		$mdl_sys = new Model_Prabasystem ();
		$data = $mdl_sys->get_a_api ( $params ['id'] );
		
		//Zend_Debug::dump($data); 
		
		$this->view->varams =  $params;
		$this->view->data =  $data;
		
	}
	
	public function listapiajaxAction() {
		try {
			$authAdapter = Zend_Auth::getInstance ();
			$identity = $authAdapter->getIdentity ();
		} catch ( Exception $e ) {
		}
		
		$this->_helper->layout->disableLayout ();
		
		$mdl_admin = new Model_Prabasystem ();
		$countgraph = $mdl_admin->count_listapi ( $_GET );
		
		$params = $mdl_admin->get_params ( 'api_type' );
		
		foreach ( $params as $v ) {
			$vparams [$v ['value_param']] = $v ['display_param'];
		}
		
		$listgraph = $mdl_admin->listapi ( $_GET );
		// Zend_Debug::dump($countgraph);die();
		$iTotalRecords = intval ( $countgraph );
		$iDisplayLength = intval ( $_GET ['iDisplayLength'] );
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart = intval ( $_GET ['iDisplayStart'] );
		$sEcho = intval ( $_GET ['sEcho'] );
		
		$records = array ();
		$records ["aaData"] = array ();
		
		$end = $iDisplayStart + $iDisplayLength;
		
		foreach ( $listgraph as $k => $v ) {
				if($v['is_running_socket']!=1) {
						$run = '<span class="label label-sm label-danger">
													socket server not running
												</span>';
				}elseif($v['is_running_socket']==1) {
						$run = '<span class="label label-sm label-success">
													socket server  running
												</span>';
					}
			
			$records ["aaData"] [] = array (
					$iDisplayStart + $k + 1,
					$v ['id'],
					$run,
					$v ['conn_name'],
					$v ['sql_text'],
					$v ['api_desc'],
					$vparams [$v ['api_type']],
					
					$v ['attrs1'],
					$v ['cache_time'],
					'<div style="text-align:center"> '.
					'<div style="text-align:center"><a href="/prabaconstruct/socket/id/' . $v ['id'] . '" data-id="' . $v ['id'] . '" class="btn btn-xs blue btn-edit"><i class="fa fa-search"></i> Update Socket </a>' . '</div>' 
			);
		}
		
		$records ["sEcho"] = $sEcho;
		$records ["iTotalRecords"] = $iTotalRecords;
		$records ["iTotalDisplayRecords"] = $iTotalRecords;
		
		/*
		 * $gg = new Escort_Model_Kpu(); $res = $gg->get_datakpu($data2['witel']); //var_dump($res);die();
		 */
		header ( "Access-Control-Allow-Origin: *" );
		header ( 'Content-Type: application/json' );
		echo json_encode ( $records );
		die ();
	}
	

	
}
