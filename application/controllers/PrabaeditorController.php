<?php


/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class PrabaeditorController extends Zend_Controller_Action {

    public function init() {
    }

    public function treefolderAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/fuelux/js/tree.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $cc = new Model_Zprabagensys();
        $cc->get_nested_model();
        //$this->view->headScript ()->appendFile ( '/assets/core/js/d3.v3.min.js' );
    }

    public function frontAction() {
        $this->view->headScript()->appendFile('/assets/core/js/d3.v3.min.js');
    }

    public function frontajaxAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        $this->view->varams = $params;
    }

    public function addappAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        // <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $params = $this->getRequest()->getParams();
        $sys = new Model_Prabasystem();
        $page = new Model_Zprabapage();
        $userapp = $page->list_users_api();
        // Zend_Debug::dump($userapp); die();
        $params = $sys->get_params('api_type');
        $modes = $sys->get_params('api_mode');
        // Zend_Debug::dump($modes); die();
        $conns = $sys->get_conn();
        $this->view->modes = $modes;
        $this->view->params = $params;
        $this->view->conns = $conns;
        $this->view->userapp = $userapp;
    }

    public function addapiAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        // <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $params = $this->getRequest()->getParams();
        $sys = new Model_Prabasystem();
        $page = new Model_Zprabapage();
        $userapp = $page->list_users_api();
        // Zend_Debug::dump($userapp); die();
        $params = $sys->get_params('api_type');
        $modes = $sys->get_params('api_mode');
        // Zend_Debug::dump($modes); die();
        $conns = $sys->get_conn();
        $this->view->modes = $modes;
        $this->view->params = $params;
        $this->view->conns = $conns;
        $this->view->userapp = $userapp;
    }

    public function websocketAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        $dd = new Model_Prabasystemaddon();
        if(isset($params['act'])) {
            $data = $m2->get_portlet($params['id']);
            // Zend_Debug::dump($data); die();
        }
        $apis = $dd->get_api_grouping();
        if(!isset($params['row'])) {
            $params['row'] = 1;
        }
        // Zend_Debug::dump($apis); die();
        $this->view->lay = $m->get_layout();
        $this->view->apis = $apis;
        $this->view->data = $data;
        $this->view->apps = $m->get_apps();
        $this->view->ws = $m->get_workspace();
        $this->view->varams = $params;
        $this->view->classicon = $m->get_params('classicon');
        $this->view->area = $m->get_params('formarea');
    }

    public function builderhtmlAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        $dd = new Model_Prabasystemaddon();
        if(isset($params['act'])) {
            $data = $m2->get_portlet($params['id']);
            // Zend_Debug::dump($data); die();
        }
        $apis = $dd->get_api_grouping();
        if(!isset($params['row'])) {
            $params['row'] = 1;
        }
        // Zend_Debug::dump($apis); die();
        $this->view->lay = $m->get_layout();
        $this->view->apis = $apis;
        $this->view->data = $data;
        $this->view->apps = $m->get_apps();
        $this->view->ws = $m->get_workspace();
        $this->view->varams = $params;
        $this->view->classicon = $m->get_params('classicon');
        $this->view->area = $m->get_params('formarea');
    }

    public function buildergraphicAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        $dd = new Model_Prabasystemaddon();
        $apis = $dd->get_api_grouping();
        if(!isset($params['row'])) {
            $params['row'] = 1;
        }
        if(isset($params['act'])) {
            $data = $m2->get_portlet($params['id']);
            // Zend_Debug::dump($data); die();
        }
        // Zend_Debug::dump($apis); die();
        $this->view->lay = $m->get_layout();
        $this->view->data = $data;
        $this->view->apis = $apis;
        $this->view->apps = $m->get_apps();
        $this->view->ws = $m->get_workspace();
        $this->view->varams = $params;
        // $this->view->classicon = $m->get_params('classicon');
        // $this->view->area = $m->get_params('formarea');
    }

    public function buildergraphicplusAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-colorpicker/css/colorpicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/fuelux/js/spinner.min.js');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        $dd = new Model_Prabasystemaddon();
        $apis = $dd->get_api_grouping();
        $par = new Model_Zparams();
        if(!isset($params['row'])) {
            $params['row'] = 1;
        }
        if(isset($params['act'])) {
            $data = $m2->get_portlet($params['id']);
            // Zend_Debug::dump($data); die();
        }
        // Zend_Debug::dump($apis); die();
        $this->view->lay = $m->get_layout();
        $this->view->data = $data;
        $this->view->apis = $apis;
        $this->view->apps = $m->get_apps();
        $this->view->ws = $m->get_workspace();
        $this->view->varams = $params;
        $this->view->chartType = $par->get_chart_type();
        $this->view->chartTheme = $par->get_chart_theme();
        // $this->view->classicon = $m->get_params('classicon');
        // $this->view->area = $m->get_params('formarea');
    }

    public function builderformAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        $dd = new Model_Prabasystemaddon();
        $apis = $dd->get_api_grouping();
        if(!isset($params['row'])) {
            $params['row'] = 1;
        }
        $data = array();
        if(isset($params['act'])) {
            $data = $m2->get_portlet($params['id']);
        }
        $this->view->lay = $m->get_layout();
        $this->view->data = $data;
        $this->view->apis = $apis;
        $this->view->apps = $m->get_apps();
        $this->view->ws = $m->get_workspace();
        $this->view->varams = $params;
        $this->view->classicon = $m->get_params('classicon');
        $this->view->area = $m->get_params('formarea');
    }

    public function buildertableAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        // $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/table-ajax.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/ui-extended-modals.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        $dd = new Model_Prabasystemaddon();
        $apis = $dd->get_api_grouping();
        $this->view->apis = $apis;
        $data = array();
        if(isset($params['act'])) {
            $data = $m2->get_portlet($params['id']);
        }
        // Zend_Debug::dump($data); die();
        $apis = $m2->get_all_api();
        $this->view->data = $data;
        $this->view->apps = $m->get_apps();
        $this->view->ws = $m->get_workspace();
        // Zend_Debug::dump($m->get_apps());die();
        $this->view->varams = $params;
    }

    public function listappAction() {
        $mm = new Model_Zprabapage();
        $cc = new Model_System();
        $apps = $cc->get_app_modules();
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/table-ajax.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/ui-extended-modals.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $sys = new Model_Prabasystem();
        $datacon = $sys->get_applications();

        foreach($datacon as $k => $v) {
            if(in_array($v['s_app'], $apps)) {
                $datacon2[$k] = $v;
            }
        }
        $cc = new Model_System();
        $dd = new Model_Zparams();
        $themes = $cc->get_themes();
        $lays = $dd->get_layout();
        $skins = $dd->get_skins();
        $this->view->lays = $lays;
        $this->view->themes = $themes;
        $this->view->skins = $skins;
        //Zend_Debug::dump($datacon2); die();
        $this->view->conns = $datacon;
    }

    public function saveformAction() {
        $xmlData = simplexml_load_string($_POST['data']);
        $config = new Zend_Config_Xml($_POST['data']);
    }

    public function addtablection() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        // <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        $params = $this->getRequest()->getParams();
    }

    public function listconnAction() {
        $mm = new Model_Zprabapage();
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/table-ajax.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/ui-extended-modals.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $sys = new Model_Prabasystem();
        $pa = new Model_Zparams();
        $this->view->adapts = $pa->get_adapters();
        // Zend_Debug::dump($pa->get_adapters()); //die();
        $datacon = $sys->get_conns_serialize();
        // Zend_Debug::dump($datacon); die();
        $this->view->conns = $datacon;
    }

    public function builderAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/builder/css/rbl_forms.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/builder/css/ui-lightness/jquery-ui-1.7.2.custom.css');
        $this->view->headScript()->appendFile('/assets/core/builder/js/rbl_base.js');
        $this->view->headScript()->appendFile('/assets/core/builder/js/rbl_form.js');
        $this->view->headScript()->appendFile('/assets/core/builder/js/rbl_col.js');
        $this->view->headScript()->appendFile('/assets/core/builder/js/rbl_elt.js');
        // $this->view->headScript()->appendFile('/assets/core/builder/js/rbl_build.js');
        $this->view->headScript()->appendFile('/assets/core/builder/js/rbl_base.js');
        // $this->view->headScript()->appendFile('/assets/core/builder/js/rbl_base.js');
        $this->view->headScript()->appendFile('/assets/core/builder/js/lib/chili/jquery.chili-2.2.js');
    }

    public function loginappAction() {
        $params = $this->getRequest()->getParams();
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $cc = new Model_System();
        $dd = new Model_Zparams();
        $themes = $dd->get_login_theme();
        $lays = $dd->get_login_position();
        $mdl_sys = new Model_Prabasystem();
        $data = $mdl_sys->get_a_app($params['id']);
        //Zend_Debug::dump($data); die();
        $this->view->params = $params;
        $this->view->lays = $lays;
        $this->view->data = $data;
        $this->view->themes = $themes;
    }

    public function editappAction() {
        $params = $this->getRequest()->getParams();
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $cc = new Model_System();
        $dd = new Model_Zparams();
        $themes = $cc->get_themes();
        $lays = $dd->get_layout();
        $skins = $dd->get_skins();
        $mdl_sys = new Model_Prabasystem();
        $data = $mdl_sys->get_a_app($params['id']);
        // Zend_Debug::dump($data); die();
        $this->view->params = $params;
        $this->view->lays = $lays;
        $this->view->data = $data;
        $this->view->skins = $skins;
        $this->view->themes = $themes;
    }

    public function editconnAction() {
        $params = $this->getRequest()->getParams();
        if(!isset($params['id'])|| $params['id'] == '') {
            $this->_redirect('/prabaeditor/listapi');
        }
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $mdl_sys = new Model_Prabasystem();
        $data = $mdl_sys->get_a_conn($params['id']);
        $page = new Model_Zprabapage();
        $conns = $mdl_sys->get_conn();
        $pa = new Model_Zparams();
        $this->view->adapts = $pa->get_adapters();
        $this->view->modes = $modes;
        $this->view->params = $params;
        $this->view->conns = $conns;
        $this->view->data = $data;
    }

    public function getUrlAction() {
        //Zend_Debug::dump($_SERVER);
        $params = $this->getRequest()->getParams();
        $this->view->varams = $params;
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $dd = new Model_Zprabapage();
        $this->view->listu = $dd->list_users_api();
    }

    public function editapiuserAction() {
        $params = $this->getRequest()->getParams();
        if(!isset($params['id'])|| $params['id'] == '') {
            $this->_redirect('/prabaeditor/listapi');
        }
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $mdl_sys = new Model_Prabasystem();
        $data = $mdl_sys->get_a_api_user($params['id']);
        $page = new Model_Zprabapage();
        // Zend_Debug::dump($data); die();
        $this->view->data = $data;
    }

    public function editapiAction() {
        $params = $this->getRequest()->getParams();
        if(!isset($params['id'])|| $params['id'] == '') {
            $this->_redirect('/prabaeditor/listapi');
        }
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $mdl_sys = new Model_Prabasystem();
        $data = $mdl_sys->get_a_api($params['id']);
        $page = new Model_Zprabapage();
        $userapp = $page->list_users_api();
        // Zend_Debug::dump($data); die();
        $modes = $mdl_sys->get_params('api_mode');
        $params = $mdl_sys->get_params('api_type');
        $conns = $mdl_sys->get_conn();
        $this->view->modes = $modes;
        $this->view->params = $params;
        $this->view->conns = $conns;
        $this->view->data = $data;
        $this->view->userapp = $userapp;
    }

    public function listapiuserAction() {
        // $mm = new Model_Prabasystem();
        // Zend_Debug::dump($mm->get_models_class()); die();
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        // $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/table-ajax.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/ui-extended-modals.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $sys = new Model_Prabasystem();
        $page = new Model_Zprabapage();
        $userapp = $page->list_users_api();
        // Zend_Debug::dump($userapp); die();
        $params = $sys->get_params('api_type');
        $modes = $sys->get_params('api_mode');
        // Zend_Debug::dump($modes); die();
        $conns = $sys->get_conn();
        $this->view->modes = $modes;
        $this->view->params = $params;
        $this->view->conns = $conns;
        $this->view->userapp = $userapp;
    }

    public function listapiAction() {
        // $mm = new Model_Prabasystem();
        // Zend_Debug::dump($mm->get_models_class()); die();
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        // $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/table-ajax.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/ui-extended-modals.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $sys = new Model_Prabasystem();
        $page = new Model_Zprabapage();
        $userapp = $page->list_users_api();
        // Zend_Debug::dump($userapp); die();
        $params = $sys->get_params('api_type');
        $modes = $sys->get_params('api_mode');
        // Zend_Debug::dump($modes); die();
        $conns = $sys->get_conn();
        $this->view->modes = $modes;
        $this->view->params = $params;
        $this->view->conns = $conns;
        $this->view->userapp = $userapp;
    }

    public function editpageAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        // <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        $data = $m2->get_page($params['id']);
        $data['id'] = $params['id'];
        //Zend_Debug::dump($data);die("ss");
        $arel = unserialize($data['element']);
        if(!isset($params['row'])&& count($arel)> 1) {
            $params['row'] = count($arel);
        } elseif(!isset($params['row'])) {
            $params['row'] = 1;
        }
        $this->view->apps = $m->get_apps();
        $this->view->ws = $m->get_workspace();
        $this->view->lay = $m->get_layout();
        // Zend_Debug::dump($m->get_layout());die();
        $this->view->varams = $params;
        $this->view->data = $data;
    }

    public function editactionAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        // <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $this->view->data = $m->get_a_action($params['id']);
    }

    public function socketAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        // <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $params = $this->getRequest()->getParams();
        $mdl_sys = new Model_Prabasystem();
        $data = $mdl_sys->get_a_api($params['id']);
        //Zend_Debug::dump($data); 
        $this->view->varams = $params;
        $this->view->data = $data;
    }

    public function addactionAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        // <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        // Zend_Debug::dump($m2->list_page());die();
        if(!isset($params['row'])) {
            $params['row'] = 1;
        }
        $this->view->lay = $m->get_layout();
    }

    public function addpageAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        // <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        // Zend_Debug::dump($m2->list_page());die();
        if(!isset($params['row'])) {
            $params['row'] = 1;
        }
        $this->view->lay = $m->get_layout();
        $this->view->apps = $m->get_apps();
        $this->view->ws = $m->get_workspace();
        // Zend_Debug::dump($m->get_apps());die();
        $this->view->varams = $params;
    }

    public function listpageAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/table-ajax.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/ui-extended-modals.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $sys = new Model_Zprabapage();
        $mdl = new Model_Prabasystem();
        $params = $sys->list_page();
        // Zend_Debug::dump($params);die();
        $apps = $mdl->get_apps();
        $this->view->params = $data;
        $this->view->apps = $apps;
    }

    public function listactionAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/table-ajax.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/ui-extended-modals.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $sys = new Model_Zprabapage();
        $mdl = new Model_Prabasystem();
        $params = $sys->list_action();
        // Zend_Debug::dump($params);die();
        //$apps = $mdl->get_apps ();
        $this->view->params = $data;
        //$this->view->apps = $apps;
    }

    public function listportletAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/table-ajax.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/ui-extended-modals.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $sys = new Model_Zprabapage();
        $mdl = new Model_Prabasystem();
        $params = $sys->list_page();
        // Zend_Debug::dump($params);die();
        $apps = $mdl->get_apps();
        $this->view->params = $data;
        $this->view->apps = $apps;
    }

    public function addportletAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        // <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $params = $this->getRequest()->getParams();
        $dd = new Model_Prabasystemaddon();
        $apis = $dd->get_api_grouping();
        $this->view->apis = $apis;
    }

    public function editportletAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        // <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $dd = new Model_Prabasystemaddon();
        $apis = $dd->get_api_grouping();
        $params = $this->getRequest()->getParams();
        $cd = new Model_Zprabapage();
        $data = $cd->get_portlet($params['id']);
        $this->view->data = $data;
        $dd = new Model_Prabasystemaddon();
        $apis = $dd->get_api_grouping();
        $this->view->apis = $apis;
        // Zend_Debug::dump($data);
        // die();
    }

    public function buildactAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/maestro/css/maestro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/maestro/css/maestro_ctools.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/maestro/js/wz_jsgraphics.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/maestro/js/admin_template_editor.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/maestro/js/jquery.contextmenu.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/maestro/js/jquery.simplemodal.min.js');
    }

    public function inboxAction(){
        $this->view->headLink()->appendStylesheet('/assets/core/apps/css/inbox.min.css');
    }
    public function calendarAction(){
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/fullcalendar/fullcalendar.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/moment.min.js');        
        $this->view->headScript()->appendFile('/assets/core/global/plugins/fullcalendar/fullcalendar.min.js');        
        $this->view->headScript()->appendFile('/assets/core/apps/scripts/calendar.min.js');        
    }

    public function nifiAction(){
        
    }
}
