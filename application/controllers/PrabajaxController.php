<?php

/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class PrabajaxController extends Zend_Controller_Action {

    public function init() {
        
    }
    public function freezeAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {
           // Zend_Debug::dump($_POST);die(); 
            $mdl = new Model_Action ();
            $update = $mdl->freeze_unfreeze($_POST['uid']);

            if ($update ['result'] === true) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $update ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $result = array(
                    'retCode' => '11',
                    'retMsg' => $update ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }
    private function cache() {
        $front = array(
            'lifetime' => null,
            'automatic_serialization' => true
        );
        $back = array(
            'cache_dir' => APPLICATION_PATH . '/../cache2/'
        );
        return Zend_Cache::factory('Core', 'File', $front, $back);
    }
	public function savethemeAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        if ($_POST) {
           // Zend_Debug::dump($_POST); die();
			$params = $this->getRequest()->getParams();
            parse_str($_POST['from'], $data);  
            //Zend_Debug::dump($data);die(); 
            $dom = new Zend_Dom_Query($_POST['content']);

            $result = $dom->query('#set-theme');

            try {  
            $theme = $result->current()->getAttribute('data-theme');
       
            } catch (Exception $e) {
                Zend_Debug::dump($e->getMessage());die();

            }
                
            switch ($theme) {
                case 1  :
                     $data['style'] = $dom->query("ul .current")->current()->getAttribute('data-style');

                break;
                case 2  :
                    $data['style'] = $dom->query("ul .current")->current()->getAttribute('data-style');
                   
                break;
                case 3  :
                    $data['style'] = $dom->query("ul .active")->current()->getAttribute('data-theme');
                break;
                case 4  :
                    $data['style'] = $dom->query("ul .active")->current()->getAttribute('data-theme'); 
                break;



            }
    # Zend_Debug::dump($data);die();
            //$res['content'] = $dom_1->query('.detail_text')->current()->textContent;

			#Zend_Debug::dump($data); die();   
			$key = array($theme=>$data);
            //$keys=  serialize($key);
            $this->cache()->save($key, "themeuser_" . $identity->uid."_".$_POST['app'], array(
                    'user_theme_' . $identity->uid));
			$result = array(
                    'retCode' => '00',
                    'retMsg' => 'Theme Settings saved',
                    'result' => true,
                    'data' => $_POST);
            header("Access-Control-Allow-Origin: *");
            header('Content-Type: application/json');
            echo json_encode($result);
            die();
        }
    }
    public function updatesocketAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();


        //$dd = new Model_Socket ();
        //$apis = $dd->get_api_grouping ();
        if ($_POST) {
            $c = new Model_Socket();
            //Zend_Debug::dump($_POST);die();	
            if (isset($_POST['running']) && $_POST['running'] == 1) {

                $c->running_socket($_POST);


                $result = array(
                    'retCode' => '00',
                    'retMsg' => 'socket server activated',
                    'result' => true,
                    'data' => $_POST
                );
            } else {


                $c->stop_socket($_POST);


                $result = array(
                    'retCode' => '00',
                    'retMsg' => 'socket server killed ',
                    'result' => true,
                    'data' => $_POST
                );
            }

            header("Access-Control-Allow-Origin: *");
            header('Content-Type: application/json');
            echo json_encode($result);
            die();
        }
    }

    public function postwizardAction() {

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $dd = new Model_Prabasystemaddon ();
        $apis = $dd->get_api_grouping();
        //Zend_Debug::dump($apis); die();

        if ($_POST) {
            //Zend_Debug::dump($_POST); die();

            $mdl = new Model_Prabasystem ();
            //	 Zend_Debug::dump($_POST); //die();

            $br = array();
            for ($i = 0; $i < count($_POST ['brname']); $i ++) {
                $br [$i] ['name'] = $_POST ['brname'] [$i];
                $br [$i] ['href'] = $_POST ['href'] [$i];
                $br [$i] ['class'] = $_POST ['brclass'] [$i];
            }

            $el = array();
            $k = 0;
            for ($i = 1; $i <= count($_POST ['col1']); $i ++) {

                if ($_POST ['col1'] [$k] != "") {

                    if (($_POST ['col1'] [$k] != "newgraphic" && $_POST ['col1'] [$k] != "newtable")) {
                        $el [$i] [1] = $_POST ['col1'] [$k];
                    } elseif ($_POST ['col1'] [$k] == "newgraphic") {
                        //	 Zend_Debug::dump($_POST['col1'] [$k]); die("sssssssss");
                        $gr[] [$i] [1] = $_POST ['col1'] [$k];
                    } else if ($_POST ['col1'] [$k] == "newtable") {
                        $tb[] [$i] [1] = $_POST ['col1'] [$k];
                    }
                }
                if ($_POST ['col2'] [$k] != "") {
                    if ($_POST ['col2'] [$k] != "newgraphic" && $_POST ['col2'] [$k] != "newtable") {
                        $el [$i] [2] = $_POST ['col2'] [$k];
                    } elseif ($_POST ['col2'] [$k] == "newgraphic") {
                        $gr[] [$i] [2] = $_POST ['col2'] [$k];
                    } else if ($_POST ['col2'] [$k] == "newtable") {
                        $tb[] [$i] [2] = $_POST ['col2'] [$k];
                    }
                }

                if ($_POST ['col3'] [$k] != "") {
                    if ($_POST ['col3'] [$k] != "newgraphic" && $_POST ['col3'] [$k] != "newtable") {
                        $el [$i] [3] = $_POST ['col3'] [$k];
                    } else if ($_POST ['col3'] [$k] == "newgraphic") {
                        $gr[] [$i] [3] = $_POST ['col3'] [$k];
                    } else if ($_POST ['col3'] [$k] == "newtable") {
                        $tb[] [$i] [3] = $_POST ['col3'] [$k];
                    }
                }
                if ($_POST ['col4'] [$k] != "") {
                    if ($_POST ['col4'] [$k] != "newgraphic" && $_POST ['col4'] [$k] != "newtable") {
                        $el [$i] [4] = $_POST ['col4'] [$k];
                    } else if ($_POST ['col4'] [$k] == "newgraphic") {
                        $gr[] [$i] [4] = $_POST ['col4'] [$k];
                    } else if ($_POST ['col4'] [$k] == "newtable") {
                        $tb[] [$i] [4] = $_POST ['col4'] [$k];
                    }
                }
                if ($_POST ['col5'] [$k] != "") {
                    if ($_POST ['col5'] [$k] != "newgraphic" && $_POST ['col5'] [$k] != "newtable") {
                        $el [$i] [5] = $_POST ['col5'] [$k];
                    } elseif ($_POST ['col5'] [$k] == "newgraphic") {
                        $gr[] [$i] [5] = $_POST ['col5'] [$k];
                    } else if ($_POST ['col5'] [$k] == "newtable") {
                        $tb[] [$i] [5] = $_POST ['col5'] [$k];
                    }
                }
                if ($_POST ['col6'] [$k] != "") {
                    if ($_POST ['col6'] [$k] != "newgraphic" && $_POST ['col6'] [$k] != "newtable") {
                        $el [$i] [6] = $_POST ['col6'] [$k];
                    } else if ($_POST ['col6'] [$k] == "newgraphic") {
                        $gr[] [$i] [6] = $_POST ['col6'] [$k];
                    } else if ($_POST ['col6'] [$k] == "newgraphic") {
                        $tb[] [$i] [6] = $_POST ['col6'] [$k];
                    }
                }
                $k ++;
            }

            foreach ($_POST ["ztype"] as $k => $v) {
                if ($v == 'graph') {
                    $res = $mdl->add_portlet_graph($k, $_POST);
                    $kgrap[] = $res['id'];
                }
                if ($v == 'table') {
                    $res = $mdl->add_portlet_table($k, $_POST);
                    $ktab[] = $res['id'];
                }
            }

            if (count($gr) > 0) {
                foreach ($gr as $zk => $zv) {
                    foreach ($zv as $zkk => $zvv) {
                        foreach ($zvv as $zkkk => $zvvv) {
                            $el[$zkk][$zkkk] = $kgrap[$zk];
                        }
                    }
                }
            }

            if (count($tb) > 0) {
                foreach ($tb as $zk => $zv) {
                    foreach ($zv as $zkk => $zvv) {
                        foreach ($zvv as $zkkk => $zvvv) {
                            $el[$zkk][$zkkk] = $ktab[$zk];
                        }
                    }
                }
            }

            //Zend_Debug::dump($el); die();

            $_POST['title'] = $_POST['titlepage'];
            $update = $mdl->add_page($_POST, $br, $el);

            die();
        }
    }

    public function postportletAction() {

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $dd = new Model_Prabasystemaddon ();
        $apis = $dd->get_api_grouping();
        //Zend_Debug::dump($apis); die();

        if ($_POST) {
            //Zend_Debug::dump($_POST);die();
            $grap = 0;
            $table = 0;
            foreach ($_POST as $k => $v) {
                if (strpos($k, 'col') !== FALSE) {
                    foreach ($v as $vv) {
                        if ($vv == 'newgraphic') {
                            $grap++;
                        }
                        if ($vv == 'newtable') {
                            $table++;
                        }
                    }
                }
            }
            $z = 1;
            for ($i = 1; $i <= $table; $i++) {
                echo '
			<input type="hidden" name="ztype[' . $z . ']" value="table"/>
<div class="col-md-6" >
<div class="portlet box grey" style="margin:0px">
	<div class="portlet-title">
		<div class="caption">Add Table</div>
		<div class="actions"></div>
		<div class="tools"></div>
	</div>
<div class="portlet-body" style="padding:15px">
			<div class="row" style="padding:0px 15px;">
									
									<div class="col-md-5">
										<div class="form-group">
											<label for="pagealias">Portlet Name</label>
											<div class="input-icon right">
										<i class="fa fa-exclamation tooltips" data-original-title="isikan dengan nama" data-container="body"></i>
											<input type="text" name="name[' . $z . ']" class="form-control" placeholder="Portlet Name" value="">
											</div>
											<span class="help-block" style="display: none;">
												<span class="label label-danger"><i class="fa fa-warning"> </i>This is inline help</span>
											</span>
										</div>
									</div>
									<div class="col-md-offset-1 col-md-5">
										<div class="form-group">
											<label for="title">Title</label>
											<div class="input-icon right">
										<i class="fa fa-exclamation tooltips" data-original-title="isikan dengan judul" data-container="body"></i>
											<input type="text" name="title[' . $z . ']" id="title" class="form-control" placeholder="Title" value="">
											</div>
											<span class="help-block" style="display: none;">
												<span class="label label-danger"><i class="fa fa-warning"> </i>This is inline help</span>
											</span>
										</div>
									</div>
				</div>
							
								
<div class="row" style="padding:0px 15px;">
		<div class="col-md-10">
					<div class="form-group">
						<label for="title">Color</label>
	

									<select class="form-control input-sm" name="color[' . $z . ']" placeholder="Color">
										<option value="">--Choose color--</option>
										<option value="red">Red</option>
										<option value="blue">Blue</option>
										<option value="green">Green</option>
										<option value="purple">Purple</option>
										<option value="yellow">Yellow</option>
										<option value="grey">Grey</option>
									</select>
									
			</div>							
									
		</div>
</div>								
								
<div class="row" style="padding:0px 5px;">
				<div class="col-md-12">							
								<div class="col-md-5">
									<div class="form-group">
										<label for="title">Type</label>
										<select class="form-control input-sm" name="type[' . $z . ']" placeholder="Type">
										<option value="">--Choose type--</option>
										<option value="">empty</option>
										<option value="box">Box</option>
										<option value="solid">Solid</option>
									
										</select>
									</div>
								</div>
								<div class="col-md-5 col-md-offset-1">
									<div class="form-group">
									<label for="title">Width</label>
										<select class="form-control input-sm" name="width[' . $z . ']" placeholder="Width">
										<option value="">--Choose width--</option>
										
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="6">6</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option value="12">12</option>
										
										</select>
									</div>	
								</div>				

		</div>
</div>

<div class="row" style="padding:0px 15px;">
									<div class="col-md-5">
										<div class="form-group">
											<label>Get Api</label>';
                echo '<select  name="get_api[' . $z . '][]"  value="' . $vv['id'] . '" class="get_api form-control" placeholder="Get Api">';
                foreach ($apis as $k => $v) {
                    echo "<optgroup label=" . $k . ">";
                    foreach ($v as $vv) {
                        echo '<option  value="' . $vv['id'] . '" >' . $vv['id'] . "-" . $vv['text'] . '</option>';
                    }
                    echo "</optgroup>";
                }
                echo "</select>";

                echo '</div>	
									</div>
									<div class="col-md-offset-1 col-md-5">
										<div class="form-group">
											<label for="header">Header Table</label>
											<input type="text" name="header[' . $z . ']"  class="form-control" placeholder="ex: Header-1|Header-2" value="">
											<span class="help-block" style="display: none;">
												<span class="label label-danger"><i class="fa fa-warning"> </i>This is inline help</span>
											</span>
										</div>
									</div>
				</div>

					</div>
					</div>
					</div>';
                $z++;
            }


            for ($i = 1; $i <= $grap; $i++) {

                echo '
<input type="hidden" name="ztype[' . $z . ']" value="graph"/>
		
<div class="col-md-6" >
<div class="portlet box grey" style="margin:0px">
	<div class="portlet-title">
		<div class="caption">Add Graphic</div>
		<div class="actions"></div>
		<div class="tools"></div>
	</div>
<div class="portlet-body" style="padding:15px">
			<div class="row" style="padding:0px 15px;">
									
									<div class="col-md-5">
										<div class="form-group">
											<label for="pagealias">Portlet Name</label>
											<div class="input-icon right">
										<i class="fa fa-exclamation tooltips" data-original-title="isikan dengan nama" data-container="body"></i>
											<input type="text" name="name[' . $z . ']" class="form-control" placeholder="Portlet Name" value="">
											</div>
											<span class="help-block" style="display: none;">
												<span class="label label-danger"><i class="fa fa-warning"> </i>This is inline help</span>
											</span>
										</div>
									</div>
									<div class="col-md-offset-1 col-md-5">
										<div class="form-group">
											<label for="title">Title</label>
											<div class="input-icon right">
										<i class="fa fa-exclamation tooltips" data-original-title="isikan dengan judul" data-container="body"></i>
											<input type="text" name="title[' . $z . ']" id="title" class="form-control" placeholder="Title" value="">
											</div>
											<span class="help-block" style="display: none;">
												<span class="label label-danger"><i class="fa fa-warning"> </i>This is inline help</span>
											</span>
										</div>
									</div>
				</div>
							
								
<div class="row" style="padding:0px 15px;">
		<div class="col-md-10">
					<div class="form-group">
						<label for="title">Color</label>
	

									<select class="form-control input-sm" name="color[' . $z . ']" placeholder="Color">
										<option value="">--Choose color--</option>
										<option value="red">Red</option>
										<option value="blue">Blue</option>
										<option value="green">Green</option>
										<option value="purple">Purple</option>
										<option value="yellow">Yellow</option>
										<option value="grey">Grey</option>
									</select>
									
			</div>							
									
		</div>
</div>								
								
<div class="row" style="padding:0px 5px;">
				<div class="col-md-12">							
								<div class="col-md-5">
									<div class="form-group">
										<label for="title">Type</label>
										<select class="form-control input-sm" name="type[' . $z . ']" placeholder="Type">
										<option value="">--Choose type--</option>
										<option value="">empty</option>
										<option value="box">Box</option>
										<option value="solid">Solid</option>
									
										</select>
									</div>
								</div>
								<div class="col-md-5 col-md-offset-1">
									<div class="form-group">
									<label for="title">Width</label>
										<select class="form-control input-sm" name="width[' . $z . ']" placeholder="Width">
										<option value="">--Choose width--</option>
										
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="6">6</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option value="12">12</option>
										
										</select>
									</div>	
								</div>				

		</div>
</div>

<div class="row" style="padding:0px 15px;">
									<div class="col-md-5">
										<div class="form-group">
											<label for="get_api">Get Api</label>';


                echo '<select multiple  name="get_api[' . $z . '][]"  value="' . $vv['id'] . '" class="get_api form-control" placeholder="Get Api">';
                foreach ($apis as $k => $v) {
                    echo "<optgroup label=" . $k . ">";
                    foreach ($v as $vv) {
                        echo '<option  value="' . $vv['id'] . '" >' . $vv['id'] . "-" . $vv['text'] . '</option>';
                    }
                    echo "</optgroup>";
                }
                echo '</select>
										</div>
									</div>
									<div class="col-md-offset-1 col-md-5">
										<div class="form-group">
											<label for="graph_type">Graphic Type</label>
											<select  name="graph_type[' . $z . ']" id="graph_type" class="form-control" placeholder="Graph Type" >
										
										<option value="">----------</option>
										<optgroup label="Bar Chart">
											<option  value="simplebar">Simple Bar Chart</option>
											<option  value="horizontalbar">Horizontal Bar Chart</option>
											<option  value="stackchart">Stack Chart</option>
										</optgroup>
										<optgroup label="Pie Chart">
											<option  value="simplepie">Simple Pie Chart</option>
											<option  value="pieplus">Pie Chart Plus</option>
											
										</optgroup>
										
										<optgroup label="Others">
											<option  value="interactive">Interactive Chart</option>
											
										</optgroup>
										
										</select>
												
											
										</div>
									</div>
								</div>
			
							<div class="row" style="padding:0px 15px;">
									<div class="col-md-5">
									
									</div>
									<div class="col-md-offset-1 col-md-5">
										<div class="form-group">
										<label for="graph_lib">Graphic Lib</label>
										<select id="graph_lib" name="graphlib[' . $z . ']" class="form-control" placeholder="GraphLib">
										<option value="">----</option>
										<option value="plot" >PlotChart</option>
										<option   value="am">AmChart</option>
										</select>
										
											
										</div>
									</div>
								</div>
								
														
			<div class="row" style="padding:0px 15px;">
				<div class="col-md-12">
					<div class="form-group">
						<label >Key&Value</label>
					</div>
					
				
					<div class="form-group  breadcrump' . $i . '" id="breadcrump' . $i . '1" data-id="1">
							<div class="col-md-10">
								<div class="col-md-3">
								<input class="form-control input-sm" name="key[' . $z . '][]" value="" placeholder="Key">
							
								</div>
								<div class="col-md-3">
								<input class="form-control input-sm" name="xvalue[' . $z . '][]" value="" placeholder="Value">
								</div>
								<div class="col-md-3">
								<input class="form-control input-sm" name="varx[' . $z . '][]" value="" placeholder="VarName">
								</div>
								<div class="col-md-3">
								<input class="form-control input-sm" name="api[' . $z . '][]" value="" placeholder="Api Id">
								</div>
							</div>
							<div class="col-md-2">
								<button type="button" class="btn grey addGC" onclick="addGC' . $i . '(this); return false;" data-act="addGC' . $i . '" data-id="1" style="padding: 5px;">Add</button>
							</div>
					</div>
				
				</div>
			</div>	
								
										
					</div>
					</div></div>';


                echo "<script>
	var gamasidx2" . $i . "=1;
	var elem=1;
	
	function addGC" . $i . "(me){
	//alert(me);
	var id = $(me).data('id');
	var currow = $('body').find('.breadcrump" . $i . ":last');";


                echo 'var str' . $i . ' = \'<div class="form-group row breadcrump' . $i . '" id="breadcrump' . $i . '\'+(parseInt(gamasidx2' . $i . ')+1)+\'" data-id="\'+(parseInt(gamasidx2' . $i . ')+1)+\'" >\'
					+\'<div class="col-md-10">\'
						+\'<div class="col-md-3">\'
						+\'<input class="form-control input-sm" name="key[' . $z . '][]" value="" placeholder="Key">\'
						+\'</div>\'
						+\'<div class="col-md-3">\'
							+\'<input class="form-control input-sm" name="xvalue[' . $z . '][]" value="" placeholder="Value">\'
						+\'</div>\'
						+\'<div class="col-md-3">\'
							+\'<input class="form-control input-sm" name="varx[' . $z . '][]" value="" placeholder="VarName">\'
						+\'</div>\'
						+\'<div class="col-md-3">\'
							+\'<input class="form-control input-sm" name="api[' . $z . '][]" value="" placeholder="Api Id">\'
						+\'</div>\'
					+\'</div>\'
					+\'<div class="col-md-2">\'
						+\'<button type="button" class="btn red removeGC' . $i . '" onclick="removeGC' . $i . '(this); return false;" data-act="removeGC' . $i . '" data-id="\'+(parseInt(gamasidx2' . $i . ')+1)+\'" style="padding: 5px;">Remove</button>\'
					+\'</div>\'
				+\'</div>\';
				
				gamasidx2++;
	
	currow.after(str' . $i . ')
	}

function removeGC' . $i . '(me){
	var id = $(me).data(\'id\');
	//console.log(id);
	$(\'body\').find(\'#breadcrump' . $i . '\'+id+\':first\').remove();
}
</script>
';

                $z++;
            }

            echo "<script>$('.get_api').select2();</script>";
        }

        die();
    }

    public function graphplusAction() {

        //$this->_helper->layout->disableLayout ();
        $this->view->headScript()->appendFile('/assets/core/plugins/flot/jquery.flot.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/flot/jquery.flot.resize.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/flot/jquery.flot.pie.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/flot/jquery.flot.stack.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/flot/jquery.flot.categories.min.js');


        $this->view->headScript()->appendFile('/assets/core/plugins/amcharts/amcharts/amcharts.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/amcharts/amcharts/serial.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/amcharts/amcharts/pie.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/amcharts/amcharts/radar.js');

        $this->view->headScript()->appendFile('/assets/core/plugins/amcharts/amcharts/themes/light.js');

        $this->view->headScript()->appendFile('/assets/core/plugins/amcharts/amcharts/themes/patterns.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/amcharts/ammap/ammap.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/amcharts/amstockcharts/amstock.js');


        $c = new Model_Zprabagraph();
        $params = $this->getRequest()->getParams();
        $data = $c->get_dump_graph($params['id']);
        //Zend_Debug::dump(unserialize($data['data_tmp'])); 
        //die();

        if ($data = "") {
            die("asdasdasd");
        }
    }

    public function chelpAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();

        $params = $this->getRequest()->getParams();
        //Zend_Debug::$params[]
        $c = new Model_Zparams();
        $dt = $c->get_params_by_param($params['ctl'], $params['act']);

        $result = array(
            'retCode' => '00',
            'retMsg' => $dt['description'],
            'result' => true,
            'data' => null
        );

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function tesactAction() {



        $dd = new Model_Zprabapage ();
        $exe = new Model_Zpraba4api ();
        $varC = $dd->get_api(152);
        // $varC['api_mode']=5;
        $conn = unserialize($varC ['conn_params']);
        $data = $exe->execute_api($conn, $varC, $ti);
        Zend_Debug::dump($data);

        die("ok");
//	$dd->	
    }

    public function teswsdl2Action() {

        try {
            $client = new SoapClient("http://10.40.9.79/embassy/xmlembassy/embassy.php?wsdl");
            $result = $client->ukur_clid(array("clid" => '1233'));
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }
        //$datax = simplexml_load_string($data);
        echo "<pre>";
        //$dataxx = ($this->xml2array($datax));
        var_dump($result);
        echo "</pre>";
        die();
    }

    public function teswsdlAction() {

        try {

            $c = new Zend_Soap_Client("http://www.webservicex.net/globalweather.asmx?WSDL");


            $data = $c->GetWeather(array('CityName' => 'JAKARTA', 'CountryName' => 'INDONESIA'));
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }
        $datax = simplexml_load_string($data);
        echo "<pre>";
        $dataxx = ($this->xml2array($datax));
        var_dump($data->GetWeatherResult);
        echo "</pre>";
        die();
    }

    function xml2array($xmlObject, $out = array()) {
        foreach ((array) $xmlObject as $index => $node)
            $out[$index] = ( is_object($node) ) ? $this->xml2array($node) : $node;

        return $out;
    }

    public function frontendAction() {
        $zz = new Model_Prabasystemaddon ();
        $data = $zz->get_all_front_end();

        //Zend_Debug::dump($data); die();

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );


        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($data);
        die();
    }

    public function frontend2Action() {
        $zz = new Model_Prabasystemaddon ();
        $params = $this->getRequest()->getParams();

        $data = $zz->get_all_front_end_app($params['id']);

        //Zend_Debug::dump($data); die();

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );


        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($data);
        die();
    }

    public function addactionAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {
            $mdl = new Model_Prabasystem ();
            // Zend_Debug::dump($_POST); die();
            // Zend_Debug::dump($el); die();
            $update = $mdl->add_action($_POST);

            if ($update) {

                $_POST['id'] = $update['id'];
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $update ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $result = array(
                    'retCode' => '11',
                    'retMsg' => $update ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function listaction2Action() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        $mdl_admin = new Model_Zprabapage ();
        $countgraph = $mdl_admin->count_listaction($_GET, $params['wf']);

        $listgraph = $mdl_admin->listaction_ajax($_GET, $params['wf']);
        // Zend_Debug::dump($listgraph);die();
        $iTotalRecords = intval($countgraph);
        $iDisplayLength = intval($_GET ['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_GET ['iDisplayStart']);
        $sEcho = intval($_GET ['sEcho']);

        $records = array();
        $records ["aaData"] = array();

        $end = $iDisplayStart + $iDisplayLength;

        //$mdl = new Model_Prabasystem ();
        //$apps = $mdl->get_apps ();
        if ($params['wf'] == 1) {

            foreach ($listgraph as $k => $v) {
                $records ["aaData"] [] = array(
                    $iDisplayStart + $k + 1,
                    $v ['id'],
                    $v ['action_name'],
                    '<div style="text-align:center"><a href="/prabadoc/launch/id/' . $v ['id'] . '" data-id="' . $v ['id'] . '" class="btn btn-xs blue btn-edit"><i class="fa fa-search"></i> Launch</a><a href="/prabadoc/builderform/id/' . $v ['id'] . '" data-id="' . $v ['id'] . '" class="btn btn-xs green btn-edit"><i class="fa fa-search"></i> Edit Content Type</a>' . '<a href="/prabawf/display/id/' . $v ['id'] . '?wf=1" data-id="' . $v ['id'] . '" class="btn btn-xs yellow btn-add"> <i class="fa fa-search"></i> View Action</a>' . '<a href="javascript:;" data-id="' . $v ['id'] . '"  class="btn btn-xs red btn-delete"><i class="fa fa-times"></i> Delete Api</a></div>'
                );
            }
        } else {

            foreach ($listgraph as $k => $v) {
                $records ["aaData"] [] = array(
                    $iDisplayStart + $k + 1,
                    $v ['id'],
                    $v ['action_name'],
                    '<div style="text-align:center"><a href="/prabadata/action/editaction/id/' . $v ['id'] . '" data-id="' . $v ['id'] . '" class="btn btn-xs green btn-edit"><i class="fa fa-search"></i> Edit Action</a>' . '<a href="/prabadata/action/display/id/' . $v ['id'] . '" data-id="' . $v ['id'] . '" class="btn btn-xs yellow btn-add"><i class="fa fa-search"></i> View Action</a>' . '<a href="javascript:;" data-id="' . $v ['id'] . '"  class="btn btn-xs red btn-delete"><i class="fa fa-times"></i> Delete Api</a><a href="javascript:;" data-id="' . $v ['id'] . '"  class="btn btn-xs blue btn-clone"><i class="fa fa-times"></i> Clone</a></div>'
                );
            }
        }


        $records ["sEcho"] = $sEcho;
        $records ["iTotalRecords"] = $iTotalRecords;
        $records ["iTotalDisplayRecords"] = $iTotalRecords;

        /*
         * $gg = new Escort_Model_Kpu(); $res = $gg->get_datakpu($data2['witel']); //var_dump($res);die();
         */
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }

    public function listactionAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        $mdl_admin = new Model_Zprabapage ();
        $countgraph = $mdl_admin->count_listaction($_GET, $params['wf']);

        $listgraph = $mdl_admin->listaction_ajax($_GET, $params['wf']);
        // Zend_Debug::dump($listgraph);die();
        $iTotalRecords = intval($countgraph);
        $iDisplayLength = intval($_GET ['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_GET ['iDisplayStart']);
        $sEcho = intval($_GET ['sEcho']);

        $records = array();
        $records ["aaData"] = array();

        $end = $iDisplayStart + $iDisplayLength;

        //$mdl = new Model_Prabasystem ();
        //$apps = $mdl->get_apps ();
        if ($params['wf'] == 1) {

            foreach ($listgraph as $k => $v) {
                $records ["aaData"] [] = array(
                    $iDisplayStart + $k + 1,
                    $v ['id'],
                    $v ['action_name'],
                    '<div style="text-align:center"><a href="/mms/prabadoc/launch/id/' . $v ['id'] . '" data-id="' . $v ['id'] . '" class="btn btn-xs blue btn-edit"><i class="fa fa-search"></i> Launch</a><a href="/mms/prabadoc/builderform/id/' . $v ['id'] . '" data-id="' . $v ['id'] . '" class="btn btn-xs green btn-edit"><i class="fa fa-search"></i> Edit Content Type</a>' . '<a href="/prabadata/action/display/id/' . $v ['id'] . '?wf=1" data-id="' . $v ['id'] . '" class="btn btn-xs yellow btn-add"> <i class="fa fa-search"></i> View Action</a>' . '<a href="javascript:;" data-id="' . $v ['id'] . '"  class="btn btn-xs red btn-delete"><i class="fa fa-times"></i> Delete Api</a></div>'
                );
            }
        } else {

            foreach ($listgraph as $k => $v) {
                $records ["aaData"] [] = array(
                    $iDisplayStart + $k + 1,
                    $v ['id'],
                    $v ['action_name'],
                    '<div style="text-align:center"><a href="/prabaeditor/editaction/id/' . $v ['id'] . '" data-id="' . $v ['id'] . '" class="btn btn-xs green btn-edit"><i class="fa fa-search"></i> Edit Action</a>' . '<a href="/prabawf/display/id/' . $v ['id'] . '" data-id="' . $v ['id'] . '" class="btn btn-xs yellow btn-add"><i class="fa fa-search"></i> View Action</a>' . '<a href="javascript:;" data-id="' . $v ['id'] . '"  class="btn btn-xs red btn-delete"><i class="fa fa-times"></i> Delete Api</a><a href="javascript:;" data-id="' . $v ['id'] . '"  class="btn btn-xs blue btn-clone"><i class="fa fa-times"></i> Clone</a></div>'
                );
            }
        }


        $records ["sEcho"] = $sEcho;
        $records ["iTotalRecords"] = $iTotalRecords;
        $records ["iTotalDisplayRecords"] = $iTotalRecords;

        /*
         * $gg = new Escort_Model_Kpu(); $res = $gg->get_datakpu($data2['witel']); //var_dump($res);die();
         */
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }

    public function execctionAction() {

        $zk = new Model_Prabasystemaddon();

        $new = new Model_Zprabawf();

        $params = $this->getRequest()->getParams();
        $data['id'] = 3;
        $data = $new->get_wf_process(11);

        die();


        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
            //Zend_Debug::dump($identity); die();
        } catch (Exception $e) {
            
        }
        $result = array(
            'transaction' => false,
            'message' => 'Invalid Parameter',
            'data' => null,
            'sql' => null,
            'parsing_data_time' => null,
            'cache_time' => null,
            'is_cache' => null
        );
        if ($_POST) {

            if (isset($_POST['json'])) {
                $dataform = json_decode($_POST['json']);

                foreach ($dataform as $k => $v) {
                    if ($v != "undefined") {
                        $_POST[$v->name] = $v->value;
                    }
                }
                unset($_POST['json']);
            }
            if ($_FILES) {
                $dest_dir = constant("APPLICATION_PATH") . "/../public/documents";
                $upload = new Zend_File_Transfer_Adapter_Http();
                $upload->setDestination($dest_dir);
                $files = $upload->getFileInfo();
                //Zend_Debug::dump($files); die();
                try {

                    $upload->receive();
                    $i = 1;
                    foreach ($files as $key => $vg) {
                        $ret = array();
                        $vg['uid'] = $identity->uid;
                        $vg['fkey'] = $key;
                        $ret = $zk->add_file($vg);
                        $_POST[$key] = $ret['id'];
                        $i++;
                    }
                } catch (Zend_File_Transfer_Exception $e) {
                    $e->getMessage();
                    Zend_Debug::dump($e);
                    die();
                }
            }
            //Zend_Debug::dump($_POST); die();

            if (isset($_POST['xxxxid']) && $_POST['xxxxid'] != "") {
                $px = $_POST['xxxxid'];
            } else {
                $px = $params ['xxxxid'];
            }
            $varC = $dd->get_api($px);
            // Zend_Debug::dump($varC); die();
            $conn = unserialize($varC ['conn_params']);
            $result = $m1->execute_api($conn, $varC, $_POST);
        }

        $this->_helper->layout->disableLayout();
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function clearcachesysAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        die("ok");
        $this->_helper->layout->disableLayout();

        $cache = Zend_Registry::get('cache');
        // $cache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
        ));
        $result = array(
            'retCode' => '00',
            'retMsg' => 'Clear All Cache',
            'result' => true,
            'data' => null
        );

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function clearcacheallAction() {
        
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
           
           
        } catch (Exception $e) {
           Zend_Debug::dump($e->message);  die();
        }
        
        $this->_helper->layout->disableLayout();
        
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_ALL);
        
        
        $result = array(
            'retCode' => '00',
            'retMsg' => 'Clear All Cache',
            'result' => false,
            'data' => null
        );

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function websocketbuilderAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {
            // Zend_Debug::dump($_POST); die();
            $mdl = new Model_Prabasystem ();
            $update = $mdl->websocketbuilder($_POST);

            if ($update ['result'] === true) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $update ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $result = array(
                    'retCode' => '11',
                    'retMsg' => $update ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }
    public function tableaubuilderAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {
            // Zend_Debug::dump($_POST); die();
            $mdl = new Model_Prabasystem ();
            $update = $mdl->tabbuilder($_POST);

            if ($update ['result'] === true) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $update ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $result = array(
                    'retCode' => '11',
                    'retMsg' => $update ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }
    public function htmlbuilderAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {
            // Zend_Debug::dump($_POST); die();
            $mdl = new Model_Prabasystem ();
            $update = $mdl->htmlbuilder($_POST);

            if ($update ['result'] === true) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $update ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $result = array(
                    'retCode' => '11',
                    'retMsg' => $update ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function dumpbuilderAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {
            // Zend_Debug::dump($_POST); die();
            $mdl = new Model_Zprabagraph ();
            $update = $mdl->dump_graph($_POST['idtime'], $_POST);



            if ($update ['result'] === true) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $update ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $result = array(
                    'retCode' => '11',
                    'retMsg' => $update ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function graphbuilderAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {
            // Zend_Debug::dump($_POST); die();
            $mdl = new Model_Prabasystem ();
            $update = $mdl->graphbuilder($_POST);



            if ($update ['result'] === true) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $update ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $result = array(
                    'retCode' => '11',
                    'retMsg' => $update ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function alertsystemAction() {
        $c = new Pm_Model_Pmsystem ();
        $params = $this->getRequest()->getParams();

        // Zend_Debug::dump($params); die();
        $ll = $c->getalert((int) $params [ID]);
        // Zend_Debug::dump($ll); die();
        $sendingalert = false;
        switch ($ll ['CHECKING_TYPE']) {
            case 'check tabel log' :
                $formula = $ll ['ALERT_FORMULA'];
                if ($ll ['CONNECTION'] == "") {
                    $status = $c->runformula($formula);
                    // Zend_Debug::dump($status); die();
                    // ($status);
                    if ($status ['STATUS'] != '1') {
                        $sendingalert = true;
                    }
                } else {
                    $connArr = explode('~', $ll ['CONNECTION']);
                    $pArr = array();
                    foreach ($connArr as $Val) {
                        $vK = explode("|", $Val);
                        $pArr [$vK [0]] = $vK [1];
                    }

                    $pparam = array(
                        'dbname' => $pArr ['dbname'],
                        'username' => $pArr ['username'],
                        'password' => $pArr ['password']
                    );
                    if ($pArr ['host']) {
                        $pparam = array(
                            'dbname' => $pArr ['dbname'],
                            'username' => $pArr ['username'],
                            'password' => $pArr ['password'],
                            'host' => $pArr ['host']
                        );
                    }
                    $dbz = Zend_Db::factory($pArr ['adapter'], $pparam);
                    // Zend_Debug::Dump($pArr['adapter']); die($formula);

                    try {
                        $status = $dbz->fetchRow($formula);
                    } catch (Exception $e) {
                        Zend_Debug::Dump($e);
                        die($formula);
                    }

                    if ($status ['STATUS'] != '1') {
                        $sendingalert = true;
                    }
                }

                break;
            case 'check file log' :
                $content = file_get_contents($ll ['LOG_OBJECT_NAME']);
                $pos = strpos($content, $ll ['ALERT_FORMULA']);
                if ($pos === false) {
                    $sendingalert = true;
                } else {
                    $sendingalert = false;
                }

                break;
            case 'check port' :

                $sendingalert = false;
                if ($ll ['CONNECTION'] != "") {
                    $fd = explode(":", $ll ['CONNECTION']);
                    $data = array();
                    $data [HOST] = $fd [0];
                    $data [PORT] = $fd [1];

                    if (!$this->check_port($data)) {
                        $sendingalert = true;
                    }
                }

                break;
            case 'check connection' :

                $sendingalert = false;
                if ($ll ['CONNECTION'] != "") {
                    $connArr = explode('~', $ll ['CONNECTION']);
                    $pArr = array();
                    foreach ($connArr as $Val) {
                        $vK = explode("|", $Val);
                        $pArr [$vK [0]] = $vK [1];
                    }

                    $pparam = array(
                        'dbname' => $pArr ['dbname'],
                        'username' => $pArr ['username'],
                        'password' => $pArr ['password']
                    );
                    if ($pArr ['host']) {
                        $pparam = array(
                            'dbname' => $pArr ['dbname'],
                            'username' => $pArr ['username'],
                            'password' => $pArr ['password'],
                            'host' => $pArr ['host']
                        );
                    }

                    try {
                        // Zend_Debug::dump($pArr['adapter']); die();
                        $dbz = Zend_Db::factory($pArr ['adapter'], $pparam);
                        $var = $dbz->getConnection();
                        // Zend_Debug::dump($var);

                        $sendingalert = false;
                    } catch (Zend_Exception $e) {
                        // Zend_Debug::dump($e); die();
                        $sendingalert = true;
                    }
                }

                // return false;

                break;
        }

        // echo $sendingalert; die();
        if ($sendingalert) {
            $cara = explode(',', $ll ['ALERT_TYPE']);
        }
        // Zend_Debug::dump($ll);
        // Zend_Debug::dump($cara);die("s");
        if (in_array('EMAIL', $cara)) {
            // $this->sending_mail($ll['ALERT_TEXT'], $ll['ZROLES']);
        }

        // Zend_Debug::dump($cara); die();

        if (in_array('SMS', $cara)) {
            $mobes = $c->get_contact($ll ['ZROLES'], 'MOBILE');
            // Zend_Debug::dump($mobes); die();
            foreach ($mobes as $mobe) {

                // $this->sending_sms($mobe['MOBILE'], $ll['ALERT_TEXT']);
            }
        }
        die("sss");
    }

    public function updateportletAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {
            // Zend_Debug::dump($_POST); die();
            $mdl = new Model_Prabasystem ();
            $_POST['name'] = strtolower($_POST['name']);
            $_POST['name'] = str_replace(array(
                "/",
                ",",
                "%",
                " ",
                ".",
                ">",
                "<",
                "-",
                ":",
                ")",
                "(",
                "_",
                "&",
                    ), "", $_POST['name']);

            $update = $mdl->update_portlet($_POST);

            if ($update ['result'] === true) {

                $_POST ['id'] = $update ['id'];
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $update ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $result = array(
                    'retCode' => '11',
                    'retMsg' => "eor",
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function formbuilderAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {
            // Zend_Debug::dump($_POST); die();


            $mdl = new Model_Prabasystem ();
            $update = $mdl->formbuilder($_POST);

            if ($update ['result'] === true) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $update ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $result = array(
                    'retCode' => '11',
                    'retMsg' => $update ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function updatepageAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {

            $_POST['pagealias'] = strtolower($_POST['pagealias']);
            $_POST['pagealias'] = str_replace(array(
                "/",
                ",",
                "%",
                " ",
                ".",
                ">",
                "<",
                "-",
                ":",
                ")",
                "(",
                "_",
                "&",
                    ), "", $_POST['pagealias']);

            $br = array();
            for ($i = 0; $i < count($_POST ['name']); $i ++) {
                $br [$i] ['name'] = $_POST ['name'] [$i];
                $br [$i] ['href'] = $_POST ['href'] [$i];
                $br [$i] ['class'] = $_POST ['class'] [$i];
            }

            $el = array();
            $k = 0;
            for ($i = 1; $i <= count($_POST ['col1']); $i ++) {
                if ($_POST ['col1'] [$k] != "") {
                    $el [$i] [1] = $_POST ['col1'] [$k];
                }
                if ($_POST ['col2'] [$k] != "") {
                    $el [$i] [2] = $_POST ['col2'] [$k];
                }

                if ($_POST ['col3'] [$k] != "") {
                    $el [$i] [3] = $_POST ['col3'] [$k];
                }
                if ($_POST ['col4'] [$k] != "") {
                    $el [$i] [4] = $_POST ['col4'] [$k];
                }
                if ($_POST ['col5'] [$k] != "") {
                    $el [$i] [5] = $_POST ['col5'] [$k];
                }
                if ($_POST ['col6'] [$k] != "") {
                    $el [$i] [6] = $_POST ['col6'] [$k];
                }
                $k ++;
            }

            // Zend_Debug::dump($br); die();

            $mdl = new Model_Prabasystem ();
            $update = $mdl->update_page($_POST, $br, $el);

            if ($update ['result'] === true) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $update ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $result = array(
                    'retCode' => '11',
                    'retMsg' => $update ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function addattrsAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {
            $mdl = new Model_Prabasystem ();

            // Zend_Debug::dump($_POST);
            $update = $mdl->add_attrs($_POST);

            if ($update ['result'] === true) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $update ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $result = array(
                    'retCode' => '11',
                    'retMsg' => $update ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function editloginappAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        // Zend_Debug::dump($_POST); die();
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {
            $mdl = new Model_Prabasystem ();
            $cc = new Model_System ();
            $update = $mdl->update_applogin($_POST);

            if ($update ['result'] === true) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $update ['message'],
                    'result' => true,
                    'data' => $_POST
                );
                $cc->generate_themes($_POST['id']);
            } else {
                $re1sult = array(
                    'retCode' => '11',
                    'retMsg' => $update ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function editappAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        // Zend_Debug::dump($_POST); die();
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {
            $mdl = new Model_Prabasystem ();
            $update = $mdl->update_app($_POST);

            if ($update ['result'] === true) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $update ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $result = array(
                    'retCode' => '11',
                    'retMsg' => $update ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function addappAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        //Zend_Debug::dump($_POST); die();
     //  echo shell_exec(" ls /home/himawijaya");die("s");
        
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {

            $cc = new Model_System ();
            $mdl = new Model_Prabasystem ();

            $_POST['short'] = strtolower($_POST['short']);
            $_POST['short'] = str_replace(array(
                "/",
                ",",
                "%",
                " ",
                ".",
                ">",
                "<",
                "-",
                ":",
                ")",
                "(",
                "_",
                "&",
                    ), "", $_POST['short']);


            $cc->generate_modules($_POST['short']);
            $update = $mdl->add_app($_POST);

            if ($update ['result'] === true) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $update ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $result = array(
                    'retCode' => '11',
                    'retMsg' => $update ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function addportletAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {
            $mdl = new Model_Prabasystem ();
            $_POST['name'] = strtolower($_POST['name']);
            $_POST['name'] = str_replace(array(
                "/",
                ",",
                "%",
                " ",
                ".",
                ">",
                "<",
                "-",
                ":",
                ")",
                "(",
                "_",
                "&",
                    ), "", $_POST['name']);

            $update = $mdl->add_portlet($_POST);

            if ($update ['result'] === true) {

                $_POST ['id'] = $update ['id'];
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $update ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $result = array(
                    'retCode' => '11',
                    'retMsg' => "eor",
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function getmethodsAction() {


        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();


        switch ($params['apitype']) {
            case 4 :
                $out = $this->get_method_zend($params);
                break;

            case 3:
            case 2 :
                $out = $this->get_method_proc($params);
                break;
        }

        echo $out;
        die();
    }

    function get_method_proc($params) {

        //Zend_Debug::dump($params); die();
        $mm = new Model_Prabasystem ();
        $dd = new Model_Prabasystemdriver ();
        $data = $mm->get_a_conn_by_name($params['conn']);
        $dat = $dd->get_list_proc($data, null, $params['xxxid']);

        $this->_helper->layout->disableLayout();



        // $out ="";
        $out = "<option></option>";
        foreach ($dat as $k => $v) {
            $out .= "<option value='" . $v . "'>" . $v . "</option>";
        }

        echo $out;
        die();
    }

    function get_method_zend($params) {


        $this->_helper->layout->disableLayout();


        $mm = new Model_Prabasystem ();
        $dat = $mm->get_method_class_model($params ['xxxid']);
        // $out ="";
        $out = "<option></option>";
        foreach ($dat as $k => $v) {
            $out .= "<option value='" . $v . "'>" . $v . "</option>";
        }

        echo $out;
        die();
    }

    public function getmodelsAction() {

        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();


        switch ($params['xxxid']) {
            case 4 :
                $out = $this->modelZend($params);
                break;

            case 3:
            case 2 :
                $out = $this->modelProc($params);
                break;
        }

        echo $out;
        die();
    }

    function modelProc($params) {

        $mm = new Model_Prabasystem ();
        $dd = new Model_Prabasystemdriver ();
        $data = $mm->get_a_conn($params['conn']);
        //Zend_Debug::dump($data);die();

        $list = $dd->get_list_proc($data, 'user');
        $out = '<div class="resType"><div  class="col-md-5">
										<div class="form-group">
											<label for="model">Schema</label><select onchange=changeModel(this)  name="model" id="model" class="form-control" placeholder="Model" >
											<option></option>';

        foreach ($list as $v) {
            $out .= "<option value='" . $v . "'>" . $v . "</option>";
        }
        $out .= '</select>
					<span class="help-block">
												Select Model Class
											</span>
										</div>
									</div>
									<div class="col-md-offset-1 col-md-5 ">
										<div class="form-group">
											<label for="method">Proc</label><select  onchange=changeMethod(this)  name="method" id="method" class="form-control" placeholder="Method" >
											</select>
										</div>
									</div></div>';


        return $out;
    }

    function modelZend($params) {



        $mm = new Model_Prabasystem ();
        $dat = $mm->get_models_class();

        $out = '<div class="resType"><div  class="col-md-5">
										<div class="form-group">
											<label for="model">Class Model</label><select onchange=changeModel(this)  name="model" id="model" class="form-control" placeholder="Model" >
											<option></option>';

        foreach ($dat as $k => $v) {
            // Zend_Debug::dump($v);
            $out .= "<optgroup label='" . $k . "'>";
            foreach ($v as $vv) {
                $out .= "<option value='" . $vv . "'>" . $vv . "</option>";
            }

            $out .= "</optgroup>";
        }
        $out .= '</select>
											<span class="help-block">
												Select Model Class
											</span>
										</div>
									</div>
									<div class="col-md-offset-1 col-md-5 ">
										<div class="form-group">
											<label for="method">Method Class</label><select  onchange=changeMethod(this)  name="method" id="method" class="form-control" placeholder="Method" >
											</select>
										</div>
									</div></div>';


        return $out;
    }

    public function getoutAction() {

        $zk = new Model_Prabasystemaddon();
        $m1 = new Model_Zpraba4api ();
        $dd = new Model_Zprabapage ();
        $params = $this->getRequest()->getParams();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );



        if ($_POST) {
            $dd = new Model_Zprabapage ();
            //$varC = $dd->get_api ($_POST['id'] );
            //Zend_Debug::dump($_POST );
            $conn = $dd->get_conn($_POST['conname']);
            //Zend_Debug::dump($conn );die();
            $data = $_POST;
            //b.conn_name, b.conn_params, b.desc as optionals, b.is_deactivate 

            $data['conn_name'] = $conn['conn_name'];
            $data['conn_params'] = $conn['conn_params'];
            $data['optionals'] = $conn['optionals'];
            $data['is_deactivate'] = $conn['is_deactivate'];


            $data['backendcache'] = $_POST['backendcache'];
            $data['sql_text'] = $_POST['sqltext'];
            $data['api_desc'] = $_POST['apidesc'];
            $data['api_type'] = $_POST['apitype'];
            //	$data['api_str_replace'] =  $_POST['is_deactivate'];
            $data['attrs1'] = $_POST['attrs1'];
            $data['cache_time'] = $_POST['cache'];
            $data['api_mode'] = $_POST['mode'];


            $mdl = new Model_Prabasystem ();

            $conn = unserialize($conn ['conn_params']);
            //Zend_Debug::dump($conn );die();	

            $vdata = explode('~', $_POST ['inputsqltext']);
            $input = array();
            foreach ($vdata as $k => $v) {
                $jj[$k] = explode('=', $v);
                $input[$jj[$k][0]] = $jj[$k][1];
            }

            $update = $m1->execute_api($conn, $data, $input);
            //Zend_Debug::dump($input);
            //Zend_Debug::dump($result);die();
        }

        //header ( "Access-Control-Allow-Origin: *" );
        //header ( 'Content-Type: application/json' );

        $out = array();
        if ($_POST['mode'] == 2) {
            foreach ($update['data'][0] as $k => $v) {

                $out[] = $k;
            }
        }
        if ($_POST['mode'] == 3) {
            foreach ($update['data'] as $k => $v) {

                $out[] = $k;
            }
        }


        die(implode(',', $out));

        die();
    }

    public function tesapiAction() {

        $zk = new Model_Prabasystemaddon();
        $m1 = new Model_Zpraba4api ();
        $dd = new Model_Zprabapage ();
        $params = $this->getRequest()->getParams();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );



        if ($_POST) {
            $dd = new Model_Zprabapage ();
            //$varC = $dd->get_api ($_POST['id'] );

            $conn = $dd->get_conn_id($_POST['conname']);
            //Zend_Debug::dump($conn );die();
            $data = $_POST;
            //b.conn_name, b.conn_params, b.desc as optionals, b.is_deactivate 

            $data['conn_name'] = $conn['conn_name'];
            $data['conn_params'] = $conn['conn_params'];
            $data['optionals'] = $conn['optionals'];
            $data['is_deactivate'] = $conn['is_deactivate'];
            $_POST['cache'] = "0";

            $data['backendcache'] = $_POST['backendcache'];
            $data['sql_text'] = $_POST['sqltext'];
            $data['api_desc'] = $_POST['apidesc'];
            $data['api_type'] = $_POST['apitype'];
            //	$data['api_str_replace'] =  $_POST['is_deactivate'];
            $data['attrs1'] = $_POST['attrs1'];
            $data['cache_time'] = $_POST['cache'];
            $data['api_mode'] = $_POST['mode'];


            $mdl = new Model_Prabasystem ();

            $conn = unserialize($conn ['conn_params']);
            //Zend_Debug::dump($data );die();	

            $vdata = explode('~', $_POST ['inputsqltext']);
            $input = array();
            foreach ($vdata as $k => $v) {
                $jj[$k] = explode('=', $v);
                $input[$jj[$k][0]] = $jj[$k][1];
            }

            //Zend_Debug::dump($conn); 
            //Zend_Debug::dump($data);die(); 
            $update = $m1->execute_api($conn, $data, $input);


            //Zend_Debug::dump($input);
            //Zend_Debug::dump($result);die();
        }

        //header ( "Access-Control-Allow-Origin: *" );
        //header ( 'Content-Type: application/json' );
        Zend_Debug::dump($update);
        die();
    }

    public function callapiAction() {
        //ini_set("display_errors", "On");
        //ini_set("memory_limit","1280M");
        $zk = new Model_Prabasystemaddon();
        $m1 = new Model_Zpraba4api ();
        $dd = new Model_Zprabapage ();
        $params = $this->getRequest()->getParams();

        //Zend_Debug::dump($params); die();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
            //Zend_Debug::dump($identity); die();
        } catch (Exception $e) {
            
        }
        $result = array(
            'transaction' => false,
            'message' => 'Invalid Parameter',
            'data' => null,
            'sql' => null,
            'parsing_data_time' => null,
            'cache_time' => null,
            'is_cache' => null
        );

        $varC = $dd->get_api($params['api']);
        $conn = unserialize($varC ['conn_params']);
        $result = $m1->execute_api($conn, $varC, $params);


        $this->_helper->layout->disableLayout();
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function postformAction() {
        //ini_set("display_errors", "On");
        //ini_set("memory_limit","1280M");
        $zk = new Model_Prabasystemaddon();
        $m1 = new Model_Zpraba4api ();
        $dd = new Model_Zprabapage ();
        $params = $this->getRequest()->getParams();

        //Zend_Debug::dump($params); die();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
            //Zend_Debug::dump($identity); die();
        } catch (Exception $e) {
            
        }
        $result = array(
            'transaction' => false,
            'message' => 'Invalid Parameter',
            'data' => null,
            'sql' => null,
            'parsing_data_time' => null,
            'cache_time' => null,
            'is_cache' => null
        );
        if ($_POST) {

            if (isset($_POST['json'])) {
                $dataform = json_decode($_POST['json']);

                foreach ($dataform as $k => $v) {
                    if ($v != "undefined") {
                        $_POST[$v->name] = $v->value;
                    }
                }
                unset($_POST['json']);
            }
            if ($_FILES) {

                //Zend_Debug::dump($_FILES); die();
                $dest_dir = constant("APPLICATION_PATH") . "/../public/uploader";
                $upload = new Zend_File_Transfer_Adapter_Http();
                $upload->setDestination($dest_dir);
                $files = $upload->getFileInfo();
                //Zend_Debug::dump($files); die();
                try {

                    $upload->receive();
                    $i = 1;
                    foreach ($files as $key => $vg) {
                        $ret = array();
                        $vg['uid'] = $identity->uid;
                        $vg['fkey'] = $key;
                        $ret = $zk->add_file($vg);
                        $_POST['files'][$key] = $ret['id'];

                        $i++;
                    }
                    $_POST['RAWFILES'] = $_FILES;
                } catch (Zend_File_Transfer_Exception $e) {
                    $e->getMessage();
                    Zend_Debug::dump($e);
                    die();
                }
            }
            //Zend_Debug::dump($_POST); die();

            if (isset($_POST['xxxxid']) && $_POST['xxxxid'] != "") {
                $px = $_POST['xxxxid'];
            } else {
                $px = $params ['xxxxid'];
            }
            try {
                $varC = $dd->get_api($px);
                // Zend_Debug::dump($varC); die();
                $conn = unserialize($varC ['conn_params']);
                $result = $m1->execute_api($conn, $varC, $_POST);
            } catch (Exception $e) {
                Zend_Debug::dump($e);
                die();
            }
        }

        $this->_helper->layout->disableLayout();
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function testconnAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {
            $mdl = new Model_Prabasystem ();
            $update = $mdl->test_conn($_POST);
            // Zend_Debug::dump($_POST); die();
            if ($update ['result'] === true) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $update ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $result = array(
                    'retCode' => '11',
                    'retMsg' => $update ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function addconnAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {

            $mdl = new Model_Prabasystem ();
            $update = $mdl->add_conn($_POST);

            if ($update ['result'] === true) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $update ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $result = array(
                    'retCode' => '11',
                    'retMsg' => $update ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function addapiuserAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {
            // Zend_Debug::dump($_POST); die();
            $mdl = new Model_Prabasystem ();
            $update = $mdl->add_apiuser($_POST);

            if ($update ['result'] === true) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $update ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $result = array(
                    'retCode' => '11',
                    'retMsg' => $update ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function cloneAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $params = $this->getRequest()->getParams();
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );


        if ($_POST) {
            //Zend_Debug::dump($_POST); die();
            $mdl = new Model_Zprabaction ();
            $update = $mdl->get_process_clone($_POST['uid']);

            if ($update ['result'] === true) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $update ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $result = array(
                    'retCode' => '11',
                    'retMsg' => $update ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function addapiAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {
            // Zend_Debug::dump($_POST); die();
            $mdl = new Model_Prabasystem ();
            $update = $mdl->add_api($_POST);

            if ($update ['result'] === true) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $update ['message'],
                    'result' => true,
                    'data' => $_POST,
					'api_id'=>$update['api_id']
                );
            } else {
                $result = array(
                    'retCode' => '11',
                    'retMsg' => $update ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function addpageAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {
            $mdl = new Model_Prabasystem ();
            // Zend_Debug::dump($_POST); die();

            $_POST['pagealias'] = strtolower($_POST['pagealias']);
            $_POST['pagealias'] = str_replace(array(
                "/",
                ",",
                "%",
                " ",
                ".",
                ">",
                "<",
                "-",
                ":",
                ")",
                "(",
                "_",
                "&",
                    ), "", $_POST['pagealias']);

            $br = array();
            for ($i = 0; $i < count($_POST ['name']); $i ++) {
                $br [$i] ['name'] = $_POST ['name'] [$i];
                $br [$i] ['href'] = $_POST ['href'] [$i];
                $br [$i] ['class'] = $_POST ['class'] [$i];
            }

            $el = array();
            $k = 0;
            for ($i = 1; $i <= count($_POST ['col1']); $i ++) {
                if ($_POST ['col1'] [$k] != "") {
                    $el [$i] [1] = $_POST ['col1'] [$k];
                }
                if ($_POST ['col2'] [$k] != "") {
                    $el [$i] [2] = $_POST ['col2'] [$k];
                }

                if ($_POST ['col3'] [$k] != "") {
                    $el [$i] [3] = $_POST ['col3'] [$k];
                }
                if ($_POST ['col4'] [$k] != "") {
                    $el [$i] [4] = $_POST ['col4'] [$k];
                }
                if ($_POST ['col5'] [$k] != "") {
                    $el [$i] [5] = $_POST ['col5'] [$k];
                }
                if ($_POST ['col6'] [$k] != "") {
                    $el [$i] [6] = $_POST ['col6'] [$k];
                }
                $k ++;
            }
            // Zend_Debug::dump($el); die();
            $update = $mdl->add_page($_POST, $br, $el);

            if ($update) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $update ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $result = array(
                    'retCode' => '11',
                    'retMsg' => $update ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function wizlistportletjsonAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $this->_helper->layout->disableLayout();

        $mdl_admin = new Model_Prabasystem ();

        $params = $this->getRequest()->getParams();

        // Zend_Debug::dump($params); die();

        $list = $mdl_admin->listapiportletjson($params);
        // Zend_Debug::dump($list); die();

        $zz = 4;
        foreach ($list as $k => $v) {
            $records ['total'] = 10;
            $records ['movies'] [0] ['id'] = "";
            $records ['movies'] [0] ['title'] = "";

            $records ['movies'] [1] ['id'] = "newgraphic";
            $records ['movies'] [1] ['title'] = "new Graphic";

            $records ['movies'] [2] ['id'] = "newtable";
            $records ['movies'] [2] ['title'] = "new Table";

            $records ['movies'] [3] ['id'] = "newgraphic";
            $records ['movies'] [3] ['title'] = "new Graphic";

            $records ['movies'] [3] ['id'] = "newgraphic";
            $records ['movies'] [3] ['title'] = "new Graphic";

            if (isset($params ['id'])) {
                $records ['total'] = 1;
            }
            $records ['movies'] [$zz] ['id'] = $v ['id'];
            if (is_numeric($v ['id'])) {
                $records ['movies'] [$zz] ['title'] = $v ['id'] . '_' . $v ['portlet_name'];
            } else {
                $records ['movies'] [$zz] ['title'] = $v ['portlet_name'];
            }
            $zz ++;
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        // echo json_encode($records);
        $jsonp_callback = isset($_GET ['callback']) ? $_GET ['callback'] : null;

        $json = json_encode($records);
        print $jsonp_callback ? "$jsonp_callback($json)" : $json;

        die();
    }

    public function listportletjsonAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $this->_helper->layout->disableLayout();

        $mdl_admin = new Model_Prabasystem ();

        $params = $this->getRequest()->getParams();

        // Zend_Debug::dump($params); die();

        $list = $mdl_admin->listapiportletjson($params);
        // Zend_Debug::dump($list); die();

        $zz = 1;
        foreach ($list as $k => $v) {
            $records ['total'] = 10;
            $records ['movies'] [0] ['id'] = "";
            $records ['movies'] [0] ['title'] = "";
            if (isset($params ['id'])) {
                $records ['total'] = 1;
            }
            $records ['movies'] [$zz] ['id'] = $v ['id'];
            if (is_numeric($v ['id'])) {
                $records ['movies'] [$zz] ['title'] = $v ['id'] . '_' . $v ['portlet_name'];
            } else {
                $records ['movies'] [$zz] ['title'] = $v ['portlet_name'];
            }
            $zz ++;
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        // echo json_encode($records);
        $jsonp_callback = isset($_GET ['callback']) ? $_GET ['callback'] : null;

        $json = json_encode($records);
        print $jsonp_callback ? "$jsonp_callback($json)" : $json;

        die();
    }

    public function listapijsonAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $params = $mdl_admin->get_params('api_type');

        Zend_Debug::dump($params);
        die();

        $this->_helper->layout->disableLayout();

        $mdl_admin = new Model_Prabasystem ();

        $list = $mdl_admin->listapiportlet();

        foreach ($list as $k => $v) {
            $records [$k] ['value'] = $v ['id'];
            $records [$k] ['text'] = 'apiId:' . $v ['id'] . '_' . substr($v ['sql_text'], 0, 20);
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        $jsonp_callback = isset($_GET ['callback']) ? $_GET ['callback'] : null;

        $json = json_encode($records);
        print $jsonp_callback ? "$jsonp_callback($json)" : $json;

        die();
    }

    public function listapiuserAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $this->_helper->layout->disableLayout();

        $mdl_admin = new Model_Prabasystem ();
        $countgraph = $mdl_admin->count_listapiuser($_GET);

        $params = $mdl_admin->get_params('api_type');

        foreach ($params as $v) {
            $vparams [$v ['value_param']] = $v ['display_param'];
        }

        $listgraph = $mdl_admin->listapiuser($_GET);
        // Zend_Debug::dump($countgraph);die();
        $iTotalRecords = intval($countgraph);
        $iDisplayLength = intval($_GET ['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_GET ['iDisplayStart']);
        $sEcho = intval($_GET ['sEcho']);

        $records = array();
        $records ["aaData"] = array();

        $end = $iDisplayStart + $iDisplayLength;

        foreach ($listgraph as $k => $v) {
            $records ["aaData"] [] = array(
                $iDisplayStart + $k + 1,
                $v ['id'],
                $v ['user_name'],
                $v ['user_ip'],
                $v ['user_key'],
                $v ['user_desc'],
                '<div style="text-align:center"><a href="/prabaeditor/editapiuser/id/' . $v ['id'] . '" data-id="' . $v ['id'] . '" class="btn btn-xs green btn-edit"><i class="fa fa-search"></i> Edit Api</a>' .
                '<a href="javascript:;" data-id="' . $v ['id'] . '" data-uname="' . $v ['user_name'] . '" class="btn btn-xs red btn-delete"><i class="fa fa-times"></i> Delete Api</a></div>'
            );
        }

        $records ["sEcho"] = $sEcho;
        $records ["iTotalRecords"] = $iTotalRecords;
        $records ["iTotalDisplayRecords"] = $iTotalRecords;

        /*
         * $gg = new Escort_Model_Kpu(); $res = $gg->get_datakpu($data2['witel']); //var_dump($res);die();
         */
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }

    public function listapiAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $this->_helper->layout->disableLayout();

        $mdl_admin = new Model_Prabasystem ();
        $countgraph = $mdl_admin->count_listapi($_GET);

        $params = $mdl_admin->get_params('api_type');

        foreach ($params as $v) {
            $vparams [$v ['value_param']] = $v ['display_param'];
        }

        $listgraph = $mdl_admin->listapi($_GET);
        // Zend_Debug::dump($countgraph);die();
        $iTotalRecords = intval($countgraph);
        $iDisplayLength = intval($_GET ['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_GET ['iDisplayStart']);
        $sEcho = intval($_GET ['sEcho']);

        $records = array();
        $records ["aaData"] = array();

        $end = $iDisplayStart + $iDisplayLength;

        foreach ($listgraph as $k => $v) {
            if ($v['is_running_socket'] != 1) {
                $run = '<br><span class="label label-sm label-danger">
													socket server not running
												</span>';
            } elseif ($v['is_running_socket'] == 1) {
                $run = '<br><span class="label label-sm label-success">
													socket server  running
												</span>';
            }

            if ($v['is_freeze'] != 1) {
                $soc = '<div style="text-align:center"><a href="javascript:;" data-id="' . $v ['id'] . '" class="btn btn-xs blue btn-freeze"><i class="fa fa-search"></i> freeze </a>';
            } elseif ($v['is_running_socket'] == 1) {
                $soc = '';
            }

            $records ["aaData"] [] = array(
                $iDisplayStart + $k + 1,
                $v ['id'],
                $v ['conn_name'],
                $v ['sql_text'],
                $v ['api_desc'],
                $vparams [$v ['api_type']],
                $v ['api_str_replace'],
                $v ['attrs1'],
                $v ['cache_time'],
                '<div style="text-align:center"><a href="/prabaeditor/editapi/id/' . $v ['id'] . '" data-id="' . $v ['id'] . '" class="btn btn-xs green btn-edit"><i class="fa fa-search"></i> Edit Api</a>' .
                //$soc.

                '<a href="javascript:;" data-id="' . $v ['id'] . '" data-uname="' . $v ['uname'] . '" data-fullname="' . $v ['fullname'] . '" data-email="' . $v ['email'] . '" data-ubis="' . $v ['ubis'] . '" data-sububis="' . $v ['sub_ubis'] . '" data-sububisid="' . $v ['sub_ubis_id'] . '" data-position="' . $v ['jabatan'] . '" data-mobilephone="' . $v ['mobile_phone'] . '" data-fixedphone="' . $v ['fixed_phone'] . '" class="btn btn-xs red btn-delete"><i class="fa fa-times"></i> Delete Api</a></div>'
            );
        }

        $records ["sEcho"] = $sEcho;
        $records ["iTotalRecords"] = $iTotalRecords;
        $records ["iTotalDisplayRecords"] = $iTotalRecords;

        /*
         * $gg = new Escort_Model_Kpu(); $res = $gg->get_datakpu($data2['witel']); //var_dump($res);die();
         */
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }

     public function deleteappAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $this->_helper->layout->disableLayout();

        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {
           // Zend_Debug::dump($_POST); die();
            $mdl_zusr = new Model_Prabasystem ();
            $delete = $mdl_zusr->delete_app($_POST);
            if ($delete ['result'] === true) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $delete ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $delete = array(
                    'retCode' => '11',
                    'retMsg' => $delete ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }
    public function deletepageAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $this->_helper->layout->disableLayout();

        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {
            // Zend_Debug::dump($_POST); die();
            $mdl_zusr = new Model_Prabasystem ();
            $delete = $mdl_zusr->delete_page($_POST ['uid']);
            if ($delete ['result'] === true) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $delete ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $delete = array(
                    'retCode' => '11',
                    'retMsg' => $delete ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function deleteportletAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $this->_helper->layout->disableLayout();

        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {
            // Zend_Debug::dump($_POST); die();
            $mdl_zusr = new Model_Prabasystem ();
            $delete = $mdl_zusr->delete_portlet($_POST ['uid']);
            if ($delete ['result'] === true) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $delete ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $delete = array(
                    'retCode' => '11',
                    'retMsg' => $delete ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function deleteapiuserAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $this->_helper->layout->disableLayout();

        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {
            // Zend_Debug::dump($_POST); die();
            $mdl_zusr = new Model_Prabasystem ();
            $delete = $mdl_zusr->delete_api_user($_POST ['uid']);
            if ($delete ['result'] === true) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $delete ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $delete = array(
                    'retCode' => '11',
                    'retMsg' => $delete ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function deleteactionAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $this->_helper->layout->disableLayout();

        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {
            // Zend_Debug::dump($_POST); die();
            $mdl_zusr = new Model_Prabasystem ();
            $delete = $mdl_zusr->delete_action($_POST ['uid']);
            if ($delete ['result'] === true) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $delete ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $delete = array(
                    'retCode' => '11',
                    'retMsg' => $delete ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function deleteapiAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $this->_helper->layout->disableLayout();

        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {
            // Zend_Debug::dump($_POST); die();
            $mdl_zusr = new Model_Prabasystem ();
            $delete = $mdl_zusr->delete_api($_POST ['uid']);
            if ($delete ['result'] === true) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $delete ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $delete = array(
                    'retCode' => '11',
                    'retMsg' => $delete ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function editapiuserAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {
            $mdl = new Model_Prabasystem ();
            $update = $mdl->update_api_user($_POST);

            if ($update ['result'] === true) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $update ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $result = array(
                    'retCode' => '11',
                    'retMsg' => $update ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function geturlAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();



        if ($_POST) {
            //Zend_Debug::dump($_POST); die();	

            $input = $_POST['input'];

            $var = explode('~', $_POST['input']);

            foreach ($var as $k => $v) {
                $zz = explode(':', $v);
                $new['input:' . $zz[0]] = $zz[1];
            }

            $text = array(
                'username' => $_POST['uname'],
                'api_id' => $_POST['api'],
                'format' => $_POST['format'],
                'is_action' => $_POST['is_action'],
                    //'input:orang'=>'1000'
            );

            $text = array_merge($text, $new);

            //Zend_Debug::dump($text);die();
            // 'input:customer_name'=>'ARY_DANAMON');

            $text = preg_replace(array(
                "@:@",
                "@%@",
                "@\/@",
                "@\<@",
                "@\>@"
                    ), array(
                '&#58;',
                '&#37;',
                '&###;',
                '&#60;',
                '&#62;'
                    ), $text);

            $enc = base64_encode(serialize($text));




            $result = array(
                'retCode' => '00',
                'retMsg' => 'prabapublish/index/data/' . $enc,
                'result' => true,
                'data' => $_POST
            );
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function geturlrawAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();



        if ($_POST) {
            //Zend_Debug::dump($_POST); die();	

            $uname = explode("::", $_POST['uname']);

            $input = strtolower(preg_replace("/[^a-zA-Z]+/", "", $_POST['input']));

            if ($_POST['urltype'] == '2') {
                $find = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
                $replace = array("c", "e", "g", "i", "m", "n", "q", "r", "u", "x");
                $api = str_replace($find, $replace, $_POST['api']);
                //Zend_Debug::dump($uname);die();
                $ptype = str_replace($find, $replace, $_POST['ptype']);
                $uname = str_replace($find, $replace, $uname[0]);
                $format = array(
                    'json' => 1,
                    'xml' => 2
                );

                $e = $api . "z" . $uname . "z" . $format[$_POST['format']] . "z" . $ptype;

                $result = array(
                    'retCode' => '00',
                    'retMsg' => '<a href="http://' . $_SERVER['HTTP_HOST'] . '/prabapublish/esb/e/' . $e .
                    '/id/' . $input . '" >link</a>',
                    'result' => true,
                    'data' => $_POST
                );
            } else {


                $input = $_POST['input'];

                $var = explode('~', $_POST['input']);
                foreach ($var as $k => $v) {
                    $zz = explode(':', $v);
                    $new['input:' . $zz[0]] = $zz[1];
                }

                $text = array(
                    'username' => $_POST['uname'],
                    'api_id' => $_POST['api'],
                    'format' => $_POST['format'],
                    'ptype' => $_POST['ptype'],
                );

                //$text = array_merge($text, $new);
                //Zend_Debug::dump($text);die();
                // 'input:customer_name'=>'ARY_DANAMON');




                $result = array(
                    'retCode' => '00',
                    'retMsg' => '<a href="http://' . $_SERVER['HTTP_HOST'] . '/prabapublish/esb/id/' . $_POST['api'] . '/ptype/' . $_POST['ptype'] . '/uname/' . $uname[1] . '/format/' . $_POST['format'] . '" >link</a>',
                    'result' => true,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function editapiAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {
            $mdl = new Model_Prabasystem ();
            $update = $mdl->update_api($_POST);

            if ($update ['result'] === true) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $update ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $result = array(
                    'retCode' => '11',
                    'retMsg' => $update ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function editconnAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {

            $mdl = new Model_Prabasystem ();
            $update = $mdl->update_conn($_POST);

            if ($update ['result'] === true) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $update ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $result = array(
                    'retCode' => '11',
                    'retMsg' => $update ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function listpageAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $this->_helper->layout->disableLayout();

        $mdl_admin = new Model_Zprabapage ();
        $countgraph = $mdl_admin->count_listpage($_GET);

        $listgraph = $mdl_admin->listpage_ajax($_GET);
        // Zend_Debug::dump($listgraph);die();
        $iTotalRecords = intval($countgraph);
        $iDisplayLength = intval($_GET ['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_GET ['iDisplayStart']);
        $sEcho = intval($_GET ['sEcho']);

        $records = array();
        $records ["aaData"] = array();

        $end = $iDisplayStart + $iDisplayLength;

        $mdl = new Model_Prabasystem ();
        $apps = $mdl->get_apps();

        foreach ($listgraph as $k => $v) {
            $records ["aaData"] [] = array(
                $iDisplayStart + $k + 1,
                $v ['id'],
                $v ['name_alias'],
                $v ['layout_id'],
                $v ['workspace_id'],
                $apps [$v ['app_id']],
                $v ['title'],
                '<div style="text-align:center"><a href="/prabaeditor/editpage/id/' . $v ['id'] . '" data-id="' . $v ['id'] . '" class="btn btn-xs green btn-edit"><i class="fa fa-search"></i> Edit Page</a>' . '<a href="/prabagen/index/page/' . $v ['id'] . '" data-id="' . $v ['id'] . '" class="btn btn-xs yellow btn-add"><i class="fa fa-search"></i> View Page</a>' . '<a href="javascript:;" data-id="' . $v ['id'] . '"  class="btn btn-xs red btn-delete"><i class="fa fa-times"></i> Delete Api</a></div>'
            );
        }

        $records ["sEcho"] = $sEcho;
        $records ["iTotalRecords"] = $iTotalRecords;
        $records ["iTotalDisplayRecords"] = $iTotalRecords;

        /*
         * $gg = new Escort_Model_Kpu(); $res = $gg->get_datakpu($data2['witel']); //var_dump($res);die();
         */
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }

    public function callpagerajaxAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $params = $this->getRequest()->getParams();

        $this->_helper->layout->disableLayout();

        $varC = $dd->get_api($params['id']);
        $conn = unserialize($varC ['conn_params']);
        $list = $m1->execute_api($conn, $varC, $params);
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($list);
        die();
    }

    public function listportletAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $this->_helper->layout->disableLayout();

        $mdl_admin = new Model_Zprabapage ();
        $countgraph = $mdl_admin->count_listportlet($_GET);

        $listgraph = $mdl_admin->listportlet_ajax($_GET);
        // Zend_Debug::dump($listgraph);die();
        $iTotalRecords = intval($countgraph);
        $iDisplayLength = intval($_GET ['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_GET ['iDisplayStart']);
        $sEcho = intval($_GET ['sEcho']);

        $records = array();
        $records ["aaData"] = array();

        $end = $iDisplayStart + $iDisplayLength;

        $mdl = new Model_Prabasystem ();
        // $apps = $mdl->get_apps();

        foreach ($listgraph as $k => $v) {
            $records ["aaData"] [] = array(
                $iDisplayStart + $k + 1,
                $v ['id'],
                $v ['portlet_name'],
                $v ['portlet_title'],
                $v ['portlet_type'],
                $v ['custom_view'],
                '<div style="text-align:center"><a href="/prabagen/viewportlet/id/' . $v ['id'] . '" data-id="' . $v ['id'] . '" class="btn btn-xs blue btn-edit"><i class="fa fa-search"></i> View</a><a href="/prabaeditor/editportlet/id/' . $v ['id'] . '" data-id="' . $v ['id'] . '" class="btn btn-xs green btn-edit"><i class="fa fa-search"></i> Edit</a>' . '<a  class="btn btn-xs yellow getscript" data-id="' . $v ['id'] . '" data-toggle="modal"><i class="fa fa-times"></i>Get Script</a>' . '<a href="javascript:;" data-id="' . $v ['id'] . '"  class="btn btn-xs red btn-delete"><i class="fa fa-times"></i> Delete</a></div>'
            );
        }

        $records ["sEcho"] = $sEcho;
        $records ["iTotalRecords"] = $iTotalRecords;
        $records ["iTotalDisplayRecords"] = $iTotalRecords;

        /*
         * $gg = new Escort_Model_Kpu(); $res = $gg->get_datakpu($data2['witel']); //var_dump($res);die();
         */
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }

    public function pmajaxerAction() {
        $url = 'http://' . $_SERVER ['HTTP_HOST'] . "/pm/cron/alertsystem/ID";
        $cc = new Pm_Model_Pmallerts ();
        $data = $cc->get_all_allerts(NULL, 1);
        // Zend_Debug::dump($data); die();
        $current = "";
        foreach ($data as $vv) {
            $current .= "" . $vv ['CRON_FORMULA'] . " /usr/bin/wget -q -O " . $_SERVER ['DOCUMENT_ROOT'] . "/logs/log.txt  $url/" . $vv [ID] . "\n";
        }
        $files = $_SERVER ['DOCUMENT_ROOT'] . "/logs/cron.txt";
        // die($files);
        file_put_contents($files, $current);
        exec("crontab " . $files, $output, $return);
        // $return =0;
        if ($return === 0) {
            echo json_encode(array(
                "success" => true
            ));
            die();
        } else {
            echo json_encode(array(
                "false" => true
            ));
            die();
        }
    }

}
