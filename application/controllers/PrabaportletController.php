<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class PrabaportletController extends Zend_Controller_Action {
	public function init() {
	}
	public function getscriptAction() {
	}
	public function indexAction() {
		//Zend_Session::namespaceUnset ( 'api' );
		$authAdapter = Zend_Auth::getInstance ();
		$identity = $authAdapter->getIdentity ();
		//Zend_Debug::dump ($identity );die();
		$ns = new Zend_Session_Namespace ( 'api' );
		$params = $this->getRequest ()->getParams ();
		$dd = new Model_Zprabapage ();
		$exe = new Model_Zpraba4api ();
		$var = $dd->get_portlet ( $params ['id'] );
		// Zend_Debug::dump($var);die();
	
		($_GET ['xin']) ? $xin = $_GET ['xin'] : $xin = $params ['xin'];
		$xin = urldecode ( $xin );
		
		$input = array ();
		if (isset ( $xin ) && $xin != null) {
			$pos = strpos ( $xin, '~' );
			$p = array ();
			$ti = array ();
			if ($pos !== false) {
				$qar = explode ( '~', $xin );
				foreach ( $qar as $q ) {
					$p [] = explode ( ':', $q );
					foreach ( $p as $pa ) {
						$ti [$pa [0]] = $pa [1];
					}
				}
			} else {
				$qarin = explode ( ':', $xin );
				$ti [$qarin [0]] = $qarin [1];
			}
		}
		if ($var ['get_api'] != "") {
			
			$vapi = explode ( ',', $var ['get_api'] );
			foreach ( $vapi as $vz ) {
				$varC = $dd->get_api ( $vz ); //Zend_Debug::dump($varC);die();
				// $varC['api_mode']=5;
				$conn = unserialize ( $varC ['conn_params'] );
				$xxArr [$vz] = $exe->execute_api ( $conn, $varC, $ti ); //Zend_Debug::dump($xxArr);die();
				$xxArrses [$vz] = $xxArr [$vz]; //Zend_Debug::dump($xxArrses);die();
				unset ( $xxArrses [$vz] ['data'] ); //Zend_Debug::dump($xxArrses);die();
			}
			
			$ns->vglobal [$var ['id']] = $xxArrses;
		}

		// Zend_Debug::dump($params);die('X');


		$api = "";
		$apitype = "";
		if (isset ( $params ['initapi'] ) && $params ['initapi'] != "") {
			$api = $params ['initapi'];
		} elseif (isset ( $var ['content_attrs'] ['api_id'] ) && $var ['content_attrs'] ['api_id'] != "") {
			$api = $var ['content_attrs'] ['api_id'];
		}
		
		if (isset ( $params ['apitype'] ) && $params ['apitype'] != "") {
			$apitype = $params ['apitype'];
		} elseif (isset ( $var ['content_attrs'] ['api_type'] ) && $var ['content_attrs'] ['api_type'] != "") {
			$apitype = $var ['content_attrs'] ['api_type'];
		}

		// Zend_Debug::dump($api);die();
		
		if ($api != "") {
			
			$vapi = explode ( ',', $api );
			foreach ( $vapi as $vz ) {
				$varC = $dd->get_api ( $vz );
				if ($var ['portlet_type'] == 'table') {
					$varC ['api_mode'] = 5;
				}
				$conn = unserialize ( $varC ['conn_params'] );
				$Arr [$vz] = $exe->execute_api ( $conn, $varC, $ti );
				
				$Arrses [$vz] = $Arr [$vz];
				unset ( $Arrses [$vz] ['data'] );
			}
		}
		
		$this->view->dataglobal  = $xxArr;
		
		if (isset ( $params['pparam']['debug'] ) && $params['pparam']['debug'] != "") {
		echo "<div class='col-md-12'>";
		echo "<p>Zend_Debug::dump(\$this->dataprocess) </p>";
		Zend_Debug::dump($Arr); 
		echo "<br>	<p>Zend_Debug::dump(\$this->dataglobal) </p>";
		Zend_Debug::dump($xxArr); 
		echo "<br>	<p>Zend_Debug::dump(\$this->datap) </p>";
		Zend_Debug::dump($var); 
		
		
		}
		
		$theme = Zend_Registry::get('theme'); 
		//echo (constant('APPLICATION_PATH')."/views/scripts/portlets/".$var['id'].".phtml"); // die();
		if(file_exists(constant('APPLICATION_PATH')."/views/theme/".$theme."/portlets/".$var['id'].".phtml")) {
			
		   	$newphtml = "portlets/".$var['id'].".phtml";
		} else {
			$newphtml = $var ['custom_view'];
			}
			//echo $newphtml; die();
		switch ($var ['portlet_type']) {
			case 'tableau' :
				//$Arrses=array();
				//Zend_Debug::dump($Arrses[30]['message']);die();
				$this->view->datap = $var;
				//Zend_Debug::dump($Arrses); die();
				//$dataform = array ();
				$ns->html [$var ['id']] = $Arrses;
				$this->view->dataprocess = $Arr;
				$this->view->datap = $var;
				//if(file_exists('prabaportlet/html.phtml'))
				($newphtml != "") ? $phtml = $newphtml : $phtml = 'prabaportlet/tableau.phtml';
				$this->renderScript ( $phtml );
			break;
				
			case 'html' :
				//$Arrses=array();
				//Zend_Debug::dump($Arrses[30]['message']);die();
				$this->view->datap = $var;
				//Zend_Debug::dump($Arrses); die();
				//$dataform = array ();
				$ns->html [$var ['id']] = $Arrses;
				$this->view->dataprocess = $Arr;
			
				//if(file_exists('prabaportlet/html.phtml'))
				($newphtml != "") ? $phtml = $newphtml : $phtml = 'prabaportlet/html.phtml';
				$this->renderScript ( $phtml );
				break;
			
			  case 'websocket' :
				//$Arrses=array();
				//Zend_Debug::dump($var);die();
				$ccd = new Model_Prabasystem();
				
				$var['content_attrs']['sconten'] = $ccd->get_a_tempsocket($var['content_attrs']['template']);
				//Zend_Debug::dump($var); die();
				
				
				
				//Zend_Debug::dump($Arrses); die();
				//$dataform = array ();
				$ns->websocket [$var ['id']] = $Arrses;
				$this->view->dataprocess = $Arr;
				$this->view->datap = $var;
				//if(file_exists('prabaportlet/html.phtml'))
				($newphtml != "") ? $phtml = $newphtml : $phtml = 'prabaportlet/websocket.phtml';
				$this->renderScript ( $phtml );
				break;
			
			case 'table' :
			
				$ns->table [$var ['id']] = $Arrses;
				$this->view->datap = $var;
				$this->view->dataprocess = $Arr;
				($newphtml != "") ? $phtml = $newphtml : $phtml = 'prabaportlet/table.phtml';
				$this->renderScript ( $phtml );
				break;
			
			case 'form' :
				
				#Zend_Debug::dump($var); die();	
				$this->view->datap = $var;
				$dataform = array ();
				if (count ( $var ['content_attrs'] ['field'] ) > 0) {
					
					$i = 0;
					$varCx = array ();
					$initval="";
					
					foreach ( $var ['content_attrs'] ['field'] as $v ) {
						$datafield = array ();
						if ($v ['initApi'] != "") {
							# Zend_Debug::dump($v['initApi']); die();
							$varCx = $dd->get_api ( $v ['initApi'] );
							$connx = unserialize ( $varCx ['conn_params'] );
							$datafield = $exe->execute_api ( $connx, $varCx );
							// Zend_Debug::dump($datafield);die();
							$initval = $datafield ['data'];
							unset ( $datafield ['data'] );
							$ns->datafield [$var ['id']] [$v ['initApi']] = $datafield;
						} elseif (isset ( $v ['initvalue'] ) && ! empty ( $v ['initvalue'] )) {
							$initval = $v ['initvalue'];
							
							// Zend_Debug::dump($initval); die();
						}
						//Zend_Debug::dump($initval); die();
						//$initarr=array();
						if (! is_array ( $initval )) {
						
							$pos = strpos ( $initval, '~' );
							if ($pos !== false) {
								$initarr = array ();
								// die("zzzzz");
								$vinit = explode ( '~', $initval );
								$x = 0;
								foreach ( $vinit as $vin ) {
									$qarin = explode ( ':', $vin );
									if(count($qarin)>1) {
										$initarr [$x] ['key'] = $qarin [0];
										$initarr [$x] ['value'] = $qarin [1];
									} else {
										$initarr [$x] ['key'] = $qarin [0];
										$initarr [$x] ['value'] = $qarin [0];	
									}	
									$x ++;
								}
							} else {
								$initarr="";
								}
						} else {
							$initarr = array ();
							foreach ( $initval as $kx => $vx ) {
								$initarr [$kx] ['key'] = $vx ['xkey'];
								$initarr [$kx] ['value'] = $vx ['xvalue'];
							}
							
						}
						$dataform [$v ['area']] [$i] = $v;
						$dataform [$v ['area']] [$i] ['datafieldinit'] = $initarr;
						
						$i ++;
					}
				}
				
				$var ['form-element'] = $dataform;
				$this->view->dataprocess = $Arr;
			
				$ns->form [$var ['id']] = $Arrses;
				$this->view->datap = $var;
			
				($newphtml != "") ? $phtml = $newphtml : $phtml = 'prabaportlet/form.phtml';
				$this->renderScript ( $phtml );
				
				break;
			
			case 'graphic' :
				$this->view->headScript ()->appendFile ( '/assets/core/plugins/flot/jquery.flot.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/plugins/flot/jquery.flot.resize.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/plugins/flot/jquery.flot.pie.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/plugins/flot/jquery.flot.stack.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/plugins/flot/jquery.flot.categories.min.js' );


				$this->view->headScript ()->appendFile ( '/assets/core/plugins/amcharts/amcharts/amcharts.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/plugins/amcharts/amcharts/serial.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/plugins/amcharts/amcharts/pie.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/plugins/amcharts/amcharts/radar.js' );
				
				$this->view->headScript ()->appendFile ( '/assets/core/plugins/amcharts/amcharts/themes/light.js' );
				
				$this->view->headScript ()->appendFile ( '/assets/core/plugins/amcharts/amcharts/themes/patterns.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/plugins/amcharts/ammap/ammap.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/plugins/amcharts/amstockcharts/amstock.js' );
				$this->view->datap = $var;
				$dataform = array ();
				
				$var ['form-element'] = $dataform;
				$this->view->dataprocess = $Arr;
				
				$ns->graphic [$var ['id']] = $Arrses;
				
				$this->view->datap = $var;
				
				($newphtml != "") ? $phtml = $newphtml : $phtml = 'prabaportlet/graph.phtml';
				$this->renderScript ( $phtml );
				
				break;
					
			case 'amgraphic' :
			
				$this->view->headScript ()->appendFile ( '/assets/core/global/plugins/amcharts/amcharts/amcharts.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/global/plugins/amcharts/amcharts/funnel.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/global/plugins/amcharts/amcharts/serial.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/global/plugins/amcharts/amcharts/gauge.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/global/plugins/amcharts/amcharts/pie.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/global/plugins/amcharts/amcharts/radar.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/global/plugins/amcharts/amcharts/xy.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/global/plugins/amcharts/amcharts/themes/dark.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/global/plugins/amcharts/amcharts/themes/black.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/global/plugins/amcharts/amcharts/themes/chalk.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/global/plugins/amcharts/amcharts/themes/light.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/global/plugins/amcharts/amcharts/themes/patterns.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/global/plugins/amcharts/ammap/ammap.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/global/plugins/amcharts/amstockcharts/amstock.js');
				$this->view->datap = $var;
				$dataform = array ();
			
				$var ['form-element'] = $dataform;
				$this->view->dataprocess = $Arr;
				$ns->graphic [$var ['id']] = $Arrses;
				$this->view->datap = $var;
				//Zend_Debug::dump($var); die();
				if(count($var['content_attrs']['api_id'])>0) {
					$nc = new Model_System();
					$vSat = array_unique($var['content_attrs']['api_id']);
					$cnt = 0;
					foreach ($vSat as $z) {
						$apidata[$z] = $nc->call_api($z);
						 if(count($apidata[$z]['data'])>$cnt) {
									 $cnt = count($apidata[$z]['data']);
						}
					
					}
				}
				
				$this->view->cnt =  $cnt;
				$this->view->datagraph =  $apidata;
				($newphtml != "") ? $phtml = $newphtml : $phtml = 'prabaportlet/amgraph.phtml';
				$this->renderScript ( $phtml );
				
				
			break;
			case 'bigchart' :
			case 'amchartpivot' :
		        $this->view->headLink()->appendStylesheet('/live.amcharts.com/vendor/jquery/css/ui-amcharts/jquery-ui-1.10.4.custom.css?d='.date("YmdHis"));
		        $this->view->headLink()->appendStylesheet('/live.amcharts.com/vendor/jquery/plugins/chosen/jquery.chosen.css?d='.date("YmdHis"));
		        $this->view->headLink()->appendStylesheet('/live.amcharts.com/vendor/jquery/plugins/spectrum/jquery.spectrum.css?d='.date("YmdHis"));
		        $this->view->headLink()->appendStylesheet('/live.amcharts.com/vendor/jquery/plugins/handsontable/jquery.handsontable.css?d='.date("YmdHis"));
		        $this->view->headLink()->appendStylesheet('/live.amcharts.com/vendor/jquery/plugins/handsontable/jquery.handsontable.removeRow.css?d='.date("YmdHis"));
		        $this->view->headLink()->appendStylesheet('/live.amcharts.com/vendor/bootstrap/css/bootstrap.css?d='.date("YmdHis"));
		        $this->view->headLink()->appendStylesheet('/live.amcharts.com/vendor/font-awesome/css/font-awesome.css?d='.date("YmdHis"));
		        $this->view->headLink()->appendStylesheet('//fonts.googleapis.com/css?family=Covered+By+Your+Grace');

		        $this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/amcharts.js?d='.date("YmdHis"));
		        $this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/funnel.js?d='.date("YmdHis"));
		        $this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/gauge.js?d='.date("YmdHis"));
		        $this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/pie.js?d='.date("YmdHis"));
		        $this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/radar.js?d='.date("YmdHis"));
		        $this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/serial.js?d='.date("YmdHis"));
		        $this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/xy.js?d='.date("YmdHis"));
		        $this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/themes/chalk.js?d='.date("YmdHis"));
		        $this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/themes/dark.js?d='.date("YmdHis"));
		        $this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/themes/light.js?d='.date("YmdHis"));
		        $this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/themes/patterns.js?d='.date("YmdHis"));
		        $this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/themes/black.js?d='.date("YmdHis"));

		        $this->view->headLink()->appendStylesheet('/live.amcharts.com/vendor/amcharts/plugins/export/export.css?d='.date("YmdHis"));
		        $this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/plugins/export/export.min.js?d='.date("YmdHis"));
		        $this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/plugins/dataloader/dataloader.min.js?d='.date("YmdHis"));
		        $this->view->headScript()->appendFile('/live.amcharts.com/vendor/amcharts/plugins/responsive/responsive.min.js?d='.date("YmdHis"));

		        $this->view->headScript()->appendFile('/live.amcharts.com/vendor/jquery/jquery-1.10.2.js?d='.date("YmdHis"));
		        $this->view->headScript()->appendFile('/live.amcharts.com/vendor/jquery/jquery-ui-1.10.4.js?d='.date("YmdHis"));
		        $this->view->headScript()->appendFile('/live.amcharts.com/vendor/jquery/plugins/router/jquery.router.js?d='.date("YmdHis"));
		        $this->view->headScript()->appendFile('/live.amcharts.com/vendor/jquery/plugins/layout/jquery.layout.js?d='.date("YmdHis"));
		        $this->view->headScript()->appendFile('/live.amcharts.com/vendor/jquery/plugins/chosen/jquery.chosen.js?d='.date("YmdHis"));
		        $this->view->headScript()->appendFile('/live.amcharts.com/vendor/jquery/plugins/spectrum/jquery.spectrum.js?d='.date("YmdHis"));
		        $this->view->headScript()->appendFile('/live.amcharts.com/vendor/jquery/plugins/handsontable/jquery.handsontable.js?d='.date("YmdHis"));
		        $this->view->headScript()->appendFile('/live.amcharts.com/vendor/jquery/plugins/handsontable/jquery.handsontable.removeRow.js?d='.date("YmdHis"));
		        $this->view->headScript()->appendFile('/live.amcharts.com/vendor/modernizr/modernizr-2.8.1.js?d='.date("YmdHis"));
		        $this->view->headScript()->appendFile('/live.amcharts.com/vendor/bootstrap/js/bootstrap.js?d='.date("YmdHis"));
		        $this->view->headScript()->appendFile('/live.amcharts.com/vendor/clipboard/clipboard.js?d='.date("YmdHis"));

				// $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/amcharts/ammap/ammap.js' );
				// $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/amcharts/amstockcharts/amstock.js');
				// $this->view->headScript()->appendScript("jQuery(document).ready(function() {
					// $('body').addClass('page-header-fixed page-sidebar-fixed page-footer-fixed');
					// App.init();
				// });");
				$this->view->headScript()->appendScript($var['content_attrs']['scripts']);
				// Zend_Debug::dump($var); die();
				$conf = $var["content_attrs"]['tpl'];
				// $conf = str_replace('"showHandOnHover": true,','"showHandOnHover": true,"showBalloon" : false,',$var["content_attrs"]['tpl']);
				$this->view->headScript()->appendScript($conf); 
				$this->view->datap = $var;
				
				($newphtml != "") ? $phtml = $newphtml : $phtml = 'prabaportlet/amchartpivot.phtml';
				$this->renderScript ( $phtml );
			break;
			case 'dynamicgraphic' :
				
				$this->view->headScript ()->appendFile ( '/assets/core/plugins/amcharts/amcharts/amcharts.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/plugins/amcharts/amcharts/funnel.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/plugins/amcharts/amcharts/serial.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/plugins/amcharts/amcharts/gauge.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/plugins/amcharts/amcharts/pie.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/plugins/amcharts/amcharts/radar.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/plugins/amcharts/amcharts/xy.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/plugins/amcharts/amcharts/themes/dark.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/plugins/amcharts/amcharts/themes/black.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/plugins/amcharts/amcharts/themes/chalk.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/plugins/amcharts/amcharts/themes/light.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/plugins/amcharts/amcharts/themes/patterns.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/plugins/amcharts/ammap/ammap.js' );
				$this->view->headScript ()->appendFile ( '/assets/core/plugins/amcharts/amstockcharts/amstock.js');
				$this->view->datap = $var;
				$dataform = array ();
			
				$var ['form-element'] = $dataform;
				$this->view->dataprocess = $Arr;
				$ns->graphic [$var ['id']] = $Arrses;
				$this->view->datap = $var;
				//Zend_Debug::dump($var); die();
				if(count($var['content_attrs']['api_id'])>0) {
					$nc = new Model_System();
					$vSat = array_unique($var['content_attrs']['api_id']);
					$cnt = 0;
					foreach ($vSat as $z) {
						$apidata[$z] = $nc->call_api($z);
						 if(count($apidata[$z]['data'])>$cnt) {
									 $cnt = count($apidata[$z]['data']);
						}
					
					}
				}
				
				$this->view->cnt =  $cnt;
				$this->view->datagraph =  $apidata;
				($newphtml != "") ? $phtml = $newphtml : $phtml = 'prabaportlet/dynamicgraph.phtml';
				$this->renderScript ( $phtml );
				
				
			break;
			
			
			
		}
		
		//$this->view->session = $ns;
	}
}
