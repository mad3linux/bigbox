<?php

/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <himawijaya@gmail.com>, 24.01.2016
 */

#require_once (APPLICATION_PATH) . '/../library/CMS/Workflow/wftaskinterface.php'; 

class PrabaqueueController extends Zend_Controller_Action {

    public function addactAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {

            $mdl = new Model_Crontab ();
            $update = $mdl->add_act($_POST, $identity->uid);
            if ($update ['result'] === true) {
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $update ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $result = array(
                    'retCode' => '11',
                    'retMsg' => $update ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function detailcronAction() {

        try {
            $auth = Zend_Auth::getInstance();
            $identity = $auth->getIdentity();
        } catch (Exception $e) {
            
        }
        //Zend_Debug::dump($identity);die();
        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($params);die();
        if (!isset($params['id']) || $params['id'] == '') {
            #	$this->_redirect('/messaging/managealerts');
        }
        $mdl_crn = new Model_Crontab();
        $cronalert = $mdl_crn->getAlertCrontab($params['id']);
        //Zend_Debug::dump($cronalert);die();
        if (!isset($cronalert['info'])) {
            #$this->_redirect('/messaging/managealerts');
        }
        $alert = array();
        $tmp1 = explode(" ", $cronalert['info']);
        $tmp2 = explode("|", $tmp1[1]);
        //Zend_Debug::dump($tmp2);die();
        $id = $tmp2[0];
        $user = $tmp2[1];
        $created_at = $tmp2[2];
        $tmp1 = explode(" ", $cronalert['cron']);
        $status = 'enable';
        $tmp2 = explode("/", $tmp1[5]);
        $config = $tmp1[0] . " " . $tmp1[1] . " " . $tmp1[2] . " " . $tmp1[3] . " " . $tmp1[4];
        $minute = $tmp1[0];
        $hour = $tmp1[1];
        $dayofmonth = $tmp1[2];
        $month = $tmp1[3];
        $dayofweek = $tmp1[4];
        if ($tmp1[0] == '#disable') {
            $status = 'disable';
            $tmp2 = explode("/", $tmp1[6]);
            $config = $tmp1[1] . " " . $tmp1[2] . " " . $tmp1[3] . " " . $tmp1[4] . " " . $tmp1[5];
            $minute = $tmp1[1];
            $hour = $tmp1[2];
            $dayofmonth = $tmp1[3];
            $month = $tmp1[4];
            $dayofweek = $tmp1[5];
        }
        //Zend_Debug::dump($tmp2);die();
        $file = $tmp2[7];
        #	$msg = file_get_contents("http://10.62.8.133/cronalert/msg/".str_replace(".sh",".php",$file)."?get=msg");
        $msg = json_decode($msg);
        //Zend_Debug::dump($msg);die();
        #	$target = file_get_contents("http://10.62.8.133/cronalert/msg/".str_replace(".sh",".php",$file)."?get=target");
        $target = json_decode($target, true);
        //Zend_Debug::dump($target);die();
        $alert = array(
            'id' => $id,
            'created by' => $user,
            'created at' => $created_at,
            'status' => $status,
            'message' => $msg->msg,
            'file' => $file,
            'config' => $config,
            'minute' => $minute,
            'hour' => $hour,
            'day of month' => $dayofmonth,
            'month' => $month,
            'day of week' => $dayofweek,
            'target' => $target
        );
        //Zend_Debug::dump($alert);die();
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->alert = $alert;
        $this->view->act = $mdl_crn->get_action();
        $this->view->cact = $mdl_crn->get_act_action($id);
        $this->view->varams = $params;
    }

    public function addcronAction() {

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        #Zend_De
        //Zend_Debug::dump($identity);die();
        //Zend_Debug::dump($_POST);die();
        if ($_POST && $_POST["machine_name"] != "") {
            $mdl_crn = new Model_Crontab();
            $mdl_api = new Model_Api();
            if (count($_POST["sch"]) > 0) {
                foreach ($_POST["sch"] as $k => $v) {
                    $cronconf = '';
                    switch ($v) {
                        case 'hourly':
                            $cronconf = $mdl_crn->getcroninterval($_POST["tgl1"] . ':00', $_POST["tgl2"] . ':00', 'hour');
                            break;
                        case 'daily':
                            $cronconf = $mdl_crn->getcroninterval($_POST["tgl1"] . ':00', $_POST["tgl2"] . ':00', 'day');
                            break;
                        case 'weekly':
                            $cronconf = $mdl_crn->getcroninterval($_POST["tgl1"] . ':00', $_POST["tgl2"] . ':00', 'week');
                            break;
                        case 'monthly':
                            $cronconf = $mdl_crn->getcroninterval($_POST["tgl1"] . ':00', $_POST["tgl2"] . ':00', 'month');
                            break;
                        default:
                            break;
                    }

                    if ($cronconf == '') {
                        break;
                    }
                    //bug::dump($cronconf.' - '.$v);die();
                    $intcron = $mdl_crn->addcronalert($identity->uid, $cronconf, $_POST["machine_name"], str_replace(" ", "_", $_POST["tgl1"]) . ':00', str_replace(" ", "_", $_POST["tgl2"]) . ':00', $v);
                }
            } else {
                if ($_POST['cron_formula'] != "") {
                    $cronconf = $_POST['cron_formula'];
                    $v = 'custom';
                    $intcron = $mdl_crn->addcronalert($identity->uid, $cronconf, $_POST["machine_name"], str_replace(" ", "_", $_POST["tgl1"]) . ':00', str_replace(" ", "_", $_POST["tgl2"]) . ':00', $v);
                }
            }
            //die();
            $this->_redirect('/prabaqueue/listcron');
        }

        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-toastr/toastr.min.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-toastr/toastr.min.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/ui-toastr.js');

        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datetimepicker/css/datetimepicker.css');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-daterangepicker/moment.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-daterangepicker/daterangepicker.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
    }

    public function addAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->_helper->layout->disableLayout();
        $result = array(
            'retCode' => '10',
            'retMsg' => 'Invalid Parameter',
            'result' => false,
            'data' => null
        );

        if ($_POST) {
            $mdl = new Model_Zprabaqueue ();
            #Zend_Debug::dump($_POST); die();
            // Zend_Debug::dump($el); die();
            $update = $mdl->add_action($_POST);

            if ($update) {

                $_POST['id'] = $update['id'];
                $result = array(
                    'retCode' => '00',
                    'retMsg' => $update ['message'],
                    'result' => true,
                    'data' => $_POST
                );
            } else {
                $result = array(
                    'retCode' => '11',
                    'retMsg' => $update ['message'],
                    'result' => false,
                    'data' => $_POST
                );
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function addqueueAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');

        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        // <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');

        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');

        $params = $this->getRequest()->getParams();

        $m = new Model_Prabasystem ();
        $m2 = new Model_Zprabapage ();

        // Zend_Debug::dump($m2->list_page());die();

        if (!isset($params ['row'])) {
            $params ['row'] = 1;
        }
        $this->view->lay = $m->get_layout();
    }

    public function deletallAction() {
        $params = $this->getRequest()->getParams();
        $cc = new Model_Zprabaqueue();
        $data = $cc->delete_running_pid($params['id']);

        $this->_redirect('/prabaqueue/list/msg/' . urlencode($data['message']));
    }

    public function runningAction() {
        $params = $this->getRequest()->getParams();
        $cc = new Model_Zprabaqueue();
        $data = $cc->set_running_pid($params['id']);

        $this->_redirect('/prabaqueue/list/msg/' . urlencode($data['message']));
    }

    public function stopAction() {
        $params = $this->getRequest()->getParams();
        $cc = new Model_Zprabaqueue();
        $data = $cc->stop_running_pid($params['id']);

        $this->_redirect('/prabaqueue/list/msg/' . urlencode($data['message']));
    }

    public function listAction() {

        #	echo APPLICATION_PATH;  die();


        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');

        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/table-ajax.js');

        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/ui-extended-modals.js');

        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');

        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	

        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');

        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');

        // $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/table-ajax.js');

        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/ui-extended-modals.js');

        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');

        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	

        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');

        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $cc = new Model_Zprabaqueue();
        $data = $cc->get_running_temp();
        $data3 = $cc->get_running_pid();
        $data2 = $cc->get_workers();
        $this->view->data = $data;
        $this->view->data2 = $data2;
        $this->view->data3 = $data3;
    }

    public function listcronAction() {
        try {
            $auth = Zend_Auth::getInstance();
            $identity = $auth->getIdentity();
        } catch (Exception $e) {
            
        }
        //Zend_Debug::dump($identity);die();
        $mdl_crn = new Model_Crontab();

        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($params);die();
        if (isset($params['id']) && $params['id'] != '' && isset($params['act']) && $params['act'] != '') {
            $check = $mdl_crn->checkIDCrontab($params['id']);
            //Zend_Debug::dump($check);die();
            if ($check) {
                switch ($params['act']) {
                    case 'deleted':
                        $check = $mdl_crn->deleteCrontab($params['id']);
                        break;
                    case 'disabled':
                        $check = $mdl_crn->deactivateCrontab($params['id']);
                        break;
                    case 'enabled':
                        $check = $mdl_crn->activateCrontab($params['id']);
                        break;
                    default:
                        break;
                }
            }
        }

        $cronalert = $mdl_crn->getallAlertCrontab();
        //Zend_Debug::dump($cronalert);die();
        $alert = array();
        foreach ($cronalert as $k => $v) {
            $tmp1 = explode(" ", $v['info']);
            //Zend_Debug::dump($tmp1);die();
            $tmp2 = explode("|", $tmp1[1]);
            //Zend_Debug::dump($tmp2);die();
            $id = $tmp2[0];
            $user = $tmp2[1];
            $created_at = $tmp2[2];
            $start = $tmp2[3];
            $end = $tmp2[4];
            $interval = $tmp2[5];
            $tmp1 = explode(" ", $v['cron']);
            $status = 'enable';
            $tmp2 = explode("/", $tmp1[5]);
            //$config = $tmp1[0]." ".$tmp1[1]." ".$tmp1[2]." ".$tmp1[3]." ".$tmp1[4];
            //$minute = $tmp1[0];
            //$hour = $tmp1[1];
            //$dayofmonth = $tmp1[2];
            //$month = $tmp1[3];
            //$dayofweek = $tmp1[4];
            if ($tmp1[0] == '#disable') {
                $status = 'disable';
                $tmp2 = explode("/", $tmp1[6]);
                //$config = $tmp1[1]." ".$tmp1[2]." ".$tmp1[3]." ".$tmp1[4]." ".$tmp1[5];
                //$minute = $tmp1[1];
                //$hour = $tmp1[2];
                //$dayofmonth = $tmp1[3];
                //$month = $tmp1[4];
                //$dayofweek = $tmp1[5];
            }
            //Zend_Debug::dump($tmp2);die();
            $file = $tmp2[7];
            //$msg = file_get_contents("http://10.62.8.133/cronalert/msg/".str_replace(".sh",".php",$file)."?get=msg");
            //$msg = json_decode($msg);
            //Zend_Debug::dump($msg);die();
            //$target = file_get_contents("http://10.62.8.133/cronalert/msg/".str_replace(".sh",".php",$file)."?get=target");
            //$target = json_decode($target,true);
            //Zend_Debug::dump($target);die();
            $alert[$id] = array(
                'id' => $id,
                'user' => $user,
                'created_at' => $created_at,
                'status' => $status,
                'start' => $start,
                'end' => $end,
                'interval' => $interval,
                //'config'=>$config,
                //'minute'=>$minute,
                //'hour'=>$hour,
                //'dayofmonth'=>$dayofmonth,
                //'month'=>$month,
                //'dayofweek'=>$dayofweek,
                'file' => $file,
                    //'msg'=>$msg->msg,
                    //'target'=>$target
            );
            //Zend_Debug::dump($alert[$id]);die();
        }
        //Zend_Debug::dump($alert);die();
        $this->view->alert = $alert;
    }

}
