<?php

/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <himawijaya@gmail.com>, 24.01.2016
 */

require_once (APPLICATION_PATH) . '/../library/CMS/Workflow/wftaskinterface.php';

class PrabawfController extends Zend_Controller_Action {

    public function runningallAction() {


        $params = $this->getRequest()->getParams();
        $i = 0;
        foreach ($params['awal'] as $v) {
            $input[$v] = $params['akhir'][$i];
            $i++;
        }
        //Zend_Debug::dump($params); die();
        $new = new Model_Zprabawf();
        $out = $new->get_wf_process($params['id'], $input, true, $params['debug']);

        echo "<div style='font-size:10px;color:red;'>";
        //echo "<div>ProsesId :".$data['step'][$next][$step]['process_to']."</div>";
        echo "<div style='float:left;width:50%'>";
        //echo "From ProcessId $next";
        echo "<span style='margin-left:5px;font-weight:bold'>INPUT</span>";
        Zend_Debug::dump($input);
        //Zend_Debug::dump($vdata);
        echo "</div>";
        echo "<div style='float:right;width:50%'>";
        echo "<span style='margin-left:5px;font-weight:bold'>OUTPUT</span>";
        Zend_Debug::dump($out);

        echo "</div>";
        echo "</div>";
        die();
    }

    public function runningstepAction() {
        $params = $this->getRequest()->getParams();
        $i = 0;
        foreach ($params['awal'] as $v) {
            $input[$v] = $params['akhir'][$i];
            $i++;
        }
        $c = new Model_Zprabawf();
        $data = $c->get_wf_process_debug($params['id']);
        $ret = $c->get_running($params['rid']);
        if (count($ret) <= 0) {

            $vAR = array();
            $cnt = count($data['step'][$data['data']['start']]);
            if ($cnt > 1) {
                for ($i = 1; $i < $cnt; $i++) {
                    $vAR[$data['data']['start']][$i] = 0;
                }

                $ser = serialize($vAR);
            }
            $inCUrrpid[$data['data']['start']][0] = 0;
            $vdata = $c->exec_process_debug($data['data']['start'], $input, $data['data'], $data['step'], $result);
            $next = $data['data']['start'];
            $step = 0;
            $datax = array(
                'idtime' => $params['rid'],
                'act_id' => $params['id'],
                'pid' => $data['data']['start'],
                'v_input' => serialize($input),
                'v_output' => serialize($vdata[$data['step'][$next][$step]['process_to']]),
                'curr_pid' => serialize($inCUrrpid),
                'branch_act' => $ser
            );
            $c->insert_running($datax);
            $text = serialize($inCUrrpid);
            $c->update_current_running($text, $params['rid']);
        } else {
            $cstep = (unserialize($ret[0]['curr_pid']));
            $vg = unserialize($ret[0]['branch_act']);
            $input = unserialize($ret[0]['v_output']);
            foreach ($cstep as $k => $v) {
                foreach ($v as $kkk => $vvv) {
                    $next = $data['step'][$k][$kkk]['process_to'];
                    $current = $k;
                    $currentstepkey = $kkk;
                }
                if (count($data['step'][$k]) > 1) {
                    foreach ($data['step'][$k] as $zz => $yy) {
                        if ($zz != 0) {
                            $vg[$k][$zz] = 0;
                        }
                    }
                    $dataser = serialize($vg);
                    if (!isset($vg[$k][$currentstepkey])) {
                        $c->update_branch($dataser, $params['rid']);
                    }
                }
            }
            $data['data']['start'] = $next;
            $inCUrrpid[$data['data']['start']][0] = 0;
            $step = 0;
            if ($data['data']['end'] == $current) {
                //die("TAMAT");
                $inCUrrpid = array();
                $z = 0;
                foreach ($vg as $k => $v) {

                    if (count($v) > 0) {
                        foreach ($v as $kk => $vv) {
                            if ($vv == 0) {
                                $next = $k;
                                $data['data']['start'] = $next;
                                $step = $kk;
                            }
                        }
                    }
                    $z++;
                }
                unset($vg[$next][$step]);
                if (count($vg[$next][$step]) == 0) {
                    //	unset($vg[$next]);
                }
                if ($data['data']['start'] != "") {
                    $inCUrrpid[$data['data']['start']][$step] = 0;
                }
                $dataser = serialize($vg);
                $c->update_branch($dataser, $params['rid']);
                $ret = $c->get_running($params['rid']);
            }


            if (count($inCUrrpid) == 0) {
                die("end Workflow");
            }

            if (count($inCUrrpid) != 0) {
                $vdata = $c->exec_process_debug($data['data']['start'], $input, $data['data'], $data['step'], $result, $step);
                $datay = array(
                    'idtime' => $params['rid'],
                    'act_id' => $params['id'],
                    'pid' => $data['data']['start'],
                    'v_input' => serialize($input),
                    'v_output' => serialize($vdata[$data['step'][$next][$step]['process_to']]),
                    'curr_pid' => serialize($inCUrrpid),
                    'branch_act' => $ret[0]['branch_act']
                );

                $c->insert_running($datay);
                $text = serialize($inCUrrpid);
                $c->update_current_running($text, $params['rid']);
            }
        }

        echo "<div style='font-size:10px;color:red;'>";
        echo "<div>ProsesId :" . $data['step'][$next][$step]['process_to'] . "</div>";
        echo "<div style='float:left;width:50%'>";
        //echo "From ProcessId $next";
        echo "<span style='margin-left:5px;font-weight:bold'>INPUT</span>";
        Zend_Debug::dump($input);
        //Zend_Debug::dump($vdata);
        echo "</div>";
        echo "<div style='float:right;width:50%'>";
        echo "<span style='margin-left:5px;font-weight:bold'>OUTPUT</span>";
        Zend_Debug::dump($vdata[$data['step'][$next][$step]['process_to']]);
        echo "</div>";
        echo "</div>";
        die();
    }

    public function runningAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        $out = '<script>
		function running_submit() {
			$.ajax({
				url: "/prabawf/runningstep/id/' . $params['id'] . '/rid/' . mktime() . '",
				type:"POST",
				data: $("#running").serialize(),
				dataType: "html",	
				beforeSend: function() {
				   
			},
			success: function(data) {
				$("#debug").html(data);
			},
			timeout:9000,
			error:function(){
				
			},
		});
	
	
	}
	function running_submitall() {
			$.ajax({
				url: "/prabawf/runningall/id/' . $params['id'] . '/rid/' . mktime() . '",
				type:"POST",
				data: $("#running").serialize(),
				dataType: "html",	
				beforeSend: function() {
				   
			},
			success: function(data) {
				$("#debug").html(data);
			},
			timeout:9000,
			error:function(){
				
			},
		});
	
	
	}
	function running_debug() {
			$.ajax({
				url: "/prabawf/runningall/id/' . $params['id'] . '/rid/' . mktime() . '/debug/1",
				type:"POST",
				data: $("#running").serialize(),
				dataType: "html",	
				beforeSend: function() {
				   
			},
			success: function(data) {
				$("#debug").html(data);
			},
			timeout:9000,
			error:function(){
				
			},
		});
	
	
	}
	';

        $out .= ' function comp0(me) {
		var val = $(me).val();
		//alert(val);
		if(val=="map") {
		$( "#mapping" ).show();
		} else {
		$( "#mapping" ).hide();
		}
	
	}
    function addPIC(me) {
		$("#tutorial tbody").append("<tr><td><input name=awal[]></td><td><input name=akhir[]></td></tr>");
		
    }
    
    
    </script>
   
	<div>
	<div style="margin: 0px 0px 0px 10px; float: l
	<div style="margin: 0px 10px 0px 0px; float: right;">&nbsp;</div>
	<div class="active"><div class="maestro_task_edit_tab_close" style="float: right;"><div class="t"><div class=""><div class="r"><div class="l"><div class="bl-cl"><div class="br-cl"><div class="tl-cl"><div class="tr-cl">
	<a href="#" onclick="(function($) { $.modal.close(); disable_ajax_indicator(); select_boxes = []; })(jQuery); return false;"><img src="/assets/core/plugins/prabawf/images/admin/close.png"></a>
  </div></div></div></div></div></div></div></div></div></div>

  <div style="clear: both;"></div>
  <div class="maestro_task_edit_panel">
    <div class="t"><div class="b"><div class="r"><div class="l"><div class="bl-wht"><div class="br-wht"><div class="tl-wht"><div class="tr-wht">
    
      <form id="running" method="post" ">
        <input type="hidden" name="task_class" value="' . $task_class . '">
        <input type="hidden" name="template_data_id" value="' . $id . '">
		  <div id="task_edit_main">
          <div style="float: none;" class="maestro_tool_tip maestro_taskname"><div class="t"><div class="b"><div class="r"><div class="l"><div class="bl-bge"><div class="br-bge"><div class="tl-bge"><div class="tr-bge">
          
          <table width="100%">
                  <tbody>
                 
                 <tr id="mapping">
                    <td style="vertical-align: top;">Input</td>
                    <td style="width: 70%;">
                    
                    <table  id="tutorial" style="border: 1px solid green;" width= 100% >
                    <thead>
                    <tr><td>Array Key</td><td>Array Value</td></tr>
                    </thead>
                    <tbody>';

        $out .= '<tr><td><input name=awal[]></td><td><input name=akhir[]></td></tr>';

        $out .= '</tbody>
                     </table>
                     <button type="button" class="btn grey" onclick="addPIC(); return false;" data-act="addPIC" data-id="1" style="padding: 5px;">Add</button>
                   </td>
                  </tr>
                </tbody></table>';

        $out .= '</div></div></div></div></div></div></div></div></div><br />

          <br></br>

        </div>
        <div class="maestro_task_edit_save_div"><input class="form-submit" type="button" onclick="running_submit();" value="Debug per Step">&nbsp;&nbsp;<input class="form-submit" type="button" onclick="running_submitall();" value="Running">&nbsp;&nbsp;<input class="form-submit" type="button" onclick="running_debug();" value="Running w Debug"></div>

      </form>
     
	</div></div></div></div></div></div></div></div><pre>
	  <div id="debug" style="background-color:#ADD8E6; height:50px;">
	  
	  
      </div>
      </pre>
      
  </div>
</div>';

        $records = array('out' => array('html' => $out));
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }

    public function init() {
        $ajaxContext = $this->_helper->getHelper('contextSwitch');
        $ajaxContext->addActionContext('ajaxer', 'json')->initContext();
        $ajaxContext->initContext();
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->initView();
    }

    public function ajaxer4Action() {
        $zz = new CMS_General();
        $params = $this->getRequest()->getParams();
        $id = $params['type'] . "_getgroups_" . $params['id'];
        $front = array(
            'lifetime' => NULL,
            'automatic_serialization' => true
        );
        $back = array('cache_dir' => APPLICATION_PATH . '/../cache/');
        $ncache = Zend_Cache::factory('Core', 'File', $front, $back);
        try {
            $vdata = $ncache->load($id);
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
        $cc = new CMS_General();
        if (!is_array($vdata)) {
            $odata = $cc->objectToArray($vdata->result);
        } else {
            $odata = $vdata;
        }

        switch ($params['type']) {

            case 'telegram':

                foreach ($odata as $v) {
                    $data[$v] = $v;
                }
                break;

            case 'wa':

                foreach ($odata as $v) {
                    $data[$v['id']] = $v['subject'];
                }

                break;
        }

        $group = $zz->__unserialize($params['group']);
        foreach ($data as $k => $d) {
            $sel = "";
            echo "<input ";
            if (in_array($k, $group)) {
                $sel = "checked";
            }

            echo "type=checkbox " . $sel . " name=group[] value='" . $d . "' />" . $d . "<br>";
        }
        echo "<br>";
        die();
    }

    public function ajaxer3Action() {
        $id = $this->getRequest()->getParam('id');
        $acc = $this->getRequest()->getParam('acc');
        $cc = new Model_Zprahu();
        switch ($id) {
            case 'tl' :
            case 'telegram' :
                $data = $cc->get_account('telegram');

                break;

            case 'wa' :


                $data = $cc->get_account('wa');

                break;

            case 'email' :

                $data = $cc->get_account('email');

                break;


            case 'gtalk' :
                $data = $cc->get_account('gtalk');

                break;
        }


        echo '<select onchange="comp1(this)" name="account" id="account">
            	<option value="">--choose api--</option>';

        foreach ($data as $k => $d) {
            $sel = "";
            if ($acc == $k) {
                $sel = "selected";
            }
            echo '<option ' . $sel . ' value="' . $k . '">' . $d . '</option>';
        }
        echo '</select>';
        die();
    }

    public function getcustom2Action() {
        $params = $this->getRequest()->getParams();
        $this->_helper->layout->disableLayout();
        $mm = new Model_Prabasystem ();
        $dat = $mm->get_method_class_model($params ['id']);
        $out = "<option></option>";
        foreach ($dat as $k => $v) {
            $out .= "<option value='" . $v . "'>" . $v . "</option>";
        }

        echo $out;
        die();
    }

    public function getcustomAction() {
        $this->_helper->layout->disableLayout();
        $mm = new Model_Prabasystem ();
        $dat = $mm->get_models_class();
        $params = $this->getRequest()->getParams();
        $out = '<td style="vertical-align: top;">Class/Model</td><td>
                    
		<select onchange=changeModel(this)  name="model" id="model"  >
											<option></option>';
        foreach ($dat as $k => $v) {
            $out .= "<optgroup label='" . $k . "'>";
            foreach ($v as $vv) {
                $sel = "";
                if ($params['xid'] == $vv) {
                    $sel = "selected";
                }
                $out .= "<option $sel value='" . $vv . "'>" . $vv . "</option>";
            }
            $out .= "</optgroup>";
        }
        $out .= '</select><br><br><select name="method" id="method"  >';
        if (isset($params['xid']) && $params['xid'] != "") {

            $mm = new Model_Prabasystem ();
            $dat = $mm->get_method_class_model($params ['xid']);
            $out .= "<option></option>";
            foreach ($dat as $k => $v) {
                $sel = "";
                if ($params['xid2'] == $v) {
                    $sel = "selected";
                }
                $out .= "<option $sel value='" . $v . "'>" . $v . "</option>";
            }
        }

        $out .='</select></td>';
        echo $out;
        die();
    }

    public function ajaxer2Action() {
        $id = $this->getRequest()->getParam('id');
        $cc = new Model_Zprabawf();
        $dat = $cc->get_api_by_conn_id($id);
        $this->_helper->layout->disableLayout();

        echo '<select name="api" id="api">
            	<option value="">--choose api--</option>';
        foreach ($dat as $d) {
            echo '<option value="' . $d['id'] . '">' . $d['id'] . '-[' . substr($d['sql_text'], 0, 30) . ']</option>';
        }
        echo '</select>';
        die();
    }

    public function ajaxerAction() {
        $ctype = $this->getRequest()->getParam('ctype');

        $taskid = $this->getRequest()->getParam('taskid');
        $tempid = $this->getRequest()->getParam('tempid');
        $action = $this->getRequest()->getParam('act');
        //Zend_Debug::dump($this->getRequest()->getParam('ltype')); die();
        if ($ctype == "PrabaOperator") {
            $ti = new $ctype($taskid, $tempid, $this->getRequest()->getParam('ltype'));
        } else {
            $ti = new $ctype($taskid, $tempid);
        }
        $out = ($ti->$action());

        $this->view->out = $out;
    }

    public function generateAction() {

        $params = $this->getRequest()->getParams();
        $c = new Model_Zprahu();
        $data = $c->get_data_by3($params['ch'], $params['type'], $params['id']);

        $vdata = json_decode(file_get_contents(str_replace(" ", "", trim($data['url_api']))));

        Zend_Debug::dump($vdata);
        die();
        $id = $params['ch'] . "_" . $params['type'] . "_" . $params['id'];

        $front = array(
            'lifetime' => NULL,
            'automatic_serialization' => true
        );

        $back = array('cache_dir' => APPLICATION_PATH . '/../cache/');
        $ncache = Zend_Cache::factory('Core', 'File', $front, $back);

        $ncache->save($vdata, $id, array(
            'prahu'));




        die("ok");
    }

    public function displayAction() {
  $params = $this->getRequest()->getParams();
        
         $this->view->varams = $params;
        //$this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery.numeric.js' );
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/prabawf/css/metadata.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/prabawf/metadata/css/basic.css');
      
        //Zend_Debug::dump($params); die();
        $datatemp = null;
        $task = array();
        $template_id = $this->getRequest()->getParam('id');
        $cc = new Model_Prabasystem();
        $this->view->act = $cc->get_a_action($template_id);
        $c = new CMS_Workflow_wfinterface($template_id);
        $var = $c->displayPage();
        //Zend_Debug::dump($var); die();
        $this->view->temp = $datatemp;
        $this->view->cwf = $var;
        $c3 = new Model_Zprabawf();
        $res = $c3->get_process($template_id);

        foreach($res as $rec) {
            // Zend_Debug::dump($rec);
            $task_type = $rec['class_type'];
            $task_class = 'Praba' . $task_type;
            #echo $task_class; 
            if(class_exists($task_class)) {
                $ti = new $task_class($rec['pid']);
            } else {
                //unknown classe
            }
            $restask = $c3->get_process_by_pid($rec['pid']);
            $task[$rec['pid']]['api'] = array();
            if($rec['class_type'] == 'Task') {
                if($rec['p_type'] == 'action') {
                    $task[$rec['pid']]['api'] = $c3->get_action_by_id_with_conn($rec['ext_id']);
                } else {
                    $task[$rec['pid']]['api'] = $c3->get_api_by_id_with_conn($rec['ext_id']);
                }
            }
            $task_class = 'Praba' . $restask['class_type'];
            $task[$rec['pid']]['res'] = $restask;
            $task[$rec['pid']]['ti'] = $ti;
            $task[$rec['pid']]['task_class'] = $task_class;
        }
        // die("ss");
        $this->view->task = $task;
        $this->view->temp = $template_id;
        $this->view->menu = $c->getContextMenu();
        //die("xx");
    }

}
