<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
use Enzim\Lib\TikaWrapper\TikaWrapper;
class PrabigtechController extends Zend_Controller_Action{


public function init (){    

}

public function assignpicAction()
{
		$params = $this->getRequest ()->getParams ();
		
		$df = new Model_Zdash();
		//$vatda = $df->create_content(true);
		$forms = $df->getdataform($params['d']);
		//Zend_Debug::dump($form); die("zzzzzzzz");
		//$cc = new Model_Zprabareportdoc();
		$dd = new Model_Zdash();
		//$data = $cc->getall_cotents_by_cid($params['d']);
		//Zend_Debug::dump($data); die();
		
		$ee = new Model_Zprabadoc();
		$files = $ee->get_main_file($params['d']);
		//Zend_Debug::dump($data); die("ss");

		$this->view->files= $files;

		
		$rows = $dd->get_solution_by_id($params['d']);
		#Zend_Debug::dump($list); die();
		$this->view->data =$data;	
		$this->view->rows =$rows;	
		$dd = new Model_Zdash();
		
		
		
		$list = $dd->get_all_content($params['d']); 
		$this->view->alist =$list;	
		$this->view->varams =$params;
		$this->view->forms =$forms;	
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery.sparkline.min.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/fullcalendar/fullcalendar/fullcalendar.min.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/scripts/form-components.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/scripts/form-validation.js' );
		
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/select2/select2_metro.css' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/select2/select2.min.js' );

	//die("debug");	
}	

public function getpictureAction () {
	
	$params = $this->getRequest ()->getParams ();
	list ($first, $last) = explode(' ', $params['id'], 2);
	$initial =  strtoupper($first[0]) ."". strtoupper($last[0]); 
	$this->_helper->layout->disableLayout ();
	$my_img = imagecreate( 45, 45 );
	$background = imagecolorallocate( $my_img, 127, 127, 127 );
	$text_colour = imagecolorallocate( $my_img, 255, 255, 0 );
	//$line_colour = imagecolorallocate( $my_img, 128, 255, 0 );
	imagestring( $my_img, 80, 10, 10, $initial, $text_colour );
	imagesetthickness ( $my_img, 15 );
	//imageline( $my_img, 30, 45, 165, 45, $line_colour );

	header ( "Access-Control-Allow-Origin: *" );
	header ( 'Content-type: image/png' );
	imagepng( $my_img );
	imagecolordeallocate( $line_color );
	imagecolordeallocate( $text_color );
	imagecolordeallocate( $background );
	imagedestroy( $my_img );
	die();
}

public function postchatAction()
{
	try {
			$authAdapter = Zend_Auth::getInstance ();
			$identity = $authAdapter->getIdentity ();
		} catch ( Exception $e ) {
		}
	
		$this->_helper->layout->disableLayout ();
		$result = array (
				'retCode' => '10',
				'retMsg' => 'Invalid Parameter',
				'result' => false,
				'data' => null 
		);
		
		if ($_POST) {
			
			//Zend_Debug::dump($_POST); die();
			$cc = new Model_Zdash ();
			
			$data['uid'] = $identity->uid;
			$data['msg'] =$_POST['msg'];
			$data['id'] =$_POST['idsol'];
			$update	 =  $cc->post_chat($data);
			
			if ($update) {
				$result = array (
						'retCode' => '00',
						'retMsg' => "ok",
						'result' => true,
						'data' => $_POST 
				);
			} else {
				$result = array (
						'retCode' => '11',
						'retMsg' => $update ['message'],
						'result' => false,
						'data' => $_POST 
				);
			}
		}
		
		header ( "Access-Control-Allow-Origin: *" );
		header ( 'Content-Type: application/json' );
		echo json_encode ( $result );
		die ();
	

}
public function chatAction()
{
	$params = $this->getRequest ()->getParams ();
	$c = new Model_Zdash();
	if($params['id']!=""){ 
		$data = $c->get_chat($params['id']);	
	} else {
		$data = $c->get_chat();

	}
	#Zend_Debug::dump($data); die();
	$chat='<ul class="chats">';
	$i=1;
	foreach ($data as $dd) {
		$class = "in";
		if($i%2==0) {
			$class = "out";
		}
		
		($dd['picture']!="")? $link = $dd['picture']: $link  = "/prabigtech/getpicture/id/".$dd['fullname'];
		
			
		$chat .= '<li class="'.$class.'">
					<img class="avatar img-responsive" alt="" src="'.$link.'"/>
					<div class="message">
						<span class="arrow">
						</span>
						<a href="#" class="name">'.$dd['fullname'].'</a>
						<span class="datetime">
							'.$dd['sdate'].'	
						</span>
						<span class="body">'.$dd['msg'].'
						</span>
					</div>
				</li>';

	
	$i++;
	}
	$chat .= '</ul>';
	echo $chat;
	
	die();
	
}

public function wrapperAction()
{
$cc = new Model_Zprabadata();

$cc->getall_docs();
	
}	
public function explorerAction()
{
		

	//die("debug");	
}	

public function dashlevel2Action()
{
		$params = $this->getRequest ()->getParams ();
		$cc = new Model_Zprabareportdoc();
		$dd = new Model_Zdash();
		$data = $cc->getall_cotents_by_cid($params['d']);
		
		
			$ee = new Model_Zprabadoc();
			$files = $ee->get_main_file($params['d']);
			//Zend_Debug::dump($data); die("ss");
		
			$this->view->files= $files;
		
		
		$rows = $dd->get_solution_by_id($params['d']);
		#Zend_Debug::dump($list); die();
		$this->view->data =$data;	
		$this->view->rows =$rows;	
		$dd = new Model_Zdash();
		
		
		$this->view->pic =$dd->get_pic_solution_by_id($params['d']);
		#Zend_Debug::dump($this->view->pic); die();
		$this->view->allpic =$dd->get_pic_solution_all();
		
		$list = $dd->get_all_content($params['d']); 
		$this->view->alist =$list;	
		$this->view->varams =$params;
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery.sparkline.min.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/fullcalendar/fullcalendar/fullcalendar.min.js' );
			$this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/scripts/form-components.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/scripts/form-validation.js' );
		
			$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/select2/select2_metro.css' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/select2/select2.min.js' );

	//die("debug");	
}	
public function dashinAction()
{
		$params = $this->getRequest ()->getParams ();
		$cc = new Model_Zprabareportdoc();
		$data = $cc->getall_cotents_by_cid($params['idsol']);
		//Zend_Debug::dump($data); die("ss");
		$this->view->data =$data;	
		
		$dd = new Model_Zdash();
		$list = $dd->get_all_content($params['id']); 
		$this->view->alist =$list;	
		$this->view->varams =$params;
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery.sparkline.min.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/fullcalendar/fullcalendar/fullcalendar.min.js' );
		

	//die("debug");	
}	
public function dash1Action()
{
		$params = $this->getRequest ()->getParams ();
		$cc = new Model_Zprabareportdoc();
		$data = $cc->getall_cotents_by_cid($params['id']);
		//Zend_Debug::dump($data); die("ss");
		$this->view->data =$data;	
		
		$dd = new Model_Zdash();
		$list = $dd->get_all_content($params['id']); 
		$all = $dd->get_all_solution(); 
		$longest = $dd->get_all_longest(); 
		
		$pie['type_solusi'] =  $dd->get_chart_type_solusi();
		$pie['type_phase'] =  $dd->get_chart_phase_type();
		$pie['type_closed'] =  $dd->get_chart_closed_solution();
		
	#	Zend_Debug::dump(array_keys($pie['type_solusi']));die();
		
		$this->view->pie =$pie;	
		$this->view->alist =$list;	
		$this->view->longest =$longest;	
		$this->view->all =$all;	
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery.sparkline.min.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/fullcalendar/fullcalendar/fullcalendar.min.js' );
		
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/flot/jquery.flot.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/flot/jquery.flot.resize.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/flot/jquery.flot.pie.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/flot/jquery.flot.stack.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/flot/jquery.flot.crosshair.js' );

}	

public function dashAction()
{
		$params = $this->getRequest ()->getParams ();
		$cc = new Model_Zprabareportdoc();
		$data = $cc->getall_cotents_by_cid($params['id']);
		//Zend_Debug::dump($data); die("ss");
		$this->view->data =$data;	
		
		$dd = new Model_Zdash();
		$list = $dd->get_all_content($params['id']); 
		$this->view->alist =$list;	
		
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery.sparkline.min.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/fullcalendar/fullcalendar/fullcalendar.min.js' );
		

	//die("debug");	
}	
public function addpicAction() {
		try {
			$authAdapter = Zend_Auth::getInstance ();
			$identity = $authAdapter->getIdentity ();
		} catch ( Exception $e ) {
		}
		$this->_helper->layout->disableLayout ();
		$result = array (
				'retCode' => '10',
				'retMsg' => 'Invalid Parameter',
				'result' => false,
				'data' => null 
		);
		
		if ($_POST) {
			#Zend_Debug::dump($_POST); die();
			$mdl = new Model_Zdash ();
			$update = $mdl->add_pic ( $_POST );
			
			if ($update ['result'] === true) {
				$result = array (
						'retCode' => '00',
						'retMsg' => $update ['message'],
						'result' => true,
						'data' => $_POST 
				);
			} else {
				$result = array (
						'retCode' => '11',
						'retMsg' => $update ['message'],
						'result' => false,
						'data' => $_POST 
				);
			}
		}
		
		header ( "Access-Control-Allow-Origin: *" );
		header ( 'Content-Type: application/json' );
		echo json_encode ( $result );
		die ();
	}
	
public function ajaxgetamAction() {
		try {
			$authAdapter = Zend_Auth::getInstance ();
			$identity = $authAdapter->getIdentity ();
		} catch ( Exception $e ) {
		}
		$params = $this->getRequest ()->getParams ();
		$cc = new Model_Zdash();
		$this->_helper->layout->disableLayout ();
		$result = $cc->get_am($params['q'],$params['segmen'] );
		
		
		header ( "Access-Control-Allow-Origin: *" );
		header ( 'Content-Type: application/json' );
		// echo json_encode($records);
		$jsonp_callback = isset ( $_GET ['callback'] ) ? $_GET ['callback'] : null;
		
		$json = json_encode ( $result );
		print $jsonp_callback ? "$jsonp_callback($json)" : $json;
		
		die ();
	
}	



public function ajaxgetcustomerAction() {
		try {
			$authAdapter = Zend_Auth::getInstance ();
			$identity = $authAdapter->getIdentity ();
		} catch ( Exception $e ) {
		}
		$params = $this->getRequest ()->getParams ();
		$cc = new Model_Zdash();
		$this->_helper->layout->disableLayout ();
		$result = $cc->get_customer($params['q'],$params['segmen'] );
		
		
		header ( "Access-Control-Allow-Origin: *" );
		header ( 'Content-Type: application/json' );
		// echo json_encode($records);
		$jsonp_callback = isset ( $_GET ['callback'] ) ? $_GET ['callback'] : null;
		
		$json = json_encode ( $result );
		print $jsonp_callback ? "$jsonp_callback($json)" : $json;
		
		die ();
	
	}	
public function reportingAction()
{
	
	die("debug");	
}	
public function ajaxsearchallAction()
{
	
		$params = $this->getRequest ()->getParams ();
		$path = (APPLICATION_PATH . '/indexes');   
		try {
			$authAdapter = Zend_Auth::getInstance ();
			$identity = $authAdapter->getIdentity ();
		} catch ( Exception $e ) {
			}
		$this->_helper->layout->disableLayout ();
		$result = array (
				'retCode' => '10',
				'retMsg' => 'Invalid Parameter',
				'result' => false,
				'data' => null 
		);
		
		if (isset($params['keywords'])&&$params['keywords']!="") {
		
			$query = Zend_Search_Lucene_Search_QueryParser::parse($params['keywords']);
			$index = Zend_Search_Lucene::open($path);	
			$hits = $index->find($query);
			$this->view->hits =  $hits; 	
			//Zend_Debug::dump($hits);die();
		$html="";
		if($hits){
			$i=3;
			foreach ($hits as $key=>$hit) {
						//Zend_Debug::dump($hit->getDocument());
						
						//Zend_Debug::dump($hit->wfname);	
						//Zend_Debug::dump(round($hit->score, 2) * 100);	
						//Zend_Debug::dump($hit->url);
						$r=0;
						$r = round($hit->score/20, 2) * 100;
							
						//Zend_Debug::dump($query->highlightMatches($hit->content));	
							
						if($i%2==1){	
							$html	.='<div class="row booking-results">';
						}
						$html	.=	'<div class="col-md-6">
										<div class="booking-result">
											<div class="booking-info">
												<h2><a href="#">'.$hit->title.'</a></h2>
												<ul class="stars list-inline">';
												
												for($z=1;$z<=$r;$z++) {
													$html .= '<li><i class="fa fa-star"></i></li>';
												 }
												
												$html .= '</ul>
													<div class="booking-img">
												
												<ul class="list-unstyled price-location">
													<li>
														<i class="fa fa-money"></i> '.$hit->wfname.'
													</li>';
													
													if($hit->fname!="") {
														$html .= '<li>
															<i class="fa fa-files-o"></i> <a target="_blank" href="'.$hit->url.'"> '.$hit->fname.'</a>
														</li>';
														}
														
												$html .= '</ul>
											</div>
												 <p><b>CONTENT FORM :</b>'.$query->highlightMatches($hit->contentform).'
												</p>
												<p><b>METADATA FILE : </b>'.$query->highlightMatches($hit->metadata).'</p>
												<p><b>METADATA FILE : </b>'.$query->highlightMatches($hit->content).'</p>
												
											</div>
										</div>';
									$html .= '</div>';
									if($i%2==0){
										$html .= '</div>';
									}
				$i++;
				}
			
		}
		$result = array (
				'retCode' => '10',
				'retMsg' => 'Invalid Parameter',
				'result' => true,
				'data' => $html 
		);
			
	}
		header ( "Access-Control-Allow-Origin: *" );
		header ( 'Content-Type: application/json' );
		echo json_encode ( $result );
		die ();	
}

public function buildAction()
{
		
		$params = $this->getRequest ()->getParams ();
		$dd = new Model_Zprabadata();
		$data = $dd->getall_cotents($params['hour']);
		$path = (APPLICATION_PATH . '/indexes');
		if($params['rebuild']==1) {
		// create the index
			$files = glob($path.'/*'); 
			foreach($files as $file){ 
				if(is_file($file))
				unlink($file); 
			}
			$files = glob($path.'/{,.}*', GLOB_BRACE);
			foreach($files as $file){ 
			if(is_file($file))
				unlink($file); 

			}
			$index = Zend_Search_Lucene::create($path);

		} else {
			$index = Zend_Search_Lucene::open($path);
		}
	
		
		foreach($data as $row) 
		{
		
		if(isset($row['wf'])) {
			foreach ($row['wf'] as $vv) {
							
				$doc = new  Zend_Search_Lucene_Document(); 
				$doc->addField(Zend_Search_Lucene_Field::UnIndexed('id', $row['id']."".$vv['name'])); 
				$doc->addField(Zend_Search_Lucene_Field::Text('title',$row['title'])); 
				$doc->addField(Zend_Search_Lucene_Field::Text('wfname', $vv['name']));
				$contentform = "";
				foreach ($vv['field'] as $k4=>$v4) {

				if(!is_array($v4)) {
					$contentform .= $v4." ";
					}
				}
				$content = "";
				$meta ="";	
				if(isset($vv['field']['file']['raw_url']) && file_exists($vv['field']['file']['raw_url'])) {
					
					$content = TikaWrapper::getText($vv['field']['file']['raw_url']);
					$meta = TikaWrapper::getMetadata($vv['field']['file']['raw_url']);	
					$doc->addField(Zend_Search_Lucene_Field::Text('content', $content, 'UTF-8'));
					$doc->addField(Zend_Search_Lucene_Field::Text('metadata', $meta, 'UTF-8'));
					$doc->addField(Zend_Search_Lucene_Field::Text('url', $vv['field']['file']['http_url']));
					$doc->addField(Zend_Search_Lucene_Field::Text('fname', $vv['field']['file']['fname']));
					
					
				} else {
					
					$doc->addField(Zend_Search_Lucene_Field::Text('content', '', 'UTF-8'));	
					$doc->addField(Zend_Search_Lucene_Field::Text('metadata', "", 'UTF-8'));
					$doc->addField(Zend_Search_Lucene_Field::Text('url', ""));
					$doc->addField(Zend_Search_Lucene_Field::Text('fname', ""));
				}
				
				$doc->addField(Zend_Search_Lucene_Field::Text('contentform', $contentform));
				
				$index->addDocument($doc);
						
				}	
			}
		} 
			
		
		$index->optimize();
		$this->view->count= $index->numDocs();
		echo $index->numDocs(); die();
}

public function searchAction (){    
	$this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery.numeric.js' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-datepicker/css/datepicker.css' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/select2/select2_metro.css' );
		
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/jquery-multi-select/css/multi-select.css' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js' );
		
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/scripts/form-components.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/select2/select2.min.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/data-tables/jquery.dataTables.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/data-tables/DT_bootstrap.js' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/data-tables/DT_bootstrap.css' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/fancybox/source/jquery.fancybox.pack.js' );



		$c = new Model_Zprabadata(); 




}	


public function indexAction (){    

$config = array(
    'endpoint' => array(
        'localhost' => array(
            'host' => 'localhost',
            'port' => 8983,
            'path' => '/solr/tmasolr',
        )
    )
);

$client = new Solarium\Client($config);
echo 'Solarium library version: ' . Solarium\Client::VERSION . ' - ';
// create a ping query

$ping = $client->createPing();

// execute the ping query

// execute the ping query
try {
    $result = $client->ping($ping);
	echo "ok";
	die();
} catch (Solarium\Exception $e) {
	var_dump($e); 
    // the SOLR server is inaccessible, do something
}
die("ss");

}


}
