<?php
/**
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>,Jul 15, 2014
 *
 */
class PrahuController extends Zend_Controller_Action {
	public function init() {
		/* Initialize action controller here */
	}
	public function table1Action() {
		try {
			$authAdapter = Zend_Auth::getInstance ();
			$identity = $authAdapter->getIdentity ();
		} catch ( Exception $e ) {
		}
		// Zend_Debug::dump($identity);die();
		
		$this->_helper->layout->disableLayout ();
		// $this->_helper->viewRenderer->setNoRender(true);
		$mdl_admin = new Model_Admin ();
		
		$countgraph = $mdl_admin->count_listuser ();
		// Zend_Debug::dump($countgraph);die();
		
		$listgraph = $mdl_admin->get_listuser ();
		// Zend_Debug::dump($listgraph);die();
		$iTotalRecords = intval ( $countgraph );
		$iDisplayLength = intval ( $_GET ['iDisplayLength'] );
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart = intval ( $_GET ['iDisplayStart'] );
		$sEcho = intval ( $_GET ['sEcho'] );
		
		$records = array ();
		$records ["aaData"] = array ();
		
		$end = $iDisplayStart + $iDisplayLength;
		
		foreach ( $listgraph as $k => $v ) {
			$records ["aaData"] [] = array (
					$iDisplayStart + $k + 1,
					$v ['id'],
					$v ['uname'],
					$v ['fullname'],
					$v ['email'],
					$v ['ubis'],
					$v ['sub_ubis'],
					$v ['jabatan'],
					$v ['mobile_phone'],
					$v ['fixed_phone'],
					'<div style="text-align:center"><a href="/admin/manageuser/uname/' . $v ['uname'] . '" data-id="' . $v ['id'] . '" data-uname="' . $v ['uname'] . '" data-fullname="' . $v ['fullname'] . '" data-email="' . $v ['email'] . '" data-ubis="' . $v ['ubis'] . '" data-sububis="' . $v ['sub_ubis'] . '" data-sububisid="' . $v ['sub_ubis_id'] . '" data-position="' . $v ['jabatan'] . '" data-mobilephone="' . $v ['mobile_phone'] . '" data-fixedphone="' . $v ['fixed_phone'] . '" class="btn btn-xs green btn-edit"><i class="fa fa-search"></i> View User</a>' . 
					// '<a href="javascript:;" data-id="'.$v['id'].'" data-uname="'.$v['uname'].'" data-fullname="'.$v['fullname'].'" data-email="'.$v['email'].'" data-ubis="'.$v['ubis'].'" data-sububis="'.$v['sub_ubis'].'" data-sububisid="'.$v['sub_ubis_id'].'" data-position="'.$v['jabatan'].'" data-mobilephone="'.$v['mobile_phone'].'" data-fixedphone="'.$v['fixed_phone'].'" class="btn btn-xs blue btn-edit"><i class="fa fa-pencil"></i> Edit User</a>'.
					'<a href="javascript:;" data-id="' . $v ['id'] . '" data-uname="' . $v ['uname'] . '" data-fullname="' . $v ['fullname'] . '" data-email="' . $v ['email'] . '" data-ubis="' . $v ['ubis'] . '" data-sububis="' . $v ['sub_ubis'] . '" data-sububisid="' . $v ['sub_ubis_id'] . '" data-position="' . $v ['jabatan'] . '" data-mobilephone="' . $v ['mobile_phone'] . '" data-fixedphone="' . $v ['fixed_phone'] . '" class="btn btn-xs red btn-delete"><i class="fa fa-times"></i> Delete User</a></div>' 
			);
		}
		// Zend_Debug::dump($records);die();
		
		$records ["sEcho"] = $sEcho;
		$records ["iTotalRecords"] = $iTotalRecords;
		$records ["iTotalDisplayRecords"] = $iTotalRecords;
		
		/*
		 * $gg = new Escort_Model_Kpu(); $res = $gg->get_datakpu($data2['witel']); //var_dump($res);die();
		 */
		header ( "Access-Control-Allow-Origin: *" );
		header ( 'Content-Type: application/json' );
		echo json_encode ( $records );
		die ();
	}

}
