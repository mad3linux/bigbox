<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class PublicController extends Zend_Controller_Action{

public function init (){
   /*
	$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
	$this->_cache = Zend_Registry::get('cache');
	$this->initView();
	*/
}

public function index (){   

}

public function loginAction (){
	
	if($_SERVER['HTTP_HOST']=='nextone.telkom.co.id') {
		$this->redirect('/nextone');	
	}
	
	$this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
	$this->view->headScript()->appendFile('/assets/core/plugins/backstretch/jquery.backstretch.min.js');
	
	$this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
	$this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
	
	$this->view->headScript()->appendFile('/assets/core/scripts/login-soft.js');
	$this->view->headLink()->appendStylesheet('/assets/core/skins/default/css/pages/login-soft.css');
	
	$params=$this->getRequest()->getParams();
	//Zend_Debug::dump($params);die();
	
	try{
		$authAdapter = Zend_Auth::getInstance();
		$usr = $authAdapter->getIdentity();
	}catch(Exception $e){
		
	}
	
  	if(isset($usr->uid) && isset($usr->uname)){
		$this->redirect('/');
	}
	
	//Zend_Debug::dump($authAdapter); die();
	$captcha = new Zend_Form_Element_Captcha(
			'captcha', // This is the name of the input field
    			array(
				// 'label' => 'Write the chars to the field',
				'captcha' => array( // Here comes the magic...
					// First the type...
					'captcha' => 'Image',
					// Length of the word...
					'wordLen' => 3,
					//'height'=>30,
					// Captcha timeout, 5 mins
					'timeout' => 300,
					'LineNoiseLevel'=>0,
					'DotNoiseLevel'=>5,
					// What font to use...
					'font' => APPLICATION_PATH . '/../public/assets/core/fonts/elephant.ttf',
					// Where to put the image
					'imgDir' => APPLICATION_PATH . '/../public/tmp/captcha/',
					// URL to the images
					// This was bogus, here's how it should be... Sorry again :S
					'imgUrl' => '/tmp/captcha/',
				)
			)
	);
	//echo $captcha;
	//Zend_Debug::dump($captcha);die();
	$form = new Form_Login();
	$form->setAction('/public/login'); 
	$form->addElement($captcha) ; 
	$this->view->form = $form;

	$this->view->captcha = $captcha;

	if (($this->_request->isPost() && isset($_POST))||$params['pvar']!="") {
		//Zend_Debug::dump($_POST);die();
		$cms_enc  = new CMS_Enc();
    	$vgf = $cms_enc->__unserialize($params["pvar"]);	
    	//Zend_Debug::dump($vgf); die();	
    	
    	
		$data = $_POST;
		$raws  =$_POST;
    
		$captcha = $data['captcha'];
		$captchaId = $captcha['id'];
		$captchaInput = $captcha['input'];
		$captchaSession = new Zend_Session_Namespace('Zend_Form_Captcha_'.$captchaId);
		//Zend_Debug::dump($captchaSession);die();
		$captchaIterator = $captchaSession->getIterator();
		//Zend_Debug::dump($captchaIterator);die();
		
		if(isset($vgf[0])&& $vgf[0]!="") 
		{
		
		$captchaInput = "auth";
		$captchaWord ="auth";
		$captchaIterator['word']='auth';
		$data['uname'] = $vgf[0];
		$data['passw'] =$vgf[1];
		}
		
		
		//Zend_Debug::dump($captchaIterator);die();
		if(!isset($captchaIterator['word'])){
			$this->redirect('/');
		}
		$captchaWord = $captchaIterator['word'];
		//echo $captchaInput.' - '.$captchaWord;die();
		//if($captchaInput == $captchaWord){
		   if(1 == 1) {	
            $u = $data['uname'];
			$p = $data['passw'];
			$mdl_zusr = new  Model_Zusers();
			$mdl_sys = new  Model_System();
			$data = $mdl_zusr->getuser($u);
			//Zend_Debug::dump($data); die('aaa');

			$arr1 = array();

			if(isset($data['id'])) {
				$rols  = $mdl_sys->get_roles_by_uid($data['id']);
	    		//Zend_Debug::dump($rols); die('xxx');
				foreach($rols as $v) {
					if($v['gid']=='104' && substr($_SERVER['REMOTE_ADDR'],0,3)!='10.'&&	$data['is_telkom']==1 && $data['ubis_id']!='47'){
					//	$arr[]='108';
					$arr[]=	$v['gid'];
					} else {
						$arr[]=	$v['gid'];
					}

					if($data['main_role']!='' && $data['main_role']!=null){
						if($v['landing_page_newtcare']!="" && $v['landing_page_newtcare']!=null && (int)$v['gid']==(int)$data['main_role']){
							$redirect_role =  $v['landing_page_newtcare'];
						}
					}

					if($v['exec']!=""){
						$exec[] =  $v['exec'];
					}
				}
			}
    			
			//Zend_Debug::dump($redirect_role); die();
			$auth =false;

			if(!isset($data['id']) || $data['id']==""){
				//Zend_Debug::dump($data); die('gg');
				$cms_ldp = new CMS_LDAP();

				$auth = $cms_ldp->auth($u,$p);
				//Zend_Debug::dump($auth); die('gg');
				if($auth){
					$data['id']="999999999";
		  			$arr[0]="108";
		  			$uid="999999999";
		  			$redirect_role='/';
				} else {
					$uid=$u;
				}
			} else {
				$uid=$data['id'];
			}

			$row=array();
			$msg = "Username dan Password anda tidak dikenal\nSilahkan gunakan user LDAP anda.";
			$ret = array(
				'success' => 0,
				'msg' => $msg
			);

			if ($u == ''){
				$ret['success'] = 0;
				$ret['msg'] = " Username is required";
			}

			if ($p == ''){
				$ret['success'] = 0;
				$ret['msg'] = "Password is required";
			}

			$cms_ldp = new CMS_LDAP();

			//Zend_Debug::dump($data); die();
			if($data['isldap']==1){
				$auth = $cms_ldp->auth($u,$p);
			}
			//echo ($auth); die();

			if($auth && $data['id']!="") {
				$info = $cms_ldp->bind($u);
				$ret['success'] = $auth;
				$authsession = Zend_Auth::getInstance();
				$storage = $authsession->getStorage();
				$obj2 = new stdClass;
				$obj2->uid = $uid;
				$obj2->uname = $u;
				$obj2->fullname = $info[0]['cn'][0];
				$obj2->isldap = 1;
				$obj2->mainrole = 1;
				$obj2->mainrole = $data['main_role'];
				$obj2->auth = 1;
				$obj2->roles = $arr;
				$obj2->ubis = $data['ubis_id'];
				$obj2->sububis = $data['sub_ubis_id'];
				$obj2->sububisname = $data['sub_ubis'];
				$storage->write($obj2);
				$ret['msg'] = "";
			} elseif(($data['isldap']!=1  or $data['isldap']=="" or !isset($data['isldap'])) && $uid!="") {
				$db = Zend_Db_Table::getDefaultAdapter();

				if(count($data) > 0){
					$authAdapter = new CMS_TmaAuth($db, 'z_users','uname', 'upassword', null, 0);
					$authAdapter->setIdentity($u);
					$authAdapter->setCredential($p);
					$result = $authAdapter->authenticate();
			
					if ($result->isValid()) {
						//var_dump($data);die();
						///cek user online
						//$cz = new  Model_Zonlineuser();
						//$cekz = $cz->cek_user_still_login($data['uname']);
						//Zend_Debug::dump($cekz); die();
						$cekz = false;
						
						if($cekz){
							$ret['success'] = 0; 
							$ret['msg'] = "Username has been used in other place."; 
							//var_dump($ret);die();
						}else{
							try{
								$authsession = Zend_Auth::getInstance();
								$storage = $authsession->getStorage();
							}catch(Exception $e){
								
							}
							$ret['success'] = 1;
							$obj2 = new stdClass;
							$obj2->uid = $data['id'];
							$obj2->uname = $data['uname'];
							$obj2->roles = $arr;
							$obj2->fullname = $data['fullname'];
							$obj2->mainrole = $data['main_role'];
							$obj2->isldap = 0;
							$obj2->auth = 1;
							$obj2->ubis = $data['ubis_id'];
							$obj2->sububis = (isset($data['sub_ubis_id']))?$data['sub_ubis_id']:null;
							$obj2->sububisname = (isset($data['sub_ubis']))?$data['sub_ubis']:null;
							$storage->write($obj2);
							$ret['msg'] = "";
						}
					} else {
						$ret['msg'] = "Wrong User Name/Password";
					}
				} else{
					$ret['success'] = 0;
					$ret['msg'] = "User tersebut tidak terdaftar.";
				}
			} else{
				$ret['success'] = 0;
				$ret['msg'] = "LDAP Error";
    		}
    
			//Zend_Debug::dump($ret); die('redirect');
			if($ret['success']==1){
				try{
					$auth = Zend_Auth::getInstance();
					$usr = $auth->getIdentity();
				}catch(Zend_Session_Exception $e){
					
				}
				//Zend_Debug::dump($usr); die();
				if(isset($usr->uid) && isset($usr->uname)){
   					$mdl_sys->update_log($usr->uname);
    					
					/// update untuk user online
			        $res2 = $mdl_zusr->update_c_act($usr->uname);      
					
					
					if(isset($_GET['redirect']) && $_GET['redirect']!='' && $_GET['redirect']!='/'){
						$this->_redirect($_GET['redirect']);
					}else if(isset($_POST['redirect']) && $_POST['redirect']!='' && $_POST['redirect']!='/'){
						$this->_redirect($_POST['redirect']);
					}else if(isset($params['redirect']) && $params['redirect']!='' && $params['redirect']!='/'){
						$this->_redirect($params['redirect']);
					} else {
						$module=  Zend_Controller_Front::getInstance()->getRequest()->getModuleName();
						
						if(isset($redirect_role) && $redirect_role!=""){
							$this->_redirect($redirect_role);
						}else if($module!='default'){
							$this->_redirect("/".$control);
						}else{
							$this->_redirect("/");
						}
					}
				}
			} else {
				
			}
		}else{
			$ret['success'] = 0;
			$ret['msg'] = "Please enter valid captcha.";
		}
	}
	
	if(isset($params['redirect']) && $params['redirect']!=''){
		$this->view->redirect = $params['redirect'];
	} 
					
	if(isset($ret)){
		$this->view->ret = $ret;
	}
}

public function logoutAction (){
	try{
		$authAdapter = Zend_Auth::getInstance();
		$identity = $authAdapter->getIdentity();
		
		///clear 
		$cc = new Model_Zusers();
		$cc->clear_logout($identity->uname);
		/// end
		
		$authAdapter->clearIdentity();
		
	}catch(Exception $e){
		
	}
	$this->_redirect('/');
}

public function runningtextbckAction(){
	
	$params=$this->getRequest()->getParams();
	$this->view->params = $params;
	
	
	
	
$dcgroup = array("DES","DBS","DGS");

// $dcgroup = array();
// $dcgroup = $arr_dcgroup;

// Zend_Debug::dump($dcgroup);die();

$mdl_odspots = new Nextone_Model_Odspots();
$tm = $mdl_odspots->getTargetMTTRAll("");
// Zend_Debug::dump($tm);die();

$thisparams = array();
$data = array();
$thisparams["dc"] = "DES";
$thisparams["cat"] = "MTTR-GGN";
$thisparams["thnbln"] = date("Y-m");
foreach($tm[$thisparams["dc"]] as $k=>$v){	
	$thisparams["target"] = $v["TARGET"];
	$data[] = $mdl_odspots->recovery_gauge_des_dgs_dbs($thisparams);
}

$thisparams["dc"] = "DGS";
foreach($tm[$thisparams["dc"]] as $k=>$v){	
	$thisparams["target"] = $v["TARGET"];
	$data[] = $mdl_odspots->recovery_gauge_des_dgs_dbs($thisparams);
}

$thisparams["dc"] = "DBS";
foreach($tm[$thisparams["dc"]] as $k=>$v){	
	$thisparams["target"] = $v["TARGET"];
	$data[] = $mdl_odspots->recovery_gauge_des_dgs_dbs($thisparams);
}


$data[] = $mdl_odspots->recoverycons('Q-GGN',$thisparams['thnbln'],3,'','','','');

// Zend_Debug::dump($data);die();
$this->view->data = $data;

$datadws = array();
$mdl_newrecovery = new Nextone_Model_Newrecovery();
$mdl_general = new Nextone_Model_General();
$tm = $mdl_general->getTargetMTTR('DWS');
// Zend_Debug::dump($tm);die();
foreach($tm as $k=>$v){
	if($v["target"]!="ALL"){
		
		$thisparams["target"] = $v["target"];	
		$datadws[] =  $mdl_newrecovery->recoverydws($thisparams["thnbln"],$thisparams["target"],'','','');
	}
}

// Zend_Debug::dump($datadws);die();
$this->view->datadws = $datadws;
	
}

public function runningtextAction(){

$params=$this->getRequest()->getParams();
$this->view->params = $params;

$mdl_odspots3 = new Nextone_Model_Odspots3();

$data = array();

$reg = array("","1","2","3","4","5","6","7");
// Zend_Debug::dump($reg);die();

$thisparams["thnbln"] = date("Y-m");
$thisparams["cat"] = "Q-GGN";
$thisparams["target"] = 3;


foreach($reg as $k=>$v){
	$thisparams["reg"] = $v;
	
	// $data[$k] = $mdl_odspots3->recoverycons('Q-GGN',$thisparams['thnbln'],3,'',$v,'','');
	$data[$k] = $mdl_odspots3->recoverycons($thisparams);
	$data[$k]["REG"] = $v;
}


// Zend_Debug::dump($data);die();
$this->view->data = $data;
	
}

public function consAction(){

$params=$this->getRequest()->getParams();
$this->view->params = $params;

$mdl_odspots3 = new Nextone_Model_Odspots3();

$data = $mdl_odspots3->runtext_cons($params);
// Zend_Debug::dump($data);die();
$this->view->data = $data;
	
}

public function consggnblsgAction(){

$params=$this->getRequest()->getParams();
$this->view->params = $params;

$mdl_odspots3 = new Nextone_Model_Odspots3();

$data = $mdl_odspots3->runtext_cons_ggnblsg($params);
// Zend_Debug::dump($data);die();
$this->view->data = $data;
	
}

public function indexcachedAction (){
	//die("ss");
	$mdl_sys = new Model_System();
	$mdl_dashboard = new Model_Dashboard();
	$mdl_dashboard2 = new Model_Dashboard2();
	$mdl_zmap = new Model_Zmap();
	$mdl_dashboardsys = new Model_Dashboardsystem();
	$ffr = new Model_Rafinarupotensi();
	$mdl_arium_dash = new Arium_Model_Dashboard2();
	
	//$sysfault_top20 = $mdl_arium_dash->get_servdown_top20_new(); //cached
	//Zend_Debug::dump($sysfault_top20);die();
	
	$cache =  Zend_Registry::get('cache');
	//$cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array('cron5'));
	$cctv = $mdl_dashboard->get_cctv(); //cached
	//Zend_Debug::dump($cctv);die();
	$olo_detail = $mdl_dashboard->get_olo_detil(); //cached
	//Zend_Debug::dump($olo_detail);die();
	$tsel = $mdl_dashboard->get_dash_telkomsel_new(); //cached
	//Zend_Debug::dump($tsel);die();
	$res = $mdl_zmap->get_cctv_down();
	//Zend_Debug::dump($res);die("s");
	$olo_kanan = $mdl_dashboardsys->get_olo_kanan();
	//Zend_Debug::dump($olo_kanan);die();
	$tselkanan = $mdl_dashboardsys->get_telkomsel_kanan();
	//Zend_Debug::dump($tselkanan);die();
	$lokasitsel = $mdl_dashboardsys->get_lokasi_tsel();
	//Zend_Debug::dump($lokasitsel);die();
	$tselname = $mdl_dashboardsys->get_name_tsel();
	//Zend_Debug::dump($tselname);die();
	$detil = $mdl_dashboardsys->get_detil();
	//Zend_Debug::dump($detil);die();
	$topparam = $mdl_dashboardsys->get_topcc_param();
	//Zend_Debug::dump($topparam); die('bb');
	$arr2 = $mdl_dashboard->get_top_ps();
	//Zend_Debug::dump($arr2); die('dd');
	$ps_sdtoday =  $mdl_dashboard->get_top_ps_new_sdtoday();
	//Zend_Debug::dump($ps_sdtoday); die('dd');
	$stat1= $ffr->get_status_sububis(56);
	//Zend_Debug::dump($stat1); die();
	$stat2 = $ffr->get_status_sububis(55);
	//Zend_Debug::dump($stat2);
	$lokasipemilu = $mdl_dashboardsys->get_lokasi_pemilu(); //cached
	//Zend_Debug::dump($lokasipemilu);die('dd');
	$listpic = $mdl_dashboardsys->get_list_vippemilu_pic();
	//Zend_Debug::dump($listpic);die('dd');
	
	//$sysfault_top20 = $mdl_arium_dash->get_servdown_top20_new(); //cached
	$sysfault_top20 = $mdl_dashboard2->get_servdown_top20_new(); //cached
	echo "get_servdown_top20_new<hr>";
	//Zend_Debug::dump($sysfault_top20);die();
	
	$sysfault_telin = $mdl_dashboardsys->getsysfault_telin();
	//$cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array('cron5_2'));
	
	//===== CCTV VICON KEMENHUB & POLRI system fault
	$sysfault_cctv_polri = $mdl_dashboardsys->getsysfault_cctv_polri();
	//Zend_Debug::dump($sysfault_cctv); die();
	
	$sysfault_cctv_kemenhub = $mdl_dashboardsys->getsysfault_cctv_kemenhub();
	//Zend_Debug::dump($sysfault_cctv); die();
	
		
	$mdl_dashboardnew = new Model_Dashboardnew();
	
	$compl_kemenhub = $mdl_dashboardnew->get_kemenhub();
	//Zend_Debug::dump($compl_kemenhub);die();
	
	$compl_polri = $mdl_dashboardnew->get_polri();
	//=====================================================
	//===== TOP 100 DBS
	$comptop100dbs = $mdl_dashboardnew->get_top100dbs();
	//Zend_Debug::dump($comptop100dbs);die();
	
	$sysfault_top100dbs = $mdl_dashboardsys->getsysfault_top100dbs();
	die('SUCCESS');
}	

public function newcachedAction (){
		
		$mdl_odspots = new Nextone_Model_Odspotscron();
		//$mdl_odspots = new Nextone_Model_Odspotscache();
	
		$params = array();
		$params["dc"] = "DGS";
		$params["group"] = "TOP 20 DGS";
		$params["thnbln"] = date("Y-m");
		Zend_Debug::dump($mdl_odspots->count_open_des_dgs_dbs($params));
	
		Zend_Debug::dump($mdl_odspots->count_close_des_dgs_dbs($params));

		
		
		$params = array();
		$params["dc"] = "DGS";
		$params["customer"] = "KEPOLISIAN";
		$params["thnbln"] = date("Y-m");
	
		Zend_Debug::dump($mdl_odspots->count_open_des_dgs_dbs($params));
		Zend_Debug::dump($mdl_odspots->count_close_des_dgs_dbs($params));

		
		$params = array();
		$params["dc"] = "DGS";
		$params["customer"] = "PERHUBUNGAN";
		$params["thnbln"] = date("Y-m");
		Zend_Debug::dump($mdl_odspots->count_open_des_dgs_dbs($params));
		Zend_Debug::dump($mdl_odspots->count_close_des_dgs_dbs($params));

		die("SUCCESS");

}	

public function cachedrecoverydwsAction (){
	
	$mdl_odspots = new Model_Odspots();
	$data = $mdl_odspots->CurrentPerformance();
	
	$mdl_newrcv = new Nextone_Model_Newrecovery();
	
	$params = array();
	
	$cache =  Zend_Registry::get('cache');
	//$cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array('cron5'));
	
	$params["periode"] = date("Y-m");
	
	$params["dc"] = "DWS";	
	$mttrpivot_dws = $mdl_newrcv->mttrpivot($params);
	
	$params["dc"] = "DES";	
	$mttrpivot_des = $mdl_newrcv->mttrpivot($params);
	
	$params["dc"] = "DBS";	
	$mttrpivot_dbs = $mdl_newrcv->mttrpivot($params);
	
	$params["dc"] = "DGS";	
	$mttrpivot_dgs = $mdl_newrcv->mttrpivot($params);
	
//==START DWS===========================================================================================================	
	//-=========== DWS
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "";
	$params["reg"] = "";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	// // Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	// // Zend_Debug::dump($table_close);die();
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	// // Zend_Debug::dump($table_open);die();
	
	//-=========== DWS -> TELKOMSEL
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "TELKOMSEL";
	$params["reg"] = "";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	// // Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	// Zend_Debug::dump($table_close);die();
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== DWS -> TOP 20 OLO
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "TOP 20 OLO";
	$params["reg"] = "";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== DWS -> OTHER OLO	
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "OTHER OLO";
	$params["reg"] = "";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END DWS===========================================================================================================

	
//==START DWS REG 1===================================================================================================	
		
	//-=========== DWS -> REG 1
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "";
	$params["reg"] = "1";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== DWS -> TELKOMSEL -> REG 1
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "TELKOMSEL";
	$params["reg"] = "1";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== DWS -> TOP 20 OLO -> REG 1
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "TOP 20 OLO";
	$params["reg"] = "1";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== DWS -> OTHER OLO -> REG 1
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "OTHER OLO";
	$params["reg"] = "1";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END DWS REG 1=====================================================================================================	


//==START DWS REG 2===================================================================================================		
		
	//-=========== DWS -> REG 2
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "";
	$params["reg"] = "2";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== DWS -> TELKOMSEL -> REG 2
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "TELKOMSEL";
	$params["reg"] = "2";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== DWS -> TOP 20 OLO -> REG 2
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "TOP 20 OLO";
	$params["reg"] = "2";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== DWS -> OTHER OLO -> REG 2
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "OTHER OLO";
	$params["reg"] = "2";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END DWS REG 2=====================================================================================================	

//==START DWS REG 3===================================================================================================		
		
	//-=========== DWS -> REG 3
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "";
	$params["reg"] = "3";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== DWS -> TELKOMSEL -> REG 3
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "TELKOMSEL";
	$params["reg"] = "3";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== DWS -> TOP 20 OLO -> REG 3
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "TOP 20 OLO";
	$params["reg"] = "3";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== DWS -> OTHER OLO -> REG 3
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "OTHER OLO";
	$params["reg"] = "3";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END DWS REG 3=====================================================================================================	
	
	
//==START DWS REG 4===================================================================================================		
		
	//-=========== DWS -> REG 4
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "";
	$params["reg"] = "4";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== DWS -> TELKOMSEL -> REG 4
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "TELKOMSEL";
	$params["reg"] = "4";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== DWS -> TOP 20 OLO -> REG 4
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "TOP 20 OLO";
	$params["reg"] = "4";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== DWS -> OTHER OLO -> REG 4
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "OTHER OLO";
	$params["reg"] = "4";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END DWS REG 4=====================================================================================================	
	
//==START DWS REG 5===================================================================================================		
		
	//-=========== DWS -> REG 5
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "";
	$params["reg"] = "5";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== DWS -> TELKOMSEL -> REG 5
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "TELKOMSEL";
	$params["reg"] = "5";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== DWS -> TOP 20 OLO -> REG 5
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "TOP 20 OLO";
	$params["reg"] = "5";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== DWS -> OTHER OLO -> REG 5
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "OTHER OLO";
	$params["reg"] = "5";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END DWS REG 5=====================================================================================================	
	
	
//==START DWS REG 6===================================================================================================		
		
	//-=========== DWS -> REG 6
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "";
	$params["reg"] = "6";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== DWS -> TELKOMSEL -> REG 6
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "TELKOMSEL";
	$params["reg"] = "6";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== DWS -> TOP 20 OLO -> REG 6
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "TOP 20 OLO";
	$params["reg"] = "6";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== DWS -> OTHER OLO -> REG 6
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "OTHER OLO";
	$params["reg"] = "6";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END DWS REG 6=====================================================================================================	
	
//==START DWS REG 7===================================================================================================		
		
	//-=========== DWS -> REG 7
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "";
	$params["reg"] = "7";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== DWS -> TELKOMSEL -> REG 7
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "TELKOMSEL";
	$params["reg"] = "7";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== DWS -> TOP 20 OLO -> REG 7
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "TOP 20 OLO";
	$params["reg"] = "7";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== DWS -> OTHER OLO -> REG 7
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "OTHER OLO";
	$params["reg"] = "7";
	
	$recoverydws = $mdl_newrcv->recoverydws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydws);die();
	
	$table_close = $mdl_newrcv->count_close_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dws($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END DWS REG 7=====================================================================================================	
	
	
	
	
	
	
	die('SUCCESS');
}

public function cachedrecoverydesAction (){
	
	$mdl_newrcv = new Nextone_Model_Newrecovery();
	
	$params = array();
	
	$cache =  Zend_Registry::get('cache');
	//$cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array('cron5'));
	
//==START des===========================================================================================================	
	//-=========== des
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "";
	$params["reg"] = "";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== des -> TOP 20 DES
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 20 DES";
	$params["reg"] = "";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== des -> TOP 200 DES
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 200 DES";
	$params["reg"] = "";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== des -> OTHER DES	
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "OTHER DES";
	$params["reg"] = "";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END des===========================================================================================================

	
//==START des REG 1===================================================================================================	
		
	//-=========== des -> REG 1
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "";
	$params["reg"] = "1";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== des -> TOP 20 DES -> REG 1
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 20 DES";
	$params["reg"] = "1";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== des -> TOP 200 DES -> REG 1
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 200 DES";
	$params["reg"] = "1";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== des -> OTHER DES -> REG 1
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "OTHER DES";
	$params["reg"] = "1";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END des REG 1=====================================================================================================	


//==START des REG 2===================================================================================================		
		
	//-=========== des -> REG 2
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "";
	$params["reg"] = "2";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== des -> TOP 20 DES -> REG 2
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 20 DES";
	$params["reg"] = "2";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== des -> TOP 200 DES -> REG 2
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 200 DES";
	$params["reg"] = "2";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== des -> OTHER DES -> REG 2
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "OTHER DES";
	$params["reg"] = "2";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END des REG 2=====================================================================================================	

//==START des REG 3===================================================================================================		
		
	//-=========== des -> REG 3
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "";
	$params["reg"] = "3";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== des -> TOP 20 DES -> REG 3
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 20 DES";
	$params["reg"] = "3";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== des -> TOP 200 DES -> REG 3
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 200 DES";
	$params["reg"] = "3";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== des -> OTHER DES -> REG 3
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "OTHER DES";
	$params["reg"] = "3";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END des REG 3=====================================================================================================	
	
	
//==START des REG 4===================================================================================================		
		
	//-=========== des -> REG 4
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "";
	$params["reg"] = "4";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== des -> TOP 20 DES -> REG 4
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 20 DES";
	$params["reg"] = "4";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== des -> TOP 200 DES -> REG 4
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 200 DES";
	$params["reg"] = "4";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== des -> OTHER DES -> REG 4
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "OTHER DES";
	$params["reg"] = "4";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END des REG 4=====================================================================================================	
	
//==START des REG 5===================================================================================================		
		
	//-=========== des -> REG 5
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "";
	$params["reg"] = "5";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== des -> TOP 20 DES -> REG 5
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 20 DES";
	$params["reg"] = "5";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== des -> TOP 200 DES -> REG 5
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 200 DES";
	$params["reg"] = "5";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== des -> OTHER DES -> REG 5
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "OTHER DES";
	$params["reg"] = "5";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END des REG 5=====================================================================================================	
	
	
//==START des REG 6===================================================================================================		
		
	//-=========== des -> REG 6
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "";
	$params["reg"] = "6";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== des -> TOP 20 DES -> REG 6
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 20 DES";
	$params["reg"] = "6";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== des -> TOP 200 DES -> REG 6
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 200 DES";
	$params["reg"] = "6";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== des -> OTHER DES -> REG 6
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "OTHER DES";
	$params["reg"] = "6";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END des REG 6=====================================================================================================	
	
//==START des REG 7===================================================================================================		
		
	//-=========== des -> REG 7
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "";
	$params["reg"] = "7";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== des -> TOP 20 DES -> REG 7
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 20 DES";
	$params["reg"] = "7";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== des -> TOP 200 DES -> REG 7
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 200 DES";
	$params["reg"] = "7";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== des -> OTHER DES -> REG 7
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "OTHER DES";
	$params["reg"] = "7";
	
	$recoverydes = $mdl_newrcv->recoverydes($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydes);die();
	
	$table_close = $mdl_newrcv->count_close_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_des($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END des REG 7=====================================================================================================	
	
	
	
	
	
	
	die('SUCCESS');
}

public function cachedrecoverydbsAction (){
	
	$mdl_newrcv = new Nextone_Model_Newrecovery();
	
	$params = array();
	
	$cache =  Zend_Registry::get('cache');
	//$cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array('cron5'));
	
//==START dbs===========================================================================================================	
	//-=========== dbs
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "";
	$params["reg"] = "";
	
	$recoverydbs = $mdl_newrcv->recoverydbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydbs);die();
	
	$table_close = $mdl_newrcv->count_close_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dbs -> TOP 100 DBS
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "TOP 100 DBS";
	$params["reg"] = "";
	
	$recoverydbs = $mdl_newrcv->recoverydbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydbs);die();
	
	$table_close = $mdl_newrcv->count_close_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dbs -> OTHER DBS	
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "OTHER DBS";
	$params["reg"] = "";
	
	$recoverydbs = $mdl_newrcv->recoverydbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydbs);die();
	
	$table_close = $mdl_newrcv->count_close_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END dbs===========================================================================================================

	
//==START dbs REG 1===================================================================================================	
		
	//-=========== dbs -> REG 1
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "";
	$params["reg"] = "1";
	
	$recoverydbs = $mdl_newrcv->recoverydbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydbs);die();
	
	$table_close = $mdl_newrcv->count_close_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dbs -> TOP 100 DBS -> REG 1
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "TOP 100 DBS";
	$params["reg"] = "1";
	
	$recoverydbs = $mdl_newrcv->recoverydbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydbs);die();
	
	$table_close = $mdl_newrcv->count_close_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dbs -> OTHER DBS -> REG 1
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "OTHER DBS";
	$params["reg"] = "1";
	
	$recoverydbs = $mdl_newrcv->recoverydbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydbs);die();
	
	$table_close = $mdl_newrcv->count_close_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END dbs REG 1=====================================================================================================	


//==START dbs REG 2===================================================================================================		
		
	//-=========== dbs -> REG 2
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "";
	$params["reg"] = "2";
	
	$recoverydbs = $mdl_newrcv->recoverydbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydbs);die();
	
	$table_close = $mdl_newrcv->count_close_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dbs -> TOP 100 DBS -> REG 2
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "TOP 100 DBS";
	$params["reg"] = "2";
	
	$recoverydbs = $mdl_newrcv->recoverydbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydbs);die();
	
	$table_close = $mdl_newrcv->count_close_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dbs -> OTHER DBS -> REG 2
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "OTHER DBS";
	$params["reg"] = "2";
	
	$recoverydbs = $mdl_newrcv->recoverydbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydbs);die();
	
	$table_close = $mdl_newrcv->count_close_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END dbs REG 2=====================================================================================================	

//==START dbs REG 3===================================================================================================		
		
	//-=========== dbs -> REG 3
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "";
	$params["reg"] = "3";
	
	$recoverydbs = $mdl_newrcv->recoverydbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydbs);die();
	
	$table_close = $mdl_newrcv->count_close_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dbs -> TOP 100 DBS -> REG 3
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "TOP 100 DBS";
	$params["reg"] = "3";
	
	$recoverydbs = $mdl_newrcv->recoverydbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydbs);die();
	
	$table_close = $mdl_newrcv->count_close_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dbs -> OTHER DBS -> REG 3
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "OTHER DBS";
	$params["reg"] = "3";
	
	$recoverydbs = $mdl_newrcv->recoverydbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydbs);die();
	
	$table_close = $mdl_newrcv->count_close_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END dbs REG 3=====================================================================================================	
	
	
//==START dbs REG 4===================================================================================================		
		
	//-=========== dbs -> REG 4
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "";
	$params["reg"] = "4";
	
	$recoverydbs = $mdl_newrcv->recoverydbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydbs);die();
	
	$table_close = $mdl_newrcv->count_close_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dbs -> TOP 100 DBS -> REG 4
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "TOP 100 DBS";
	$params["reg"] = "4";
	
	$recoverydbs = $mdl_newrcv->recoverydbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydbs);die();
	
	$table_close = $mdl_newrcv->count_close_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dbs -> OTHER DBS -> REG 4
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "OTHER DBS";
	$params["reg"] = "4";
	
	$recoverydbs = $mdl_newrcv->recoverydbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydbs);die();
	
	$table_close = $mdl_newrcv->count_close_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END dbs REG 4=====================================================================================================	
	
//==START dbs REG 5===================================================================================================		
		
	//-=========== dbs -> REG 5
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "";
	$params["reg"] = "5";
	
	$recoverydbs = $mdl_newrcv->recoverydbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydbs);die();
	
	$table_close = $mdl_newrcv->count_close_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dbs -> TOP 100 DBS -> REG 5
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "TOP 100 DBS";
	$params["reg"] = "5";
	
	$recoverydbs = $mdl_newrcv->recoverydbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydbs);die();
	
	$table_close = $mdl_newrcv->count_close_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dbs -> OTHER DBS -> REG 5
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "OTHER DBS";
	$params["reg"] = "5";
	
	$recoverydbs = $mdl_newrcv->recoverydbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydbs);die();
	
	$table_close = $mdl_newrcv->count_close_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END dbs REG 5=====================================================================================================	
	
	
//==START dbs REG 6===================================================================================================		
		
	//-=========== dbs -> REG 6
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "";
	$params["reg"] = "6";
	
	$recoverydbs = $mdl_newrcv->recoverydbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydbs);die();
	
	$table_close = $mdl_newrcv->count_close_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dbs -> TOP 100 DBS -> REG 6
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "TOP 100 DBS";
	$params["reg"] = "6";
	
	$recoverydbs = $mdl_newrcv->recoverydbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydbs);die();
	
	$table_close = $mdl_newrcv->count_close_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dbs -> OTHER DBS -> REG 6
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "OTHER DBS";
	$params["reg"] = "6";
	
	$recoverydbs = $mdl_newrcv->recoverydbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydbs);die();
	
	$table_close = $mdl_newrcv->count_close_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END dbs REG 6=====================================================================================================	
	
//==START dbs REG 7===================================================================================================		
		
	//-=========== dbs -> REG 7
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "";
	$params["reg"] = "7";
	
	$recoverydbs = $mdl_newrcv->recoverydbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydbs);die();
	
	$table_close = $mdl_newrcv->count_close_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dbs -> TOP 100 DBS -> REG 7
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "TOP 100 DBS";
	$params["reg"] = "7";
	
	$recoverydbs = $mdl_newrcv->recoverydbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydbs);die();
	
	$table_close = $mdl_newrcv->count_close_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dbs -> OTHER DBS -> REG 7
	$params["thnbln"] = date("Y-m");
	$target_mttr = "4";
	$params["group"] = "OTHER DBS";
	$params["reg"] = "7";
	
	$recoverydbs = $mdl_newrcv->recoverydbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydbs);die();
	
	$table_close = $mdl_newrcv->count_close_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dbs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END dbs REG 7=====================================================================================================	
	
	
	
	
	
	
	die('SUCCESS');
}

public function cachedrecoverydgsAction (){
	
	$mdl_newrcv = new Nextone_Model_Newrecovery();
	
	$params = array();
	
	$cache =  Zend_Registry::get('cache');
	//$cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array('cron5'));
	
	// TOP 20 DGS
	$tmpopendgs = $mdl_newrcv->count_open_dgs("","","TOP 20 DGS","","");
	$tmpclosedgs = $mdl_newrcv->count_close_dgs("ALL","","TOP 20 DGS","","");
	
	// KEMEMHUB NEW
	$tmpopenkemenhub = $mdl_newrcv->count_open_dgs("","","KEMENHUB","","");
	$tmpclosekemenhub = $mdl_newrcv->count_close_dgs("ALL","","KEMENHUB","","");
	// POLRI NEW
	$tmpopenkemenhub = $mdl_newrcv->count_open_dgs("","","POLRI","","");
	$tmpclosekemenhub = $mdl_newrcv->count_close_dgs("ALL","","POLRI","","");
	
//==START dgs===========================================================================================================	
	//-=========== dgs
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "";
	$params["reg"] = "";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dgs -> TOP 20 DGS
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 20 DGS";
	$params["reg"] = "";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dgs -> TOP 100 DGS
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 100 DGS";
	$params["reg"] = "";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dgs -> OTHER DGS	
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "OTHER DGS";
	$params["reg"] = "";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END DGS===========================================================================================================

	
//==START DGS REG "1"===================================================================================================	
		
	//-=========== dgs -> REG "1"
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "";
	$params["reg"] = "1";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dgs -> TOP 20 DGS
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 20 DGS";
	$params["reg"] = "1";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dgs -> TOP 100 DGS
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 100 DGS";
	$params["reg"] = "1";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dgs -> OTHER DGS	
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "OTHER DGS";
	$params["reg"] = "1";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END dgs REG "1"=====================================================================================================	
		
//==START DGS REG "2"===================================================================================================	
		
	//-=========== dgs -> REG "2"
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "";
	$params["reg"] = "2";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dgs -> TOP 20 DGS
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 20 DGS";
	$params["reg"] = "2";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dgs -> TOP 100 DGS
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 100 DGS";
	$params["reg"] = "2";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dgs -> OTHER DGS	
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "OTHER DGS";
	$params["reg"] = "2";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END dgs REG "2"=====================================================================================================	
		
//==START DGS REG "3"===================================================================================================	
		
	//-=========== dgs -> REG "3"
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "";
	$params["reg"] = "3";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dgs -> TOP 20 DGS
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 20 DGS";
	$params["reg"] = "3";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dgs -> TOP 100 DGS
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 100 DGS";
	$params["reg"] = "3";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dgs -> OTHER DGS	
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "OTHER DGS";
	$params["reg"] = "3";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END dgs REG "3"=====================================================================================================	
		
//==START DGS REG "4"===================================================================================================	
		
	//-=========== dgs -> REG "4"
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "";
	$params["reg"] = "4";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dgs -> TOP 20 DGS
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 20 DGS";
	$params["reg"] = "4";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dgs -> TOP 100 DGS
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 100 DGS";
	$params["reg"] = "4";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dgs -> OTHER DGS	
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "OTHER DGS";
	$params["reg"] = "4";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END dgs REG "4"=====================================================================================================	
		
//==START DGS REG "5"===================================================================================================	
		
	//-=========== dgs -> REG "5"
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "";
	$params["reg"] = "5";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dgs -> TOP 20 DGS
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 20 DGS";
	$params["reg"] = "5";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dgs -> TOP 100 DGS
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 100 DGS";
	$params["reg"] = "5";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dgs -> OTHER DGS	
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "OTHER DGS";
	$params["reg"] = "5";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END dgs REG "5"=====================================================================================================	
		
//==START DGS REG "6"===================================================================================================	
		
	//-=========== dgs -> REG "6"
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "";
	$params["reg"] = "6";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dgs -> TOP 20 DGS
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 20 DGS";
	$params["reg"] = "6";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dgs -> TOP 100 DGS
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 100 DGS";
	$params["reg"] = "6";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dgs -> OTHER DGS	
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "OTHER DGS";
	$params["reg"] = "6";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END dgs REG "6"=====================================================================================================	
		
//==START DGS REG "7"===================================================================================================	
		
	//-=========== dgs -> REG "6"
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "";
	$params["reg"] = "7";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dgs -> TOP 20 DGS
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 20 DGS";
	$params["reg"] = "7";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dgs -> TOP 100 DGS
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "TOP 100 DGS";
	$params["reg"] = "7";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	
	//-=========== dgs -> OTHER DGS	
	$params["thnbln"] = date("Y-m");
	$target_mttr = "3.5";
	$params["group"] = "OTHER DGS";
	$params["reg"] = "7";
	
	$recoverydgs = $mdl_newrcv->recoverydgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	//Zend_Debug::dump($recoverydgs);die();
	
	$table_close = $mdl_newrcv->count_close_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);
	$table_open = $mdl_newrcv->count_open_dgs($params['thnbln'],$target_mttr,$params['group'],$params['reg']);

//==END dgs REG "7"=====================================================================================================	
	
	
	
	
	
	
	die('SUCCESS');
}

public function alertmanageAction (){   

	$params=$this->getRequest()->getParams();
	$CMS_himfunc = new CMS_Himfunctions();
				
	try{
	$curl = $CMS_himfunc->curl_whatsapp($params['group'], urlencode($params['msg']));
	
	die("sent");
	}catch(Exception $e){
		
		Zend_Debug::dump($e);
		}	


}

public function nextonecachedAction(){
$mdl_dashboard = new Nextone_Model_Servicerecovery();

$params['cat'] = 'MTTR-GGN';
$params['thnbln'] = '2014-11';
//Zend_Debug::dump($params);

echo 'DWS';
$servicerecoverydws = $mdl_dashboard->serv_recovery_dws($params);
Zend_Debug::dump($servicerecoverydws);

echo '<hr>';
echo 'DBS';
$servicerecoverydbs = $mdl_dashboard->serv_recovery_dbs($params);
Zend_Debug::dump($servicerecoverydbs);

echo '<hr>';
echo 'DES';
$serv_recovery_des = $mdl_dashboard->serv_recovery_des($params);
Zend_Debug::dump($serv_recovery_des);

$params['cat'] = 'MTTR';
$params['thnbln'] = '2014-11';

echo '<hr>';
echo 'COMPLAINT TSEL GAUGE';
$comp_tsel_gauge = $mdl_dashboard->comp_tsel_gauge($params);
Zend_Debug::dump($comp_tsel_gauge);

echo '<hr>';
echo 'COMPLAINT TSEL CLOSE';
$comp_tsel_close = $mdl_dashboard->comp_tsel_close($params);
Zend_Debug::dump($comp_tsel_close);

echo '<hr>';
echo 'COMPLAINT TSEL ACTIVE';
$comp_tsel = $mdl_dashboard->comp_tsel($params);
Zend_Debug::dump($comp_tsel);

echo '<hr>';
echo 'COMPLAINT TOP OLO GAUGE';
$comp_topolo_gauge = $mdl_dashboard->comp_topolo_gauge($params);
Zend_Debug::dump($comp_topolo_gauge);

echo '<hr>';
echo 'COMPLAINT TOP OLO CLOSE';
$comp_topolo_close = $mdl_dashboard->comp_topolo_close($params);
Zend_Debug::dump($comp_topolo_close);

echo '<hr>';
echo 'COMPLAINT TOP OLO ACTIVE';
$comp_topolo = $mdl_dashboard->comp_topolo($params);
Zend_Debug::dump($comp_topolo);

echo '<hr>';
echo 'COMPLAINT OTHER OLO GAUGE';
$comp_otherolo_gauge = $mdl_dashboard->comp_otherolo_gauge($params);
Zend_Debug::dump($comp_otherolo_gauge);

echo '<hr>';
echo 'COMPLAINT OTHER OLO CLOSE';
$comp_otherolo_close = $mdl_dashboard->comp_otherolo_close($params);
Zend_Debug::dump($comp_otherolo_close);

echo '<hr>';
echo 'COMPLAINT OTHER OLO ACTIVE';
$comp_otherolo = $mdl_dashboard->comp_otherolo($params);
Zend_Debug::dump($comp_otherolo);

/*echo '<hr>';
echo 'COMPLAINT TOP 200 DBS GAUGE';
$comp_top200dbs_gauge = $mdl_dashboard->comp_top200dbs_gauge($params);
Zend_Debug::dump($comp_top200dbs_gauge); */





die('oks');
}

public function testaAction(){
	$this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
	$this->view->headScript()->appendFile('/assets/core/plugins/backstretch/jquery.backstretch.min.js');
	
	$this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
	$this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
	
	$this->view->headScript()->appendFile('/assets/core/scripts/login-soft.js');
	$this->view->headLink()->appendStylesheet('/assets/core/skins/default/css/pages/login-soft.css');
	
	$params=$this->getRequest()->getParams();
	//Zend_Debug::dump($params);die();
	
	try{
		$authAdapter = Zend_Auth::getInstance();
		$usr = $authAdapter->getIdentity();
	}catch(Exception $e){
		
	}
	
	//die('ok');
}

public function mappingareaAction(){
	//die('Mapping Area');
}

public function testedAction(){
		
$cc = new Nextone_Model_Odspots();
$data  = $cc->getTargetMTTRtes("CONS");
Zend_Debug::dump($data); die();

}

//=== send report sms
public function hargasahamAction(){
	
$mdl_dailysms = new Model_Dailysms();

$data = $mdl_dailysms->hargasaham();
// Zend_Debug::dump($data);die();


$hrg_close_h0 = $data['today']['HRG_CLOSED'];
$hrg_close_h1 = $data['h-1']['HRG_CLOSED'];
$hrg_close_h2 = $data['h-2']['HRG_CLOSED'];
$hrg_close_h3 = $data['h-3']['HRG_CLOSED'];
$hrg_close_w1 = $data['w-1']['HRG_CLOSED'];
$hrg_close_m1 = $data['m-1']['HRG_CLOSED'];
$market_cap = round($data['today']['MARKET_CAP']/1000000000000,1);
$market_cap_target = number_format(($data['today']['MARKET_CAP']/300000000000000)*100, 2, ',', '');

$deviesi_h0 = round(($hrg_close_h0/$hrg_close_h1-1)*100,2);
$deviesi_h1 = round(($hrg_close_h1/$hrg_close_h2-1)*100,2);
$deviesi_h2 = round(($hrg_close_h2/$hrg_close_h3-1)*100,2);
$deviesi_h3 = round(($hrg_close_h3/$hrg_close_w1-1)*100,2);
$deviesi_w1 = round(($hrg_close_w1/$hrg_close_m1-1)*100,2);


$msg = "";
$msg .= "DAILY MARKET CAP\n";
if($_GET['revised']){
	$msg .= "(Revisi)\n";	
}
//$msg .= date('l, d-M-Y', strtotime( date('Y/m/d')." -1 days"))."\n\n";
$msg .= date('l, d-M-Y')."\n\n";
$msg .= "HARGA SAHAM:\n";
$msg .= "Day: Harga Saham(%Growth)\n\n";
$msg .= "Today: ".$hrg_close_h0."(".$deviesi_h0."%)\n";
$msg .= "H-".$data['h-1']['DAY_MIN'].": ".$hrg_close_h1."(".$deviesi_h1."%)\n";
$msg .= "H-".$data['h-2']['DAY_MIN'].": ".$hrg_close_h2."(".$deviesi_h2."%)\n";
$msg .= "H-".$data['h-3']['DAY_MIN'].": ".$hrg_close_h3."(".$deviesi_h3."%)\n";
$msg .= "W-1: ".$hrg_close_w1."(".$deviesi_w1."%)\n";
$msg .= "M-1: ".$hrg_close_m1."\n\n";
$msg .= "MARKET CAP: ".$market_cap."T(".$market_cap_target."% of 300T)\n\n";
$msg .= "Sent ".date("j F Y, H:i:s");

echo '<pre>';
echo $msg;
echo '</pre>';
	
die();
}

public function revenueprognosaAction(){
	
$mdl_dailysms = new Model_Dailysms();

$data = $mdl_dailysms->revenueprognosa();
// Zend_Debug::dump($data);die();


$msg = "";
if(isset($_GET['revised']) && $_GET['revised']=='true'){
	$msg .= "(Revised Today)\n";
}
$msg .="";
$msg .= "DAILY REVENUE PROGNOSA UNCONSOLE\n";
$msg .= date('l, d-M-Y', strtotime( date('Y/m/d')." -1 days"))."\n\n";

$msg .= "Div: MTD(%Growth MoM)\n";



$all = 0;
$all_1 = 0;
foreach($data as $k=>$v){
	$all += $v['BULAN_INI'];
	$all_1 += $v['BULAN_LALU'];
}

$msg .= "All Rev: ".str_replace(',00','',number_format($all,2,',','.'))."M(".round(($all/$all_1)*100-100,2)."%)\n";

foreach($data as $k=>$v){
	if($v['PROD_PORTOFOLIO']=='NETWORK SERVICE'){
		$prod = "NETSERVC";
	} else if($v['PROD_PORTOFOLIO']=='INTERCONNECTION'){
		$prod = "INTERCON";
	} else if($v['PROD_PORTOFOLIO']=='WIRELINE'){
		$prod = "VOICE";
	} else {
		$prod = $v['PROD_PORTOFOLIO'];
	}
	$msg .= $prod.": ".str_replace(',00','',number_format($v['BULAN_INI'],2,',','.'))."M(".round($v['PERSEN'],2)."%)\n";
}

$msg .="\nSent ".date("j F Y, H:i:s");

echo "<pre>";
echo $msg;
echo "</pre>";

die();
} 

public function saleslisindihomeAction(){
		
$mdl_dailysms = new Model_Dailysms();

$data = $mdl_dailysms->saleslisindihome();
// Zend_Debug::dump($data[3]);//die();
// Zend_Debug::dump($data[4]);die();

$msg = "";

$msg .= "DAILY SALES and LIS INDIHOME\n";
$msg .= date('l, d-M-Y', strtotime( date('Y/m/d')." -1 days"))."\n\n";

$msg .= "DAILY SALES\n";
$msg .= "Div: Last Day(%AcH)/MTD(%Ach)\n";

$sales_today_all = 0;
$target_today_all = 0;
$sales_mtd_all = 0;
$target_mtd_all = 0;

foreach($data[0] as $k=>$v){
	$sales_today_all += $v['SALES_TODAY'];
	$target_today_all += $v['TARGET_TODAY'];
	$sales_mtd_all += $data[1][$k]['SALES_MTD'];
	$target_mtd_all += $data[1][$k]['TARGET_MTD'];
}

$msg .= "All: ".number_format($sales_today_all,0,'.',',')."(".round(($sales_today_all/$target_today_all)*100,2)."%)/".number_format($sales_mtd_all,0,'.',',')."(".round(($sales_mtd_all/$target_mtd_all)*100,2)."%)\n";


foreach($data[0] as $k=>$v){
	$msg .= $v['REG'].": ".number_format($v['SALES_TODAY'],0,'.',',')."(".$v['PERSEN_TODAY']."%)/".number_format($data[1][$k]['SALES_MTD'],0,'.',',')."(".round($data[1][$k]['PERSEN_MTD'],2)."%)\n";
}

$msg .= "\nCONNECT RATE (CR) FIBER ONLY\n";
$msg .= "Div: HMCON/HMPAS(%CR)\n";

$lis_all = 0;
$homepass_all = 0;

foreach($data[2] as $k=>$v){
	// Zend_Debug::dump($v);die();
	$lis_all += $data[3][$k]['HMCON'];
	$homepass_all += $v['HOMEPASS'];
}

$msg .= "All: ".number_format($lis_all,0,'.',',')."/".number_format($homepass_all,0,'.',',')."(".round(($lis_all/$homepass_all)*100,2)."%)\n";

foreach($data[2] as $k=>$v){
	
	
	// $msg .= "R".$v['REG'].": ".number_format($v['LIS'],0,'.',',')."/".number_format($v['HOMEPASS'],0,'.',',')."(".round($v['PERSEN_LIS'],2)."%)\n";
	$msg .= "R".$v['REG'].": ".number_format($data[3][$k]['HMCON'],0,'.',',')."/".number_format($v['HOMEPASS'],0,'.',',')."(".round(($data2[$k]['HMCON']/$v['HOMEPASS'])*100,2)."%)\n";
	
}


$msg .= "\nLIS 3P: ".number_format($data[4]['LIS'],0,'.',',')." (".$data[4]['PCT']."% of 3Jt)\n\n";
$msg .="Sent ".date("j F Y, H:i:s");


echo "<pre>";
echo $msg;
echo "</pre>";


die();
}

public function salesnalindihomeAction(){
		
$mdl_dailysms = new Model_Dailysms();

$data = $mdl_dailysms->salesnalindihome();
// Zend_Debug::dump($data[3]);//die();
// Zend_Debug::dump($data);die();

$msg = "";

$msg .= "DAILY SALES and NAL INDIHOME\n";
$msg .= date('l, d-M-Y', strtotime( date('Y/m/d')." -1 days"))."\n\n";

$msg .= "DAILY SALES\n";
$msg .= "Div: Last Day(%AcH)/MTD(%Ach)\n";

$sales_today_all = 0;
$target_today_all = 0;
$sales_mtd_all = 0;
$target_mtd_all = 0;

foreach($data[0] as $k=>$v){
	$sales_today_all += $v['SALES_TODAY'];
	$target_today_all += $v['TARGET_TODAY'];
	$sales_mtd_all += $data[1][$k]['SALES_MTD'];
	$target_mtd_all += $data[1][$k]['TARGET_MTD'];
}

$msg .= "All: ".number_format($sales_today_all,0,'.',',')."(".round(($sales_today_all/$target_today_all)*100,2)."%)/".number_format($sales_mtd_all,0,'.',',')."(".round(($sales_mtd_all/$target_mtd_all)*100,2)."%)\n";


foreach($data[0] as $k=>$v){
	$msg .= $v['REG'].": ".number_format($v['SALES_TODAY'],0,'.',',')."(".$v['PERSEN_TODAY']."%)/".number_format($data[1][$k]['SALES_MTD'],0,'.',',')."(".round($data[1][$k]['PERSEN_MTD'],2)."%)\n";
}

// Zend_Debug::dump($data);

$msg .= "\nNAL INDIHOME\n";
$msg .= "Div: NAL(NAL Lastday)/%NAL Fiber/%CR\n";

$nal_ytd_all =0;
$nal_ytd_fb =0;
$nal_ld =0;
$ftth_indihome =0;
$all_indihome =0;

foreach($data[2] as $k=>$v){
	// Zend_Debug::dump($v);die();
	$nal_ytd_all += $v['NAL_YTD'];
	$nal_ytd_fb += $v['NAL_YTD_FB'];
	$nal_ld += $v['NAL_LD'];	
	
	$ftth_indihome +=$data[3][$k]["FTTH_INDIHOME"];
	$all_indihome +=$data[3][$k]["ALL_INDIHOME"];
}

$nal_fb_persen = ($nal_ytd_fb/$nal_ytd_all)*100;
$nal_cr_persen = ($ftth_indihome/$all_indihome)*100;

$msg .="All: ".number_format($nal_ytd_all,0,".",",")."(".number_format($nal_ld,0,".",",").")/".(float)number_format($nal_fb_persen,2,".",",")."%/".(float)number_format($nal_cr_persen,2,".",",")."%\n";

$nal_fb_persen =0; 
$nal_cr_persen =0; 
foreach($data[2] as $k=>$v){
	
	$nal_fb_persen = ($v["NAL_YTD_FB"]/$v["NAL_YTD"])*100;
	$nal_cr_persen = ($data[3][$k]["FTTH_INDIHOME"]/$data[3][$k]["ALL_INDIHOME"])*100;
	
	$msg .= $v["REG"].": ".number_format($v["NAL_YTD"],0,".",",")."(".number_format($v["NAL_LD"],0,".",",").")/".(float)number_format($nal_fb_persen,2,".",",")."%/".(float)number_format($nal_cr_persen,2,".",",")."%\n";
	
}


// $msg .= "\nLIS 3P: ".number_format($data[4]['LIS'],0,'.',',')." (".$data[4]['PCT']."% of 3Jt)";
$msg .="\n\nSent ".date("j F Y, H:i:s");


echo "<pre>";
echo $msg;
echo "</pre>";


die();
}

public function ggnretailAction(){

$mdl_dailysms = new Model_Dailysms();

$data = $mdl_dailysms->ggnretail();
// Zend_Debug::dump($data);die();

die();
}

public function ggnretailnonateroAction(){

$mdl_dailysms = new Model_Dailysms();

$data = $mdl_dailysms->ggnretail_nonatero();
// Zend_Debug::dump($data);die();


$a_date = date("Y-m-d");
$ldom =  date("t", strtotime($a_date));
$d_now = date("d");
$m_now = date("m");
// echo $ldom;

if($ldom == $d_now && $m_now != 12){
	$posisi = "".date("d F",strtotime( date('Y/m/d')." -1 days"))." vs ".date("d F")." ";
} else if($ldom == $d_now && $m_now == 12){
	$posisi = "".date("d F Y",strtotime( date('Y/m/d')." -1 days"))." vs ".date("d F Y")." ";
} else {
	$posisi = "".date("d",strtotime( date('Y/m/d')." -1 days"))." vs ".date("d")." ".date("F Y")."";
} 



$msg = "TOTAL TIKET BERLANGSUNG\n";
$msg .= "Posisi : ".$posisi." pukul ".$data[0]." WIB\n\n";
$msg .= "DIV: H-1/H(%Growth)\n";

$jml_lalu = 0;
$jml_now = 0;
$jml_lalu_nas = 0;
$jml_now_nas = 0;
foreach($data[1] as $k=>$v){
	
	$jml_lalu_nas += $v["JML_GGN_LALU"];
	$jml_now_nas += $v["JML_GGN_NOW"];
	
	$jml_lalu = number_format($v["JML_GGN_LALU"],0,'.',',');
	$jml_now = number_format($v["JML_GGN_NOW"],0,'.',',');
	
	$div= str_replace("REGIONAL ","R",$v["DIV"]);	
	$msg .= $div.": ".$jml_lalu."/".$jml_now."(".round($v["GROWTH"],2)."%)\n";
}

$growth = (($jml_now_nas-$jml_lalu_nas)/$jml_lalu_nas)*100;
$jml_lalu_nas = number_format($jml_lalu_nas,0,'.',',');
$jml_now_nas = number_format($jml_now_nas,0,'.',',');

$msg .= "NASIONAL: ".$jml_lalu_nas."/".$jml_now_nas."(".round($growth,2)."%)\n";

$msg .="\nSent ".date("j F Y, H:i:s");

echo "<pre>";
echo $msg;
echo "</pre>";

die();
}


//=== send report telegram per regional
public function salesnalindihomerAction(){
	
$params=$this->getRequest()->getParams();
// Zend_Debug::dump($params);die();
		
$mdl_dailysms = new Model_Dailysms();

$data = $mdl_dailysms->salesnalindihomebyreg($params["reg"]);
// Zend_Debug::dump($data[4]);//die();

$msg = "";

$msg .= "DAILY SALES and NAL INDIHOME TR".$params["reg"]."\n";
$msg .= date('l, d-M-Y', strtotime( date('Y/m/d')." -1 days"))."\n\n";

$msg .= "DAILY SALES\n";
$msg .= "Witel: Last Day(%Ach)/MTD(%Ach)\n";

$sales_today_all = 0;
$target_today_all = 0;
$sales_mtd_all = 0;
$target_mtd_all = 0;

foreach($data[0] as $k=>$v){
	$sales_today_all += $v['SALES_TODAY'];
	$target_today_all += $v['TARGET_TODAY'];
	$sales_mtd_all += $data[1][$k]['SALES_MTD'];
	$target_mtd_all += $data[1][$k]['TARGET_MTD'];
}

$msg .= "TR".$params["reg"].": ".number_format($sales_today_all,0,'.',',')."(".round(($sales_today_all/$target_today_all)*100,2)."%)/".number_format($sales_mtd_all,0,'.',',')."(".round(($sales_mtd_all/$target_mtd_all)*100,2)."%)\n";


foreach($data[0] as $k=>$v){
	$msg .= $v['SWITEL'].": ".number_format($v['SALES_TODAY'],0,'.',',')."(".$v['PERSEN_TODAY']."%)/".number_format($data[1][$k]['SALES_MTD'],0,'.',',')."(".round($data[1][$k]['PERSEN_MTD'],2)."%)\n";
}

// Zend_Debug::dump($data);

$msg .= "\nNAL INDIHOME\n";
$msg .= "Witel: NAL(Lastday/YTD)/%NAL Fiber/%CR\n";

$nal_ytd_all =0;
$nal_ytd_fb =0;
$nal_ld =0;
$ftth_indihome =0;
$all_indihome =0;

foreach($data[2] as $k=>$v){
	// Zend_Debug::dump($v);die();
	$nal_ytd_all += $v['NAL_YTD'];
	$nal_ytd_fb += $v['NAL_YTD_FB'];
	$nal_ld += $v['NAL_LD'];	
	
	$ftth_indihome +=$data[3][$k]["FTTH_INDIHOME"];
	$all_indihome +=$data[3][$k]["ALL_INDIHOME"];
}

$nal_fb_persen = ($nal_ytd_fb/$nal_ytd_all)*100;
$nal_cr_persen = ($ftth_indihome/$all_indihome)*100;

$msg .="TR".$params["reg"].": ".number_format($nal_ld,0,".",",")."(".number_format($nal_ytd_all,0,".",",").")/".(float)number_format($nal_fb_persen,2,".",",")."%/".(float)number_format($nal_cr_persen,2,".",",")."%\n";

$nal_fb_persen =0; 
$nal_cr_persen =0; 
foreach($data[2] as $k=>$v){
	
	$nal_fb_persen = ($v["NAL_YTD_FB"]/$v["NAL_YTD"])*100;
	$nal_cr_persen = ($data[3][$k]["FTTH_INDIHOME"]/$data[3][$k]["ALL_INDIHOME"])*100;
	
	$msg .= $v["SWITEL"].": ".number_format($v["NAL_LD"],0,".",",")."(".number_format($v["NAL_YTD"],0,".",",").")/".(float)number_format($nal_fb_persen,2,".",",")."%/".(float)number_format($nal_cr_persen,2,".",",")."%\n";
	
}


$msg .= "\nLIS 3P: ".number_format($data[4]['LIS'],0,'.',',')." (".$data[4]['PCT']."% of ".(float)($data[4]["TARGET"]/1000)."K)\n\n";
$msg .="Sent ".date("j F Y, H:i:s");


echo "<pre>";
echo $msg;
echo "</pre>";
// die();

if(isset($params["send"]) && $params["send"]==true){
	

	
$arr_target = array(
	"1"=>"Budi_Codet",
	"2"=>"Budi_Codet",
	"3"=>"ALERT_TR3",
	"4"=>"Budi_Codet",
	"5"=>"Budi_Codet",
	"6"=>"ALERTTR6",
	"7"=>"ALERTTR7",
);

$target = $arr_target[$params["reg"]];
$send = $mdl_dailysms->send_telegram_broadcast($target,$msg);
// Zend_Debug::dump($send);die();

$result = '';
$result .= 'target: "'.$target.'"<br>';
$result .= 'message: "'.$send['message'].'"';

echo "<hr>";
echo "<pre>";
echo $result;
echo "</pre>";

}


die();
}

public function salesnalindihomer2Action(){
	
$params=$this->getRequest()->getParams();
// Zend_Debug::dump($params);die();
		
$mdl_dailysms = new Model_Dailysms();

$data = $mdl_dailysms->salesnalindihomebyreg2($params["reg"]);
// Zend_Debug::dump($data[2]);//die();

$msg = "";

$msg .= "NAL INDIHOME / NAL INDIHOME FIBER TR".$params["reg"]."\n";
$msg .= "Position data: ".date('l, d-M-Y')."\n";
$msg .= "Jam update: ".date("H:i")."\n\n";

// Zend_Debug::dump($data);

$msg .= "NAL INDIHOME\n";
$msg .= "Witel: NAL(Today/MTD/YTD)\n";

$nal_td = 0;
$nal_mtd = 0;
$nal_ytd = 0;

foreach($data[2] as $k=>$v){
	
	$nal_td += $v["NAL_LD"];
	$nal_mtd += $v["NAL_MTD"];
	$nal_ytd += $v["NAL_YTD"];
}

$msg .= "TR".$params["reg"].": ".$nal_td."/".$nal_mtd."/".$nal_ytd."\n";
	

foreach($data[2] as $k=>$v){
	
	
	$msg .= $v["SWITEL"].": ".number_format($v["NAL_LD"],0,".",",")."/".number_format($v["NAL_MTD"],0,".",",")."/".number_format($v["NAL_YTD"],0,".",",")."\n";
	
}

$msg .= "\n% NAL INDIHOME FIBER\n";
$msg .= "Witel: % NAL(Today/MTD/YTD)\n";


$nal_td =0;
$nal_mtd =0;
$nal_ytd =0;
$nal_td_fb =0;
$nal_mtd_fb =0;
$nal_ytd_fb =0;

foreach($data[2] as $k=>$v){
	
	$nal_td += $v["NAL_LD"];
	$nal_mtd += $v["NAL_MTD"];
	$nal_ytd += $v["NAL_YTD"];
	$nal_td_fb += $v["NAL_TD_FB"];
	$nal_mtd_fb += $v["NAL_MTD_FB"];
	$nal_ytd_fb += $v["NAL_YTD_FB"];
}


	$nal_td_fb_persen = (float)number_format(($nal_td_fb/$nal_td)*100,2,",",".");
	$nal_mtd_fb_persen = (float)number_format(($nal_mtd_fb/$nal_mtd)*100,2,",",".");
	$nal_ytd_fb_persen = (float)number_format(($nal_ytd_fb/$nal_ytd)*100,2,",",".");
	
	if($nal_td_fb_persen>100){
		$nal_td_fb_persen = 100;
	}

$msg .= "TR".$params["reg"].": ".$nal_td_fb_persen."%/".$nal_mtd_fb_persen."%/".$nal_ytd_fb_persen."%\n";
	
$nal_td_fb_persen = 0;
$nal_mtd_fb_persen = 0;
$nal_ytd_fb_persen = 0;
foreach($data[2] as $k=>$v){
	
	$nal_td_fb_persen = (float)number_format(($v["NAL_TD_FB"]/$v["NAL_LD"])*100,2,".",",");
	$nal_mtd_fb_persen = (float)number_format(($v["NAL_MTD_FB"]/$v["NAL_MTD"])*100,2,".",",");
	$nal_ytd_fb_persen = (float)number_format(($v["NAL_YTD_FB"]/$v["NAL_YTD"])*100,2,".",",");
	
	if($nal_td_fb_persen>100){
		$nal_td_fb_persen = 100;
	}
	
	$msg .= $v["SWITEL"].": ".$nal_td_fb_persen."%/".$nal_mtd_fb_persen."%/".$nal_ytd_fb_persen."%\n";
	
}



// Zend_Debug::dump($data[5]);die();
$msg .= "\n5 BESAR % NAL YTD Fiber Per KELAS";
$msg .= "\nWitel: NAL(Today/MTD/YTD/%YTD Fiber)";
$tmpdata5 = array();
foreach($data[5] as $k=>$v){
	$tmpdata5[$v["KELAS"]][] = $v;
}
// Zend_Debug::dump($tmpdata5);die();

$arr_kelas = array("PLATINUM","","TITANIUM","GOLD");

// foreach($arr_kelas as $k=>$v){
	$msg .="\nPLATINUM\n";
	foreach($tmpdata5["PLATINUM"] as $kk=>$vv){
		if($kk<5){	
			$msg .= $vv["SWITEL"].": ".number_format($vv["NAL_LD"],0,".",",")."/".number_format($vv["NAL_MTD"],0,".",",")."/".number_format($vv["NAL_YTD"],0,".",",")."/".$vv["PERSEN_FIBER"]."%\n";	
		}
	}
	$msg .="\nTITANIUM\n";
	foreach($tmpdata5["TITANIUM"] as $kk=>$vv){
		if($kk<5){		
			$msg .= $vv["SWITEL"].": ".number_format($vv["NAL_LD"],0,".",",")."/".number_format($vv["NAL_MTD"],0,".",",")."/".number_format($vv["NAL_YTD"],0,".",",")."/".$vv["PERSEN_FIBER"]."%\n";	
		}
	}
	$msg .="\nGOLD\n";
	foreach($tmpdata5["GOLD"] as $kk=>$vv){
		if($kk<5){		
			$msg .= $vv["SWITEL"].": ".number_format($vv["NAL_LD"],0,".",",")."/".number_format($vv["NAL_MTD"],0,".",",")."/".number_format($vv["NAL_YTD"],0,".",",")."/".$vv["PERSEN_FIBER"]."%\n";	
		}
	}
// }


// if($params["reg"]=="7"){
	
// $msg .= "\nLIS 3P: ".number_format($data[4]['LIS'],0,'.',',')." (".(float)number_format(($data[4]['LIS']/123.96)*100,"2",",",".")."% of 123.960)";
// } else {
	
// $msg .= "\nLIS 3P: ".number_format($data[4]['LIS'],0,'.',',')." (".$data[4]['PCT']."% of ".(float)($data[4]["TARGET"]/1000)."K)";	
// }
$msg .="\n\nSent ".date("j F Y, H:i:s");


echo "<pre>";
echo $msg;
echo "</pre>";
// die();

if(isset($params["send"]) && $params["send"]==true){
	

	
$arr_target = array(
	"1"=>"Budi_Codet",
	"2"=>"Budi_Codet",
	"3"=>"ALERT_TR3",
	"4"=>"Budi_Codet",
	"5"=>"Budi_Codet",
	"6"=>"ALERTTR6",
	"7"=>"ALERTTR7",
);

$target = $arr_target[$params["reg"]];
// $target = "Randy_Rahman";
// die($target);


$send = $mdl_dailysms->send_telegram_broadcast($target,$msg);
// Zend_Debug::dump($send);die();

$result = '';
$result .= 'target: "'.$target.'"<br>';
$result .= 'message: "'.$send['message'].'"';

echo "<hr>";
echo "<pre>";
echo $result;
echo "</pre>";

}


die();
}

public function ggnretailnonaterorAction(){
	
$params=$this->getRequest()->getParams();
// Zend_Debug::dump($params);die();

$mdl_dailysms = new Model_Dailysms();

$data = $mdl_dailysms->ggnretailbyreg_nonatero($params["reg"]);
// Zend_Debug::dump($data);die();


$a_date = date("Y-m-d");
$ldom =  date("t", strtotime($a_date));
$d_now = date("d");
$m_now = date("m");
// echo $ldom;

if($ldom == $d_now && $m_now != 12){
	$posisi = "".date("d F",strtotime( date('Y/m/d')." -1 days"))." vs ".date("d F")." ";
} else if($ldom == $d_now && $m_now == 12){
	$posisi = "".date("d F Y",strtotime( date('Y/m/d')." -1 days"))." vs ".date("d F Y")." ";
} else {
	$posisi = "".date("d",strtotime( date('Y/m/d')." -1 days"))." vs ".date("d")." ".date("F Y")."";
}



$msg = "TOTAL TIKET BERLANGSUNG TR".$params["reg"]."\n";
$msg .= "Posisi : ".$posisi." pukul ".$data[0]." WIB\n\n";
$msg .= "Witel: H-1/H/%Growth\n";

$jml_lalu = 0;
$jml_now = 0;
$jml_lalu_nas = 0;
$jml_now_nas = 0;
foreach($data[1] as $k=>$v){
	
	$jml_lalu_nas += $v["JML_GGN_LALU"];
	$jml_now_nas += $v["JML_GGN_NOW"];
	
	$jml_lalu = number_format($v["JML_GGN_LALU"],0,'.',',');
	$jml_now = number_format($v["JML_GGN_NOW"],0,'.',',');
	
	// $div= str_replace("REGIONAL ","R",$v["DIV"]);	
	$div= $v["WITEL"];	
	$msg .= $div.": ".$jml_lalu."/".$jml_now."/".round($v["GROWTH"],2)."%\n";
}

$growth = (($jml_now_nas-$jml_lalu_nas)/$jml_lalu_nas)*100;
$jml_lalu_nas = number_format($jml_lalu_nas,0,'.',',');
$jml_now_nas = number_format($jml_now_nas,0,'.',',');

$msg .= "TR".$params["reg"].": ".$jml_lalu_nas."/".$jml_now_nas."/".round($growth,2)."%\n";

$msg .="\nSent ".date("j F Y, H:i:s");

echo "<pre>";
echo $msg;
echo "</pre>";
// die();

if(isset($params["send"]) && $params["send"]==true){
	

	
$arr_target = array(
	"1"=>"Budi_Codet",
	"2"=>"Budi_Codet",
	"3"=>"ALERT_TR3",
	"4"=>"Budi_Codet",
	"5"=>"Budi_Codet",
	"6"=>"ALERTTR6",
	"7"=>"ALERTTR7",
);

$target = $arr_target[$params["reg"]];

$send = $mdl_dailysms->send_telegram_broadcast($target,$msg);
// Zend_Debug::dump($send);die();

$result = '';
$result .= 'target: "'.$target.'"<br>';
$result .= 'message: "'.$send['message'].'"';

echo "<hr>";
echo "<pre>";
echo str_replace("\n","\n",$result);
echo "</pre>";

}

die();
}

public function salesnalindihomeperdatelAction(){

$params=$this->getRequest()->getParams();
// Zend_Debug::dump($params);die();

$mdl_dailysms = new Model_Dailysms();

$tmpdata = $mdl_dailysms->salesnalindihomeperdatelbyreg($params);
// Zend_Debug::dump($tmpdata);die();

$data = array();
$data["updated_date"] = $tmpdata[0]["UPDATED_DATE"];
$data["data"] = array();

foreach($tmpdata as $k=>$v){
	$data["data"][$v["WITEL"]][] = $v;
}

// Zend_Debug::dump($data);die();


$msg .="NAL INDIHOME TR".$params["reg"]."\n";
$msg .="Position data: ".date("l, d-M-Y",strtotime($data["updated_date"]))."\n";
$msg .="Jam update: ".date("H:i",strtotime($data["updated_date"]))."\n\n";

$msg .="DATEL TD/MTD/YTD/%GROWTH MTD\n";
$td_total_t = 0;
$mtd_total_t = 0;
$ytd_total_t = 0;
$mtdmin_total_t = 0;
$msg_witel = "";
foreach($data["data"] as $k=>$v){

$msg_datel = "";
$td_total = 0;
$mtd_total = 0;
$ytd_total = 0;
$mtdmin_total = 0;
$compared1 = 0;
$compared = 0;
$comparew1 = 0;
$comparew = 0;
foreach($v as $k2=>$v2){

$td_total += $v2["TD"];
$mtd_total += $v2["MTD"];
$ytd_total += $v2["YTD"];
$mtdmin_total += $v2["MTDMIN"];

$compared1 = $v2["MTD"]-$v2["MTDMIN"];
$compared = ($compared1 > 0 ? "+".$compared1 : $compared1);

$comparew1 += $compared1;
$comparew = ($comparew1 > 0 ? "+".$comparew1 : $comparew1);

$msg_datel .=" ".$v2["DATEL_SNAME"].": ".$v2["TD"]."/".$v2["MTD"]."/".$v2["YTD"]."/".$v2["GROWTH_MTD"]."% (".$compared.")\n";
}


$td_total_t += $td_total;
$mtd_total_t += $mtd_total;
$ytd_total_t += $ytd_total;
$mtdmin_total_t += $mtdmin_total;

$comparer1 += $comparew1;
$comparer = ($comparer1 > 0 ? "+".$comparer1 : $comparer1);

$growth_mtd = ($mtdmin_total != 0 ? round((($mtd_total - $mtdmin_total)/$mtdmin_total*100),2) : "x");


$msg_witel .=$k.": ".$td_total."/".$mtd_total."/".$ytd_total."/".$growth_mtd."% (".$comparew.")\n";
$msg_witel .=$msg_datel;
$msg_witel .="\n";
}

$growth_mtd_t = ($mtdmin_total_t != 0 ? round((($mtd_total_t - $mtdmin_total_t)/$mtdmin_total_t*100),2) : "x");

$msg .= "TREG-".$params["reg"].": ".$td_total_t."/".$mtd_total_t."/".$ytd_total_t."/".$growth_mtd_t."% (".$comparer.")\n\n";
$msg .= $msg_witel;

$msg .="Sent ".date("j F Y, H:i:s");

echo "<pre>";
echo str_replace("\n","\n",$msg);
echo "</pre>";
echo "<hr>";
// echo "<pre>";
// echo $msg;
// echo "</pre>";



if(isset($params["send"]) && $params["send"]==true){
	
	
$arr_target = array(
	"1"=>"Budi_Codet",
	"2"=>"Budi_Codet",
	"3"=>"ALERT_TR3",
	"4"=>"Budi_Codet",
	"5"=>"Budi_Codet",
	"6"=>"ALERTTR6",
	"7"=>"ALERTTR7",
);

$target = $arr_target[$params["reg"]];
// $target = "Randy_Rahman";

$send = $mdl_dailysms->send_telegram_broadcast($target,$msg);
// Zend_Debug::dump($send);die();

$result = '';
$result .= 'target: "'.$target.'"<br>';
$result .= 'message: "'.$send['message'].'"';

echo "<hr>";
echo "<pre>";
echo $result;
echo "</pre>";

}


die();
}

public function potretggnberlangsungAction(){
	
$params=$this->getRequest()->getParams();
// Zend_Debug::dump($params);die();

$mdl_dailysms = new Model_Dailysms();
	
$tmpdata = $mdl_dailysms->potretggnberlangsung($params);
// Zend_Debug::dump($tmpdata);die();

$arr_abjad = array("A","B","C","D","E","F");

$data = array();
foreach($tmpdata as $k=>$v){
	$data[$v["TIER_GGN"]][] = $v;
	
}
// Zend_Debug::dump($data);//die();

$msg = "";
$msg .="Potret Tiket GGN Berlangsung (NASIONAL)\n";
$msg .="Posisi:\n";
$msg .="20 September 2015\n";
$msg .="Pukul 08 WIB vs\n";
$msg .="21 September 2015 Pukul 08 WIB\n";

$msg .="\n";
$i=0;
foreach ($data as $k=>$v){
	
$k = substr($k,3);
$msg .=$arr_abjad[$i].". Usia GGN: ".$k."\n";
$msg .="TReg | H-1 | H | %Growth\n";

foreach($v as $k2=>$v2){
	
$regional = str_replace("REGIONAL ","TREG",$v2["DIV"]);
$ggn_lalu = number_format($v2["JML_GGN_LALU"],0,",",".");
$ggn_now = number_format($v2["JML_GGN_NOW"],0,",",".");
$growth = (float)round($v2["GROWTH"],2);

$msg .=$regional." | ".$ggn_lalu." | ".$ggn_now." | ".$growth."%\n";
}
$msg .="\n";
$i++;
}

$msg .="\n";
$msg .="Note:\n";
$msg .="Usia GGN < 3 hari: Turun(-15.22)\n";
$msg .="Usia GGN 4 s/d 7 hari: Naik(33.43)\n";
$msg .="Usia GGN 8 s/d 15 hari: Turun(-3.02)\n";
$msg .="Usia GGN 16 s/d 30 hari: Turun(-0.75)\n";
$msg .="Usia GGN > 30 hari: Naik(5.32)\n";
$msg .="Totally TIKET BERLANGSUNG: Turun(-5.83)\n";
$msg .="\n";
$msg .="Keterangan :\n";
$msg .="- Sisa GGN adalah GGN BERLANGSUNG SAAT INI pelanggan PL, BL dan CL\n";
$msg .="dibandingkan dengan waktu yang sama hari kemarin\n";
$msg .="\n";
$msg .="Demikian Terima Kasih\n";
$msg .="Salam Indihome 3P 100% Fiber\n";

echo "<pre>";
echo $msg;
echo "</pre>";

die();
}

}

