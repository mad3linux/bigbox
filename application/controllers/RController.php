<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class RController extends Zend_Rest_Controller
{
  public function init()
{
  $this->_helper->viewRenderer->setNoRender(true);
}  
 public function indexAction()
{
  $this->getResponse()->setBody('Hello World');
  $this->getResponse()->setHttpResponseCode(200);
}

public function getAction()
{
  $this->getResponse()->setBody('Foo!');
  $this->getResponse()->setHttpResponseCode(200);
}

public function postAction()
{
  $this->getResponse()->setBody('resource created');
  $this->getResponse()->setHttpResponseCode(200);
}

public function putAction()
{
  $this->getResponse()->setBody('resource updated');
  $this->getResponse()->setHttpResponseCode(200);
}

public function deleteAction()
{
  $this->getResponse()->setBody('resource deleted');
  $this->getResponse()->setHttpResponseCode(200);
}
}