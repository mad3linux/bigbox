<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class SearchController extends Zend_Controller_Action
{
    public function indexAction()
    {
       
		$path = (APPLICATION_PATH . '/indexes');   
		if($_GET['keyword'])  
		{
			$query = Zend_Search_Lucene_Search_QueryParser::parse($_GET[keyword]);
			$index = Zend_Search_Lucene::open($path);	
			$hits = $index->find($query);
			$this->view->hits =  $hits; 
		}  
	}
    
    public function buildAction()
    {
        // create the index
        $path = (APPLICATION_PATH . '/indexes');
        $files = glob($path.'/*'); 
		foreach($files as $file){ 
		if(is_file($file))
				unlink($file); 
		
		}
		$files = glob($path.'/{,.}*', GLOB_BRACE);
		foreach($files as $file){ 
		if(is_file($file))
				unlink($file); 
		
		}
        $index = Zend_Search_Lucene::create($path);
		$cc =  new Model_Zsearch();
		$data = $cc->get_all();
		//Zend_Debug::dump($data); die;
		
		foreach($data as $row) 
			{
				
				$doc = new Zend_Search_Lucene_Document(); 
				$doc->addField(Zend_Search_Lucene_Field::UnIndexed('id', $row[id])); 
				$doc->addField(Zend_Search_Lucene_Field::Text('document_title', $row[uname]." ".$row[fullname]." ".$row[first_name]." ".$row[last_name]." ".$row[email])); 
				$doc->addField(Zend_Search_Lucene_Field::Text('document_content', $row[uname]." ".$row[fullname]." ".$row[user_type]." ".$row[descr]." ".$row[work_unit]." ".$row[first_name]." ".$row[last_name]." ".$row[position]." ".$res[affiliation]." ".$row[url]." ".$row[simple_bio]." ".$row[twitter]." ".$row[facebook]." ".$row[mobile_phone]." ".$row[country]." "));
				$doc->addField(Zend_Search_Lucene_Field::Text('venue', $row['venue'])); 
				$doc->addField(Zend_Search_Lucene_Field::Text('icon', $row['icon']));
				$doc->addField(Zend_Search_Lucene_Field::Text('uname', $row['uname'])); 
				$doc->addField(Zend_Search_Lucene_Field::Keyword('selular', $row['selular']));
				$doc->addField(Zend_Search_Lucene_Field::Keyword('flexi', $row['flexi']));
				$doc->addField(Zend_Search_Lucene_Field::Text('divisi', $row['divisi']));
				$doc->addField(Zend_Search_Lucene_Field::Text('fullname', $row[first_name]." ".$row[last_name]));
				$doc->addField(Zend_Search_Lucene_Field::Keyword('nik', $row['nik']));
				$doc->addField(Zend_Search_Lucene_Field::Keyword('person_id', $row['person_id']));	
				$doc->addField(Zend_Search_Lucene_Field::Text('jadwal', $row['jadwal']));
				$doc->addField(Zend_Search_Lucene_Field::Text('group', $row['group']));
				$doc->addField(Zend_Search_Lucene_Field::Text('nama', $row['nama']));
				$doc->addField(Zend_Search_Lucene_Field::Text('jabatan', $row['jabatan']));
				$index->addDocument($doc);

			}
			$index->optimize();
			$this->view->count= $index->numDocs();
			
        
    }
    
    
    public function indexdeviceAction()
    {
        // create the index
        $path = (APPLICATION_PATH . '/indexes/devices');
        $files = glob($path.'/*'); 
        /*
		foreach($files as $file){ 
		if(is_file($file))
				unlink($file); 
		
		}
		$files = glob($path.'/{,.}*', GLOB_BRACE);
		foreach($files as $file){ 
		if(is_file($file))
				unlink($file); 
		
		}
		*/
        $index = Zend_Search_Lucene::create($path);
		$cc =  new Rafinaru_Model_Rafinarusearch();
		$data = $cc->get_cust_service_devices();
		//Zend_Debug::dump($data); die;
		
		foreach($data as $row) 
			{
				
				$doc = new Zend_Search_Lucene_Document(); 
				//$doc->addField(Zend_Search_Lucene_Field::UnIndexed('serv_id', $row[serv_id])); 
				$doc->addField(Zend_Search_Lucene_Field::Keyword('serv_id', $row['serv_id'])); 
				$doc->addField(Zend_Search_Lucene_Field::Text('status', $row['status']));
				$doc->addField(Zend_Search_Lucene_Field::Text('witel', $row['witel'])); 
				$doc->addField(Zend_Search_Lucene_Field::Text('kandatel', $row['kandatel']));
				$doc->addField(Zend_Search_Lucene_Field::Text('cust_name', $row['cust_name'])); 
				$doc->addField(Zend_Search_Lucene_Field::Keyword('alamat', $row['alamat']));
				$doc->addField(Zend_Search_Lucene_Field::Keyword('neighbor', $row['neighbor']));
				$doc->addField(Zend_Search_Lucene_Field::Keyword('ip_address', $row['ip_address']));
				$doc->addField(Zend_Search_Lucene_Field::Keyword('cust_serv_abbr', $row['cust_serv_abbr']));
				$doc->addField(Zend_Search_Lucene_Field::Keyword('hst_name2', $row['hst_name2']));
				$index->addDocument($doc);

			}
			$index->optimize();
			$this->view->count= $index->numDocs();
			
        
    }
    public function getdeviceAction()
    {
       	$this->view->headScript()->appendFile('/assets/coresystem/libs/colorbox/jquery.colorbox.js');
		$this->view->headLink()->appendStylesheet('/assets/coresystem/libs/colorbox/colorbox.css');
		 $this->view->headLink()->appendStylesheet('/assets/rafinaru/css/bit.css');	
		$path = (APPLICATION_PATH . '/indexes/devices');
		if($_GET['keyword'])  
		{

			$cc =  new Model_Zsearch();
			$query = $cc->get_search($_GET['keyword']);
			
			//Zend_Debug::dump($query); // die();
			$this->view->query =  $query ; 
		
			
		}  
		
		
		
	}
	/*  getdeviceAction() OLD
	 public function getdeviceAction()
    {
       
		$path = (APPLICATION_PATH . '/indexes/devices');
		if($_GET['keyword'])  
		{
			$query = Zend_Search_Lucene_Search_QueryParser::parse($_GET[keyword]);
			$index = Zend_Search_Lucene::open($path);	
			$hits = $index->find($query);
			$this->view->hits =  $hits; 
		
			
		}  
		
		
		
	} */
}
?>
