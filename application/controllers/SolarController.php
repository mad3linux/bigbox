<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class SolarController extends Zend_Controller_Action
{
public function init (){    

}


public function generatedeventAction () {

	$this->view->headScript()->appendFile('/assets/core/plugins/highcharts4.0.4/js/highcharts.js');	
	$this->view->headScript()->appendFile('/assets/core/plugins/highcharts4.0.4/js/highcharts-more.js');	
	$this->view->headScript()->appendFile('/assets/core/plugins/highcharts4.0.4/js/modules/exporting.js');	
	$this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-fileupload/bootstrap-fileupload.css');
	$this->view->headLink()->appendStylesheet('/assets/core/skins/default/css/pages/timeline.css');
	$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-fileupload/bootstrap-fileupload.js');
	$this->view->headScript()->appendFile('/assets/core/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js');
	$this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
	$this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
	$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
	$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
	$this->view->headScript()->appendFile('/assets/core/js/graphindex-modals.js');
	$this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
	$this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
	$this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css');
	$this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datetimepicker/css/datetimepicker.css');
	$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
	$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
	$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-daterangepicker/moment.min.js');
	$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-daterangepicker/daterangepicker.js');
	$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js');
	$this->view->headScript()->appendFile('/assets/core/plugins/date-id-ID.js');
	$this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
	
	$this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
	$this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
	$this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
	$this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
	
	
	$params=$this->getRequest()->getParams();
	$d = new Model_Event();
	$data  = $d->get_event_temp($params['id']); 
	//Zend_Debug::dump($data); die();
	$v_event = unserialize(base64_decode($data['data']));
	
	$this->view->v_event  = $v_event;
	
}
public function indexAction (){


try {
	
//$new = new Model_Socket();
//$new->getRedis();
	
$redis = new redisent\Redis('radis://radisTelkom:zaKNGP8P6B8bj8gFCbXzwk5RDeBtuq8H@10.62.29.105:6379');
$data = $redis->select(3);
$data = $redis->zrevrange('INSTANT_OFFERING:HIT:WEBDOMAIN', 0, 5, 'withscores');

//zrevrange INSTANT_OFFERING:F5:HIT:SPEEDY  0 5 withscores

Zend_Debug::dump($data); die();
} catch(Exception $e) {

	Zend_Debug::dump($e);die();
} 


}	


public function builderAction (){
	$this->view->headScript ()->appendFile ( '/assets/core/plugins/drag/_scripts/jquery-2.0.2.min.js' );
	$this->view->headScript ()->appendFile ( '/assets/core/plugins/drag/_scripts/jquery-ui-1.10.4.min.js' );
	$this->view->headScript ()->appendFile ( '/assets/core/plugins/drag/_scripts/newsletter-builder.js' );
	
	$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/drag/_css/Icomoon/style.css' );
	$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/drag/_css/newsletter-builder.css' );
	$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/drag/_css/newsletter.css' );

}

public function ajaxerAction (){

	$this->_helper->layout->disableLayout();
	$this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
	$this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');

	$this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
	$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
	$this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
	$this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
	$this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
	$this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
	$params=$this->getRequest()->getParams();
	//Zend_Debug::dump($params); die();
	
$cc = new Model_Eventexec();
$sublocs = $cc->get_sub_location($params['val']);

	$result = '<select onchange="stochange(this)" size="2" class="him_multi_select" id="sto'.$params['ID'].'" name="sto1" style="width:100%;padding:5px;font-size:15px;height:200px">';
	foreach ($sublocs as $k=>$v){
		//$str = iconv("UTF-8","UTF-8//IGNORE",$v['sto_description']);
		//Zend_Debug::dump($str);
		$result .= '<option value="'.$v['ID'].'|'.$v['SUB_LOCATION_NAME'].'">'.$v['SUB_LOCATION_NAME'].'</option>';
	}
	
	
	$result .= '</select>';

//Zend_Debug::dump($sublocs); die();
//$sublocs = $cc->get_sub_location();
	header("Access-Control-Allow-Origin: *");
	header('Content-Type: application/json');
	echo json_encode($result);
	die();
}

	
public function eventAction (){

$v = array(
'title'=>'KAA', 
'city'=>'Bandung',
'address'=>'Jalan Asia Afirka',
'validfrom'=>'09/01/2015',
'validuntil'=>'09/01/2015',
'img_logo'=>'1.png',
'img_wifi'=>'1.png',
'img_tsel'=>'1.png',
'img_datin'=>'1.png',
'img_cctv'=>'1.png',
'img_wifi'=>'1.png',
'img_top'=>'1.png',
'img_middle'=>'1.png',
'img_bottom'=>'1.png',
'num_top'=>'100',
'num_midlow'=>'70',
'num_midtop'=>'00',
'sublocation'=>array(1, 2, 5, 7),
'model_api'=>'Model_Eventexec',
'location'=>array(1, 2),
'tab'=>array(
array('title'=>'surveillance', 'key'=>'surveillance', 'portlet'=>array(array('title'=>'Media Center', 'key'=>'M'), array('title'=>'Akomodasi', 'key'=>'A'), array('title'=>'Security', 'key'=>'S'), array('title'=>'International Airport', 'key'=>'I'), array('title'=>'Venue', 'key'=>'V'), array('title'=>'Summary', 'key'=>'SUM'))),


array('title'=>'Link Monitoring', 'key'=>'linkmonitoring', 'portlet'=>array(array('title'=>'LINK ASTINET', 'key'=>'img_url', 'content'=>array('/images/LINK_ASTINET_MEDIA_CENTER/1.png')))),

array('title'=>'TICKET', 'key'=>'ticket', 'portlet'=>array(array('title'=>'WIFI', 'key'=>'wifi_ticket'))),

array('title'=>'REPORT', 'key'=>'report', 'portlet'=>array(array('title'=>'laporan', 'key'=>'form_url', 'contents'=>"<pre>Pak Awal Ykh

Kami laporkan status fastel VIP TelkomGroup di area MASIV lokasi Jakarta dan Bandung pada AACC 2015 posisi tgl 01 September pukul <input id='input1' name='input1'> wib sbb :

1. Secara umum fastel berfungsi <input id='input2' name='input2'> dengan rincian terlampir

2. Trafik pada core normal dengan pemakaian tertinggi terjadi pada pukul <input id='input3' name='input3'>  sebesar <input id='input4' name='input4'> . 
 
3. Aktivitas Posko hari ini difokuskan :
Jakarta : <textarea id='input5' name='input5'></textarea>
Bandung : <textarea id='input6' name='input6'></textarea>
 
4. Petugas Posko : 
Hari ini, Tuesday, 01 September 2015 :
DM Posko Semanggi : <input id='input7' name='input7'>
DM Posko Bandung : <input id='input8' name='input8'>

Besok : Wednesday, 02 September 2015 :
DM Posko Semanggi : <input id='input9' name='input9'>
DM Posko Bandung : <input id='input10' name='input10'>


5. Related Info :
<textarea id='input11' name='input11'></textarea>



Demikian. Tks



----------------------
Lampiran :

Status Fastel TGroup posisi 01 September 2015 jam <input id='input12' name='input12'> wib

1. Rekap Fastel di lokasi MASIV

Lokasi Jakarta

M Center/JCC Hall B/Astinet 10 Gbps/ Ok
M Center/JCC Hall B/ Wifi 8 AP 1 Gbps/Ok
M Center/JCC Hall B/ Sinyal Tsel/Ok
M Center/JCC Hall B/SGN 1 Unit/ok

Akomodasi/Hotel Sultan/wifi ID 1 Gbps/ok
Akomodasi/Hotel Sultan/Sinyal TSel/ok
Akomodasi/Hotel pendukung (18)/ Wifi id/ok
Akomodasi/Hotel pendukung (18)/ Sinyal Tsel/ok

Security/Puskodal/Astinet 20 Mbps/ok
Security/cctv Puskodal/Metro, VPN 100 Mbps/ok
Security/Paspampres/ Astinet 20 Mbps/ok
Security/TNI AD/AStinet 20 Mbps/ok
Security/ CCTV Puskotis/ VPN IP 3 titik 25 Mbps/ok


Int. Airport/Bandara Soeta/ Wifi Id 160 AP/ Ok
Int. Airport/Bandara Soeta/ Sinyal Tsel/ Ok
Int. Airport/Bandara Halim/ Wifi id 45 Ap/Ok
Int. Airport/Bandara Halim/ Sinyal Tsel/Ok

Venue/JCC Meeting Room/Astinet 50 Mbps/ok
Venue/JCC Hall A/Astinet 100 Mbps/ok
Venue/JCC-all/Wifi ID 1 Gbps/ok
Venue/JCC-all/Sinyal Tsel/ok


Lokasi Bandung

M Centre/Hotel Ibis/Astinet 2 Gbps/ok
M Centre/Hotel Ibis/Wifi id 7 AP 1 Gbps/ok
M Centre/Hotel Ibis/Sinyal Tsel/ok
M Centre/Hotel Ibis/SNG 1 Unit/ok
M Centre/New Majestik/Astinet 2 Gbps/ok
M Centre/New Majestik/Wifi id 6 AP 1 Gbps/ok
M Centre/New Majestik/Sinyal Tsel/ok
M Centre/PGN/ Astinet 2 Gbps/ok
M Centre/PGN/ Wifi id 13 AP 1 Gbps/ok
M Centre/PGN/ Sinyal Tsel/ok


Accomodation/Hotel Savoy/wifi 10 Ap 500 Mbps/ok
Accomodation/Hotel Savoy/Sinyal Tsel/ok

Security/Paspampres/Astinet 20 Mbps/ok
Security/ BIN/ Astinet 7 link 120 Mbps/ok
Security/ CCTV Puskotis/ VPN IP 4 titik 95 Mbps/ok


Int. Airport/Husein/wifi id 6 AP/ok
Int.Airport/Husein/Sinyal Tsel/ok

Venue/Savoy Homan/Metro 50 Mbps/ok
Venue/Balai Pakuan/ Metro 50 Mbps/ok
Venue/Majestik/Metro 50 Mbps/ok
Venue/Area Historical Walk/wifi id 1 Gbps/ok
Venue/Gedung Merdeka/Astinet 1 Gbps/ok
Venue/All site /Sinyal Tsel/ok


2. REKAP FASTEL TELKOMSEL
KAA Critical Network Incident Update
Type/Status/Reg/Loc/Start/Duration/End/Impact/Actions/Estimated End
<textarea id='input13' name='input13'></textarea>


3. REKAP FASTEL TELIN 
DATIN: <input id='input14' name='input14'> M of 10 G
VOICE: <input id='input15' name='input15'>; ACD : <input id='input16' name='input16'>; ASR: <input id='input17' name='input17'>
TOTAL BACKBONE INTERNATIONAL <input id='input18' name='input18'> of <input id='input19' name='input19'>

GBR: <input id='input20' name='input20'>
DMI: <input id='input21' name='input21'>
BTM: <input id='input22' name='input22'>
SBY: <input id='input23' name='input23'>
SMG: <input id='input24' name='input24'>


Demikian kami sampaikan
Duty Manager :<input id='input25' name='input25'>
Deputy DM : <input id='input26' name='input26'>
Telp posko: 021-5740400</pre>"))), 
array('title'=>'COLAB', 'portlet'=>array(array('title'=>'WIFI', 'key'=>'collab'))),
array('title'=>'POSKO', 'portlet'=>array(array('title'=>'Jadwal Posko', 'key'=>'posko'))),
array('title'=>'INFO', 'portlet'=>array(array('title'=>'Jadwal Posko', 'key'=>'presentation_url', 'folder'=>'/images/Agenda Kegiatan'))),
array('title'=>'TRAFFIC', 'portlet'=>array(array('title'=>'Traffic', 'key'=>'traffic')))

)
);

//echo base64_encode(serialize($v));die();
$c = new Model_Event();
$icon['tsel'] = $c->get_icon("tsel");
$icon['datin'] = $c->get_icon("datin");
$icon['cctv'] = $c->get_icon("cctv");
$icon['wifi'] = $c->get_icon("wifi");
$icon['toprange'] = $c->get_icon("toprange");
$icon['midrange'] = $c->get_icon("midrange");
$icon['bottomrange'] = $c->get_icon("bottomrange");
$icon['unknownrange'] = $c->get_icon("unknownrange");

$cc = new Model_Eventexec();
$locs = $cc->get_location();
$sublocs = $cc->get_sub_location($locs[0]['ID']);
//Zend_Debug::dump($sublocs); die();

$this->view->locs =  $locs;
$this->view->sublocs =  $sublocs;
//Zend_Debug::dump($data); die();
$this->view->icons =$icon;
$this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
$this->view->headScript()->appendFile('/assets/core/plugins/jquery-validation/dist/additional-methods.min.js');
$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js');
$this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
$this->view->headScript()->appendFile('/assets/core/js/eventwizard-form-wizard2.js');

$this->view->headLink()->appendStylesheet('/assets/core/plugins/tag-it/css/jquery.tagit.css');
$this->view->headLink()->appendStylesheet('/assets/core/plugins/tag-it/css/tagit.ui-zendesk.css');
$this->view->headScript()->appendFile('/assets/core/plugins/tag-it/js/tag-it.js');

$this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
$this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
$this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css');
$this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datetimepicker/css/datetimepicker.css');
$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js');
$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-daterangepicker/moment.min.js');
$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-daterangepicker/daterangepicker.js');
$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js');
$this->view->headScript()->appendFile('/assets/core/plugins/date-id-ID.js');

$this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
$this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');

$this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');




}

public function gettabpaneAction(){
	
	try{
		$authAdapter = Zend_Auth::getInstance();
		$identity = $authAdapter->getIdentity();
	}catch(Exception $e){
		
	}


	$params = $this->getRequest()->getParams();	
	//Zend_Debug::dump($params); die();
	$c = new Model_Event();
	$data  = $c->get_event_temp($params['id']); 
	//Zend_Debug::dump($data); die();
	$v_event = unserialize(base64_decode($data['data']));
	foreach ($v_event['tab'] as $z) {
			$new[$z['key']] = $z;
			foreach ($z['portlet'] as $zz) {
				$new[$z['key']]['newportlet'][$zz['key']]=$zz;	
				//Zend_Debug::dump($zz);
			}
	}
	
	//Zend_Debug::dump($new);
	$out = "";
	foreach ($new[$params['tpanename']]['newportlet'] as $zzz) {
		$out .= $this->getportlet($zzz['key']);	
	}
echo $out;
die();
}

function getportlet($key){
	
		switch ($key) {
			case  'M':
			case 'A':
		
			case 'S':
		
			case 'I':
			
			case 'V':
			
		return 	$this->get_masiv($key, $v_event ); 
			break;
			
			//case :
			//break;
			
			//case :
			//break;	
			
		}
		
		return "<h1>asdasd</h1>";
		
	}
	
	
	
	function get_masiv($key, $v_event) {
		
			
	echo '<div class="row">';
	foreach($data as $k=>$v){
	echo '<div class="col-md-'.$arr_col[$k].'">
			<div class="panel panel-default" style="margin-bottom:10px;">
				<div class="panel-heading">
					<h3 class="panel-title"><b style="color:red;">'.$k.'</b>'.$arr_masiv[$k].'</h3>
				</div>
				<div class="panel-body square1-body" style="background-color:#404040;color:white;padding:10px 0px 0px 0px;text-align:center;vertical-align:middle;min-height:155px;">';
		foreach($v as $k2=>$v2){
			// Zend_Debug::dump($v2);
			$img = '/images/ICON/CIRCLE/circle_'.(count($v2['MAX_STATUS'])>0?$arr_color_status[$v2['MAX_STATUS']]:'bwhite_fgrey').'';
			// if($v2['ALIAS_NAME']=='SOETTA'){
				// $img = '/images/ICON/EVENT/event_bwhite_fblue.png';
			// }
			
			echo '<div style="position:relative; border:0px solid yellow; width:65px; height:65px; text-align:center; display:inline-block; margin:2px;">';
					
					
					
					
					echo'
					<!-- LOCATION EVENT -->					
					<span style="position:absolute; width:100%; border:0px solid green; left:0px;  top:0px;">
						<a class="csr" href="#'.$v2['LOC_ID'].'"" data-tpanename="detailsurveillance" data-event_id="'.$v2['EVENT_ID'].'" data-loc_id="'.$v2['LOC_ID'].'" onclick="getTabPane(this);"><img src="'.$img.'" style="width:40px;"></a>
					</span>
					
					<!-- LAYANAN -->';
					
					if(count($v2['WIFI'])>0){
						echo '<span style="position:absolute; border:0px solid green; left:4px;  top:-2px;">
						<a class="csr" href="javascript:;" data-tpanename="detailsurveillance" data-event_id="'.$v2['EVENT_ID'].'" data-loc_id="'.$v2['LOC_ID'].'" data-service="wifi" onclick="getTabPane(this);"><img src="/images/ICON/ACCESS POINT/access_'.$arr_color_status[$v2['WIFI']['STATUS']].'" style="width:18px;"></a>
					</span>';
					}
					
					
					if(count($v2['TELKOMSEL'])>0){
					echo '<span style="position:absolute; border:0px solid green; right:4px;  top:-2px;">
						<a class="csr" href="javascript:;" data-tpanename="detailsurveillance" data-event_id="'.$v2['EVENT_ID'].'" data-loc_id="'.$v2['LOC_ID'].'" data-service="telkomsel" onclick="getTabPane(this);"><img src="/images/ICON/TELKOMSEL/tsel_'.$arr_color_status[$v2['TELKOMSEL']['STATUS']].'" style="width:18px;"></a>
					</span>';
					}
					
					
					if(count($v2['DATIN'])>0){
						echo '<span style="position:absolute; border:0px solid green; left:5px;  bottom:26px;">
						<a class="csr" href="javascript:;" data-tpanename="detailsurveillance" data-event_id="'.$v2['EVENT_ID'].'" data-loc_id="'.$v2['LOC_ID'].'" data-service="datin" onclick="getTabPane(this);"><img src="/images/ICON/SQUARE/square_'.$arr_color_status[$v2['DATIN']['STATUS']].'" style="width:15px;"></a>
					</span>';
					}
					
					if(count($v2['CCTV'])>0){
						echo '<span style="position:absolute; border:0px solid green; right:5px;  bottom:26px;">
						<a class="csr" href="javascript:;" data-tpanename="detailsurveillance" data-event_id="'.$v2['EVENT_ID'].'" data-loc_id="'.$v2['LOC_ID'].'" data-service="cctv" onclick="getTabPane(this);"><img src="/images/ICON/CCTV/cctv_'.$arr_color_status[$v2['CCTV']['STATUS']].'" style="width:15px;"></a>
					</span>';
					}
					
					
					echo'
					<span style="position:absolute; width:100%; left:0px; top:42px; text-align:center; font-size:8px;">
						'.$v2["ALIAS_NAME"].'
					</span>
				</div>';
		}
					
		echo '	</div>
			</div>
		</div>';
		
		if($k=='A'){
			echo '</div>';
			echo '<div class="row">';
		}
	}	
	echo '</div>';
		
		
	}
}
