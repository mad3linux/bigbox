<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
 
//require_once (APPLICATION_PATH) . '/../library/CMS/Workflow/wftaskinterface.php';

class WorkflowController extends Zend_Controller_Action {

    public function init() {

        $ajaxContext = $this->_helper->getHelper('contextSwitch');
        $ajaxContext->addActionContext('ajaxer', 'json')->initContext();
        $ajaxContext->initContext();
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');

        $this->_cache = Zend_Registry::get('cache');

        $this->initView();
    }

    public function indexAction() {
        $var = new Model_Znode();
        // $var->getNodeByType('articles');
    }

    public function editAction() {
        
    }

    public function displayAction() {
		//die('qq');
        //   $ct = new Model_Zwftemplatedatanext();
        // $ct->fetchallbyids(12,3);
        
        $params=$this->getRequest()->getParams();
		$this->view->varams = $params; 
		if($params[v]=='ajax') 
		{
			$this->_helper->layout->disableLayout();
		}
		$this->view->headScript()->appendFile('/assets/coresystem/js/jquery.simplemodal.min.js');
		$this->view->headLink()->appendStylesheet('/assets/coresystem/css/maestro.css');
		$this->view->headScript()->appendFile('/assets/coresystem/js/admin_template_editor.js');
		$this->view->headScript()->appendFile('/assets/coresystem/js/jquery.contextmenu.js');

        $this->view->headScript()->appendFile('/assets/coresystem/js/maestro_structure_admin.js');

        $this->view->headScript()->appendFile('/assets/coresystem/js/moderator.js');
        
        $this->view->headScript()->appendFile('/assets/coresystem/js/taskconsole.js');
        
        $this->view->headScript()->appendFile('/assets/coresystem/js/wz_jsgraphics.js');
       

			
        $params = $this->getRequest()->getParams();
        $datatemp = null;
        $task = array();

        $template_id = $this->getRequest()->getParam('id');
			
        //die($template_id);
        $c = new CMS_Workflow_wfinterface($template_id);
        $c2 = new Model_Zwftemplate();
       
        $datatemp = $c2->fetchdata($template_id);
        //Zend_Debug::dump($datatemp); die();
        
        $var = $c->displayPage();
		//Zend_Debug::dump($var); die('www');
        //print $c->initializeJavascriptArrays();die();

        $this->view->temp = $datatemp;
        $this->view->cwf = $var;


        ///get display tasks

        $c3 = new Model_Zwftemplatedata();

        $c3->steppingworkflow($template_id);

        $res = $c3->fetchdatabytid($template_id);

        //Zend_Debug::dump($res); die();

        foreach ($res as $rec) {
            $task_type = substr($rec->TASK_CLASS_NAME, 15);
            $task_class = 'MaestroTaskInterface' . $task_type;

			//Zend_Debug::dump($task_class);//die('www');
			//Zend_Debug::dump($rec->ID);//die('www');
            if (class_exists($task_class)) {
				//die('www');
                $ti = new $task_class($rec->ID);
				//Zend_Debug::dump($ti);die('www');
            } else {

                $ti = new MaestroTaskInterfaceUnknown($rec->ID, 0, $task_class);
            }
			//Zend_Debug::dump($ti);die('www');

            $restask = $c3->fetchdatabyid($rec->ID);
            $task_type = substr($restask->TASK_CLASS_NAME, 15);
            $task_class = 'MaestroTaskInterface' . $task_type;

            $task[$rec->ID]['res'] = $restask;
            $task[$rec->ID]['ti'] = $ti;
            $task[$rec->ID]['task_class'] = $task_class;
        }

        $this->view->task = $task;

        $this->view->menu = $c->getContextMenu();
    }

    public function ajaxerAction() {
        $ctype = $this->getRequest()->getParam('ctype');

        $taskid = $this->getRequest()->getParam('taskid');
        $tempid = $this->getRequest()->getParam('tempid');
        $action = $this->getRequest()->getParam('act');

        $ti = new $ctype($taskid, $tempid);

        $out = ($ti->$action());
        //die($out);
        //Zend_Debug::dump($out); die("aa");
        $this->view->out = $out;
    }

    public function listAction() {

        //'variables' => array('tid' => NULL, 'operation' => NULL, 'edit_var' => 0)
        $var = new Model_Zwftemplate();
        $this->view->num_records = $var->fetchalldata()->count();

        $this->view->data = $var->fetchalldata()->toArray();

        //Zend_Debug::dump($var->fetchalldata()->toArray());
    }

    public function launchAction() {
        $auth = Zend_Auth::getInstance();
        $identity = $auth->getIdentity();

        $cform = $this->view->form = new Form_Zwffilter();
        $cform->setAction('/workflow/launch');
        $var = new Model_Zwftemplate();
        $cdb3 = new Model_Zwfproject();
        $cdb4 = new Model_Zwfprocess();
        $cdb5 = new Model_Zwfnode();
        $cdb6 = new Model_Zwfqueue();

        $cdb7 = new Model_Zwftemplatedata();


        $cform->setMethod('post');
        $cform->setAttrib('class', 'flat');

        if ($this->_request->isPost() && $cform->isValid($_POST)) {
            $post = $cform->getValues();

            $started = $cdb7->getstarted($post['segment']);
//        Zend_Debug::dump($started); die();


            $cResult = $var->fetchdata($post['segment']);

            $data3 = array(
                'PROJECT_CODE' => "",
                'PROJECT_NAME' => "",
                'PROJECT_DESCR' => "",
                'ACCOUNT_MANAGER' => "",
                'START_DATE' => NULL,
                'END_DATE' => NULL,
                'REVENUE_ESTIMATE' => NULL,
                'NOKES_ESTIMATE' => NULL,
                'BILLING_ESTIMATE' => NULL
            );

            $car3 = $cdb5->createitem($data3);

            $data = array(
                'PROJECT_NUM' => '',
                'ORIGINATOR_UID' => $identity->ID,
                'DESCRIPTION' => $cResult->TEMPLATE_NAME,
                'STATUS' => 0,
                'PREV_STATUS' => 0,
                'RELATED_PROCESSES' => '0',
                'PROJECT_CONTENT_ID' => $car3->ID
            );


            $car = $cdb3->createdata($data);

            $data2 = array(
                'TEMPLATE_ID' => $post['segment'],
                'FLOW_NAME' => $cResult->TEMPLATE_NAME,
                'COMPLETE' => 0,
                'INITIATOR_UID' => $identity->ID,
                'PID' => 0,
                'INITIATING_PID' => $car->ID,
                'TRACKING_ID' => $car->ID
            );

            $car2 = $cdb4->createdata($data2);

            $data6 = array(
                'PROCESS_ID' => $car2->ID,
                'TEMPLATE_DATA_ID' => $started->ID,
                'TASK_CLASS_NAME' => $started->TASK_CLASS_NAME,
                'IS_INTERACTIVE' => 0,
                'SHOW_IN_DETAIL' => 0,
                'HANDLER' => '',
                'TASK_DATA' => '',
                'TEMP_DATA' => '',
                'STATUS' => 0,
                'ARCHIVED' => 1,
                'RUN_ONCE' => 0,
                'ZUID' => '',
                'PREPOPULATE' => '',
                'COMPLETED_DATE' => NULL,
                'STARTED_DATE' => NULL,
                'NEXT_REMINDER_TIME' => '',
                'NUM_REMINDER_SENT' => 0,
            );

            //die("a");
            $cdb6->createitem($data6);
        }

        $this->view->form = $cform;

        $launch = $cdb4->querylaunch($identity->ID);

        //  Zend_Debug::dump($launch); die();
        $this->view->launch = $launch;

        $this->view->num_records = $var->fetchalldata()->count();

        $this->view->data = $var->fetchalldata()->toArray();

        $this->view->messages = $this->_flashMessenger->getMessages();
    }

    public function createdAction() {
        $auth = Zend_Auth::getInstance();
        $identity = $auth->getIdentity();
        $cdb = new Model_Zwfprocess();
        $cdb2 = new Model_Zwfnode();
        $cdb3 = new Model_Zwftemplatedata();
        $cdb4 = new Model_Zwftdnodeassign();
        $crole = new Model_Zrole();
        $cgroup = new Model_Zog();
        $roles = $crole->fecthdata();
        $roarray = array();
        $grarray = array();
        foreach ($roles as $rk => $rv) {
            $roarray[$rv['ROLE_ID']] = $rv;
        }
        $grs = $cgroup->fetchdata();
        foreach ($grs as $rk => $rv) {
            $grarray[$rv['ID']] = $rv;
        }
        $this->view->params = $this->getRequest()->getParams();
        if ($_POST && isset($_POST['save'])) {
            //die("as");
           // Zend_Debug::dump($_POST);
            $cdb4->deletedatabynid($_POST['nid']); // die();   
            $cdb2->updatedata($_POST);
            //Zend_Debug::dump($_POST);die();
            foreach ($_POST['pic_am'] as $pkey => $pval) {
                if (($pval) != NULL) {
                    $var = explode(';', $pval);
                    foreach ($var as $key => $val) {
                        if (trim($val) != NULL) {
                            $varx = explode('|', $val);
//  Zend_Debug::dump($varx); die();  
                            $data = array(
                                'TEMPLATE_DATA_ID' => $pkey,
                                'ASSIGN_TYPE' => 1,
                                'ASSIGN_BY' => $identity->ID,
                                'ASSIGN_ID' => trim($varx[2]),
                                'NODE_ID' => $_POST['nid'],
                                'PROCESS_ID' => $_POST['pid'],
                                'DUE_DATE' => new Zend_Db_Expr("to_date('" . $_POST['schedule'][$pkey] . "', 'DD/MM/YYYY')")
                            );
                            $_req1 = $cdb4->createitem($data);


                            if ($_req1) {
                                $this->_flashMessenger->addMessage('Data Berhasil Disimpan|ok');
                            } else {
                                $this->_flashMessenger->addMessage('Data Gagal Disimpan|error');
                            }
                        }
                    }
                }
            }
        }

        if ($_POST && isset($_POST['launch'])) {
            $cdb5 = new Model_Zwfqueue();
            $cdb6 = new Model_Zwftemplatedatanext();
            $st = $cdb5->fetchdatastarted($_POST['pid']);
//        Zend_Debug::dump($st); die();
            $datacomp = array(
                'ZUID' => 1,
                'ID' => $st['ID']
            );
//         Zend_Debug::dump($datacomp); die();
            $cdb5->updatecomplete($datacomp);
            $get = $cdb6->fetchdatafromarr($st['TEMPLATE_DATA_ID']);
            $tdata = $cdb3->fetchdatabyid($get['TEMPLATE_DATA_TO']);
            //Zend_Debug::dump($tdata); die();
            $data8 = array(
                'PROCESS_ID' => $_POST['pid'],
                'TEMPLATE_DATA_ID' => $get['TEMPLATE_DATA_TO'],
                'TASK_CLASS_NAME' => $tdata['TASK_CLASS_NAME'],
                'IS_INTERACTIVE' => 0,
                'SHOW_IN_DETAIL' => 0,
                'HANDLER' => '',
                'TASK_DATA' => '',
                'TEMP_DATA' => '',
                'STATUS' => 0,
                'ARCHIVED' => 1,
                'RUN_ONCE' => 0,
                'ZUID' => '',
                'PREPOPULATE' => '',
                'COMPLETED_DATE' => NULL,
                'NEXT_REMINDER_TIME' => '',
                'NUM_REMINDER_SENT' => 0,
            );
            $crx = $cdb5->createitem($data8);
            $cdb9 = new Model_Zwfqueuefrom();
            $data9 = array(
                'QUEUE_ID' => $crx->ID,
                'FROM_QUEUE_ID' => $st['ID']
            );
            $cdb9->createitem($data9);
            $this->_redirect('/workflow/launch');
        }

        $param = $this->_request->getParam('ID');
        $data = $cdb->querydata($param);
        $this->view->data = $data;
        $data3 = $cdb3->steppingworkflow($data['TEMPLATE_ID']);

        $allpicuser = $cdb4->fetchdatabynidbyuser($data['ID']);

        $this->view->pic = $data3;

        $allpic = $cdb4->fetchdatabynid($data['ID']);
        //Zend_Debug::dump($allpic); 
//  Zend_Debug::dump($allpic);die();
        if ($allpic) {
            $parray = array();
            foreach ($allpic as $key => $val) {
                if ($val['ASSIGN_TYPE'] == 2) {
                    $parray[$val['ID']] = array(
                        'ASSIGN_TYPE' => $val['ASSIGN_TYPE'],
                        'ASSIGN_BY' => $val['ASSIGN_BY'],
                        'TEMPLATE_DATA_ID' => $val['TEMPLATE_DATA_ID'],
                        'ASSIGN_ID' => $roarray[$val['ASSIGN_ID']],
                    );
                }
                if ($val['ASSIGN_TYPE'] == 3) {
                    $parray[$val['ID']] = array(
                        'ASSIGN_TYPE' => $val['ASSIGN_TYPE'],
                        'ASSIGN_BY' => $val['ASSIGN_BY'],
                        'TEMPLATE_DATA_ID' => $val['TEMPLATE_DATA_ID'],
                        'ASSIGN_ID' => $grarray[$val['ASSIGN_ID']],
                    );
                }
                if ($val['ASSIGN_TYPE'] == 1) {
                    $parray[$val['ID']] = array(
                        'ASSIGN_TYPE' => $val['ASSIGN_TYPE'],
                        'ASSIGN_BY' => $val['ASSIGN_BY'],
                        'TEMPLATE_DATA_ID' => $val['TEMPLATE_DATA_ID'],
                            // 'ASSIGN_ID'=>$grarray[$val['ASSIGN_ID']],
                    );
                }
            }
        }

        //Zend_Debug::dump($data3); die();

        $userP = array();
        foreach ($allpicuser as $pic) {
            $userP[$pic['TEMPLATE_DATA_ID']] = $pic;
        }

        $this->view->picu = $userP;
        //Zend_Debug::dump($userP); die();
        //$this->_redirect();
    }

    public function xajaxerAction() {
		//die('tes');
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $c = new Model_Zamnik();
        //$list = $c->autocomplete(strtoupper($_GET['name_startsWith']));
        $list = $c->autoAM(strtoupper($_GET['name_startsWith']));
        echo $_GET['callback'] . '(' . json_encode($list) . ');';
        die();
    }

    public function xxajaxerAction() {

        $param = $this->_request->getParam('PAR');
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $c = new Model_Zamnik();

        if ($param == "NIK") {
            $list = $c->autocomplete(strtoupper($_GET['term']));
            $data = array();
            foreach ($list as $k => $l) {
                $data[$k]['id'] = $l['NIK'] . "|" . $l['FULLNAME'];
                $data[$k]['label'] = $l['NIK'] . "|" . $l['FULLNAME'];
                $data[$k]['value'] = $l['NIK'] . "|" . $l['FULLNAME'] . "|" . $l['ID'];
            }
        } elseif ($param == "ROLE") {

            $list = $c->autocompleterole(strtoupper($_GET['term']));
            $data = array();
            foreach ($list as $k => $l) {
                $data[$k]['id'] = $l['ROLE_NAME'] . "|" . $l['ROLE_ID'];
                $data[$k]['label'] = $l['ROLE_NAME'] . "|" . $l['ROLE_ID'];
                $data[$k]['value'] = $l['ROLE_NAME'] . "|" . $l['ROLE_ID'];
            }
        } elseif ($param == "GROUP") {

            $list = $c->autocompletegroup(strtoupper($_GET['term']));
            $data = array();
            // die("a");
            foreach ($list as $k => $l) {
                $data[$k]['id'] = $l['GROUP_NAME'] . "|" . $l['ID'];
                $data[$k]['label'] = $l['GROUP_NAME'] . "|" . $l['ID'];
                $data[$k]['value'] = $l['GROUP_NAME'] . "|" . $l['ID'];
            }
        }
        ///Zend_Debug::dump($list); die();
        echo json_encode($data);
        die();
    }

    public function testAction() {
        
    }

    public function taskconsoleinboxAction() {
        $auth = Zend_Auth::getInstance();
        $identity = $auth->getIdentity();

        
       
        $data = array(
            'UID' => $identity->ID
        );

        $cdb1 = new Model_Zwfqueue();
        $boxes = $cdb1->taskinbox($data);

         //Zend_Debug::dump($boxes); die();

        $this->view->inbox = $boxes;
        $this->view->messages = $this->_flashMessenger->getMessages();
    }

    public function inboxdetilAction() {
        //$this->_helper->layout->disableLayout();
        $this->view->params = $this->getRequest()->getParams();
        $params = $this->getRequest()->getParams();
        
        $auth = Zend_Auth::getInstance();
        $identity = $auth->getIdentity();
        $cdb = new Model_Zwfprocess();
        $cdb2 = new Model_Zwfnode();
        $cdb3 = new Model_Zwftemplatedata();
        $cdb4 = new Model_Zwftdnodeassign();
        $cdb5 = new Model_Zwfqueue();
        $cdb6 = new Model_Zwftemplatedatanext();
        $currdata = $cdb3->fetchdatabyid($params["TDID"]);
        $cproj = new Model_Zwfproject();
        //Zend_Debug::dump($currdata); die(); 	


        $crole = new Model_Zrole();
        $cgroup = new Model_Zog();
        $roles = $crole->fecthdata();
        $roarray = array();
        $grarray = array();
        foreach ($roles as $rk => $rv) {

            $roarray[$rv['ROLE_ID']] = $rv;
        }

        $grs = $cgroup->fetchdata();
        foreach ($grs as $rk => $rv) {

            $grarray[$rv['ID']] = $rv;
        }



        $proses = $cdb5->fetchdatacurrent($params['ID']);
        $this->view->proses = $proses;


        if ($_POST && isset($_POST['launch'])) {
           
            $st = $cdb5->fetchdatacurrent($_POST['pid']);
            
            $get = $cdb6->fetchdatafromarr($st['TEMPLATE_DATA_ID']);

            $tdata = $cdb3->fetchdatabyid($get['TEMPLATE_DATA_TO']);
            $datacomp = array(
                'ZUID' => 1,
                'ID' => $st['ID'],
                'STATUS_DESCR'=>$_POST["STATUS_DESCR"]
                
            );

            
            
            $cdb5->updatecomplete($datacomp);
            
          
            
            $data8 = array(
                'PROCESS_ID' => $_POST['pid'],
                'TEMPLATE_DATA_ID' => $get['TEMPLATE_DATA_TO'],
                'TASK_CLASS_NAME' => $tdata['TASK_CLASS_NAME'],
                'IS_INTERACTIVE' => 0,
                'SHOW_IN_DETAIL' => 0,
                'HANDLER' => '',
                'TASK_DATA' => '',
                'TEMP_DATA' => '',
                'STATUS' => 0,
                'ARCHIVED' => 1,
                'RUN_ONCE' => 0,
                'ZUID' => '',
                'PREPOPULATE' => '',
                'COMPLETED_DATE' => NULL,
                'NEXT_REMINDER_TIME' => '',
                'NUM_REMINDER_SENT' => 0,
            );

            $crx = $cdb5->createitem($data8);
            //new Zend_Db_Expr("SYSTDATE()");

            $cdb9 = new Model_Zwfqueuefrom();

            $data9 = array(
                'QUEUE_ID' => $crx->ID,
                'FROM_QUEUE_ID' => $st['ID']
            );
            $cdb9->createitem($data9);
            
            if($tdata['TASK_CLASS_NAME']=="MaestroTaskTypeEnd"){
                    
               //update prject =1 ketika projeck berakhir 
               $cproj->updatestat($params['NDID'],'1');
                
                
                $upda = array(
                    'ID' => $params['ID'],
                    'CURRENT_QUEUE' => $crx->ID,
                    'COMPLETE'=>'1',
                    'COMPLETED_DATE'=>new Zend_Db_Expr("SYSDATE")
                    
                );
            }else{
                $upda = array(
                    'ID' => $params['ID'],
                    'CURRENT_QUEUE' => $crx->ID,
                    'COMPLETE'=>'0',
                    'COMPLETED_DATE'=>""
                );
                
            }
            
            $cdb->updatecurrent($upda);
            
            $this->_flashMessenger->addMessage("You've been completed TASK|ok");
            $this->_redirect('/workflow/taskconsoleinbox');
        }

        $param = $this->_request->getParam('ID');

        $data = $cdb->querydataparent($param);
        $this->view->data = $data;

        
        
        //Zend_Debug::dump($data);die();

        $data3 = $cdb3->steppingworkflow($data['TEMPLATE_ID']);

        // Zend_Debug::dump($data); die();
      //  $data3x = $cdb3->steppingworkflowcurrent($data['TEMPLATE_ID'], 36);

       // Zend_Debug::dump($data3); die();




        $allpic = $cdb4->fetchdatabynidbyuser($data['ID']);

        $cdb10 = new Model_Zfiles();

        //Zend_Debug::dump($allpic);
        $pic = array();
        $fle = array();
        foreach ($data3 as $v) {

            if ($v['FIRST_TASK'] != 2) {
                
                $pic[$v['ID']] = $cdb4->fetchdatabynidtdid($params['NDID'], $v['ID']);
                
                $fle[$v['ID']] = $cdb10->fetchdatabynidtdid($params['NDID'], $v['ID']);
                $desc[$v['ID']] = $cdb5->fetchbypidtdid($params['ID'], $v['ID']);

               // Zend_Debug::dump($desc);die();
                $data33[$v['ID']] = array(
                    'TASK_NAME' => $v['TASK_NAME'],
                    'ID' => $v['ID'],
                    'PIC' => $pic[$v['ID']],
                    'FIES' => $fle[$v['ID']],
                    'FIRST_TASK' => $v['FIRST_TASK'],
                    'DESC' =>  $desc[$v['ID']],
                );
                
                
            }
        }

    // Zend_Debug::dump($data33);die();




        if ($allpic) {
            $parray = array();
            foreach ($allpic as $key => $val) {
                if ($val['ASSIGN_TYPE'] == 1) {

                    $parray[$val['TEMPLATE_DATA_ID']] = array(
                        'ASSIGN_TYPE' => $val['ASSIGN_TYPE'],
                        'ASSIGN_BY' => $val['ASSIGN_BY'],
                        'TEMPLATE_DATA_ID' => $val['TEMPLATE_DATA_ID'],
                        'ASSIGN_ID' => $val['NIK'] . '|' . $val['FULLNAME']
                    );
                }
            }
        }
        $this->view->tdata = $currdata;
        $this->view->pic = $data33;
    }

    public function uploadAction() {
//		include(APPLICATION_PATH."/easy_upload/upload_class.php"); //classes is the map where the class file is stored

        $upload = new CMS_Api();
        $upload->upload_dir = APPLICATION_PATH . '/plugins/tesupload';
        $upload->extensions = array('.png', '.jpg', '.zip', '.pdf'); // specify the allowed extensions here
        //$upload->rename_file = true;
        if (!empty($_FILES)) {
            $upload->the_temp_file = $_FILES['userfile']['tmp_name'];
            $upload->the_file = $_FILES['userfile']['name'];
            $upload->http_error = $_FILES['userfile']['error'];
            $upload->do_filename_check = 'y'; // use this boolean to check for a valid filename
            if ($upload->upload()) {

                echo '<div id="status">success</div>';
                echo '<div id="message">' . $upload->file_copy . ' Successfully Uploaded</div>';
                //return the upload file
                echo '<div id="uploadedfile">' . $upload->file_copy . '</div>';
            } else {
                echo '<div id="status">failed</div>';
                echo '<div id="message">' . $upload->show_error_string() . '</div>';
            }
            die();
        }
    }

    public function cekAction() {

        include("../library/CMS/upload_class.php"); //classes is the map where the class file is stored

        $upload = new file_upload();
//	print_r($upload);die();
        $upload->upload_dir = APPLICATION_PATH . '/plugins/tesupload/';
        $upload->extensions = array('.png', '.jpg', '.zip', '.pdf'); // specify the allowed extensions here
        //$upload->rename_file = true;


        if (!empty($_FILES)) {
            $upload->the_temp_file = $_FILES['userfile']['tmp_name'];
            $upload->the_file = $_FILES['userfile']['name'];
            $upload->http_error = $_FILES['userfile']['error'];
            $upload->do_filename_check = 'y'; // use this boolean to check for a valid filename
            if ($upload->upload()) {

                echo '<div id="status">success</div>';
                echo '<div id="message">' . $upload->file_copy . ' Successfully Uploaded</div>';
                //return the upload file
                echo '<div id="uploadedfile">' . $upload->file_copy . '</div>';
            } else {

                echo '<div id="status">failed</div>';
                echo '<div id="message">' . $upload->show_error_string() . '</div>';
            }
            die();
        }
    }

    public function templateAction() {
        $auth = Zend_Auth::getInstance();
//		$identity = $auth->getIdentity();
//		print_r($identity);die();
        $_form1 = new Form_Ztemplate();
        $_form1->setMethod(Zend_Form::METHOD_POST);
        $_form1->setAction('/workflow/template');
        $_form1->reset();

        $_model1 = new Model_Ztemplate();
//    	print_r($_data1);die(); 

        $this->view->form1 = $_form1;

        $_req1 = $_POST['TEMPLATE_ID'];
        if (isset($_req1) && !empty($_req1)) {
//    		print_r($_req1);die();
            $_res1 = $_model1->getTemplateById($_req1);
//    		print_r($_res1);die();
            $_form1->populate($_res1[0]);
        } else {
            $_form1->populate(array('TEMPLATE_CREATEDATE' => date('d-M-Y')));
        }
        $_adapter1 = $_model1->getAllTemplate();
        $_paginator1 = new Zend_Paginator(new Zend_Paginator_Adapter_Array($_adapter1));
//    	print_r($_paginator1);die(); 
        $_paginator1->setItemCountPerPage(5);
        $_page1 = $this->_request->getParam('page', 1);
        $_paginator1->setCurrentPageNumber($_page1);
        $this->view->paginator = $_paginator1;
        $this->view->citem = 5;
        $this->view->page = $this->_request->getParam('page', 1);
//    	print_r($this->view);die(); 
    }

    public function addTemplateAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $auth = Zend_Auth::getInstance();
        $identity = $auth->getIdentity();
        $_model1 = new Model_Ztemplate();
        $_req1 = $_POST['TEMPLATE_NAME'];
        if (isset($_req1)) {
            $data = array(
                'USER_ID' => $identity->ID,
                'TEMPLATE_NAME' => $_POST['TEMPLATE_NAME'],
                'TEMPLATE_MSGBODY' => $_POST['TEMPLATE_MSGBODY']
            );
            $check = true;
            foreach ($data as $item) {
                if ($item == null || $item == '') {
                    $list[0] = array(
                        'STAT' => 'WRONG',
                        'MSG' => 'Data belum terisi semua!'
                    );
                    $check = false;
                    break;
                }
            }

            if ($check == true) {
                //print_r($data);die();
                $_res1 = $_model1->checkTemplateName($_req1);

                //echo ($_res1==true)?'true':'false';die();
                if ($_res1 == false) {
                    $res = $_model1->addTemplate($data);
                    if ($res != null) {
                        $list[0] = array(
                            'STAT' => 'SUCCESS',
                            'MSG' => 'Data insert succed!'
                        );
                    } else {
                        $list[0] = array(
                            'STAT' => 'FAILED',
                            'MSG' => 'Data insert failed'
                        );
                    }
                } else {
                    $list[0] = array(
                        'STAT' => 'FAILED',
                        'MSG' => 'Already exist!'
                    );
                }
            }
        }
        echo $_GET['callback'] . '(' . json_encode($list) . ');';
        die();
    }

    public function editTemplateAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $_model1 = new Model_Ztemplate();
        $_req1 = $_POST['TEMPLATE_NAME'];
        if (isset($_req1)) {
            $data = array(
                'TEMPLATE_ID' => $_POST['TEMPLATE_ID'],
                'TEMPLATE_NAME' => $_POST['TEMPLATE_NAME'],
                'TEMPLATE_MSGBODY' => $_POST['TEMPLATE_MSGBODY']
            );
            $check = true;
            foreach ($data as $item) {
                if ($item == null || $item == '') {
                    $list[0] = array(
                        'STAT' => 'WRONG',
                        'MSG' => 'Please fill in blank!'
                    );
                    $check = false;
                    break;
                }
            }

            if ($check == true) {
                //print_r($data);die();
                $_res1 = $_model1->checkTemplateName2($data);

                //echo ($_res1==true)?'true':'false';die();
                if ($_res1 == false) {
                    $res = $_model1->updateTemplate($data);
                    if ($res != null) {
                        $list[0] = array(
                            'STAT' => 'SUCCESS',
                            'MSG' => 'Template edited!'
                        );
                    } else {
                        $list[0] = array(
                            'STAT' => 'FAILED',
                            'MSG' => 'Updating  failed!'
                        );
                    }
                } else {
                    $list[0] = array(
                        'STAT' => 'FAILED',
                        'MSG' => 'Template is exist!'
                    );
                }
            }
        }
        echo $_GET['callback'] . '(' . json_encode($list) . ');';
        die();
    }

    public function deleteTemplateAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
//	    echo 'CEK';die();
        $_model1 = new Model_Ztemplate();
//    	$_POST['PRJPRNT_ID']='7';
        $_req1 = $_POST['TEMPLATE_ID'];
//    	print_r($_POST);echo $_req1;die();
        if (isset($_req1)) {
            if (!empty($_req1)) {
                $res = $_model1->deleteTemplate($_req1);
                if ($res == true) {
                    $list[0] = array(
                        'STAT' => 'SUCCESS',
                        'MSG' => 'Data deleted',
                        'ID' => $_req1
                    );
                } else {
                    $list[0] = array(
                        'STAT' => 'FAILED',
                        'MSG' => 'failed!'
                    );
                }
                //$this->_helper->redirector('index','profiling');
            } else {
                $list[0] = array(
                    'STAT' => 'FAILED',
                    'MSG' => $_req1
                );
            }
        }
        echo $_GET['callback'] . '(' . json_encode($list) . ');';
        die();
    }

    public function mastertemplateAction() {
		
		
		//$this->view->headScript()->appendScript("vex.defaultOptions.className = 'vex-theme-default';");


		$params=$this->getRequest()->getParams();
		$this->view->varams = $params; 
        $modTemplate = new Model_Zwftemplate();
        $temp = $modTemplate->fetchalldata()->toArray();
       
        
        $this->view->temp = $temp;
        $form = new Form_Zwftemp();

        $m_tdata = new Model_Zwftemplatedata();
		
        if ($this->_request->isPost() && $form->isValid($_POST)) {
            $data = $form->getValues();
            $modTemplate->updatedata($data);
            $this->_redirect('/workflow/mastertemplate');
        }

        if ($this->_getParam('ID')) {
            $var = $modTemplate->fetchdata($this->_getParam('ID'))->toArray();
            $form->populate($var);
            $this->view->edit = $var;
            $this->view->form = $form;
        }
         $this->view->messages = $this->_flashMessenger->getMessages();
#        Zend_Debug::dump($params);die();
        if($params[v]=='ajax') 
		{
			$this->_helper->layout->disableLayout();
		}
		
         
    }

    public function tempactAction() {
		$this->_helper->layout->disableLayout();
		
		
        $params = $this->_request->getParams();
        $nn =   $params['atr'];
        $m1 = new Model_Zwftemplate();
        $m_tdata = new Model_Zwftemplatedata();
        switch ($params['act']) {

            case "copy" :
				$temp = $m1->fetchdata($params['ID'])->toArray();

                $trx = $m1->createdatax($temp, $params['inval']);

                $dat = $m_tdata->fetchdatabytid($params['ID'])->toArray();
                foreach ($dat as $data) {
                    $data['TEMPLATE_ID'] = $trx;
                    $m_tdata->createdata($data);
                }
                
			echo '<div class="notification-box info">
                            <span>INFO!</span> Template dengan nama "'.$params['inval'].'"
                            sudah dibuat.
                            <div class="icon"></div>
                        </div>';die();
           
                break;



            case "archive" :

               
                
                $data=array('ID'=>$params['ID'], 'STATUS'=>'9');
              
                $m1->updatestatus($data);
                echo '<div class="notification-box info">
                            <span>INFO!</span> Template ini sudah diarsipkan.
                            <div class="icon"></div>
                        </div>';die();
                
               
                
                break;
            
            case "unarchive" :

                
                
                $data=array('ID'=>$params['ID'], 'STATUS'=>NULL);
                $m1->updatestatus($data);
                echo '<div class="notification-box info">
                            <span>INFO!</span> Template ini sudah diaktifkan kembali.
                            <div class="icon"></div>
                        </div>';die();
                break; 
        }

        //Zend_Debug::dump($params); die();
        //die("a");
    }

}
