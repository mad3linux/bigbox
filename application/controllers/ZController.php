<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class ZController extends Zend_Controller_Action {
	public function init() {
	}
	
    public function publicAction() {
        
    }    
    
	public function qAction() {
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ( true );
		$params = $this->getRequest ()->getParams ();
	
		$mm = new Model_Zpraba4apipublish ();
		$dd = new Model_Zprabapage ();
		if(!is_numeric($params['id'])&& $params['e']!="") {
			
			$replace=array("1","2","3","4","5","6","7","8","9","0");
			$find=array("c", "e","g","i","m","n","q","r","u","x");
			$v_da= explode("z", $params['e']);
			//$e = $api."0".$uname."1".$format[$_POST['format']]."2".$ptype3;
			
			$ptype = str_replace($find, $replace, $v_da[3]);
			$format = str_replace($find, $replace, $v_da[2]);
			$uname = str_replace($find, $replace, $v_da[1]);
			$api = str_replace($find, $replace, $v_da[0]);
		
			$arrformat = array(
				1=>'json',
				2=>'xml'
				);
			//Zend_Debug::dump($ptype); die();		
			$d_uname = $dd->get_a_user_api($uname); 
			
			$data ['username']= $d_uname['user_name'];
			$data ['api_id'] = $api; 
			$data ['format'] = $arrformat[$format]; 
			$params['ptype']=$ptype;
		} else {
			$data ['username']= $params['uname'];
			$data ['api_id'] = $params['id']; 
			$data ['format'] = $params['format']; 
		}
		
		
		if(isset($params['ptype'])) {
			
			if($params['ptype']==2) {
				return $this->call4action($data);
			} else if ($data['ptype']==3) {
				return $this->call4portlet($data);
				}
		}
		
		
		
		$vinit = $mm->get_users_api ( $data );
		//Zend_Debug::dump($data); die();
		$xin = array ();
		if ($vinit) {
			if ($vinit ['is_restricted_ip'] == 1 && $vinit ['user_ip'] != $this->get_client_ip ()) {
				echo json_encode ( array (
						'transcation' => false,
						'message' => 'ip not registered' 
				) );
				die ();
		}
			
		$exe = new Model_Zpraba4api ();
		$varC = $dd->get_api ( $data ['api_id'] );
		
			//Zend_Debug::dump($varC); die();
			
		$conn = unserialize ( $varC ['conn_params'] );
		foreach($data as $v=>$vv) {
			 //$pos = strpos($v, ':');
			 $vR = explode(':', $v);
			 if(isset($vR[1])) 
			 {
				$Vdata[$vR[1]]=$vv;	
			 }
			}
			//Zend_Debug::dump($Vdata); die();
			parse_str(parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY), $array);
			//Zend_Debug::dump($array);die();
			
			$exedata = $exe->execute_api($conn, $varC, $array);
			
			
			
			if ($vinit ['is_show_sql'] != 1) {
				unset ( $exedata ['sql'] );
			}
			if ($vinit ['is_show_parsing_data'] != 1) {
				unset ( $exedata ['parsing_data_time'] );
			}
		} else {
			$exedata = array (
					'transcation' => false,
					'message' => 'unknown error' 
			);
		}
	//Zend_Debug::dump($data); die();
		if ($data ['format'] == 'xml') {
			header ( "Content-type: application/xml" );
			
			// $dataxx=array('data'=>$Arr, "api"=>$sqlChart);
			$dataxx = array (
					'data' => $exedata 
			);
			$xml = CMS_Arraytoxml::createXML ( 'root', $dataxx );
			
			// General::debug($Arr);
			
			echo $xml->saveXML ();
			
			die ();
		} else {
			echo json_encode ( $exedata );
			die ();
		}
	}
	
	function call4portlet ($data) {
		
		$exe = new Model_Zprabawf();	
		$mm = new Model_Zpraba4apipublish ();
		$dd = new Model_Zprabapage ();
		$vinit = $mm->get_users_action ( $data );
		//Zend_Debug::dump($vinit); die();
		$xin = array ();
		if ($vinit) {
			if ($vinit ['is_restricted_ip'] == 1 && $vinit ['user_ip'] != $this->get_client_ip ()) {
				echo json_encode ( array (
						'transcation' => false,
						'message' => 'ip not registered' 
				) );
				die ();
			}
			
			foreach($data as $v=>$vv) {
			 //$pos = strpos($v, ':');
			 $vR = explode(':', $v);
			 if(isset($vR[1])) 
			 {
				$Vdata[$vR[1]]=$vv;	
			 }
			}
			//Zend_Debug::dump($Vdata); die();
			$exedata = $exe->get_wf_process($data ['api_id'], $Vdata);
			
			if ($vinit ['is_show_sql'] != 1) {
				unset ( $exedata ['sql'] );
			}
			if ($vinit ['is_show_parsing_data'] != 1) {
				unset ( $exedata ['parsing_data_time'] );
			}
		} else {
			$exedata = array (
					'transcation' => false,
					'message' => 'unknown error' 
			);
		}
		
		if ($data ['format'] == 'xml') {
			header ( "Content-type: application/xml" );
			
			// $dataxx=array('data'=>$Arr, "api"=>$sqlChart);
			$dataxx = array (
					'data' => $exedata 
			);
			$xml = CMS_Arraytoxml::createXML ( 'root', $dataxx );
			
			// General::debug($Arr);
			
			echo $xml->saveXML ();
			;
			die ();
		} else {
			echo json_encode ( $exedata );
			die ();
		}
		
	}
	function call4action ($data) {
		
		$exe = new Model_Zprabawf();	
		$mm = new Model_Zpraba4apipublish ();
		$dd = new Model_Zprabapage ();
		$vinit = $mm->get_users_action ( $data );
		//Zend_Debug::dump($vinit); die();
		$xin = array ();
		if ($vinit) {
			if ($vinit ['is_restricted_ip'] == 1 && $vinit ['user_ip'] != $this->get_client_ip ()) {
				echo json_encode ( array (
						'transcation' => false,
						'message' => 'ip not registered' 
				) );
				die ();
			}
			
			foreach($data as $v=>$vv) {
			 //$pos = strpos($v, ':');
			 $vR = explode(':', $v);
			 if(isset($vR[1])) 
			 {
				$Vdata[$vR[1]]=$vv;	
			 }
			}
			//Zend_Debug::dump($Vdata); die();
			$exedata = $exe->get_wf_process($data ['api_id'], $Vdata);
			
			if ($vinit ['is_show_sql'] != 1) {
				unset ( $exedata ['sql'] );
			}
			if ($vinit ['is_show_parsing_data'] != 1) {
				unset ( $exedata ['parsing_data_time'] );
			}
		} else {
			$exedata = array (
					'transcation' => false,
					'message' => 'unknown error' 
			);
		}
		
		if ($data ['format'] == 'xml') {
			header ( "Content-type: application/xml" );
			
			// $dataxx=array('data'=>$Arr, "api"=>$sqlChart);
			$dataxx = array (
					'data' => $exedata 
			);
			$xml = CMS_Arraytoxml::createXML ( 'root', $dataxx );
			
			// General::debug($Arr);
			
			echo $xml->saveXML ();
			;
			die ();
		} else {
			echo json_encode ( $exedata );
			die ();
		}
		
	}
	public function encryptAction() {
		$text = array (
				'username' => 'ApiMyTelkom',
				'api_id' => 1,
				'format' => 'xml',
				'input:orang'=>'1000'
				 
		);
		// 'input:customer_name'=>'ARY_DANAMON');
		
		$text = preg_replace ( array (
				"@:@",
				"@%@",
				"@\/@",
				"@\<@",
				"@\>@" 
		), array (
				'&#58;',
				'&#37;',
				'&###;',
				'&#60;',
				'&#62;' 
		), $text );
		
		$enc = base64_encode ( serialize ( $text ) );
		
		Zend_Debug::dump ( $enc );
		die ( "s" );
		
		$data = serialize ( $data );
		
		$encrypted = urlencode ( base64_encode ( mcrypt_encrypt ( MCRYPT_RIJNDAEL_256, md5 ( $key ), $data, MCRYPT_MODE_CBC, md5 ( md5 ( $key ) ) ) ) );
		
		Zend_Debug::dump ( $encrypted );
		die ();
	}
	function get_client_ip() {
		$ipaddress = '';
		if (getenv ( 'HTTP_CLIENT_IP' ))
			$ipaddress = getenv ( 'HTTP_CLIENT_IP' );
		else if (getenv ( 'HTTP_X_FORWARDED_FOR' ))
			$ipaddress = getenv ( 'HTTP_X_FORWARDED_FOR' );
		else if (getenv ( 'HTTP_X_FORWARDED' ))
			$ipaddress = getenv ( 'HTTP_X_FORWARDED' );
		else if (getenv ( 'HTTP_FORWARDED_FOR' ))
			$ipaddress = getenv ( 'HTTP_FORWARDED_FOR' );
		else if (getenv ( 'HTTP_FORWARDED' ))
			$ipaddress = getenv ( 'HTTP_FORWARDED' );
		else if (getenv ( 'REMOTE_ADDR' ))
			$ipaddress = getenv ( 'REMOTE_ADDR' );
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}
	public function indexAction() {
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ( true );
		$params = $this->getRequest ()->getParams ();
		$mm = new Model_Zpraba4apipublish ();
		$dd = new Model_Zprabapage ();
		$text = unserialize ( base64_decode ( $params ['data'] ) );
		$data = preg_replace ( array (
				"@&#58;@",
				"@&#37;@",
				"@&###;@",
				"@&#60;@",
				"@&#62;@" 
		), array (
				':',
				'%',
				'/',
				'<',
				'>' 
		), $text );
		
		if(isset($data['p_type'])) {
			
			if($data['p_type']==2) {
				return $this->call4action($data);
			} else if ($data['p_type']==3) {
				return $this->call4portlet($data);
				}
		}
		
		$vinit = $mm->get_users_api ( $data );
		//Zend_Debug::dump($data); die();
		$xin = array ();
		if ($vinit) {
			if ($vinit ['is_restricted_ip'] == 1 && $vinit ['user_ip'] != $this->get_client_ip ()) {
				echo json_encode ( array (
						'transcation' => false,
						'message' => 'ip not registered' 
				) );
				die ();
			}
			
			$exe = new Model_Zpraba4api ();
			$varC = $dd->get_api ( $data ['api_id'] );
			
			//Zend_Debug::dump($varC); die();
			
			$conn = unserialize ( $varC ['conn_params'] );
			foreach($data as $v=>$vv) {
			 //$pos = strpos($v, ':');
			 $vR = explode(':', $v);
			 if(isset($vR[1])) 
			 {
				$Vdata[$vR[1]]=$vv;	
			 }
			}
			//Zend_Debug::dump($Vdata); die();
			$exedata = $exe->execute_api($conn, $varC, $Vdata);
			
			
			
			if ($vinit ['is_show_sql'] != 1) {
				unset ( $exedata ['sql'] );
			}
			if ($vinit ['is_show_parsing_data'] != 1) {
				unset ( $exedata ['parsing_data_time'] );
			}
		} else {
			$exedata = array (
					'transcation' => false,
					'message' => 'unknown error' 
			);
		}
		
		if ($data ['format'] == 'xml') {
			header ( "Content-type: application/xml" );
			
			// $dataxx=array('data'=>$Arr, "api"=>$sqlChart);
			$dataxx = array (
					'data' => $exedata 
			);
			$xml = CMS_Arraytoxml::createXML ( 'root', $dataxx );
			
			// General::debug($Arr);
			
			echo $xml->saveXML ();
			
			die ();
		} else {
			echo json_encode ( $exedata );
			die ();
		}
	}
	public function viewportletAction() {
		Zend_Session::namespaceUnset ( 'api' );
		$ns = new Zend_Session_Namespace ( 'api' );
		$ns->api = array ();
		$params = $this->getRequest ()->getParams ();
		if (isset ( $params ['modal'] ) && $params ['modal'] == 1) {
			$this->_helper->layout->disableLayout ();
		}
		
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery.numeric.js' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-datepicker/css/datepicker.css' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/select2/select2_metro.css' );
		
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/jquery-multi-select/css/multi-select.css' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js' );
		
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/scripts/form-components.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/select2/select2.min.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/data-tables/jquery.dataTables.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/data-tables/DT_bootstrap.js' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/data-tables/DT_bootstrap.css' );
		$this->view->element = $params ['id'];
		$this->view->varams = $params;
	}
	public function callapiAction() {
		
		
		
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ( true );
		$params = $this->getRequest ()->getParams ();
		$mm = new Model_Zpraba4apipublish ();
		$dd = new Model_Zprabapage ();
		
		
		($_GET['xin'])? $xin = $_GET['xin']  : $xin = $params['xin']; 
		$xin = urldecode($xin);
        $xin = str_replace('garing', '/', $xin);
        $xin = str_replace('tambah', '+', $xin);
        $xin = str_replace('prosen', '%', $xin);
        
			if($xin!=NULL){
			$pos = strpos($xin, '~');
			$p=array();
			$ti=array();
			if($pos !== false){
			$qar = explode('~', $xin);
			foreach($qar as $q)
			{
			$p[]=explode(':',$q);
			foreach($p as $pa)
			{
			$ti[$pa[0]]=$pa[1];
			}
			}

			} else {
			$qarin = explode(':', $xin);
			$ti[$qarin[0]]=$qarin[1];

			}
			}
		
		$xin = array ();
	
		$exe = new Model_Zpraba4api ();
		$varC = $dd->get_api ( $params ['id'] );
		$conn = unserialize ( $varC ['conn_params'] );
		//Zend_Debug::dump($varC);die();
		$exedata = $exe->execute_api ($conn, $varC, $ti);
			
		
		if ($params ['format'] == 'xml') {
			header ( "Content-type: application/xml" );
			
			// $dataxx=array('data'=>$Arr, "api"=>$sqlChart);
			$dataxx = array (
					'data' => $exedata 
			);
			$xml = CMS_Arraytoxml::createXML ( 'root', $dataxx );
			
			// General::debug($Arr);
			
			echo $xml->saveXML ();
			;
			die ();
		} else {
			echo json_encode ( $exedata );
			die ();
		}
	
		
	}
}
