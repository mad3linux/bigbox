<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Form_Login2 extends Zend_Form
{

   public function  init() {
        $this->setMethod('post');
	// create new element
        $id = $this->createElement('hidden', 'ID');
        // element options
        $id->setDecorators(array('ViewHelper'));
        // add the element to the form
        $this->addElement($id);

   
        $username = $this->createElement('text','uname');
       //$username = setAttrib('maxlength', '20');
		$username->setLabel('NIK : ');
        $username->setRequired('true');
        //$username->setvalue('NIK');
        $username->addFilter('StripTags');
        $username->addDecorator('Label', array('class' => 'label-div-small'));
       //$username->addErrorMessage('The username is required!');
        $username
        ->setAttribs(array(
        		"class"=>"input-large normal label-div-small", "maxlength"=>"20",
        			"style"=>"font-size: 14px; font-weight: normal;border: 1px solid #e5e5e5;"
			)
        );
        
        $this->addElement($username);
        $password = $this->createElement('password', 'passw');
        $password->setLabel('PASSWORD: ');
	//$password->setAttrib('size', '30');	
        $password->setRequired('true');
        $password->addDecorator('Label', array('class' => 'label-div-small'));
        $password->setAttribs(array(
        		"class"=>"input-large normal", "maxlength"=>"20",
                "autocomplete" => "off",
                "style"=>"font-size: 14px; font-weight: normal;border: 1px solid #e5e5e5;"     		
        )
        );
        $this->addElement($password);
		 // And finally add some CSRF protection
        try{
			$hash = new Zend_Form_Element_Hash('csrf');
			$hash->setIgnore(true);
			$hash->removeDecorator('label');
			$this->addElement($hash);
		}catch(Exception $e){
			
		}
		
		
		/*
		$tes = $this->createElement('select', 'APPS');
        $tes->setLabel('EVENT: ');
		$tes->setAttribs(array(
        		"class"=>"input-large normal", "maxlength"=>"50",
                "autocomplete" => "off"        		
        ));
		$dd = new Model_System();
		$apppps = $dd->get_all_apps();
		foreach($apppps as $k => $u)
    	{
    		$tes->addMultiOption($k,$u);
    	}
		
		$this->addElement($tes);
		*/
        $submit = $this->addElement('submit', 'submit', array('label' => 'SUBMIT','style'=> 'height: 53px;', 'class'=>'login-button button button-small button-blue button-fullscreen full-bottom'));

   }
}
