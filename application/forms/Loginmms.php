<?php


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Form_Loginmms extends Zend_Form {

    public function init() {
        $this->setMethod('post');
        // create new element
        $id = $this->createElement('hidden', 'ID');
        // element options
        ///$id->setDecorators(array('ViewHelper', 'Errors', array('HtmlTag', array('tag' => 'div', 'class'=>'form-group'));
        // add the element to the form
        $this->addElement($id);
        $username = $this->createElement('text', 'uname');
        //$username = setAttrib('maxlength', '20');
        // $username->setDecorators(array('ViewHelper', 'Errors', array('HtmlTag', array('tag' => 'div', 'class'=>'form-group'))));
        $username->setLabel('NIK : ');
        $username->setRequired('true');
        //$username->setvalue('NIK');
        $username->addFilter('StripTags');
        $username->addDecorator('Label', array('class' => 'control-label visible-ie8 visible-ie9'));
        $username->setAttribs(array("class" => "form-control placeholder-no-fix", "maxlength" => "50", "placeholder" => 'Username (NIK)', "autocomplete" => "off"));
        $this->addElement($username);
        $password = $this->createElement('password', 'passw');
        $password->setLabel('PASSWORD: ');
        //$password->setAttrib('size', '30'); 
        $password->setRequired('true');
        $password->addDecorator('Label', array('class' => 'control-label visible-ie8 visible-ie9'));
        $password->setAttribs(array("class" => "form-control placeholder-no-fix", "placeholder" => 'Password', "maxlength" => "50", "autocomplete" => "off"));
        $this->addElement($password);
        $hash = new Zend_Form_Element_Hash('csrf');
        $hash->setIgnore(true);
        $hash->removeDecorator('label');
        $this->addElement($hash);
        $captcha = new Zend_Form_Element_Captcha('captcha', // This is the name of the input field
        array('label' => '', 'captcha' => array(// Here comes the magic...
        // First the type...
        'captcha' => 'Image', // Length of the word...
        'wordLen' => 3, //'height'=>30,
        // Captcha timeout, 5 mins
        'timeout' => 300, 'LineNoiseLevel' => 0, 'DotNoiseLevel' => 5, // What font to use...
        'font' => APPLICATION_PATH . '/../public/assets/core/fonts/elephant.ttf', // Where to put the image
        'imgDir' => APPLICATION_PATH . '/../public/tmp/captcha/', // URL to the images
        // This was bogus, here's how it should be... Sorry again :S
        'imgUrl' => '/tmp/captcha/',)));
        $captcha->removeDecorator('HtmlTag');
        $captcha->getDecorator('Label')->setTag(null);
        $captcha->removeDecorator('DtDdWrapper');
        $captcha = $this->addElement($captcha);
        $submit = $this->addElement('submit', 'submit', array('label' => 'Login', 'style' => '', 'class' => 'btn blue uppercase'));
    }
}
