<?php

/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class Model_Action extends Zend_Db_Table_Abstract {

    public function get_attrs_by_id($id) {
        try {
            $c = new Model_Zprabawf();
            $vdat = $this->_db->fetchRow("select * from zpraba_process where act_id=? and class_type='Start'", (int) $id);
            $dat = $this->_db->fetchRow("select b.* from zpraba_process_next a inner join zpraba_process b on a.process_to=b.pid where process_from= ?", $vdat['pid']);
            $dat['attrs'] = unserialize($dat['zparams_serialize']);
            // Zend_Debug::dump($dat['attrs']);
            //$string ="";

            foreach($dat['attrs']['OperatorData']['initial'] as $k => $v) {
                // $cek[$v] = $dat['attrs']['OperatorData']['defaultinitial'][$k];
                if($v != "") {
                    $cek[$k] = $v . "=" . $dat['attrs']['OperatorData']['defaultinitial'][$k];
                }
            }
        }
        catch(Exception $e) {
        }
        return implode(",", $cek);
    }

    public function freeze_unfreeze($id) {
        try {
            $sql = "select is_freeze from zpraba_action where id=" . (int) $id . "";
            $cek = $this->_db->fetchOne($sql);
            // echo $sql;
            //echo $cek;
            //die();
            if($cek == 1) {
                $this->_db->query("update zpraba_action set is_freeze=null where  id=" . 
                                  (int) $id . 
                                   "");
            } else {
                $this->_db->query("update zpraba_action set is_freeze=1 where  id=" . 
                                  (int) $id . 
                                   "");
            }
            $result = array('result' => true,
                            'message' => 'Succes');
        }
        catch(Exception $e) {
            $result = array('result' => false,
                            'message' =>$e->getMessage());
        }
        return $result;
    }
}
