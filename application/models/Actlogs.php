<?php

class Model_Actlogs extends Zend_Db_Table_Abstract {
    
    function count_list_logs_crawl($GET) {
        // Zend_Debug::dump($GET); //die();
       $aColumns = array('no',
                          'id',
                          'index_url',
                          'sub_domain',
                          'log_crawl');
        
        
         $sOrder = "";
        if(isset($GET['iSortCol_0'])&& intval($GET['iSortCol_0'])> 0) {
            $sOrder = "ORDER BY  ";
            for($i = 1;
            $i < intval ($GET['iSortingCols'] );
            $i ++ )//for ( $i=1 ; $i<count($aColumns) ; $i++ )
            {
                if($GET['bSortable_' . 
                   intval($GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= "`" . $aColumns[intval($GET['iSortCol_' . 
                                                      $i])] . "` " .($GET['sSortDir_' . 
                                                                     $i] === 'asc' ? 'asc' : 'desc'). ", ";
                }
            }
            $sOrder .= "`" . $aColumns[intval($GET['iSortCol_0'])] . "` " .($GET['sSortDir_0'] === 'asc' ? 'asc' : 'desc'). ", ";
            // echo $sOrder.'<br>';
            $sOrder = substr_replace($sOrder, "", - 2);
            if($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }
        
        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $sWhere = " WHERE 1=1 ";
        if(isset($GET['sSearch'])&& $GET['sSearch'] != "") {
            $sWhere .= " AND (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .=  "`" . $aColumns[$i] . "` LIKE '%" . $GET['sSearch'] . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }
       
      //  die($sWhere);
        
        /* Individual column filtering */
        for($i = 1;
        $i < count ($aColumns );
        $i ++ ) {
            if(isset($GET['bSearchable_' . $i])&& $GET['bSearchable_' . 
                     $i] == "true" && $GET['sSearch_' . 
                     $i] != '') {
                if($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $GET['sSearch_' . $i] . "%' ";
            }
        }
        $qry = "SELECT 
					count(*) as row 
				FROM 
				pmas_crawl_index	 " . $sWhere . " " . $whapp . " " . $sOrder;
        try {
            $data = $this->_db->fetchOne($qry);
            // Zend_Debug::dump($data);die();
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }
	function count_list_logs_crawlbigspider($GET) {
        // Zend_Debug::dump($GET); //die();
       $aColumns = array('no',
                          'id',
                          'index_url',
                          'sub_domain',
                          'log_crawl');
        
        /*
         * Paging
         */
		$sLimit = "";
		
		$getdatatable	= $_GET['datatable'];
		$page			= round($getdatatable['pagination']['page']);
		$perpages		= round($getdatatable['pagination']['perpage']);
		$pages			= ceil($iTotalRecords/$perpages);
		$sorttype		= $getdatatable['sort']['sort'];
		$sortby			= $getdatatable['sort']['field'];
		
		$iDisplayLength = intval($perpages);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart 	= intval(($page-1)*$perpages);
		$sSearch		= $getdatatable['query']['generalSearch'];
		
		
        if(isset($iDisplayStart)&& $iDisplayLength != '-1') {
            $sLimit = "LIMIT " . intval($iDisplayStart). ", " . intval($iDisplayLength);
        }
        
         /*
         * Ordering
         */
         $sOrder = "";
		  // Zend_Debug::dump($data); die();
        if(isset($sortby)) {
			if ($sortby=='No') {
				$filsort	= 'id';
			} else {
				$filsort	= $sortby;
			}
            $sOrder = "ORDER BY ".$filsort." ".$sorttype;
        } else {
			$sOrder = "";
		}
        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $sWhere = '';
        if(isset($sSearch)&& $sSearch != "") {
            $sWhere .= "WHERE (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $sSearch . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }
       
       
      //  die($sWhere);
        
        /* Individual column filtering */
        // for($i = 1;
        // $i < count ($aColumns );
        // $i ++ ) {
            // if(isset($GET['bSearchable_' . $i])&& $GET['bSearchable_' . 
                     // $i] == "true" && $GET['sSearch_' . 
                     // $i] != '') {
                // if($sWhere == "") {
                    // $sWhere = "WHERE ";
                // } else {
                    // $sWhere .= " AND ";
                // }
                // $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $GET['sSearch_' . $i] . "%' ";
            // }
        // }
        $qry = "SELECT 
					count(*) as row 
				FROM 
				pmas_crawl_index	 " . $sWhere . " " . $whapp . " " . $sOrder;
        try {
            $data = $this->_db->fetchOne($qry);
            // Zend_Debug::dump($data);die();
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }
    public function get_list_logs_crawl($GET) {
      $aColumns = array('no',
                          'id',
                          'index_url',
                          'sub_domain',
                          'log_crawl');
        /*
         * Paging
         */
        $sLimit = "";
        if(isset($GET['iDisplayStart'])&& $GET['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . intval($GET['iDisplayStart']). ", " . intval($GET['iDisplayLength']);
        }
        
        /*
         * Ordering
         */
        $sOrder = "";
        if(isset($GET['iSortCol_0'])&& intval($GET['iSortCol_0'])> 0) {
            $sOrder = "ORDER BY  ";
            for($i = 1;
            $i < intval ($GET['iSortingCols'] );
            $i ++ )//for ( $i=1 ; $i<count($aColumns) ; $i++ )
            {
                if($GET['bSortable_' . 
                   intval($GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= "`" . $aColumns[intval($GET['iSortCol_' . 
                                                      $i])] . "` " .($GET['sSortDir_' . 
                                                                     $i] === 'asc' ? 'asc' : 'desc'). ", ";
                }
            }
            $sOrder .= "`" . $aColumns[intval($GET['iSortCol_0'])] . "` " .($GET['sSortDir_0'] === 'asc' ? 'asc' : 'desc'). ", ";
            // echo $sOrder.'<br>';
            $sOrder = substr_replace($sOrder, "", - 2);
            if($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        } else {
            
             $sOrder  = "order by log_crawl, media_id, sub_domain asc";
        }
        
        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $sWhere = " WHERE 1=1 ";
        if(isset($GET['sSearch'])&& $GET['sSearch'] != "") {
            $sWhere .= " AND (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .=  "`" . $aColumns[$i] . "` LIKE '%" . $GET['sSearch'] . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }
       
      //  die($sWhere);
        
        /* Individual column filtering */
        for($i = 1;
        $i < count ($aColumns );
        $i ++ ) {
            if(isset($GET['bSearchable_' . $i])&& $GET['bSearchable_' . 
                     $i] == "true" && $GET['sSearch_' . 
                     $i] != '') {
                if($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $GET['sSearch_' . $i] . "%' ";
            }
        }
        // echo $sWhere.'<br>';
        // echo $sOrder.'<br>';
        // echo $sLimit.'<br>';
        // die();
        $qry = "SELECT *
				FROM pmas_crawl_index
					 " . $sWhere . "  " . $sOrder . " " . $sLimit;
        //die($qry);
        try {
            $data = $this->_db->fetchAll($qry);
           
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }
	public function get_list_logs_crawlbigspider($GET) {
      $aColumns = array('no',
                          'id',
                          'index_url',
                          'sub_domain',
                          'log_crawl');
        /*
         * Paging
         */
		$sLimit = "";
		
		$getdatatable	= $_GET['datatable'];
		$page			= round($getdatatable['pagination']['page']);
		$perpages		= round($getdatatable['pagination']['perpage']);
		$pages			= ceil($iTotalRecords/$perpages);
		$sorttype		= $getdatatable['sort']['sort'];
		$sortby			= $getdatatable['sort']['field'];
		
		$iDisplayLength = intval($perpages);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart 	= intval(($page-1)*$perpages);
		$sSearch		= $getdatatable['query']['generalSearch'];
		
		
        if(isset($iDisplayStart)&& $iDisplayLength != '-1') {
            $sLimit = "LIMIT " . intval($iDisplayStart). ", " . intval($iDisplayLength);
        }
        
        
         /*
         * Ordering
         */
       if(isset($sortby)) {
			if ($sortby=='No') {
				$filsort	= 'id';
			} else {
				$filsort	= $sortby;
			}
            $sOrder = "ORDER BY ".$filsort." ".$sorttype;
        } else {
             $sOrder  = "order by log_crawl, sub_domain asc";
        }
        
        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
         $sWhere = '';
        if(isset($sSearch)&& $sSearch != "") {
            $sWhere .= "WHERE (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $sSearch . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }
      //  die($sWhere);
        
        /* Individual column filtering */
        // for($i = 1;
        // $i < count ($aColumns );
        // $i ++ ) {
            // if(isset($GET['bSearchable_' . $i])&& $GET['bSearchable_' . 
                     // $i] == "true" && $GET['sSearch_' . 
                     // $i] != '') {
                // if($sWhere == "") {
                    // $sWhere = "WHERE ";
                // } else {
                    // $sWhere .= " AND ";
                // }
                // $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $GET['sSearch_' . $i] . "%' ";
            // }
        // }
        // echo $sWhere.'<br>';
        // echo $sOrder.'<br>';
        // echo $sLimit.'<br>';
        // die();
        $qry = "SELECT *
				FROM pmas_crawl_index
					 " . $sWhere . "  " . $sOrder . " " . $sLimit;
        //die($qry);
        try {
            $data = $this->_db->fetchAll($qry);
           
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }
    function count_list_logs($GET) {
        // Zend_Debug::dump($GET); //die();
       $aColumns = array('no',
                          'log_type',
                          'subject',
                          'reff_id',
                          'log_date',
                          'description',
                          'x2');
        /*
         * Ordering
         */
         $sOrder = "";
        if(isset($GET['iSortCol_0'])&& intval($GET['iSortCol_0'])> 0) {
            $sOrder = "ORDER BY  ";
            for($i = 1;
            $i < intval ($GET['iSortingCols'] );
            $i ++ )//for ( $i=1 ; $i<count($aColumns) ; $i++ )
            {
                if($GET['bSortable_' . 
                   intval($GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= "`" . $aColumns[intval($GET['iSortCol_' . 
                                                      $i])] . "` " .($GET['sSortDir_' . 
                                                                     $i] === 'asc' ? 'asc' : 'desc'). ", ";
                }
            }
            $sOrder .= "`" . $aColumns[intval($GET['iSortCol_0'])] . "` " .($GET['sSortDir_0'] === 'asc' ? 'asc' : 'desc'). ", ";
            // echo $sOrder.'<br>';
            $sOrder = substr_replace($sOrder, "", - 2);
            if($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }
        
        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $sWhere = " WHERE 1=1 ";
        if(isset($GET['sSearch'])&& $GET['sSearch'] != "") {
            $sWhere .= " AND (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .=  "`" . $aColumns[$i] . "` LIKE '%" . $GET['sSearch'] . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }
       
      //  die($sWhere);
        
        /* Individual column filtering */
        for($i = 1;
        $i < count ($aColumns );
        $i ++ ) {
            if(isset($GET['bSearchable_' . $i])&& $GET['bSearchable_' . 
                     $i] == "true" && $GET['sSearch_' . 
                     $i] != '') {
                if($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $GET['sSearch_' . $i] . "%' ";
            }
        }
        $qry = "SELECT 
					count(*) as row 
				FROM 
				z_log	 " . $sWhere . " " . $whapp . " " . $sOrder;
        try {
            $data = $this->_db->fetchOne($qry);
            // Zend_Debug::dump($data);die();
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }
	function count_list_logsbigspider($GET) {
        // Zend_Debug::dump($GET); //die();
       $aColumns = array('no',
                          'log_type',
                          'subject',
                          'reff_id',
                          'log_date',
                          'description',
                          'x2');
		$sLimit="";
		
		$getdatatable	= $_GET['datatable'];
		$page			= round($getdatatable['pagination']['page']);
		$perpages		= round($getdatatable['pagination']['perpage']);
		$pages			= ceil($iTotalRecords/$perpages);
		$sorttype		= $getdatatable['sort']['sort'];
		$sortby			= $getdatatable['sort']['field'];
		
		$iDisplayLength = intval($perpages);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart 	= intval(($page-1)*$perpages);
		$sSearch		= $getdatatable['query']['generalSearch'];
		
		
        if(isset($iDisplayStart)&& $iDisplayLength != '-1') {
            $sLimit = "LIMIT " . intval($iDisplayStart). ", " . intval($iDisplayLength);
        }
        /*
         * Ordering
         */
         $sOrder = "";
		  // Zend_Debug::dump($data); die();
        if(isset($sortby)) {
			if ($sortby=='No') {
				$filsort	= 'id';
			} else {
				$filsort	= $sortby;
			}
            $sOrder = "ORDER BY ".$filsort." ".$sorttype;
        } else {
			$sOrder = "";
		}
        // if(isset($GET['iSortCol_0'])&& intval($GET['iSortCol_0'])> 0) {
            // $sOrder = "ORDER BY  ";
            // for($i = 1;
            // $i < intval ($GET['iSortingCols'] );
            // $i ++ )//for ( $i=1 ; $i<count($aColumns) ; $i++ )
            // {
                // if($GET['bSortable_' . 
                   // intval($GET['iSortCol_' . $i])] == "true") {
                    // $sOrder .= "`" . $aColumns[intval($GET['iSortCol_' . 
                                                      // $i])] . "` " .($GET['sSortDir_' . 
                                                                     // $i] === 'asc' ? 'asc' : 'desc'). ", ";
                // }
            // }
            // $sOrder .= "`" . $aColumns[intval($GET['iSortCol_0'])] . "` " .($GET['sSortDir_0'] === 'asc' ? 'asc' : 'desc'). ", ";
            // // echo $sOrder.'<br>';
            // $sOrder = substr_replace($sOrder, "", - 2);
            // if($sOrder == "ORDER BY") {
                // $sOrder = "";
            // }
        // }
        
        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        // $sWhere = " WHERE 1=1 ";
        // if(isset($GET['sSearch'])&& $GET['sSearch'] != "") {
            // $sWhere .= " AND (";
            // for($i = 1;
            // $i < count ($aColumns );
            // $i ++ ) {
                // $sWhere .=  "`" . $aColumns[$i] . "` LIKE '%" . $GET['sSearch'] . "%' OR ";
            // }
            // // die($sWhere);
            // $sWhere = substr_replace($sWhere, "", - 3);
            // $sWhere .= ')';
        // }
		 $sWhere = '';
        if(isset($sSearch)&& $sSearch != "") {
            $sWhere .= "WHERE (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $sSearch . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }
       
      //  die($sWhere);
        
        /* Individual column filtering */
        // for($i = 1;
        // $i < count ($aColumns );
        // $i ++ ) {
            // if(isset($GET['bSearchable_' . $i])&& $GET['bSearchable_' . 
                     // $i] == "true" && $GET['sSearch_' . 
                     // $i] != '') {
                // if($sWhere == "") {
                    // $sWhere = "WHERE ";
                // } else {
                    // $sWhere .= " AND ";
                // }
                // $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $GET['sSearch_' . $i] . "%' ";
            // }
        // }
        $qry = "SELECT 
					count(*) as row 
				FROM 
				z_log	 " . $sWhere . " " . $whapp . " " . $sOrder;
        try {
            $data = $this->_db->fetchOne($qry);
            // Zend_Debug::dump($data);die();
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }
	
	public function get_list_logsbigspider($GET) {
        $aColumns = array('no',
                          'log_type',
                          'subject',
                          'reff_id',
                          'log_date',
                          'description',
                          'x2');
        
        /*
         * Paging
         */
		$sLimit = "";
		
		$getdatatable	= $_GET['datatable'];
		$page			= round($getdatatable['pagination']['page']);
		$perpages		= round($getdatatable['pagination']['perpage']);
		$pages			= ceil($iTotalRecords/$perpages);
		$sorttype		= $getdatatable['sort']['sort'];
		$sortby			= $getdatatable['sort']['field'];
		
		$iDisplayLength = intval($perpages);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart 	= intval(($page-1)*$perpages);
		$sSearch		= $getdatatable['query']['generalSearch'];
		
		
        if(isset($iDisplayStart)&& $iDisplayLength != '-1') {
            $sLimit = "LIMIT " . intval($iDisplayStart). ", " . intval($iDisplayLength);
        }
        
        
        /*
         * Ordering
         */
       if(isset($sortby)) {
			if ($sortby=='No') {
				$filsort	= 'id';
			} else {
				$filsort	= $sortby;
			}
            $sOrder = "ORDER BY ".$filsort." ".$sorttype;
        } else {
             $sOrder  = "ORDER by log_date desc";
        }
        
        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $sWhere = '';
        if(isset($sSearch)&& $sSearch != "") {
            $sWhere .= "WHERE (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $sSearch . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }
       
      //  die($sWhere);
        
        /* Individual column filtering */
        // for($i = 1;
        // $i < count ($aColumns );
        // $i ++ ) {
            // if(isset($GET['bSearchable_' . $i])&& $GET['bSearchable_' . 
                     // $i] == "true" && $GET['sSearch_' . 
                     // $i] != '') {
                // if($sWhere == "") {
                    // $sWhere = "WHERE ";
                // } else {
                    // $sWhere .= " AND ";
                // }
                // $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $GET['sSearch_' . $i] . "%' ";
            // }
        // }
        // echo $sWhere.'<br>';
        // echo $sOrder.'<br>';
        // echo $sLimit.'<br>';
        // die();
        $qry = "SELECT *
				FROM z_log
					 " . $sWhere . "  " . $sOrder . " " . $sLimit;
        //die($qry);
        try {
            $data = $this->_db->fetchAll($qry);
           
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }
    public function get_list_logs($GET) {
        $aColumns = array('no',
                          'log_type',
                          'subject',
                          'reff_id',
                          'log_date',
                          'description',
                          'x2');
        
        /*
         * Paging
         */
        $sLimit = "";
        if(isset($GET['iDisplayStart'])&& $GET['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . intval($GET['iDisplayStart']). ", " . intval($GET['iDisplayLength']);
        }
        
        /*
         * Ordering
         */
        $sOrder = "";
        if(isset($GET['iSortCol_0'])&& intval($GET['iSortCol_0'])> 0) {
            $sOrder = "ORDER BY  ";
            for($i = 1;
            $i < intval ($GET['iSortingCols'] );
            $i ++ )//for ( $i=1 ; $i<count($aColumns) ; $i++ )
            {
                if($GET['bSortable_' . 
                   intval($GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= "`" . $aColumns[intval($GET['iSortCol_' . 
                                                      $i])] . "` " .($GET['sSortDir_' . 
                                                                     $i] === 'asc' ? 'asc' : 'desc'). ", ";
                }
            }
            $sOrder .= "`" . $aColumns[intval($GET['iSortCol_0'])] . "` " .($GET['sSortDir_0'] === 'asc' ? 'asc' : 'desc'). ", ";
            // echo $sOrder.'<br>';
            $sOrder = substr_replace($sOrder, "", - 2);
            if($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        } else {
            
             $sOrder  = "order by log_date desc";
        }
        
        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $sWhere = " WHERE 1=1 ";
        if(isset($GET['sSearch'])&& $GET['sSearch'] != "") {
            $sWhere .= " AND (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .=  "`" . $aColumns[$i] . "` LIKE '%" . $GET['sSearch'] . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }
       
      //  die($sWhere);
        
        /* Individual column filtering */
        for($i = 1;
        $i < count ($aColumns );
        $i ++ ) {
            if(isset($GET['bSearchable_' . $i])&& $GET['bSearchable_' . 
                     $i] == "true" && $GET['sSearch_' . 
                     $i] != '') {
                if($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $GET['sSearch_' . $i] . "%' ";
            }
        }
        // echo $sWhere.'<br>';
        // echo $sOrder.'<br>';
        // echo $sLimit.'<br>';
        // die();
        $qry = "SELECT *
				FROM z_log
					 " . $sWhere . "  " . $sOrder . " " . $sLimit;
        //die($qry);
        try {
            $data = $this->_db->fetchAll($qry);
           
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }

    public function get_logs_by_keys() {
    }

    public function insert_log($data) {
        try {
            $var = array($data['log_type'],
                        $data['subject'],
                        $data['refference'],
                        $data['reff_id'],
                        $data['description'],
                        $data['x1'],
                        $data['x2'],
                        $data['x3'],
                        $data['x4'],
                        $data['xint1'],
                        $data['xint2']);
            // Zend_Debug::dump($var); die();    
            $this->_db->query("insert into z_log (log_date, log_type, subject,refference, reff_id, description, x1, x2, x3, x4, xint1,xint2) values (now(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", $var);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            //die("eror");
            return false;
        }
        return true;
    }
}
