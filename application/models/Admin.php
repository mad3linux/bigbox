<?php


/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class Model_Admin extends Zend_Db_Table_Abstract {

    private function cache() {
        $front = array('lifetime' => null,
                       'automatic_serialization' => true);
        $back = array('cache_dir' => APPLICATION_PATH . '/../cache2/');
        return Zend_Cache::factory('Core', 'File', $front, $back);
    }

    public function get_cache_theme($app) {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $var = $this->cache()->load("themeuser_" . 
                                    $identity->uid . 
                                    "_" . 
                                    $app);
        return $var;
    }

    function compact_users() {
        $data = $this->_db->fetchAll("select * from z_users");

        foreach($data as $v) {
            $this->_db->query("update z_users set  upassword = '" . 
                              md5($v['upassword']). 
                                  "' where id= " . 
                                  $v['id'] . 
                                  "  ");
        }
    }

    function synch_users() {
        $data = $this->_db->fetchAll("select * from tb_users");

        foreach($data as $v) {
            $this->_db->query("update tb_users set  ldap = 1,  email_address='" . 
                              $v['username'] . 
                              "@telkom.co.id' where username= " . 
                              $v['username'] . 
                              "  ");
        }
        die("ok");
    }

    function count_listuser($app, $GET) {
        // Zend_Debug::dump($GET); //die();
        $aColumns = array('no',
                          'id',
                          'uname',
                          'fullname',
                          'email',
                          'ubis',
                          'sub_ubis',
                          'jabatan',
                          'mobile_phone',
                          'fixed_phone');
        
        /*
         * Ordering
         */
       $sOrder = "";
		if (isset ( $_GET ['iSortCol_0'] )) {
			$sOrder = "ORDER BY  ";
			for($i = 1; $i < intval ( $_GET ['iSortingCols'] ); $i ++) {
				if ($_GET ['bSortable_' . intval ( $_GET ['iSortCol_' . $i] )] == "true") {
					$sOrder .= "`" . $aColumns [intval ( $_GET ['iSortCol_' . $i] )] . "` " . ($_GET ['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
				}
			}
			
			$sOrder = substr_replace ( $sOrder, "", - 2 );
			if ($sOrder == "ORDER BY") {
				$sOrder = "";
			}
		}
        
        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $whapp = "";
        $sWhere = " WHERE 1=1";
        if($app != "") {
            $whapp = "AND app = '$app'";
        }
        if(isset($GET['sSearch'])&& $GET['sSearch'] != "") {
            $sWhere .= "AND (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= " LIKE '%" . $GET['sSearch'] . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }
        
        /* Individual column filtering */
        for($i = 1;
        $i < count ($aColumns );
        $i ++ ) {
            if(isset($GET['bSearchable_' . $i])&& $GET['bSearchable_' . 
                     $i] == "true" && $GET['sSearch_' . 
                     $i] != '') {
                if($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
               	$sWhere .= "`" . $aColumns [$i] . "` LIKE '%" . $_GET ['sSearch_' . $i] . "%' ";
            }
        }
        $qry = "SELECT 
					count(id) as row 
				FROM 
				v_users	 " . $sWhere . " " . $whapp . " " . $sOrder;
        try {
            $data = $this->_db->fetchOne($qry);
            // Zend_Debug::dump($data);die();
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }

    function get_listuser($app, $GET) {
        $aColumns = array('no',
                          'id',
                          'uname',
                          'fullname',
                          'email',
                          'ubis',
                          'sub_ubis',
                          'jabatan',
                          'mobile_phone',
                          'fixed_phone');
        
        /*
         * Paging
         */
        $sLimit = "";
        if(isset($GET['iDisplayStart'])&& $GET['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . intval($GET['iDisplayStart']). ", " . intval($GET['iDisplayLength']);
        }
        
        /*
         * Ordering
         */
       $sOrder = "";
		if (isset ( $_GET ['iSortCol_0'] ) && intval ( $_GET ['iSortCol_0'] ) > 0) {
			$sOrder = "ORDER BY  ";
			for($i = 1; $i < intval ( $_GET ['iSortingCols'] ); $i ++ )
			//for ( $i=1 ; $i<count($aColumns) ; $i++ )
			{ 

				if ($_GET ['bSortable_' . intval ( $_GET ['iSortCol_' . $i] )] == "true") {
					$sOrder .= "`" . $aColumns [intval ( $_GET ['iSortCol_' . $i] )] . "` " . ($_GET ['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
				}
			}
			
			$sOrder .= "`" . $aColumns [intval ( $_GET ['iSortCol_0'] )] . "` " . ($_GET ['sSortDir_0'] === 'asc' ? 'asc' : 'desc') . ", ";
			// echo $sOrder.'<br>';
			
			$sOrder = substr_replace ( $sOrder, "", - 2 );
			if ($sOrder == "ORDER BY") {
				$sOrder = "";
			}
		}
        
        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $sWhere = " WHERE 1=1 ";
        if(isset($GET['sSearch'])&& $GET['sSearch'] != "") {
            $sWhere .= " AND (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= " LIKE '%" . $GET['sSearch'] . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }
        if($app != "") {
            $whapp = "AND app = '$app'";
        }
        //die($sWhere);
        
        /* Individual column filtering */
        for($i = 1;
        $i < count ($aColumns );
        $i ++ ) {
            if(isset($GET['bSearchable_' . $i])&& $GET['bSearchable_' . 
                     $i] == "true" && $_GET['sSearch_' . 
                     $i] != '') {
                if($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
               $sWhere .= "`" . $aColumns [$i] . "` LIKE '%" . $_GET ['sSearch_' . $i] . "%' ";
            }
        }
        // echo $sWhere.'<br>';
        // echo $sOrder.'<br>';
        // echo $sLimit.'<br>';
        // die();
        $qry = "SELECT 
					id,uname,fullname,email,ubis,sub_ubis,jabatan,mobile_phone,fixed_phone,ubis_id,sub_ubis_id,upassword,person_id 
				FROM v_users
					 " . $sWhere . " " . $whapp . " " . $sOrder . " " . $sLimit;
         //die($qry);
        try {
            $data = $this->_db->fetchAll($qry);
            // Zend_Debug::dump($data);die();
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }

    function get_userubis($uname = '') {
        $qry = "SELECT 
					c.* 
				FROM 
					z_users b, p_sub_ubis c 
				where 
					b.sub_ubis_id=c.id 
					and 
					b.uname='" . $uname . "'";
        // die($qry);
        try {
            $data = $this->_db->fetchRow($qry);
            // Zend_Debug::dump($data);die();
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }
}
