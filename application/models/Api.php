<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Api extends Zend_Db_Table_Abstract{

public function getApiUser() {
	$q = "select distinct(a.uname),a.id,a.ubis_id,b.name as ubis,a.sub_ubis_id,c.name sub_ubis from z_users a,p_ubis b,p_sub_ubis c where a.ubis_id=b.id and a.sub_ubis_id=c.id order by ubis_id, uname";
	$data = array();
	//die($q);
	try {
		$data = $this->_db->fetchAll($q);
		return $data;
	} catch (Exception $e) {
	   Zend_Debug::dump($e->getMessage());die($q);		
	}
}

public function getApiUser2($par = array()) {
	//var_dump($par);die();
	$term = (isset($par['term'])?$par['term']:'');
	$user = (isset($par['user'])&&$par['user']=='true'?true:false);
	$group = (isset($par['group'])&&$par['group']=='true'?true:false);
	if(($user && $group) || (!$user && !$group)){
		$qry = 'select 
						distinct(a.uname),a.fullname,a.id,a.ubis_id,b.name as ubis,a.sub_ubis_id,c.name sub_ubis, d.attr_code,d.attr_val 
					from 
						z_users a left join p_ubis b on a.ubis_id=b.id left join p_sub_ubis c on a.sub_ubis_id=c.id left join z_user_attr d on a.id=d.uid 
					where 
						a.uname like "%'.$term.'%" 
					UNION ALL 
					select 
						distinct(a.group_name) as uname,a.group_name as fullname, a.gid as id,0 as ubis_id,"Group" as ubis,null as sub_ubis_id,"User Group" as sub_ubis, b.attr_code,b.attr_val 
					from 
						z_groups a left join z_group_attr b on a.gid=b.gid 
					where 
						a.group_name like "%'.$term.'%" order by ubis_id, uname';
	}else if($group){
		$qry = "select 
						distinct(a.group_name) as uname,a.group_name as fullname, a.gid as id,0 as ubis_id,'Group' as ubis,null as sub_ubis_id,'User Group' as sub_ubis, b.attr_code,b.attr_val 
					from 
						z_groups a left join z_group_attr b on a.gid=b.gid 
					where 
						a.group_name like '%".$term."%' 
					order by 
						ubis_id, uname";
	}else{
		$qry = "select 
				distinct(a.uname),a.fullname,a.id,a.ubis_id,b.name as ubis,a.sub_ubis_id,c.name sub_ubis, d.attr_code,d.attr_val 
			from 
				z_users a 
			left join 
				p_ubis b on a.ubis_id=b.id 
			left join 
				p_sub_ubis c on a.sub_ubis_id=c.id 
			left join 
				z_user_attr d on a.id=d.uid 
			where 
				a.uname like '%".$term."%' 
			order by 
				ubis_id, uname";
	}
	
	$data = array();
	//die($qry);
	try {
		$data = $this->_db->fetchAll($qry);
		return $data;
	} catch (Exception $e) {
	   Zend_Debug::dump($e->getMessage());die($q);		
	}
}

public function getApiUser3($par = array()) {
	//var_dump($par);die();
	$term = (isset($par['term'])?$par['term']:'');
	$user = (isset($par['user'])&&$par['user']=='true'?true:false);
	$group = (isset($par['group'])&&$par['group']=='true'?true:false);
	if(($user && $group) || (!$user && !$group)){
		$qry = 'select 
	distinct(a.uname),a.fullname,a.id,a.ubis_id,b.name as ubis,a.sub_ubis_id,c.name sub_ubis, d.attr_code,d.attr_val 
from 
	 z_user_attr d left join z_users a on a.id=d.uid left join p_ubis b on a.ubis_id=b.id left join p_sub_ubis c on a.sub_ubis_id=c.id
where 
	a.uname like "%'.$term.'%" 
UNION ALL 
select 
	distinct(a.group_name) as uname,a.group_name as fullname, a.gid as id,0 as ubis_id,"Group" as ubis,null as sub_ubis_id,"User Group" as sub_ubis, b.attr_code,b.attr_val 
from 
	z_group_attr b left join z_groups a on a.gid=b.gid 
where 
	a.group_name like "%'.$term.'%" 
order by ubis_id, uname';
	}else if($group){
		$qry = 'select 
	distinct(a.group_name) as uname,a.group_name as fullname, a.gid as id,0 as ubis_id,"Group" as ubis,null as sub_ubis_id,"User Group" as sub_ubis, b.attr_code,b.attr_val 
from 
	z_group_attr b left join z_groups a on a.gid=b.gid 
where 
	a.group_name like "%'.$term.'%" 
order by ubis_id, uname';
	}else{
		$qry = 'select 
	distinct(a.uname),a.fullname,a.id,a.ubis_id,b.name as ubis,a.sub_ubis_id,c.name sub_ubis, d.attr_code,d.attr_val 
from 
	 z_user_attr d left join z_users a on a.id=d.uid left join p_ubis b on a.ubis_id=b.id left join p_sub_ubis c on a.sub_ubis_id=c.id
where 
	a.uname like "%'.$term.'%" 
order by 
	ubis_id, uname';
	}
	
	$data = array();
	//die($qry);
	try {
		$data = $this->_db->fetchAll($qry);
		return $data;
	} catch (Exception $e) {
	   Zend_Debug::dump($e->getMessage());die($q);		
	}
}

function insertupdate_attr($data, $uid) {
	//Zend_Debug::dump($data);//die($qry);
	//Zend_Debug::dump($uid);die($qry);
	
	$qry = array();
	if($data['type']=='user') {
		if($data['havetelegram']=='true') {
			$qry[] = "update z_user_attr set attr_val= '".$data['telegram']."' where uid =".$data['uid']." and attr_code ='telegram'";
		} else {
			$qry[] = "insert into z_user_attr (uid,attr_code,attr_val) values (".$data['uid'].",'telegram','".$data['telegram']."')";
		}
		
		if($data['havewhatsapp']=='true') {
			$qry[] = "update z_user_attr set attr_val= '".$data['whatsapp']."' where uid =".$data['uid']." and attr_code ='whatsapp'";
		} else {
			$qry[] = "insert into z_user_attr (uid,attr_code,attr_val) values (".$data['uid'].",'whatsapp','".$data['whatsapp']."')";
		}
		
		if($data['haveemail']=='true') {
			$qry[] = "update z_user_attr set attr_val= '".$data['email']."' where uid =".$data['uid']." and attr_code ='email'";
		} else {
			$qry[] = "insert into z_user_attr (uid,attr_code,attr_val) values (".$data['uid'].",'email','".$data['email']."')";
		}
		
		if($data['havegtalk']=='true') {
			$qry[] = "update z_user_attr set attr_val= '".$data['gtalk']."' where uid =".$data['uid']." and attr_code ='gtalk'";
		} else {
			$qry[] = "insert into z_user_attr (uid,attr_code,attr_val) values (".$data['uid'].",'gtalk','".$data['gtalk']."')";
		}
	}else if($data['type']=='group') {
		if($data['havetelegram']=='true') {
			$qry[] = "update z_group_attr set attr_val= '".$data['telegram']."' where gid =".$data['uid']." and attr_code ='telegram'";
		} else {
			$qry[] = "insert into z_group_attr (gid,attr_code,attr_val) values (".$data['uid'].",'telegram','".$data['telegram']."')";
		}
		
		if($data['havewhatsapp']=='true') {
			$qry[] = "update z_group_attr set attr_val= '".$data['whatsapp']."' where gid =".$data['uid']." and attr_code ='whatsapp'";
		} else {
			$qry[] = "insert into z_group_attr (gid,attr_code,attr_val) values (".$data['uid'].",'whatsapp','".$data['whatsapp']."')";
		}
		
		if($data['haveemail']=='true') {
			$qry[] = "update z_group_attr set attr_val= '".$data['email']."' where gid =".$data['uid']." and attr_code ='email'";
		} else {
			$qry[] = "insert into z_group_attr (gid,attr_code,attr_val) values (".$data['uid'].",'email','".$data['email']."')";
		}
		
		if($data['havegtalk']=='true') {
			$qry[] = "update z_group_attr set attr_val= '".$data['gtalk']."' where gid =".$data['uid']." and attr_code ='gtalk'";
		} else {
			$qry[] = "insert into z_group_attr (gid,attr_code,attr_val) values (".$data['uid'].",'gtalk','".$data['gtalk']."')";
		}
	}
	//Zend_Debug::dump($qry);die($qry);
	//die($sql);
	try {
		foreach ($qry as $k=>$v){
			$this->_db->query($v);
		}
		$data = array('success'=>true, 'message'=>'Data atribut berhasil diupdate');
	} catch(Exception $e) 	{
		$data = array('success'=>false, 'message'=> $e->getMessage());
	}
	return $data;
}

public function getAttr($id,$type){
$data = array();
switch($type){
	case 'user':
		$q = "select * from z_user_attr where uid='".$id."'";
		try {
			$data = $this->_db->fetchAll($q);
		} catch (Exception $e) {
		   Zend_Debug::dump($e->getMessage());die($q);		
		}
		break;
	case 'group':
		$q = "select * from z_group_attr where gid='".$id."'";
		try {
			$data = $this->_db->fetchAll($q);
		} catch (Exception $e) {
		   Zend_Debug::dump($e->getMessage());die($q);		
		}
		break;
	default:
		break;
}
return $data;

}

}
