<?php

/**
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>,Jul 15, 2014
 *
 */

class Model_Cache extends Zend_Db_Table_Abstract {

    public function get_id($id, $input = array()) {
        $cc = new CMS_Functions();
        $d = $cc->__serialize($input); //Zend_Debug::dump($d);die();
        $id = md5($d . 
                  "" . 
                  $id);
        return $id;
    }

    public function get_cache($ncache, $id) {
        return $ncache->load($id);
    }

    public function cachefunc($time) {
        $cache_time = 3600;
        if($time != "") {
            $cache_time = $time;
        }
        $front = array('lifetime' =>$cache_time,
                       'automatic_serialization' => true);
        $back = array('cache_dir' => APPLICATION_PATH . '/../cache/');
        return Zend_Cache::factory('Core', 'File', $front, $back);
    }
}
