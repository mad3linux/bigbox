<?php
/**
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <himawijaya@gmail.com>,Jul 15, 2014
 *
 */
class Model_Catalog extends Zend_Db_Table_Abstract {
function getCatalog($ext = "") {

	try {
		$authAdapter = Zend_Auth::getInstance ();
		$usr = $authAdapter->getIdentity();
	} catch ( Exception $e ) {
			
	}

	//Zend_Debug::dump($authAdapter);die();
	if($authAdapter->hasIdentity()){
		$qry = "select * from  m_kategori order by urutan_kategori";
	}else{
		$qry = "select * from  m_kategori where id_kategori < 9 order by urutan_kategori";
	}
	//die($qry);
	try {
		$data = $this->_db->fetchAll( $qry );
		//Zend_Debug::dump($data);die();
		foreach ($data as $k=>$v){
			$qry2 = "select * from  m_refnumber where kategori_id = ".$v["id_kategori"]."  order by nama_refnumber ASC";
			$data[$k]['child'] = $this->_db->fetchAll( $qry2 );
			//Zend_Debug::dump($data[$k]['child']);die();
			
			foreach ($data[$k]['child'] as $k2=>$v2){
				$qry3 = "select * from  detail_service2 where status_layanan='Operated' and refnumber_id= ".$v2["id_refnumber"]." ".$ext;
				$data[$k]['child'][$k2]['child'] = $this->_db->fetchAll( $qry3 );
			//Zend_Debug::dump($data[$k]['child'][$k2]['child']);die();
			}
		}
		//Zend_Debug::dump($data);die();
		return $data;
	} catch ( Exception $e ) {
		Zend_Debug::dump ( $e->getMessage () );
		die ( $q );
	}
}

function getServDetail($refid) {
	if(isset($refid) && $refid!=''){
		$qry = "select * from  detail_service2 where refnumber_id = '".$refid."'";
	}else{
		$qry = "select * from  detail_service2";
	}
	
	try {
		$data = $this->_db->fetchRow( $qry );
		//Zend_Debug::dump($data);die();
		
		$qry2 = "select * from  m_refnumber where id_refnumber= '".$data["refnumber_id"]."'";
		$data['m_refnumber'] = $this->_db->fetchRow( $qry2 );
		//Zend_Debug::dump($data['m_refnumber']);die();
			
		$qry3 = "select * from  m_subkategori where id_subkategori= '".$data['m_refnumber']["subkategori_id"]."'";
		$data['m_subkategori'] = $this->_db->fetchRow( $qry3 );
		//Zend_Debug::dump($data['m_subkategori']);die();				
		
		$qry4 = "select * from  m_kategori where id_kategori= '".$data['m_subkategori']["kategori_id"]."'";
		$data['m_kategori'] = $this->_db->fetchRow( $qry4 );
		//Zend_Debug::dump($data['m_kategori']);die();
		
		//$qry5 = "SELECT * FROM  gambar WHERE refnumber_id='".$data["refnumber_id"]."'";
		//$data['gambar'] = $this->_db->fetchRow( $qry5 );
		
		//Zend_Debug::dump($data);die();
		return $data;
	} catch ( Exception $e ) {
		Zend_Debug::dump ( $e->getMessage () );
		die ( $q );
	}
}

function kategori(){


$sql = "SELECT * FROM  m_kategori WHERE id_kategori < 9 ORDER BY urutan_kategori";

try {

	$data = $this->_db->fetchAll($sql);
	//Zend_Debug::dump($data);
	return $data;

} catch ( Exception $e ) {
	Zend_Debug::dump ( $e->getMessage () );
}


}

function kategori_super_admin(){

$sql = "select * from  m_kategori order by urutan_kategori";

try {

	$data = $this->_db->fetchAll($sql);
	//Zend_Debug::dump($data);
	return $data;

} catch ( Exception $e ) {
	Zend_Debug::dump ( $e->getMessage () );
}

}

function subkategori_super_admin($id){

$sql = "select * from  m_subkategori where kategori_id='".$id."' order by urutan_subkategori";

try {

	$data = $this->_db->fetchAll($sql);
	//Zend_Debug::dump($data);
	return $data;

} catch ( Exception $e ) {
	Zend_Debug::dump ( $e->getMessage () );
}

}
function kategori_admin_unit($data){

//Zend_Debug::dump($data);die();

//$sql = "SELECT * FROM  m_kategori WHERE id_kategori in (".$data.") ORDER BY urutan_kategori";
$sql = "SELECT distinct a.* FROM  m_kategori a inner join  m_subkategori b on a.id_kategori=b.kategori_id Where b.unit_support=".(int)$data;
//die($sql);
try {

	$data = $this->_db->fetchAll($sql);
	//Zend_Debug::dump($data);
	return $data;

} catch ( Exception $e ) {
	Zend_Debug::dump ( $e->getMessage () );
}


}

function subkategori_admin_unit($ubis,$sububis){

$sql = "SELECT distinct * FROM  m_subkategori WHERE kategori_id in (".$ubis.") and unit_support =".(int)$sububis." ORDER BY urutan_subkategori";
//die($sql);
//echo $sql;
try {

	$data = $this->_db->fetchAll($sql);
	//Zend_Debug::dump($data);
	return $data;

} catch ( Exception $e ) {
	Zend_Debug::dump ( $e->getMessage () );
}


}

function mrefnumber_admin_unit($id_kategori,$id_subkategori,$ext){


$sql = "select a.* from  m_refnumber a, detail_service2 b where a.kategori_id in (".$id_kategori.") and a.subkategori_id in (".$id_subkategori.")  and a.id_refnumber=b.refnumber_id and ( b.status_layanan='Operated' or b.status_layanan is null or b.status_layanan='') $ext order by a.nama_refnumber";
echo $sql;

try {

	$data = $this->_db->fetchAll($sql);
	//Zend_Debug::dump($data);
	return $data;

} catch ( Exception $e ) {
	Zend_Debug::dump ( $e->getMessage () );
}

}


function kategori_support(){

$sql = "SELECT * FROM  m_kategori ORDER BY urutan_kategori";

try {

	$data = $this->_db->fetchAll($sql);
	//Zend_Debug::dump($data);
	return $data;

} catch ( Exception $e ) {
	Zend_Debug::dump ( $e->getMessage () );
}


}

function subkategori($id){

$sql = "SELECT * FROM  m_subkategori WHERE kategori_id = ".$id." ORDER BY urutan_subkategori";

try {

	$data = $this->_db->fetchAll($sql);
	//Zend_Debug::dump($data);
	return $data;

} catch ( Exception $e ) {
	Zend_Debug::dump ( $e->getMessage () );
}


}

function mrefnumber($id_kategori,$id_subkategori,$ext){


$sql = "select a.* from  m_refnumber a, detail_service2 b where a.kategori_id=".$id_kategori." and a.subkategori_id=".$id_subkategori."  and a.id_refnumber=b.refnumber_id and ( b.status_layanan='Operated' or b.status_layanan is null or b.status_layanan='') $ext order by a.nama_refnumber";
//echo $sql;

try {

	$data = $this->_db->fetchAll($sql);
	//Zend_Debug::dump($data);
	return $data;

} catch ( Exception $e ) {
	Zend_Debug::dump ( $e->getMessage () );
}

}

function mrefnumber_support($id_kategori,$id_subkategori){


$sql = "select * from  m_refnumber where kategori_id= ".$id_kategori." and subkategori_id= ".$id_subkategori." order by urutan_refnumber";
//echo $sql;

try {

	$data = $this->_db->fetchAll($sql);
	//Zend_Debug::dump($data);
	return $data;

} catch ( Exception $e ) {
	Zend_Debug::dump ( $e->getMessage () );
}

}

function mrefnumber_kat_id_9(){

$sql = "SELECT * FROM  m_refnumber WHERE kategori_id= 9";

try {

	$data = $this->_db->fetchAll($sql);
	//Zend_Debug::dump($data);
	return $data;

} catch ( Exception $e ) {
	Zend_Debug::dump ( $e->getMessage () );
}


}

function mrefnumber_kat_id_10(){

$sql = "SELECT * FROM  m_refnumber WHERE kategori_id= 10";

try {

	$data = $this->_db->fetchAll($sql);
	//Zend_Debug::dump($data);
	return $data;

} catch ( Exception $e ) {
	Zend_Debug::dump ( $e->getMessage () );
}


}

function get_unit_support($id){

$sql = "";

}

}

