<?php

class Model_Configs extends Zend_Db_Table_Abstract {

    function prefix() {
        return Zend_Registry::get('pre');
    }

    function cache($conf = array()) {
        $cc = new CMS_General();
        return $cc->cachefunc($conf);
    }

    public function get_all() {
        try {
            $cache = $this->cache(array('lifetime' => "~"));
            if($config = $cache->load('CALASAN_MODEL_CONFIGS_GET_ALL')) {
                return $config;
            }
            $sql = "select * from " . $this->prefix(). "configurations ";

            $data = $this->_db->fetchAll($sql);
            foreach($data as $v) {
                $config[$v['slug']] = $v['value'];
            }
            $config['image::max-size'] = $config['image-max-size'];
            $config['image::save-original'] = true;
            $config['image::allow-animated-gif'] = $config['image-max-size'];
            $config['image::ext-allowed'] =($config['image-allow-type'] != "")? $config['image-allow-type'] : 'gif,png,jpg';
            $config['mail.from'] = $config['site_email'];
            $config['app.debug'] =($config['enable-debug'] != "")? $config['enable-debug'] : 0;
            $timezone =($config['timezone'] != "")? $config['timezone'] : 'UTC';
            if($timezone)
                date_default_timezone_set($timezone);
            $config['app.timezone'] =($config['timezone'] != "")? $config['timezone'] : 'UTC';
            $driver =($config['site-mail-driver'] != "")? $config['site-mail-driver'] : 'mail';
            if(!empty($driver)) {
                $config['mail.driver'] = $driver;
            }
            $config['mail.host'] = $config['mail-driver-host'];
            $config['mail.username'] = $config['mail-driver-username'];
            $config['mail.password'] = $config['mail-driver-password	'];
            $config['mail.from'] = $config['site_email'];
            $from = array('address' =>$config['mail-from-address'],
                          'name' =>$config['mail-from-name']);
            $config['mail.from'] = $from;
            $config['mail.encryption'] = $config['site-mail-encryption'];
            $config['mail.port']=($config['mail-driver-port'] != "")? $config['mail-driver-port'] : '587';
            $config['theme::minifyAssets'] = $config['minify-assets'];
            $cache->save($config, 'CALASAN_MODEL_CONFIGS_GET_ALL', array('calasan', 'system'));
        }
        catch(Exception $e) {
        }
        return $config;
    }

}
