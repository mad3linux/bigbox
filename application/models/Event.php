<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Event extends Zend_Db_Table_Abstract {
	
	
	public function get_icon($dev) {
		$files = scandir ( APPLICATION_PATH . '/../public/assets/core/event/icon/'.$dev.'/' );
		unset($files[0]);
		unset($files[1]);
		return $files; 
	}
	
		public function get_event_temp($id) {
			try {
			
				$data = $this->_db->fetchRow("select * from event_temp where ext_id = ? and p_type='event'", $id);	
				
				//Zend_Debug::dump($data); die();
				return $data;
			} catch (Exception $e) {
				
			  Zend_Debug::dump($e); die();
			
			}
			
			
			
	}

}


