<?php

/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class Model_Gcek extends Zend_Db_Table_Abstract {

    function __construct() {
        $params = array(
            'username' => 'amdes',
            'password' => 'des_isc_098',
            'host' => '10.62.13.150',
            'dbname' => '(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=10.62.13.150)(PORT=1525))(CONNECT_DATA=(SERVER=dedicated)(SERVICE_NAME=desdb)))'
        );

        try {
            $this->_db = Zend_Db::factory('Oracle', $params);
            $this->_db->getConnection();
        } catch (Zend_Db_Adapter_Exception $e) {
            echo $e;
        } catch (Zend_Exception $e) {
            echo $e;
        }
    }

    public function get_cc() {
        $sql = "SELECT NIP_NAS, COUNT(*) FROM AMDES.IFE_TICARES_TIBSNP_CC_20150317 WHERE NIPNAS IN ('19933', '151924')  GROUP BY NIP_NAS";
        try {
            $data = $this->_db->fetchAll($sql);
            return $data;
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($sql);
        }
    }

    public function get_data_cc($cc) {
        $sql = "SELECT * FROM AMDES.IFE_TICARES_TIBSNP_CC_20150317 ORDER BY NIP_NAS, ACCOUNT_NUM WHERE NIP_NAS='$cc' ";
        try {
            $data = $this->_db->fetchAll($sql);
            return $data;
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($sql);
        }
    }

}
