<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Generalsystem extends Zend_Db_Table_Abstract {
	public function get_raw_ubis() {
		$q = "select * from p_ubis where ifnull(flag,0)=0 order by `name`";
		try {
			$data = $this->_db->fetchAll ( $q );
			return $data;
		} catch ( Exception $e ) {
			Zend_Debug::dump ( $e->getMessage () );
			die ( $q );
		}
	}
	public function get_raw_sub_ubis($param = null) {
		// die($param);
		if ($param == null) {
			$qry = "select * from p_sub_ubis where ifnull(flag,0)=0 order by `name`";
		} else {
			$qry = "select * from p_sub_ubis where ubis_id=? and ifnull(flag,0)=0  order by `name`";
		}
		// echo $qry;die("ss");
		try {
			$data = $this->_db->fetchAll ( $qry, $param );
			return $data;
		} catch ( Exception $e ) {
			Zend_Debug::dump ( $e->getMessage () );
			die ( $q );
		}
	}
}

