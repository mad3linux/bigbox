<?php


/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

// ini_set('display_errors', true);
class Model_Hive extends Zend_Db_Table_Abstract {

	function __construct(){

		try {

			shell_exec("kinit -kt '/var/www/html/bigbox/usr_engineer.keytab' usr_engineer@HDPTELKOM.CO.ID");
			// $klist = shell_exec('klist'); Zend_Debug::dump($klist);die();

			// $param = array("dbname" => "hiveJTN", "username" => "720200", 'password' => "Anzalna3001");
			$param = array("dbname" => "hiveJTN", "username" => "usr_engineer", 'password' => "usr_3ng1n3er");

			$this->_dbHive = Zend_Db::factory("Pdo_Impala", $param);
            $this->_dbHive->getConnection();
			
		} catch (Zend_Db_Adapter_Exception $e) {
			return array(
				'transaction'=>false,
				'time'=>date('Y-m-d H:i:s'),
				'type'=>'connection',
				'text'=>'Error Connection!',
				'duration'=>(float)'0.000',
				'message'=>$e->getMessage(),
				'data'=>false,
			);
		} catch (Exception $e) {
			return array(
				'transaction'=>false,
				'time'=>date('Y-m-d H:i:s'),
				'type'=>'connection',
				'text'=>'Error Connection!',
				'duration'=>(float)'0.000',
				'message'=>$e->getMessage(),
				'data'=>false,
			);
		}
	}

	function cache($id,$lifetime=3600){
		$frontendOptions = array(
		   'lifetime' => 30,                   // cache lifetime of 30 seconds
		   'automatic_serialization' => false  // this is the default anyways
		);
		 
		$backendOptions = array(
			'cache_dir' => APPLICATION_PATH . '/../cache/'
		);
		 
		$cache = Zend_Cache::factory(
			'Output',
			'File',
			$frontendOptions,
			$backendOptions
		);

		return $cache;
	}

	public function connect_to_hive(){

		try {

			$dbz = Zend_Db::factory("Pdo_Impala", array("dbname" => "hiveJTN", "username" => "720200", 'password' => "Anzalna3001"));
            $dbz->getConnection();

			return $dbz;
			
		} catch (Zend_Db_Adapter_Exception $e) {
			return false;
			Zend_Debug::dump($e->getMessage());die();
		} catch (Exception $e) {
			return false;
			Zend_Debug::dump($e->getMessage());die();
		}
	}

	public function fetchAll($sql='',$param=array()){

		$time_start = explode(' ',microtime());
    	try {
            $data = $this->_dbHive->fetchAll($sql,$param);
            $time_end = explode(' ',microtime());
            $duration = number_format ( ($time_end [1] + $time_end [0] - ($time_start [1] + $time_start [0])), 3 );
			return array(
    			'transaction'=>true,
    			'time'=>date('Y-m-d H:i:s'),
    			'type'=>'fetchAll',
    			'text'=>$sql,
    			'duration'=>$duration,
    			'message'=>'Success',
    			'data'=>$data,
    		);    		
    	} catch (Exception $e) {
            $time_end = explode(' ',microtime());
            $duration = number_format ( ($time_end [1] + $time_end [0] - ($time_start [1] + $time_start [0])), 3 );
    		return array(
    			'transaction'=>false,
    			'time'=>date('Y-m-d H:i:s'),
    			'type'=>'fetchAll',
    			'text'=>$sql,
    			'duration'=>$duration,
    			'message'=>$e->getMessage(),
    			'data'=>array(),
    		);
    	}
	}
	public function fetchRow($sql='',$param=array()){

		$time_start = explode(' ',microtime());
    	try {
            $data = $this->_dbHive->fetchRow($sql,$param);
            $time_end = explode(' ',microtime());
            $duration = number_format ( ($time_end [1] + $time_end [0] - ($time_start [1] + $time_start [0])), 3 );
			return array(
    			'transaction'=>true,
    			'time'=>date('Y-m-d H:i:s'),
    			'type'=>'fetchRow',
    			'text'=>$sql,
    			'duration'=>$duration,
    			'message'=>'Success',
    			'data'=>$data,
    		);    		
    	} catch (Exception $e) {
            $time_end = explode(' ',microtime());
            $duration = number_format ( ($time_end [1] + $time_end [0] - ($time_start [1] + $time_start [0])), 3 );
    		return array(
    			'transaction'=>false,
    			'time'=>date('Y-m-d H:i:s'),
    			'type'=>'fetchRow',
    			'text'=>$sql,
    			'duration'=>$duration,
    			'message'=>$e->getMessage(),
    			'data'=>array(),
    		);
    	}
	}
	public function fetchOne($sql='',$param=array()){

		$time_start = explode(' ',microtime());
    	try {
            $data = $this->_dbHive->fetchOne($sql,$param);
            $time_end = explode(' ',microtime());
            $duration = number_format ( ($time_end [1] + $time_end [0] - ($time_start [1] + $time_start [0])), 3 );
			return array(
    			'transaction'=>true,
    			'time'=>date('Y-m-d H:i:s'),
    			'type'=>'fetchOne',
    			'text'=>$sql,
    			'duration'=>$duration,
    			'message'=>'Success',
    			'data'=>$data,
    		);    		
    	} catch (Exception $e) {
            $time_end = explode(' ',microtime());
            $duration = number_format ( ($time_end [1] + $time_end [0] - ($time_start [1] + $time_start [0])), 3 );
    		return array(
    			'transaction'=>false,
    			'time'=>date('Y-m-d H:i:s'),
    			'type'=>'fetchOne',
    			'text'=>$sql,
    			'duration'=>$duration,
    			'message'=>$e->getMessage(),
    			'data'=>array(),
    		);
    	}
	}

	public function getLastCacheTime($cid=''){
		if($cid==''){
			return array(
				'transaction'=>false,
				'message'=>'Invalid parameters.',
				'data'=> false,
			);
		}

        $file = APPLICATION_PATH.'/../cache/zend_cache---'.$cid;

        if(file_exists($file)){
            $last_cache_time = date('Y-m-d H:i:s',filemtime($file));
        } else {
            $last_cache_time = date('Y-m-d H:i:s');
        }

        return array(
        	'transaction'=>true,
        	'message'=>'Success',
        	'data'=>$last_cache_time,
        );
	}


	public function executeQueryEditor($sql='',$limit=1000,$offset=1,$clear_cache=''){

		if($sql==''){
			return array(
				'transaction'=>false,
				'time'=>date('Y-m-d H:i:s'),
				'type'=>'fetchAll',
				'text'=>null,
				'duration'=>(float)'0.000',
				'message'=>'Invalid parameters.',
				'data'=>null,
			);
		}

		$time_start = explode(' ',microtime());

		$sql = trim($sql); 
		$sqlLower = strtolower($sql); 
		$hasSelect = (substr($sqlLower, 0,6)=='select'?true:false);
		$hasLimit = (strpos($sqlLower, 'limit')!==false?true:false);

		if($hasSelect && !$hasLimit){
			// $sql = "SELECT * FROM (".$sql.") a LIMIT ".(int)$limit." OFFSET ".(int)$offset;
			$sql = "SELECT * FROM (".$sql.") a LIMIT ".(int)$limit;
		}

		// Zend_Debug::dump($sql);die();

		if(!is_bool($clear_cache)){
			if(isset($_POST['clear_cache'])){
				$clear_cache = (boolean)(int)$_POST['clear_cache'];				
			} else if(isset($_GET['clear_cache'])){
				$clear_cache = (boolean)(int)$_GET['clear_cache'];
			} else {
				$clear_cache = false;
			}
		}

		if(!$clear_cache){

			$input = array(
				'function'=>'executeQueryEditor',
				'sql'=>$sql,
			);

	        $z = new Model_Cache();
	        $cid = $z->get_id('Hive',$input);
	        $cache = $z->cachefunc(6400);
	        $data = $z->get_cache($cache, $cid);

	        if(!$data){

	        	$res = $this->fetchAll($sql); //Zend_Debug::dump($res);die();
	    		$cache->save($res['data'], $cid, array('systemaction'));
	    		$lct = $this->getLastCacheTime($cid);

		        $res['cache'] = array(
		        	'id'=>$cid,
		        	'time'=>$lct['data'],
		        );

				return $res;
	        }

    		$lct = $this->getLastCacheTime($cid);

            $time_end = explode(' ',microtime());
    		$duration = number_format ( ($time_end [1] + $time_end [0] - ($time_start [1] + $time_start [0])), 3 );

	        return array(
				'transaction'=>true,
				'time'=>date('Y-m-d H:i:s'),
				'type'=>'fetchAll',
				'text'=>$sql,
				'duration'=>$duration,
				'message'=>'Success load cache',
				'data'=>$data,
				'cache'=>array(
					'id'=>$cid,
					'time'=>$lct['data'],
				),
	        );
		}

    	$res = $this->fetchAll($sql);
		$cache->save($res['data'], $cid, array('systemaction'));
		$lct = $this->getLastCacheTime($cid);

        $res['cache'] = array(
        	'id'=>$cid,
        	'time'=>$lst['data'],
        );

		return $res;
	}

	public function exeSelectQry($sql=''){

		if($sql!=''){
			$_POST['qry'] = $sql;
		}

		$time_start = explode(' ',microtime());

		$limitVal = 1000;
		$limit = ' LIMIT '.$limitVal;
		if(isset($_POST['limit']) && (int)$_POST['limit']>0){
			$limitVal = (int)$_POST['limit'];
			$limit = ' LIMIT '.$limitVal;
		}

		$offsetVal = 1;
		$offset = " OFFSET ".$offsetVal;
		if(isset($_POST['offset']) && (int)$_POST['offset']>=0){
			$offsetVal = (int)$_POST['offset'];
			$offset = " OFFSET ".(int)$_POST['offset']."";
		}


		$qryLower = strtolower($_POST['qry']);
		$hasLimit = (strpos($qryLower,'limit')!==FALSE?TRUE:FALSE);
		$hasSelect = (strpos($qryLower,'select')!==FALSE&&strpos($qryLower,'select')==0?TRUE:FALSE);

		$sql = $_POST['qry'];
		if(isset($_POST['qry']) && $_POST['qry']!=''){
			if($hasSelect && !$hasLimit){
				$sql = "select * from (".$_POST['qry'].") a".$limit."".$offset."";
			}
		} else if(isset($_POST['database']) && $_POST['database']!='' && isset($_POST['table']) && $_POST['table']!=''){
			$sql = "select * from (select * from ".$_POST['database'].".".$_POST['table'].") a".$limit."".$offset;
		}

		// Zend_Debug::dump(strpos(strtolower($_POST['qry']),'limit'));//die();
		// Zend_Debug::dump($sql);die();

		$input = array(
			'function'=>'exeSelectQry',
			'qry'=>$sql,
			'limit'=>$limitVal,
			'offset'=>$offsetVal,
		);
		// Zend_Debug::dump($sql);die();
		try {

            $z = new Model_Cache();
            $cid = $z->get_id('Hive',$input);
            $cache = $z->cachefunc(6400);
            $data = $z->get_cache($cache, $cid); // Zend_Debug::dump($data);die();
            $time_end = explode(' ',microtime());

            // $data = false;

            $message = ' from cache';
            if(!$data || $_POST['is_cache']=='false' || $_POST['is_cache']==false){
				// $dbz = $this->connect_to_hive();
		        $data = $this->fetchAll($sql); $time_end = explode(' ',microtime());
                $cache->save($data, $cid, array('systemaction'));

                $message ='';
            }

            $parse_time = number_format ( ($time_end [1] + $time_end [0] - ($time_start [1] + $time_start [0])), 3 );  

			return array(
				'transaction'=>true,
				'sql'=>$sql,
				'data'=>$data,
				'parsing_data_time'=>$parse_time,
				'message'=>'success'.$message,
			);
			
		} catch (Exception $e) {

            $time_end = explode(' ',microtime());
            $parse_time = number_format ( ($time_end [1] + $time_end [0] - ($time_start [1] + $time_start [0])), 3 );  

			return array(
				'transaction'=>false,
				'sql'=>$sql,
				'data'=>array(),
				'parsing_data_time'=>$parse_time,
				'message'=>$e->getMessage(),
			);
		}
	}

	public function exeTransactionQry(){

		try {

			$sql = $_POST['qry'];

			$input = array(
				'function'=>'exeTransactionQry',
				'qry'=>$sql,
			);

			// Zend_Debug::dump($input);die();

            $z = new Model_Cache();
            $cid = $z->get_id('Hive',$input);
            $cache = $z->cachefunc(6400);
            $data = $z->get_cache($cache, $cid);
            // Zend_Debug::dump($data);die();

            // $data = false;

            $message = ' from cache';
            if(!$data || $_POST['is_cache']=='false' || $_POST['is_cache']==false){
				$dbz = $this->connect_to_hive();
		        $data = $dbz->query($sql);
                $cache->save($data, $cid, array('systemaction'));

                $message ='';
            }

            // Zend_Debug::dump($sql);//die();
            // Zend_Debug::dump($data);die();

			return array(
				'transaction'=>true,
				'sql'=>$sql,
				'data'=>$data,
				'message'=>'success'.$message,
			);		
		} catch (Exception $e) {

			return array(
				'transaction'=>false,
				'sql'=>$sql,
				'data'=>array(),
				'message'=>$e->getMessage(),
			);
		}
	}

	public function insert_bigquery_editor_log($p=array()){

		try {

			$authAdapter = Zend_Auth::getInstance ();
			$identity = $authAdapter->getIdentity ();

			$in = array(
				null,
				$p['sql_text'],
				(int)$p['sql_transaction'],
				$p['message'],
				$identity->uid,
			);

			$sql = "INSERT INTO bigbox.bigquery_editor_log VALUES (?,?,?,?,?,now())";
			// Zend_Debug::dump($in);
			// Zend_Debug::dump($sql);die();
			$this->_db->query($sql,$in);			

			return array('transaction'=>true,'message'=>'success');
		} catch (Exception $e) {
			// Zend_Debug::dump($e->getMessage());die();
			return array('transaction'=>false,'message'=>$e->getMessage());
		}
	}
	//============================================
	public function SchemasTablesColumns($clear_cache=false){

		if(!is_bool($clear_cache)){
			$clear_cache = false;
		}

        $input = array(
            'function'=>'SchemasTablesColumns'
        );

        $z = new Model_Cache();
        $cid = $z->get_id('Hive',$input);
        $cache = $z->cachefunc(604800);
        $data = $z->get_cache($cache, $cid);

        // Zend_Debug::dump($data);die();

        if(!$data || $clear_cache){
            $schemas = $this->getSchemas(false);
            // Zend_Debug::dump($schemas);die('XXX');

            $today = date('Y-m-d'); //Zend_Debug::dump($today);
            $last_cache_time = date('Y-m-d', strtotime(date('Y-m-d').' -1 days')); //Zend_Debug::dump($last_cache_time);
            if(isset($schemas['cache'])){
            	$last_cache_time = date('Y-m-d',strtotime($schemas['cache']['time'])); //Zend_Debug::dump($last_cache_time);
            }

            // Zend_Debug::dump($last_cache_time);die();

            if($today!=$last_cache_time){
	            // die('X');
            	$schemas = $this->getSchemas(true);
            }
            // die('Y');

            $data = array();
            if($schemas['transaction']){
	            foreach ($schemas['data'] as $k => $v) {
	            	$data[$k] = array(
	            		'database_name'=>$v['database_name'],
	            		'tables'=>array(),
	            	);
	                $tables = $this->getTables($v['database_name'],false);
	                // Zend_Debug::dump($tables);die();
		            if(isset($tables['cache'])){
		            	// Zend_Debug::dump($tables);
		            	// die('X');
		            	$last_cache_time = date('Y-m-d',strtotime($tables['cache']['time'])); //Zend_Debug::dump($last_cache_time);
		            }
		            // die('Y');

		            if($today!=$last_cache_time){
		            	// Zend_Debug::dump($tables);
		            	// die('X');
		            	$tables = $this->getTables($v['database_name'],true);
		            }
		            // die('Y');
		            // break;

	                $data[$k]['tables'] = $tables['data'];
	                // Zend_Debug::dump($data);die();

	                if($tables['transaction']){
		                foreach ($tables['data'] as $k1 => $v1) {
		                	$data[$k]['tables'][$k1]['columns'] = array();
		                	if(
		                		$v['database_name'].".".$v1['tab_name']!="staging.page_time_json" &&
		                		$v['database_name'].".".$v1['tab_name']!="staging.page_time_json2"
		                	){
			                    $columns = $this->getColumns($v['database_name'],$v1['tab_name'],false);
			                    // Zend_Debug::dump($columns);die();
					            if(isset($columns['cache'])){
					            	$last_cache_time = date('Y-m-d',strtotime($columns['cache']['time'])); //Zend_Debug::dump($last_cache_time);
					            }

					            if($today!=$last_cache_time){
					            	// Zend_Debug::dump($columns);
					            	// die('X');
					            	$columns = $this->getColumns($v['database_name'],$v1['tab_name'],true);
					            }

			                    $data[$k]['tables'][$k1]['columns'] = (array)$columns['data'];
			                    // Zend_Debug::dump($data);die();
		                	}
		                	// break;
		                }
		                // break;
	                }
	                // die('XXXX');	
	            }
            }
            // Zend_Debug::dump($data);die();

            $cache->save($data, $cid, array('systemaction'));
        }

        $file = APPLICATION_PATH.'/../cache/zend_cache---'.$cid;

        if(file_exists($file)){
            $last_cache_time = date('Y-m-d H:i:s',filemtime($file));
        } else {
            $last_cache_time = date('Y-m-d H:i:s');
        }
		return array(
			'transaction'=>true,
			'message'=>'SUCCESS ',
			'data'=>$data,
			'cache'=>array(
				'id'=>$cid,
				'time'=>$last_cache_time,
			),
		);
	}
	public function getSchemas($clear_cache=''){

		if(!is_bool($clear_cache)){
			if(isset($_POST['clear_cache'])){
				$clear_cache = (boolean)(int)$_POST['clear_cache'];				
			} else if(isset($_GET['clear_cache'])){
				$clear_cache = (boolean)(int)$_GET['clear_cache'];
			} else {
				$clear_cache = false;
			}
		}

		$time_start = explode(' ',microtime());

		$input = array(
			'function'=>'getSchemas',
		);

        $z = new Model_Cache();
        $cid = $z->get_id('Hive',$input);
        $cache = $z->cachefunc(86400);
        $data = $z->get_cache($cache, $cid);
        $sql = "SHOW DATABASES";

        if(!$data || $clear_cache){
			$res = $this->fetchAll($sql);
	    	$cache->save($res['data'], $cid, array('systemaction'));

	        $file = APPLICATION_PATH.'/../cache/zend_cache---'.$cid;

	        if(file_exists($file)){
	            $last_cache_time = date('Y-m-d H:i:s',filemtime($file));
	        } else {
	            $last_cache_time = date('Y-m-d H:i:s');
	        }

	        $res['cache'] = array(
	        	'id'=>$cid,
	        	'time'=>$last_cache_time,
	        );
			return $res;
        }

        $file = APPLICATION_PATH.'/../cache/zend_cache---'.$cid;

        if(file_exists($file)){
            $last_cache_time = date('Y-m-d H:i:s',filemtime($file));
        } else {
            $last_cache_time = date('Y-m-d H:i:s');
        }
        
        $time_end = explode(' ',microtime());
        $duration = number_format ( ($time_end [1] + $time_end [0] - ($time_start [1] + $time_start [0])), 3 );

		return array(
			'transaction'=>true,
			'time'=>date('Y-m-d H:i:s'),
			'type'=>'fetchAll',
			'text'=>$sql,
			'duration'=>$duration,
			'message'=>'Success load cache',
			'data'=>$data,
			'cache'=>array(
				'id'=>$cid,
				'time'=>$last_cache_time,
			),
		);
	}
	public function getTables($database_name='',$clear_cache=''){

		if($database_name==''){
			return array(
				'transaction'=>false,
				'time'=>date('Y-m-d H:i:s'),
				'type'=>'fetchAll',
				'text'=>null,
				'duration'=>(float)'0.000',
				'message'=>'Invalid parameters.',
				'data'=>null,
			);
		}

		if(!is_bool($clear_cache)){
			if(isset($_POST['clear_cache'])){
				$clear_cache = (boolean)(int)$_POST['clear_cache'];				
			} else if(isset($_GET['clear_cache'])){
				$clear_cache = (boolean)(int)$_GET['clear_cache'];
			} else {
				$clear_cache = false;
			}
		}

		// Zend_Debug::dump($clear_cache);die();

		$time_start = explode(' ',microtime());

		$input = array(
			'function'=>'getTables',
			'database_name'=>$database_name,
		);

        $z = new Model_Cache();
        $cid = $z->get_id('Hive',$input);
        $cache = $z->cachefunc(86400);
        $data = $z->get_cache($cache, $cid);
        $sql = "SHOW TABLES IN ".$database_name;

        if(!$data || $clear_cache){
			$res = $this->fetchAll($sql);
	    	$cache->save($res['data'], $cid, array('systemaction'));

	        $file = APPLICATION_PATH.'/../cache/zend_cache---'.$cid;

	        if(file_exists($file)){
	            $last_cache_time = date('Y-m-d H:i:s',filemtime($file));
	        } else {
	            $last_cache_time = date('Y-m-d H:i:s');
	        }

	        $res['cache'] = array(
	        	'id'=>$cid,
	        	'time'=>$last_cache_time,
	        );

			return $res;
        }

        $file = APPLICATION_PATH.'/../cache/zend_cache---'.$cid;

        if(file_exists($file)){
            $last_cache_time = date('Y-m-d H:i:s',filemtime($file));
        } else {
            $last_cache_time = date('Y-m-d H:i:s');
        }
        
        $time_end = explode(' ',microtime());
        $duration = number_format ( ($time_end [1] + $time_end [0] - ($time_start [1] + $time_start [0])), 3 );

		return array(
			'transaction'=>true,
			'time'=>date('Y-m-d H:i:s'),
			'type'=>'fetchAll',
			'text'=>$sql,
			'duration'=>$duration,
			'message'=>'Success load cache',
			'data'=>$data,
			'cache'=>array(
				'id'=>$cid,
				'time'=>$last_cache_time,
			),
		);
	}
	public function getColumns($database_name='',$table_name='',$clear_cache=''){

		if($database_name==''||$table_name==''){
			return array(
				'transaction'=>false,
				'time'=>date('Y-m-d H:i:s'),
				'type'=>'fetchAll',
				'text'=>null,
				'duration'=>(float)'0.000',
				'message'=>'Invalid parameters.',
				'data'=>null,
			);
		}

		if(!is_bool($clear_cache)){
			if(isset($_POST['clear_cache'])){
				$clear_cache = (boolean)(int)$_POST['clear_cache'];				
			} else if(isset($_GET['clear_cache'])){
				$clear_cache = (boolean)(int)$_GET['clear_cache'];
			} else {
				$clear_cache = false;
			}
		}

		// Zend_Debug::dump($clear_cache);die();

		$time_start = explode(' ',microtime());

		$input = array(
			'function'=>'getColumns',
			'database_name'=>$database_name,
			'table_name'=>$table_name,
		);

        $z = new Model_Cache();
        $cid = $z->get_id('Hive',$input);
        $cache = $z->cachefunc(86400);
        $data = $z->get_cache($cache, $cid); //Zend_Debug::dump($data);die();
        $sql = "DESCRIBE ".$database_name.".".$table_name;

        if(!$data || $clear_cache){
			$res = $this->fetchAll($sql);
	    	$cache->save($res['data'], $cid, array('systemaction'));

	        $file = APPLICATION_PATH.'/../cache/zend_cache---'.$cid;

	        if(file_exists($file)){
	            $last_cache_time = date('Y-m-d H:i:s',filemtime($file));
	        } else {
	            $last_cache_time = date('Y-m-d H:i:s');
	        }

	        $res['cache'] = array(
	        	'id'=>$cid,
	        	'time'=>$last_cache_time,
	        );
			return $res;
        }

        $file = APPLICATION_PATH.'/../cache/zend_cache---'.$cid;

        if(file_exists($file)){
            $last_cache_time = date('Y-m-d H:i:s',filemtime($file));
        } else {
            $last_cache_time = date('Y-m-d H:i:s');
        }
        
        $time_end = explode(' ',microtime());
        $duration = number_format ( ($time_end [1] + $time_end [0] - ($time_start [1] + $time_start [0])), 3 );

		return array(
			'transaction'=>true,
			'time'=>date('Y-m-d H:i:s'),
			'type'=>'fetchAll',
			'text'=>$sql,
			'duration'=>$duration,
			'message'=>'Success load cache',
			'data'=>$data,
			'cache'=>array(
	        	'id'=>$cid,
	        	'time'=>$last_cache_time,
	        ),
		);
	}

	public function getProperties($database_name='',$table_name='',$clear_cache=''){

		if($database_name==''||$table_name==''){
			return array(
				'transaction'=>false,
				'time'=>date('Y-m-d H:i:s'),
				'type'=>'fetchAll',
				'text'=>null,
				'duration'=>(float)'0.000',
				'message'=>'Invalid parameters.',
				'data'=>null,
			);
		}

		if(!is_bool($clear_cache)){
			if(isset($_POST['clear_cache'])){
				$clear_cache = (boolean)(int)$_POST['clear_cache'];				
			} else if(isset($_GET['clear_cache'])){
				$clear_cache = (boolean)(int)$_GET['clear_cache'];
			} else {
				$clear_cache = false;
			}
		}

		// Zend_Debug::dump($clear_cache);die();

		$time_start = explode(' ',microtime());

		$input = array(
			'function'=>'getProperties',
			'database_name'=>$database_name,
			'table_name'=>$table_name,
		);

        $z = new Model_Cache();
        $cid = $z->get_id('Hive',$input);
        $cache = $z->cachefunc(86400);
        $data = $z->get_cache($cache, $cid);
        $sql = "SHOW TBLPROPERTIES ".$database_name.".".$table_name;

        if(!$data || $clear_cache){
			$res = $this->fetchAll($sql);
	    	$cache->save($res['data'], $cid, array('systemaction'));

	        $file = APPLICATION_PATH.'/../cache/zend_cache---'.$cid;

	        if(file_exists($file)){
	            $last_cache_time = date('Y-m-d H:i:s',filemtime($file));
	        } else {
	            $last_cache_time = date('Y-m-d H:i:s');
	        }

	        $res['cache'] = array(
	        	'id'=>$cid,
	        	'time'=>$last_cache_time,
	        );
			return $res;
        }

        $file = APPLICATION_PATH.'/../cache/zend_cache---'.$cid;

        if(file_exists($file)){
            $last_cache_time = date('Y-m-d H:i:s',filemtime($file));
        } else {
            $last_cache_time = date('Y-m-d H:i:s');
        }
        
        $time_end = explode(' ',microtime());
        $duration = number_format ( ($time_end [1] + $time_end [0] - ($time_start [1] + $time_start [0])), 3 );

		return array(
			'transaction'=>true,
			'time'=>date('Y-m-d H:i:s'),
			'type'=>'fetchAll',
			'text'=>$sql,
			'duration'=>$duration,
			'message'=>'Success load cache',
			'data'=>$data,
			'cache'=>array(
				'id'=>$cid,
				'time'=>$last_cache_time,
			),
		);
	}
	public function getSamples($database_name='',$table_name='',$clear_cache=''){

		if($database_name==''||$table_name==''){
			return array(
				'transaction'=>false,
				'time'=>date('Y-m-d H:i:s'),
				'type'=>'fetchAll',
				'text'=>null,
				'duration'=>(float)'0.000',
				'message'=>'Invalid parameters.',
				'data'=>null,
			);
		}

		if(!is_bool($clear_cache)){
			if(isset($_POST['clear_cache'])){
				$clear_cache = (boolean)(int)$_POST['clear_cache'];				
			} else if(isset($_GET['clear_cache'])){
				$clear_cache = (boolean)(int)$_GET['clear_cache'];
			} else {
				$clear_cache = false;
			}
		}

		// Zend_Debug::dump($clear_cache);die();

		$time_start = explode(' ',microtime());

		$input = array(
			'function'=>'getSamples',
			'database_name'=>$database_name,
			'table_name'=>$table_name,
		);

        $z = new Model_Cache();
        $cid = $z->get_id('Hive',$input);
        $cache = $z->cachefunc(86400);
        $data = $z->get_cache($cache, $cid);
        $sql = "SELECT * FROM ".$database_name.".".$table_name." LIMIT 10";

        if(!$data || $clear_cache){
			$res = $this->fetchAll($sql);
	    	$cache->save($res['data'], $cid, array('systemaction'));

	        $file = APPLICATION_PATH.'/../cache/zend_cache---'.$cid;

	        if(file_exists($file)){
	            $last_cache_time = date('Y-m-d H:i:s',filemtime($file));
	        } else {
	            $last_cache_time = date('Y-m-d H:i:s');
	        }

	        $res['cache'] = array(
	        	'id'=>$cid,
	        	'time'=>$last_cache_time,
	        );
			return $res;
        }

        $file = APPLICATION_PATH.'/../cache/zend_cache---'.$cid;

        if(file_exists($file)){
            $last_cache_time = date('Y-m-d H:i:s',filemtime($file));
        } else {
            $last_cache_time = date('Y-m-d H:i:s');
        }
        
        $time_end = explode(' ',microtime());
        $duration = number_format ( ($time_end [1] + $time_end [0] - ($time_start [1] + $time_start [0])), 3 );

		return array(
			'transaction'=>true,
			'time'=>date('Y-m-d H:i:s'),
			'type'=>'fetchAll',
			'text'=>$sql,
			'duration'=>$duration,
			'message'=>'Success load cache',
			'data'=>$data,
			'cache'=>array(
				'id'=>$cid,
				'time'=>$last_cache_time,
			),
		);
	}
	public function getCount($database_name='',$table_name='',$clear_cache=''){

		if($database_name==''||$table_name==''){
			return array(
				'transaction'=>false,
				'time'=>date('Y-m-d H:i:s'),
				'type'=>'fetchAll',
				'text'=>null,
				'duration'=>(float)'0.000',
				'message'=>'Invalid parameters.',
				'data'=>null,
			);
		}

		if(!is_bool($clear_cache)){
			if(isset($_POST['clear_cache'])){
				$clear_cache = (boolean)(int)$_POST['clear_cache'];				
			} else if(isset($_GET['clear_cache'])){
				$clear_cache = (boolean)(int)$_GET['clear_cache'];
			} else {
				$clear_cache = false;
			}
		}

		// Zend_Debug::dump($clear_cache);die();

		$time_start = explode(' ',microtime());

		$input = array(
			'function'=>'getCount',
			'database_name'=>$database_name,
			'table_name'=>$table_name,
		);

        $z = new Model_Cache();
        $cid = $z->get_id('Hive',$input);
        $cache = $z->cachefunc(86400);
        $data = $z->get_cache($cache, $cid);
        $sql = "DESCRIBE FORMATTED ".$database_name.".".$table_name;

        // $data = false;

        if(!$data || $clear_cache){
			$res = $this->fetchAll($sql);
			// Zend_Debug::dump($res);die();
			if($res['transaction']){
				$data_type = array_column($res['data'], 'data_type');
				$data_type = array_map('trim', $data_type);
				$key = array_search('numRows',$data_type);
				// Zend_Debug::dump($key);die();
				if(is_numeric($key) && (int)trim($res['data'][$key]['comment'])>0){
	    			$res['data'] = (int)trim($res['data'][$key]['comment']);
	    			$cache->save($res['data'], $cid, array('systemaction'));

			        $file = APPLICATION_PATH.'/../cache/zend_cache---'.$cid;
			        if(file_exists($file)){
			            $last_cache_time = date('Y-m-d H:i:s',filemtime($file));
			        } else {
			            $last_cache_time = date('Y-m-d H:i:s');
			        }

			        $res['cache'] = array(
			        	'id'=>$cid,
			        	'time'=>$last_cache_time,
			        );
	    			return $res;
		        }
			}

        	$sql = "SELECT COUNT(*) numRows ".$database_name.".".$table_name;
        	$res = $this->fetchOne($sql);

        	if($res['transaction']){
    			$cache->save($res['data'], $cid, array('systemaction'));

		        $file = APPLICATION_PATH.'/../cache/zend_cache---'.$cid;
		        if(file_exists($file)){
		            $last_cache_time = date('Y-m-d H:i:s',filemtime($file));
		        } else {
		            $last_cache_time = date('Y-m-d H:i:s');
		        }

		        $res['cache'] = array(
		        	'id'=>$cid,
		        	'time'=>$last_cache_time,
		        );
    			return $res;        		
        	} else {
    			$cache->save(-1, $cid, array('systemaction'));

		        $file = APPLICATION_PATH.'/../cache/zend_cache---'.$cid;
		        if(file_exists($file)){
		            $last_cache_time = date('Y-m-d H:i:s',filemtime($file));
		        } else {
		            $last_cache_time = date('Y-m-d H:i:s');
		        }

		        $res['cache'] = array(
		        	'id'=>$cid,
		        	'time'=>$last_cache_time,
		        );
		        return $res;
        	}
        }

        $file = APPLICATION_PATH.'/../cache/zend_cache---'.$cid;

        if(file_exists($file)){
            $last_cache_time = date('Y-m-d H:i:s',filemtime($file));
        } else {
            $last_cache_time = date('Y-m-d H:i:s');
        }
        
        $time_end = explode(' ',microtime());
        $duration = number_format ( ($time_end [1] + $time_end [0] - ($time_start [1] + $time_start [0])), 3 );

		return array(
			'transaction'=>true,
			'time'=>date('Y-m-d H:i:s'),
			'type'=>'fetchOne',
			'text'=>$sql,
			'duration'=>$duration,
			'message'=>'Success load cache',
			'data'=>(int)$data,
			'cache'=>array(
				'id'=>$cid,
				'time'=>$last_cache_time,
			),
		);
	}

	public function getCountx($database_name='',$table_name='',$clear_cache=false){

		if(isset($_POST['clear_cache'])){
			$clear_cache = (boolean)$_POST['clear_cache'];
		}

		if(isset($_POST['database_name']) && $_POST['database_name']!=''){
			$database_name = $_POST['database_name'];
			$table_name = $_POST['table_name'];
		}

		if($database_name==''||$table_name==''){
			return array(
				'transaction'=>false,
				'sql'=>NULL,
				'data'=>array(),
				'message'=>'failed',
			);
		}


		$input = array(
			'function'=>'getCount',
			'database_name'=>$database_name,
			'table_name'=>$table_name,
		);

        $z = new Model_Cache();
        $cid = $z->get_id('Hive',$input);
        $cache = $z->cachefunc(86400);
        $data = $z->get_cache($cache, $cid);

        if($clear_cache){
        	$data = false;
        }
		
		$sql = "DESCRIBE FORMATTED ".$database_name.".".$table_name;

        // Zend_Debug::dump($data);die();
		try {

            $message =' from cache';
            if(!$data){
            	$dbz = $this->connect_to_hive();
				$tmp = $dbz->fetchAll($sql);
        		
        		// $tmp = $this->_db->fetchAll($sql);
        		

				$data_type = array_column($tmp, 'data_type');
				$data_type = array_map('trim', $data_type);
				$key = array_search('numRows',$data_type);
				// echo $key."\n";

				if(is_numeric($key) && (int)trim($tmp[$key]['comment'])>0){
		        	$data = (int)trim($tmp[$key]['comment']);
				} else {
					$sql = "select count(*) total from ".$database_name.".".$table_name;
		   			$data = $dbz->fetchOne($sql);
		   			$data = (int)$data;
				}

                $cache->save($data, $cid, array('systemaction'));

                $message ='';
            }

			return array(
				'transaction'=>true,
				'sql'=>$sql,
				'data'=>$data,
				'message'=>'success'.$message,
				'cid'=>$cid,
			);
		} catch (Exception $e) {

			// Zend_Debug::dump($e->getMessage());die($sql);

			return array(
				'transaction'=>false,
				'sql'=>$sql,
				'data'=>-1,
				'message'=>$e->getMessage(),
			);
		}
	}
	public function getCountByQuery($clear_cache=false){

		if(isset($_POST['clear_cache'])){
			$clear_cache = (boolean)$_POST['clear_cache'];
		}

		// Zend_Debug::dump($clear_cache);
		// Zend_Debug::dump($_POST);
		// die();

		if($_POST['qry']==''){
			return array(
				'transaction'=>false,
				'sql'=>NULL,
				'data'=>array(),
				'message'=>'failed',
			);
		}

		try {

			$sql = "SELECT COUNT(*) FROM (".$_POST['qry'].") a";

			$input = array(
				'function'=>'getCountByQuery',
				'sql'=>$sql,
			);

            $z = new Model_Cache();
            $cid = $z->get_id('Hive',$input);
            $cache = $z->cachefunc(86400);
            $data = $z->get_cache($cache, $cid);

            // Zend_Debug::dump($data);die();

            if($clear_cache){
            	$data = false;
            }

            if(!$data){
            	$dbz = $this->connect_to_hive();
		        $data = $dbz->fetchOne($sql);
		        $data = number_format($data,0,'.',',');
                $cache->save($data, $cid, array('systemaction'));

                $message =' from cache';
            }			

			return array(
				'transaction'=>true,
				'sql'=>$sql,
				'data'=>$data,
				'message'=>'success'.$message,
			);

		} catch (Exception $e) {
			return array(
				'transaction'=>false,
				'sql'=>$sql,
				'data'=>array(),
				'message'=>$e->getMessage().'=='.$sql,
			);
		}
	}
}
