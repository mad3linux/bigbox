<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Messaging extends Zend_Db_Table_Abstract{

public function getAlertGroups($gid = null) {
	$where = "";
	
	if($gid!=null || $gid!=''){
		$where = " where a.gid='".$gid."' ";
	}
	$q = "select
            distinct(a.group_name) as uname,a.group_name as fullname, a.gid as id, b.attr_code,b.attr_val
          from
            z_groups a right join z_group_attr b on a.gid=b.gid 
			".$where."
          order by uname";
	$data = array();
	//die($q);
	try {
		$temp = $this->_db->fetchAll($q);
		//Zend_Debug::dump($temp);die();
		if(count($temp)>0){
			foreach($temp as $k=>$v){
				if(!isset($data[$v['id']])){
					$data[$v['id']]['name'] = $v['uname'];
				}
				$data[$v['id']][$v['attr_code']] = $v['attr_val'];
			}
		}
		return $data;
	} catch (Exception $e) {
	   Zend_Debug::dump($e->getMessage());die($q);		
	}
}

public function getAlertUsers($uid = null) {
	$where = "";
	
	if($uid!=null || $uid!=''){
		$where = " where a.id='".$uid."' ";
	}
	$q = "select
            distinct(a.uname),a.fullname, a.id as uid, b.attr_code,b.attr_val
          from
            z_users a right join z_user_attr b on a.id=b.uid 
			".$where."
          order by uname";
	$data = array();
	//die($q);
	try {
		$temp = $this->_db->fetchAll($q);
		//Zend_Debug::dump($temp);die();
		if(count($temp)>0){
			foreach($temp as $k=>$v){
				if(!isset($data[$v['uid']])){
					$data[$v['uid']]['name'] = $v['uname'];
				}
				$data[$v['uid']][$v['attr_code']] = $v['attr_val'];
			}
		}
		return $data;
	} catch (Exception $e) {
	   Zend_Debug::dump($e->getMessage());die($q);		
	}
}

public function getAlertTypes() {
	$q = "select attr_code from z_group_attr a union select attr_code from z_user_attr b";
	$data = array();
	//die($q);
	try {
		$temp = $this->_db->fetchAll($q);
		//Zend_Debug::dump($temp);die();
		if(count($temp)>0){
			foreach($temp as $k=>$v){
				if(!in_array($v['attr_code'],$data)){
					$data[] = $v['attr_code'];
				}
			}
		}
		return $data;
	} catch (Exception $e) {
	   Zend_Debug::dump($e->getMessage());die($q);		
	}
}

function updategroupattr($gid,$attrcode,$attrval){
	$q = "UPDATE z_group_attr set attr_val = '".$attrval."' where gid='".$gid."' and attr_code='".$attrcode."'";
	//die($q);
	try {
		$temp = $this->_db->query($q);
		return $temp;
	} catch (Exception $e) {
	   Zend_Debug::dump($e->getMessage());die($q);		
	}
}

function insertgroupattr($gid,$attrcode,$attrval){
	$attr = $this->getAlertGroups($gid);
	if(isset($attr[$gid]) && isset($attr[$gid][$attrcode])){
		$this->updategroupattr($gid,$attrcode,$attrval);
	}else{
		$q = "INSERT INTO z_group_attr (gid,attr_code,attr_val) values ('".$gid."','".$attrcode."','".$attrval."')";
		//die($q);
		try {
			$temp = $this->_db->query($q);
			return $temp;
		} catch (Exception $e) {
		   Zend_Debug::dump($e->getMessage());die($q);		
		}
	}
}

function updateuserattr($uid,$attrcode,$attrval){
	$q = "UPDATE z_user_attr set attr_val = '".$attrval."' where uid='".$uid."' and attr_code='".$attrcode."'";
	//die($q);
	try {
		$temp = $this->_db->query($q);
		return $temp;
	} catch (Exception $e) {
	   Zend_Debug::dump($e->getMessage());die($q);		
	}
}

function insertuserattr($uid,$attrcode,$attrval){
	$attr = $this->getAlertUsers($uid);
	//Zend_Debug::dump($attr[$uid][$attrcode]);die();
	if(isset($attr[$uid]) && isset($attr[$uid][$attrcode])){
		$this->updateuserattr($uid,$attrcode,$attrval);
	}else{
		$q = "INSERT INTO z_user_attr (uid,attr_code,attr_val) values (".$uid.",'".$attrcode."','".$attrval."')";
		//die($q);
		try {
			$temp = $this->_db->query($q);
			return $temp;
		} catch (Exception $e) {
		   Zend_Debug::dump($e->getMessage());die($q);		
		}
	}
}

function count_listgroup($param = null) {
	//Zend_Debug::dump($param);//die('qqq');
	$types = $this->getAlertTypes();
	//Zend_Debug::dump($types);die('qqq');
	$data = 0;
	$aColumns = array( 'act','no','a.group_name' );
	$aColumns = array_merge($aColumns, $types);
	//Zend_Debug::dump($aColumns);die('qqq');
	$bypass = array(0,1);
	
	/*
	* Ordering
	*/
	$sOrder = "";
	if ( isset( $_GET['iSortCol_0'] ) ){
		if( (int)$_GET['iSortCol_0']>2){
			$orderby = "attr_code";
		}else{
			$orderby = "a.group_name";
		}
		$sOrder = "ORDER BY  ";
		if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_0']) ] == "true" ){
			$sOrder .= $orderby." ".($_GET['sSortDir_0']==='asc' ? 'asc' : 'desc') .", ";
		}

		$sOrder = substr_replace( $sOrder, "", -2 );
		if ( $sOrder == "ORDER BY" ){
			$sOrder = "";
		}
	}
	
	$searchLike = array();

	/* 
	* Filtering
	* NOTE this does not match the built-in DataTables filtering which does it
	* word by word on any field. It's possible to do here, but concerned about efficiency
	* on very large tables, and MySQL's regex functionality is very limited
	*/
	$sWhere = "";

	if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
		$searchLike[2] = $_GET['sSearch'];
		foreach($aColumns as $k=>$v){
			if($k>2){
				$searchLike[$k] = $_GET['sSearch'];
			}
		}
	}
	
	for ( $i=0 ; $i<count($aColumns) ; $i++ ){
		if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
			$searchLike[$i] = $_GET['sSearch_'.$i];
		}
	}
	
	$code = array();
	$sql = "select count(group_name) from (select distinct(a.group_name) from z_groups a right join z_group_attr b on a.gid=b.gid";
	foreach($searchLike as $k=>$v){
		if($k>2){
			$code[] = $sql." where attr_code='".$aColumns[$k]."' and attr_val LIKE '%".$v."%' ".$sOrder.") c";
		}else{
			$code[] = $sql." where ".$aColumns[$k]." LIKE '%".$v."%' ".$sOrder.") c";
		}
	}
	//Zend_Debug::dump($code);//die('qqq');
	//Zend_Debug::dump(implode(' union ',$code));die('qqq');
	
	if(count($code)>2){
		$qry=implode(' union ',$code);
	}else{
		$qry= "select count(group_name) from (select distinct(a.group_name) from z_groups a right join z_group_attr b on a.gid=b.gid ".$sOrder.") c ";
	}
	//die($qry);
	try {
		$data = $this->_db->fetchOne($qry);
		//Zend_Debug::dump($data);die();
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);
	}
	return $data;
}

function get_listgroup($param = null) {
	//Zend_Debug::dump($param);//die('qqq');
	$types = $this->getAlertTypes();
	//Zend_Debug::dump($types);die('qqq');
	$data = 0;
	$aColumns = array( 'act','no','a.group_name' );
	$aColumns = array_merge($aColumns, $types);
	//Zend_Debug::dump($aColumns);die('qqq');
	$bypass = array(0,1);
	
	/*
	* Ordering
	*/
	$sOrder = "";
	if ( isset( $_GET['iSortCol_0'] ) ){
		if( (int)$_GET['iSortCol_0']>2){
			$orderby = "attr_code";
		}else{
			$orderby = "group_name";
		}
		$sOrder = "ORDER BY  ";
		if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_0']) ] == "true" ){
			$sOrder .= "`".$orderby."` ".($_GET['sSortDir_0']==='asc' ? 'asc' : 'desc') .", ";
		}

		$sOrder = substr_replace( $sOrder, "", -2 );
		if ( $sOrder == "ORDER BY" ){
			$sOrder = "";
		}
	}
	
	$searchLike = array();

	/* 
	* Filtering
	* NOTE this does not match the built-in DataTables filtering which does it
	* word by word on any field. It's possible to do here, but concerned about efficiency
	* on very large tables, and MySQL's regex functionality is very limited
	*/
	$sWhere = "";

	if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
		$searchLike[2] = $_GET['sSearch'];
		foreach($aColumns as $k=>$v){
			if($k>2){
				$searchLike[$k] = $_GET['sSearch'];
			}
		}
	}
	
	for ( $i=0 ; $i<count($aColumns) ; $i++ ){
		if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
			$searchLike[$i] = $_GET['sSearch_'.$i];
		}
	}
	
	$code = array();
	$sql = "select * from (select a.group_name, a.gid, b.gid as gid2, b.attr_code,b.attr_val from z_groups a right join z_group_attr b on a.gid=b.gid";
	foreach($searchLike as $k=>$v){
		if($k>2){
			$code[] = $sql." where attr_code='".$aColumns[$k]."' and attr_val LIKE '%".$v."%' ".$sOrder.") c";
		}else{
			$code[] = $sql." where ".$aColumns[$k]." LIKE '%".$v."%' ".$sOrder.") c";
		}
	}
	//Zend_Debug::dump($code);//die('qqq');
	//Zend_Debug::dump(implode(' union ',$code));die('qqq');
	
	if(count($code)>0){
		$qry=implode(' union ',$code);
	}else{
		$qry= "select * from (select a.group_name, a.gid, b.gid as gid2, b.attr_code,b.attr_val from z_groups a right join z_group_attr b on a.gid=b.gid order by a.group_name) c ".$sOrder;
	}
	//die($qry);
	$data = array();
	try {
		$temp = $this->_db->fetchAll($qry);
		//Zend_Debug::dump($temp);die();
		if(count($temp)>0){
			foreach($temp as $k=>$v){
				if($v['gid']==null || $v['gid']==''){
					$v['gid']=$v['gid2'];
				}
				if(!isset($data[$v['gid']])){
					$data[$v['gid']]['name'] = $v['group_name'];
				}
				$data[$v['gid']][$v['attr_code']] = $v['attr_val'];
			}
		}
		
		foreach($data as $k=>$v){
			$qry2 = "select * from (select a.group_name, a.gid, b.gid as gid2, b.attr_code,b.attr_val from z_groups a right join z_group_attr b on a.gid=b.gid where b.gid='".$k."' order by a.group_name) c ".$sOrder;
			$temp2 = $this->_db->fetchAll($qry2);
			//Zend_Debug::dump($temp2);die();
			if(count($temp2)>0){
				foreach($temp2 as $k2=>$v2){
					if($v2['gid']==null || $v2['gid']==''){
						$v2['gid']=$v2['gid2'];
					}
					//die($v2['gid']);
					if(!isset($data[$v2['gid']][$v2['attr_code']])){
						$data[$v2['gid']][$v2['attr_code']] = $v2['attr_val'];
					}
				}
			}
			//Zend_Debug::dump($data);die();
		
			foreach($types as $k2=>$v2){
				//die($v2);
				if(!isset($data[$k][$v2])){
					$data[$k][$v2] = "";
				}
			}
		}
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);
	}
	//Zend_Debug::dump($data);die();
	return $data;
}

function count_listuser($param = null) {
	//Zend_Debug::dump($param);//die('qqq');
	$types = $this->getAlertTypes();
	//Zend_Debug::dump($types);die('qqq');
	$data = 0;
	$aColumns = array( 'act','no','a.uname','a.fullname' );
	$aColumns = array_merge($aColumns, $types);
	//Zend_Debug::dump($aColumns);die('qqq');
	$bypass = array(0,1);
	
	/*
	* Ordering
	*/
	$sOrder = "";
	if ( isset( $_GET['iSortCol_0'] ) ){
		if( (int)$_GET['iSortCol_0']>3){
			$orderby = "attr_code";
		}else{
			$orderby = $aColumns[$_GET['iSortCol_0']];
		}
		$sOrder = "ORDER BY  ";
		if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_0']) ] == "true" ){
			$sOrder .= $orderby." ".($_GET['sSortDir_0']==='asc' ? 'asc' : 'desc') .", ";
		}

		$sOrder = substr_replace( $sOrder, "", -2 );
		if ( $sOrder == "ORDER BY" ){
			$sOrder = "";
		}
	}
	
	$searchLike = array();

	/* 
	* Filtering
	* NOTE this does not match the built-in DataTables filtering which does it
	* word by word on any field. It's possible to do here, but concerned about efficiency
	* on very large tables, and MySQL's regex functionality is very limited
	*/
	$sWhere = "";

	if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
		$searchLike[2] = $_GET['sSearch'];
		$searchLike[3] = $_GET['sSearch'];
		foreach($aColumns as $k=>$v){
			if($k>3){
				$searchLike[$k] = $_GET['sSearch'];
			}
		}
	}
	
	for ( $i=0 ; $i<count($aColumns) ; $i++ ){
		if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
			$searchLike[$i] = $_GET['sSearch_'.$i];
		}
	}
	//Zend_Debug::dump($searchLike);//die('qqq');
	
	$code = array();
	$sql = "select count(uname) from (select distinct(a.uname) from z_users a right join z_user_attr b on a.id=b.uid";
	foreach($searchLike as $k=>$v){
		if($k>3){
			$code[] = $sql." where attr_code='".$aColumns[$k]."' and attr_val LIKE '%".$v."%' ".$sOrder.") c";
		}else{
			$code[] = $sql." where ".$aColumns[$k]." LIKE '%".$v."%' ".$sOrder.") c";
		}
	}
	//Zend_Debug::dump($code);die('qqq');
	//Zend_Debug::dump(implode(' union ',$code));die('qqq');
	
	if(count($code)>0){
		$qry=implode(' union ',$code);
	}else{
		$qry= "select count(uname) from (select distinct(a.uname) from z_users a right join z_user_attr b on a.id=b.uid ".$sOrder.") c ";
	}
	//die($qry);
	try {
		$data = $this->_db->fetchOne($qry);
		//Zend_Debug::dump($data);die();
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);
	}
	return $data;
}

function get_listuser($param = null) {
	//Zend_Debug::dump($param);//die('qqq');
	$types = $this->getAlertTypes();
	//Zend_Debug::dump($types);die('qqq');
	$data = 0;
	$aColumns = array( 'act','no','a.uname','a.fullname' );
	$aColumns = array_merge($aColumns, $types);
	//Zend_Debug::dump($aColumns);die('qqq');
	$bypass = array(0,1);
	
	/*
	* Ordering
	*/
	$sOrder = "";
	if ( isset( $_GET['iSortCol_0'] ) ){
		if( (int)$_GET['iSortCol_0']>3){
			$orderby = "attr_code";
		}else{
			$orderby = $aColumns[$_GET['iSortCol_0']];
		}
		$sOrder = "ORDER BY  ";
		if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_0']) ] == "true" ){
			$sOrder .= $orderby." ".($_GET['sSortDir_0']==='asc' ? 'asc' : 'desc') .", ";
		}

		$sOrder = substr_replace( $sOrder, "", -2 );
		if ( $sOrder == "ORDER BY" ){
			$sOrder = "";
		}
	}
	
	$searchLike = array();

	/* 
	* Filtering
	* NOTE this does not match the built-in DataTables filtering which does it
	* word by word on any field. It's possible to do here, but concerned about efficiency
	* on very large tables, and MySQL's regex functionality is very limited
	*/
	$sWhere = "";

	if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
		$searchLike[2] = $_GET['sSearch'];
		$searchLike[3] = $_GET['sSearch'];
		foreach($aColumns as $k=>$v){
			if($k>3){
				$searchLike[$k] = $_GET['sSearch'];
			}
		}
	}
	
	for ( $i=0 ; $i<count($aColumns) ; $i++ ){
		if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
			$searchLike[$i] = $_GET['sSearch_'.$i];
		}
	}
	
	$code = array();
	$sql = "select * from (select a.uname,a.fullname, a.id as uid, b.attr_code,b.attr_val from z_users a right join z_user_attr b on a.id=b.uid";
	foreach($searchLike as $k=>$v){
		if($k>3){
			$code[] = $sql." where attr_code='".$aColumns[$k]."' and attr_val LIKE '%".$v."%' ".$sOrder.") c";
		}else{
			$code[] = $sql." where ".$aColumns[$k]." LIKE '%".$v."%' ".$sOrder.") c";
		}
	}
	//Zend_Debug::dump($code);//die('qqq');
	//Zend_Debug::dump(implode(' union ',$code));die('qqq');
	
	if(count($code)>0){
		$qry=implode(' union ',$code);
	}else{
		$qry= "select * from (select a.uname,a.fullname, a.id as uid, b.attr_code,b.attr_val from z_users a right join z_user_attr b on a.id=b.uid order by a.uname) c ".$sOrder;
	}
	//die($qry);
	$data = array();
	try {
		$temp = $this->_db->fetchAll($qry);
		//Zend_Debug::dump($temp);die();
		if(count($temp)>0){
			foreach($temp as $k=>$v){
				if(!isset($data[$v['uid']])){
					$data[$v['uid']]['uname'] = $v['uname'];
					$data[$v['uid']]['fullname'] = $v['fullname'];
				}
				$data[$v['uid']][$v['attr_code']] = $v['attr_val'];
			}
		}
		//Zend_Debug::dump($data);die();
		foreach($data as $k=>$v){
			$qry2 = "select * from (select a.uname, a.id as uid, b.attr_code,b.attr_val from z_users a right join z_user_attr b on a.id=b.uid where b.uid='".$k."' order by a.uname) c ".$sOrder;
			$temp2 = $this->_db->fetchAll($qry2);
			//Zend_Debug::dump($temp2);die();
			if(count($temp2)>0){
				foreach($temp2 as $k2=>$v2){
					if(!isset($data[$v2['uid']][$v2['attr_code']])){
						$data[$v2['uid']][$v2['attr_code']] = $v2['attr_val'];
					}
				}
			}
		
			foreach($types as $k2=>$v2){
				//die($v2);
				if(!isset($data[$k][$v2])){
					$data[$k][$v2] = "";
				}
			}
		}
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);
	}
	//Zend_Debug::dump($data);die();
	return $data;
}

}
