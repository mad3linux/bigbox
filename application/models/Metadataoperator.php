<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Metadataoperator extends Zend_Db_Table_Abstract{

function crawling_mail($data) {
	return $data;
}

function check_changed($data) {
	return $data;
}
function exec_script_ssh_complete_input($data) {

$connection = ssh2_connect($data['inputcustom'][0], 22);
try {
		ssh2_auth_password($connection, $data['inputcustom'][1], $data['inputcustom'][2]);
		$stream = ssh2_exec($connection, $data['inputcustom'][3]);
		//$stream = ssh2_exec($connection, 'ls -lh '.$v[4]);
		$errorStream = ssh2_fetch_stream($stream, SSH2_STREAM_STDERR);
		stream_set_blocking($errorStream, true);
		stream_set_blocking($stream, true);

		$data['output'] =  "Output: " . stream_get_contents($stream);
		 $data['error'] = "Error: " . stream_get_contents($errorStream);
		
} catch (Exception $e) {
		$data['errorsystem']= $e;
	}
	return $data;
}


function exec_script_ssh($data) {

//Zend_Debug::dump($data); die();	
$connection = ssh2_connect($data['ip'], 22);
try {
		ssh2_auth_password($connection, $data['user'], $data['pwd']);
		$stream = ssh2_exec($connection, $data['inputcustom'][0]);
		//$stream = ssh2_exec($connection, 'ls -lh '.$v[4]);
		$errorStream = ssh2_fetch_stream($stream, SSH2_STREAM_STDERR);
		stream_set_blocking($errorStream, true);
		stream_set_blocking($stream, true);

		$data['output'] =  "Output: " . stream_get_contents($stream);
		 $data['error'] = "Error: " . stream_get_contents($errorStream);
		
} catch (Exception $e) {
		$data['errorsystem']= $e;
	}
	return $data;
}

function initial() {


return array(
'ip'=>'10.62.29.24',
'user'=>'hdfs',
'pwd'=>'cdh4dm1n'

);
	
}


function httpPost($url,$params)
{
	$postData = '';
   //create name value pairs seperated by &
   foreach($params as $k => $v) 
   { 
	  $postData .= $k . '='.$v.'&'; 
   }
   rtrim($postData, '&');
 
    $ch = curl_init();  
 
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch,CURLOPT_HEADER, false); 
    curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    
 
    $output=curl_exec($ch);
 
    curl_close($ch);
    return $output;
 
}


function get_detik() {
ini_set('max_execution_time', 300);
	$params = array(
	"datepick" => "08/04/2015",
	);
 
	//echo $this->httpPost('http://news.detik.com/indeks', $params); 
	
	$config = new Zend_Config_Ini( APPLICATION_PATH . '/configs/application.ini' , 'production');
	$config = $config->toArray();
	$config = array(
			'endpoint' => array(
			'localhost' => array(
			'host' => $config['data']['solr']['host'],
			'port' => $config['data']['solr']['port'],
			'path' => $config['data']['solr']['path'],
			)
		)
	);	
	libxml_use_internal_errors(true);
	$html = file_get_contents('http://news.detik.com/indeks');
	//$html = $this->httpPost('http://news.detik.com/indeks', $params);	
		try {
			$c2 = new CMS_LIB_parse();
			$dom = new Zend_Dom_Query($html);
			$results = $dom->query('#indeks-container a');
			$tdate = $dom->query('#indeks-container .labdate');
			$sub_judul = $dom->query('#indeks-container .sub_judul');
			$i=1;
			$content="";
			foreach ($results as $result) {
                          if($i>150 && $i<=200 ) {
                             $content = file_get_contents('http:'.$result->getAttribute('href'));
                             $content = $c2->remove($content,"<!--", "-->");
                             $content = $c2->remove($content,"<style", "</style>");
                             $content = $c2->remove($content,"<script", "</script>");
                             $content = str_replace("  ", "", $content);
                             $content = preg_replace('/\s+/', ' ', $content);
                             $dom_1 = new Zend_Dom_Query($content);
                             $results_1 = $dom_1->query('.detail_text');
                             $author_1 = $dom_1->query('.jdl .author');
                             $topic_1 = $dom_1->query('.jdl h2');
                             $data[$i]['link']= 'http:'.$result->getAttribute('href');
                             $data[$i]['text']= $result->nodeValue;
                             $data[$i]['tdate']= trim($tdate->current()->textContent);
                             $data[$i]['content']= trim($results_1->current()->textContent);
                             $data[$i]['author']= trim($author_1->current()->textContent);
                             $data[$i]['topic']= trim($topic_1->current()->textContent);
                             $data[$i]['path']= $result->getAttribute('href');
                             $tdate->next();

				}
				$i++;
			}
			
				//Zend_Debug::dump($data); die();
				$client = new Solarium\Client($config);	
				//$client = new Solarium_Client($config);
				$update = $client->createUpdate();
				
				$i=1;
				foreach ($data as $v) {
					//$time = explode(" ", )
					$var=array();
					$var = explode(" ", $v['tdate']);
					$date = date("Y-m-d", strtotime($var[1]." ".$var[2]." ".$var[3])); 
					$datetime= $date."T".$var[4].":00Z";
					$doc = $update->createDocument();
					$doc->id = $v['link'];
					$doc->ts_author= $v['author'];
					$doc->content = $v['content'];
					$doc->taxonomy_names = $v['topic'];
					$doc->label = $v['text'];
					$doc->bundle = "news";
					$doc->bundle_name = "detik online";
					$doc->site = "http://detik.com";
					$doc->url = $v['link'];
					$doc->path = $v['path'];
					$doc->content_date = $datetime;
					if($v['topic']!="") {
						$doc->taxonomy_names = $v['topic'];
					}
					$foradd[]=$doc;
				}
				//Zend_Debug::dump($foradd);
				
				$update->addDocuments($foradd);
				$update->addCommit();
				$result = $client->update($update);
				Zend_Debug::dump($result->getStatus());
				die();
					
		} catch (Exception $e) {
			Zend_Debug::dump($e); die("error");
		}
		//Zend_Debug::dump($data); die();

		die("ok");
	
}

  /**
   * Attempt to exchange a code for an valid authentication token.
   * If $crossClient is set to true, the request body will not include
   * the request_uri argument
   * Helper wrapped around the OAuth 2.0 implementation.
   *
   * @param $code string code from accounts.google.com
   * @param $crossClient boolean, whether this is a cross-client authentication
   * @return string token
   */
	
function crawling_web($data=array())
{
	$SEED_URL = $data['url'];
	$MAX_PENETRATION = $data['level'];
	$FETCH_DELAY = 1;
	$ALLOW_OFFISTE = false;
	$spider_array = array();
	$cl1 = new CMS_LIB_simplespider();
	$c = new CMS_LIB_http();
	$c2 = new CMS_LIB_parse();


	$temp_link_array = $cl1->harvest_links($SEED_URL);


	$spider_array = $cl1->archive_links($spider_array, 0, $temp_link_array);

	$spider_array= $temp_link_array;
//Zend_Debug::dump($spider_array); die();
# Spider links from remaining penetration levels
for($penetration_level=1; $penetration_level<=$MAX_PENETRATION;
$penetration_level++)
{
$previous_level = $penetration_level - 1;
for($xx=0; $xx<count($spider_array[$previous_level]); $xx++)
{
unset($temp_link_array);
$temp_link_array = $cl1->harvest_links($spider_array[$previous_level][$xx]);



	$spider_array = $cl1->archive_links($spider_array, $penetration_level,
	$temp_link_array);
}
}


$links = array_unique($spider_array);

//Zend_Debug::dump($links); die();
$new=array();

$i=1;

foreach ($links as $l) {
	
	if (strpos($l, $SEED_URL) !== false &&$SEED_URL."/" != $l ) {
    

	$vcon = $c->http($l);
	//$content =  strip_tags($vcon['FILE']);
	
	//Zend_Debug::dump($vcon); die();
	
	if($vcon['STATUS']['http_code']=='200') {
		
			$content =  ($vcon['FILE']);
			$content = $c2->remove($content,"<!--", "-->");
			$content= $c2->remove($content,"<a", "</a>");
			$content = $c2->remove($content,"<style", "</style>");
			$content = $c2->remove($content,"<script", "</script>");
			$content = $c2->remove($content,"<img", " >");
			$content = strip_tags($content);
			$content = str_replace("  ", "", $content);
			$content = preg_replace('/\s+/', ' ', $content);
			//Zend_Debug::dump($content); die();
			
			$new[$i]['adapter_name'] = $SEED_URL;
			$new[$i]['address'] = $l;
			$new[$i]['api_id'] = "";
			$new[$i]['content'] = $content;
			$new[$i]['content_raw'] = $vcon['FILE'];
			$new[$i]['content_serialize'] = serialize($vcon['STATUS']);
			$new[$i]['hash'] = $l;
			$new[$i]['id'] = "";
			$new[$i]['item'] = "";
			$new[$i]['machine_id'] = $vcon['STATUS']['primary_ip'];
			$new[$i]['object_name'] = $l;
			$new[$i]['object_type'] = 'htmlcontent';
			$new[$i]['path'] = $l;
			$new[$i]['path_alias'] = $l;
			$new[$i]['schema_id'] = "";
			$new[$i]['source_type'] = "crawling";
			$new[$i]['taxonomy_names'] = "";
			$new[$i]['spell'] = "";
			$new[$i]['taxonomy_names'] = "";
			$new[$i]['teaser'] = "";
			$new[$i]['tid'] = "";
			$new[$i]['title'] = $l;
			$new[$i]['url'] = $l;
		}
	}
	

$i++;
}
//Zend_Debug::dump($new);die();

$id = $this->builder($new, $data['rebuild']);
 //Zend_Debug::dump($id); die();
return array('transaction'=>true, 'message'=>'has indexed '.$id);
}	



/*
 * 
 * name: unknown
 * @param
 * @return 
 * 
 */

function get_delta($var)
{
 
}	


function grab_dump($var)
{
    ob_start();
    var_dump($var);
    return ob_get_clean();
}
public function indexing_conn_by_id($input) {
	
	//Zend_Debug::dump($input); die();
	
	$dd = new Model_Zprabaexec();
	
	$con = $input['inputcustom'][0];
	
	$id= $dd->get_metadata_conn($con);
	
	
	return array('transaction'=>true, 'message'=>'has indexed '.$id);
	}
	
	
public function builder($data, $rebuild=false){
			//	Zend_Debug::dump($data); die();
			
				$path = (APPLICATION_PATH . '/metadata');
				//echo $path ; die();
				if($rebuild) {
				// create the index
					$files = glob($path.'/*'); 
					foreach($files as $file){ 
						if(is_file($file))
						unlink($file); 
					}
					$files = glob($path.'/{,.}*', GLOB_BRACE);
					foreach($files as $file){ 
					if(is_file($file))
						unlink($file); 

					}
					$index = Zend_Search_Lucene::create($path);

				} else {
					try{
					$index = Zend_Search_Lucene::open($path);
					} catch (Exception $e) {
						Zend_Debug::dump($e); die();	
					}
				
				}
				
				foreach($data as $row) 
				{
					
					$doc = new  Zend_Search_Lucene_Document(); 
					$doc = Zend_Search_Lucene_Document_Html::loadHTML($htmlString);
					$doc->addField(Zend_Search_Lucene_Field::UnIndexed('id', $row['id'])); 
				
					$doc->addField(Zend_Search_Lucene_Field::Text('adapter_name',$row['adapter_name'])); 
					$doc->addField(Zend_Search_Lucene_Field::Text('address', $row['address']));
				
					$doc->addField(Zend_Search_Lucene_Field::Text('api_id',$row['api_id'])); 
					$doc->addField(Zend_Search_Lucene_Field::Text('content', $row['content']));
					$doc->addField(Zend_Search_Lucene_Field::Text('content_raw', $row['content_raw']));
					
					$doc->addField(Zend_Search_Lucene_Field::UnIndexed('content_serialize',$row['content_serialize'])); 
					$doc->addField(Zend_Search_Lucene_Field::Text('hash', $row['hash']));
				
					$doc->addField(Zend_Search_Lucene_Field::Text('id',$row['id'])); 
					$doc->addField(Zend_Search_Lucene_Field::Text('item', $row['item']));
				
					$doc->addField(Zend_Search_Lucene_Field::Text('machine_id',$row['machine_id'])); 
					$doc->addField(Zend_Search_Lucene_Field::Text('object_name', $row['object_name']));
				
					$doc->addField(Zend_Search_Lucene_Field::Text('object_type',$row['object_type'])); 
					$doc->addField(Zend_Search_Lucene_Field::Text('path', $row['path']));
				
					$doc->addField(Zend_Search_Lucene_Field::Text('path_alias',$row['path_alias'])); 
					
					$doc->addField(Zend_Search_Lucene_Field::Text('schema_id',$row['schema_id'])); 
					$doc->addField(Zend_Search_Lucene_Field::Text('source_type', $row['source_type']));
				
				
					$index->addDocument($doc);
								
						
				} 
					
				
				$index->optimize();
				//	$this->view->count= $index->numDocs();
				return $index->numDocs(); 
	}
	
	
public function indexing_prabac_api($input) {
	
		$dd = new Model_Zprabaexec();
		$con = $input['inputcustom'][0];
		$id= $dd->get_metadata_conn($con);
		return array('transaction'=>true, 'message'=>'has indexed '.$id);
	}
	
	public function indexing_link($input) {
		//$username = 'ADMIN';
		//$password = 'admin';
		//$url = 'http://speed.telkom.co.id/';

	
		//curl_setopt($ch, CURLOPT_URL, 'http://speed.telkom.co.id/pm/ogpreport/reportogpdatin/main/PROVISIONING');
	
		$username = $input['username'];
		$password = $input['password'];
		$rebuild = $input['rebuild'];
		$url = $input['dns'];
		$menuurl = $input['url'];
		
		$prul = parse_url($menuurl);
		$al = str_replace("/", " ", $prul['path']);
		//echo $al; die();
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'USERNAME='.$username.'&PASSWORD='.$password);
		curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$store = curl_exec($ch);
		curl_setopt($ch, CURLOPT_URL, $menuurl);
		//execute the request
		$content = curl_exec($ch);
	
		//$text = strip_tags($content);
		//$content = preg_replace("/&#?[a-z0-9]{2,8};/i","",$text );
		$dom = new DOMDocument();
		$dom->preserveWhiteSpace = false; 
		$dom->formatOutput       = false;

		$html = $dom->loadHTML($content);
		$tables = $dom->getElementsByTagName('table'); 
		
		$rows = $tables->item(0)->getElementsByTagName('tr'); 
		
		foreach ($rows as $row) 
		{ 
	      $cols = $row->getElementsByTagName('td'); 

	      $val[] = array('no'=>$cols->item(0)->nodeValue,
	      				'order_type'=>$cols->item(1)->nodeValue,
	      				'service_type'=>$cols->item(2)->nodeValue,
	      				'sero_date'=>$cols->item(3)->nodeValue,
	      				'sero_status_date'=>$cols->item(4)->nodeValue,
	      				'customer'=>$cols->item(5)->nodeValue,
	      				'so_number'=>$cols->item(6)->nodeValue,
	      				'ticares_id'=>$cols->item(7)->nodeValue,
	      				'service_id'=>$cols->item(8)->nodeValue,
	      				'divre'=>$cols->item(9)->nodeValue,
	      				'witel'=>$cols->item(10)->nodeValue,
	      				'sto_code'=>$cols->item(11)->nodeValue,
	      				'sto_name'=>$cols->item(12)->nodeValue,
	      				'speed'=>$cols->item(13)->nodeValue,
	      				'lama_order'=>$cols->item(14)->nodeValue,
	      				'task_terlama'=>$cols->item(15)->nodeValue,
	      				);

	    } 

		
		$var = array(
					'adapter_name'=>$url,
					'address'=>$menuurl,
					'api_id'=>'',
					'content'=>"",
					'content_serialize'=>serialize($val),
					'hash'=>'',
					'id'=>'',
					'item'=>$al,
					'machine_id'=>$url,
					'object_name'=>$menuurl,
					'object_type'=>'link',
					'path'=>'',
					'path_alias'=>'',
					'schema_id'=>'',
					'source_type'=>'application',
					'spell'=>'',
					'taxonomy_names'=>'',
					'teaser'=>'',
					'tid'=>'',
					'timestamp'=>'',
					'title'=>$menuurl,
					'url'=>''
					);
				
		
		
	$id=	$this->builder(array($var), $rebuild);
	
	
	
	return array('transaction'=>true, 'message'=>'has indexed '.$id);
	}
		
		
	
	
}
