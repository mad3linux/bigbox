<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Metax extends Zend_Db_Table_Abstract {
		public function get_adapters ($con) 
	{
		
		try {
			$rows  = $this->_db->fetchRow("select * from zpraba_conn where id=?", $con);
			//Zend_Debug::dump($rows); die();
		} catch (Exception $e) {
			Zend_Debug::dump($e); die();
		} 
		
		return $rows;

	}
	
	
	
	public function get_kolom ($data) 
	{	
		try { 
			$adapters = $this->get_adapters($data[0]);
			$c = new Model_Zprabametadata($data[0]);
			$new = $c->get_tables();
			//Zend_Debug::dump($data); die();
			
			//Zend_Debug::dump($data); die();
		} catch (Exception $e) {
				Zend_Debug::dump($e); die();
			}
		
		return $new[$data[1]];
	
	}
		public function get_table ($con) 
	{	
		try { 
			$adapters = $this->get_adapters($con);
			$c = new Model_Zprabametadata($con);
			$data = $c->get_tables();
			//Zend_Debug::dump($data); die();
			$i=0;
			foreach ($data as $k=>$v) {
				 $new[$con][$i]['name']= $k;
				 $new[$con][$i]['type']= 'item';
				 $new[$con][$i]['id']=$con."~".$k;
			 
				$i++;
			}
			
			//Zend_Debug::dump($data); die();
		} catch (Exception $e) {
				Zend_Debug::dump($e); die();
			}
		
		return $new;
	
	}
		
		
		public function get_conns_serialize() {
			$sql = "select * from zpraba_conn order by 1";
			try {
				$item = $this->_db->fetchAll ( $sql );
				foreach ( $item as $k => $v ) {
					$data [$k] = $v;
					$data [$k] ['conn'] = unserialize ( $v ['conn_params'] );
				}
				
				
			} catch ( Exception $e ) {
				Zend_Debug::dump ( $e->getMessage () );
				die ( $sql );
			}
			
		//Zend_Debug::dump($data); die
		$new =array();	
		$i=0;
		foreach ($data as $v) {
			$new[$i]['name'] = 	$v['conn_name'];
			$new[$i]['id'] = 	$v['id'];
			$new[$i]['type'] = 	'folder';
			//$new[$v['id']]['ctype'] = 	'conn';
			$new[$i]['sort'] = 	$v['id'];
		$i++;
		}
		
		
		return $new;
	
	}
	
}
