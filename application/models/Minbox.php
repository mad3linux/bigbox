<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Minbox extends Zend_Db_Table_Abstract {


public function get_for_paging($data) {
		$conn  = new Model_Zpraba4api();
		$conn->conn_by_id($id);
		
		$aColumns = array (
				'no',
				'id',
				'name_alias',
				'layout_id',
				'workspace_id',
				'app_id',
				'title' 
		);
		
		$sLimit = "";
		if (isset ( $data ['iDisplayStart'] ) && $data ['iDisplayLength'] != '-1') {
			$sLimit = "LIMIT " . intval ( $data ['iDisplayStart'] ) . ", " . intval ( $data ['iDisplayLength'] );
		}
		
		/*
		 * Ordering
		 */
		// Zend_Debug::dump($data); die();
		
		if (isset ( $data ['iSortCol_0'] ) && intval ( $data ['iSortCol_0'] ) > 0) {
			$sOrder = "ORDER BY  ";
			for($i = 1; $i < intval ( $data ['iSortingCols'] ); $i ++ )
			//for ( $i=1 ; $i<count($aColumns) ; $i++ )
			{

				if ($data ['bSortable_' . intval ( $data ['iSortCol_' . $i] )] == "true") {
					$sOrder .= "`" . $aColumns [intval ( $data ['iSortCol_' . $i] )] . "` " . ($data ['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
				}
			}
			
			$sOrder .= "`" . $aColumns [intval ( $data ['iSortCol_0'] )] . "` " . ($data ['sSortDir_0'] === 'asc' ? 'asc' : 'desc') . ", ";
			// echo $sOrder.'<br>';
			
			$sOrder = substr_replace ( $sOrder, "", - 2 );
			if ($sOrder == "ORDER BY") {
				$sOrder = "";
			}
		}
		
		/*
		 * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
		 */
		// $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
		$sWhere = '';
		if (isset ( $data ['sSearch'] ) && $data ['sSearch'] != "") {
			$sWhere .= "WHERE (";
			
			for($i = 1; $i < count ( $aColumns ); $i ++) {
				$sWhere .= "`" . $aColumns [$i] . "` LIKE '%" . $data ['sSearch'] . "%' OR ";
			}
			// die($sWhere);
			$sWhere = substr_replace ( $sWhere, "", - 3 );
			$sWhere .= ')';
		}
		
		// die($sWhere);
		/* Individual column filtering */
		for($i = 1; $i < count ( $aColumns ); $i ++) {
			if (isset ( $data ['bSearchable_' . $i] ) && $data ['bSearchable_' . $i] == "true" && $data ['sSearch_' . $i] != '') {
				if ($sWhere == "") {
					$sWhere = "WHERE ";
				} else {
					$sWhere .= " AND ";
				}
				$sWhere .= "`" . $aColumns [$i] . "` LIKE '%" . $data ['sSearch_' . $i] . "%' ";
			}
		}
		
		$qry = "SELECT * FROM 
					`zpraba_page` " . $sWhere . " " . $sOrder . " " . $sLimit;
		// echo $qry; die();
		try {
			$data = $this->_db->fetchAll ( $qry );
			// Zend_Debug::dump($data);die();
			return $data;
		} catch ( Exception $e ) {
			Zend_Debug::dump ( $e->getMessage () );
			die ( $q );
		}
		
		
		$iTotalRecords = intval ( $list['count'] );
		$iDisplayLength = intval ( $params ['iDisplayLength'] );
		
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart = intval ( $params ['iDisplayStart'] );
		$sEcho = intval ( $params ['sEcho'] );
		$records = array ();
		$records ["aaData"] = array ();
		
		$end = $iDisplayStart + $iDisplayLength;
		
		
		
		
		
		foreach ( $list['data'] as $k => $v ) {
			$head = array_keys($v);
			$records ["aaData"] [] = array (
					$iDisplayStart + $k + 1,
					$v ['id'],
					$v ['name_alias'],
					$v ['layout_id'],
					$v ['workspace_id'],
					$apps [$v ['app_id']],
					$v ['title'],
					'<div style="text-align:center"><a href="/prabaeditor/editpage/id/' . $v ['id'] . '" data-id="' . $v ['id'] . '" class="btn btn-xs green btn-edit"><i class="fa fa-search"></i> Edit Page</a>' . '<a href="/prabagen/index/page/' . $v ['id'] . '" data-id="' . $v ['id'] . '" class="btn btn-xs yellow btn-add"><i class="fa fa-search"></i> View Page</a>' . '<a href="javascript:;" data-id="' . $v ['id'] . '"  class="btn btn-xs red btn-delete"><i class="fa fa-times"></i> Delete Api</a></div>' 
			);
		}
		
		$records ["sEcho"] = $sEcho;
		$records ["iTotalRecords"] = $iTotalRecords;
		$records ["iTotalDisplayRecords"] = $iTotalRecords;
	
}

public function custom() {
	
//konseksi solr dengan solarium


$data = $this->_db->fetchAll("select * from z_users limit 0,50");
return  array('transaction'=>true, 'data'=>$data, "message"=>"success" );

	
}

public function getAlertGroupsXX($gid = null) {
	$where = "";
	
	if($gid!=null || $gid!=''){
		$where = " where a.gid='".$gid."' ";
	}
	$q = "select
            distinct(a.group_name) as uname,a.group_name as fullname, a.gid as id, b.attr_code,b.attr_val
          from
            z_groups a right join z_group_attr b on a.gid=b.gid 
			".$where."
          order by uname";
	$data = array();
	//die($q);
	try {
		$temp = $this->_db->fetchAll($q);
		//Zend_Debug::dump($temp);die();
		if(count($temp)>0){
			foreach($temp as $k=>$v){
				if(!isset($data[$v['id']])){
					$data[$v['id']]['name'] = $v['uname'];
				}
				$data[$v['id']][$v['attr_code']] = $v['attr_val'];
			}
		}
		return $data;
	} catch (Exception $e) {
	   Zend_Debug::dump($e->getMessage());die($q);		
	}
}

function get_listuserXXX($param = null) {
	//Zend_Debug::dump($param);//die('qqq');
	$types = $this->getAlertTypes();
	//Zend_Debug::dump($types);die('qqq');
	$data = 0;
	$aColumns = array( 'act','no','a.uname','a.fullname' );
	$aColumns = array_merge($aColumns, $types);
	//Zend_Debug::dump($aColumns);die('qqq');
	$bypass = array(0,1);
	
	/*
	* Ordering
	*/
	$sOrder = "";
	if ( isset( $_GET['iSortCol_0'] ) ){
		if( (int)$_GET['iSortCol_0']>3){
			$orderby = "attr_code";
		}else{
			$orderby = $aColumns[$_GET['iSortCol_0']];
		}
		$sOrder = "ORDER BY  ";
		if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_0']) ] == "true" ){
			$sOrder .= $orderby." ".($_GET['sSortDir_0']==='asc' ? 'asc' : 'desc') .", ";
		}

		$sOrder = substr_replace( $sOrder, "", -2 );
		if ( $sOrder == "ORDER BY" ){
			$sOrder = "";
		}
	}
	
	$searchLike = array();

	/* 
	* Filtering
	* NOTE this does not match the built-in DataTables filtering which does it
	* word by word on any field. It's possible to do here, but concerned about efficiency
	* on very large tables, and MySQL's regex functionality is very limited
	*/
	$sWhere = "";

	if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
		$searchLike[2] = $_GET['sSearch'];
		$searchLike[3] = $_GET['sSearch'];
		foreach($aColumns as $k=>$v){
			if($k>3){
				$searchLike[$k] = $_GET['sSearch'];
			}
		}
	}
	
	for ( $i=0 ; $i<count($aColumns) ; $i++ ){
		if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
			$searchLike[$i] = $_GET['sSearch_'.$i];
		}
	}
	
	$code = array();
	$sql = "select * from (select a.uname,a.fullname, a.id as uid, b.attr_code,b.attr_val from z_users a right join z_user_attr b on a.id=b.uid";
	foreach($searchLike as $k=>$v){
		if($k>3){
			$code[] = $sql." where attr_code='".$aColumns[$k]."' and attr_val LIKE '%".$v."%' ".$sOrder.") c";
		}else{
			$code[] = $sql." where ".$aColumns[$k]." LIKE '%".$v."%' ".$sOrder.") c";
		}
	}
	//Zend_Debug::dump($code);//die('qqq');
	//Zend_Debug::dump(implode(' union ',$code));die('qqq');
	
	if(count($code)>0){
		$qry=implode(' union ',$code);
	}else{
		$qry= "select * from (select a.uname,a.fullname, a.id as uid, b.attr_code,b.attr_val from z_users a right join z_user_attr b on a.id=b.uid order by a.uname) c ".$sOrder;
	}
	//die($qry);
	$data = array();
	try {
		$temp = $this->_db->fetchAll($qry);
		//Zend_Debug::dump($temp);die();
		if(count($temp)>0){
			foreach($temp as $k=>$v){
				if(!isset($data[$v['uid']])){
					$data[$v['uid']]['uname'] = $v['uname'];
					$data[$v['uid']]['fullname'] = $v['fullname'];
				}
				$data[$v['uid']][$v['attr_code']] = $v['attr_val'];
			}
		}
		//Zend_Debug::dump($data);die();
		foreach($data as $k=>$v){
			$qry2 = "select * from (select a.uname, a.id as uid, b.attr_code,b.attr_val from z_users a right join z_user_attr b on a.id=b.uid where b.uid='".$k."' order by a.uname) c ".$sOrder;
			$temp2 = $this->_db->fetchAll($qry2);
			//Zend_Debug::dump($temp2);die();
			if(count($temp2)>0){
				foreach($temp2 as $k2=>$v2){
					if(!isset($data[$v2['uid']][$v2['attr_code']])){
						$data[$v2['uid']][$v2['attr_code']] = $v2['attr_val'];
					}
				}
			}
		
			foreach($types as $k2=>$v2){
				//die($v2);
				if(!isset($data[$k][$v2])){
					$data[$k][$v2] = "";
				}
			}
		}
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);
	}
	//Zend_Debug::dump($data);die();
	return $data;
}


public function getInbox($type) {
	$authAdapter = Zend_Auth::getInstance ();
	$identity = $authAdapter->getIdentity ();
	
	$where = "";
	
	if($type!=null || $type!=''){
		$where = " and a.inbox_type='".$type."' ";
	}
	$qry = "select a.*, b.fullname FROM zpraba_inbox a inner join z_users b on a.uid_from =  b.id where uid_in =? order by incoming_date desc ";

	$data = array();
	
	try {
		$data = $this->_db->fetchAll($qry, $identity->uid);
		return $data;
	} catch (Exception $e) {
	   Zend_Debug::dump($e->getMessage());die($qry);		
	}
}


public function getMessage($params) {
	$where = "";
	if($gid!=null || $gid!=''){
		$where = " where a.gid='".$gid."' ";
	}

	$id=$params['id'];

	$qry = "select a.*, b.fullname FROM zpraba_inbox a inner join z_users b on a.uid_from =  b.id  where a.id=$id ";

	$data = array();
#	die($qry);
	try {
		$data = $this->_db->fetchRow($qry);
		//Zend_Debug::dump($temp);die();

		return $data;
	} catch (Exception $e) {
	   Zend_Debug::dump($e->getMessage());die($qry);		
	}
}

public function notreadinbox($type) {
	$authAdapter = Zend_Auth::getInstance ();
	$identity = $authAdapter->getIdentity ();
	
	$where = "";
	
	if($type!=null || $type!=''){
		$where = " and a.inbox_type='".$type."' ";
	}
	$qry = "select a.*, b.fullname FROM zpraba_inbox a inner join z_users b on a.uid_from =  b.id where uid_in =? ".$where." and a.status=0 order by incoming_date desc ";
	//echo ($identity->uid);
	$data = array();
	
	try {
		$data = $this->_db->fetchAll($qry, $identity->uid);
		return $data;
	} catch (Exception $e) {
	   Zend_Debug::dump($e->getMessage());die($qry);		
	}
}

public function countnotreadinbox($type) {
	
	$authAdapter = Zend_Auth::getInstance ();
	$identity = $authAdapter->getIdentity ();
	
	$where = "";
	
	if($type!=null || $type!=''){
		//$where = " and a.inbox_type='".$type."' ";
	}
	$qry = "select a.*, b.fullname FROM zpraba_inbox a inner join z_users b on a.uid_from =  b.id where uid_in =? ".$where." and status=0 order by incoming_date desc ";
	die($qry);
	$data = array();
	
	try {
		$data = $this->_db->fetchAll($qry, $identity->uid);
		return $data;
	} catch (Exception $e) {
	   Zend_Debug::dump($e->getMessage());die($qry);		
	}
}
public function countUnreadMsg($params) {

#	Zend_Debug::dump($params);die();

	$where = "";
	
	if($gid!=null || $gid!=''){
		$where = " where a.gid='".$gid."' ";
	}


	$id=$params['id'];

	$qry = "SELECT count(*) FROM zpraba_inbox  where 1=1 ";

	$data = array();
#	die($qry);
	try {
		$data = $this->_db->fetchOne($qry);
		//Zend_Debug::dump($temp);die();

		return $data;
	} catch (Exception $e) {
	   Zend_Debug::dump($e->getMessage());die($qry);		
	}
}


public function getPrahu() {
	$where = "";
	

	$qry = "SELECT * FROM zpraba_prahu ";

	$data = array();
#	die($qry);
	try {
		$data = $this->_db->fetchAll($qry);
		//Zend_Debug::dump($temp);die();

		return $data;
	} catch (Exception $e) {
	   Zend_Debug::dump($e->getMessage());die($qry);		
	}
}

function insertPrahu($data){
// Zend_Debug::dump($data);//die('sss');

		$id=$data['id'];

	if ($id){
		$sql= "UPDATE zpraba_prahu SET c_number='".$data['c_number']."', t_chanel='".$data['t_chanel']."',
					p_type='".$data['p_type']."', url_api='".$data['url_api']."', is_active=".$data['is_active']." 
				WHERE id=$id ";
	}else{
		$sql = "INSERT INTO zpraba_prahu (c_number, t_chanel, p_type, url_api, is_active)
				VALUES ('".$data['c_number']."','".$data['t_chanel']."','".$data['p_type']."','".$data['url_api']."',".$data['is_active'].")";

	}


# die($sql);
	try {
	       $data = $this->_db->query($sql);
					//Zend_Debug::dump($dataitem);die();
	            
	        } catch (Exception $e) {
	            return false;
	        }
					
			//Zend_Debug::dump($dataitem);die();	
			return $data;
	}

function deletePrahu($id){
// Zend_Debug::dump($data);//die('sss');


		$sql = "DELETE FROM zpraba_prahu WHERE id=$id ";


# die($sql);
	try {
	       $data = $this->_db->query($sql);
			//Zend_Debug::dump($dataitem);die();
	            
	        } catch (Exception $e) {
	            return false;
	        }
					
			//Zend_Debug::dump($dataitem);die();	
			return $data;
	}

public function dataPrahu($id) {
	$where = "";
	
	$qry = "SELECT * FROM zpraba_prahu where id=$id ";

	$data = array();
#	die($qry);
	try {
		$data = $this->_db->fetchRow($qry);
		//Zend_Debug::dump($temp);die();

		return $data;
	} catch (Exception $e) {
	   Zend_Debug::dump($e->getMessage());die($qry);		
	}
}


}
