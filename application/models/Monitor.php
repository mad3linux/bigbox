<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Monitor extends Zend_Db_Table_Abstract{
function __construct() {
	
$params = array('username' => 'escort', 'password' => 'escort#135', 'host' => '10.62.8.130', 'dbname' => 'escort');
$params2 = array(
	'dbname'=>'(DESCRIPTION= (ADDRESS= (PROTOCOL=TCP) (HOST=10.62.185.59) (PORT=1521) ) (CONNECT_DATA= (SERVER=default) (SID=dwhmart) ) )',
	'username'=>'mart_wifi',
	'password'=>'Martwifi_01',
	'port'=>'1521',
	'host'=>'10.62.185.59'
);

try {
       
	$this->_db = Zend_Db::factory('Mysqli', $params);
	$this->_db->getConnection();
	$this->_db2 = Zend_Db::factory('Oracle', $params2);
	$this->_db2->getConnection();
} catch (Zend_Db_Adapter_Exception $e) {
	echo $e;
} catch (Zend_Exception $e) {
	echo $e;
}
}
public function get_rekap() {
$q = "select cust_category,cust_serv_abbr,status,count(1) as total
from
(
select a.serv_id,a.cust_serv_abbr,
a.cust_name cust_category,a.cust_name,a.wil_id,a.witel,
case when status = 3 then 'up' else
case when status = 2 then 'down' end end status, b.reg, null update_date, null pic, null phone
from v_mart_cust_service a, (select distinct reg,wil_id from p_sto_all) b
where user_cat='EVENT'
and cust_name is not null
and witel is not null
and status in (2,3)
and cust_serv_abbr is not null
and a.wil_id=b.wil_id 
) a group by cust_category,cust_serv_abbr,status";

try {
	$data = $this->_db->fetchAll($q);
	return $data;
} catch (Exception $e) {
   Zend_Debug::dump($e->getMessage());die($q);		
}	
}

public function get_rajaampat() {
$q = "select cust_category,cust_serv_abbr,
case when sum(status_up)=sum(total) then 'UP' else
case when sum(status_up)/sum(total) >= 0.7 then 'WARNING' else
case when sum(status_up)/sum(total) < 0.7 then 'DOWN' end end end status
from
( 
select cust_category,cust_serv_abbr,
sum(case when status = 'up' then 1 else 0 end) status_up,
sum(case when status = 'down' then 1 else 0 end) status_down,
count(1) total
from
( 
select a.serv_id,a.cust_serv_abbr,
a.cust_name cust_category,a.cust_name,a.wil_id,a.witel,
case when status = 3 then 'up' else
case when status = 2 then 'down' end end status, b.reg, null update_date, null pic, null phone
from v_mart_cust_service a, (select distinct reg,wil_id from p_sto_all) b
where user_cat='EVENT'
and cust_name is not null
and witel is not null
and status in (2,3)
and cust_serv_abbr is not null
and a.wil_id=b.wil_id
) a group by cust_category,cust_serv_abbr,status
)y
group by cust_category,cust_serv_abbr";

try {
	$data = $this->_db->fetchAll($q);
	return $data;
} catch (Exception $e) {
   Zend_Debug::dump($e->getMessage());die($q);		
}	
}

public function get_unaoc(){

$q = "SELECT cust_category,cust_serv_abbr,
CASE WHEN SUM(status_up)=SUM(total) THEN 'UP' ELSE
CASE WHEN SUM(status_up)/SUM(total) >= 0.7 THEN 'WARNING' ELSE
CASE WHEN SUM(status_up)/SUM(total) < 0.7 THEN 'DOWN' END END END STATUS
FROM
(
SELECT cust_category,cust_serv_abbr,
SUM(CASE WHEN STATUS = 'up' THEN 1 ELSE 0 END) status_up,
SUM(CASE WHEN STATUS = 'down' THEN 1 ELSE 0 END) status_down,
COUNT(1) total
FROM
(
SELECT a.serv_id,a.cust_serv_abbr,
a.cust_name cust_category,a.cust_name,a.wil_id,a.witel,
CASE WHEN STATUS = 3 THEN 'up' ELSE
CASE WHEN STATUS = 2 THEN 'down' END END STATUS, b.reg, NULL update_date, NULL pic, NULL phone
FROM v_mart_cust_service a, (SELECT DISTINCT reg,wil_id FROM p_sto_all) b
WHERE user_cat='EVENT_UNAOC_2014'
AND cust_name IS NOT NULL
AND witel IS NOT NULL
AND STATUS IN (2,3)
AND cust_serv_abbr IS NOT NULL
AND a.wil_id=b.wil_id
UNION
SELECT a.sero_serv_id serv_id,a.sero_sert_abbreviation cust_serv_abbr,
a.cust_name cust_category,a.cust_name,a.wil_id,a.witel,
CASE WHEN current_state = 0 THEN 'up' ELSE
CASE WHEN current_state = 1 THEN 'down' END END STATUS, b.reg, NULL update_date, NULL pic, NULL phone
FROM cust_service_devices a, (SELECT DISTINCT reg,wil_id FROM p_sto_all) b, cacti_bali c
WHERE user_cat='EVENT_UNAOC_2014'
AND cust_name IS NOT NULL
AND witel IS NOT NULL
AND current_state IN (0,1)
AND sero_sert_abbreviation IS NOT NULL
AND a.wil_id=b.wil_id
AND a.ip_address=c.address
) a GROUP BY cust_category,cust_serv_abbr,STATUS
)Y
GROUP BY cust_category,cust_serv_abbr";

try {
	$data = $this->_db->fetchAll($q);
	return $data;
} catch (Exception $e) {
   Zend_Debug::dump($e->getMessage());die($q);		
}	

}

public function get_sumunaoc(){

$q = "SELECT cust_serv_abbr, a.cust_name,COUNT(CASE WHEN STATUS = 3 THEN 'up' END) UP,COUNT(CASE WHEN STATUS = 2 THEN 'down' END) DOWN, COUNT(STATUS) TOTAL
FROM v_mart_cust_service a, (SELECT DISTINCT reg,wil_id FROM p_sto_all) b
WHERE user_cat='EVENT_UNAOC_2014'
AND cust_name IS NOT NULL
AND witel IS NOT NULL
AND STATUS IN (2,3)
AND cust_serv_abbr IS NOT NULL
AND a.wil_id=b.wil_id
GROUP BY cust_name,STATUS
UNION
SELECT sero_sert_abbreviation AS cust_serv_abbr, a.cust_name,COUNT(CASE WHEN current_state = 0 THEN 'up' END) UP,COUNT(CASE WHEN current_state = 1 THEN 'down' END) DOWN, COUNT(current_state) TOTAL
FROM cust_service_devices a, (SELECT DISTINCT reg,wil_id FROM p_sto_all) b, cacti_bali c
WHERE user_cat='EVENT_UNAOC_2014'
AND cust_name IS NOT NULL
AND witel IS NOT NULL
AND current_state IN (0,1)
AND sero_sert_abbreviation IS NOT NULL
AND a.wil_id=b.wil_id
AND a.ip_address=c.address
GROUP BY cust_name,current_state";

try {
	$data = $this->_db->fetchAll($q);
	return $data;
} catch (Exception $e) {
   Zend_Debug::dump($e->getMessage());die($q);		
}	

}

public function get_unaocdetail(){

$q = "SELECT a.serv_id,a.cust_serv_abbr,a.port,
a.cust_name cust_category,a.cust_name,a.wil_id,a.witel,
CASE WHEN STATUS = 3 THEN 'up' ELSE
CASE WHEN STATUS = 2 THEN 'down' END END STATUS, b.reg, NULL update_date, '720294 / I MADE ARYA SUDIARTANA' pic, '085237019898' phone
FROM v_mart_cust_service a, (SELECT DISTINCT reg,wil_id FROM p_sto_all) b
WHERE user_cat='EVENT_UNAOC_2014'
AND cust_name IS NOT NULL
AND witel IS NOT NULL
AND STATUS IN (2,3)
AND cust_serv_abbr IS NOT NULL
AND a.wil_id=b.wil_id
UNION
SELECT a.sero_serv_id serv_id,a.sero_sert_abbreviation cust_serv_abbr, a.port,
a.cust_name cust_category,a.cust_name,a.wil_id,a.witel,
CASE WHEN current_state = 0 THEN 'up' ELSE
CASE WHEN current_state = 1 THEN 'down' END END STATUS, b.reg, NULL update_date, '720294 / I MADE ARYA SUDIARTANA' pic, '085237019898' phone
FROM cust_service_devices a, (SELECT DISTINCT reg,wil_id FROM p_sto_all) b, cacti_bali c
WHERE user_cat='EVENT_UNAOC_2014'
AND cust_name IS NOT NULL
AND witel IS NOT NULL
AND current_state IN (0,1)
AND sero_sert_abbreviation IS NOT NULL
AND a.wil_id=b.wil_id
AND a.ip_address=c.address
ORDER BY cust_category, serv_id";

try {
	$data = $this->_db->fetchAll($q);
	return $data;
} catch (Exception $e) {
   Zend_Debug::dump($e->getMessage());die($q);		
}	

}

public function get_surveilance(){

$sql = "SELECT cust_category,cust_serv_abbr,
CASE WHEN SUM(status_up)=SUM(total) THEN 'UP' ELSE
CASE WHEN SUM(status_up)/SUM(total) >= 0.7 THEN 'WARNING' ELSE
CASE WHEN SUM(status_up)/SUM(total) < 0.7 THEN 'DOWN' END END END STATUS
FROM
(
SELECT cust_category,cust_serv_abbr,
SUM(CASE WHEN STATUS = 'up' THEN 1 ELSE 0 END) status_up,
SUM(CASE WHEN STATUS = 'down' THEN 1 ELSE 0 END) status_down,
COUNT(1) total
FROM
(
SELECT a.serv_id,a.cust_serv_abbr,
a.cust_name cust_category,a.cust_name,a.wil_id,a.witel,
CASE WHEN STATUS = 3 THEN 'up' ELSE
CASE WHEN STATUS = 2 THEN 'down' END END STATUS, b.reg, NULL update_date, NULL pic, NULL phone
FROM v_mart_cust_service a, (SELECT DISTINCT reg,wil_id FROM p_sto_all) b
WHERE user_cat='EVENT_VICON_MP3I_2014'
AND cust_name IS NOT NULL
AND witel IS NOT NULL
AND STATUS IN (2,3)
AND cust_serv_abbr IS NOT NULL
AND a.wil_id=b.wil_id
) a GROUP BY cust_category,cust_serv_abbr,STATUS
)Y
GROUP BY cust_category,cust_serv_abbr";
//die($sql);
try {
	$data = $this->_db->fetchAll($sql);
	return $data;
} catch (Exception $e) {
   Zend_Debug::dump($e->getMessage());die($sql);		
}	

}

public function get_sumsurveilance(){

$sql = "SELECT a.cust_serv_abbr, a.cust_name,
COUNT(CASE WHEN STATUS = 3 THEN 'up' END) UP,
COUNT(CASE WHEN STATUS = 2 THEN 'down' END) DOWN, COUNT(STATUS) TOTAL
FROM v_mart_cust_service a, (SELECT DISTINCT reg,wil_id FROM p_sto_all) b
WHERE user_cat='EVENT_VICON_MP3I_2014'
AND cust_name IS NOT NULL
AND witel IS NOT NULL
AND STATUS IN (2,3)
AND cust_serv_abbr IS NOT NULL
AND a.wil_id=b.wil_id GROUP BY cust_name;";
//die($sql);
try {
	$data = $this->_db->fetchAll($sql);
	return $data;
} catch (Exception $e) {
   Zend_Debug::dump($e->getMessage());die($sql);		
}	

}

public function get_surveilancedetail(){

$sql = "SELECT a.serv_id,a.cust_serv_abbr,
a.cust_name cust_category,a.cust_name,a.wil_id,a.witel,
CASE WHEN STATUS = 3 THEN 'UP' ELSE
CASE WHEN STATUS = 2 THEN 'DOWN' END END STATUS, b.reg, NULL update_date, NULL pic, NULL phone,
a.neighbor,a.port
FROM v_mart_cust_service a, (SELECT DISTINCT reg,wil_id FROM p_sto_all) b
WHERE user_cat='EVENT_VICON_MP3I_2014'
AND cust_name IS NOT NULL
AND witel IS NOT NULL
AND STATUS IN (2,3)
AND cust_serv_abbr IS NOT NULL
AND a.wil_id=b.wil_id";
//die($sql);
try {
	$data = $this->_db->fetchAll($sql);
	return $data;
} catch (Exception $e) {
   Zend_Debug::dump($e->getMessage());die($sql);		
}	

}

public function get_sumvicon(){

$sql = "SELECT a.cust_serv_abbr, a.cust_name,
COUNT(CASE WHEN STATUS = 3 THEN 'up' END) UP,
COUNT(CASE WHEN STATUS = 2 THEN 'down' END) DOWN, COUNT(STATUS) TOTAL
FROM v_mart_cust_service a, (SELECT DISTINCT reg,wil_id FROM p_sto_all) b
WHERE user_cat='EVENT_VICON_MP3I_2014'
AND cust_name IS NOT NULL
AND witel IS NOT NULL
AND STATUS IN (2,3)
AND cust_serv_abbr IS NOT NULL
AND a.wil_id=b.wil_id GROUP BY cust_name;";
//die($sql);
try {
	$data = $this->_db->fetchAll($sql);
	return $data;
} catch (Exception $e) {
   Zend_Debug::dump($e->getMessage());die($sql);		
}	

}

public function get_detailvicon(){

$sql = "SELECT a.serv_id,a.cust_serv_abbr,
a.cust_name cust_category,a.cust_name,a.wil_id,a.witel,
CASE WHEN STATUS = 3 THEN 'UP' ELSE
CASE WHEN STATUS = 2 THEN 'DOWN' END END STATUS, b.reg, NULL update_date, NULL pic, NULL phone,
a.neighbor,a.port
FROM v_mart_cust_service a, (SELECT DISTINCT reg,wil_id FROM p_sto_all) b
WHERE user_cat='EVENT_VICON_MP3I_2014'
AND cust_name IS NOT NULL
AND witel IS NOT NULL
AND STATUS IN (2,3)
AND cust_serv_abbr IS NOT NULL
AND a.wil_id=b.wil_id";
//die($sql);
try {
	$data = $this->_db->fetchAll($sql);
	return $data;
} catch (Exception $e) {
   Zend_Debug::dump($e->getMessage());die($sql);		
}	

}

public function get_bdf(){

$q = "SELECT cust_category,cust_serv_abbr,
CASE WHEN SUM(status_up)=SUM(total) THEN 'UP' ELSE
CASE WHEN SUM(status_up)/SUM(total) >= 0.7 THEN 'WARNING' ELSE
CASE WHEN SUM(status_up)/SUM(total) < 0.7 THEN 'DOWN' END END END STATUS
FROM
(
SELECT cust_category,cust_serv_abbr,
SUM(CASE WHEN STATUS = 'up' THEN 1 ELSE 0 END) status_up,
SUM(CASE WHEN STATUS = 'down' THEN 1 ELSE 0 END) status_down,
COUNT(1) total
FROM
(
SELECT a.serv_id,a.cust_serv_abbr,
a.cust_name cust_category,a.cust_name,a.wil_id,a.witel,
CASE WHEN STATUS = 3 THEN 'up' ELSE
CASE WHEN STATUS = 2 THEN 'down' END END STATUS, b.reg, NULL update_date, NULL pic, NULL phone
FROM v_mart_cust_service a, (SELECT DISTINCT reg,wil_id FROM p_sto_all) b
WHERE user_cat='EVENT_BDF_2014'
AND cust_name IS NOT NULL
AND witel IS NOT NULL
AND STATUS IN (2,3)
AND cust_serv_abbr IS NOT NULL
AND a.wil_id=b.wil_id
UNION
SELECT a.sero_serv_id serv_id,a.sero_sert_abbreviation cust_serv_abbr,
a.cust_name cust_category,a.cust_name,a.wil_id,a.witel,
CASE WHEN current_state = 0 THEN 'up' ELSE
CASE WHEN current_state = 1 THEN 'down' END END STATUS, b.reg, NULL update_date, NULL pic, NULL phone
FROM cust_service_devices a, (SELECT DISTINCT reg,wil_id FROM p_sto_all) b, cacti_bali c
WHERE user_cat='EVENT_BDF_2014'
AND cust_name IS NOT NULL
AND witel IS NOT NULL
AND current_state IN (0,1)
AND sero_sert_abbreviation IS NOT NULL
AND a.wil_id=b.wil_id
AND a.ip_address=c.address
) a GROUP BY cust_category,cust_serv_abbr,STATUS
)Y
GROUP BY cust_category,cust_serv_abbr";

try {
	$data = $this->_db->fetchAll($q);
	return $data;
} catch (Exception $e) {
   Zend_Debug::dump($e->getMessage());die($q);		
}	

}

public function get_sumbdf(){

$q = "SELECT a.cust_serv_abbr,a.cust_name,SUM(a.up)UP, SUM(a.down) DOWN, SUM(a.total) TOTAL FROM (SELECT cust_serv_abbr, a.cust_name,COUNT(CASE WHEN STATUS = 3 THEN 'up' END) UP,COUNT(CASE WHEN STATUS = 2 THEN 'down' END) DOWN, COUNT(STATUS) TOTAL
FROM v_mart_cust_service a, (SELECT DISTINCT reg,wil_id FROM p_sto_all) b
WHERE user_cat='EVENT_BDF_2014'
AND cust_name IS NOT NULL
AND witel IS NOT NULL
AND STATUS IN (2,3)
AND cust_serv_abbr IS NOT NULL
AND a.wil_id=b.wil_id
GROUP BY cust_name,STATUS
UNION
SELECT sero_sert_abbreviation AS cust_serv_abbr, a.cust_name,COUNT(CASE WHEN current_state = 0 THEN 'up' END) UP,COUNT(CASE WHEN current_state = 1 THEN 'down' END) DOWN, COUNT(current_state) TOTAL
FROM cust_service_devices a, (SELECT DISTINCT reg,wil_id FROM p_sto_all) b, cacti_bali c
WHERE user_cat='EVENT_BDF_2014'
AND cust_name IS NOT NULL
AND witel IS NOT NULL
AND current_state IN (0,1)
AND sero_sert_abbreviation IS NOT NULL
AND a.wil_id=b.wil_id
AND a.ip_address=c.address
GROUP BY cust_name,current_state)a
GROUP BY a.cust_name, a.cust_serv_abbr";

try {
	$data = $this->_db->fetchAll($q);
	return $data;
} catch (Exception $e) {
   Zend_Debug::dump($e->getMessage());die($q);		
}	

}

public function get_bdfdetail(){

$q = "SELECT a.serv_id,a.cust_serv_abbr,a.port,
a.cust_name cust_category,a.cust_name,a.wil_id,a.witel,
CASE WHEN STATUS = 3 THEN 'up' ELSE
CASE WHEN STATUS = 2 THEN 'down' END END STATUS, b.reg, NULL update_date, null pic, null phone
FROM v_mart_cust_service a, (SELECT DISTINCT reg,wil_id FROM p_sto_all) b
WHERE user_cat='EVENT_BDF_2014'
AND cust_name IS NOT NULL
AND witel IS NOT NULL
AND STATUS IN (2,3)
AND cust_serv_abbr IS NOT NULL
AND a.wil_id=b.wil_id
UNION
SELECT a.sero_serv_id serv_id,a.sero_sert_abbreviation cust_serv_abbr, a.port,
a.cust_name cust_category,a.cust_name,a.wil_id,a.witel,
CASE WHEN current_state = 0 THEN 'up' ELSE
CASE WHEN current_state = 1 THEN 'down' END END STATUS, b.reg, NULL update_date, null pic, null phone
FROM cust_service_devices a, (SELECT DISTINCT reg,wil_id FROM p_sto_all) b, cacti_bali c
WHERE user_cat='EVENT_BDF_2014'
AND cust_name IS NOT NULL
AND witel IS NOT NULL
AND current_state IN (0,1)
AND sero_sert_abbreviation IS NOT NULL
AND a.wil_id=b.wil_id
AND a.ip_address=c.address
ORDER BY cust_category, serv_id";

try {
	$data = $this->_db->fetchAll($q);
	return $data;
} catch (Exception $e) {
   Zend_Debug::dump($e->getMessage());die($q);		
}	

}

public function get_prri1(){

$q = "SELECT cust_category,cust_serv_abbr,
CASE WHEN SUM(status_up)=SUM(total) THEN 'UP' ELSE
CASE WHEN SUM(status_up)/SUM(total) >= 0.7 THEN 'WARNING' ELSE
CASE WHEN SUM(status_up)/SUM(total) < 0.7 THEN 'DOWN' END END END STATUS
FROM
(
SELECT cust_category,cust_serv_abbr,
SUM(CASE WHEN STATUS = 'up' THEN 1 ELSE 0 END) status_up,
SUM(CASE WHEN STATUS = 'down' THEN 1 ELSE 0 END) status_down,
COUNT(1) total
FROM
(
SELECT a.serv_id,a.cust_serv_abbr,
a.kandatel cust_category,a.cust_name,a.wil_id,a.witel,
CASE WHEN STATUS = 3 THEN 'up' ELSE
CASE WHEN STATUS = 2 THEN 'down' END END STATUS, b.reg, NULL update_date, NULL pic, NULL phone
FROM v_mart_cust_service a, (SELECT DISTINCT reg,wil_id FROM p_sto_all) b
WHERE user_cat='VICON_JKW_20141020'
AND cust_name IS NOT NULL
AND witel IS NOT NULL
AND STATUS IN (2,3)
AND cust_serv_abbr IS NOT NULL
AND a.wil_id=b.wil_id
) a GROUP BY cust_category,cust_serv_abbr,STATUS
)Y
GROUP BY cust_category,cust_serv_abbr";

try {
	$data = $this->_db->fetchAll($q);
	return $data;
} catch (Exception $e) {
   Zend_Debug::dump($e->getMessage());die($q);		
}	

}

public function get_sumprri1(){

$q = "SELECT a.cust_serv_abbr, a.cust_category cust_name, SUM(status_up) up, SUM(status_down) down, SUM(total) total  FROM 
(SELECT cust_category,cust_serv_abbr,
SUM(CASE WHEN STATUS = 'up' THEN 1 ELSE 0 END) status_up,
SUM(CASE WHEN STATUS = 'down' THEN 1 ELSE 0 END) status_down,
COUNT(1) total
FROM
(
SELECT a.serv_id,a.cust_serv_abbr,
a.kandatel cust_category,a.cust_name,a.wil_id,a.witel,
CASE WHEN STATUS = 3 THEN 'up' ELSE
CASE WHEN STATUS = 2 THEN 'down' END END STATUS, b.reg, NULL update_date, NULL pic, NULL phone
FROM v_mart_cust_service a, (SELECT DISTINCT reg,wil_id FROM p_sto_all) b
WHERE user_cat='VICON_JKW_20141020'
AND cust_name IS NOT NULL
AND witel IS NOT NULL
AND STATUS IN (2,3)
AND cust_serv_abbr IS NOT NULL
AND a.wil_id=b.wil_id
) a GROUP BY cust_category,cust_serv_abbr,STATUS) a
GROUP BY cust_serv_abbr,cust_name";

try {
	$data = $this->_db->fetchAll($q);
	return $data;
} catch (Exception $e) {
   Zend_Debug::dump($e->getMessage());die($q);		
}	

}

public function get_prri1detail(){

$q = "SELECT a.serv_id,a.cust_serv_abbr,
a.kandatel cust_category,a.cust_name,a.
wil_id,a.witel,
CASE WHEN STATUS = 3 THEN 'up' ELSE
CASE WHEN STATUS = 2 THEN 'down' END END STATUS, b.reg, NULL update_date, NULL pic, NULL phone,
a.neighbor,concat(a.neighbor,' ',a.port) port
FROM v_mart_cust_service a, (SELECT DISTINCT reg,wil_id FROM p_sto_all) b
WHERE user_cat='VICON_JKW_20141020'
AND cust_name IS NOT NULL
AND witel IS NOT NULL
AND STATUS IN (2,3)
AND cust_serv_abbr IS NOT NULL
AND a.wil_id=b.wil_id";

try {
	$data = $this->_db->fetchAll($q);
	return $data;
} catch (Exception $e) {
   Zend_Debug::dump($e->getMessage());die($q);		
}	

}

public function get_aug17(){

$q = "SELECT cust_name,cust_serv_abbr,
CASE WHEN SUM(status_up)=SUM(total) THEN 'UP' ELSE
CASE WHEN SUM(status_up)/SUM(total) >= 0.7 THEN 'WARNING' ELSE
CASE WHEN SUM(status_up)/SUM(total) < 0.7 THEN 'DOWN' END END END STATUS
FROM
(
SELECT cust_name,cust_serv_abbr,
SUM(CASE WHEN STATUS = 'up' THEN 1 ELSE 0 END) status_up,
SUM(CASE WHEN STATUS = 'down' THEN 1 ELSE 0 END) status_down,
COUNT(1) total
FROM
(
SELECT a.serv_id,a.cust_serv_abbr,
a.kandatel cust_category,a.cust_name,a.wil_id,a.witel,
CASE WHEN STATUS = 3 THEN 'up' ELSE
CASE WHEN STATUS IN (1,2,4) THEN 'down' END END STATUS, b.reg, NULL update_date, NULL pic, NULL phone
FROM v_mart_cust_service a, (SELECT DISTINCT reg,wil_id FROM p_sto_all) b
WHERE user_cat='AUG'
AND cust_name IS NOT NULL
AND witel IS NOT NULL
AND STATUS IN (1,2,3,4)
AND cust_serv_abbr IS NOT NULL
AND a.wil_id=b.wil_id
) a GROUP BY cust_name,cust_serv_abbr,STATUS
)Y
GROUP BY cust_name,cust_serv_abbr";

try {
	$data = $this->_db->fetchAll($q);
	return $data;
} catch (Exception $e) {
   Zend_Debug::dump($e->getMessage());die($q);		
}	

}

public function get_sumaug17(){

$q = "SELECT a.cust_serv_abbr, a.cust_name cust_name, SUM(status_up) up, SUM(status_down) down, SUM(total) total  FROM 
(SELECT cust_name,cust_serv_abbr,
SUM(CASE WHEN STATUS = 'up' THEN 1 ELSE 0 END) status_up,
SUM(CASE WHEN STATUS = 'down' THEN 1 ELSE 0 END) status_down,
COUNT(1) total
FROM
(SELECT a.serv_id,a.cust_serv_abbr,
a.kandatel cust_category,a.cust_name,a.wil_id,a.witel,
CASE WHEN STATUS = 3 THEN 'up' ELSE
CASE WHEN STATUS IN (1,2,4) THEN 'down' END END STATUS, b.reg, NULL update_date, NULL pic, NULL phone,
a.neighbor,a.port
FROM v_mart_cust_service a, (SELECT DISTINCT reg,wil_id FROM p_sto_all) b
WHERE user_cat='AUG'
AND cust_name IS NOT NULL
AND witel IS NOT NULL
AND STATUS IN (1,2,3,4)
AND cust_serv_abbr IS NOT NULL
AND a.wil_id=b.wil_id) a GROUP BY cust_name,cust_serv_abbr,STATUS) a
GROUP BY cust_serv_abbr,cust_name";

try {
	$data = $this->_db->fetchAll($q);
	return $data;
} catch (Exception $e) {
   Zend_Debug::dump($e->getMessage());die($q);		
}	

}

public function get_aug17detail(){

$q = "SELECT a.serv_id,a.cust_serv_abbr,
a.kandatel cust_category,a.cust_name,a.wil_id,a.witel,
CASE WHEN STATUS = 3 THEN 'up' ELSE
CASE WHEN STATUS IN (1,2,4) THEN 'down' END END STATUS, b.reg, NULL update_date, NULL pic, NULL phone,
a.neighbor,a.port
FROM v_mart_cust_service a, (SELECT DISTINCT reg,wil_id FROM p_sto_all) b
WHERE user_cat='AUG'
AND cust_name IS NOT NULL
AND witel IS NOT NULL
AND STATUS IN (1,2,3,4)
AND cust_serv_abbr IS NOT NULL
AND a.wil_id=b.wil_id";

try {
	$data = $this->_db->fetchAll($q);
	return $data;
} catch (Exception $e) {
   Zend_Debug::dump($e->getMessage());die($q);		
}	

}

function getMASIVloc(){
	$sql = "SELECT CASE WHEN flag = 'M' THEN	1 WHEN flag = 'A' THEN	2 WHEN flag = 'S' THEN	3 WHEN flag = 'I' THEN	4 WHEN flag = 'V' THEN	5 ELSE	99 END urflag, 
event_id,loc_id,alias_name, flag
FROM	EVENT_LOCATION
WHERE 1=1 AND event_id=1 and	sub_loc_id = 0
ORDER BY	CASE WHEN flag = 'M' THEN	1 WHEN flag = 'A' THEN	2 WHEN flag = 'S' THEN	3 WHEN flag = 'I' THEN	4 WHEN flag = 'V' THEN	5 ELSE	99 END,
 loc_name";
	
	try {
		$data = $this->_db2->fetchAll($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}	
 
}

function getWifiStatusLocation(){
	$sql = "SELECT   CASE
            WHEN flag = 'M'
               THEN 1
            WHEN flag = 'A'
               THEN 2
            WHEN flag = 'S'
               THEN 3
            WHEN flag = 'I'
               THEN 4
            WHEN flag = 'V'
               THEN 5
            ELSE 99
         END urflag,
         b.flag, b.loc_name, b.alias_name,
         DECODE (status, 3, 5, status) status, COUNT (*), SUM (user_online)
    FROM event_device_ap a, event_location b
   WHERE a.loc_id = b.loc_id AND a.sub_loc_id = b.sub_loc_id
GROUP BY b.loc_name, b.alias_name, b.flag, DECODE (status, 3, 5, status)
ORDER BY CASE
            WHEN flag = 'M'
               THEN 1
            WHEN flag = 'A'
               THEN 2
            WHEN flag = 'S'
               THEN 3
            WHEN flag = 'I'
               THEN 4
            WHEN flag = 'V'
               THEN 5
            ELSE 99
         END,
         alias_name";
	
	try {
		$data = $this->_db2->fetchAll($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}
}

function getListEvent(){
	$sql = "SELECT   event_id AS VALUE, event_name AS display
    FROM event_list
ORDER BY event_id";
	
	try {
		$data = $this->_db2->fetchAll($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}	
}

function getListLocEvent($event_id=1){
	$sql = "SELECT DISTINCT loc_id AS VALUE, alias_name AS display,
                CASE
                   WHEN flag = 'M'
                      THEN 1
                   WHEN flag = 'A'
                      THEN 2
                   WHEN flag = 'S'
                      THEN 3
                   WHEN flag = 'I'
                      THEN 4
                   WHEN flag = 'V'
                      THEN 5
                   ELSE 99
                END urflag,
                flag
           FROM event_location
          WHERE event_id = ".$event_id."
       ORDER BY CASE
                   WHEN flag = 'M'
                      THEN 1
                   WHEN flag = 'A'
                      THEN 2
                   WHEN flag = 'S'
                      THEN 3
                   WHEN flag = 'I'
                      THEN 4
                   WHEN flag = 'V'
                      THEN 5
                   ELSE 99
                END,
                alias_name";
	
	try {
		$data = $this->_db2->fetchAll($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}	
}

function getSubLocation($params){
	
	// Zend_Debug::dump($params);//die();
	
	$sWhere = "";
	if($params['event_id']!=''){
		$sWhere .="and event_id=".$params['event_id']." ";
	}
	if($params['loc_id']!=''){
		$sWhere .="and loc_id=".$params['loc_id']." ";
	}
	// if($params['sub_loc_id']!=''){
		// $sWhere .="and sub_loc_id=".$params['sub_loc_id']." ";
	// }
	
	$sql = "select distinct sub_loc_id, sub_loc_name 
from event_location 
where 1=1 ".$sWhere."
order by sub_loc_id";
	// echo '<pre>';
	// echo $sql;
	// echo '</pre>';
	// die();
	try {
		$data = $this->_db2->fetchAll($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}	
}

function getSubLocation2($params){
	
	// Zend_Debug::dump($params);//die();
	
	$sWhere = "";
	if($params['event_id']!=''){
		$sWhere .="and event_id=".$params['event_id']." ";
	}
	if($params['loc_id']!=''){
		$sWhere .="and loc_id=".$params['loc_id']." ";
	}
	if($params['sub_loc_id']!=''){
		$sWhere .="and sub_loc_id=".$params['sub_loc_id']." ";
	}
	
	$sql = "select distinct sub_loc_id, sub_loc_name 
from event_location 
where 1=1 ".$sWhere."
order by sub_loc_id";
	// echo '<pre>';
	// echo $sql;
	// echo '</pre>';
	// die();
	try {
		$data = $this->_db2->fetchAll($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}	
}

function getListDeviceByService($params){
	
	$sWhere = "";
	if($params['event_id']!=''){
		$sWhere .="and event_id=".$params['event_id']." ";
	}
	if($params['loc_id']!=''){
		$sWhere .="and loc_id=".$params['loc_id']." ";
	}
	if($params['sub_loc_id']!=''){
		$sWhere .="and sub_loc_id=".$params['sub_loc_id']." ";
	}
	
	$sql_wifi = "select * from 
(select null device_id,'WIFI' service_type,event_id,loc_id, sub_loc_name,ethernetmac,null service_id,null service_name,nms_alias nama_perangkat,null link_id,null graph_id,decode(status,3,5,status) status, sum(clientcount) clientcount
from v_event_ap_status where 1=1 ".$sWhere."
group by event_id,loc_id, sub_loc_id,sub_loc_name,ethernetmac,nms_alias,decode(status,3,5,status) order by sub_loc_id, decode(status,3,5,status)) a";

	if($params['service']!=''){
		$sWhere .=" and service_type='".strtoupper($params['service'])."' ";
	}

	$sql_cctv = "select * from 
(select device_id,service_type, event_id,loc_id, sub_loc_name,null ethernetmac,service_name,service_id,service_name nama_perangkat, link_id, graph_id,decode(status,4,0,1,0,2,1,status) STATUS,null clientcount
from V_EVENT_ROUTER_STATUS where 1=1 and service_type='CCTV' ".$sWhere."
group by service_type,service_id,service_name,link_id,graph_id,rrd_name,event_id,loc_id,sub_loc_id,sub_loc_name,device_id,host_name_alias,decode(status,4,0,1,0,2,1,status) order by sub_loc_id) a";

	$sql_telkomsel = "select * from 
(select null device_id,service_type, event_id,loc_id, sub_loc_name,null ethernetmac,service_name,service_id,service_name nama_perangkat,null link_id, graph_id,decode(status,4,0,1,0,2,1,status) STATUS,null clientcount
from V_EVENT_ROUTER_STATUS where 1=1 and service_type='TELKOMSEL' ".$sWhere."
group by service_type,service_id,service_name,graph_id,rrd_name,event_id,loc_id,sub_loc_id,sub_loc_name,host_name_alias,decode(status,4,0,1,0,2,1,status) order by sub_loc_id,decode(status,4,0,1,0,2,1,status)) a";

	$sql_datin = "select * from 
(select device_id,service_type, event_id,loc_id, sub_loc_name,null ethernetmac,service_name,service_id,service_name nama_perangkat,link_id, 
(select graph_id from EVENT_DEVICE_ROUTER c where b.service_id=c.sid and b.link_id=c.link_id and c.device_type='PE') graph_id,
(case when jml_dev=tmp_st/3 then 3 when NOL=0 then 0 else 1 end) STATUS,null clientcount from (
select device_id,service_type, event_id,loc_id, sub_loc_name,service_id,service_name,link_id,max(graph_id) graph_id, count(*) JML_DEV, sum(status) TMP_ST, min(status) NOL from 
(select device_id,service_type, event_id,loc_id,sub_loc_id,null ethernetmac,sub_loc_name,service_id,service_name,link_id,graph_id,device_id id_perangkat,host_name_alias nama_perangkat,decode(status,4,0,1,0,2,1,nvl(status,0)) status
from V_EVENT_ROUTER_STATUS where 1=1 and service_type='DATIN'  ".$sWhere."
group by device_id,service_type,event_id,loc_id,sub_loc_id,sub_loc_name,service_id,service_name,host_name_alias,link_id,graph_id,decode(status,4,0,1,0,2,1,nvl(status,0)) order by sub_loc_id,decode(status,4,0,1,0,2,1,nvl(status,0))) a
group by device_id,service_type, event_id,loc_id, sub_loc_name,service_id,service_name,link_id) b)";
	
	$sql_datin = "select * from 
(select null device_id,service_type, event_id,loc_id, sub_loc_name,null ethernetmac,service_name,service_id,service_name nama_perangkat,link_id, 
(select graph_id from EVENT_DEVICE_ROUTER c where b.service_id=c.sid and b.link_id=c.link_id and c.device_type='PE') graph_id,
(case when jml_dev=tmp_st/3 then 3 when NOL=0 then 0 else 1 end) STATUS,null clientcount from (
select service_type, event_id,loc_id, sub_loc_name,service_id,service_name,link_id,max(graph_id) graph_id, count(*) JML_DEV, sum(status) TMP_ST, min(status) NOL from 
(select service_type, event_id,loc_id,sub_loc_id,null ethernetmac,sub_loc_name,service_id,service_name,link_id,graph_id,device_id id_perangkat,host_name_alias nama_perangkat,decode(status,4,0,1,0,2,1,nvl(status,0)) status
from V_EVENT_ROUTER_STATUS where 1=1 and service_type='DATIN'  ".$sWhere."
group by service_type,event_id,loc_id,sub_loc_id,sub_loc_name,service_id,service_name,device_id,host_name_alias,link_id,graph_id,decode(status,4,0,1,0,2,1,nvl(status,0)) order by sub_loc_id,decode(status,4,0,1,0,2,1,nvl(status,0))) a
group by service_type, event_id,loc_id, sub_loc_name,service_id,service_name,link_id) b)";
	
	
	if($params['service']=='wifi'){
		$sql = $sql_wifi;
	} else if($params['service']=='telkomsel'){
		$sql = $sql_telkomsel;	
	} else if($params['service']=='datin'){
		$sql = $sql_datin;		
	} else if($params['service']=='cctv'){
		$sql = $sql_cctv;		
	} else {
		$sql = $sql_wifi." union all ".$sql_telkomsel." union all ".$sql_datin." union all ".$sql_cctv;
	}
	
	
	// echo '<pre>';
	// echo $sql;
	// echo '</pre>';
	// die();
	
	
	
	try {
		$data = $this->_db2->fetchAll($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}	
	
}

function getStatusLayanan($params){
	
	$sql = "SELECT   CASE
            WHEN flag = 'M'
               THEN 1
            WHEN flag = 'A'
               THEN 2
            WHEN flag = 'S'
               THEN 3
            WHEN flag = 'I'
               THEN 4
            WHEN flag = 'V'
               THEN 5
            ELSE 99
         END urflag,
         a.*,
         CASE
            WHEN pct >=1 and pct < 70
               THEN 3
            WHEN pct >= 70 AND pct < 100
               THEN 2            
            WHEN pct = 100
               THEN 1
            ELSE 0
         END status
    FROM v_event_dash_status a
ORDER BY 1";
	
	try {
		$data = $this->_db2->fetchAll($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}	
}

function getMaxStatusLocation(){
	$sql = "select urflag,loc_id,flag,max(status) max_status from
(SELECT   CASE
            WHEN flag = 'M'
               THEN 1
            WHEN flag = 'A'
               THEN 2
            WHEN flag = 'S'
               THEN 3
            WHEN flag = 'I'
               THEN 4
            WHEN flag = 'V'
               THEN 5
            ELSE 99
         END urflag,
         a.*,
         CASE
            WHEN pct >=1 and pct < 70
               THEN 3
            WHEN pct >= 70 AND pct < 100
               THEN 2            
            WHEN pct = 100
               THEN 1
            ELSE 0
         END status
    FROM v_event_dash_status a
ORDER BY 1)
group by urflag,loc_id,flag
order by urflag,loc_id";
	
	try {
		$data = $this->_db2->fetchAll($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}	
}

function getCountStatusLayananPerLocation(){
	$sql = "select  CASE
            WHEN flag = 'M'
               THEN 1
            WHEN flag = 'A'
               THEN 2
            WHEN flag = 'S'
               THEN 3
            WHEN flag = 'I'
               THEN 4
            WHEN flag = 'V'
               THEN 5
            ELSE 99
         END urflag, a.* from v_event_dash_status a
order by  CASE
            WHEN flag = 'M'
               THEN 1
            WHEN flag = 'A'
               THEN 2
            WHEN flag = 'S'
               THEN 3
            WHEN flag = 'I'
               THEN 4
            WHEN flag = 'V'
               THEN 5
            ELSE 99
         END, alias_name,service_type";
	
	try {
		$data = $this->_db2->fetchAll($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}	
}

function getPerangkat($params){

	if($params['service_type']=='WIFI'){
		$sql = "select a.nms_name, a.loc_id,b.loc_name,b.sub_loc_id,b.sub_loc_name, a.ap_location,a.ETHERNETMAC macaddress,a.ipaddress,a.status  , a.clientcount
from v_event_ap_status a, event_location b 
where 1=1 and a.loc_id=b.loc_id(+) and a.sub_loc_id=b.sub_loc_id(+) and
a.ethernetmac='".$params['ethernetmac']."' ";
	} else if($params['service_type']=='TELKOMSEL'){
		$sql = "select distinct * from 
(select null device_id,service_type,host_name,host_name_alias,port,ip_address, event_id,loc_id, sub_loc_name,null ethernetmac,service_name,service_id,service_name nama_perangkat,null link_id, graph_id,decode(status,4,0,1,0,2,1,status) STATUS,null clientcount
from V_EVENT_ROUTER_STATUS where 1=1 and service_type='TELKOMSEL' and loc_id=".$params['loc_id']." and service_name='".$params['nama_perangkat']."'
group by service_type,host_name,host_name_alias,port,ip_address,service_id,service_name,graph_id,rrd_name,event_id,loc_id,sub_loc_id,sub_loc_name,device_id,host_name_alias,decode(status,4,0,1,0,2,1,status) order by sub_loc_id,decode(status,4,0,1,0,2,1,status)) a";
	}

	
	// die($sql);
	try {
		$data = $this->_db2->fetchRow($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}	

	
}

function getCollaborationEvent(){
	
	$sql = "SELECT   a.*, TO_CHAR (date_time, 'yyyy-mm-dd hh:mi:ss') datetime
    FROM event_collaboration a
   WHERE level_id = 0
ORDER BY date_time DESC";
	
	try {
		$data = $this->_db2->fetchAll($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}	
}

function getUserOnlineTraffic($params){
	
	$sql = "select x.* from
(select date_id,time_id,date_id||'-'||time_id periode,nms_name,ETHERNETMAC, sum(clientcount) clientcount, sum(traffic) traffic 
from EVENT_AP_STATUS_HIST 
where 1=1 and date_id=to_char(sysdate,'YYYYMMDD')
and nms_name='".$params['nama_perangkat']."'
group by date_id,time_id,nms_name,ETHERNETMAC
order by date_id||'-'||time_id) x";
	$sql = "select x.* from
(select date_id,time_id,date_id||'-'||time_id periode,ETHERNETMAC, sum(clientcount) clientcount, sum(traffic) traffic 
from EVENT_AP_STATUS_HISTDAY 
where 1=1 and date_id=to_char(sysdate,'YYYYMMDD')
and ethernetmac='".$params['ethernetmac']."'
group by date_id,time_id,ETHERNETMAC
order by date_id||'-'||time_id) x";
// die($sql);
	
	try {
		$data = $this->_db2->fetchAll($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}	
}

function ins_chats($params){
	$dataform = json_decode($params['json']);
	// Zend_Debug::dump($dataform);die();
	$msg = $params['text_message'];
	
	$msg = str_replace("'","*petik*",$msg);
	
	// Zend_Debug::dump($identity);
	
	$sql = "insert into event_collaboration (nik,nama,date_time,message,filename)
	values ('".$params["uname"]."','".$params["fullname"]."',sysdate,'".$msg."','".$params['filename']."')";
	// die($sql);
	
	
	try {
		$this->_db2->query($sql);
		
		$sql = "commit;";
		$this->_db2->query($sql2);
		// Zend_Debug::dump($data);die();
		// return $data;
		
		$result = array("transaction"=>"true");
	} catch (Exception $e) {
		$result = array("transaction"=>"false");
		// Zend_Debug::dump($e->getMessage());die($q);		
	}	
	
	return $result;

	
}

function ins_comment($params){
	$msg = $params['text_message'];
	
	$msg = str_replace("'","*petik*",$msg);
	
	// Zend_Debug::dump($identity);
	
	$sql = "insert into event_collaboration (nik,nama,date_time,message,filename,level_id,parent_level)
	values ('".$params["uname"]."','".$params["fullname"]."',sysdate,'".$msg."','".$params['filename']."',1,".$params['parent_level'].")";
	// die($sql);
	
	
	try {
		$this->_db2->query($sql);
		
		$sql = "commit;";
		$this->_db2->query($sql2);
		// Zend_Debug::dump($data);die();
		// return $data;
		
		$result = array("transaction"=>"true");
	} catch (Exception $e) {
		$result = array("transaction"=>"false");
		// Zend_Debug::dump($e->getMessage());die($q);		
	}	
	
	return $result;

	
}

function count_comment(){
	$sql="SELECT   parent_level, COUNT (1) jml_comment
    FROM event_collaboration
   WHERE level_id = 1
GROUP BY parent_level";
	
	try {
		$data = $this->_db2->fetchAll($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}	
}


function del_chats($params){
	
	
	// Zend_Debug::dump($identity);
	
	$sql = "delete from event_collaboration where id=".$params['id'];
	// die($sql);
	
	
	try {
		$this->_db2->query($sql);
		
		$sql = "commit";
		$this->_db2->query($sql2);
		// Zend_Debug::dump($data);die();
		// return $data;
		
		$result = array("transaction"=>"true");
	} catch (Exception $e) {
		$result = array("transaction"=>"false");
		// Zend_Debug::dump($e->getMessage());die($q);		
	}	
	
	return $result;

	
}

function getTanggalDanLokasiPosko($nikname){
	
	$sWhere="";
	if($nikname!=''){
		$sWhere .=" and (upper(nik) like '%".strtoupper($nikname)."%' or upper(nama) like '%".strtoupper($nikname)."%') ";
	}
	
	$sql="SELECT   tgl_posko, nvl(trim(lokasi),'NULL') lokasi, COUNT (1) jml
    FROM v_event_jadwal_posko
   WHERE 1 = 1 ".$sWhere."
GROUP BY tgl_posko, trim(lokasi)
ORDER BY tgl_posko";

	// die($sql);
	
	try {
		$data = $this->_db2->fetchAll($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}	
}
function getPICLokasi($params){
	// $sql = "SELECT   nama, nik, jabatan, unit, no_hp, jadwal
    // FROM v_event_jadwal_posko
   // WHERE tgl_posko = '".$params['tanggal']."' AND lokasi = '".$params['lokasi']."'
// ORDER BY tgl_posko";

	$sql="SELECT   CASE
            WHEN (lower(nama) LIKE '%".$params['filter']."%' OR lower(nik) LIKE '%".$params['filter']."%')
               THEN 1
            ELSE 2
         END urutan, nama, nik, jabatan, unit, no_hp, jadwal, peran
    FROM v_event_jadwal_posko
   WHERE 1 =1 AND tgl_posko = '".$params['tanggal']."' AND nvl(trim(lokasi),'NULL') = '".$params['lokasi']."'
ORDER BY CASE
            WHEN (lower(nama) LIKE '%".$params['filter']."%' OR lower(nik) LIKE '%".$params['filter']."%')
               THEN 1
            ELSE 2
         END";

	// die($sql);
	
	try {
		$data = $this->_db2->fetchAll($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}	
}

function getInbox(){
	$sql="SELECT   a.ID, b.loc_id, b.alias_name, a.layanan, a.prioritas, a.description,
         a.user_id, a.status,
         TO_CHAR (a.created_date, 'YYYY-MM-DD HH:MI:SS') created_date
    FROM event_tickets a LEFT JOIN event_location b ON a.LOCATION = b.loc_id(+)
   WHERE 1 = 1 AND a.status <> 3
GROUP BY a.ID,
         b.loc_id,
         b.alias_name,
         a.layanan,
         a.prioritas,
         a.description,
         a.user_id,
         a.status,
         TO_CHAR (a.created_date, 'YYYY-MM-DD HH:MI:SS')
ORDER BY created_date DESC";
	
	try {
		$data = $this->_db2->fetchAll($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}	
}

function insert_ticket($params,$uid){
	
	// Zend_Debug::dump($params);
	
	$sql="insert into event_tickets 
(location,layanan,prioritas,description,user_id,status,created_date)
values
('".$params['loc']."','".$params['layanan']."','".$params['prioritas']."','".$params['description']."',".$uid.",'1',sysdate)";	
	// die($sql);
	try{
		$this->_db2->query($sql);
		
		// $sql = "commit;";		
		// $this->_db2->query($sql);
		
		$result = array('result'=>true, 'message'=> 'Ticket has been added.');
	} catch (Exception $e) {
		$result = array('result'=>false, 'message'=> $e->getMessage());
	}
	// Zend_Debug::dump($result);die();
	return $result;
	
}

function getAdditionalInfo($link=55){
	$sql = "select * from v_event_router_status
where link_id='".$link."'
order by seq_id";
	
	try {
		$data = $this->_db2->fetchAll($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}	

}

function getStatusLinkMediaCenter(){
	$sql="select decode(link_id,51,1,56,1,52,2,57,2,54,3,59,3,55,4,60,4,53,5,58,5)image,
decode(link_id,51,1,56,2,52,1,57,2,54,1,59,2,55,1,60,2,53,1,58,2) col,rrd_name,link_id,
service_id,link_id,status,graph_id,service_name,seq_id, current_traffic from v_event_router_status
where 1=1 and link_id in (51,56,52,57,54,59,55,60,53,58)
order by 1,2,seq_id";
	
	try {
		$data = $this->_db2->fetchAll($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}	
}

function getStatusLinkMetroCCTVBandung(){
	$sql="select link_id,rrd_name,service_id,status,graph_id,service_name,seq_id,current_traffic 
from v_event_router_status 
where 1=1 
and link_id in(73,76,74,543,544,72,75,79,80,81,82,77,83,84,78,71,539,538)";
	
	try {
		$data = $this->_db2->fetchAll($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}	
}

function getStatusLinkDatinSecurity(){
	$sql="select link_id,rrd_name,service_id,status,graph_id,service_name,seq_id,current_traffic 
from v_event_router_status 
where 1=1 
and link_id in(85,86,87,88,89,90,91,92)
order by link_id,seq_id";
	
	try {
		$data = $this->_db2->fetchAll($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}	
}

function getStatusLinkDatinVenue(){
	$sql="select link_id,rrd_name,service_id,status,graph_id,service_name,seq_id,current_traffic 
from v_event_router_status 
where 1=1 
and link_id in(589,587,583,591,593,595,597,599)
order by link_id,seq_id";
	
	try {
		$data = $this->_db2->fetchAll($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}	
}

function getCommentLevel1($parent_level){
	$sql="SELECT   a.*, TO_CHAR (date_time, 'yyyy-mm-dd hh:mi:ss') datetime
    FROM event_collaboration a
   WHERE level_id = 1 AND parent_level = ".$parent_level."
ORDER BY date_time DESC";
	
	try {
		$data = $this->_db2->fetchAll($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}	
}

function getDataRekap(){
	$sql="SELECT   DECODE (area, 'M', 1, 'A', 2, 'S', 3, 'I', 4, 'V', 5) urutan, a.*
    FROM event_laporan a
ORDER BY DECODE (area, 'M', 1, 'A', 2, 'S', 3, 'I', 4, 'V', 5)";
	
	try {
		$data = $this->_db2->fetchAll($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}	
}

function inputlaporan($params){
	
	$msg = str_replace("'","*petik*",$params["message"]);
	
	$sql="INSERT INTO event_message
            (ID,MESSAGE, created_by, created_date
            )
     VALUES ((select nvl(max(id),0)+1 max_id from event_message),'".$msg."', '".$params['nik']."', SYSDATE
            )";
	// die($sql);
	try{
		$this->_db2->query($sql);
		
		// $sql = "commit;";		
		// $this->_db2->query($sql);
		
		$result = array('result'=>true, 'message'=> 'Report has been added.');
	} catch (Exception $e) {
		$result = array('result'=>false, 'message'=> $e->getMessage());
	}
	// Zend_Debug::dump($result);die();
	return $result;
}

function getDMposko(){
	$sql="SELECT nik, nama
  FROM v_event_jadwal_posko
 WHERE 1 = 1
   AND tgl_posko = TO_CHAR (SYSDATE, 'yyyymmdd')
   AND peran = 'DUTY MANAGER'
   AND kota = 'Jakarta'";
	
	try {
		$data = $this->_db2->fetchRow($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}	
}

function getSummaryBySubLocation($params){
	$sWhere = "";
	if($params['event_id']!=''){
		$sWhere .="and event_id=".$params['event_id']." ";
	}
	if($params['loc_id']!=''){
		$sWhere .="and loc_id=".$params['loc_id']." ";
	}
	if($params['sub_loc_id']!=''){
		$sWhere .="and sub_loc_id=".$params['sub_loc_id']." ";
	}
	$sql = "select service_type, sub_loc_name, sum(status_up) status_up, sum(status_down) status_down, sum(status_unknown) status_unknown, sum(clientcount) clientcount from
(SELECT   service_type, loc_id, alias_name, sub_loc_name, clientcount, flag,
            NVL (SUM (status_up), 0) status_up,
            NVL (SUM (status_down), 0) status_down,
            NVL (SUM (status_unknown), 0) status_unknown,
            CASE
               WHEN NVL (SUM (status_up), 0) + NVL (SUM (status_down), 0) =
                                                                        0
                  THEN 0
               ELSE 
            ROUND (  NVL (SUM (status_up), 0)
                   / (NVL (SUM (status_up), 0) + NVL (SUM (status_down), 0))
                   * 100
                  ) 
            END pct
       FROM (SELECT 'WIFI' service_type, loc_id, alias_name, sub_loc_name, clientcount, flag,
                    CASE
                       WHEN DECODE (status, 5, 3, 1, 2, 3, 3, 1) =
                                                                  3
                          THEN 1
                    END status_up,
                    CASE
                       WHEN DECODE (status, 5, 3, 1, 2, 3, 3, 1) =
                                                                2
                          THEN 1
                    END status_down,
                    CASE
                       WHEN DECODE (status,
                                    5, 3,
                                    1, 2,
                                    3, 3,
                                    1
                                   ) = 1
                          THEN 1
                    END status_unknown
               FROM v_event_ap_status
              WHERE 1=1 ".$sWhere.")
   GROUP BY service_type, loc_id, alias_name, sub_loc_name, clientcount, flag
   UNION ALL
   SELECT   service_type, loc_id, alias_name, sub_loc_name, clientcount, flag,
            NVL (SUM (status_up), 0) status_up,
            NVL (SUM (status_down), 0) status_down,
            NVL (SUM (status_unknown), 0) status_unknwon,
            CASE
               WHEN NVL (SUM (status_up), 0) + NVL (SUM (status_down), 0) =
                                                                        0
                  THEN 0
               ELSE 
            ROUND (  NVL (SUM (status_up), 0)
                   / (NVL (SUM (status_up), 0) + NVL (SUM (status_down), 0))
                   * 100
                  ) 
             end     pct
       FROM (SELECT service_type, loc_id, alias_name, sub_loc_name, 0 clientcount, flag,
                    CASE
                       WHEN status = 3
                          THEN 1
                    END status_up, CASE
                       WHEN status = 2
                          THEN 1
                    END status_down,
                    CASE
                       WHEN (status = 1 OR status = 4 or status is null)
                          THEN 1
                    END status_unknown
               FROM v_event_router_status
              WHERE 1=1 and service_type <>'DATIN' ".$sWhere.")
      WHERE (   status_up IS NOT NULL
             OR status_down IS NOT NULL
             OR status_unknown IS NOT NULL
            )
   GROUP BY service_type, loc_id, alias_name, sub_loc_name, clientcount, flag   
UNION ALL
   SELECT   service_type, loc_id, alias_name, sub_loc_name, clientcount, flag,
            NVL (SUM (status_up), 0) status_up,
            NVL (SUM (status_down), 0) status_down,
            NVL (SUM (status_unknown), 0) status_unknwon,
            CASE WHEN NVL (SUM (status_up), 0) = 0 and NVL (SUM (status_down), 0) = 0  THEN 0 
               WHEN NVL (SUM (status_up), 0) = 0 and NVL (SUM (status_down), 0) <> 0  THEN 1
               else ROUND (NVL (SUM (status_up), 0)
                           / (  NVL (SUM (status_up), 0)
                              + NVL (SUM (status_down), 0)
                             )
                           * 100
                          )
             end     pct
       FROM (SELECT service_type, loc_id, alias_name, sub_loc_name, clientcount, flag,
                    CASE
                       WHEN status = 3
                          THEN 1
                    END status_up, 
                    CASE
                       WHEN status = 1
                          THEN 1
                    END status_down,
                    0 status_unknown
               --FROM v_event_router_status
               from V_EVENT_ROUTER_STATUS_DTN
              WHERE 1=1 ".$sWhere.")
      WHERE (   status_up IS NOT NULL
             OR status_down IS NOT NULL
             OR status_unknown IS NOT NULL
            )
   GROUP BY service_type, loc_id, alias_name, sub_loc_name, clientcount, flag) xx
   group by service_type, sub_loc_name";

// Zend_Debug::dump($sql);die();
	// die($sql);
	try {
		$data = $this->_db2->fetchAll($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}	
}


function getInboxTicketAP(){
	$sql = "SELECT   ID, alias_name, nms_name, ap_location, device_type, nms_id,
         ethernetmac, TO_CHAR (date_id, 'yyyy-mm-dd hh24:mi:ss') date_id,
         status_ggn, TO_CHAR (date_closed, 'yyyy-mm-dd hh24:mi:ss')date_closed, user_id
    FROM event_ticket_alert
   WHERE 1 = 1
ORDER BY status_ggn, date_id DESC, date_closed desc, alias_name";
	
	try {
		$data = $this->_db2->fetchAll($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}	
}

function getInboxTicketRouter(){
	$sql = "SELECT DISTINCT a.id,b.service_type,b.service_name,b.ip_address,b.vlan_id,a.flag_sent,c.alias_name,a.service_id,
 a.host_name host_name,a.port,a.device_type,a.status_old,
 a.status_new,to_char(a.created_date,'YYYY-MM-dd hh24:mi:ss') created_date,status_ggn,to_char(a.send_date,'YYYY-MM-dd hh24:mi:ss') send_date, a.user_id
FROM event_ticket_alert_router a,
 event_device_router b,
 event_location c
WHERE a.host_name=b.host_name
and a.port=b.port
AND b.loc_id = c.loc_id
and b.sub_loc_id=c.sub_loc_id
order by a.status_ggn, created_date desc, send_date desc";
	
	try {
		$data = $this->_db2->fetchAll($sql);
		// Zend_Debug::dump($data);die();
		return $data;
	} catch (Exception $e) {
		Zend_Debug::dump($e->getMessage());die($q);		
	}	
}

function closeticket($params,$identity){
	
	if($params['layanan']=='WIFI'){
		
		$sql = "update event_ticket_alert set status_ggn=1, date_closed=sysdate, user_id=".$identity->uname." where id=".$params['id'];
	} else {
		// $sql = "update event_ticket_alert_router set status_ggn=1, send_date=sysdate, user_id=".$identity->uid." where id=38";
		$sql = "update event_ticket_alert_router set status_ggn=1, send_date=sysdate, user_id=".$identity->uname." where id=".$params['id'];
	}
	
	// die($sql);
	try{
		$this->_db2->query($sql);
		
		// $sql = "commit;";		
		// $this->_db2->query($sql);
		
		$result = array('result'=>true, 'message'=> 'Ticket has been CLOSED.');
	} catch (Exception $e) {
		$result = array('result'=>false, 'message'=> $e->getMessage());
	}
	// Zend_Debug::dump($result);die();
	return $result;
}

public function get_kaa_serosertabbr() {
$q = "SELECT distinct(sero_sert_abbreviation) sero_sert_abbreviation 
from cust_service_devices 
where user_cat = 'KAA'";

try {
	$data = $this->_db->fetchAll($q);
	return $data;
} catch (Exception $e) {
   Zend_Debug::dump($e->getMessage());die($q);		
}	
}

public function get_kaa_neighbor() {
$q = "SELECT distinct(neighbor) neighbor,port,sero_serv_id,data_rrd,snmp_index,id 
from cust_service_devices 
where user_cat = 'KAA' 
order by neighbor";

try {
	$data = $this->_db->fetchAll($q);
	return $data;
} catch (Exception $e) {
   Zend_Debug::dump($e->getMessage());die($q);		
}	
}

function getDivWit(){
	$sql = "select b.regional, a.witel, 
case when a.status not in('Up','Down') then 'Down'
else a.status end sstatus, 
count(*) jumlah
from wifi_nms_ap_detail a, WIFI_WITEL b
where a.witel=b.witel
group by b.regional, a.witel, 
case when a.status not in('Up','Down') then 'Down'
else a.status end
order by 1,2";

try {
	$data = $this->_db2->fetchAll($sql);
	return $data;
} catch (Exception $e) {
   Zend_Debug::dump($e->getMessage());die($q);		
}	
}

}
