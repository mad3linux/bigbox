<?php


/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class Model_Prabamedia extends Zend_Db_Table_Abstract {

    public function get_media_all($tag) {
        $config = array('endpoint' => array('localhost' => array('host' => 'localhost',
                        'port' => 8983,
                        'path' => '/solr/nutch/',
                       )));
        try {
            //
            $client = new Solarium\Client($config);
            $query = $client->createSelect();
            $query->setQuery('content:' . 
                             $tag);
            $hl = $query->getHighlighting();
            $hl->setFields('content');
            $hl->setSimplePrefix('<p style="color:red">');
            $hl->setSimplePostfix('</>');
            $resultset = $client->select($query);
            $highlighting = $resultset->getHighlighting();
            $i = 1;

            foreach($resultset as $document) {

                foreach($document as $field => $value) {
                    $new[$i][$field] = $value;
                }
                $highlightedDoc = $highlighting->getResult($document->id);
                if($highlightedDoc) {

                    foreach($highlightedDoc as $field => $highlight) {
                        $new[$i]['highlight'] = implode(' (...) ', $highlight). ' ';
                    }
                }
                $i ++;
            }
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
        return $new;
    }
}
