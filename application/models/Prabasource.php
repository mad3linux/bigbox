<?php


/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class Model_Prabasource extends Zend_Db_Table_Abstract {

    public function add_new_file($data) {
        $path = str_replace("~", "/", $data['path']);
        try {
            if(is_file($path)) {
                $ourFileName = $public = constant('APPLICATION_PATH'). '/../public/docs_scripts/execscripts/' . $data['title'];
                rename($path, $ourFileName);
            } else {
                $ourFileName = $public = constant('APPLICATION_PATH'). '/../public/docs_scripts/execscripts/' . $data['title'];
                $ourFileHandle = fopen($ourFileName, 'w');
                fclose($ourFileHandle);
                chmod($ourFileName, 0766);
            }
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
        //die("sssad");
    }

    public function update_script($data) {
        //public/docs_scripts/execscripts
        //Zend_Debug::dump($data); die();
        try {
            $path = str_replace("~", "/", $data['nodeid']);
            //die($path);
            if(is_file($path)&& is_writeable($path)) {
                file_put_contents($path, $data['source']);
                chmod($file, 0766);
                $success = true;
            } else {
                die("not writeable");
            }
        }
        catch(Exception $e) {
            //Zend_Debug::dump($e); die();	
            return false;
        }
        return true;
        //die("sssad");
    }
}
