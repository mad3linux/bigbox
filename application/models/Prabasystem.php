<?php

/*
 * Copyright (C) Prabtech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class Model_Prabasystem extends Zend_Db_Table_Abstract {
     public function check_auth($data) {

         $dat = $this->_db->fetchRow("select * from zpraba_users_api where user_name=? and user_key=?", array($data['user'], md5($data['key'])));
         
         return $dat;
         
     }    
    public function add_portlet_table($k, $data) {

        $sql = "insert into zpraba_portlet (portlet_name, portlet_title, weight, portlet_type, portlet_attrs, content_attrs) values (?, ?, ?, ?, ?, ?)";


        $vatr = array();
        foreach ($data ['attrs'][$k] as $v) {
            $vatr [$v] = true;
        }

        $var = array(
            'api_id' => implode(',', $data ['get_api'][$k]),
            'header' => $data ['header'][$k],
            'attrs' => $vatr
            );


        $br = array(
            'color' => $data ['color'][$k],
            'type' => $data ['type'][$k],
            'width' => $data ['width'][$k],
                //'tools' => $data ['tools'],
                //'actions' => $act 
            );

        $var0 = array(
            $data ['name'][$k],
            $data ['title'][$k],
            $data ['weight'][$k],
            'table',
            serialize($br),
            serialize($var)
            );


        try {
            $this->_db->query($sql, $var0);
            $result = array(
                'result' => true,
                'message' => 'New Portlet has been created.',
                'id' => $this->_db->lastInsertId()
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }

    public function add_update_portlet_bigchart($data,$portlet) {
		if(isset($data['pid']) && $data['pid']!=""){
			$sql = "update zpraba_portlet set portlet_name = ?, portlet_title = ?, weight = ?, portlet_type = ?, portlet_attrs = ?, content_attrs = ? where id = ".$data['pid'];
		}else{
			$sql = "insert into zpraba_portlet (portlet_name, portlet_title, weight, portlet_type, portlet_attrs, content_attrs) values (?, ?, ?, ?, ?, ?)";
		}


		// Zend_Debug::dump($data);die();
        $br = array(
            'color' => $portlet ['color'],
            'type' => $portlet ['type'],
            'width' => $portlet ['width']
		);

        $var0 = array(
            $portlet ['name'],
            $portlet ['title'],
            $portlet ['weight'],
            'bigchart',
            serialize($br),
            serialize($data)
        );

		// Zend_Debug::dump($var0);die();
        try {
            $this->_db->query($sql, $var0);
            $result = array(
                'result' => true,
                'message' => 'New Portlet has been created.',
                'id' =>(isset($data['pid']) && $data['pid']!="")?$data['pid']:$this->_db->lastInsertId()
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }

    public function add_update_portlet_pivotamchart($data,$portlet) {
		if(isset($data['pid']) && $data['pid']!=""){
			$sql = "update zpraba_portlet set portlet_name = ?, portlet_title = ?, weight = ?, portlet_type = ?, portlet_attrs = ?, content_attrs = ? where id = ".$data['pid'];
		}else{
			$sql = "insert into zpraba_portlet (portlet_name, portlet_title, weight, portlet_type, portlet_attrs, content_attrs) values (?, ?, ?, ?, ?, ?)";
		}


		// Zend_Debug::dump($data);die();
        $br = array(
            'color' => $portlet ['color'],
            'type' => $portlet ['type'],
            'width' => $portlet ['width']
		);

        $var0 = array(
            $portlet ['name'],
            $portlet ['title'],
            $portlet ['weight'],
            'amchartpivot',
            serialize($br),
            serialize($data)
        );

		// Zend_Debug::dump($var0);die();
        try {
            $this->_db->query($sql, $var0);
            $result = array(
                'result' => true,
                'message' => 'New Portlet has been created.',
                'id' =>(isset($data['pid']) && $data['pid']!="")?$data['pid']:$this->_db->lastInsertId()
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }

    public function add_portlet_graph($k, $data) {
        //Zend_Debug::dump($data); die();
        $sql = "insert into zpraba_portlet (portlet_name, portlet_title, weight, portlet_type, portlet_attrs, content_attrs) values (?, ?, ?, ?, ?, ?)";


        for ($i = 0; $i < count($data ['key'][$k]); $i ++) {
            if ($data ['key'][$k][$i] != "") {
                $field [$i] ['key'] = $data ['key'][$k] [$i];
                $field [$i] ['value'] = $data ['xvalue'][$k] [$i];

                $field [$i] ['name'] = $data ['varx'][$k] [$i];
                $field [$i] ['api'] = $data ['api'][$k] [$i];
            }
        }




        // Zend_Debug::dump($field); die();
        $var0 = array(
            "graph_id" => $data ['graph_id'][$k],
            "class" => $data ['graph_class'][$k],
            "api_id" => implode(',', $data ['get_api'][$k]),
            "graph_type" => $data ['graph_type'][$k],
            "post_url" => $data ['post_url'][$k],
            "field" => $field,
            "graph_lib" => $data ['graphlib'][$k]
            );



        $br = array(
            'color' => $data ['color'][$k],
            'type' => $data ['type'][$k],
            'width' => $data ['width'][$k],
                //'tools' => $data ['tools'],
                //'actions' => $act 
            );

        $var = array(
            $data ['name'][$k],
            $data ['title'][$k],
            $data ['weight'][$k],
            'graphic',
            serialize($br),
            serialize($var0)
            );


        try {
            $this->_db->query($sql, $var);
            $result = array(
                'result' => true,
                'message' => 'New Portlet has been created.',
                'id' => $this->_db->lastInsertId()
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }

    public function websocketbuilder($data) {
        //Zend_Debug::dump($data); die();
        $sql = "update zpraba_portlet  set content_attrs= ? where id=?";
        $var = array(
            "template" => $data ['template'],
            );

        try {
            $this->_db->query($sql, array(
                serialize($var),
                $data ['pid']
                ));
            // $pid = $this->_db->lastInsertId();
            $result = array(
                'result' => true,
                'message' => 'Succes. View  <a href="/prabagen/viewportlet/id/' . $data ['pid'] . '">Portlet</a>'
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        // Zend_Debug::dump($result); die();
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }
    public function tabbuilder($data) {
      //  Zend_Debug::dump($data); die();
        $sql = "update zpraba_portlet  set content_attrs= ? where id=?";
        $var = array(
            "val_id" => $data ['val_id'],
            "server" => $data ['server'],
          
            
            "contents" => $data ['schtml']
            );

        try {
            $this->_db->query($sql, array(
                serialize($var),
                $data ['pid']
                ));
            // $pid = $this->_db->lastInsertId();
            $result = array(
                'result' => true,
                'message' => 'Succes. View  <a href="/prabagen/viewportlet/id/' . $data ['pid'] . '">Portlet</a>'
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        // Zend_Debug::dump($result); die();
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }
    public function htmlbuilder($data) {

        $sql = "update zpraba_portlet  set content_attrs= ? where id=?";
        $var = array(
            "html_id" => $data ['html_id'],
            "html_class" => $data ['html_class'],
            "api_id" => implode(',', $data ['get_api']),
            "post_url" => $data ['post_url'],
            "contents" => $data ['schtml']
            );

        try {
            $this->_db->query($sql, array(
                serialize($var),
                $data ['pid']
                ));
            // $pid = $this->_db->lastInsertId();
            $result = array(
                'result' => true,
                'message' => 'Succes. View  <a href="/prabagen/viewportlet/id/' . $data ['pid'] . '">Portlet</a>'
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        // Zend_Debug::dump($result); die();
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }

    public function graphbuilder($data) {

        //Zend_Debug::dump($data); die();
        $sql = "update zpraba_portlet  set content_attrs= ? where id=?";

        for ($i = 0; $i < count($data ['key']); $i ++) {
            if ($data ['key'][$i] != "") {
                $field [$i] ['key'] = $data ['key'] [$i];
                $field [$i] ['value'] = $data ['xvalue'] [$i];

                $field [$i] ['name'] = $data ['varx'] [$i];
                $field [$i] ['api'] = $data ['api'] [$i];
            }
        }




        // Zend_Debug::dump($field); die();
        $var = array(
            "graph_id" => $data ['graph_id'],
            "class" => $data ['graph_class'],
            "api_id" => implode(',', $data ['get_api']),
            "graph_type" => $data ['graph_type'],
            "post_url" => $data ['post_url'],
            "field" => $field,
            "graph_lib" => $data ['graphlib']
            );

        // Zend_Debug::dump($field); die();


        try {
            $this->_db->query($sql, array(
                serialize($var),
                $data ['pid']
                ));
            // $pid = $this->_db->lastInsertId();
            $result = array(
                'result' => true,
                'message' => 'Portlet Graph has been created/changed. <a href="/prabagen/viewportlet/id/' . $data ['pid'] . '">View Portlet</a>'
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        // Zend_Debug::dump($result); die();
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }

    public function update_portlet($data) {
        // Zend_Debug::dump($data); die();
        $sql = "update zpraba_portlet set get_api=?, portlet_name=?, portlet_title=?, weight=?, portlet_type=?, portlet_attrs=?, custom_view=? where id=?";

        for ($xx = 0; $xx < count($data ['act_label']); $xx ++) {
			if($data ['act_label'] [$xx]!= ""){
            $act [$xx] ['label'] = $data ['act_label'] [$xx];
			}
			if($data ['act_class'] [$xx]!= ""){
            $act [$xx] ['class'] = $data ['act_class'] [$xx];
			}
			if($data ['act_clsicon'] [$xx]!= ""){
            $act [$xx] ['classicon'] = $data ['act_clsicon'] [$xx];
			}
			if($data ['act_modal'] [$xx]!= ""){
            $act [$xx] ['data-toggle'] = $data ['act_modal'] [$xx];
			}
			if($data ['act_id'] [$xx]!= ""){
            $act [$xx] ['id'] = $data ['act_id'] [$xx];
			}
			if($data ['act_target'] [$xx]!= ""){
            $act [$xx] ['data-target'] = $data ['act_target'] [$xx];
			}
        }

        $br = array(
            'color' => $data ['color'],
            'type' => $data ['type'],
            'width' => $data ['width'],
            'tools' => $data ['tools'],
            'actions' => $act
            );

        $var = array(
            implode(',', $data ['get_api']),
            $data ['name'],
            $data ['title'],
            $data ['weight'],
            $data ['maintype'],
            serialize($br),
            $data ['customview'],
            $data ['id']
            );

        try {



            $this->_db->query($sql, $var);

            $result = array(
                'result' => true,
                'message' => 'Portlet has been updated.',
                'id' => $data ['id']
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        // Zend_Debug::dump($result); die();
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }

    public function formbuilder($data) {

        // Zend_Debug::dump($data); die();
        $sql = "update zpraba_portlet  set content_attrs= ? where id=?";

        for ($i = 0; $i < count($data ['id']); $i ++) {
            $field [$i] ['id'] = $data ['id'] [$i];
            $field [$i] ['type'] = $data ['type'] [$i];
            $field [$i] ['name'] = $data ['id'] [$i];
            $field [$i] ['class'] = $data ['class'] [$i];
            $field [$i] ['label'] = $data ['label'] [$i];
            $field [$i] ['classicon'] = $data ['classicon'] [$i];
            $field [$i] ['initvalue'] = $data ['initvalue'] [$i];
            $field [$i] ['initApi'] = $data ['initapi'] [$i];
            $field [$i] ['required'] = $data ['req'] [$i];
            $field [$i] ['area'] = $data ['area'] [$i];
            $field [$i] ['help'] = $data ['help'] [$i];
        }

        // Zend_Debug::dump($field); die();
        $var = array(
            "id" => $data ['form_id'],
            "class" => $data ['formclass'],
            // "method" => $data['formclass'],
            "postmessage" => $data ['post_message'],
            "extendedjs" => $data ['extendedjs'],
            "api_id" => implode(',', $data ['get_api']),
            "post_api" => $data ['post_api'],
            "post_url" => $data ['post_url'],
            "field" => $field
            );

        try {
            $this->_db->query($sql, array(
                serialize($var),
                $data ['pid']
                ));
            $pid = $this->_db->lastInsertId();
            $result = array(
                'result' => true,
                'message' => 'Form has been updated.View  <a href="/prabagen/viewportlet/id/' . $data ['pid'] . '">Portlet</a>'
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        // Zend_Debug::dump($result); die();
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }

    public function add_attrs($data) {
        //Zend_Debug::dump($data); die();
        $vatr = array();
        foreach ($data ['attrs'] as $v) {
            $vatr [$v] = true;
        }

        $var = array(
            'api_id' => implode(',', $data ['get_api']),
            'header' => $data ['header'],
            'attrs' => $vatr
            );
        $sql = "update zpraba_portlet set content_attrs=? where id =?";
        try {
            $this->_db->query($sql, array(
                serialize($var),
                $data ['id']
                ));
            $result = array(
                'result' => true,
                'message' => 'Success.  View  <a href="/prabagen/viewportlet/id/' . $data ['id'] . '">Portlet</a>',
                'id' => $data ['id']
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage(),
                );
        }
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }
     public function delete_app($data) {
        $sql = "delete from zpraba_app where id=?";
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'production');
        $config = $config->toArray();
        //Zend_Debug::dump($config); die();
        $connection = ssh2_connect($config['data']['server']['ip'], 22);
        
        if($connection){
        if(ssh2_auth_password($connection, $config['data']['server']['user'], $config['data']['server']['pwd'])){
                $stream = ssh2_exec($connection, "rm -fr ".APPLICATION_PATH . '/modules/'.$data['short']);
            }
        }   
        try {
            $this->_db->query($sql, $data['id']);
            $result = array(
                'result' => true,
                'message' => 'Page has been deleted.'
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            
        }
    
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
       
    }
    public function get_applications() {
        $cache = Zend_Registry::get('cache');
        $sql = "select * from zpraba_app";
        if (!$data = $cache->load('get_applications')) {
            try {
                $data = $this->_db->fetchAll($sql);

                $cache->save($data, 'get_applications', array(
                    'prabasystem'
                    ));
            } catch (Exception $e) {
                Zend_Debug::dump($e->getMessage());
                die($sql);
            }
        }
        return $data;
    }

    public function get_conns_serialize() {
        $cache = Zend_Registry::get('cache');

        if (!$data = $cache->load('zpraba_conn')) {
            $sql = "select * from zpraba_conn order by 1";
            try {
                $item = $this->_db->fetchAll($sql);
                foreach ($item as $k => $v) {
                    $data [$k] = $v;
                    $data [$k] ['conn'] = unserialize($v ['conn_params']);
                }

                $cache->save($data, 'zpraba_conn' . $uid, array(
                    'prabasystem'
                    ));
            } catch (Exception $e) {
                Zend_Debug::dump($e->getMessage());
                die($sql);
            }
        }
        return $data;
    }

    public function get_method_class_model($model) {
        $new_s = array();
        $class_methods = get_class_methods($model);
        foreach ($class_methods as $method_name) {
            $new_s [] = $method_name;
            if ($method_name == '__construct') {
                return $new_s;
            }
        }
    }

    public function get_method_class_model_operator($model) {
        $new_s = array();
        $class_methods = get_class_methods($model);


        foreach ($class_methods as $method_name) {
            if (substr($method_name, 0, 3) == 'pub') {
                $new_s [] = $method_name;
            }
            if ($method_name == '__construct') {
                return $new_s;
            }
        }
    }

    public function get_models_class() {
        $files = glob(APPLICATION_PATH . '/models/*.php');
        $files2 = glob(APPLICATION_PATH . '/modules/*/models/*.php');
        $var = array();
        $zar = array();
        $vvv = array();
        $nested = array();
        foreach ($files as $k => $f) {
            $var [$k] = explode('/', $f);
            $zar = explode('.php', end($var [$k]));
            $vvv [] = strtolower($zar [0]);
            $nested ['core'] = $vvv;
        }

        $var = array();
        $zar = array();
        $vvv2 = array();
        foreach ($files2 as $k => $f) {
            $var [$k] = explode('/', $f);
            $zar = explode('.php', end($var [$k]));
            $vvv2 [] = strtolower($zar [0]);
            $nested [$var [$k] [count($var [$k]) - 3]] [] = strtolower($zar [0]);
        }

        $model = array();
        foreach ($nested as $kx => $vx) {
            if ($kx != 'core') {
                foreach ($vx as $vz)
                    $model [$kx] [] = ucfirst($kx) . '_Model_' . ucfirst($vz);
            } else {
                foreach ($vx as $vz)
                    $model ['core'] [] = 'Model_' . ucfirst($vz);
            }
        }
        return $model;
    }

    public function get_models_class_operator() {
        $files = glob(APPLICATION_PATH . '/models/*.php');
        $files2 = glob(APPLICATION_PATH . '/modules/*/models/*.php');

        // Zend_Debug::dump($files );


        $var = array();
        $zar = array();
        $vvv = array();
        $nested = array();
        foreach ($files as $k => $f) {
            $var [$k] = explode('/', $f);
            $zar = explode('.php', end($var [$k]));
            //Zend_Debug::dump(substr($zar[0],0,3));
            if (substr($zar[0], 0, 3) == 'Pub') {
                $vvv [] = strtolower($zar [0]);
            }

            $nested ['core'] = $vvv;
        }

        $var = array();
        $zar = array();
        $vvv2 = array();
        foreach ($files2 as $k => $f) {
            $var [$k] = explode('/', $f);
            $zar = explode('.php', end($var [$k]));

            if (substr($zar[0], 0, 3) == 'Pub') {
                $vvv2 [] = strtolower($zar [0]);
                $nested [$var [$k] [count($var [$k]) - 3]] [] = strtolower($zar [0]);
            }
        }
        // Zend_Debug::dump($nested);
        //die();
        $model = array();
        foreach ($nested as $kx => $vx) {
            if ($kx != 'core') {
                foreach ($vx as $vz)
                    $model [$kx] [] = ucfirst($kx) . '_Model_' . ucfirst($vz);
            } else {
                foreach ($vx as $vz)
                    $model ['core'] [] = 'Model_' . ucfirst($vz);
            }
        }
        return $model;
    }

    public function get_modules() {
        $files2 = glob(APPLICATION_PATH . '/modules/*');
        $m = array();
        foreach ($files2 as $k => $v) {
            $Var [$k] = explode('/', $v);
            $m [] = end($Var [$k]);
        }

        $ret = array_merge($m, array(
            'default'
            ));
        return $ret;
    }

    public function connvert($link) {
        $var = explode('/', $link);

        $variables = (count($var) - 3) / 2;
        // echo $variables; die();
        $par = array();
        if ($variables >= 1) {
            for ($i = 1; $i <= $variables; $i ++) {
                $par [$var [2 * $i + 1]] = $var [2 * $i + 2];
            }
        }

        if (in_array($var [1], $this->get_modules())) {
            // $modules=$var[0];
            $ret = array(
                $var [1],
                $var [2],
                $var [3],
                $par
                );
        } else {
            $ret = array(
                'default',
                $var [1],
                $var [2],
                $par
                );
        }

        // Zend_Debug::dump($ret); die("zzzzzzz");
        return $ret;
    }

    public function listapiportletjson($params) {
        if (isset($params ['id']) && $params ['id'] [0] == "/") {

            return array(
                0 => array(
                    'id' => $params ['id'],
                    'portlet_name' => $params ['id']
                    )
                );
        }

        if ($params ['q'] [0] == "/") {

            return array(
                0 => array(
                    'id' => $params ['q'],
                    'portlet_name' => $params ['q']
                    )
                );
        }

        if (is_numeric($params ['q'])) {
            $sql = "select * from zpraba_portlet where id like '%" . $params ['q'] . "%' order by 1";
        } else {
            $sql = "select * from zpraba_portlet where portlet_name like '%" . $params ['q'] . "%' order by 1";
        }

        if (isset($params ['id'])) {
            $sql = "select * from zpraba_portlet where id =" . $params ['id'] . "";
        }
        try {

            $item = $this->_db->fetchAll($sql);

            return $item;
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($sql);
        }
    }

    public function listapijson() {
        $cache = Zend_Registry::get('cache');
        $sql = "select * from zpraba_api order by 1";

        if (!$item = $cache->load('listapijson')) {
            try {
                $item = $this->_db->fetchAll($sql);
                $cache->save($item, 'listapijson', array(
                    'prabasystem'
                    ));
            } catch (Exception $e) {
                Zend_Debug::dump($e->getMessage());
                die($sql);
            }
        }
        return $item;
    }

    public function listapiuser($data) {
        $aColumns = array(
            'id',
            'user_name',
            'user_api',
            'user_key',
            'user_desc'
            );

        $sLimit = "";
        if (isset($data ['iDisplayStart']) && $data ['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . intval($data ['iDisplayStart']) . ", " . intval($data ['iDisplayLength']);
        }

        /*
         * Ordering
         */
        // Zend_Debug::dump($data); die();

        if (isset($data ['iSortCol_0']) && intval($data ['iSortCol_0']) > 0) {
            $sOrder = "ORDER BY  ";
            for ($i = 1; $i < intval($data ['iSortingCols']); $i ++) {
            //for ( $i=1 ; $i<count($aColumns) ; $i++ )

                if ($data ['bSortable_' . intval($data ['iSortCol_' . $i])] == "true") {
                    $sOrder .= "" . $aColumns [intval($data ['iSortCol_' . $i])] . " " . ($data ['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
                }
            }

            $sOrder .= "" . $aColumns [intval($data ['iSortCol_0'])] . " " . ($data ['sSortDir_0'] === 'asc' ? 'asc' : 'desc') . ", ";
            // echo $sOrder.'<br>';

            $sOrder = substr_replace($sOrder, "", - 2);
            if ($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }

        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $sWhere = '';
        if (isset($data ['sSearch']) && $data ['sSearch'] != "") {
            $sWhere .= "WHERE (";

            for ($i = 1; $i < count($aColumns); $i ++) {
                $sWhere .= "" . $aColumns [$i] . " LIKE '%" . $data ['sSearch'] . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }

        // die($sWhere);
        /* Individual column filtering */
        for ($i = 1; $i < count($aColumns); $i ++) {
            if (isset($data ['bSearchable_' . $i]) && $data ['bSearchable_' . $i] == "true" && $data ['sSearch_' . $i] != '') {
                if ($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                $sWhere .= "" . $aColumns [$i] . " LIKE '%" . $data ['sSearch_' . $i] . "%' ";
            }
        }
        // echo $sWhere.'<br>';
        // echo $sOrder.'<br>';
        // echo $sLimit.'<br>';
        // die();
        $qry = "SELECT * FROM 
        zpraba_users_api " . $sWhere . " " . $sOrder . " " . $sLimit;
        // echo $qry; die();
        try {
            $data = $this->_db->fetchAll($qry);
            // Zend_Debug::dump($data);die();
            return $data;
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }

    public function listapi($data) {
        $aColumns = array(
            'no',
            'id',
            'conn_name',
            'sql_text',
            'api_desc',
            'api_type',
            'api_str_replace',
            'attrs1',
            'cache_time'
            );

        $sLimit = "";
        if (isset($data ['iDisplayStart']) && $data ['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . intval($data ['iDisplayStart']) . ", " . intval($data ['iDisplayLength']);
        }

        /*
         * Ordering
         */
        // Zend_Debug::dump($data); die();

        if (isset($data ['iSortCol_0']) && intval($data ['iSortCol_0']) > 0) {
            $sOrder = "ORDER BY  ";
            for ($i = 1; $i < intval($data ['iSortingCols']); $i ++) {
            //for ( $i=1 ; $i<count($aColumns) ; $i++ )

                if ($data ['bSortable_' . intval($data ['iSortCol_' . $i])] == "true") {
                    $sOrder .= "" . $aColumns [intval($data ['iSortCol_' . $i])] . " " . ($data ['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
                }
            }

            $sOrder .= "" . $aColumns [intval($data ['iSortCol_0'])] . " " . ($data ['sSortDir_0'] === 'asc' ? 'asc' : 'desc') . ", ";
            // echo $sOrder.'<br>';

            $sOrder = substr_replace($sOrder, "", - 2);
            if ($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }

        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $sWhere = '';
        if (isset($data ['sSearch']) && $data ['sSearch'] != "") {
            $sWhere .= "WHERE (";

            for ($i = 1; $i < count($aColumns); $i ++) {
                $sWhere .= "" . $aColumns [$i] . " LIKE '%" . $data ['sSearch'] . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }

        // die($sWhere);
        /* Individual column filtering */
        for ($i = 1; $i < count($aColumns); $i ++) {
            if (isset($data ['bSearchable_' . $i]) && $data ['bSearchable_' . $i] == "true" && $data ['sSearch_' . $i] != '') {
                if ($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                $sWhere .= "" . $aColumns [$i] . " LIKE '%" . $data ['sSearch_' . $i] . "%' ";
            }
        }
        // echo $sWhere.'<br>';
        // echo $sOrder.'<br>';
        // echo $sLimit.'<br>';
        // die();
        $qry = "SELECT * FROM 
        zpraba_api " . $sWhere . " " . $sOrder . " " . $sLimit;
        // echo $qry; die();
        try {
            $data = $this->_db->fetchAll($qry);
            // Zend_Debug::dump($data);die();
            return $data;
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }
	
	public function listapibigenvelope($data) {
        $aColumns = array(
            'no',
            'id',
            'conn_name',
            'sql_text',
            'api_desc',
            'api_type',
            'api_str_replace',
            'attrs1',
            'cache_time'
            );

        $sLimit = "";
		
		$getdatatable	= $_GET['datatable'];
		$page			= round($getdatatable['pagination']['page']);
		$perpages		= round($getdatatable['pagination']['perpage']);
		$pages			= ceil($iTotalRecords/$perpages);
		$sorttype		= $getdatatable['sort']['sort'];
		$sortby			= $getdatatable['sort']['field'];
		
		$iDisplayLength = intval($perpages);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart 	= intval(($page-1)*$perpages);
		$sSearch		= $getdatatable['query'];
		
		
        if(isset($iDisplayStart)&& $iDisplayLength != '-1') {
            $sLimit = "LIMIT " . intval($iDisplayStart). ", " . intval($iDisplayLength);
        }

        /*
         * Ordering
         */
        // Zend_Debug::dump($data); die();

        if(isset($sortby)) {
            $sOrder = "ORDER BY ".$sortby." ".$sorttype;
        } else {
			$sOrder = "";
		}

        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $sWhere = '';
        if(isset($sSearch['generalSearch'])&& $sSearch['generalSearch'] != "") {
            $sWhere .= "WHERE (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $sSearch['generalSearch'] . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        } else if(isset($sSearch['conn_name'])&& $sSearch['conn_name'] != "") {
            $sWhere .= "WHERE (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $sSearch['conn_name'] . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        } else if(isset($sSearch['id'])&& $sSearch['id'] != "") {
            $sWhere .= "WHERE (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $sSearch['id'] . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        } else if(isset($sSearch['sql_text'])&& $sSearch['sql_text'] != "") {
            $sWhere .= "WHERE (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $sSearch['sql_text'] . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        } else if(isset($sSearch['api_desc'])&& $sSearch['api_desc'] != "") {
            $sWhere .= "WHERE (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $sSearch['api_desc'] . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }

        // die($sWhere);
        /* Individual column filtering */
        // for ($i = 1; $i < count($aColumns); $i ++) {
            // if (isset($data ['bSearchable_' . $i]) && $data ['bSearchable_' . $i] == "true" && $data ['sSearch_' . $i] != '') {
                // if ($sWhere == "") {
                    // $sWhere = "WHERE ";
                // } else {
                    // $sWhere .= " AND ";
                // }
                // $sWhere .= "" . $aColumns [$i] . " LIKE '%" . $data ['sSearch_' . $i] . "%' ";
            // }
        // }
        // echo $sWhere.'<br>';
        // echo $sOrder.'<br>';
        // echo $sLimit.'<br>';
        // die();
        $qry = "SELECT * FROM 
        zpraba_api " . $sWhere . " " . $sOrder . " " . $sLimit;
        // echo $qry; die();
        try {
            $data = $this->_db->fetchAll($qry);
            // Zend_Debug::dump($data);die();
            return $data;
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }

    function count_listapiuser($data) {
        $aColumns = array(
            'id',
            'user_name',
            'user_api',
            'user_key',
            'user_desc'
            );

        $sLimit = "";
        if (isset($data ['iDisplayStart']) && $data ['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . intval($data ['iDisplayStart']) . ", " . intval($data ['iDisplayLength']);
        }

        /*
         * Ordering
         */
        $sOrder = "";
        if (isset($data ['iSortCol_0']) && intval($data ['iSortCol_0']) > 0) {
            $sOrder = "ORDER BY  ";
            for ($i = 1; $i < intval($data ['iSortingCols']); $i ++) {
            //for ( $i=1 ; $i<count($aColumns) ; $i++ )

                if ($data ['bSortable_' . intval($data ['iSortCol_' . $i])] == "true") {
                    $sOrder .= "" . $aColumns [intval($data ['iSortCol_' . $i])] . " " . ($data ['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
                }
            }

            $sOrder .= "" . $aColumns [intval($data ['iSortCol_0'])] . " " . ($data ['sSortDir_0'] === 'asc' ? 'asc' : 'desc') . ", ";
            // echo $sOrder.'<br>';

            $sOrder = substr_replace($sOrder, "", - 2);
            if ($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }

        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $sWhere = '';
        if (isset($data ['sSearch']) && $data ['sSearch'] != "") {
            $sWhere .= "WHERE (";

            for ($i = 1; $i < count($aColumns); $i ++) {
                $sWhere .= "" . $aColumns [$i] . " LIKE '%" . $data ['sSearch'] . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }
        // die($sWhere);
        /* Individual column filtering */
        for ($i = 1; $i < count($aColumns); $i ++) {
            if (isset($data ['bSearchable_' . $i]) && $data ['bSearchable_' . $i] == "true" && $data ['sSearch_' . $i] != '') {
                if ($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                $sWhere .= "" . $aColumns [$i] . " LIKE '%" . $data ['sSearch_' . $i] . "%' ";
            }
        }

        $qry = "SELECT 
        count(id) as row 
        FROM 
        zpraba_users_api " . $sWhere . " " . $sOrder;
        // die($qry);
        try {
            $data = $this->_db->fetchOne($qry);

            return $data;
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }

    function count_listapi($data) {
        $aColumns = array(
            'no',
            'id',
            'conn_name',
            'sql_text',
            'api_desc',
            'api_type',
            'api_str_replace',
            'attrs1',
            'cache_time'
            );

        $sLimit = "";
        if (isset($data ['iDisplayStart']) && $data ['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . intval($data ['iDisplayStart']) . ", " . intval($data ['iDisplayLength']);
        }

        /*
         * Ordering
         */
        $sOrder = "";
        if (isset($data ['iSortCol_0']) && intval($data ['iSortCol_0']) > 0) {
            $sOrder = "ORDER BY  ";
            for ($i = 1; $i < intval($data ['iSortingCols']); $i ++) {
            //for ( $i=1 ; $i<count($aColumns) ; $i++ )

                if ($data ['bSortable_' . intval($data ['iSortCol_' . $i])] == "true") {
                    $sOrder .= "" . $aColumns [intval($data ['iSortCol_' . $i])] . " " . ($data ['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
                }
            }

            $sOrder .= "" . $aColumns [intval($data ['iSortCol_0'])] . " " . ($data ['sSortDir_0'] === 'asc' ? 'asc' : 'desc') . ", ";
            // echo $sOrder.'<br>';

            $sOrder = substr_replace($sOrder, "", - 2);
            if ($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }

        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $sWhere = '';
        if (isset($data ['sSearch']) && $data ['sSearch'] != "") {
            $sWhere .= "WHERE (";

            for ($i = 1; $i < count($aColumns); $i ++) {
                $sWhere .= "" . $aColumns [$i] . " LIKE '%" . $data ['sSearch'] . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }
        // die($sWhere);
        /* Individual column filtering */
        for ($i = 1; $i < count($aColumns); $i ++) {
            if (isset($data ['bSearchable_' . $i]) && $data ['bSearchable_' . $i] == "true" && $data ['sSearch_' . $i] != '') {
                if ($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                $sWhere .= "" . $aColumns [$i] . " LIKE '%" . $data ['sSearch_' . $i] . "%' ";
            }
        }

        $qry = "SELECT 
        count(id) as row 
        FROM 
        zpraba_api " . $sWhere . " " . $sOrder;
        // die($qry);
        try {
            $data = $this->_db->fetchOne($qry);

            return $data;
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }
	
	function count_listapibigenvelope($data) {
        $aColumns = array(
            'no',
            'id',
            'conn_name',
            'sql_text',
            'api_desc',
            'api_type',
            'api_str_replace',
            'attrs1',
            'cache_time'
            );

        $sLimit = "";
        
		$getdatatable	= $_GET['datatable'];
		$page			= round($getdatatable['pagination']['page']);
		$perpages		= round($getdatatable['pagination']['perpage']);
		$pages			= ceil($iTotalRecords/$perpages);
		$sorttype		= $getdatatable['sort']['sort'];
		$sortby			= $getdatatable['sort']['field'];
		
		$iDisplayLength = intval($perpages);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart 	= intval(($page-1)*$perpages);
		$sSearch		= $getdatatable['query'];
		
		
        if(isset($iDisplayStart)&& $iDisplayLength != '-1') {
            $sLimit = "LIMIT " . intval($iDisplayStart). ", " . intval($iDisplayLength);
        }

        /*
         * Ordering
         */
        if(isset($sortby)) {
            $sOrder = "ORDER BY ".$sortby." ".$sorttype;
        } else {
			$sOrder = "";
		}

        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $sWhere = '';
        if(isset($sSearch['generalSearch'])&& $sSearch['generalSearch'] != "") {
            $sWhere .= "WHERE (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $sSearch['generalSearch'] . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        } else if(isset($sSearch['conn_name'])&& $sSearch['conn_name'] != "") {
            $sWhere .= "WHERE (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $sSearch['conn_name'] . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        } else if(isset($sSearch['id'])&& $sSearch['id'] != "") {
            $sWhere .= "WHERE (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $sSearch['id'] . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        } else if(isset($sSearch['sql_text'])&& $sSearch['sql_text'] != "") {
            $sWhere .= "WHERE (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $sSearch['sql_text'] . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        } else if(isset($sSearch['api_desc'])&& $sSearch['api_desc'] != "") {
            $sWhere .= "WHERE (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $sSearch['api_desc'] . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }

        $qry = "SELECT 
        count(id) as row 
        FROM 
        zpraba_api " . $sWhere . " " . $sOrder;
        // die($qry);
        try {
            $data = $this->_db->fetchOne($qry);

            return $data;
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }

    public function get_params($id) {
        $cache = Zend_Registry::get('cache');
        $sql = "select * from zpraba_params where param = ? order by value_param asc";

        if (!$item = $cache->load('get_params' . $id)) {
            try {
                $item = $this->_db->fetchAll($sql, $id);
                $cache->save($item, 'get_params' . $id, array(
                    'prabasystem'
                    ));
            } catch (Exception $e) {
                Zend_Debug::dump($e->getMessage());
                die($sql);
            }
        }
        return $item;
    }

    public function get_a_conn_by_name($id) {
        $cache = Zend_Registry::get('cache');
        $sql = "select * from zpraba_conn where conn_name=?";
        if (!$data = $cache->load('get_a_conn_by_name' . $id)) {

            try {
                $item = $this->_db->fetchRow($sql, $id);
                $data = $item;
                $data ['cparams'] = unserialize($item ['conn_params']);
                $cache->save($data, 'get_a_conn_by_name' . $id, array(
                    'prabasystem'
                    ));
            } catch (Exception $e) {
                Zend_Debug::dump($e->getMessage());
                die($sql);
            }
        }
        return $data;
    }

    public function get_a_conn($id) {
        $cache = Zend_Registry::get('cache');
        $sql = "select * from zpraba_conn where id=?";
        if (!$data = $cache->load('get_a_conn' . $id)) {

            try {
                $item = $this->_db->fetchRow($sql, $id);
                $data = $item;
                $data ['cparams'] = unserialize($item ['conn_params']);
                // Zend_Debug::dump($data); die();
                $cache->save($data, 'get_a_conn' . $id, array(
                    'prabasystem'
                    ));
            } catch (Exception $e) {
                Zend_Debug::dump($e->getMessage());
                die($sql);
            }
        }
        return $data;
    }

    public function get_a_tempsocket($id) {
        //$cache = Zend_Registry::get ( 'cache' );
        $sql = "select * from zpraba_template_websocket where id=?";
        //if (! $data = $cache->load ( 'get_a_app' . $id )) {

        try {
            $data = $this->_db->fetchRow($sql, $id);

            //$cache->save ( $data, 'get_a_app' . $id, array (
            //'prabasystem' 
            //) );
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($sql);
        }
        //}
        return $data;
    }

    public function get_a_action($id) {
        //$cache = Zend_Registry::get ( 'cache' );
        $sql = "select * from zpraba_action where id=?";
        //if (! $data = $cache->load ( 'get_a_app' . $id )) {

        try {
            $data = $this->_db->fetchRow($sql, $id);

            //$cache->save ( $data, 'get_a_app' . $id, array (
            //'prabasystem' 
            //) );
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($sql);
        }
        //}
        return $data;
    }

    public function get_a_app($id) {
        $cache = Zend_Registry::get('cache');
        $sql = "select * from zpraba_app where id=?";
        if (!$data = $cache->load('get_a_app' . $id)) {

            try {
                $data = $this->_db->fetchRow($sql, $id);

                $cache->save($data, 'get_a_app' . $id, array(
                    'prabasystem'
                    ));
            } catch (Exception $e) {
                Zend_Debug::dump($e->getMessage());
                die($sql);
            }
        }
        return $data;
    }

    public function get_conn($action = false) {
        $cache = Zend_Registry::get('cache');
        $sql = "select * from zpraba_conn";
        #$sql2 = "select CONCAT_WS('-', 'ACT', id) as id, action_name as conn_name,  'desc', 'conn_params', 'is_deactivate'  from zpraba_action where act_type in (1, 2) order by id ";

        $act = 0;
        if ($action) {
            $act = 1;
        }
        if (!$item = $cache->load('zpraba_conn' . $act)) {

            try {
                $item = $this->_db->fetchAll($sql);
                if ($action) {
                    #$item2 = $this->_db->fetchAll ( $sql2 );
                    $item2[0] = array(
                        'id' => 'ACTION',
                        'conn_name' => 'GET ACTION',
                        'conn_params' => '',
                        'desc' => '',
                        'is_deactivate' => '',
                        );
                    $item = array_merge($item, $item2);
                }

                $cache->save($item, 'zpraba_conn' . $act, array(
                    'prabasystem'
                    ));
            } catch (Exception $e) {
                Zend_Debug::dump($e->getMessage());
                die($sql);
            }
        }
        return $item;
    }

    public function get_apps() {
        $cache = Zend_Registry::get('cache');
        $sql = "select * from zpraba_app";

        if (!$var = $cache->load('get_apps')) {
            try {
                $item = $this->_db->fetchAll($sql);

                $var = array();
                foreach ($item as $v) {
                    $var [$v ['id']] = $v ['l_app'];
                }
                $cache->save($var, 'get_apps', array(
                    'prabasystem'
                    ));
            } catch (Exception $e) {
                Zend_Debug::dump($e->getMessage());
                die($sql);
            }
        }
        return $var;
    }

    public function get_workspace() {
        $cache = Zend_Registry::get('cache');
        $sql = "select * from zpraba_workspace";

        if (!$var = $cache->load('get_workspace')) {
            try {
                $item = $this->_db->fetchAll($sql);

                $var = array();
                foreach ($item as $v) {
                    $var [$v ['id']] = $v ['id'] . "--" . $v ['workspace_name'];
                }

                $cache->save($var, 'get_workspace', array(
                    'prabasystem'
                    ));
            } catch (Exception $e) {
                Zend_Debug::dump($e->getMessage());
                die($sql);
            }
        }
        return $var;
    }

    public function get_layout() {
        $cache = Zend_Registry::get('cache');
        $sql = "select * from zpraba_layout";

        if (!$var = $cache->load('get_layout')) {
            try {
                $item = $this->_db->fetchAll($sql);

                $var = array();
                foreach ($item as $v) {
                    $var [$v ['id']] = $v ['id'] . "--" . $v ['layout_name'];
                }
                $cache->save($var, 'get_layout', array(
                    'prabasystem'
                    ));
            } catch (Exception $e) {
                Zend_Debug::dump($e->getMessage());
                die($sql);
            }
        }
        return $var;
    }

    public function test_conn($data) {
        try {

            // die($data['descr']);
            if (isset($data ['descr']) && $data ['descr'] != "") {
                $vf = explode(";", $data ['descr']);

                foreach ($vf as $vz) {
                    $cf2 = explode("=", $vz);
                    $data [$cf2 [0]] = $cf2 [1];
                }
            }

            // Zend_Debug::dump($data); die();
            $adapter = $data ['adapter'];
            unset($data ['id']);
            unset($data ['pid']);
            unset($data ['conn']);
            unset($data ['descr']);
            unset($data ['adapter']);
            if ($adapter == 'Pdo_Netezza') {
                unset($data ['host']);
                unset($data ['port']);
            }

            if ($adapter == 'Pdo_Mssql') {
                $data ['pdoType'] = ' dblib';
            }

            $db = Zend_Db::factory($adapter, $data);
            $db->getConnection();
            $result = array(
                'result' => true,
                'message' => 'Connected'
                );
        } catch (Zend_Db_Adapter_Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
        } catch (Zend_Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
        }

        return $result;
    }

    public function update_applogin($data) {

        //Zend_Debug::dump($data); die();
        $sql = "update zpraba_app set extended_login=? where id=?";


        $var = array(
            serialize($data),
            $data['id']
            );

        try {
            $this->_db->query($sql, $var);
            $result = array(
                'result' => true,
                'message' => 'application has been updated.'
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        // Zend_Debug::dump($result); die();
        $cache = Zend_Registry::get('cache');
        // $cache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }

    public function add_applogin($data) {

        //Zend_Debug::dump($data); die();
        $sql = "update zpraba_app set extended_login=? where id=?";


        $var = array(
            serialize($data),
            $data['id']
            );

        try {
            $this->_db->query($sql, $var);
            $result = array(
                'result' => true,
                'message' => 'application has been updated.'
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        // Zend_Debug::dump($result); die();
        $cache = Zend_Registry::get('cache');
        // $cache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }

    public function update_app($data) {
        $sql = "update zpraba_app set l_app=?, app_desc=?, extended_css=?, extended_data=? where id=?";


        $var = array(
            $data ['long'],
            $data ['descr'],
            $data ['extended'],
            serialize($data),
            $data['id']
            );

        try {
            $this->_db->query($sql, $var);
            $result = array(
                'result' => true,
                'message' => 'application has been updated.'
                );


            $dd = new Model_System();
            $dd->generate_layout($data['id'], $data);
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        // Zend_Debug::dump($result); die();
        $cache = Zend_Registry::get('cache');
        // $cache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }

    public function add_app($data) {
        $sql = "insert into zpraba_app (s_app, l_app, app_desc, extended_css, extended_data) values (?, ?, ?, ?, ?)";

        $var = array(
            $data ['short'],
            $data ['long'],
            $data ['descr'],
            $data ['extended'],
            serialize($data),
            );

        try {
            $this->_db->query($sql, $var);

            $dd = new Model_System();
            $dd->generate_layout($this->_db->lastInsertId(), $data);

            $result = array(
                'result' => true,
                'message' => 'New application has been created.'
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        // Zend_Debug::dump($result); die();
        $cache = Zend_Registry::get('cache');
        // $cache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }

    public function add_conn($data) {
      //  Zend_Debug::dump($data); die();
        $sql = "insert into zpraba_conn (is_deactivate, conn_name, `desc`, conn_params) values (?, ?, ?, ?)";

        $br = array(
            'adapter' => $data ['adapter'],
            'host' => $data ['host'],
            'dbname' => $data ['dbname'],
            'port' => $data ['port'],
            'username' => $data ['username'],
            'password' => $data ['password']
            );
        $var = array(
            $data ['dis'],
            $data ['conn'],
            $data ['descr'],
            serialize($br)
            );

        try {
            $this->_db->query($sql, $var);
            $result = array(
                'result' => true,
                'message' => 'New Connection has been created.'
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        // Zend_Debug::dump($result); die();
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));

        return $result;
    }

    public function add_portlet($data) {
        // Zend_Debug::dump($data); die();
        $sql = "insert into zpraba_portlet (portlet_name, portlet_title, weight, portlet_type, portlet_attrs, custom_view, get_api) values (?, ?, ?, ?, ?, ?, ?)";

        for ($xx = 0; $xx < count($data ['act_label']); $xx ++) {

            $act [$xx] ['label'] = $data ['act_label'] [$xx];
            $act [$xx] ['class'] = $data ['act_class'] [$xx];
            $act [$xx] ['classicon'] = $data ['act_clsicon'] [$xx];
            $act [$xx] ['data-toggle'] = $data ['act_modal'] [$xx];
            $act [$xx] ['id'] = $data ['act_id'] [$xx];
            $act [$xx] ['data-target'] = $data ['act_target'] [$xx];
        }

        $br = array(
            'color' => $data ['color'],
            'type' => $data ['type'],
            'width' => $data ['width'],
            'tools' => $data ['tools'],
            'actions' => $act
            );

        $var = array(
            $data ['name'],
            $data ['title'],
            $data ['weight'],
            $data ['maintype'],
            serialize($br),
            $data ['customview'],
            implode(',', $data ['get_api'])
            );


        try {
            $this->_db->query($sql, $var);

            $result = array(
                'result' => true,
                'message' => 'New Portlet has been created.',
                'id' => $this->_db->lastInsertId()
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        // Zend_Debug::dump($result); die();
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }

    public function update_page($data, $br, $el) {
        // Zend_Debug::dump($el); die();
        $sql = "update zpraba_page set name_alias='" . $data ['pagealias'] . "', 	layout_id='" . $data ['layout'] . "',  workspace_id='" . $data ['workspace'] . "', app_id='" . $data ['app'] . "', breadcrumb='" . serialize($br) . "', title='" . $data ['title'] . "', element='" . serialize($el) . "' where id =" . $data ['id'] . " ";
        // die($sql);
        $var = array(
            $data ['pagealias'],
            $data ['layout'],
            $data ['workspace'],
            $data ['app'],
            serialize($br),
            $data ['title'],
            serialize($el),
            $data ['id']
            );

        try {
            $this->_db->query($sql, $var);
            $result = array(
                'result' => true,
                'message' => 'Page has been Updated.  <a href="/prabagen/index/page/' . $data ['id'] . '">View page</a>'
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        // Zend_Debug::dump($result); die();
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }

    public function update_action($data) {

        $in_array = explode(",", $data['in_array']);
        $sql = "update zpraba_action set action_name= ? , act_type= ?, act_descr=?, input_array=? where id =?";
        $var = array(
            $data ['action_name'],
            $data ['act_type'],
            $data ['zdescr'],
            serialize($in_array),
            $data ['id']);


        try {
            $this->_db->query($sql, $var);

            $result = array(
                'result' => true,
                'id' => $data ['id'],
                'message' => 'Action has been updated.'
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        // Zend_Debug::dump($result); die();
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }

    public function add_action($data) {

        if ($data['id'] != "") {
            return $this->update_action($data);
        }

        $sql = "insert into zpraba_action (action_name, act_type, act_descr, input_array) values (?, ?, ?, ?)";

        $in_array = explode(",", $data['in_array']);
        $var = array(
            $data ['action_name'],
            $data ['act_type'],
            $data ['zdescr'],
            serialize($in_array)
            );

        try {
            $this->_db->query($sql, $var);
            $id = $this->_db->lastInsertId();
            $start = $this->_db->query("insert into zpraba_process (act_id, class_type, offset_top, offset_left) values ($id, 'Start', 38, 49)");


            $end = $this->_db->query("insert into zpraba_process (act_id, class_type, offset_top, offset_left) values ($id, 'End', 324, 800)");
            $result = array(
                'result' => true,
                'id' => $id,
                'message' => 'Action has been created.'
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        // Zend_Debug::dump($result); die();
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }

    public function add_page($data, $br, $el) {
        $sql = "insert into zpraba_page (name_alias, 	layout_id,  workspace_id, app_id, breadcrumb, title, element) values (?, ?, ?, ?, ?, ?, ?)";

        $var = array(
            $data ['pagealias'],
            $data ['layout'],
            $data ['workspace'],
            $data ['app'],
            serialize($br),
            $data ['title'],
            serialize($el)
            );

        try {
            $this->_db->query($sql, $var);
            $id = $this->_db->lastInsertId();
            // => $this->_db->lastInsertId () 
            $result = array(
                'result' => true,
                'message' => 'Page has been created.View  <a href="/prabagen/index/page/' . $id . '">page</a>',
                'id' => $id
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        // Zend_Debug::dump($result); die();
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }

    public function add_apiuser($data) {

        //Zend_Debug::dump($data); die();
        $sql = "insert into zpraba_users_api (user_name, user_ip, user_key, user_desc) values (?, ?, ?, ?)";

        $var = array(
            $data ['uname'],
            $data ['ip'],
            md5($data ['ukey']),
            $data ['apidesc'],
            );

        try {
            $this->_db->query($sql, $var);

            $result = array(
                'result' => true,
                'message' => 'User for Api has been created.'
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        // Zend_Debug::dump($result); die();
        $cache = Zend_Registry::get('cache');
        // $cache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }

    public function add_api($data) {

        // Zend_Debug::dump($data); die();
        //$vconn = explode ("~", $data['conname']);

        $vconn = $this->get_conn($data['conname']);
        foreach ($vconn as $v) {
            $vconnArr[$v['id']] = $v['conn_name'];
        }

        $data['conname'];

        $sql = "insert into zpraba_api (conn_id, backendcache, conn_name, sql_text, api_desc, api_type, api_str_replace, attrs1, cache_time, api_mode, array_output, array_input) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        $fx = explode('~', $data['inputsqltext']);

        $input = array();
        foreach ($fx as $k => $v) {
            $jj[$k] = explode('=', $v);
            $input[] = $jj[$k][0];
        }


        $vinput = implode(",", $input);


        $var = array(
            $data['conname'],
            $data ['backendcache'],
            $vconnArr[$data['conname']],
            $data ['sqltext'],
            $data ['apidesc'],
            $data ['apitype'],
            $data ['str'],
            $data ['attrs1'],
            $data ['cache'],
            $data ['mode'],
            $data ['arrayout'],
            $vinput
            );

        try {
            $this->_db->query($sql, $var);
            $pid = $this->_db->lastInsertId();

            foreach ($data ['access_api'] as $k => $v) {
                ($data ['restricted_ip'] [$k] != "") ? $ip = $data ['restricted_ip'] [$k] : $ip = 0;
                ($data ['access_vsql'] [$k] != "") ? $vsql = $data ['access_vsql'] [$k] : $vsql = 0;
                ($data ['access_vparsing'] [$k] != "") ? $parsing = $data ['access_vparsing'] [$k] : $parsing = 0;

                $this->_db->query("insert into  zpraba_api_users(api_id, api_user_id, is_show_sql,is_restricted_ip,is_show_parsing_data) values (?, ?, ?, ?, ?)", array(
                    $pid,
                    $k,
                    $vsql,
                    $ip,
                    $parsing
                    ));
            }

            $result = array(
                'result' => true,
                'message' => 'Api has been created.',
				'api_id'=>$pid
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        // Zend_Debug::dump($result); die();
        $cache = Zend_Registry::get('cache');
        // $cache->clean(Zend_Cache::CLEANING_MODE_ALL);
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }

    public function delete_portlet($id) {
        $sql = "delete from zpraba_portlet where id=?";

        try {
            $this->_db->query($sql, $id);
            $result = array(
                'result' => true,
                'message' => 'Page has been deleted.'
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        // Zend_Debug::dump($result); die();
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }

    public function delete_page($id) {
        $sql = "delete from zpraba_page where id=?";

        try {
            $this->_db->query($sql, $id);
            $result = array(
                'result' => true,
                'message' => 'Page has been deleted.'
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        // Zend_Debug::dump($result); die();
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }

    public function delete_api_user($id) {
        $sql = "delete from zpraba_users_api where id=?";

        try {
            $this->_db->query($sql, $id);
            $result = array(
                'result' => true,
                'message' => 'User has been deleted.'
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        // Zend_Debug::dump($result); die();
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }

    public function delete_action($id) {
        $sql = "delete from zpraba_action where id=?";

        try {
            $this->_db->query($sql, $id);
            $result = array(
                'result' => true,
                'message' => 'Item has been deleted.'
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        // Zend_Debug::dump($result); die();
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }
	 public function delete_conn($id) {
        $sql = "delete from zpraba_conn where id=?";

        try {
            $this->_db->query($sql, $id);
            $result = array(
                'result' => true,
                'message' => 'Connection has been deleted.'
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        // Zend_Debug::dump($result); die();
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }

    public function delete_api($id) {
        $sql = "delete from zpraba_api where id=?";

        try {
            $this->_db->query($sql, $id);
            $result = array(
                'result' => true,
                'message' => 'Api has been deleted.'
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        // Zend_Debug::dump($result); die();
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }

    public function get_a_api_user($id) {
        $sql = "select * from zpraba_users_api where id = ?";
        //die($sql);
        $cache = Zend_Registry::get('cache');
        if (!$item = $cache->load('get_a_api_user' . $id)) {
            try {
                $item = $this->_db->fetchRow($sql, $id);




                $cache->save($item, 'get_a_api_user' . $id, array(
                    'prabasystem'
                    ));
            } catch (Exception $e) {
                Zend_Debug::dump($e->getMessage());
                die($sql);
            }
        }
        return $item;
    }

    public function get_a_api($id) {
        $sql = "select * from zpraba_api where id = ?";
        $cache = Zend_Registry::get('cache');
        if (!$data = $cache->load('get_a_api' . $id)) {
            try {
                $item = $this->_db->fetchRow($sql, $id);

                $api = $this->_db->fetchAll("select *  from zpraba_api_users where api_id =?", $id);
                foreach ($api as $v) {
                    $vapi [$v ['api_user_id']] ['access'] = 1;
                    $vapi [$v ['api_user_id']] ['view_sql'] = $v ['is_show_sql'];
                    $vapi [$v ['api_user_id']] ['view_parsing'] = $v ['is_show_parsing_data'];
                    $vapi [$v ['api_user_id']] ['restricted_ip'] = $v ['is_restricted_ip'];
                }

                $data = $item;
                $data ['grant'] = $vapi;
                $cache->save($data, 'get_a_api' . $id, array(
                    'prabasystem'
                    ));
            } catch (Exception $e) {
                Zend_Debug::dump($e->getMessage());
                die($sql);
            }
        }
        return $data;
    }

    public function update_api_user($data) {

        //Zend_Debug::dump($data); die();
        
        
        try {
           if(!empty($data ['ukey'])) {
        $sql = "update zpraba_users_api set user_name =?, user_ip=?, user_key=?, user_desc=? where id=?";
        $var = array(
            $data ['uname'],
            $data ['ip'],
            md5($data ['ukey']),
            $data ['apidesc'],
            $data ['id']
            );
               $this->_db->query($sql, $var);
        }else {
            $sql = "update zpraba_users_api set user_name =?, user_ip=?,  user_desc=? where id=?";
        $var = array(
            $data ['uname'],
            $data ['ip'],
            
            $data ['apidesc'],
            $data ['id']
            );
               $this->_db->query($sql, $var);
            
        }

         
            $result = array(
                'result' => true,
                'message' => 'User has been update.'
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
        }

        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }

    public function update_api($data) {


        //$vconn = explode ("~", $data['conname']);

        $vconn = $this->get_conn($data['conname']);
        foreach ($vconn as $v) {
            $vconnArr[$v['id']] = $v['conn_name'];
        }



        $sql = "update zpraba_api set conn_id = ?,  backendcache =?, conn_name=?, sql_text=?, api_desc=?, api_type=?, api_str_replace=?, attrs1=?, cache_time=?, api_mode=?, array_output=?, array_input=?  where id=?";
        // die($sql);



        $fx = explode('~', $data['inputsqltext']);

        $input = array();
        foreach ($fx as $k => $v) {
            $jj[$k] = explode('=', $v);
            $input[] = $jj[$k][0];
        }


        $vinput = implode(",", $input);
        $var = array(
            $data ['conname'],
            $data ['backendcache'],
            $vconnArr[$data ['conname']],
            $data ['sqltext'],
            $data ['apidesc'],
            $data ['apitype'],
            $data ['str'],
            $data ['attrs1'],
            $data ['cache'],
            $data ['mode'],
            $data ['arrayout'],
            $vinput,
            $data ['id']
            );

        try {
            $this->_db->query($sql, $var);
            $this->_db->query("delete from zpraba_api_users where api_id= ?", $data ['id']);
            foreach ($data ['access_api'] as $k => $v) {
                ($data ['restricted_ip'] [$k] != "") ? $ip = $data ['restricted_ip'] [$k] : $ip = 0;
                ($data ['access_vsql'] [$k] != "") ? $vsql = $data ['access_vsql'] [$k] : $vsql = 0;
                ($data ['access_vparsing'] [$k] != "") ? $parsing = $data ['access_vparsing'] [$k] : $parsing = 0;

                $this->_db->query("insert into  zpraba_api_users(api_id, api_user_id, is_show_sql,is_restricted_ip,is_show_parsing_data) values (?, ?, ?, ?, ?)", array(
                    $data ['id'],
                    $k,
                    $vsql,
                    $ip,
                    $parsing
                    ));
            }
            $result = array(
                'result' => true,
                'message' => 'Api has been update.'
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        // Zend_Debug::dump($result); die();
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }

    public function update_conn($data) {
        $sql = "update zpraba_conn set is_asession=?, is_deactivate=?, conn_name=?, conn_params=?, `desc`=?   where id=?";
        // die($sql);
        $br = array(
            'adapter' => $data ['adapter'],
            'host' => $data ['host'],
            'dbname' => $data ['dbname'],
            'port' => $data ['port'],
            'username' => $data ['username'],
            'password' => $data ['password']
            );
        try {
            $this->_db->query($sql, array(
                $data ['asession'],
                $data ['dis'],
                $data ['conn'],
                serialize($br),
                $data ['descr'],
                $data ['id']
                ));
            $result = array(
                'result' => true,
                'message' => 'Conn has been update.'
                );
        } catch (Exception $e) {
            $result = array(
                'result' => false,
                'message' => $e->getMessage()
                );
            // Zend_Debug::dump($e->getMessage());die($sql);
        }
        // Zend_Debug::dump($result); die();
        $cache = Zend_Registry::get('cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array(
            "prabasystem"
            ));
        return $result;
    }

}
