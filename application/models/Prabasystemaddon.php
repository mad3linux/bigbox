<?php
/*
 * Copyright (C) Prabtech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Prabasystemaddon extends Zend_Db_Table_Abstract {
	
	public function sinkronisasi_api_z() {
		$dd = new Model_Prabasystem ();
	
		$data = $this->_db->fetchAll("select * from zpraba_api where api_type=4");
			
		foreach ( $data as $v ) 
		{
			
			$this->_db->query("update zpraba_api set conn_id =  17 , conn_name = 'Php Class' where id= ".$v['id']." ");
		}
		
		return true;
		
	}	
	
	public function sinkronisasi_api() {
		$dd = new Model_Prabasystem ();
		$vdata = $dd->get_conn();
		
		$in=array();
		
		foreach ($vdata as $v ) {
			$in[$v['conn_name']] = $v['id'];	
		}
		
		$data = $this->_db->fetchAll("select distinct(conn_name)  as conn_name from zpraba_api");
		
		foreach ( $data as $v ) 
		{
			
			$this->_db->query("update zpraba_api set conn_id =  ".$in[$v['conn_name']]." where conn_name = '".$v['conn_name']."' ");
		}
		
		return true;
		
	}	
	
	public function add_file($data, $descr=null) {
		
		( $descr=="" ) ? $fde=serialize($data["options"]):$fde=$descr;
		
		#Zend_Debug::dump($data); die();
		$sql = "insert into zpraba_files (fname, fkey, fsize, raw_url, http_url, created_date, uid, ftype, fdescr, attr1) 
		values (?, ?, ?, ?, ?, now(), ?, ?, ?, ?)";
		$var = array (
				$data ['name'],
				$data ['fkey'],
				$data ['size'],
				$data ['destination'].'/'.$data ['name'],
				$_SERVER['HTTP_ORIGIN'].'/uploader/'.$data ['name'],
				$data ['uid'],
				$data ['type'],
				$fde,
				$data ['attr1'], 
		);
		
		try {
			$this->_db->query ( $sql, $var );
			//die( $sql); 
			$result = array (
					'id' => $this->_db->lastInsertId () ,
					'result' => true,
					'message' => 'New application has been created.' 
			);
		} catch ( Exception $e ) {
			$result = array (
					'id' => $this->_db->lastInsertId (),
					'result' => false,
					'message' => $e->getMessage () 
			);
			//Zend_Debug::dump($e->getMessage());die($sql);
		}
		// Zend_Debug::dump($result); die();
		$cache = Zend_Registry::get ( 'cache' );
		// $cache->clean(Zend_Cache::CLEANING_MODE_ALL);
		$cache->clean ( Zend_Cache::CLEANING_MODE_MATCHING_TAG, array (
				"zpraba_files" 
		) );
		return $result;
	}
	
	public function get_api_grouping() {
		$cache = Zend_Registry::get ( 'cache' );
		$sql = "select * from zpraba_api order by 1 ";
		$sql2 = "select * from zpraba_action where act_type in (1, 2) order by 1 ";
		
		
		if (! $item = $cache->load ( 'get_api_grouping' )) {
			try {
				$data = $this->_db->fetchAll ( $sql );
				$data2 = $this->_db->fetchAll ( $sql2 );
				
				
				$item = array ();
				foreach ( $data as $v ) {
					$item [$v ['conn_name']] [$v ['id']] = $v;
					$item [$v ['conn_name']] [$v ['id']] ['text'] = substr(str_replace("'", " ", $v ['sql_text']), 0, 50 );
				}
				foreach ($data2 as $vx ) {
					$item['ACTION']["act-".$vx['id']]['id'] ="act-".$vx['id']; 
					$item['ACTION']["act-".$vx['id']]['text']=$vx['action_name'];
				}
				//Zend_Debug::dump($item); die();
				
				$cache->save ( $item, 'get_api_grouping', array (
						'prabasystem' 
				) );
			} catch ( Exception $e ) {
				Zend_Debug::dump ( $e->getMessage () );
				die ( $sql );
			}
		}
		return $item;
	}
	
	public function get_all_user_json() {
	$aColumns = array (
				'no',
				'id',
				'user_name',
				'user_ip',
				'user_key',
				'user_desc' 
		);
		
		$sLimit = "";
		if (isset ( $data ['iDisplayStart'] ) && $data ['iDisplayLength'] != '-1') {
			$sLimit = "LIMIT " . intval ( $data ['iDisplayStart'] ) . ", " . intval ( $data ['iDisplayLength'] );
		}
		
		/*
		 * Ordering
		 */
		// Zend_Debug::dump($data); die();
		
		if (isset ( $data ['iSortCol_0'] ) && intval ( $data ['iSortCol_0'] ) > 0) {
			$sOrder = "ORDER BY  ";
			for($i = 1; $i < intval ( $data ['iSortingCols'] ); $i ++ )
			//for ( $i=1 ; $i<count($aColumns) ; $i++ )
			{

				if ($data ['bSortable_' . intval ( $data ['iSortCol_' . $i] )] == "true") {
					$sOrder .= "`" . $aColumns [intval ( $data ['iSortCol_' . $i] )] . "` " . ($data ['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
				}
			}
			
			$sOrder .= "`" . $aColumns [intval ( $data ['iSortCol_0'] )] . "` " . ($data ['sSortDir_0'] === 'asc' ? 'asc' : 'desc') . ", ";
			// echo $sOrder.'<br>';
			
			$sOrder = substr_replace ( $sOrder, "", - 2 );
			if ($sOrder == "ORDER BY") {
				$sOrder = "";
			}
		}
		
		/*
		 * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
		 */
		// $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
		$sWhere = '';
		if (isset ( $data ['sSearch'] ) && $data ['sSearch'] != "") {
			$sWhere .= "WHERE (";
			
			for($i = 1; $i < count ( $aColumns ); $i ++) {
				$sWhere .= "`" . $aColumns [$i] . "` LIKE '%" . $data ['sSearch'] . "%' OR ";
			}
			// die($sWhere);
			$sWhere = substr_replace ( $sWhere, "", - 3 );
			$sWhere .= ')';
		}
		
		// die($sWhere);
		/* Individual column filtering */
		for($i = 1; $i < count ( $aColumns ); $i ++) {
			if (isset ( $data ['bSearchable_' . $i] ) && $data ['bSearchable_' . $i] == "true" && $data ['sSearch_' . $i] != '') {
				if ($sWhere == "") {
					$sWhere = "WHERE ";
				} else {
					$sWhere .= " AND ";
				}
				$sWhere .= "`" . $aColumns [$i] . "` LIKE '%" . $data ['sSearch_' . $i] . "%' ";
			}
		}
		// echo $sWhere.'<br>';
		// echo $sOrder.'<br>';
		// echo $sLimit.'<br>';
		// die();
		$qry = "SELECT * FROM 
					`zpraba_users_api` " . $sWhere . " " . $sOrder . " " . $sLimit;
		// echo $qry; die();
		try {
			$data = $this->_db->fetchAll ( $qry );
			// Zend_Debug::dump($data);die();
			return $data;
		} catch ( Exception $e ) {
			Zend_Debug::dump ( $e->getMessage () );
			die ( $q );
		}
	}
	
	public function exec_action($data) {
			
		$sql  =  "select * from zpraba_process where act_id = ".(int)$data['id']." and step is not null order by step, pid" ;
		$sqc = "select max(step) as cnt from zpraba_process where act_id=".(int)$data['id'];
		
		
		//die($sql);
		try {
			$cnt  = $this->_db->fetchOne($sqc);
			$data  = $this->_db->fetchAll($sql);
		} catch(Exception $e) {
			Zend_Debug::dump ( $e->getMessage () );
		}
		foreach ($data as $v) {
			$var[$v['step']][$v['pid']]  = $v;
		}
		
		//Zend_Debug::dump($var); die();
		
		for($i=1;$i<=$cnt;$i++) {
			
			foreach ($var[$i] as $z) {
				$return[$i][$z['pid']] = $this->exec_item_action($z);
			}
				
		}
		
		
		Zend_Debug::dump($return); die();
		
	}
	
	public function exec_item_action($data, $input=array()) {
	
		$m1 = new Model_Zpraba4api ();
		$dd = new Model_Zprabapage ();
		
		//Zend_Debug::dump($data);// die();
		switch ($data['p_type']) {
			//call api
			case 1:
				
			$varC = $dd->get_api ( $data['ext_id'] );
			// Zend_Debug::dump($varC); die();
			$conn = unserialize ( $varC ['conn_params'] );
			$result = $m1->execute_api ( $conn, $varC, $input );
			//Zend_Debug::dump($result); die();
	
				
			break;
			
			//call url
			case 2:
			
			
			break;
			
			
			
			
		}
	
	return $result;
		
	} 

	public function get_steping_process($act_id) {
		
		$sql = "select * from zpraba_process where act_id=".(int)$act_id;
		$data = $this->_db->fetchAll($sql);
		//Zend_Debug::dump($data);
		foreach ( $data as $v ) {
			if($v['class_type']=='Start') {
				$start =  $v['pid'];	
			} elseif ($v['class_type']=='End') {
				$end  = $v['pid'];
				} 
			$idall[] = $v['pid'];
			$vdata[$v['pid']] = $v;
			
		}
		if (count($idall)>0) {
			$vall =  implode(',', $idall);
			$sqlx = "select * from zpraba_process_next where process_from in ($vall)";
			$datax = $this->_db->fetchAll($sqlx);
			//Zend_Debug::dump($datax); die();
		}	
		
		foreach ($datax as $v) {
			$vdatax[$v['process_from']][$v['id']] =  $v;
		}
		
		//Zend_Debug::dump($vdatax[$start]); die();
		if (count($vdatax[$start])>0) {
			
				$data =  $this->exec_process($vdatax[$start], $vdatax, $vdata, array()); 
			
		
		}
		
		
		die();
		
		
		return $item;
	}
	public function exec_process($v,  $vdatax, $vdata, $input=array(), $type=null) 
	{
		echo  "--------------------";
			Zend_Debug::dump($v);
			//Zend_Debug::dump($vdatax);
			//Zend_Debug::dump($vdata);
			//echo "tpe : ".$type;
			
		//die($type);
			if(count($v)>0){
			 foreach ($v as $vx) {
						$vz =  $vdatax[$vx['process_to']];
						//Zend_Debug::dump($vz);//die("asdasd");
						foreach ($vz as $zz) {
							$this->exec_process_detail($vz, $zz, $vdatax, $vdata); 
						}
				
				}
			}
			
	}
	
	public function exec_process_detail ($vz, $v, $vdatax, $vdata) {
			Zend_Debug::dump($v);
		
		
		//Zend_Debug::dump($vdata[$v['process_to']]['class_type']); die("oooooooooooo");
		
		switch ($vdata[$v['process_from']]['class_type']) {
			//call api
			case 'Task':
				$this->exec_type_task($vz, $v,  $vdatax, $vdata, $input);
			break;
		
			case 'Operator':
				$this->exec_type_operator($vz, $v,  $vdatax, $vdata, $input);
			break;
			
			case 'Start':
			case 'End':
				$this->exec_type_start_end($vz, $v,  $vdatax, $vdata, $input);
			break;
			
		}
		
		
	
		
	}
	
	

	public function exec_type_task($vz, $v,  $vdatax, $vdata) 
	{
		//	die("zzzz");
		
	
		//Zend_Debug::dump($v);die("ooo");
			///Zend_Debug::dump($vdatax);
			//Zend_Debug::dump($vdata);
		//die("zzzz");
		echo "<br>task";
		
			$m1 = new Model_Zpraba4api ();
			$dd = new Model_Zprabapage ();	
			$varC = $dd->get_api ( $datax['ext_id'] );
			//$conn = unserialize ( $varC ['conn_params'] );
			//$result = $m1->execute_api ( $conn, $varC, $input );
			//Zend_Debug::dump($result); die();
			$result=array();
			return  $this->exec_process($vz,  $vdatax, $vdata, $result); 
			
			
	}


	public function exec_type_start_end($vz, $v,  $vdatax, $vdata) 
	{
		
				echo "ending";	
			
		
			return $this->exec_process($vz, $vdatax, $vdata); 
			
			//return $result;
			
			
	}
	public function exec_type_operator($vz, $v,  $vdatax, $vdata) 
	{
		
		echo "<br>Operator";
			return $this->exec_process($vz, $vdatax, $vdata); 
			
	}
	
	
	public function get_all_front_end() 
	{
		
		//get all app
		$a = new Model_Prabasystem();
		$app = $a-> get_apps();
		unset($app[1]);
		//	Zend_Debug::dump($app); die();

		$page = $this->_db->fetchAll ("select * from zpraba_page order by app_id, id");
		$newpage=array();
		foreach ($page as $v) {
			$newpage[$v['app_id']][$v['id']] = $v;
			$zz =  unserialize($v['element']);	
			$zar =array();
			foreach ($zz as $v2) {
				
				foreach ($v2 as $v3) {
					if(is_numeric($v3)) {
						$zar[$v3]=$v3;
						$vz[] = $v3;		
					}
				}
			}
			$gt  = array_unique($zar);
			//$vz[] = $gt;
			$newpage[$v['app_id']][$v['id']]['portl'] = $gt;
			//$newpage[$v['id']]['portlets'] = $zz;
		}
		
		//Zend_Debug::dump($vz);die();
		$v5t   =  implode(',', array_unique($vz));
		//Zend_Debug::dump($v5t); die();
		
		$apiz = $this->_db->fetchAll("select * from zpraba_portlet where id in ($v5t)");
		$apizArr =  array();
		foreach  ($apiz as $k =>$v) 
		{
			$apizArr[$v['id']]=$v;
		}
		
		//Zend_Debug::dump($apizArr); die();
		foreach ($newpage as $k=>$v) {
			 $i=0;
			foreach ($v as $kk=> $vv) {
					
				foreach ( $vv['portl'] as $kkk=>$vvv) {
					$newchild0[$k][]['name']  = $apizArr[$kkk]['portlet_title']. ' (id = '.$kkk.')';
				}
				$chil[$k][$i]['name'] = $vv['title'].' (id ='.$vv['id'].')';
				$chil[$k][$i]['children'] = $newchild0[$k];	
			$i++;
			}
		}
		
		//return ($chil); //die();
		$i=0;
		foreach ($app as $k=>$v) {
				//Zend_Debug::dump($k); //die();
	
			$newC[$i]['name'] = $v;		
			$newC[$i]['children'] = $chil[$k];
			
		$i++;
		}
		
		//Zend_Debug::dump($newC); die();
		
		return array(
		'name'=>'Application',
		'children'=>$newC
		
		);
		
		
		
		
		Zend_Debug::dump($newpage); die();
		return $newpage;
			
	}
public function get_all_front_end_app($id) 
	{
		
		//get all app
		$a = new Model_Prabasystem();
		//$app = $a-> get_apps();
		//Zend_Debug::dump($app); die();
		$app = $this->_db->fetchRow("select * from zpraba_app where id = $id");
	//	Zend_Debug::dump($app); die();
		try{
				$page = $this->_db->fetchAll ("select * from zpraba_page  where app_id = $id");
			} catch (Exception $e) {
				Zend_Debug::dump($e); die();	
				
			}
		//		Zend_Debug::dump($page); die();	
		if(count($page)==0) {
		 return array();	
		}
		
		$newpage=array();
		foreach ($page as $v) {
			$newpage[$v['app_id']][$v['id']] = $v;
			$zz =  unserialize($v['element']);	
			$zar =array();
			foreach ($zz as $v2) {
				
				foreach ($v2 as $v3) {
					if(is_numeric($v3)) {
						$zar[$v3]=$v3;
						$vz[] = $v3;		
					}
				}
			}
			$gt  = array_unique($zar);
			//$vz[] = $gt;
			$newpage[$v['app_id']][$v['id']]['portl'] = $gt;
			//$newpage[$v['id']]['portlets'] = $zz;
		}
		
		//Zend_Debug::dump($vz);die();
		$v5t   =  implode(',', array_unique($vz));
		//Zend_Debug::dump($v5t); die();
		
		$apiz = $this->_db->fetchAll("select * from zpraba_portlet where id in ($v5t)");
		$apizArr =  array();
		foreach  ($apiz as $k =>$v) 
		{
			$apizArr[$v['id']]=$v;
		}
		
		//Zend_Debug::dump($apizArr); die();
		foreach ($newpage as $k=>$v) {
			 $i=0;
			foreach ($v as $kk=> $vv) {
					
				foreach ( $vv['portl'] as $kkk=>$vvv) {
					$newchild0[$k][]['name']  = $apizArr[$kkk]['portlet_title']. ' (id = '.$kkk.')';
				}
				$chil[$k][$i]['name'] = $vv['title'].' (id ='.$vv['id'].')';
				$chil[$k][$i]['children'] = $newchild0[$k];	
			$i++;
			}
		}
		
		return array(
		'name'=>$app['l_app'],
		'children'=>$chil[$id]
		
		);
		
		
		
		
		Zend_Debug::dump($newpage); die();
		return $newpage;
			
	}



}
