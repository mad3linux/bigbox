<?php
/*
 * Copyright (C) Prabtech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Prabasystemdriver extends Zend_Db_Table_Abstract {
	
	
	public function get_list_proc($data, $type="", $skema) 
	{
		switch ( $data['cparams']['adapter'] ) 
		{
				case 'Mysqli' :
				case 'Mysql' :
				case 'Pdo_mysql' :
				
					return $this->mysql_proc($data, $type, $skema);
			
				break;
				
				case 'Oracle' :
				case 'Pdo_oci' :
					return $this->oracle_proc($data, $type, $skema);
			
				break;
		}
	}
	
	
	//SELECT username FROM all_users ORDER BY username;
	
	function mysql_proc($data, $type, $skema) 
	{
			$dbz=  $this->get_new_conn ($data);	
			
			if($type!='user') {
				try {
					$sql  = "SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND ROUTINE_SCHEMA = ? "; 
						$res = $dbz->fetchAll ($sql, $skema) ;
						foreach ( $res as $v ) 
						{
							$vdat[] =  $v['SPECIFIC_NAME'];	
						}
						return $vdat;
					} catch (Exception $e) {
						return array();
					} 
				
			} else {
				
				try {
					$sql  = "SHOW DATABASES;"; 
					$res = $dbz->fetchAll ($sql, $data['cparams']['dbname']) ;
					//$stmt = $dbz->query ($sql, $data['cparams']['dbname']);
					//$res = $stmt->setFetchMode(Zend_Db::FETCH_NUM);
					foreach ( $res as $v ) 
						{
							$vdat[] =  $v['Database'];	
						}
					//Zend_Debug::dump($vdat); die();
						return $vdat;
					} catch (Exception $e) {
						return array();
					} 

				
			}	
	
	}


	function oracle_proc($data, $type, $skema) 
	{
			
			//Zend_Debug::dump($data);die();
			$dbz=  $this->get_new_conn ($data);	
			
			if($type!='user') {
				try {
					
					$vdat=array();
					$vdat0 = array();
					
					//get proc
					$sql  = "SELECT * FROM ALL_OBJECTS WHERE OBJECT_TYPE IN ('PROCEDURE') and owner ='$skema'"; 
					$res = $dbz->fetchAll ($sql) ;
					foreach ($res as $z) {
						$vdat0[] = $z['OBJECT_NAME'];
						
					}
					
					
					//get proc
					$sql2  = "SELECT * FROM ALL_OBJECTS WHERE OBJECT_TYPE IN ('PACKAGE') and owner ='$skema'"; 
					$res2 = $dbz->fetchAll ($sql2) ;
				
					foreach ($res2 as $v ) 
					{
						$sqlx = "SELECT PROCEDURE_NAME FROM ALL_PROCEDURES WHERE  OBJECT_NAME = '".$v['OBJECT_NAME']."'";
						$resx = $dbz->fetchAll ($sqlx) ;
							//Zend_Debug::dump($resx); die();
						foreach ($resx as $vx) {
						if($vx['PROCEDURE_NAME']!="") {	
						$vdat[] =	$v['OBJECT_NAME'] .".". $vx['PROCEDURE_NAME'];
						}
						}
					}
					//Zend_Debug::dump(array_merge($vdat, $vdat0));die();
					
					
						return array_merge($vdat, $vdat0);
					} catch (Exception $e) {
						return array();
					} 
				
			} else {
				
				try {
					$sql  = "SELECT USERNAME FROM ALL_USERS ORDER BY USERNAME"; 
					$res = $dbz->fetchAll ($sql) ;
					//$stmt = $dbz->query ($sql, $data['cparams']['dbname']);
					//$res = $stmt->setFetchMode(Zend_Db::FETCH_NUM);
					foreach ( $res as $v ) 
						{
							$vdat[] =  $v['USERNAME'];	
						}
					//Zend_Debug::dump($vdat); die();
						return $vdat;
					} catch (Exception $e) {
						return array();
					} 

				
			}	
	
	}


	function get_new_conn ($data) 
	{
	
		$conn = $data['cparams'];
		if (strtolower ( $conn ['adapter'] ) == 'mysql' || strtolower ( $conn ['adapter'] ) == 'mysqli' || strtolower ( $conn ['adapter'] ) == 'pdo_mysql' || strtolower ( $conn ['adapter'] ) == 'pdo_sqlite') {
			$adapterdb = "msyql";
		} elseif (strtolower ( $conn ['adapter'] ) == 'oracle' || strtolower ( $conn ['adapter'] ) == 'pdo_oci') {
			$adapterdb = "oracle";
		} elseif (strtolower ( $conn ['adapter'] ) == 'pdo_pgsql') {
			$adapterdb = "postgresql";
		} 

		else {
			// return array("data"=>"", "transaction"=>false, "message"=>"adapter db not yet supported", 'parse_time'=>"");
		}
		
		if (isset ( $data ['desc'] ) && $data ['desc'] != "") {
			$vf = explode ( ";", $data ['desc'] );
			foreach ( $vf as $vz ) {
				$cf2 = explode ( "=", $vz );
				$conn [$cf2 [0]] = $cf2 [1];
			}
		}
		$adapter = $conn ['adapter'];
		unset ( $data ['desc'] );
		unset ( $conn ['adapter'] );
		if ($adapter == 'Pdo_Netezza') {
			unset ( $conn ['host'] );
			unset ( $conn ['port'] );
		}
		
		if ($adapter == 'Pdo_Mssql') {
			$conn ['pdoType'] = ' dblib';
		}
		
		try {
			return Zend_Db::factory ( $adapter, $conn );
		} catch (Exception $e) {
			
			Zend_Debug::dump($e->getMessage());die();
			
		}
	}

}

