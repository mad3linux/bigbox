<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Socket extends Zend_Db_Table_Abstract{

//$rediskey = array(
		//'webdomain'=>'INSTANT_OFFERING:HIT:WEBDOMAIN',
		//'webcategory'=>'INSTANT_OFFERING:HIT:WEBCATEGORY',
		//'speedy'=>'INSTANT_OFFERING:HIT:SPEEDY',
		//'f5webdomain'=>'INSTANT_OFFERING:F5:HIT:WEBDOMAIN',
		//'f5webcategory'=>'INSTANT_OFFERING:F5:HIT:WEBCATEGORY',
		//'f5speedy'=>'INSTANT_OFFERING:F5:HIT:SPEEDY',
		//'cfswebdomain'=>'INSTANT_OFFERING:CFS:HIT:WEBDOMAIN',
		//'cfswebcategory'=>'INSTANT_OFFERING:CFS:HIT:WEBCATEGORY',
		//'cfsspeedy'=>'INSTANT_OFFERING:CFS:HIT:SPEEDY'
	//);

public function getRedis2() {
	 $rediskey = array(
		'webdomain'=>'INSTANT_OFFERING:HIT:WEBDOMAIN',
		'webcategory'=>'INSTANT_OFFERING:HIT:WEBCATEGORY',
		'speedy'=>'INSTANT_OFFERING:HIT:SPEEDY',
		'f5webdomain'=>'INSTANT_OFFERING:F5:HIT:WEBDOMAIN',
		'f5webcategory'=>'INSTANT_OFFERING:F5:HIT:WEBCATEGORY',
		'f5speedy'=>'INSTANT_OFFERING:F5:HIT:SPEEDY',
		'cfswebdomain'=>'INSTANT_OFFERING:CFS:HIT:WEBDOMAIN',
		'cfswebcategory'=>'INSTANT_OFFERING:CFS:HIT:WEBCATEGORY',
		'cfsspeedy'=>'INSTANT_OFFERING:CFS:HIT:SPEEDY'
	);
	
	$redis = new redisent\Redis('radis://radisTelkom:zaKNGP8P6B8bj8gFCbXzwk5RDeBtuq8H@10.62.29.105:6379');
	$now = date("w");
		
		
		
	$redis->select($now);
	
	$speedyhit = $redis->zrevrange('INSTANT_OFFERING:HIT:SPEEDY', 0, 5, 'withscores');
	
	//Zend_Debug::dump($speedyhit); die();
	foreach ($speedyhit as $k=>$v ) 
	{
		if($k% 2 == 0) {
			$key_speedyhit[] = $v;
		} else 	 {
			$val_speedyhit[] =$v;
		}
	}


	$arr_speedyhit = array(
		"tuple"=>array('val'=>$key_speedyhit, 'score'=>$val_speedyhit, "data"=>"speedyhit"));

	
	$webdomainhit = $redis->zrevrange('INSTANT_OFFERING:HIT:WEBDOMAIN', 0, 5, 'withscores');
	foreach ($webdomainhit as $k=>$v ) 
	{
		if($k% 2 == 0) {
			$key_webdomainhit[] = $v;
		} else 	 {
			$val_webdomainhit[] =$v;
		}
	}
	
	$arr_webdomainhit = array(
		"tuple"=>array('val'=>$key_webdomainhit, 'score'=>$val_webdomainhit, "data"=>"webdomainhit"));
		
	


	$webcategoryhit = $redis->zrevrange('INSTANT_OFFERING:HIT:WEBCATEGORY', 0, 5, 'withscores');
	foreach ($webcategoryhit as $k=>$v ) 
	{
		if($k% 2 == 0) {
			$key_webcategoryhit[] = $v;
		} else 	 {
			$val_webcategoryhit[] =$v;
		}
	}
	
	$arr_webcategoryhit = array(
		"tuple"=>array('val'=>$key_webcategoryhit, 'score'=>$val_webcategoryhit, "data"=>"webcategoryhit"));
		

	
	$array=array($arr_speedyhit, $arr_webdomainhit, $arr_webcategoryhit);
	$zzz = array_rand($array);
	//Zend_Debug::dump($zzz); die();
	$yyy = $array[$zzz];
	
	$data = array('tuples'=>$arr_webdomainhit	);	

	
	return array (
	"data" => $data,
	"transaction" => 'true',
	"message" => 'success'
	
	);
	
	
	}

public function getRedis($input) {
		//return true;
			//Zend_Debug::dump($input); die();
			$redis = new Redis();
			$redis->connect('10.62.29.105', 6379, 60);
			$redis->auth("zaKNGP8P6B8bj8gFCbXzwk5RDeBtuq8H");
			// $this->stdout(date('w'));
			$redis->select(date('w'));
			// $this->stdout('INSTANT_OFFERING:HIT:WEBDOMAIN');
			foreach($input['loop'] as $idx=>$key){
				$red = $redis->zRevRangeByScore($key, '+inf', '-inf', array('withscores' => TRUE,'limit' => array(0, 5)));
				// $this->stdout(count($red));
				$data = array();
				$i = count($red);
				foreach($red as $k=>$v){
					$data[$idx.$i] = array($k,$v);
					$i--;
				}
				$result = array("data" => array('data'=>$data, "dtime"=>date("Y-m-d H:i:s")), "transaction"=>true, "message"=>"succed");
				//$this->send($user,json_encode($result,true));
				//var_dump($result); die();
				return $result;
			}
		
			
	
}

function get_running_pid($id)
	{
		$content = shell_exec(" ps -ef | grep -sw 'id4socket=".$id."' | grep -v grep | awk '{print $2}'");
		#$content = str_replace("\n", "", $content); 
		#$content = str_replace("\r", "", $content);
		//Zend_Debug::dump($content); die();
		return $content;
	}	
	
public function running_socket ($data)  {
		
		//Zend_Debug::dump($data); die();	
		$cache = Zend_Registry::get ( 'cache' );
		
		//$this->stop_socket($data['id']);
		$attr = base64_encode(serialize(array('port'=>$data['port'], 'host'=>$data['host'], 'arrayinput'=>$data['inputsqltext'])));
		$exe ="nohup   /usr/bin/php ". APPLICATION_PATH."/../bin/cli.php socketserver run id4socket=".$data['id']." port=".$data['port']." host=".$data['host']."  data='".$attr."'> /dev/null 2>&1 & ";
		//echo $exe; die();
		shell_exec($exe);
		$pid = $this->get_running_pid($data['id']);
		//echo $pid;die();
		$this->_db->query("update zpraba_api set socket_attrs=?, is_running_socket=?, running_pid=?  where id=? ", array($attr, 1, $pid, $data['id']));
		
		$cache->clean ( Zend_Cache::CLEANING_MODE_MATCHING_TAG, array ("prabasystem" ) );
		
		return true;
}	

public function stop_socket ($data)  {
				try {
				
				$cache = Zend_Registry::get ( 'cache' );
				//$attr = serialize(array('port'=>$data['port'], 'host'=>$data['host']));
				$attr = base64_encode(serialize(array('port'=>$data['port'], 'host'=>$data['host'], 'arrayinput'=>$data['inputsqltext'])));
				$pid = $this->get_running_pid($data['id']);
				//echo $pid; die();
				shell_exec('kill -9  '.$pid.'');	
			
				$this->_db->query("update zpraba_api set socket_attrs=?, is_running_socket=?, running_pid=? where id=? ", array($attr, 0, "", $data['id']));
				$cache->clean ( Zend_Cache::CLEANING_MODE_MATCHING_TAG, array ("prabasystem" ) );
				
				} catch (Exception $e) {
					Zend_Debug::dump($e); die();
					
				}
				return true;
			
			
}



public function check_stop_running_socket ($data)  {
				try {
					
				$var =  $this->_db->fetchRow("select * from  zpraba_api  where id=? ", $data['id']);	
				
				$cache = Zend_Registry::get ( 'cache' );
				$attr = serialize(array('port'=>$data['port'], 'host'=>$data['host']));
				
				$pid = $this->get_running_pid($data['id']);
				//echo $pid; die();
				shell_exec('kill -9  '.$pid.'');	
				
				$this->_db->query("update zpraba_api set socket_attrs=?, is_running_socket=?, running_pid=? where id=? ", array($attr, 0, "", $data['id']));
				$cache->clean ( Zend_Cache::CLEANING_MODE_MATCHING_TAG, array ("prabasystem" ) );
				
				} catch (Exception $e) {
					Zend_Debug::dump($e); die();
					
				}
				return true;
			
			
}

}
