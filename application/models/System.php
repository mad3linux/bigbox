<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_System extends Zend_Db_Table_Abstract {
	
	
	
	public function get_session_tableau($tipe="development", $link=null, $view=null, $user=null, $pwd=null) {
		
		$c = new CMS_General();
		
	if($tipe=='development') {
			
			($link) ? $href = $link:$href="https://10.62.175.37/tableau/prabac.php";
			
			if($view!=null||$user!=null||$pwd!=null) {
				$varam =array(
					'usename'=>$user,
					'password'=>$password,
					'view'=>$view,
					
				);	
				$href=$href."?xin=".$c->__serialize($varam);
			}
			
			
			curl_setopt($ch, CURLOPT_URL, $href);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 40);
			
			$output = curl_exec($ch);
			//echo $output;die();
			curl_close($ch);	
			die("ok");

	}	else if ($tipe=='production') {
			
			($link) ? $href = $link:$href="https://smartanalytics.telkom.co.id:440/performance/prabac.php";
			
			if($view!=null||$user!=null||$pwd!=null) {
				$varam =array(
					'usename'=>$user,
					'password'=>$password,
					'view'=>$view,
					
				);	
				$href=$href."?xin=".$c->__serialize($varam);
			}
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $href);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 40);
		 
			$output = curl_exec($ch);
			//echo $output;die();
			curl_close($ch);	
		
			die("ok");
		
	}
		
		
		
	}
	
	public function generate_layout($id, $ar) {
		$theme_c = Zend_Registry::get('theme'); 
		$data = $this->_db->fetchRow("select * from zpraba_app where id =?", $id);
		
		try {
			$vdata = unserialize($data['extended_data']);
			
			$copy =  ( APPLICATION_PATH . '/../unprod/'.$theme_c.'/PRABAMODULE/views/layouts/templayout.phtml' );
			$target =  ( APPLICATION_PATH . '/modules/'.$data['s_app']."/views/layouts/templayout.phtml" );
			
			$str=file_get_contents($copy);
			$old= array('theme-1.phtml');
			$cthe= explode(".", $vdata['themes']);
			$new= array(''.$cthe[0].'.phtml');
			$str = str_replace($old, $new,  $str);
			$str = str_replace('"', '\"',  $str);
			$str = str_replace('$', '\$',  $str);
		
			$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'production');
			$config = $config->toArray();
		
			$connection = ssh2_connect($config['data']['server']['ip'], $config['data']['server']['port']);

			if($connection){
			if(ssh2_auth_password($connection, $config['data']['server']['user'], $config['data']['server']['pwd'])){
				$stream = ssh2_exec($connection, 'echo  "'.$str.'" >  '.$target);

				}
			}	
			
			
			return true;
		} catch (Exception $e ) {
			Zend_Debug::dump($e); die();
		}
		
		
	}
	
	public function generate_themes($id) {
		$theme_c = Zend_Registry::get('theme'); 
		//$files =  ( APPLICATION_PATH . '/../unprod/' );
		$data = $this->_db->fetchRow("select * from zpraba_app where id =?", $id);
		
		try {
			$vdata = unserialize($data['extended_login']);
			
		
			$target =  ( APPLICATION_PATH . '/modules/'.$data['s_app']."/views/scripts/public/login.phtml" );
			$str=file_get_contents($target);
		
			$old= array('_SIGNIN_', '_USERNAME_', '_PASSWORD_', '_APP_DESCRIPTION_', '_FOOTER_', '_LOGIN_');
			$new= array($vdata['signin'], $vdata['uname'], $vdata['passlabel'], $data['app_desc'], $vdata['footer'], $vdata['loginlabel']);
			$str = str_replace($old, $new,  $str);
			
			//echo $str; die();
			//file_put_contents($target, $str);
			
			return true;
		} catch (Exception $e ) {
			Zend_Debug::dump($e); die();
		}
		
		
	}
	
	public function generate_modules($name, $ar) {
		
		
		try {
		$theme_c = Zend_Registry::get('theme'); 
		
 		$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'production');
        $config = $config->toArray();
     
        $connection = ssh2_connect($config['data']['server']['ip'], $config['data']['server']['port']);
		if($connection){
		if(ssh2_auth_password($connection, $config['data']['server']['user'], $config['data']['server']['pwd'])){
				$stream = ssh2_exec($connection, "mkdir  ".APPLICATION_PATH . '/modules/'.$name);
			
			}
		}	
		

		$copy =  ( APPLICATION_PATH . '/../unprod/'.$theme_c.'/PRABAMODULE' );
		$target =  ( APPLICATION_PATH . '/modules/'.$name );
		$this->copy_fold($copy, $target);
		
		
		$connection = ssh2_connect($config['data']['server']['ip'], $config['data']['server']['port']);
		if($connection){
		if(ssh2_auth_password($connection, $config['data']['server']['user'], $config['data']['server']['pwd'])){
				$stream = ssh2_exec($connection, 'sed -i "s/class PRABAMODULE/class '.ucfirst($name).'/g" '.$target.'/* ');
				$stream = ssh2_exec($connection, 'sed -i "s/class PRABAMODULE/class '.ucfirst($name).'/g" '.$target.'/controllers/* ');

			}
		}	
	
		return true;
		} catch (Exception $e ) {
			Zend_Debug::dump($e); die();
		}
		
		
	}
	
	function copy_fold($source, $dest){
		$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'production');
        $config = $config->toArray();
        //Zend_Debug::dump($config); die();
      
    	if(is_dir($source)) {
        $dir_handle=opendir($source);
      //  $files=readdir($dir_handle);
       // Zend_Debug::dump($files); die();
        while($file=readdir($dir_handle)){
            if($file!="." && $file!=".."){
                if(is_dir($source."/".$file)){
                    if(!is_dir($dest."/".$file)){
                    	//die("xxx");
                     	//  echo "mkdir".$dest."/".$file."";//die();
                     		  $connection = ssh2_connect($config['data']['server']['ip'], $config['data']['server']['port']);	
                    		if(ssh2_auth_password($connection, $config['data']['server']['user'], $config['data']['server']['pwd'])){
                    			
                    			
								$stream = ssh2_exec($connection, "mkdir ".$dest."/".$file);
							}

                    }
                    $this->copy_fold($source."/".$file, $dest."/".$file);
                } else {

                   // copy($source."		/".$file, $dest."/".$file);
                		  $connection = ssh2_connect($config['data']['server']['ip'], $config['data']['server']['port']);	
                		if(ssh2_auth_password($connection, $config['data']['server']['user'], $config['data']['server']['pwd'])){
                				
								$stream = ssh2_exec($connection, "cp ".$source."/".$file." ".$dest."/".$file);
							}
                }
            }
        }
        closedir($dir_handle);
    } else {
       // copy($source, $dest);
    	  $connection = ssh2_connect($config['data']['server']['ip'], $config['data']['server']['port']);	
    	if(ssh2_auth_password($connection, $config['data']['server']['user'], $config['data']['server']['pwd'])){
                			$stream = ssh2_exec($connection, "cp ".$source ." ". $dest);
							}
    }
	}
	
	public function get_themes() {
		$files = scandir ( APPLICATION_PATH . '/../public/images/themes/' );
		$new = array();
		foreach ($files as $v) {
			if(strlen($v)>2) {		
				$new[$v] = $v;
			}
		}
		
		return $new; 
	}
	
	public function get_app_modules() {
		$files = scandir ( APPLICATION_PATH . '/modules/' );
		unset($files[0]);
		$files[1]='core';
		return $files; 
	}
	
	public function get_controllers($is_nested = null) {
		$files = glob ( APPLICATION_PATH . '/controllers/*Controller.php' );
		// Zend_Debug::dump($files); die();
		$files2 = glob ( APPLICATION_PATH . '/modules/*/controllers/*Controller.php' );
		// Zend_Debug::dump($files2); die();
		$var = array ();
		$zar = array ();
		$vvv = array ();
		$nested = array ();
		foreach ( $files as $k => $f ) {
			$var [$k] = explode ( '/', $f );
			$zar = explode ( 'Controller.php', end ( $var [$k] ) );
			$vvv [] = strtolower ( $zar [0] );
			$nested ['core'] = $vvv;
		}
		
		$var = array ();
		$zar = array ();
		$vvv2 = array ();
		foreach ( $files2 as $k => $f ) {
			$var [$k] = explode ( '/', $f );
			$key  = count ( $var [$k]);
			$zar = explode ( 'Controller.php', end ( $var [$k] ) );
			$vvv2 [] = strtolower ( $zar [0] );
			//Zend_Debug::dump($key);die();
			$nested [$var [$k] [($key-3)]] [] = strtolower ( $zar [0] );
			
		}
		
		if ($is_nested) {
			return ($nested);
		}
		
		
		$ret = array_unique ( array_merge ( $vvv, $vvv2 ) );
		return $ret;
	}

	public function get_apps_bigbox(){

		try {

			$sql = "SELECT * FROM z_groups ".
			"WHERE 1=1 ".
			"AND SUBSTRING(group_name,1,3)='Big' ".
			"AND SUBSTRING(group_name,-4)='Role'";

			$data = $this->_db->fetchAll($sql);

			return $data;
			
		} catch (Exception $e) {
			Zend_Debug::dump($e->getMessage());die();
		}
	}

	public function get_menus_bigbox(){

		try {

			$sql = "SELECT a.*, c.group_name, c.`desc`, b.menu_name, b.menu_link, b.class ".
			"FROM z_menus_groups a ".
			"left join z_menus b on a.menu_id=b.id ".
			"left join z_groups c on a.group_id=c.gid ".
			"where 1=1 ".
			"and a.group_id in (46,47,48,49,50,51,52,53) ".
			"order by a.group_id, b.weight";

			$data = $this->_db->fetchAll($sql);

			return $data;
			
		} catch (Exception $e) {
			Zend_Debug::dump($e->getMessage());die();
		}
	}
	
	public function get_roles($wf=false) {
		$sql = "select * from z_groups";
		try {
			
			
			if($wf) {
				$sql = "select gid, group_name from z_groups union select 'initiator'  as gid,  'initiator'  as group_name  union select 'assignee'  as gid,  'assignee'  as group_name";
			
			}
			$items = $this->_db->fetchAll ( $sql );
		} catch ( Exception $e ) {
			Zend_Debug::dump ( $e->getMessage());
			die ( $q );
		}
		return $items;
	}
	
	public function get_permissions_by_uid($uid) {
		$sql = "select a.*  from z_groups a inner join z_users_groups b on a.gid=b.gid where b.uid =$uid";
		// die($sql);
		try {
			$items = $this->_db->fetchAll ( $sql );
		} catch ( Exception $e ) {
			Zend_Debug::dump ( $e->getMessage () );
			die ( $q );
		}
		return $items;
	}
	
	public function get_roles_by_uid($id) {
		// die($id);
		$sql = "select * from z_groups a inner join z_users_groups b on a.gid = b.gid where b.uid =?";
		try {
			$items = $this->_db->fetchAll ( $sql, $id );
		} catch ( Exception $e ) {
			Zend_Debug::dump ( $e->getMessage () );
			die ( $q );
		}
		return $items;
	}
	
	public function update_log($uname) {
		$qry = "insert into  h_log_user(nik, tanggal) values(?,now())";
		$qry2 = "insert into  c_log_user(nik, tanggal) values(?,now())";
		try {
			$this->_db->query ( $qry, $uname );
			$this->_db->query ( $qry2, $uname );
		} catch ( Exception $e ) {
			Zend_Debug::dump ( $e );
			die ( $qry );
		}
	}
	
	function get_menus_by_app($id = null) {
		$where = "";
		if ($id) {
			$where = "where app_key=?";
		}
		$sql = "select 
			id, 
			CASE
				WHEN 
					parent_id = '' OR parent_id is null 
				THEN 
					0 
				ELSE 
					parent_id 
			END AS parent_id,
			menu_name,menu_link,menu_desc,
			CASE
				WHEN 
					is_active = '' OR is_active is null 
				THEN 
					0 
				ELSE 
					is_active 
			END AS is_active,
			CASE
				WHEN 
					weight = '' OR weight is null 
				THEN 
					0 
				ELSE 
					weight 
			END AS weight,class from z_menus " . $where . " order by weight, id,parent_id";
		
		try {
			$items = $this->_db->fetchAll ( $sql, $id );
			return $items;
		} catch ( Exception $e ) {
			Zend_Debug::dump ( $e->getMessage () );
			die ( $q );
		}
		return $items;
	}
	
	public function insert_update_menu($data) {
	//Zend_Debug::dump($data); die();
	($data['id']!="") ? $action  ="update" :$action ='create';
	$result = array('result'=>false, 'message'=> '');  
			
	if($action == 'update')	{
		$sql  = "update z_menus  set parent_id =?, menu_name = ?, menu_link= ?, menu_desc =?, is_active =?, weight =?, class=? where id =?";
	} elseif($action == 'create') {
		$sql  = "insert into z_menus  (parent_id, menu_name, menu_link, menu_desc,is_active, weight, class ) values (?, ?, ?, ?, ?, ?, ?)";
	}
		
	try {
		if($action == 'create')	 {
			$with = array($data['parent'], $data['name'],$data['link'],$data['desc'], ((isset($data['isactive']))?'1':'0'), $data['weight'], $data['icon_cls_send']  );
			$this->_db->query($sql, $with);
			$result = array('result'=>true, 'message'=>'Menu '.$data['name'].' has been created.');
		} else {
			$with = array($data['parent'], $data['name'], $data['link'], $data['desc'], ((isset($data['isactive']))?'1':'0'), $data['weight'], $data['icon_cls_send'], $data['id']);
			$this->_db->query($sql, $with);
			$result = array('result'=>true, 'message'=>'Menu '.$data['name'].' has been updated.');
		}	
	} catch (Exception $e) {
		$result = array('result'=>false, 'message'=> $e->getMessage());  
	}			
	return $result;
	}
	
	public function insert_update_role($data) {
		// Zend_Debug::dump($data); die();
		$with = array ();
		($data ['rid'] != "") ? $action = "update" : $action = 'create';
		$result = array (
				'result' => false,
				'message' => '' 
		);
		
		if ($action == 'update') {
			$cek = $this->_db->fetchOne ( "select count(*) from z_groups where gid != ? and group_name = ?", array (
					$data ['rid'],
					$data ['role_name'] 
			) );
			if ($cek >= 1) {
				return array (
						'result' => false,
						'message' => $data ['role_name'] . " has been exist." 
				);
			}
			
			$sql = "update z_groups  set group_name = ?, `desc`= ?, perms =?, landing_page=?, pages=? where gid =?";
		} elseif ($action == 'create') {
			$cek = $this->_db->fetchOne ( "select count(*) from z_groups where group_name = ?", $data ['role_name'] );
			if ($cek >= 1) {
				return array (
						'result' => false,
						'message' => $data ['role_name'] . " has been exist." 
				);
			}
			$sql = "insert into z_groups  (group_name, `desc`, perms, landing_page, pages) values (?, ?, ?, ?,?)";
		}
		
		try {
			if ($action == 'create') {
				$with = array (
						$data ['role_name'],
						$data ['role_description'],
						serialize ( $data ['perms'] ),
						$data ['landing'], 
						serialize ( $data ['get_pages'] ), 
				);
				$this->_db->query ( $sql, $with );
				$gid = $this->_db->lastInsertId ();
				if (isset ( $_POST ['menus'] ) && count ( $_POST ['menus'] ) > 0) {
					foreach ( $data ['menus'] as $kk => $vv ) {
						$this->_db->query ( "insert into z_menus_groups (menu_id, group_id) values (?, ?) ", array (
								$kk,
								$data ['rid'] 
						) );
					}
				}
				$result = array (
						'result' => true,
						'message' => 'Role ' . $data ['role_name'] . ' has been created.',
						'gid' => $gid 
				);
			} else {
				$with = array (
						$data ['role_name'],
						$data ['role_description'],
						serialize ( $data ['perms'] ),
						$data ['landing'],
						serialize ( $data ['get_pages'] ), 
						$data ['rid'] 
				);
				$this->_db->query ( $sql, $with );
				
				$this->_db->query ( "delete from z_menus_groups where group_id=?", $data ['rid'] );
				
				if (isset ( $data ['menus'] ) && count ( $data ['menus'] ) > 0) {
					foreach ( $data ['menus'] as $kk => $vv ) {
						$this->_db->query ( "insert into z_menus_groups (menu_id, group_id) values (?, ?) ", array (
								$kk,
								$data ['rid'] 
						) );
					}
				}
				
				$result = array (
						'result' => true,
						'message' => 'Role ' . $data ['role_name'] . ' has been updated.' 
				);
			}
		} catch ( Exception $e ) {
			$result = array (
					'result' => false,
					'message' => $e->getMessage () 
			);
		}
		return $result;
	}
	
	public function delete_menus($menus) {
		// die($menus);
		$sql = "delete from z_menus where id in (" . $menus . ")";
		// Zend_Debug::dump($sql);die();
		try {
			$this->_db->query ( $sql );
			$result = array (
					'result' => true,
					'message' => 'Menu has been deleted.' 
			);
		} catch ( Exception $e ) {
			$result = array (
					'result' => false,
					'message' => $e->getMessage () 
			);
		}
		return $result;
	}
	
	public function activate_menu($id, $newstat) {
		try {
			$sqlupda_pers = "update z_menus set is_active=" . $newstat . " where id=" . $id;
			// die($sqlupda_pers);
			$this->_db->query ( $sqlupda_pers );
			$result = array (
					'result' => true,
					'message' => 'Menu has been ' . (($newstat == 1) ? 'activated' : 'deactivated') . '.' 
			);
		} catch ( Exception $e ) {
			$result = array (
					'result' => false,
					'message' => $e->getMessage () 
			);
		}
		
		return $result;
	}
	
	public function get_role_by_id($id) {
		$sql = "select  *  from z_groups  where gid =?";
		try {
			$items = $this->_db->fetchRow ( $sql, $id );
		} catch ( Exception $e ) {
			Zend_Debug::dump ( $e->getMessage () );
			die ( $sql );
		}
		return $items;
	}
	
	function get_menus_by_roles($id = "") {
		if ($id) {
			$sql = "select distinct a.* from z_menus a left join z_menus_groups b on a.id=b.menu_id where b.group_id in ($id)  order by weight";
		}
		
		try {
			$items = $this->_db->fetchAll ( $sql);
			return $items;
		} catch ( Exception $e ) {
            return array();
			Zend_Debug::dump ( $e->getMessage () );
			die ( $sql );
		}
		return $items;
	}
	
	function get_menus_by_role($id = "") {
		if ($id) {
			$sql = "select distinct a.* from z_menus a left join z_menus_groups b on a.id=b.menu_id where b.group_id = ?  ";
		}
		
		try {
			$items = $this->_db->fetchAll ( $sql, $id );
			return $items;
		} catch ( Exception $e ) {
			Zend_Debug::dump ( $e->getMessage () );
			die ( $sql );
		}
		return $items;
	}
	
	public function delete_roles($roles) {
		// die($menus);
		$sql = "delete from z_groups where gid in (" . $roles . ")";
		// Zend_Debug::dump($sql);die();
		try {
			$this->_db->query ( $sql );
			$this->_db->query ( "delete from z_menus_groups where group_id in (" . $roles . ")" );
			
			$result = array (
					'result' => true,
					'message' => 'Roles has been deleted.' 
			);
		} catch ( Exception $e ) {
			$result = array (
					'result' => false,
					'message' => $e->getMessage () 
			);
		}
		return $result;
	}
	
	public function get_use_by_id($id) {
		$sql = "select a.*, b.name as ubis, c.name as sub_ubis  from z_users a  left join p_ubis b on a.ubis_id=b.id left join p_sub_ubis c on a.sub_ubis_id=c.id  where a.id =?";
		try {
			$items = $this->_db->fetchRow ( $sql, $id );
		} catch ( Exception $e ) {
			Zend_Debug::dump ( $e->getMessage () );
			die ( $q );
		}
		
		return $items;
	}
	
	public function call_api($id, $xin) {
			$dd = new Model_Zprabapage ();
			$m1 = new Model_Zpraba4api ();
			$varC = $dd->get_api ( $id );
			//Zend_Debug::dump($varC); die();
			$conn = unserialize ( $varC ['conn_params'] );
			$result = $m1->execute_api ( $conn, $varC, $xin );	
		return $result;
	}
	
	public function exec_api($id, $xin) {
		$dd = new Model_Zprabapage ();
		$m1 = new Model_Zpraba4api ();
		$varC = $dd->get_api ( $id );
		// Zend_Debug::dump($varC); die();
		$conn = unserialize ( $varC ['conn_params'] );
		$result = $m1->exec_api ( $conn, $varC, $xin );
		// Zend_Debug::dump($result); die();		
		return $result;
	}
}

