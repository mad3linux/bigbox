<?php

/*
 * Copyright (C) Prabtech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class Model_Zdash extends Zend_Db_Table_Abstract {

    public function getdataform($cid) {

        $authAdapter = Zend_Auth::getInstance();
        $identity = $authAdapter->getIdentity();

        $v0params = $this->get_params_by_qid($cid);

        $params["ctype"] = $v0params['act_id'];
        $v_params["ctype"] = $v0params['act_id'];

        $v_params["pidin"] = $v0params['id'];
        $v_params["content_id"] = $cid;
        $v_params["qid"] = $v0params['current_queue'];
        $v_params["pid"] = $v0params['current_process'];
        #Zend_Debug::dump($v_params); die();

        $z = new CMS_General();
        $por = new Model_Zprabawf();

        $cc = new Model_Zprabawf();

        $data = $cc->get_action_content_type($params['ctype']);


        $zzzz = new Model_Zprabadoc();
        $dataproses = $por->get_process_by_pid($v_params['pid']);
        $show = (unserialize($dataproses['zparams_serialize'])); //die();
        #Zend_Debug::dump($dataproses); die();
        $vv = $show['Data']['weight'];
        asort($vv);
        foreach ($data['field'] as $zz) {
            $vnew[$zz['fname']] = $zz;
        }
        foreach ($vv as $k => $v) {
            $v2[$k] = $vnew[$k];
        }
        $data['field'] = $v2;
        $showapproval = false;
        $inter = array_intersect($show['Data']['approval'], $identity->roles);
        if (count($inter) > 0) {
            $showapproval = true;
        }
        $data['showapproval'] = $showapproval;
        if (count($data ['field']) > 0) {

            $i = 0;
            $varCx = array();
            $initval = "";
            foreach ($data ['field'] as $v) {
                $initval = "";
                if (isset($show['Data']['field'][$v['fname']]) && $show['Data']['field'][$v['fname']] == '1') {
                    $datafield = array();
                    if (isset($v['detail'] ['initvalue']) && !empty($v ['detail'] ['initvalue'])) {
                        $initval = $v['detail'] ['initvalue'];
                    } elseif ($v['detail'] ['initApi'] != "") {
                        $varCx = $dd->get_api($v['detail'] ['initApi']);
                        $connx = unserialize($varCx ['conn_params']);
                        $datafield = $exe->execute_api($connx, $varCx);
                        $initval = $datafield ['data'];
                    }
                    //Zend_Debug::dump($initval); die();
                    $initarr = array();
                    if (!is_array($initval)) {

                        $pos = strpos($initval, '~');
                        if ($pos !== false) {
                            $initarr = array();

                            $vinit = explode('~', $initval);
                            $x = 0;
                            foreach ($vinit as $vin) {
                                $qarin = explode(':', $vin);

                                $initarr [$x] ['key'] = $qarin [0];
                                $initarr [$x] ['value'] = $qarin [1];
                                if (count($qarin) <= 1) {
                                    $initarr [$x] ['key'] = $qarin [0];
                                    $initarr [$x] ['value'] = $qarin [0];
                                }
                                $x ++;
                            }
                        }
                    } else {
                        $initarr = array();
                        foreach ($initval as $kx => $vx) {
                            $initarr [$kx] ['key'] = $vx ['key'];
                            $initarr [$kx] ['value'] = $vx ['value'];
                        }
                    }
                    $dataform [$v ['detail']['area']] [$i] = $v;
                    $dataform [$v ['detail']['area']] [$i] ['datafieldinit'] = $initarr;

                    $i ++;
                }
            }
        }


        if ($v_params['pidin'] != "") {
            $vidat = $zzzz->get_initial_value_content($v_params['pidin']);
            //	Zend_Debug::dump($vidat); die("ss");
            if (count($vidat) >= 1) {
                $vdata['initial'] = $vidat;
            }
        }

        #Zend_Debug::dump($dataform); die();

        $data['form-element'] = $dataform;

        $vdata['data'] = $data;
        $vdata['show'] = $show;
        $vdata['varams'] = array_merge($params, $v_params);

        return $vdata;
    }

    public function get_count_pic_assign($rid = 141) {
        try {


            $data2 = $this->_db->fetchAll("select distinct
							a . *, count(*) as cnt
						from z_users a
								inner join
							z_users_groups b ON a.id = b.uid inner join 
							 sm_wf_solution_pic d ON a.id=d.uid 
						where
							b.gid = ? group by a.id", $rid);
            //Zend_Debug::dump($data2); die();											
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            die();
            return false;
        }
        return $data2;
    }

    public function get_am($li, $la = "") {
        try {
            if ($la != "") {
                $sq = " and SEGMEN = '" . $la . "' ";
            }

            $sql = "select NIK as id, NAMA, JABATAN, UNIT, AREA
										
									from
										sm_employee
									where (NIK like '%" . strtoupper($li) . "%' or NAMA like '%" . strtoupper($li) . "%')   $sq limit 0,10";
            #	die($sql);							
            $data = $this->_db->fetchAll($sql);
        } catch (Exception $e) {
            return array();
        }
        $new['total_count'] = count($data);
        $new['items'] = $data;

        return $new;
    }

    public function get_customer($li, $la = "") {
        try {
            if ($la != "") {
                $sq = " and SEGMEN = '" . $la . "' ";
            }
            //die($la);

            $data = $this->_db->fetchAll("select NIP_NAS as id, STANDARD_NAME from sm_cbase_dives where standard_name like '%" . strtoupper($li) . "%' $sq limit 0,10");
        } catch (Exception $e) {
            return array();
        }
        $new['total_count'] = count($data);
        $new['items'] = $data;

        return $new;
    }

    public function get_params_by_qid($qid) {
        try {
            $data = $this->_db->fetchRow("select * from zpraba_process_instance where ext_id=?", $qid);
        } catch (Exception $e) {
            return array();
        }
        #Zend_Debug::dump($data); die();
        return $data;
    }

    public function get_pic_solution_by_id($id) {
        try {
            $data = $this->_db->fetchAll("select a.uid, 
    b.uname, b.mobile_phone, b.fullname, c.name
from
    sm_wf_solution_pic a
        inner join
    z_users b ON a.uid = b.id
        inner join
    p_ubis c ON b.ubis_id = c.id where a.cid=?", $id);
        } catch (Exception $e) {
            return array();
        }

        foreach ($data as $z) {
            $new[] = $z['uid'];
        }
        return $new;
    }

    public function get_pic_solution_all() {
        try {
            $data = $this->_db->fetchAll("select  b.id as uid,
    b.uname, b.mobile_phone, b.fullname, c.name
from
    z_users b 
        inner join
    p_ubis c ON b.ubis_id = c.id");
        } catch (Exception $e) {
            return array();
        }

        foreach ($data as $vv) {
            $new[$vv['name']][] = $vv;
        }
        #Zend_Debug::dump($new); die();
        return $new;
    }

    public function get_solution_by_id($id) {
        try {
            $data = $this->_db->fetchRow("select  a.id as sid, a.created,
		b . *
	from
		zpraba_content a
			inner join
		zpraba_content_revision c ON a.vid = c.vid
			inner join
		sm_wf_solution b ON a.vid = b.vid where a.id =?", $id);
        } catch (Exception $e) {
            return array();
        }
        return $data;
    }

    public function get_all_longest($my = false) {

        $authAdapter = Zend_Auth::getInstance();
        $identity = $authAdapter->getIdentity();
        $sql = "select a.id as sid,
		 DATEDIFF(now(),a.created) as longest, b.*
	from
		zpraba_content a
			inner join
		zpraba_content_revision c ON a.vid = c.vid
			inner join
		sm_wf_solution b ON a.vid = b.vid  order by a.created asc limit 0,30";

        if ($my) {
            $sql = "select a.id as sid,
			 DATEDIFF(now(),a.created) as longest, b.*
		from
			zpraba_content a
				inner join
			zpraba_content_revision c ON a.vid = c.vid
				inner join
			sm_wf_solution b ON a.vid = b.vid  
			inner join zpraba_process_instance d on  d.ext_id=a.id where d.initiator_uid=" . $identity->uid . "
		order by a.created asc";
        }

        try {


            $data = $this->_db->fetchAll($sql);
            //Zend_Debug::dump($data); die();
        } catch (Exception $e) {
            return array();
        }
        return $data;
    }

    public function get_all_solution($my = false) {
        try {

            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();

            $sql = "select a.id as sid,  a.created,
		b . *
	from
		zpraba_content a
			inner join
		zpraba_content_revision c ON a.vid = c.vid
			inner join
		sm_wf_solution b ON a.vid = b.vid  order by a.created desc ";
            if ($my) {
                $sql = "select a.id as sid,  a.created,
		b . *
	from
		zpraba_content a
			inner join
		zpraba_content_revision c ON a.vid = c.vid
			inner join
		sm_wf_solution b ON a.vid = b.vid  inner join zpraba_process_instance d on  d.ext_id=a.id where d.initiator_uid=" . $identity->uid . " order by a.created desc ";
            }
            #die($sql); 
            $data = $this->_db->fetchAll($sql);

            #Zend_Debug::dump($data); die();	
        } catch (Exception $e) {
            return array();
        }
        return $data;
    }

    public function get_chart_phase_type($my = false) {
        try {

            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();

            $sql = "select  b.* from zpraba_content a  inner join zpraba_content_revision c on a.vid = c.vid inner join sm_wf_solution b on a.vid=b.vid";

            if ($my) {
                $sql = "select  b.* from zpraba_content a  inner join zpraba_content_revision c on a.vid = c.vid inner join sm_wf_solution b on a.vid=b.vid inner join zpraba_process_instance f
		on f.ext_id = a.id where f.initiator_uid=" . $identity->uid;
            }

            $data = $this->_db->fetchAll($sql);
            foreach ($data as $v) {
                if ($v['phase_status'] != "") {
                    $new[$v['phase_status']][] = $v;
                } else {
                    $new['Filter'][] = $v;
                }
            }
        } catch (Exception $e) {
            return false;
        }
        return $new;
    }

    public function get_chart_type_solusi($my = false) {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();

            $sql = "select  b.* from zpraba_content a  inner join zpraba_content_revision c on a.vid = c.vid inner join sm_wf_solution b on a.vid=b.vid";

            if ($my) {
                $sql = "select  b.* from zpraba_content a  inner join zpraba_content_revision c on a.vid = c.vid 
				inner join sm_wf_solution b on a.vid=b.vid inner join  zpraba_process_instance f 
				on a.id = f.ext_id where f.initiator_uid=" . $identity->uid;
            }

            $data = $this->_db->fetchAll($sql);



            foreach ($data as $v) {
                if ($v['type_solution'] != "") {
                    $new[$v['type_solution']][] = $v;
                } else {
                    //$new[$v['Filter']] = $v;	
                }
            }
        } catch (Exception $e) {
            return false;
        }
        return $new;
    }

    public function get_chart_closed_solution($my = false) {
        try {

            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();

            $sql = "select 
				b . *
			from
				zpraba_content a
					inner join
				zpraba_content_revision c ON a.vid = c.vid
					inner join
				sm_wf_solution b ON a.vid = b.vid  inner join zpraba_process_instance f

		on a.id=f.ext_id where f.complete=1";
            if ($my) {
                $sql = "select 
					b . *
				from
					zpraba_content a
						inner join
					zpraba_content_revision c ON a.vid = c.vid
						inner join
					sm_wf_solution b ON a.vid = b.vid  inner join zpraba_process_instance f

				on a.id=f.ext_id where f.complete=1 and f.initiator_uid=  " . $identity->uid;
            }


            $data = $this->_db->fetchAll($sql);



            foreach ($data as $v) {
                if ($v['closed_sttaus'] != "") {
                    $new[$v['closed_sttaus']][] = $v;
                } else {
                    //$new[$v['Filter']] = $v;	
                }
            }
        } catch (Exception $e) {
            return false;
        }
        return $new;
    }

    public function get_count_pic($rid = 141) {
        try {

            $data = $this->_db->fetchAll("select distinct
							a . *, count(*) as cnt
						from
							z_users a
								inner join
							z_users_groups b ON a.id = b.uid inner join 
							 sm_wf_solution d ON a.id=d.assignee inner join	
						 zpraba_content c on  c.vid = d.vid
						where
							b.gid = ? group by a.id ", $rid);

            $data2 = $this->_db->fetchAll("select distinct
							a . *, count(*) as cnt
						from z_users a
								inner join
							z_users_groups b ON a.id = b.uid inner join 
							 sm_wf_solution_pic d ON a.id=d.uid 
						where
							b.gid = ? group by a.id", $rid);
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            die();
            return false;
        }
        return $data;
    }

    public function get_all_content($id) {
        try {
            $data = $this->_db->fetchAll("select * from zpraba_content where content_type =?", $id);
        } catch (Exception $e) {
            return false;
        }
        return $data;
    }

    public function add_pic($data) {
        try {

            $this->_db->query("delete from sm_wf_solution_pic where cid=?", $data['id']);
            foreach ($data['get_api'] as $z) {
                $this->_db->query("insert into sm_wf_solution_pic (cid, uid) values (?, ?)", array($data['id'], $z));
            }
            //$data = $this->_db->fetchAll("select * from zpraba_content where content_type =?", $id);
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    public function post_chat($data) {

        try {
            if ($data['id'] != "") {
                $this->_db->query("insert into zpraba_chat (uid, sdate, msg, ext_id_1) values (?, now(), ?, ?) ", array($data['uid'], $data['msg'], $data['id']));
            } else {
                $this->_db->query("insert into zpraba_chat (uid, sdate, msg) values (?, now(), ?) ", array($data['uid'], $data['msg']));
            }
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    public function get_chat($ext_id = null, $ext_id2 = null) {

        try {

            $x = "";
            if ($ext_id != "") {
                $x = " and a.ext_id_1= " . (int) $ext_id;
            }

            $y = "";
            if ($ext_id2 != "") {
                $y = " and a.ext_id_2= " . (int) $ext_id2;
            }

            $sql = "select a.*, b.fullname, b.picture from zpraba_chat a inner join z_users b on a.uid = b.id where 1=1  $x $y order by a.id desc 	limit 0, 200";
            //	echo ($sql);die();	
            $data = $this->_db->fetchAll($sql);
        } catch (Exception $e) {
            
        }
        return $data;
    }

}
