<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
use Enzim\Lib\TikaWrapper\TikaWrapper;
class Model_Zprabadata extends Zend_Db_Table_Abstract{
    
public function get_sm_solution($data){
	try {
		
		$path = (APPLICATION_PATH . '/indexes');
		if($data['rebuild']==1) {
		// create the index
			$files = glob($path.'/*'); 
			foreach($files as $file){ 
				if(is_file($file))
				unlink($file); 
			}
			$files = glob($path.'/{,.}*', GLOB_BRACE);
			foreach($files as $file){ 
			if(is_file($file))
				unlink($file); 

			}
			$index = Zend_Search_Lucene::create($path);

		} else {
			$index = Zend_Search_Lucene::open($path);
		}
		
		$sql ="select b.* from zpraba_content a inner join sm_wf_solution b on a.vid=b.vid where a.id=?";
		$row = $this->_db->fetchRow($sql, $data['id']);
		
		$doc = new  Zend_Search_Lucene_Document(); 
		$doc->addField(Zend_Search_Lucene_Field::UnIndexed('cid', $row['cid'])); 
		$doc->addField(Zend_Search_Lucene_Field::Text('title',$row['req_name'])); 
		$doc->addField(Zend_Search_Lucene_Field::Text('description', $row['req_descr']));
		if($row['attachment']!="") {
			$f1= $this->get_a_file($row['attachment']);	
			if($f1['raw_url']!="" && file_exists($f1['raw_url'])) {
			
				$contentf1 = TikaWrapper::getText($f1['raw_url']);
				$metaf1 = TikaWrapper::getMetadata($f1['raw_url']);

				$doc->addField(Zend_Search_Lucene_Field::Text('contentf1', $contentf1, 'UTF-8'));
				$doc->addField(Zend_Search_Lucene_Field::Text('metadataf1', $metaf1, 'UTF-8'));
				$doc->addField(Zend_Search_Lucene_Field::Text('urlf1', $f1['http_url']));
				$doc->addField(Zend_Search_Lucene_Field::Text('fnamef1', $f1['fname']));
			}
		
		} else {
				$doc->addField(Zend_Search_Lucene_Field::Text('contentf1', '', 'UTF-8'));	
				$doc->addField(Zend_Search_Lucene_Field::Text('metadataf1', "", 'UTF-8'));
				$doc->addField(Zend_Search_Lucene_Field::Text('urlf1', ""));
				$doc->addField(Zend_Search_Lucene_Field::Text('fnamef1', ""));
		}
	
	
		if($row['attachment2']!="") {
			$f2= $this->get_a_file($row['attachment2']);	
			if($f2['raw_url']!="" && file_exists($f2['raw_url'])) {
			
				$contentf2 = TikaWrapper::getText($f2['raw_url']);
				$metaf2 = TikaWrapper::getMetadata($f2['raw_url']);

				$doc->addField(Zend_Search_Lucene_Field::Text('contentf2', $contentf2, 'UTF-8'));
				$doc->addField(Zend_Search_Lucene_Field::Text('metadataf2', $metaf2, 'UTF-8'));
				$doc->addField(Zend_Search_Lucene_Field::Text('urlf2', $f2['http_url']));
				$doc->addField(Zend_Search_Lucene_Field::Text('fnamef2', $f2['fname']));
			}
		
		} else {
				$doc->addField(Zend_Search_Lucene_Field::Text('contentf2', '', 'UTF-8'));	
				$doc->addField(Zend_Search_Lucene_Field::Text('metadataf2', "", 'UTF-8'));
				$doc->addField(Zend_Search_Lucene_Field::Text('urlf2', ""));
				$doc->addField(Zend_Search_Lucene_Field::Text('fnamef2', ""));
		}
		
		
	
    } catch (Exception $e) {
        Zend_Debug::dump($e->getMessage());die($sql);        
		}   
	}

	public function get_a_file($fid) {
			$vg  = "select * from zpraba_files where fid =  ".$fid;
			$edata  = $this->_db->fetchRow($vg);
			
			return $edata;
	}

}

