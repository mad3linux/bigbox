<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Zinbox extends Zend_Db_Table_Abstract {



public function get_outgoing_by_user($uid) 
   {
	$sql ="SELECT D.FULLNAME,
				  Q.STATUS,
				  Z.SUBJECT,
				  Z.MESSAGES,
				  Z.ID,
				  A.ID                                          AS QID,
				  TO_CHAR(Z.CREATED_DATE, 'DD/MM/YYYY HH24:MI') AS CREATED_DATE
				FROM Z_MESSAGES Z
				INNER JOIN Z_WFQUEUE A
				ON Z.QID= A.ID
				INNER JOIN Z_USERS D
				ON Z.FROM_UID = D.ID
				INNER JOIN Z_WFPROCESS Q
				ON Q.ID            =A.PROCESS_ID
				WHERE Z.USER_ID    =109
				AND (Q.STATUS      =1
				OR Q.STATUS       IS NULL
				OR Q.STATUS        =98 OR Q.STATUS        =999)
				AND Q.CURRENT_QUEUE!=Z.QID AND A.STATUS=1" ;
	try {
			//die($sql);
		$data = $this->_db->fetchAll($sql);
		
			}catch(Exception $e) {
				Zend_Debug::dump($e); die($sql);
		
		}	  
		
		//Zend_Debug::dump($newdata);
	   return $data;
	}
	
	
	
	 public function get_draft_by_user($uid) 
   {
	$sql ="SELECT D.FULLNAME, Q.STATUS,
			  Z.SUBJECT,
			  Z.MESSAGES,
			  Z.ID,
			  A.ID                                          AS QID,
			  TO_CHAR(Z.CREATED_DATE, 'DD/MM/YYYY HH24:MI') AS CREATED_DATE
			FROM Z_MESSAGES Z
			INNER JOIN Z_WFQUEUE A
			ON Z.QID= A.ID
			INNER JOIN Z_USERS D
			ON Z.FROM_UID  = D.ID
			INNER JOIN Z_WFPROCESS Q ON Q.ID=A.PROCESS_ID
			WHERE Z.USER_ID=$uid AND (Q.STATUS=9) AND Q.CURRENT_QUEUE=Z.QID" ;
	try {
	
		$data = $this->_db->fetchAll($sql);
		
			}catch(Exception $e) {
				Zend_Debug::dump($e); die($sql);
		
		}	  
		
		//Zend_Debug::dump($newdata);
	   return $data;
	}
	

    public function get_allinbox_by_user($uid) 
   {
	$sql ="SELECT D.FULLNAME, Q.STATUS,
  Z.SUBJECT,
  Z.MESSAGES,
  Z.ID,
  A.ID                                          AS QID,
  TO_CHAR(Z.CREATED_DATE, 'DD/MM/YYYY HH24:MI') AS CREATED_DATE
FROM Z_MESSAGES Z
INNER JOIN Z_WFQUEUE A
ON Z.QID= A.ID
INNER JOIN Z_USERS D
ON Z.FROM_UID  = D.ID
INNER JOIN Z_WFPROCESS Q ON Q.ID=A.PROCESS_ID
WHERE Z.USER_ID=$uid AND (Q.STATUS=1 OR Q.STATUS IS NULL OR Q.STATUS=98) AND Q.CURRENT_QUEUE=Z.QID" ;
	try {
		//die($sql);
		$data = $this->_db->fetchAll($sql);
		
			}catch(Exception $e) {
				Zend_Debug::dump($e); die($sql);
		
		}	  
		
		//Zend_Debug::dump($newdata);
	   return $data;
	}
    public function get_a_inbox($id) 
   {
	
	$sql ="SELECT A.*,
			  TO_CHAR(A.CREATED_DATE, 'DD/MM/YYY HH24:MI:SS') AS CREATED,
			  E.ID                                            AS NID,
			  B.FULLNAME,
			  B.EMAIL,
			  C.TEMPLATE_DATA_ID,
			  E.TEMPLATE_ID,
			  C.TASK_NAME, 
			  O.TASK_NAME AS LAST_TASK_NAME, 
				ZZ.REMINDER_INTERVAL,
				ZZ.REMINDER_SUBJECT,
				ZZ.REMINDER_MESSAGE,
				ZZ.POST_NOTIFY_SUBJECT,
				ZZ.POST_NOTIFY_MESSAGE,
				ZZ.SEND_INTERVAL
			FROM Z_MESSAGES A
			INNER JOIN Z_USERS B
			ON A.FROM_UID=B.ID
			INNER JOIN Z_WFQUEUE C
			ON A.QID= C.ID
			LEFT JOIN Z_WFQUEUE O
			ON A.LAST_QID= O.ID
			
			INNER JOIN Z_WFPROCESS D
			ON A.PID=D.ID
			INNER JOIN Z_WFNODE E
			ON D.TRACKING_ID=E.ID
			INNER JOIN Z_WFTEMPLATE_DATA ZZ ON C.TEMPLATE_DATA_ID =ZZ.ID
			WHERE A.ID  =$id" ;
	
	try {
	//	die($sql);
		$data = $this->_db->fetchRow($sql);
		//$new = array();
		$vf =$this->_db->fetchAll("SELECT B.ID, B.CUSTOMER_NAME FROM SW_WFNODE_CUSTOMER A INNER JOIN SW_CUSTOMER_DATA B ON A.CUSTOMER_ID=B.ID WHERE A.ZWF_NODE_ID=".(int)$data['NID']);
		foreach($vf as $v) 
		{
		 $cust[]= $v['CUSTOMER_NAME'];	
			
		}
		$var_cyus =  implode(',', $cust);
		
		$data['CUSTOMER']=$var_cyus;
		
		
			}catch(Exception $e) {
				Zend_Debug::dump($e); die($sql);
		}	  
		//Zend_Debug::dump($data); die(); 
		//Zend_Debug::dump($newdata);
	   return $data;
	}
    
   
   public function get_files($NID, $PID) 
   {
	$sql ="SELECT * FROM  Z_FILES  WHERE NODE_ID='$NID' AND PROCESS_ID=$PID" ;
		
		
	try {
		
		$data = $this->_db->fetchAll($sql);
		
			}catch(Exception $e) {
				Zend_Debug::dump($e); die($sql);
		
		}	  
		
		//Zend_Debug::dump($newdata);
	   return $data;
	}
   
    public function get_files_qid($id) 
   {
	$sql ="SELECT * FROM  Z_FILES  WHERE  QID=$id" ;
		
		
	try {
		
		$data = $this->_db->fetchAll($sql);
		
			}catch(Exception $e) {
				Zend_Debug::dump($e); die($sql);
		
		}	  
		
		//Zend_Debug::dump($newdata);
	   return $data;
	}
   public function get_a_user($ID) 
   {
	$sql ="SELECT * FROM  Z_USERS  WHERE ID=$ID" ;
		
		
	try {
		
		$data = $this->_db->fetchRow($sql);
		
			}catch(Exception $e) {
				Zend_Debug::dump($e); die($sql);
		
		}	  
		
	
	   return $data;
	}
   
   
   public function get_a_files($ID) 
   {
	$sql ="SELECT * FROM  Z_FILES  WHERE ID=$ID" ;
		
		
	try {
		
		$data = $this->_db->fetchRow($sql);
		
			}catch(Exception $e) {
				Zend_Debug::dump($e); die($sql);
		
		}	  
		
	
	   return $data;
	}
	
	
	public function update_inbox($data, $status, $uid) 
	{
		//Zend_Debug::dump($data); die();
		
		
		$upd ="UPDATE Z_WFPROCESS SET STATUS ='$status'  WHERE ID = '".$data['PID']."'";
		
		$sql ="INSERT INTO Z_WFPROCESS_STATUS (PID, STATUS, USER_ID, UPDATED_DATE, QID) VALUES ('".$data['PID']."', '".$status."', '$uid', SYSDATE,  '".$data['QID']."')" ;
		//die($sql);
		try {
			$this->_db->query($upd);
			$this->_db->query($sql);
			//die("s");
			return true;
				}catch(Exception $e) {
					Zend_Debug::dump($e); die($sql);
			}	  
			
		   
	}
	
	
	public function update_wf_submit($data) 
	{
				//Zend_Debug::dump($data);die();
				
				
				
				
				$node  = new Model_Zwfnode();
				
				$auth = Zend_Auth::getInstance();
				$identity = $auth->getIdentity();
				$dd = new Model_Zproject();
				$temp  = new Model_Zwftemplate();
				$tempdata = new Model_Zwftemplatedata();
				$proj = new Model_Zwfproject();
				$proce = new Model_Zwfprocess();
				$node_assign = new Model_Zwftdnodeassign();
				$queue = new Model_Zwfqueue();
				$quefrom = new Model_Zwfqueuefrom();
				$temdatnext = new Model_Zwftemplatedatanext();
				$mess= new Model_Zmessage();
				$mess= new Model_Zmessage();	
			$datnode = $node->fetchdata($data['ID']); 
			$st = $queue->fetchdatacurrent($data['PID']);
			//Zend_Debug::dump($datnode); die("ok");
            
            $get = $temdatnext->fetchdatafromarr($st['TEMPLATE_DATA_ID']);
			//Zend_Debug::dump($get); die("ok");
            $tdata = $tempdata->fetchdatabyid($get['TEMPLATE_DATA_TO']);
            //Zend_Debug::dump($tdata); die("ok");
            $datacomp = array(
                'ZUID' => $identity->uid,
                'ID' => $st['ID'],
                //'STATUS_DESCR'=>$_POST["STATUS_DESCR"]
                
            );
            
            
            
			
            $queue->updatecomplete($datacomp);
            
			$data8 = array(
                'PROCESS_ID' => $data['PID'],
                'TEMPLATE_DATA_ID' => $get['TEMPLATE_DATA_TO'],
                'TASK_CLASS_NAME' => $tdata['TASK_CLASS_NAME'],
                'IS_INTERACTIVE' => 0,
                'SHOW_IN_DETAIL' => 0,
                'HANDLER' => '',
                'TASK_DATA' => '',
                'TEMP_DATA' => '',
                'STATUS' => 0,
                'ARCHIVED' => 1,
                'RUN_ONCE' => 0,
                'ZUID' => '',
                'PREPOPULATE' => '',
                'COMPLETED_DATE' => NULL,
                'NEXT_REMINDER_TIME' => '',
                'NUM_REMINDER_SENT' => 0,
                'TASK_NAME' => $tdata['TASK_NAME'],
            );

            $crx = $queue->createitem($data8);
          
            $data9 = array(
                'QUEUE_ID' => $crx->ID,
                'FROM_QUEUE_ID' => $st['ID']
            );
            
           $quefrom->createitem($data9);
            
            if($tdata['TASK_CLASS_NAME']=="MaestroTaskTypeEnd"){
                    
              //update process =1 ketika projeck berakhir 
              //$cproj->updatestat($params['NDID'],'1');
                
                $proce->completed($data['PID'], $identity->uid);
                $upda = array(
                    'ID' => $data['PID'],
                    'CURRENT_QUEUE' => $crx->ID,
                    'COMPLETE'=>'1',
                    'COMPLETED_DATE'=>new Zend_Db_Expr("SYSDATE")
                    
                );
            }else{
                $upda = array(
                    'ID' => $data['PID'],
                    'CURRENT_QUEUE' => $crx->ID,
                    'COMPLETE'=>'0',
                    'COMPLETED_DATE'=>""
                );
              
              
			$cf = new Model_Zfiles();
			$cms = new CMS_Functions();  
              //cek dan copy file for next step 
              
            if($data[files]) 
            {
				
				foreach ($data[files] as $k => $v) {
					
				$fil = $this->get_a_files($k);
				$dest_dir = (APPLICATION_PATH) . '/../public/uploads/'.$data['ID'].'/'.$fil['QID'].'/'.$fil['FILE_NAME'];
				if (is_file($dest_dir)) { 
				$new = (APPLICATION_PATH) . '/../public/uploads/'.$data['ID'].'/'.$data['QID'].'/'.$fil['FILE_NAME'];
					copy($dest_dir, $new);
				}
				
			
				
				$params1=array();
				$params1['upload']['name']=$fil['FILE_NAME'];
				$params1['upload']['type']=$fil['FILE_TYPE'];
				$params1['upload']['size']=$fil['FILE_SIZE'];
				
				$params2=array();
				$params2['TDID']=$fil['TEMPLATE_DATA_ID'];
				$params2['ID']=$fil['NODE_ID'];
				$params2['descr']=$fil['FDESCR'];
				$params2['QID']=$data['QID'];
				$params2['version']=$fil['VERSION'];
				$uid=$identity->uid;
				
				$cf->create_file($params1, $params2, $uid);
				
				} 
			}
              
              
              
			  if($tdata['ASSIGN_ID']){
				$dat_a= $dd->get_users_ubis($tdata['ASSIGN_ID']);
				
				
				foreach($dat_a as $zxx){
					$inbox = array(
						'MESSAGES'=>$data['massage'],
						'FROM_UID'=>$identity->uid,
						'SUBJECT'=>$datnode['PROJECT_DESCR'],
						'QID'=>$crx->ID,
						'LAST_QID'=>$data['QID'],
						'PID'=>$data['PID'],
						'USER_ID'=>$zxx[ID],
					);
				
					$mess->new_message($inbox);
					if($data['email']!="")
					{
					   $vfrom = $this->get_a_user($identity->uid);	 	
					   $vto = $this->get_a_user($identity->uid);	
					   if($vto['EMAIL']){
					   $cms->sending_mail($data['POST_NOTIFY_SUBJECT'], $data['POST_NOTIFY_MESSAGE'], $vto['FULLNAME'], $vfrom['FULLNAME'], $vto['EMAIL'], $vfrom['EMAIL']); 
					   }				
					
					
					}
					if($data['sms']!="")
					{
					   //$vfrom = $this->get_a_user($identity->uid);	 	
					   $vto = $this->get_a_user($identity->uid);	
					   if($vto['MOBILE_PHONE']){
						$cms->sending_sms($vto['MOBILE_PHONE'], $data['POST_NOTIFY_MESSAGE']); 
					   }				
					}
				
				}
				
				
				
				
				
				
				}
			}	
				$proce->updatecurrent($upda);
		   
		   //die("oke banget");
		   return true;
	}
	public function update_wf_return($data) 
	{
				//Zend_Debug::dump($data);die();
				
				$node  = new Model_Zwfnode();
				$auth = Zend_Auth::getInstance();
				$identity = $auth->getIdentity();
				$dd = new Model_Zproject();
				$temp  = new Model_Zwftemplate();
				$tempdata = new Model_Zwftemplatedata();
				$proj = new Model_Zwfproject();
				$proce = new Model_Zwfprocess();
				$node_assign = new Model_Zwftdnodeassign();
				$queue = new Model_Zwfqueue();
				$quefrom = new Model_Zwfqueuefrom();
				$temdatnext = new Model_Zwftemplatedatanext();
				$mess= new Model_Zmessage();
				$mess= new Model_Zmessage();	
				
				
			$datnode = $node->fetchdata($data['ID']); 
				
			$st = $queue->fetchdatacurrent($data['PID']);
			//Zend_Debug::dump($st); die("ok");
            
            $get = $temdatnext->fetchdata_to_arr($st['TEMPLATE_DATA_ID']);
			//Zend_Debug::dump($get); die("ok");
            $tdata = $tempdata->fetchdatabyid($get['TEMPLATE_DATA_FROM']);
            //Zend_Debug::dump($tdata); die("ok");
            $datacomp = array(
                'ZUID' => $identity->uid,
                'ID' => $st['ID'],
                //'STATUS_DESCR'=>$_POST["STATUS_DESCR"]
                
            );

            $queue->updatecomplete($datacomp);
            
			$data8 = array(
                'PROCESS_ID' => $data['PID'],
                'TEMPLATE_DATA_ID' => $get['TEMPLATE_DATA_FROM'],
                'TASK_CLASS_NAME' => $tdata['TASK_CLASS_NAME'],
                'IS_INTERACTIVE' => 0,
                'SHOW_IN_DETAIL' => 0,
                'HANDLER' => '',
                'TASK_DATA' => '',
                'TEMP_DATA' => '',
                'STATUS' => 0,
                'ARCHIVED' => 1,
                'RUN_ONCE' => 0,
                'ZUID' => '',
                'PREPOPULATE' => '',
                'COMPLETED_DATE' => NULL,
                'NEXT_REMINDER_TIME' => '',
                'NUM_REMINDER_SENT' => 0,
                'TASK_NAME' => $tdata['TASK_NAME'],
            );

            $crx = $queue->createitem($data8);
          
            $data9 = array(
                'QUEUE_ID' => $crx->ID,
                'FROM_QUEUE_ID' => $st['ID']
            );
            
           $quefrom->createitem($data9);
            
            if($tdata['TASK_CLASS_NAME']=="MaestroTaskTypeEnd"){
                    
               //update prject =1 ketika projeck berakhir 
              // $cproj->updatestat($params['NDID'],'1');
                
                
                $upda = array(
                    'ID' => $data['PID'],
                    'CURRENT_QUEUE' => $crx->ID,
                    'COMPLETE'=>'1',
                    'COMPLETED_DATE'=>new Zend_Db_Expr("SYSDATE")
                    
                );
            }else{
                $upda = array(
                    'ID' => $data['PID'],
                    'CURRENT_QUEUE' => $crx->ID,
                    'COMPLETE'=>'0',
                    'COMPLETED_DATE'=>""
                );
                
                $dat_a= $dd->get_users_ubis($tdata['ASSIGN_ID']);
			
                foreach($dat_a as $zxx){
				$inbox = array(
					'MESSAGES'=>$data['massage'],
					'FROM_UID'=>$identity->uid,
					'SUBJECT'=>$datnode['PROJECT_DESCR'],
					'QID'=>$crx->ID,
					'LAST_QID'=>$data['QID'],
					'PID'=>$data['PID'],
					'USER_ID'=>$zxx[ID],
				);
				
					$mess->new_message($inbox);
				}
            }
          //  Zend_Debug::dump($upda);die();
            $proce->updatecurrent($upda);
			
		   
		   //die("oke banget");
		   return true;
	}
   
}
