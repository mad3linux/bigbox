<?php


/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
use Monolog\Logger;
use Monolog\Handler\RedisHandler;
use Monolog\Formatter;
use Monolog\Formatter\JsonFormatter;

class Model_Zlogs extends Zend_Db_Table_Abstract {

    public function get_conncet() {
        try {
            $config = new Zend_Config_Ini(APPLICATION_PATH . 
                                          '/configs/application.ini', 'production');
            $config = $config->toArray();
            $redis = new Redis();
            $redis->connect($config['data']['redis']['host'], $config['data']['redis']['port'], 60);
            return $redis;
        }
        catch(Exception $e) {
            return false;
            //   Zend_Debug::dump($e); die();
        }
    }

    public function get_data_top50() {
        $c = new CMS_General();
        try {
            $redis = $this->get_conncet();
            $data = $redis->lRange('prabaclogs', - 100, - 1);
            krsort($data);
            $new = array();

            foreach($data as $k => $v) {
                $var = array();
                $var = json_decode($v);
                //  Zend_Debug::dump($var->datetime->date);
                $ke = strtotime($var->datetime->date);
                $new[$ke] = json_decode($v);
                $new[$ke]->time = $c->humanTiming($ke);
            }
            return array("data" =>$new,
                         "transaction" => true,
                         "message" => "success");
        }
        catch(Exception $e) {
            return array("data" => array(),
                         "transaction" => false,
                         "message" =>$e->getMessage());
        }
    }

    public function insert_log($data) {
        
        /*
         DEBUG (100): Detailed debug information.
         INFO (200): Interesting events. Examples: User logs in, SQL logs.
         NOTICE (250): Normal but significant events.
         WARNING (300): Exceptional occurrences that are not errors. Examples: Use of deprecated APIs, poor use of an API, undesirable things that are not necessarily wrong.
         ERROR (400): Runtime errors that do not require immediate action but should typically be logged and monitored.
         CRITICAL (500): Critical conditions. Example: Application component unavailable, unexpected exception.
         ALERT (550): Action must be taken immediately. Example: Entire website down, database unavailable, etc. This should trigger the SMS alerts and wake you up.
         EMERGENCY (600): Emergency: system is unusable.
         */
        try {
            $log = new Logger($data['logger']);
            $redis = $this->get_conncet();
            $redis = new RedisHandler($redis, "prabaclogs");
            $redis->setFormatter(new \Monolog\Formatter\JsonFormatter);
            $log->pushHandler($redis);
            // Zend_Debug::dump($identity->uname); die();
            $log->pushProcessor(function($record) {
                $authAdapter = Zend_Auth::getInstance();
                $identity = $authAdapter->getIdentity();
                $record['users']['username'] = $identity->uname;
                $record['users']['uid'] = $identity->uid;
                $record['users']['fullname'] = $identity->fullname;
                $record['users']['roles'] = $identity->roles;
                return $record;
            }
           );
            $log->pushProcessor(new \Monolog\Processor\WebProcessor);
            $log->log($data['level'], $data['message'], $data['context']);
            return array('transaction' => true,
                         'message' => 'success');
        }
        catch(Exception $e) {
            return array('transaction' => false,
                         'message' =>$e->getMessage());
        }
    }

    public function insert_redis($data) {
        try {
            $redis = $this->get_conncet();
            $redis->set($data['key'], $data['value']);
            return array('transaction' => true,
                         'message' => 'success');
        }
        catch(Exception $e) {
            return array('transaction' => false,
                         'message' =>$e->getMessage());
        }
    }

    public function test() {
        $redis = $this->get_conncet();
        $redis->setOption(Redis::OPT_SCAN, Redis::SCAN_RETRY);
        
        /* retry when we get no keys back */
        $currenttime = time();
        $arr_keys = $redis->scan($it, '*', 50);

        foreach($arr_keys as $str_key) {
            $data = $redis->get($str_key);
            echo "----";
            Zend_Debug::dump($str_key);
            Zend_Debug::dump($data);
            echo "----";
        }
    }
}
