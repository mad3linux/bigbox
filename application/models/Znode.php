<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Znode extends Zend_Db_Table_Abstract
{

	protected $_name = 'Z_NODE';
	protected $_sequence ='Z_NODE_NID_SEQ';
	protected $_primary ='NID';


  public function addNode($title,$nik,$type)
  { 
      
   
    $row = $this->createRow();
    if ($row) 
    {
      //$row->VID=$name;
      $row->TYPE=$type;
      //$row->LANG=$name;
      $row->TITLE=$title;
      $row->USER_ID=$nik;
      $row->STATUS=1;
      $row->CREATED=new Zend_Db_Expr(SYSDATE);
      $row->CHANGED=new Zend_Db_Expr(SYSDATE);
      	//$row->COMMENT_ID=$name;
      $row->PROMOTE=1;
      	//$row->MODERATE=$name;
      $row->STICKY=0;
      $row->TNID=0;
      $row->TRANSLATE=0;
      $row->save();
    //return the new user
      return $row->NID;
    } else {
      throw new Zend_Exception("Could not create user! ");
    }

  }


 	public function updateNode($nid,$title,$nik,$type,$vid)
  { 
//  	echo $title,' - ',$nik,' - ',$nid,' - ',$type,' - ',$vid;die();
    $row = $this->find($nid)->current();
    if($row) 
    {
      $row->CREATED=new Zend_Db_Expr(SYSDATE);
      $row->CHANGED=new Zend_Db_Expr(SYSDATE);
      $row->TYPE=$type;
      $row->TITLE=$title;
      $row->USER_ID=$nik;  
      $row->VID=$vid;                
      //$row->VID=;
      $row->save();
      //return the updated user
      return $row;
    } else{
      throw new Zend_Exception("Upload setting update failed!");
    }
  }

	public function updateVID($nid,$vid)
	{
	    $row = $this->find($nid)->current();
	    if($row) 
	    {
	      $row->VID=$vid;      
	      $row->save();
	      //return the updated user
	      return $row;
	    } else{
	      throw new Zend_Exception("Upload setting update failed!");
	    }
	}

	public function publishNode($nid,$operation)
	{
	    $row = $this->find($nid)->current();
	    if($row) 
	    {
	      $row->STATUS=$operation;      
	      $row->save();
	      //return the updated user
	      return $row;
	    }
	}

  public function deleteNode($nid)
  { 
    $row = $this->find($nid)->current();
    if($row) 
    {
      $row->delete();
      return 1;
    } else {
      return 0;
     
    }

  }
 	
  public function fetchPaginatorAdapter()
        {
        $query="SELECT * FROM Z_NODE";
        $items=	$this->_db->fetchAll($query);
            
         return $items;			
  }

//created by Fikri
	public function getLastNews(){
        $query="SELECT a.NID,b.VID,b.TITLE, b.BODY,TO_CHAR(b.TIMESTAMP,'DD/MM/YYYY HH24:MI') as TIMESTAMP,c.FULLNAME
                 FROM Z_NODE a, (SELECT * FROM Z_NODE_REVISIONS ORDER BY TIMESTAMP DESC) b,Z_USERS c
                 WHERE a.VID = b.VID 
                 AND a.USER_ID = c.ID 
                 AND a.TYPE = 'news' 
                 AND a.STATUS = '0' 
                 AND ROWNUM < 6 ORDER BY a.NID DESC, b.VID DESC, b.TIMESTAMP DESC";
        
        try {
			$items=	$this->_db->fetchAll($query);	
			
			} catch (Exception $e) {
				Zend_Debug::dump($e); die();
			}
        
		return $items;			
	}
	
	public function getNewsById($id){
        $query="SELECT a.NID,b.VID,b.TITLE, b.BODY,TO_CHAR(b.TIMESTAMP,'DD/MM/YYYY HH24:MI') as TIMESTAMP,c.FULLNAME
                 FROM Z_NODE a, Z_NODE_REVISIONS b,Z_USERS c
                 WHERE a.VID = b.VID 
                 AND b.VID = '".$id."' 
                 AND a.TYPE = 'news' 
                 AND a.USER_ID = c.ID";
        $items=	$this->_db->fetchAll($query);
		return $items;			
	}
//end of created by Fikri


}
             
