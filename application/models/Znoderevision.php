<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Znoderevision extends Zend_Db_Table_Abstract
{

  protected $_name = 'Z_NODE_REVISIONS';
	protected $_sequence ='Z_NODE_REVISIONS_VID_SEQ';
	protected $_primary ='VID';


  public function addNodeRev($title,$body,$nik,$nid)
  {
     ////  die($body);
//  	echo $title,' - ',$nik,' - ',$nid,'<br>',$body;die();
    /*comment by Fikri Apr 20th 2012
  	$row = $this->createRow();
//      print_r($row);die();
    if ($row) 
    { 
      $row->NID=$nid;
      //$row->LANG=$name;
      $row->USER_ID=$nik;
      $row->TITLE=$title;
      $row->BODY=$body;
      //$row->TEASER=;
      //$row->LOG=;
      $row->TIMESTAMP=new Zend_Db_Expr(SYSDATE);
      //$row->FORMAT=;
//      print_r($row);die();
      $row->save();
    //return the new user
    	echo $row->VID;die();
      return $row->VID;
    } else {
      throw new Zend_Exception("Could not create user! ");
    }*/
    $stmt = $this->_db->prepare("INSERT INTO Z_NODE_REVISIONS 
    			( VID, NID, USER_ID, TITLE, TIMESTAMP, BODY ) 
    			VALUES ( 
					Z_NODE_REVISIONS_VID_SEQ.nextVal, '".
    				$nid. "', '".
    				$nik. "', '".
    				$title. "', ".  
    				"TO_Date('".date('m/d/Y H:i:s'). "', 'MM/DD/YYYY HH24:MI:SS')". ", :clob)");
        $lob = oci_new_descriptor($this->getAdapter()->getConnection(), OCI_D_LOB);
        
        $stmt->bindParam(':clob', $lob, OCI_B_CLOB, -1);
        $lob->writeTemporary($body);
        $stmt->execute();
        $lob->close();
        //die("s");
    /*
    $_query1 = "INSERT INTO Z_NODE_REVISIONS 
    			( VID, NID, USER_ID, TITLE, TIMESTAMP, BODY2 ) 
    			VALUES ( 
					Z_NODE_REVISIONS_VID_SEQ.nextVal, '".
    				$nid. "', '".
    				$nik. "', '".
    				$title. "', ".  
    				"TO_Date('".date('m/d/Y H:i:s'). "', 'MM/DD/YYYY HH24:MI:SS')". ", '". 
    				$body. "')";  

	
//    	print_r($_query1);die();
    */
      //  $_res1 = $this->_db->query($_query1);
        //$check = (!empty($_res1))?true:false;
   //     if($check==true){
     
     return  ($this->_db->lastSequenceId('Z_NODE_REVISIONS_VID_SEQ'));
     
     
     
  }



  public function deleteNodeRev($nid)
  { 

    $condition = array('NID = ?' =>$nid );
    $delete=$this->delete($condition);		

    return $delete;

  }
 	
	public function fetchPaginatorAdapter($NID=null,$VID=null)
	{
		$query="SELECT R.TITLE,R.BODY,R.VID,R.NID,R.USER_ID,R.TIMESTAMP
						FROM Z_NODE_REVISIONS R,Z_NODE N
						WHERE R.VID=N.VID AND R.NID=N.NID";

		if (($NID) && ($VID))		
			{
				$query2=" AND R.VID='".$VID."' AND R.NID='".$NID."'";
			}				
		$allquery=$query.$query2;			
    $items=	$this->_db->fetchAll($allquery);

    return $items;			
	}

    
  public function getLastId(){
  		$_query = "SELECT * FROM (SELECT VID FROM Z_NODE_REVISIONS ORDER BY VID DESC) WHERE ROWNUM<=1";
  		$_res = $this->_db->query($_query);
  		foreach($_res as $item){
  			$data = $item['VID'];
  		}
  		return $data;
  }
 	
}
             
