<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Znodetype extends Zend_Db_Table_Abstract
{

  protected $_name = 'Z_NODE_TYPE';
	protected $_sequence ='Z_NODE_TYPE_ID_SEQ';
	protected $_primary ='ID';


  public function addNodeType($name,$desc)
  {
//  	echo $name.' '.$desc;die();
    $row = $this->createRow();
    if ($row) 
    {
		// update the row values
	    $row->TYPE=strtolower(str_replace(" ", "_", "$name"));
	    $row->NAME=$name;
	    $row->MODULE="node";
	    $row->DESCRIPTION=$desc;
		if (empty($name))
	      	$row->HAS_TITLE=0;
	    else
	      	$row->HAS_TITLE=1;
	      	
	    $row->TITLE_LABEL="Title";
		if (empty($name))
	      $row->HAS_BODY=0;
  		else
  		  $row->HAS_BODY=1;

  	  	$row->MIN_WORD_COUNT=strlen($desc);
	    $row->CUSTOM=1;
	    $row->MODIFIED=1;
	    $row->LOCKED=0;
	    $row->ORIG_TYPE=strtolower(str_replace(" ", "_", "$name"));
	    $row->save();
	    //return the new user
	    return $row;
	} else {
		throw new Zend_Exception("Could not create user! ");
	}
  }


 	public function updateNodeType($id,$name,$desc)
  { 
    $row = $this->find($id)->current();
    if($row) 
    {
      $row->NAME=$name;
      $row->DESCRIPTION=$desc;
      $row->save();
      //return the updated user
      return $row;
    } else{
      throw new Zend_Exception("Upload setting update failed!");
    }
  }

  public function deleteNodeType($id)
  { 
    $row = $this->find($id)->current();
    if($row) 
    {
      $row->delete();
      return 1;
    } else {
      return 0;
     
    }
  }
 	
  public function fetchPaginatorAdapter()
  {
	$query="SELECT * FROM Z_NODE_TYPE";
    $items=	$this->_db->fetchAll($query);
    return $items;			
  }

  public function cekTable($name)
  {
    $query="SELECT COUNT(NAME) FROM Z_NODE_TYPE WHERE NAME=".$name."";
    $items=	$this->_db->fetchOne($query);
  
    return $items;			
  } 	
 	
}
             
