<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Zpraba4api extends Zend_Db_Table_Abstract {
	
	public function conn_by_id($id) {
		$varC = $this->_db->fetchRow("select * from zpraba_conn where id =?", $id); 
		$conn = unserialize ( $varC ['conn_params'] );
		
		
		if (isset ( $varC ['desc'] ) && $varC ['desc'] != "") {
				$vf = explode ( ";", $varC ['desc'] );
				
				foreach ( $vf as $vz ) {
					$cf2 = explode ( "=", $vz );
					$conn [$cf2 [0]] = $cf2 [1];
				}
			}
		//Zend_Debug::dump($conn); die();
		switch (strtolower ( $conn ['adapter'] )) {
			case 'mysql' :
			case 'mysqli' :
			case 'pdo_mysql' :
			case 'pdo_sqlite' :
				$adapterdb = "msyql";
			break;
			
			case 'oracle':
			case 'pdo_oci':
				$adapterdb = "oracle";
			break;
			
			case 'pdo_pgsql':
				$adapterdb = "postgresql";
			break;
	
			
		
		}
		
		if (strtolower ( $conn ['adapter'] ) == 'mysql' || strtolower ( $conn ['adapter'] ) == 'mysqli' || strtolower ( $conn ['adapter'] ) == 'pdo_mysql' || strtolower ( $conn ['adapter'] ) == 'pdo_sqlite') {
			$adapterdb = "msyql";
		} elseif (strtolower ( $conn ['adapter'] ) == 'oracle' || strtolower ( $conn ['adapter'] ) == 'pdo_oci') {
			$adapterdb = "oracle";
		} elseif (strtolower ( $conn ['adapter'] ) == 'pdo_pgsql') {
			$adapterdb = "postgresql";
		} 

	
		
		if (isset ( $data ['optionals'] ) && $data ['optionals'] != "") {
			$vf = explode ( ";", $data ['optionals'] );
			foreach ( $vf as $vz ) {
				$cf2 = explode ( "=", $vz );
				$conn [$cf2 [0]] = $cf2 [1];
			}
		}
		$adapter = $conn ['adapter'];
		unset ( $data ['optionals'] );
		unset ( $conn ['adapter'] );
		if ($adapter == 'Pdo_Netezza') {
			unset ( $conn ['host'] );
			unset ( $conn ['port'] );
		}
		
		if ($adapter == 'Pdo_Mssql') {
			$conn ['pdoType'] = ' dblib';
		}
		if($adapter!="Php_File") {
			$dbz = Zend_Db::factory ( $adapter, $conn );
			return $dbz;
		}
		return false;
	}	
		
	public function execute_api($conn, $data, $xin = array()) {
		//Zend_Debug::dump($conn); die();
		$authAdapter = Zend_Auth::getInstance ();
		$identity = $authAdapter->getIdentity ();
		#Zend_Debug::dump($xin); die();
		if($data['conn_name']=='call4action') {
			$clas  = new Model_Zprabawf();
			return  $clas->get_wf_process($data['act_id'], $xin);
		}

		if ($data ['is_deactivate'] == 1) {
			return array (
					'transaction' => false,
					'message' => 'dbconnection was determinated',
					'data' => "",
					'sql' => "",
					'parsing_data_time' => "",
					'cache_time' => "",
					'is_cache' => "",
					'adapter' => $conn ['adapter'],
					'host' => $conn ['host'] 
			);
		}
		
		if ($data ['ping_b4_call'] == 1) {
			
			if (! $this->pingserver ( $conn )) {
				return array (
						'transaction' => false,
						'message' => 'can not connect to host '.$conn ['host'],
						'data' => "",
						'sql' => "",
						'parsing_data_time' => "",
						'cache_time' => "",
						'is_cache' => "",
						'adapter' => $conn ['adapter'],
						'host' => $conn ['host'] 
				);
			}
		}
		
		$sql_raw = $data ['sql_text'];
		$ti = array ();
		if (count ( $xin ) > 0) {
			foreach ( $xin as $k => $vv ) {
				$ti [':' . $k] = $vv;
			}
		}
		
		$xinArr = "";
		if (count ( $xin ) > 0) {
			$xinArr = implode ( '_', $xin );
			foreach ( $xin as $k => $q ) {
				$qarin = explode ( ':', $q );
				
				$data ['sql_text'] = str_replace ( ':' . $k, $q, $data ['sql_text'] );
			}
		}
		
		
		foreach ( $xin as $k => $q ) {
				$qarin = explode ( ':', $q );
				
				$data ['sql_text'] = str_replace ( ':' . $k, $q, $data ['sql_text'] );
		}
		
		foreach ($identity as $kk => $vv)
		{
			$data ['sql_text'] = str_replace ( ':SESSION_' . $kk, $vv, $data ['sql_text'] );
		}
		
		
	$cck = new CMS_General();	
	if($data ['api_str_replace'] != NULL){
		
		$i=1;
		$sql="";
		for($i;$i<=$data['api_str_replace'];)
		{
			
		  $vara =  $cck->get_string_between($data ['sql_text'], '[php'.$i.']', '[/php'.$i.']');
		 // die($vara);
		  $data ['sql_text'] = $cck->replaceTags($data ['sql_text'], '[php'.$i.']', '[/php'.$i.']', "");
		  $data ['sql_text']= str_replace( '[php'.$i.']', "", $data ['sql_text']);
		  $data ['sql_text'] =str_replace( '[/php'.$i.']', "", $data ['sql_text']);
		  //die($vara);
		  eval($vara);
		  $data ['sql_text'] = $data ['sql_text'] .$sql ; 
		  
		  
		  $sql="";
		  $i++;
		}
 //   echo $data ['sql_text']; die();
    }
		
		$cache_id = 'api_' .$data ['api_type'] .'_'.  $data ['id'] . '_' . $xinArr;
		
		$cache_id = str_replace ( array (
				"/",
				",",
				"%",
				" ",
				".",
				">",
				"<",
				"-",
				":",
				")",
				"(" 
		), "z", $cache_id );
		
		$ncache = $this->cachefunc ( $data );
		
		$iscache = "true";
		$dataRet = array ();
		$dataRet ['data'] = "";
		$dataRet ['parse_time'] = "0";
	//echo $data ['sql_text'];
		$justdata = $ncache->load ( $cache_id );
		if($data['api_mode']==1) {$data['id']="";}	
		if (! ($justdata) or $data['id']=="") {
		
			try {
				$iscache = "false";
				$dataRet = $this->execute_api_type ( $conn, $data, $xin, $sql_raw, $ti );
				$data4 = array (
						'transaction' => $dataRet ['transaction'],
						'message' => $dataRet ['message'],
						'data' => $dataRet ['data'],
						'sql' => $data ['sql_text'],
						'parsing_data_time' => $dataRet ['parse_time'],
						'cache_time' => $data ['cache_time'],
						'is_cache' => $iscache,
						'adapter' => $conn ['adapter'],
						'host' => $conn ['host'] 
				);
			} catch ( Exception $e ) {
				$data4 = array (
						'transaction' => false,
						'message' => $e,
						'data' => "",
						'sql' => $data ['sql_text'],
						'parsing_data_time' => $dataRet ['parse_time'],
						'cache_time' => $data ['cache_time'],
						'is_cache' => $iscache,
						'adapter' => $conn ['adapter'],
						'host' => $conn ['host'] 
				);
			}
			
			$ncache->save ( $dataRet ['data'], $cache_id, array (
					'api',
					'api_' . $data ['id'] 
			) );
		} else {
			//die("2");
			$data4 = array (
					'transaction' => true,
					'message' => "success from cache",
					'data' => $justdata,
					'sql' => $data ['sql_text'],
					'parsing_data_time' => $dataRet ['parse_time'],
					'cache_time' => $data ['cache_time'],
					'is_cache' => $iscache,
					'adapter' => $conn ['adapter'],
					'host' => $conn ['host'] 
			);
		}
		
		return $data4;
	}
	
	public function exec_api($conn, $data, $xin = array()) {
		// Zend_Debug::dump($data); die();
		$authAdapter = Zend_Auth::getInstance ();
		$identity = $authAdapter->getIdentity ();
		// Zend_Debug::dump($xin); die();
		if($data['conn_name']=='call4action') {
			$clas  = new Model_Zprabawf();
			return  $clas->get_wf_process($data['act_id'], $xin);
		}

		if ($data ['is_deactivate'] == 1) {
			return array (
					'transaction' => false,
					'message' => 'dbconnection was determinated',
					'data' => "",
					'sql' => "",
					'parsing_data_time' => "",
					'cache_time' => "",
					'is_cache' => "",
					'adapter' => $conn ['adapter'],
					'host' => $conn ['host'] 
			);
		}
		
		if ($data ['ping_b4_call'] == 1) {
			
			if (! $this->pingserver ( $conn )) {
				return array (
						'transaction' => false,
						'message' => 'can not connect to host '.$conn ['host'],
						'data' => "",
						'sql' => "",
						'parsing_data_time' => "",
						'cache_time' => "",
						'is_cache' => "",
						'adapter' => $conn ['adapter'],
						'host' => $conn ['host'] 
				);
			}
		}
		
		$sql_raw = $data ['sql_text'];
		$ti = array ();
		if (count ( $xin ) > 0) {
			foreach ( $xin as $k => $vv ) {
				$ti [':' . $k] = $vv;
			}
		}

		// added by randy 20180116
		// Zend_Debug::dump($_POST);die();
		$postArr = "";
		if(isset($_POST) && count($_POST)>0){
			unset($_POST['sEcho']);
			$postArr = implode('_', $_POST);
		}
		// Zend_Debug::dump($postArr);die();
		//=======================
		
		$xinArr = "";
		if (count ( $xin ) > 0) {
			$xinArr = implode ( '_', $xin );
			foreach ( $xin as $k => $q ) {
				$qarin = explode ( ':', $q );
				
				$data ['sql_text'] = str_replace ( ':' . $k, $q, $data ['sql_text'] );
			}
		}
		
		
		foreach ( $xin as $k => $q ) {
				$qarin = explode ( ':', $q );
				
				$data ['sql_text'] = str_replace ( ':' . $k, $q, $data ['sql_text'] );
		}
		
		foreach ($identity as $kk => $vv)
		{
			$data ['sql_text'] = str_replace ( ':SESSION_' . $kk, $vv, $data ['sql_text'] );
		}
		
		
	$cck = new CMS_General();
	// Zend_Debug::dump($data ['api_str_replace'] != NULL);die();
	if($data ['api_str_replace'] != NULL){
		// Zend_Debug::dump($data); die();
		$i=1;
		$sql="";
		for($i;$i<=$data['api_str_replace'];)
		{
			
		  $vara =  $cck->get_string_between($data ['sql_text'], '[php'.$i.']', '[/php'.$i.']');
		 // die($vara);
		  $data ['sql_text'] = $cck->replaceTags($data ['sql_text'], '[php'.$i.']', '[/php'.$i.']', "");
		  $data ['sql_text']= str_replace( '[php'.$i.']', "", $data ['sql_text']);
		  $data ['sql_text'] =str_replace( '[/php'.$i.']', "", $data ['sql_text']);
		  //die($vara);
		  eval($vara);
		  $data ['sql_text'] = $data ['sql_text'] .$sql ; 
		  
		  
		  $sql="";
		  $i++;
		}
		// echo $data ['sql_text']; die();
    }
	// die("qqq");
		
		$z = new Model_Cache();
		$inputcache = array($data ['api_type'],$data ['id'],$xinArr,$postArr);
		$cache_id = $z->get_id('PrabacAPI',$inputcache);
		// Zend_Debug::dump($cache_id);die();
		$ncache = $this->cachefunc ( $data );
		// Zend_Debug::dump($ncache);die();
		
		$iscache = "true";
		$dataRet = array ();
		$dataRet ['data'] = "";
		$dataRet ['parse_time'] = "0";
		// echo $data ['sql_text'];die();

		// Zend_Debug::dump($data);die('www');
		// $justdata = false;
		// if($data['cache_time']!=0 || $data['cache_time']!="" || $data['cache_time']!=null){
			$justdata = $ncache->load ( $cache_id );
		// }
		// Zend_Debug::dump($data);//die();
		// Zend_Debug::dump($justdata);die();
		if($data['api_mode']==1) {$data['id']="";}	
		if (! ($justdata) or $data['id']=="") {
		
			try {
				$iscache = "false";
				$dataRet = $this->execute_api_type ( $conn, $data, $xin, $sql_raw, $ti );
				// Zend_Debug::dump($dataRet);die('x');
				$data4 = array (
						'transaction' => $dataRet ['transaction'],
						'message' => $dataRet ['message'],
						'data' => $dataRet ['data'],
						'sql' => $data ['sql_text'],
						'parsing_data_time' => $dataRet ['parse_time'],
						'cache_time' => $data ['cache_time'],
						'last_cache' => null, //added by randy 20180116
						'is_cache' => $iscache,
						'adapter' => $conn ['adapter'],
						'host' => $conn ['host'] 
				);
			} catch ( Exception $e ) {
				$data4 = array (
						'transaction' => false,
						'message' => $e,
						'data' => "",
						'sql' => $data ['sql_text'],
						'parsing_data_time' => $dataRet ['parse_time'],
						'cache_time' => $data ['cache_time'],
						'last_cache' => null, //added by randy 20180116
						'is_cache' => $iscache,
						'adapter' => $conn ['adapter'],
						'host' => $conn ['host'] 
				);
			}
			
			$ncache->save ( $dataRet ['data'], $cache_id, array (
					'api',
					'api_' . $data ['id'] 
			) );
		} else {
			// die("2");

			// added by randy 20180116
			$filemtime = 0;
			if(file_exists(APPLICATION_PATH.'/../cache/zend_cache---'.$cache_id)){
				$filemtime = filemtime(APPLICATION_PATH.'/../cache/zend_cache---'.$cache_id);
			}
			//========================
			

			$data4 = array (
					'transaction' => true,
					'message' => "success from cache",
					'data' => $justdata,
					'sql' => $data ['sql_text'],
					'parsing_data_time' => $dataRet ['parse_time'],
					'cache_time' => $data ['cache_time'],
					'last_cache' => ($filemtime>0?date('Y-m-d H:i:s', $filemtime):null), //added by randy 20180116
					'is_cache' => $iscache,
					'adapter' => $conn ['adapter'],
					'host' => $conn ['host'] 
			);
		}
		
		return $data4;
	}
	
	function execute_api_type($conn, $data, $xin = array(), $sql_raw, $ti) {
		//Zend_Debug::dump($conn); die();
		switch (strtolower ( $conn ['adapter'] )) {
			case 'mysql' :
			case 'mysqli' :
			case 'pdo_mysql' :
			case 'pdo_sqlite' :
				$adapterdb = "msyql";
			break;
			
			case 'oracle':
			case 'pdo_oci':
				$adapterdb = "oracle";
			break;
			
			case 'pdo_pgsql':
				$adapterdb = "postgresql";
			break;
			
			case 'wsdl':
			
				define ( 'PAGE_PARSE_START_TIME', microtime () );
				$c = new Model_Zprabaservices();
				$xdata = $c->wsdl($conn, $data, $xin = array(), $sql_raw, $ti);
				define ( 'PAGE_PARSE_END_TIME', microtime () );
				$time_end = explode ( ' ', PAGE_PARSE_END_TIME );
				$time_start = explode ( ' ', PAGE_PARSE_START_TIME );
				// echo $transaction; die();
				$parse_time = number_format ( ($time_end [1] + $time_end [0] - ($time_start [1] + $time_start [0])), 3 );
				return array (
						"data" => $xdata['Arr'],
						"transaction" => $xdata['transaction'],
						"message" => $xdata['message'],
						'parse_time' => $parse_time,
						'sql_text' => $data ['sql_text'] 
				);
				
				
			
			break;
			
			case 'urljson':
			
				define ( 'PAGE_PARSE_START_TIME', microtime () );
				$c = new Model_Zprabaservices();
				$xdata = $c->urljson($conn, $data, $xin = array(), $sql_raw, $ti);
				define ( 'PAGE_PARSE_END_TIME', microtime () );
				$time_end = explode ( ' ', PAGE_PARSE_END_TIME );
				$time_start = explode ( ' ', PAGE_PARSE_START_TIME );
				// echo $transaction; die();
				$parse_time = number_format ( ($time_end [1] + $time_end [0] - ($time_start [1] + $time_start [0])), 3 );
				return array (
						"data" => $xdata['Arr'],
						"transaction" => $xdata['transaction'],
						"message" => $xdata['message'],
						'parse_time' => $parse_time,
						'sql_text' => $data ['sql_text'] 
				);
				
				
			
			break;
		}
		
		if (strtolower ( $conn ['adapter'] ) == 'mysql' || strtolower ( $conn ['adapter'] ) == 'mysqli' || strtolower ( $conn ['adapter'] ) == 'pdo_mysql' || strtolower ( $conn ['adapter'] ) == 'pdo_sqlite') {
			$adapterdb = "msyql";
		} elseif (strtolower ( $conn ['adapter'] ) == 'oracle' || strtolower ( $conn ['adapter'] ) == 'pdo_oci') {
			$adapterdb = "oracle";
		} elseif (strtolower ( $conn ['adapter'] ) == 'pdo_pgsql') {
			$adapterdb = "postgresql";
		} 

		else {
			// return array("data"=>"", "transaction"=>false, "message"=>"adapter db not yet supported", 'parse_time'=>"");
		}
		
		if (isset ( $data ['optionals'] ) && $data ['optionals'] != "") {
			$vf = explode ( ";", $data ['optionals'] );
			foreach ( $vf as $vz ) {
				$cf2 = explode ( "=", $vz );
				$conn [$cf2 [0]] = $cf2 [1];
			}
		}
		$adapter = $conn ['adapter'];
		unset ( $data ['optionals'] );
		unset ( $conn ['adapter'] );
		if ($adapter == 'Pdo_Netezza') {
			unset ( $conn ['host'] );
			unset ( $conn ['port'] );
		}
		
		if ($adapter == 'Pdo_Mssql') {
			$conn ['pdoType'] = ' dblib';
		}
		if($adapter!="Php_File") {
			$dbz = Zend_Db::factory ( $adapter, $conn );
		}
		define ( 'PAGE_PARSE_START_TIME', microtime () );
		switch ($data ['api_type']) {
			case "1" :
			default :
				// die("s");
				
				$iscache = "false";
				switch ($data ['api_mode']) {
					
					case "1" :
						$Arr = array ();
						try {
							$dbz->query ( $data ['sql_text'] );
							$message = "success";
							$transaction = true;
						} catch ( Exception $e ) {
							$transaction = false;
							$message = $e->getMessage ();
						}
						
						break;
					
					case "2" :
					default :
						// die("s");
						$Arr = array ();
						try {
							$Arr = $dbz->fetchAll ( $data ['sql_text'] );
							// Zend_Debug::dump($Arr); die();
							$message = "success";
							$transaction = true;
						} catch ( Exception $e ) {
							// Zend_Debug::dump($e->getMessage()); die();
							$transaction = false;
							$message = $e->getMessage ();
						}
						
						break;
					
					case "3" :
						$Arr = array ();
						try {
							$Arr = $dbz->fetchRow ( $data ['sql_text'] );
							$message = "success";
							$transaction = true;
						} catch ( Exception $e ) {
							$transaction = false;
							$message = $e->getMessage ();
						}
						
						break;
					
					case "4" :
						$Arr = null;
						try {
							$Arr = $dbz->fetchOne ( $data ['sql_text'] );
							$message = "success";
							$transaction = true;
						} catch ( Exception $e ) {
							$transaction = false;
							$message = $e->getMessage ();
						}
						break;
					case "5" :
						
						$Arr = null;
						try {
							
							// $Arr = $dbz->fetchAll($data['sql_text']);
							// Zend_Debug::dump($Arr); die();
							$stmt = $dbz->query ( $data ['sql_text'] );
							$stmt->setFetchMode ( Zend_Db::FETCH_NUM );
							$Arr = $stmt->fetchAll ();
							if($data ['api_mode']=="5") {
								//$new=array();
								//foreach ($Arr as $v) {
									//$new[] = $v;
								//}
								
								//$Arr =$new;	
							}
							
							//Zend_Debug::dump($Arr); die();
							$message = "success";
							$transaction = true;
						} catch ( Exception $e ) {
							$transaction = false;
							$message = $e->getMessage ();
							// Zend_Debug::dump($e->getMessage()); die();
						}
						break;
				}
				
				define ( 'PAGE_PARSE_END_TIME', microtime () );
				$time_end = explode ( ' ', PAGE_PARSE_END_TIME );
				$time_start = explode ( ' ', PAGE_PARSE_START_TIME );
				// echo $transaction; die();
				$parse_time = number_format ( ($time_end [1] + $time_end [0] - ($time_start [1] + $time_start [0])), 3 );
				return array (
						"data" => $Arr,
						"transaction" => $transaction,
						"message" => $message,
						'parse_time' => $parse_time,
						'sql_text' => $data ['sql_text'] 
				);
				break;
			
			case "4" :
				$vModel = explode ( "::", $data ['sql_text'] );
				//Zend_Debug::dump($vModel); die();
				$newObject = $vModel [0];
				$obj = new $newObject ();
				$handler = array (
						$obj,
						$vModel [1] 
				);
				if (is_callable ( $handler )) {
					define ( 'PAGE_PARSE_START_TIME', microtime () );
					$Arr = call_user_func_array ( $handler, array($xin) );
					//Zend_Debug::dump($Arr); die();
					define ( 'PAGE_PARSE_END_TIME', microtime () );
					$time_end = explode ( ' ', PAGE_PARSE_END_TIME );
					$time_start = explode ( ' ', PAGE_PARSE_START_TIME );
					$parse_time = number_format ( ($time_end [1] + $time_end [0] - ($time_start [1] + $time_start [0])), 3 );
					return array (
							"data" => $Arr ['data'],
							"transaction" => $Arr ['transaction'],
							"message" => $Arr ['message'],
							'parse_time' => $parse_time 
					);
				}
				return array (
						"data" => "",
						"transaction" => false,
						"message" => "unknown class",
						'parse_time' => $parse_time 
				);
				break;
			
			case "2" :
				
				define ( 'PAGE_PARSE_START_TIME', microtime () );
				switch ($adapterdb) {
					case "mysql" :
					default :
						$dbz = Zend_Db::factory ( $adapter, $conn );
						$Arr = array ();
						try {
							
							foreach ( $xin as $vkey => $vxin ) {
								$cek = strpos ( $sql_raw, $vkey );
								if ($cek !== false) {
									$sql_raw = str_replace ( ":" . $vkey, "?", $sql_raw );
									$varin [$vkey] = $vxin;
								}
							}
							
							$stmt = $dbz->query ( "CALL " . $sql_raw, $varin );
							$Arr = $stmt->fetchAll ();
							if ($data ['api_mode'] == 5) {
								$x = 0;
								$newa = array ();
								$chu = array_keys ( $Arr [0] );
								foreach ( $Arr as $k => $v ) {
									foreach ( $chu as $c ) {
										$newa [$x] [] = $v [$c];
									}
									
									$x ++;
								}
								$Arr = $newa;
							}
							$message = "success";
							$transaction = true;
						} catch ( Exception $e ) {
							$transaction = false;
							$message = $e->getMessage ();
							// Zend_Debug::dump($message); die();
						}
						
						break;
					
					case "oracle" :
						
						$sql = "BEGIN " . $sql_raw . "; END;";
						// die($sql);
						$dbz = Zend_Db::factory ( $adapter, $conn );
						try {
							$stmt = $dbz->prepare ( $sql );
							$out = sprintf ( "%20s", "" );
							$arrout = array (
									$data ['attrs1'] => &$out 
							);
							// $xin=array('WHO'=>'DEDEB');
							$params = array_merge ( $xin, $arrout );
							
							$stmt->execute ( $params );
							// Zend_Debug::dump($params); die("aa");
							$Arr = $params [$data ['attrs1']];
							$message = "success";
							$transaction = true;
						} catch ( Exception $e ) {
							// Zend_Debug ::dump($e->getMessage()); die("s");
							$transaction = false;
							$message = $e->getMessage ();
						}
						
						break;
				}
				
				define ( 'PAGE_PARSE_END_TIME', microtime () );
				$time_end = explode ( ' ', PAGE_PARSE_END_TIME );
				$time_start = explode ( ' ', PAGE_PARSE_START_TIME );
				$parse_time = number_format ( ($time_end [1] + $time_end [0] - ($time_start [1] + $time_start [0])), 3 );
				
				return array (
						"data" => $Arr,
						"transaction" => $transaction,
						"message" => $message,
						'parse_time' => $parse_time,
						'sql_text' => $sql 
				);
				
				break;
			
			case 3 :
				define ( 'PAGE_PARSE_START_TIME', microtime () );
				
				switch ($adapterdb) {
					
					case "oracle" :
						$sql = "BEGIN " . $sql_raw . "; END;";
						// die($sql);
						$dbz = Zend_Db::factory ( $adapter, $conn );
						try {
							$Arr = $dbz->execCursor ( $sql, $data ['attrs1'], $ti )->fetchCursorWithLimit ();
							if ($data ['api_mode'] == 5) {
								$x = 0;
								$newa = array ();
								$chu = array_keys ( $Arr [0] );
								foreach ( $Arr as $k => $v ) {
									foreach ( $chu as $c ) {
										$newa [$x] [] = $v [$c];
									}
									
									$x ++;
								}
								$Arr = $newa;
							}
							
							$message = "success";
							$transaction = true;
						} catch ( Exception $e ) {
							$transaction = false;
							$message = $e->getMessage ();
						}
						
						break;
				}
				
				define ( 'PAGE_PARSE_END_TIME', microtime () );
				$time_end = explode ( ' ', PAGE_PARSE_END_TIME );
				$time_start = explode ( ' ', PAGE_PARSE_START_TIME );
				$parse_time = number_format ( ($time_end [1] + $time_end [0] - ($time_start [1] + $time_start [0])), 3 );
				
				return array (
						"data" => $Arr,
						"transaction" => $transaction,
						"message" => $message,
						'parse_time' => $parse_time,
						'sql_text' => $sql 
				);
				break;
		}
	}
	function pingserver($conn) {
		$def = array (
				'Pdo_Mssql' => '1433',
				'Mysql' => '3306',
				'Mysqli' => '3306',
				'Pdo_mysql' => '3306',
				'Oracle' => '1521',
				'Pdo_oci' => '1521',
				'Pdo_Pgsql' => '5432',
				'Pdo_Netezza' => '5480' 
		);
		
		if (isset ( $conn ['port'] ) && $conn ['port'] != "") {
			$port = $conn ['port'];
		} else {
			$port = $def [$conn ['adapter']];
		}
		
		if ($fp = @fsockopen ( $conn ['host'], $port, $errno, $errstr, 2 )) {
			
			@fclose ( $fp );
			return true;
		} else {
			return false;
		}
	}
	function cachefunc($data) {
		
		// Zend_Debug::dump($xin); die("");
		/*
		 * $frontendOpts = array( 'caching' => true, 'lifetime' => 1800, 'automatic_serialization' => true ); $backendOpts = array( 'servers' =>array( array( 'host' => 'localhost', 'port' => 11211 ) ), 'compression' => false ); $cache = Zend_Cache::factory('Core', 'Memcached', $frontendOpts, $backendOpts);
		 */
		$cache_time = 3600;
		if($data ['cache_time']==""){$data ['cache_time']=0;}
		if ($data ['cache_time'] == '~') {
			$front = array (
					'lifetime' => NULL,
					'automatic_serialization' => true 
			);
		} elseif ($data ['cache_time'] == '0') {
			$front = array (
					'caching' => FALSE,
					'lifetime' => ( int ) $data ['cache_time'],
					'automatic_serialization' => true 
			);
		} elseif ($data ['cache_time'] != NULL) {
			
			$front = array (
					'lifetime' => ( int ) $data ['cache_time'],
					'automatic_serialization' => true 
			);
		} else {
			$front = array (
					'lifetime' => $cache_time,
					'automatic_serialization' => true 
			);
		}
		
		switch ($data ['backendcache']) {
			case 'file' :
			default :
				$back = array (
						'cache_dir' => APPLICATION_PATH . '/../cache/' 
				);
				
				return Zend_Cache::factory ( 'Core', 'File', $front, $back );
				
				break;
			
			case 'memcached' :
				$backendOpts = array (
						'servers' => array (
								array (
										'host' => 'localhost',
										'port' => 11211 
								) 
						),
						'compression' => false 
				);
				
				return Zend_Cache::factory ( 'Core', 'Memcached', $front, $backendOpts );
				
				break;
			
			case 'mongodb' :
				$backendOptions = array (
						'database_name' => 'zend_cache',
						'collection' => 'cache' 
				);
				return Zend_Cache::factory ( 'Core', 'MongoDb', $front, $backendOptions );
				
				break;
			
			case 'libmemcached' :
				$backendOptions = array (
						'database_name' => 'zend_cache',
						'collection' => 'cache' 
				);
				return Zend_Cache::factory ( 'Core', 'Libmemcached', $front, array () );
				
				break;
		}
	}
}
