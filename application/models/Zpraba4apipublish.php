<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Zpraba4apipublish extends Zend_Db_Table_Abstract {
	
	public function get_users_api($data) {
		// Zend_Debug::dump($ip); die("s");
		$sql = "select a.*, c.user_ip, b.is_show_sql, b.is_restricted_ip, b.is_show_parsing_data from zpraba_api a
			inner join
		zpraba_api_users b ON b.api_id = a.id
			inner join
		zpraba_users_api c ON c.id = b.api_user_id 
			where c.user_name=?   and a.id=?  and b.publish_type=1";
		// die($sql);
		try {
			$datax = $this->_db->fetchRow ( $sql, array (
					$data ['username'],
					$data ['api_id'] 
			) );
			
			return $datax;
			
			// Zend_Debug::dump($datax);
			// die();
		} catch ( Exception $e ) {
			Zend_Debug::dump ( $e );
			die ( "s" );
		}
	}
	
	public function get_users_action($data) {
		// Zend_Debug::dump($ip); die("s");
		$sql = "select a.*, c.user_ip, b.is_show_sql, b.is_restricted_ip, b.is_show_parsing_data from zpraba_action a
			inner join
		zpraba_api_users b ON b.api_id = a.id
			inner join
		zpraba_users_api c ON c.id = b.api_user_id 
			where c.user_name=?   and a.id=?  and b.publish_type=2";
		// die($sql);
		try {
			$datax = $this->_db->fetchRow ( $sql, array (
					$data ['username'],
					$data ['api_id'] 
			) );
			
			return $datax;
			
			// Zend_Debug::dump($datax);
			// die();
		} catch ( Exception $e ) {
			Zend_Debug::dump ( $e );
			//die ( "s" );
		}
	}
}
