<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Zprabacrons extends Zend_Db_Table_Abstract{
	
    public function get_all_allerts_v1($cat = NULL, $me=null) {
        $sql = null;
        $sql2 = null;
        $sql3 = null;
        if ($cat) {
            $sql = " AND ALERT_TYPE LIKE '%$cat%' ";
        }
        
        if($me) 
        {
			
		}															
				
        $query = "select * from zpraba_crons where 1=1 $sql  order by id desc ";
		try {
			$items = $this->_db->fetchAll($query);
		} catch (Exception $e){
			Zend_Debug::dump($e); die();
		}
        
        return $items;
    }
    public function createdallert($data) {
	  		
		
			($data['SMS']) ? $IS_SMS= $data['SMS'] : $IS_SMS = NULL;
			($data['EMAIL']) ? $IS_EMAIL = $data['EMAIL'] : $IS_EMAIL = NULL;
			($data['INBOX']) ? $IS_INBOX = $data['INBOX'] : $IS_INBOX = NULL;
			//$a_type = $IS_SMS.",".$IS_EMAIL.",".$IS_INBOX; 
			$type = array($IS_SMS, $IS_EMAIL, $IS_INBOX); 
			
			$vartype = implode(',', array_filter($type));	
			//Zend_Debug::dump($vartype); die();
			
			$formula = str_replace("'", '"', $data[ALERT_FORMULA]);
        try {

            $sql = "INSERT INTO Z_ALERTS (ID,ALERT_NAME, ALERT_FORMULA, ALERT_TYPE, ZROLES, 
            				ALERT_TEXT,  CHECKING_TYPE, CONNECTION, CRON_FORMULA, LOG_OBJECT_NAME, STATUS
            				) 
            VALUES (Z_ALERTS_ID_SEQ.NEXTVAL, '" . $data[ALERT_NAME] . "','" .$formula  . "' 
            ,'".$vartype."'
            , '" . $data[ROLE] . "', '" . $data[ALERT_TEXT] . "', '" . $data[CHECKINGT_TYPE] . "',
            '" . $data[CONNECTION] . "', '" . $data[CRON_FORMULA] . "', 
            '" . $data[LOG_OBJECT_NAME] . "', '".$data['STATUS']."' )";

           // die($sql);
            $this->_db->query($sql);
            $aid = $this->_db->lastSequenceId('Z_ALERTS_ID_SEQ');        

					return $aid;
        } catch (Exception $e) {
            return false;
        }
    }
    
    
    public function get_all_allerts($cat = NULL, $status=null) {
        $sql = null;
        $sql2 = null;
        $sql3 = null;
        if ($cat) {
            $sql = " AND ALERT_TYPE =$cat ";
        }
        
        if($status) 
        {
					$sql2 = "AND STATUS =1";
		}															
				
        $query = "SELECT * FROM Z_ALERTS WHERE 1=1   
         $sql  $sql2 ORDER BY ID DESC ";
        #die($query);
        $items = $this->_db->fetchAll($query);
        return $items;
    }
    
    public function get_allert_by_id($id) {
        $query = "SELECT ID, ALERT_NAME, ALERT_FORMULA, ALERT_TYPE, ZROLES, 
        ALERT_TEXT, CHECKING_TYPE, CONNECTION, CRON_FORMULA,  
        LOG_OBJECT_NAME, STATUS   
        FROM Z_ALERTS WHERE ID= $id ";
        #die($query);
        $items = $this->_db->fetchRow($query);
        return $items;
    }
 
	
	public function insertdoc($data) {
	
        ($data['SID']) ? $IS_SID= $data['SID'] : $IS_SID = 'NULL';
       
        try {


            $sql = "INSERT INTO PM_DOCUMENTS (FID, DOC_NAME, DOC_SIZE, RAW_URL, SID, PID, CREATED_DATE) VALUES ('" . $data[FID] . "','" . $data[DOC_NAME] . "' ,'" . $data[DOC_SIZE] . "','" . 
			$data[RAW_URL] . "', $IS_SID, ".$data[PID].", SYSDATE)";
            //die($sql);
            $row = $this->_db->query($sql);
            $id = $this->_db->lastSequenceId('PM_DOCS_SEQ');

     
            return $id;
        } catch (Exception $e) {
            return false;
        }
    }


	public function deleteallert($id) {
	   $cache = Zend_Registry::get('cache');

        try {
            $sql = "DELETE FROM Z_ALERTS WHERE ID=$id";
            #die($sql);
            $this->_db->query($sql);
            $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array('PM_SERVICE'));
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
    
     public function updateallert($data) {
			($data['SMS']) ? $IS_SMS= $data['SMS'] : $IS_SMS = NULL;
			($data['EMAIL']) ? $IS_EMAIL = $data['EMAIL'] : $IS_EMAIL = NULL;
			($data['INBOX']) ? $IS_INBOX = $data['INBOX'] : $IS_INBOX = NULL;
			$type = array($IS_SMS, $IS_EMAIL, $IS_INBOX); 
			
			$vartype = implode(',', array_filter($type));	
			$formula = str_replace("'", '"', $data[ALERT_FORMULA]);
    	 
        try {
					$sql = "UPDATE Z_ALERTS 
									SET ALERT_NAME = '" . $data[ALERT_NAME] . "', 
									ALERT_FORMULA='" . $formula . "',  						
									ZROLES='" . $data['ROLE'] . "',
									ALERT_TYPE='".$vartype."',  
									ALERT_TEXT='" . $data[ALERT_TEXT] . "', 
									CHECKING_TYPE= '" . $data[CHECKINGT_TYPE] . "', 
									CONNECTION='" . $data[CONNECTION] . "', 
									CRON_FORMULA = '" . $data[CRON_FORMULA] . "',  
									LOG_OBJECT_NAME = '" . $data[LOG_OBJECT_NAME] . "' ,
									STATUS = '" . $data[STATUS] . "' 
									
									WHERE ID=". $data[PID] ."";
			
            //die($sql);
                     
          $this->_db->query($sql);
				
            return true;
        } catch (Exception $e) {
            return false;
        }  
	}
	
}

