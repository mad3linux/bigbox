<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Zprabaction extends Zend_Db_Table_Abstract{

public function get_process_clone($id) {
	try {
		
		$q1 = "select  * from zpraba_action where id=? ";
	
		$q2 = "select  * from zpraba_process where act_id=? ";
	
		
		$data1 = $this->_db->fetchRow($q1, $id);
		$data2 = $this->_db->fetchAll($q2, $id);
		
		$this->_db->query("insert into zpraba_action (action_name, act_type, attr1) values(?, ?, ?) ", array($data1['action_name'], $data1['act_type'], $data1['attr1']));
		$actid = $this->_db->lastInsertId();
		$vz=array();
		$oldpid=array();
		foreach ($data2 as $v) {
				
			$this->_db->query("insert into zpraba_process (act_id, process_name, p_type, class_type, ext_id, if_false, param_process, zparams_serialize, offset_top, offset_left) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", array(
			$actid, $v['process_name'], $v['p_type'], $v['class_type'], $v['ext_id'], $v['if_false'], $v['param_process'], $v['zparams_serialize'], $v['offset_top'], $v['offset_left']));
			$pid =$this->_db->lastInsertId();
			$oldpid[] = $v['pid'];
			$vz[$v['pid']] =$pid; 	
		}
		
		$lpid= implode(",", $oldpid);
		//Zend_Debug::dump($lpid);die();
		$q3 = "select  * from zpraba_process_next where process_to in ( $lpid )  or  process_from in ($lpid)";
		$data3 = $this->_db->fetchAll($q3);
		#Zend_Debug::dump($data3); die();
		if(count($data3)>0) {
				
			foreach ($data3 as $vw) {
				$this->_db->query("insert into zpraba_process_next (process_to, process_from, line_type) values (?, ?, ?)", array($vz[$vw['process_to']], $vz[$vw['process_from']], $vw['line_type']));
				
			}
		}
		
		$result = array (
					'result' => true,
					'message' => 'clone process succed' 
			);
		
	} catch (Exception $e) {
	   	$result = array (
					'result' => false,
					'message' => $e->getMessage () 
			);		
	}
	
	return $result;
	
}


}
