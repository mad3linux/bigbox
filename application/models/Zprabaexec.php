<?php

/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class Model_Zprabaexec extends Zend_Db_Table_Abstract {
    /*
     * 
     * name: get_adapters
     * @param
     * @return
     * 
     */

    public function get_adapters($con) {

        try {
            $rows = $this->_db->fetchRow("select * from zpraba_conn where id=?", $con);
            //Zend_Debug::dump($rows); die();
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            die();
        }

        return $rows;
    }

    public function get_metadata_conn($con) {
        try {
            $adapters = $this->get_adapters($con);
            $c = new Model_Zprabametadata($con);
            $data = $c->get_tables();
            echo md5(serialize($data));
            die();
            Zend_Debug::dump($data);
            die();
            $vars = unserialize($adapters['conn_params']);
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            die();
        }

        foreach ($data as $k => $v) {

            //Zend_Debug::dump($v);
            $col = array();
            $ser = "";
            foreach ($v as $k2 => $v2) {
                $col[] = $k2;
            }
            $colA = implode(",", $col);
            $so[] = array(
                'adapter_name' => $vars['dbname'],
                'address' => $vars['host'],
                'api_id' => '',
                'content' => '',
                'content_serialize' => serialize($v),
                'hash' => '',
                'id' => '',
                'item' => $colA,
                'machine_id' => $vars['localhost'],
                'object_name' => $k,
                'object_type' => 'table',
                'path' => '',
                'path_alias' => '',
                'schema_id' => '',
                'source_type' => 'database',
                'spell' => '',
                'taxonomy_names' => '',
                'teaser' => '',
                'tid' => '',
                'timestamp' => '',
                'title' => $k,
                'url' => ''
            );
        }
        //Zend_Debug::dump($so);	
        $vz = $this->builder($so);
        //echo $vz; die();
        return $vz;
    }

    public function builder($data) {


        $path = (APPLICATION_PATH . '/metadata');
        //echo $path ; die();
        if ($_GET['	'] == 1) {
            // create the index
            $files = glob($path . '/*');
            foreach ($files as $file) {
                if (is_file($file))
                    unlink($file);
            }
            $files = glob($path . '/{,.}*', GLOB_BRACE);
            foreach ($files as $file) {
                if (is_file($file))
                    unlink($file);
            }
            $index = Zend_Search_Lucene::create($path);
        } else {
            try {
                $index = Zend_Search_Lucene::open($path);
            } catch (Exception $e) {
                Zend_Debug::dump($e);
                die("ss");
            }
        }

        foreach ($data as $row) {
            //Zend_Debug::dump($row); die();
            $doc = new Zend_Search_Lucene_Document();
            $doc->addField(Zend_Search_Lucene_Field::UnIndexed('id', $row['id']));

            $doc->addField(Zend_Search_Lucene_Field::Text('adapter_name', $row['adapter_name']));
            $doc->addField(Zend_Search_Lucene_Field::Text('address', $row['address']));

            $doc->addField(Zend_Search_Lucene_Field::Text('api_id', $row['api_id']));
            $doc->addField(Zend_Search_Lucene_Field::Text('content', $row['content']));

            $doc->addField(Zend_Search_Lucene_Field::UnIndexed('content_serialize', $row['content_serialize']));

            $doc->addField(Zend_Search_Lucene_Field::UnIndexed('content_raw', $row['content_raw']));

            $doc->addField(Zend_Search_Lucene_Field::Text('hash', $row['hash']));

            $doc->addField(Zend_Search_Lucene_Field::Text('id', $row['id']));
            $doc->addField(Zend_Search_Lucene_Field::Text('item', $row['item']));

            $doc->addField(Zend_Search_Lucene_Field::Text('machine_id', $row['machine_id']));
            $doc->addField(Zend_Search_Lucene_Field::Text('object_name', $row['object_name']));

            $doc->addField(Zend_Search_Lucene_Field::Text('object_type', $row['object_type']));
            $doc->addField(Zend_Search_Lucene_Field::Text('path', $row['path']));

            $doc->addField(Zend_Search_Lucene_Field::Text('path_alias', $row['path_alias']));

            $doc->addField(Zend_Search_Lucene_Field::Text('schema_id', $row['schema_id']));
            $doc->addField(Zend_Search_Lucene_Field::Text('source_type', $row['source_type']));


            $index->addDocument($doc);
        }


        $index->optimize();
        //	$this->view->count= $index->numDocs();
        return $index->numDocs();
    }

}
