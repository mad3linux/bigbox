<?php


/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class Model_Zprabametadata extends Zend_Db_Table_Abstract {

    function __construct($conn_id) {
        try {
            $this->id = $conn_id;
            $dd = new Model_Zpraba4api();
            $cc = $dd->conn_by_id($conn_id);
            $this->_db = $cc;
            $this->_db->getConnection();
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die("sss");
        }
    }

    public function get_tables() {
        try {
            //echo $this->id; die();
            $tab =($this->_db->listTables());
            $new = array();
            $i = 1;

            foreach($tab as $k => $t) {
                //if($k<=11) {
                $new[$t] = $this->_db->describeTable($t);
                //}
                $i ++;
            }
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
        return($new);
    }

    public function get_tablesx() {
        try {
            $tab =($this->_db->listTables());
            $new = array();
            $i = 1;

            foreach($tab as $k => $t) {
                //if($k<=11) {
                $new[$t] = $this->_db->describeTable($t);
                //}
                $i ++;
            }
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
        return($new);
    }
}
