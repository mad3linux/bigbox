<?php


/*
 * Copyright (C) Prabtech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class Model_Zprabapage extends Zend_Db_Table_Abstract {

    public function get_conn_id($conn) {
        $sql = "select b.conn_name, b.conn_params, b.desc as optionals, b.is_deactivate from zpraba_conn b where b.id = '$conn'";
        //die($sql);
        try {
            $data = $this->_db->fetchRow($sql);
            //$cache->save ( $data, 'zpraba_api', array ('prabasystem' ) );
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($sql);
        }
        //}
        return $data;
    }

    public function get_conn($conn) {
        $sql = "select b.conn_name, b.conn_params, b.desc as optionals, b.is_deactivate from zpraba_conn b where b.conn_name = '$conn'";
        //die($sql);
        try {
            $data = $this->_db->fetchRow($sql);
            //$cache->save ( $data, 'zpraba_api', array ('prabasystem' ) );
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($sql);
        }
        //}
        return $data;
    }

    public function get_all_api() {
        $cache = Zend_Registry::get('cache');
        $sql = "select * from zpraba_api order by 1";
        if(!$data = $cache->load('zpraba_api')) {
            try {
                $data = $this->_db->fetchAll($sql);
                $cache->save($data, 'zpraba_api', array('prabasystem'));
            }
            catch(Exception $e) {
                Zend_Debug::dump($e->getMessage());
                die($sql);
            }
        }
        return $data;
    }

    public function list_users_api() {
        $cache = Zend_Registry::get('cache');
        $sql = "select * from zpraba_users_api order by user_name";
        if(!$item = $cache->load('list_users_api')) {
            try {
                $item = $this->_db->fetchAll($sql, $id);
                $cache->save($item, 'list_users_api', array('prabasystem'));
            }
            catch(Exception $e) {
                Zend_Debug::dump($e->getMessage());
                die($sql);
            }
        }
        return $item;
    }

    public function list_page() {
        $cache = Zend_Registry::get('cache');
        $sql = "select * from zpraba_page order by 1";
        if(!$vr = $cache->load('list_page')) {
            try {
                $item = $this->_db->fetchAll($sql, $id);
                $vr = array();

                foreach($item as $k => $v) {
                    $vr[$k] = $v;
                    $vr[$k]['breadcrumb'] = unserialize($v['breadcrumb']);
                    $vr[$k]['element'] = unserialize($v['element']);
                }
                // Zend_Debug::dump($vr);die();
                $cache->save($vr, 'list_page', array('prabasystem'));
            }
            catch(Exception $e) {
                Zend_Debug::dump($e->getMessage());
                die($sql);
            }
        }
        return $vr;
    }

    public function list_action_wf() {
        $cache = Zend_Registry::get('cache');
        $sql = "select * from zpraba_action where attr1 !='' order by 1";
        if(!$item = $cache->load('list_action_wf')) {
            try {
                $item = $this->_db->fetchAll($sql);
                $cache->save($item, 'list_action_wf', array('prabasystem'));
            }
            catch(Exception $e) {
                Zend_Debug::dump($e->getMessage());
                die($sql);
            }
        }
        return $vr;
    }

    public function list_action() {
        $cache = Zend_Registry::get('cache');
        $sql = "select * from zpraba_action order by 1";
        if(!$item = $cache->load('list_action')) {
            try {
                $item = $this->_db->fetchAll($sql);
                $cache->save($item, 'list_action', array('prabasystem'));
            }
            catch(Exception $e) {
                Zend_Debug::dump($e->getMessage());
                die($sql);
            }
        }
        return $vr;
    }

    public function get_page($id) {
        //die($id);
        $cache = Zend_Registry::get('cache');
        if(!$item = $cache->load('get_page' . $id)) {
            if(is_numeric($id)) {
                $sql = "select * from zpraba_page a left join zpraba_app b on a.app_id = b.id where a.id = ? ";
            } else {
                $sql = "select * from zpraba_page a left join zpraba_app b on a.app_id = b.id where a.name_alias = ?";
            }
            // die($sql);
            try {
                $item = $this->_db->fetchRow($sql, $id);
                $cache->save($item, 'get_page' . 
                             $id, array('prabasystem'));
            }
            catch(Exception $e) {
                Zend_Debug::dump($e->getMessage());
                die($sql);
            }
        }
        return $item;
    }

    public function get_portlet($id) {
        $cache = Zend_Registry::get('cache');
        $sql = "select * from zpraba_portlet where id = ?";
        //if (! $item = $cache->load ( 'zpraba_portlet' . $id )) {
        try {
            $item = $this->_db->fetchRow($sql, $id);
            $item['portlet_attrs'] = unserialize($item['portlet_attrs']);
            $item['content_attrs'] = unserialize($item['content_attrs']);
            
            /*
             $cache->save ( $item, 'zpraba_portlet' . $id, array (
             'prabasystem'
             ) );
             */
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($sql);
        }
        //}
        return $item;
    }

    public function get_portlets($id) {
        $cache = Zend_Registry::get('cache');
        $sql = "select * from zpraba_portlet where id in ( $id )";
        if(!$vf = $cache->load('get_portlets' . $id)) {
            try {
                $item = $this->_db->fetchAll($sql);
                $vf = array();

                foreach($item as $kk) {
                    $vf[$kk['id']] = $kk;
                    $vf[$kk['id']]['portlet_attrs'] = unserialize($kk['portlet_attrs']);
                    $vf[$kk['id']]['content_attrs'] = unserialize($kk['content_attrs']);
                }
                $cache->save($vf, 'get_portlets' . 
                             $id, array('prabasystem'));
            }
            catch(Exception $e) {
                Zend_Debug::dump($e->getMessage());
                die($sql);
            }
        }
        return $vf;
    }

    public function get_api($id) {
        $cache = Zend_Registry::get('cache');
        $sql = "select a.*, b.conn_name, b.conn_params, b.desc as optionals, b.is_deactivate from zpraba_api a inner join zpraba_conn b on a.conn_id= b.id where a.id = ?";
        $cek = strpos($id, "act");
        if($cek !== false) {
            $dd = explode("-", $id);
            return array('conn_name' => 'call4action',
                         'conn_params' => serialize(array()),
                         'act_id' =>$dd[1]);
        }
        if(!$item = $cache->load('get_api' . $id)) {
            try {
                $item = $this->_db->fetchRow($sql, $id);
                $cache->save($item, 'get_api' . 
                             $id, array('prabasystem'));
            }
            catch(Exception $e) {
                Zend_Debug::dump($e->getMessage());
                die($sql);
            }
        }
        return $item;
    }

    public function listpage_ajax($data) {
        $aColumns = array('no',
                          'id',
                          'name_alias',
                          'layout_id',
                          'workspace_id',
                          'app_id',
                          'title');
        $sLimit = "";
        if(isset($data['iDisplayStart'])&& $data['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . intval($data['iDisplayStart']). ", " . intval($data['iDisplayLength']);
        }
        
        /*
         * Ordering
         */
        // Zend_Debug::dump($data); die();
        if(isset($data['iSortCol_0'])&& intval($data['iSortCol_0'])> 0) {
            $sOrder = "ORDER BY  ";
            for($i = 1;
            $i < intval ($data['iSortingCols'] );
            $i ++ )//for ( $i=1 ; $i<count($aColumns) ; $i++ )
            {
                if($data['bSortable_' . 
                   intval($data['iSortCol_' . $i])] == "true") {
                    $sOrder .= "`" . $aColumns[intval($data['iSortCol_' . 
                                                      $i])] . "` " .($data['sSortDir_' . 
                                                                     $i] === 'asc' ? 'asc' : 'desc'). ", ";
                }
            }
            $sOrder .= "`" . $aColumns[intval($data['iSortCol_0'])] . "` " .($data['sSortDir_0'] === 'asc' ? 'asc' : 'desc'). ", ";
            // echo $sOrder.'<br>';
            $sOrder = substr_replace($sOrder, "", - 2);
            if($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }
        
        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $sWhere = '';
        if(isset($data['sSearch'])&& $data['sSearch'] != "") {
            $sWhere .= "WHERE (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $data['sSearch'] . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }
        // die($sWhere);
        
        /* Individual column filtering */
        for($i = 1;
        $i < count ($aColumns );
        $i ++ ) {
            if(isset($data['bSearchable_' . $i])&& $data['bSearchable_' . 
                     $i] == "true" && $data['sSearch_' . 
                     $i] != '') {
                if($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $data['sSearch_' . $i] . "%' ";
            }
        }
        // echo $sWhere.'<br>';
        // echo $sOrder.'<br>';
        // echo $sLimit.'<br>';
        // die();
        $qry = "SELECT * FROM 
					`zpraba_page` " . $sWhere . " " . $sOrder . " " . $sLimit;
        // echo $qry; die();
        try {
            $data = $this->_db->fetchAll($qry);
            // Zend_Debug::dump($data);die();
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }
	
	public function listpage_ajaxbigbox($data) {
        $aColumns = array('no',
                          'id',
                          'name_alias',
                          'layout_id',
                          'workspace_id',
                          'app_id',
                          'title');
		$sLimit = "";
		
		$getdatatable	= $_GET['datatable'];
		$page			= round($getdatatable['pagination']['page']);
		$perpages		= round($getdatatable['pagination']['perpage']);
		$pages			= ceil($iTotalRecords/$perpages);
		$sorttype		= $getdatatable['sort']['sort'];
		$sortby			= $getdatatable['sort']['field'];
		
		$iDisplayLength = intval($perpages);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart 	= intval(($page-1)*$perpages);
		$sSearch		= $getdatatable['query']['generalSearch'];
		
		
        if(isset($iDisplayStart)&& $iDisplayLength != '-1') {
            $sLimit = "LIMIT " . intval($iDisplayStart). ", " . intval($iDisplayLength);
        }
        
        /*
         * Ordering
         */
        // Zend_Debug::dump($data); die();
        if(isset($sortby)) {
			if ($sortby=='No') {
				$filsort	= 'id';
			} else {
				$filsort	= $sortby;
			}
            $sOrder = "ORDER BY ".$filsort." ".$sorttype;
        } else {
			$sOrder = "";
		}
        
        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $sWhere = '';
        if(isset($sSearch)&& $sSearch != "") {
            $sWhere .= "WHERE (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $sSearch . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }
        // die($sWhere);
        
        /* Individual column filtering */
        // for($i = 1;
        // $i < count ($aColumns );
        // $i ++ ) {
            // if(isset($data['bSearchable_' . $i])&& $data['bSearchable_' . 
                     // $i] == "true" && $data['sSearch_' . 
                     // $i] != '') {
                // if($sWhere == "") {
                    // $sWhere = "WHERE ";
                // } else {
                    // $sWhere .= " AND ";
                // }
                // $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $data['sSearch_' . $i] . "%' ";
            // }
        // }
        // echo $sWhere.'<br>';
        // echo $sOrder.'<br>';
        // echo $sLimit.'<br>';
        // die();
        $qry = "SELECT * FROM 
					`zpraba_page` " . $sWhere . " " . $sOrder . " " . $sLimit;
        // echo $qry; die();
        try {
            $data = $this->_db->fetchAll($qry);
            // Zend_Debug::dump($data);die();
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }

    public function listaction_ajax($data, $wf = null) {
        $aColumns = array('no',
                          'id',
                          'action_name');
        $sLimit = "";
        if(isset($data['iDisplayStart'])&& $data['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . intval($data['iDisplayStart']). ", " . intval($data['iDisplayLength']);
        }
        
        /*
         * Ordering
         */
        // Zend_Debug::dump($data); die();
        if(isset($data['iSortCol_0'])&& intval($data['iSortCol_0'])> 0) {
            $sOrder = "ORDER BY  ";
            for($i = 1;
            $i < intval ($data['iSortingCols'] );
            $i ++ )//for ( $i=1 ; $i<count($aColumns) ; $i++ )
            {
                if($data['bSortable_' . 
                   intval($data['iSortCol_' . $i])] == "true") {
                    $sOrder .= "`" . $aColumns[intval($data['iSortCol_' . 
                                                      $i])] . "` " .($data['sSortDir_' . 
                                                                     $i] === 'asc' ? 'asc' : 'desc'). ", ";
                }
            }
            $sOrder .= "`" . $aColumns[intval($data['iSortCol_0'])] . "` " .($data['sSortDir_0'] === 'asc' ? 'asc' : 'desc'). ", ";
            // echo $sOrder.'<br>';
            $sOrder = substr_replace($sOrder, "", - 2);
            if($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }else {
             $sOrder = "ORDER BY action_name ASC ";
            }
        
        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $sWhere = '';
        if(isset($data['sSearch'])&& $data['sSearch'] != "") {
            $sWhere .= "WHERE (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $data['sSearch'] . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }
        // die($sWhere);
        
        /* Individual column filtering */
        for($i = 1;
        $i < count ($aColumns );
        $i ++ ) {
            if(isset($data['bSearchable_' . $i])&& $data['bSearchable_' . 
                     $i] == "true" && $data['sSearch_' . 
                     $i] != '') {
                if($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $data['sSearch_' . $i] . "%' ";
            }
        }
        // echo $sWhere.'<br>';
        // echo $sOrder.'<br>';
        // echo $sLimit.'<br>';
        // die();
        $ql = "";
        if($wf == 1) {
           ($sWhere == "")? $wh = "where" : $wh = " and ";
            $ql = " $wh attr1 !='' ";
        }
        $qry = "SELECT * FROM 
					`zpraba_action` " . $sWhere . " " . $ql . "  " . $sOrder . " " . $sLimit;
        // echo $qry; die();
        try {
            $data = $this->_db->fetchAll($qry);
            // Zend_Debug::dump($data);die();
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }
	
	public function listaction_ajaxbigaction($data, $wf = null) {
        $aColumns = array('no',
                          'id',
                          'action_name');
        $sLimit = "";
		
		$getdatatable	= $_GET['datatable'];
		$page			= round($getdatatable['pagination']['page']);
		$perpages		= round($getdatatable['pagination']['perpage']);
		$pages			= ceil($iTotalRecords/$perpages);
		$sorttype		= $getdatatable['sort']['sort'];
		$sortby			= $getdatatable['sort']['field'];
		
		$iDisplayLength = intval($perpages);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart 	= intval(($page-1)*$perpages);
		$sSearch		= $getdatatable['query']['generalSearch'];
		
		
        if(isset($iDisplayStart)&& $iDisplayLength != '-1') {
            $sLimit = "LIMIT " . intval($iDisplayStart). ", " . intval($iDisplayLength);
        }
        
        /*
         * Ordering
         */
        // Zend_Debug::dump($data); die();
        if(isset($sortby)) {
			if ($sortby=='No') {
				$filsort	= 'id';
			} else {
				$filsort	= $sortby;
			}
            $sOrder = "ORDER BY ".$filsort." ".$sorttype;
        } else {
			$sOrder = "";
		}
        
        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $sWhere = '';
        if(isset($sSearch)&& $sSearch != "") {
            $sWhere .= "WHERE (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $sSearch . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }
        // die($sWhere);
        
        /* Individual column filtering */
        // for($i = 1;
        // $i < count ($aColumns );
        // $i ++ ) {
            // if(isset($data['bSearchable_' . $i])&& $data['bSearchable_' . 
                     // $i] == "true" && $data['sSearch_' . 
                     // $i] != '') {
                // if($sWhere == "") {
                    // $sWhere = "WHERE ";
                // } else {
                    // $sWhere .= " AND ";
                // }
                // $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $data['sSearch_' . $i] . "%' ";
            // }
        // }
        // echo $sWhere.'<br>';
        // echo $sOrder.'<br>';
        // echo $sLimit.'<br>';
        // die();
        $ql = "";
        if($wf == 1) {
           ($sWhere == "")? $wh = "where" : $wh = " and ";
            $ql = " $wh attr1 !='' ";
        }
        $qry = "SELECT * FROM 
					`zpraba_action` " . $sWhere . " " . $ql . "  " . $sOrder . " " . $sLimit;
        // echo $qry; die();
        try {
            $data = $this->_db->fetchAll($qry);
            // Zend_Debug::dump($data);die();
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }

    public function listportlet_ajax($data) {
        $aColumns = array('no',
                          'id',
                          'portlet_name',
                          'portlet_title',
                          'portlet_type',
                          'custom_view');
        $sLimit = "";
        if(isset($data['iDisplayStart'])&& $data['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . intval($data['iDisplayStart']). ", " . intval($data['iDisplayLength']);
        }
        
        /*
         * Ordering
         */
        // Zend_Debug::dump($data); die();
        if(isset($data['iSortCol_0'])&& intval($data['iSortCol_0'])> 0) {
            $sOrder = "ORDER BY  ";
            for($i = 1;
            $i < intval ($data['iSortingCols'] );
            $i ++ )//for ( $i=1 ; $i<count($aColumns) ; $i++ )
            {
                if($data['bSortable_' . 
                   intval($data['iSortCol_' . $i])] == "true") {
                    $sOrder .= "`" . $aColumns[intval($data['iSortCol_' . 
                                                      $i])] . "` " .($data['sSortDir_' . 
                                                                     $i] === 'asc' ? 'asc' : 'desc'). ", ";
                }
            }
            $sOrder .= "`" . $aColumns[intval($data['iSortCol_0'])] . "` " .($data['sSortDir_0'] === 'asc' ? 'asc' : 'desc'). ", ";
            // echo $sOrder.'<br>';
            $sOrder = substr_replace($sOrder, "", - 2);
            if($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }
        
        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $sWhere = '';
        if(isset($data['sSearch'])&& $data['sSearch'] != "") {
            $sWhere .= "WHERE (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $data['sSearch'] . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }
        // die($sWhere);
        
        /* Individual column filtering */
        for($i = 1;
        $i < count ($aColumns );
        $i ++ ) {
            if(isset($data['bSearchable_' . $i])&& $data['bSearchable_' . 
                     $i] == "true" && $data['sSearch_' . 
                     $i] != '') {
                if($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $data['sSearch_' . $i] . "%' ";
            }
        }
        // echo $sWhere.'<br>';
        // echo $sOrder.'<br>';
        // echo $sLimit.'<br>';
        // die();
        $qry = "SELECT * FROM 
					`zpraba_portlet` " . $sWhere . " " . $sOrder . " " . $sLimit;
        // echo $qry; die();
        try {
            $data = $this->_db->fetchAll($qry);
            // Zend_Debug::dump($data);die();
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }
	
	public function listportlet_ajaxbigbuilder($data) {
        $aColumns = array('no',
                          'id',
                          'portlet_name',
                          'portlet_title',
                          'portlet_type',
                          'custom_view');
        $sLimit = "";
		
		$getdatatable	= $_GET['datatable'];
		$page			= round($getdatatable['pagination']['page']);
		$perpages		= round($getdatatable['pagination']['perpage']);
		$pages			= ceil($iTotalRecords/$perpages);
		$sorttype		= $getdatatable['sort']['sort'];
		$sortby			= $getdatatable['sort']['field'];
		
		$iDisplayLength = intval($perpages);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart 	= intval(($page-1)*$perpages);
		$sSearch		= $getdatatable['query']['generalSearch'];
		
		
        if(isset($iDisplayStart)&& $iDisplayLength != '-1') {
            $sLimit = "LIMIT " . intval($iDisplayStart). ", " . intval($iDisplayLength);
        }
        
        /*
         * Ordering
         */
        // Zend_Debug::dump($data); die();
        if(isset($sortby)) {
            $sOrder = "ORDER BY ".$sortby." ".$sorttype;
        } else {
			$sOrder = "";
		}
        
        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $sWhere = '';
        if(isset($sSearch)&& $sSearch != "") {
            $sWhere .= "WHERE (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $sSearch . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }
        // die($sWhere);
		
        $qry = "SELECT * FROM 
					`zpraba_portlet` " . $sWhere . " " . $sOrder . " " . $sLimit;
        // echo $qry; die();
        try {
            $data = $this->_db->fetchAll($qry);
            // Zend_Debug::dump($data);die();
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }

    function count_listaction($data) {
        $aColumns = array('no',
                          'id',
                          'action_name',
                         );
        $sLimit = "";
        if(isset($data['iDisplayStart'])&& $data['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . intval($data['iDisplayStart']). ", " . intval($data['iDisplayLength']);
        }
        
        /*
         * Ordering
         */
        $sOrder = "";
        if(isset($data['iSortCol_0'])&& intval($data['iSortCol_0'])> 0) {
            $sOrder = "ORDER BY  ";
            for($i = 1;
            $i < intval ($data['iSortingCols'] );
            $i ++ )//for ( $i=1 ; $i<count($aColumns) ; $i++ )
            {
                if($data['bSortable_' . 
                   intval($data['iSortCol_' . $i])] == "true") {
                    $sOrder .= "`" . $aColumns[intval($data['iSortCol_' . 
                                                      $i])] . "` " .($data['sSortDir_' . 
                                                                     $i] === 'asc' ? 'asc' : 'desc'). ", ";
                }
            }
            $sOrder .= "`" . $aColumns[intval($data['iSortCol_0'])] . "` " .($data['sSortDir_0'] === 'asc' ? 'asc' : 'desc'). ", ";
            // echo $sOrder.'<br>';
            $sOrder = substr_replace($sOrder, "", - 2);
            if($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }
        
        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $sWhere = '';
        if(isset($data['sSearch'])&& $data['sSearch'] != "") {
            $sWhere .= "WHERE (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $data['sSearch'] . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }
        // die($sWhere);
        
        /* Individual column filtering */
        for($i = 1;
        $i < count ($aColumns );
        $i ++ ) {
            if(isset($data['bSearchable_' . $i])&& $data['bSearchable_' . 
                     $i] == "true" && $data['sSearch_' . 
                     $i] != '') {
                if($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $data['sSearch_' . $i] . "%' ";
            }
        }
        $ql = "";
        if($wf == 1) {
           ($sWhere == "")? $wh = "where " : $wh = " and ";
            $ql = " $wh attr1 !='' ";
        }
        $qry = "SELECT 
					count(id) as row 
				FROM 
					`zpraba_action` " . $sWhere . " " . $ql . " " . $sOrder;
        // die($qry);
        try {
            $data = $this->_db->fetchOne($qry);
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }
	
	function count_listactionbigaction($data) {
        $aColumns = array('no',
                          'id',
                          'action_name',
                         );
        $sLimit = "";
		
		$getdatatable	= $_GET['datatable'];
		$page			= round($getdatatable['pagination']['page']);
		$perpages		= round($getdatatable['pagination']['perpage']);
		$pages			= ceil($iTotalRecords/$perpages);
		$sorttype		= $getdatatable['sort']['sort'];
		$sortby			= $getdatatable['sort']['field'];
		
		$iDisplayLength = intval($perpages);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart 	= intval(($page-1)*$perpages);
		$sSearch		= $getdatatable['query']['generalSearch'];
		
		
        if(isset($iDisplayStart)&& $iDisplayLength != '-1') {
            $sLimit = "LIMIT " . intval($iDisplayStart). ", " . intval($iDisplayLength);
        }
        
        /*
         * Ordering
         */
        // Zend_Debug::dump($data); die();
        if(isset($sortby)) {
			if ($sortby=='No') {
				$filsort	= 'id';
			} else {
				$filsort	= $sortby;
			}
            $sOrder = "ORDER BY ".$filsort." ".$sorttype;
        } else {
			$sOrder = "";
		}
        
        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $sWhere = '';
        if(isset($sSearch)&& $sSearch != "") {
            $sWhere .= "WHERE (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $sSearch . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }
        // die($sWhere);
        
        /* Individual column filtering */
        // for($i = 1;
        // $i < count ($aColumns );
        // $i ++ ) {
            // if(isset($data['bSearchable_' . $i])&& $data['bSearchable_' . 
                     // $i] == "true" && $data['sSearch_' . 
                     // $i] != '') {
                // if($sWhere == "") {
                    // $sWhere = "WHERE ";
                // } else {
                    // $sWhere .= " AND ";
                // }
                // $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $data['sSearch_' . $i] . "%' ";
            // }
        // }
        $ql = "";
        if($wf == 1) {
           ($sWhere == "")? $wh = "where " : $wh = " and ";
            $ql = " $wh attr1 !='' ";
        }
        $qry = "SELECT 
					count(id) as row 
				FROM 
					`zpraba_action` " . $sWhere . " " . $ql . " " . $sOrder;
        // die($qry);
        try {
            $data = $this->_db->fetchOne($qry);
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }

    function count_listpage($data) {
        $aColumns = array('no',
                          'id',
                          'name_alias',
                          'layout_id',
                          'workspace_id',
                          'app_id',
                          'title');
        $sLimit = "";
        if(isset($data['iDisplayStart'])&& $data['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . intval($data['iDisplayStart']). ", " . intval($data['iDisplayLength']);
        }
        
        /*
         * Ordering
         */
        $sOrder = "";
        if(isset($data['iSortCol_0'])&& intval($data['iSortCol_0'])> 0) {
            $sOrder = "ORDER BY  ";
            for($i = 1;
            $i < intval ($data['iSortingCols'] );
            $i ++ )//for ( $i=1 ; $i<count($aColumns) ; $i++ )
            {
                if($data['bSortable_' . 
                   intval($data['iSortCol_' . $i])] == "true") {
                    $sOrder .= "`" . $aColumns[intval($data['iSortCol_' . 
                                                      $i])] . "` " .($data['sSortDir_' . 
                                                                     $i] === 'asc' ? 'asc' : 'desc'). ", ";
                }
            }
            $sOrder .= "`" . $aColumns[intval($data['iSortCol_0'])] . "` " .($data['sSortDir_0'] === 'asc' ? 'asc' : 'desc'). ", ";
            // echo $sOrder.'<br>';
            $sOrder = substr_replace($sOrder, "", - 2);
            if($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }
        
        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $sWhere = '';
        if(isset($data['sSearch'])&& $data['sSearch'] != "") {
            $sWhere .= "WHERE (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $data['sSearch'] . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }
        // die($sWhere);
        
        /* Individual column filtering */
        for($i = 1;
        $i < count ($aColumns );
        $i ++ ) {
            if(isset($data['bSearchable_' . $i])&& $data['bSearchable_' . 
                     $i] == "true" && $data['sSearch_' . 
                     $i] != '') {
                if($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $data['sSearch_' . $i] . "%' ";
            }
        }
        $qry = "SELECT 
					count(id) as row 
				FROM 
					`zpraba_page` " . $sWhere . " " . $sOrder;
        // die($qry);
        try {
            $data = $this->_db->fetchOne($qry);
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }
	
	function count_listpagebigbox($data) {
        $aColumns = array('no',
						  'id',
                          'name_alias',
                          'layout_id',
                          'workspace_id',
                          'app_id',
                          'title');
		$sLimit = "";
		
        $getdatatable	= $_GET['datatable'];
		$page			= round($getdatatable['pagination']['page']);
		$perpages		= round($getdatatable['pagination']['perpage']);
		$pages			= ceil($iTotalRecords/$perpages);
		$sorttype		= $getdatatable['sort']['sort'];
		$sortby			= $getdatatable['sort']['field'];
		
		$iDisplayLength = intval($perpages);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart 	= intval(($page-1)*$perpages);
		$sSearch		= $getdatatable['query']['generalSearch'];
		
        if(isset($iDisplayStart)&& $iDisplayLength != '-1') {
            $sLimit = "LIMIT " . intval($iDisplayStart). ", " . intval($iDisplayLength);
        }
        
        /*
         * Ordering
         */
        $sOrder = "";
        if(isset($sortby)) {
			if ($sortby=='No') {
				$filsort	= 'id';
			} else {
				$filsort	= $sortby;
			}
            $sOrder = "ORDER BY ".$filsort." ".$sorttype;
        } else {
			$sOrder = "";
		}
        
        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $sWhere = '';
        if(isset($sSearch)&& $sSearch != "") {
            $sWhere .= "WHERE (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $sSearch . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }
        // die($sWhere);
        
        /* Individual column filtering */
        // for($i = 1;
        // $i < count ($aColumns );
        // $i ++ ) {
            // if(isset($data['bSearchable_' . $i])&& $data['bSearchable_' . 
                     // $i] == "true" && $data['sSearch_' . 
                     // $i] != '') {
                // if($sWhere == "") {
                    // $sWhere = "WHERE ";
                // } else {
                    // $sWhere .= " AND ";
                // }
                // $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $data['sSearch_' . $i] . "%' ";
            // }
        // }
        $qry = "SELECT 
					count(id) as row 
				FROM 
					`zpraba_page` " . $sWhere . " " . $sOrder;
        // die($qry);
        try {
            $data = $this->_db->fetchOne($qry);
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }

    function count_listportlet($data) {
        $aColumns = array('no',
                          'id',
                          'portlet_name',
                          'portlet_title',
                          'portlet_type');
        $sLimit = "";
        if(isset($data['iDisplayStart'])&& $data['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . intval($data['iDisplayStart']). ", " . intval($data['iDisplayLength']);
        }
        
        /*
         * Ordering
         */
        $sOrder = "";
        if(isset($data['iSortCol_0'])&& intval($data['iSortCol_0'])> 0) {
            $sOrder = "ORDER BY  ";
            for($i = 1;
            $i < intval ($data['iSortingCols'] );
            $i ++ )//for ( $i=1 ; $i<count($aColumns) ; $i++ )
            {
                if($data['bSortable_' . 
                   intval($data['iSortCol_' . $i])] == "true") {
                    $sOrder .= "`" . $aColumns[intval($data['iSortCol_' . 
                                                      $i])] . "` " .($data['sSortDir_' . 
                                                                     $i] === 'asc' ? 'asc' : 'desc'). ", ";
                }
            }
            $sOrder .= "`" . $aColumns[intval($data['iSortCol_0'])] . "` " .($data['sSortDir_0'] === 'asc' ? 'asc' : 'desc'). ", ";
            // echo $sOrder.'<br>';
            $sOrder = substr_replace($sOrder, "", - 2);
            if($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }
        
        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $sWhere = '';
        if(isset($data['sSearch'])&& $data['sSearch'] != "") {
            $sWhere .= "WHERE (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $data['sSearch'] . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }
        // die($sWhere);
        
        /* Individual column filtering */
        for($i = 1;
        $i < count ($aColumns );
        $i ++ ) {
            if(isset($data['bSearchable_' . $i])&& $data['bSearchable_' . 
                     $i] == "true" && $data['sSearch_' . 
                     $i] != '') {
                if($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $data['sSearch_' . $i] . "%' ";
            }
        }
        $qry = "SELECT 
					count(id) as row 
				FROM 
					`zpraba_portlet` " . $sWhere . " " . $sOrder;
        // die($qry);
        try {
            $data = $this->_db->fetchOne($qry);
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }
	
	function count_listportletbigbox($data) {
        $aColumns = array('no',
                          'id',
                          'portlet_name',
                          'portlet_title',
                          'portlet_type');
        $sLimit = "";
		
		$getdatatable	= $_GET['datatable'];
		$page			= round($getdatatable['pagination']['page']);
		$perpages		= round($getdatatable['pagination']['perpage']);
		$pages			= ceil($iTotalRecords/$perpages);
		$sorttype		= $getdatatable['sort']['sort'];
		$sortby			= $getdatatable['sort']['field'];
		
		$iDisplayLength = intval($perpages);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart 	= intval(($page-1)*$perpages);
		$sSearch		= $getdatatable['query']['generalSearch'];
		
        if(isset($iDisplayStart)&& $iDisplayLength != '-1') {
            $sLimit = "LIMIT " . intval($iDisplayStart). ", " . intval($iDisplayLength);
        }
        
        /*
         * Ordering
         */
        $sOrder = "";
        if(isset($sortby)) {
            $sOrder = "ORDER BY ".$sortby." ".$sorttype;
        } else {
			$sOrder = "";
		}
        
        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $sWhere = '';
        if(isset($sSearch)&& $sSearch != "") {
            $sWhere .= "WHERE (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $sSearch . "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }
        // die($sWhere);
        
        /* Individual column filtering */
        // for($i = 1;
        // $i < count ($aColumns );
        // $i ++ ) {
            // if(isset($data['bSearchable_' . $i])&& $data['bSearchable_' . 
                     // $i] == "true" && $data['sSearch_' . 
                     // $i] != '') {
                // if($sWhere == "") {
                    // $sWhere = "WHERE ";
                // } else {
                    // $sWhere .= " AND ";
                // }
                // $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $data['sSearch_' . $i] . "%' ";
            // }
        // }
        $qry = "SELECT 
					count(id) as row 
				FROM 
					`zpraba_portlet` " . $sWhere . " " . $sOrder;
        // die($qry);
        try {
            $data = $this->_db->fetchOne($qry);
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }

    public function get_a_user_api($id) {
        $cache = Zend_Registry::get('cache');
        $sql = "select * from zpraba_users_api  where id=? ";
        if(!$item = $cache->load('get_a_users_api' . $id)) {
            try {
                $item = $this->_db->fetchRow($sql, $id);
                $cache->save($item, 'get_a_users_api' . 
                             $id, array('prabasystem'));
            }
            catch(Exception $e) {
                Zend_Debug::dump($e->getMessage());
                die($sql);
            }
        }
        return $item;
    }
}
