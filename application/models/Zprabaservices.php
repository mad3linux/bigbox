<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Zprabaservices extends Zend_Db_Table_Abstract {

	public function wsdl($conn, $data, $xin = array(), $sql_raw, $ti) {
		
		$fd = new CMS_General();
		
		$method= explode("(", $data['sqltext']);
		$method0  =$method[0]; 
		//var_dump($ti);
		$new= array();
		foreach ($ti as $k=>$v) {
			$new[str_replace(":", "", $k)]=$v;
		}
		//Zend_Debug::dump($new);die();
		
		$Arr=array();
		try{
			$client = new SoapClient($conn['dbname']);    
			$ArrObj = $client->$method0($new);
			$Arr = $fd->objectToArray($ArrObj);
			$message = "success";
			$transaction = true;
			
				
		} catch (Exception $e) { 
				$message = $e->getMessage();
				$transaction = false;
		}
		return array(
			'Arr'=>$Arr, 
			'message'=>$message,
			'transaction'=>$transaction);
		
			
	}
	public function urljson($conn, $data, $xin = array(), $sql_raw, $ti) {
		
		$fd = new CMS_General();
		$query = parse_url($data['sqltext'], PHP_URL_QUERY);
		parse_str($query, $params);
		$new=array();
		foreach ($params as $k=>$v) {
			if(isset( $ti[$v])) {
				$new[$k]=$ti[$v];
					
			} else {
				$new[$k]=$v;
				
			}
			
		}
	
		$get =  http_build_query($new) . "\n";
	
		
		$Arr=array();
		try{
			
			$url =$conn['dbname']."?".$get;
		//	echo $url; die();
				$Arr =  json_decode(file_get_contents(str_replace(" ", "", trim($url))));
	//	Zend_Debug::dump($Arr);die();
			$message = "success";
			$transaction = true;
			
				
		} catch (Exception $e) { 
				$message = $e->getMessage();
				$transaction = false;
		}
		return array(
			'Arr'=>$Arr, 
			'message'=>$message,
			'transaction'=>$transaction);
		
			
	}
	
	
	
	
}
