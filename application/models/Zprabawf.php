<?php

/*
 * Copyright (C) Prabtech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class Model_Zprabawf extends Zend_Db_Table_Abstract {

    function get_a_wf_process($id, $v) {
        try {
            if($v['is_freeze'] != 1) {
                $sql = "select * from zpraba_process where act_id= ?  ";
                $tmp = $this->_db->fetchAll($sql, $id);
                return $tmp;
            }
            $z = new Model_Cache();
            $cache = $z->cachefunc(6000);
            $cid = $z->get_id("get_a_wf_process", $id);
            $tmp = $z->get_cache($cache, $cid);
            if(!$tmp) {
                $sql = "select * from zpraba_process where act_id= ?  ";
                $tmp = $this->_db->fetchAll($sql, $id);
                $cache->save($tmp, $cid, array('systemaction'));
            }
        }
        catch(Exception $e) {
        }
        return $tmp;
    }

    public function formbuilder($data, $sname = null) {
        for($i = 0;
        $i < count ($data['id'] );
        $i ++ ) {
            $field[$data['id'][$i]]['id'] = $data['id'][$i];
            $field[$data['id'][$i]]['type'] = $data['type'][$i];
            $field[$data['id'][$i]]['name'] = $data['id'][$i];
            $field[$data['id'][$i]]['class'] = $data['class'][$i];
            $field[$data['id'][$i]]['label'] = $data['label'][$i];
            $field[$data['id'][$i]]['classicon'] = $data['classicon'][$i];
            $field[$data['id'][$i]]['initvalue'] = $data['initvalue'][$i];
            $field[$data['id'][$i]]['initApi'] = $data['initapi'][$i];
            $field[$data['id'][$i]]['required'] = $data['req'][$i];
            $field[$data['id'][$i]]['area'] = $data['area'][$i];
            $field[$data['id'][$i]]['help'] = $data['help'][$i];
        }
        try {
            //insert into content_type
            if(isset($data['pid'])&& $data['pid'] != "" && $sname != "") {
                return $this->updateformbuilder($data, $sname);
            }
            $datainitend = serialize(array("Data" => array('cumweight' => "100")));
            $this->_db->query("insert into zpraba_action (action_name, act_type, attr1) values (?, ?, ?)", array($data['fname'], 3, $data['sname']));
            $id = $this->_db->lastInsertId();
            $start = $this->_db->query("insert into zpraba_process (act_id, class_type, offset_top, offset_left) values ($id, 'Start', 38, 49)");
            $end = $this->_db->query("insert into zpraba_process (act_id, class_type, offset_top, offset_left, zparams_serialize) values ($id, 'End', 324, 800, '$datainitend')");
            $this->_db->query("insert into zpraba_content_type (cid, type, c_name, `description`) values (?, ?, ?, ?)", array($id, $data['sname'], $data['fname'], $data['descr']));
            //insert into config zpraba_field_config	
            $sql = "";
            $sql2 = "";

            foreach($data['id'] as $k => $v) {
                $this->_db->query("insert into zpraba_field_config (cid, bundle, fname, label, data, ftype) values (?, ?, ?, ?, ?, ?)", array($id, $data['sname'], $field[$v]['id'], $field[$v]['label'], serialize($field[$v]), $field[$v]['type']));
                $fl = "";
                if($field[$v]['type'] == 'file') {
                    $fl = "`fid` int(10) unsigned DEFAULT NULL, KEY `fid` (`fid`),";
                }
                $sql = "CREATE TABLE `zprabaf_data_" . $v . "` (
		  `cid` int(10) unsigned NOT NULL,
		  `entity_type` varchar(128) NOT NULL DEFAULT '',
		  `bundle` varchar(128) NOT NULL DEFAULT '',
		  `deleted` tinyint(4) NOT NULL DEFAULT '0',
		  `entity_id` int(10) unsigned NOT NULL,
		  `revision_id` int(10) unsigned DEFAULT NULL,
		   `delta` int(10) unsigned NOT NULL,
		  `" . $v . "_value` longtext,
		  `" . $v . "_format` varchar(255) DEFAULT NULL,
		  " . $fl . "
		  KEY `cid` (`cid`),
		  KEY `entity_type` (`entity_type`),
		  KEY `bundle` (`bundle`),
		  KEY `deleted` (`deleted`),
		  KEY `entity_id` (`entity_id`),
		  KEY `revision_id` (`revision_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
                $sql2 = "CREATE TABLE `zprabaf_revision_" . $v . "`  (
		  `cid` int(10) unsigned NOT NULL,
		  `entity_type` varchar(128) NOT NULL DEFAULT '',
		  `bundle` varchar(128) NOT NULL DEFAULT '',
		  `deleted` tinyint(4) NOT NULL DEFAULT '0',
		  `entity_id` int(10) unsigned NOT NULL,
		  `revision_id` int(10) unsigned NOT NULL,
		  `delta` int(10) unsigned NOT NULL,
		   `" . $v . "_value` longtext, 
		  `" . $v . "_format` varchar(255) DEFAULT NULL,
		  " . $fl . "
		   KEY `cid` (`cid`),
		  KEY `entity_type` (`entity_type`),
		  KEY `bundle` (`bundle`),
		  KEY `deleted` (`deleted`),
		  KEY `entity_id` (`entity_id`),
		  KEY `revision_id` (`revision_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
                $this->_db->query($sql);
                $this->_db->query($sql2);
            }
            //create tabel field
            $result = array('result' => true,
                            'message' => 'Created.');
        }
        catch(Exception $e) {
            $result = array('result' => false,
                            'message' =>$e->getMessage());
        }
        return $result;
    }

    public function update_custom_field($data, $sname) {
        for($i = 0;
        $i < count ($data['id'] );
        $i ++ ) {
            if($data['id'][$i] != "") {
                $field[$i]['bundle'] = $sname;
                $field[$i]['fname'] = $data['id'][$i];
                $field[$i]['label'] = $data['label'][$i];
                $field[$i]['ftype'] = $data['type'][$i];
                $field[$i]['cid'] = $data['pid'];
                $field[$i]['detail']['id'] = $data['id'][$i];
                $field[$i]['detail']['type'] = $data['type'][$i];
                $field[$i]['detail']['name'] = $data['id'][$i];
                $field[$i]['detail']['class'] = $data['class'][$i];
                $field[$i]['detail']['label'] = $data['label'][$i];
                $field[$i]['detail']['classicon'] = $data['classicon'][$i];
                $field[$i]['detail']['initvalue'] = $data['initvalue'][$i];
                $field[$i]['detail']['initApi'] = $data['initapi'][$i];
                $field[$i]['detail']['required'] = $data['req'][$i];
                $field[$i]['detail']['area'] = $data['area'][$i];
                $field[$i]['detail']['help'] = $data['help'][$i];
            }
        }
        //	Zend_Debug::dump($field); die();
        try {
            $this->_db->query("update zpraba_action set action_name=?, attr1=?   where attr1=?", array($data['fname'], $data['sname'], $sname));
            $this->_db->query("update zpraba_content_type set type=?, c_name=?, `description`=?, table_name=?, table_field_zip=?  where type=?", array($data['sname'], $data['fname'], $data['descr'], $data['tname'], serialize($field), $sname));
            $result = array('result' => true,
                            'message' => 'Created.');
        }
        catch(Exception $e) {
            $result = array('result' => false,
                            'message' =>$e->getMessage());
        }
        return $result;
    }

    public function updateformbuilder($data, $sname) {
        if(trim($data['tname'])!= "") {
            return $this->update_custom_field($data, $sname);
        }
        for($i = 0;
        $i < count ($data['id'] );
        $i ++ ) {
            if($data['id'][$i] != "") {
                $field[$data['id'][$i]]['id'] = $data['id'][$i];
                $field[$data['id'][$i]]['type'] = $data['type'][$i];
                $field[$data['id'][$i]]['name'] = $data['id'][$i];
                $field[$data['id'][$i]]['class'] = $data['class'][$i];
                $field[$data['id'][$i]]['label'] = $data['label'][$i];
                $field[$data['id'][$i]]['classicon'] = $data['classicon'][$i];
                $field[$data['id'][$i]]['initvalue'] = $data['initvalue'][$i];
                $field[$data['id'][$i]]['initApi'] = $data['initapi'][$i];
                $field[$data['id'][$i]]['required'] = $data['req'][$i];
                $field[$data['id'][$i]]['area'] = $data['area'][$i];
                $field[$data['id'][$i]]['help'] = $data['help'][$i];
            }
        }
        try {
            #die($sname); 
            $this->_db->query("update zpraba_action set action_name=?, attr1=?   where attr1=?", array($data['fname'], $data['sname'], $sname));
            //die("ok");
            $this->_db->query("delete from zpraba_field_config where bundle=?", $sname);
            $this->_db->query("update zpraba_content_type set type=?, c_name=?, `description`=?  where type=?", array($data['sname'], $data['fname'], $data['descr'], $sname));
            //insert into config zpraba_field_config	

            foreach($data['id'] as $k => $v) {
                if($field[$v]['id'] != "") {
                    $fl = "";
                    if($field[$v]['type'] == 'file') {
                        $fl = "`fid` int(10) unsigned DEFAULT NULL, KEY `fid` (`fid`),";
                    }
                    $this->_db->query("insert into zpraba_field_config (cid, bundle, fname, label, data, ftype) values (?, ?, ?, ?, ?, ?)", array($data['pid'], $data['sname'], $field[$v]['id'], $field[$v]['label'], serialize($field[$v]), $field[$v]['type']));
                    $cek_exist = count($this->_db->fetchAll("SHOW TABLES LIKE 'zprabaf_data_" . $field[$v]['id'] . "' "));
                    //echo "SHOW TABLES LIKE 'zprabaf_data_".$field[$v]['id']."' "; die();
                    //echo $cek_exist; die();
                    if($cek_exist == "0") {
                        //echo "asd"; die();
                        $sql = "CREATE TABLE `zprabaf_data_" . $v . "` 
			( `cid` int(10) unsigned NOT NULL,
			`entity_type` varchar(128) NOT NULL DEFAULT '',
			`bundle` varchar(128) NOT NULL , 
			`deleted` tinyint(4) NOT NULL DEFAULT '0', 
			`entity_id` int(10) unsigned NOT NULL, 
			`revision_id` int(10) unsigned DEFAULT NULL, 
			`delta` int(10) unsigned NOT NULL, 
			`" . $v . "_value` longtext, 
			`" . $v . "_format` varchar(255) DEFAULT NULL, 
			" . $fl . " KEY `entity_type` (`entity_type`), 
			 KEY `cid` (`cid`),
			KEY `bundle` (`bundle`),
			KEY `deleted` (`deleted`), 
			KEY `entity_id` (`entity_id`), 
			KEY `revision_id` (`revision_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
                        $sql2 = "CREATE TABLE `zprabaf_revision_" . $v . "` 
			(`cid` int(10) unsigned NOT NULL,
			`entity_type` varchar(128) NOT NULL DEFAULT '',
			`bundle` varchar(128) NOT NULL DEFAULT '', 
			`deleted` tinyint(4) NOT NULL DEFAULT '0', 
			`entity_id` int(10) unsigned NOT NULL, 
			`revision_id` int(10) unsigned NOT NULL, 
			`delta` int(10) unsigned NOT NULL, 
			`" . $v . "_value` longtext,  
			`" . $v . "_format` varchar(255) DEFAULT NULL, 
			" . $fl . " KEY `entity_type` (`entity_type`), 
			KEY `cid` (`cid`),
			KEY `bundle` (`bundle`), 
			KEY `deleted` (`deleted`), 
			KEY `entity_id` (`entity_id`), 
			KEY `revision_id` (`revision_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
                        //echo ($sql); die();
                        $this->_db->query($sql);
                        $this->_db->query($sql2);
                    } else {
                        $this->_db->query("DROP TABLE IF EXISTS `zprabaf_data_" . 
                                          $v . 
                                          "`");
                        $sql = "CREATE TABLE `zprabaf_data_" . $v . "` 
			(`cid` int(10) unsigned NOT NULL,
			`entity_type` varchar(128) NOT NULL, 
			`bundle` varchar(128) NOT NULL DEFAULT '', 
			`deleted` tinyint(4) NOT NULL DEFAULT '0',
			`entity_id` int(10) unsigned NOT NULL,
			`revision_id` int(10) unsigned DEFAULT NULL,
			`delta` int(10) unsigned NOT NULL, 
			`" . $v . "_value` longtext, 
			`" . $v . "_format` varchar(255) DEFAULT NULL, 
			" . $fl . " KEY `entity_type` (`entity_type`), 
			KEY `cid` (`cid`),
			KEY `bundle` (`bundle`),
			KEY `deleted` (`deleted`), 
			KEY `entity_id` (`entity_id`),
			KEY `revision_id` (`revision_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
                        $this->_db->query("DROP TABLE IF EXISTS `zprabaf_revision_" . 
                                          $v . 
                                          "`");
                        $sql2 = "CREATE TABLE `zprabaf_revision_" . $v . "`  
			(`cid` int(10) unsigned NOT NULL,
			`entity_type` varchar(128) NOT NULL DEFAULT '',
			`bundle` varchar(128) NOT NULL DEFAULT '', 
			`deleted` tinyint(4) NOT NULL DEFAULT '0',
			`entity_id` int(10) unsigned NOT NULL, 
			`revision_id` int(10) unsigned NOT NULL, 
			`delta` int(10) unsigned NOT NULL, 
			`" . $v . "_value` longtext,  
			`" . $v . "_format` varchar(255) DEFAULT NULL, 
			" . $fl . " KEY `entity_type` (`entity_type`), 
			KEY `cid` (`cid`),
			KEY `bundle` (`bundle`), 
			KEY `deleted` (`deleted`), 
			KEY `entity_id` (`entity_id`), 
			KEY `revision_id` (`revision_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
                        //die($sql);
                        $this->_db->query($sql);
                        $this->_db->query($sql2);
                    }
                }
            }
            $result = array('result' => true,
                            'message' => 'Created.');
        }
        catch(Exception $e) {
            $result = array('result' => false,
                            'message' =>$e->getMessage());
        }
        return $result;
    }

    public function get_content_type_field($actid) {
        //echo $actid; die();
        $data = $this->_db->fetchRow("select * from zpraba_content_type where cid=?", $actid);
        if($data['table_name'] != "") {
            $item = unserialize($data['table_field_zip']);
        } else {
            $sql = "select  a.*  from  zpraba_field_config a inner join zpraba_action  b on a.bundle= b.attr1 where b.id = ? ";
            try {
                $item = $this->_db->fetchAll($sql, $actid);
            }
            catch(Exception $e) {
            }
        }
        //Zend_Debug::dump($item); die();
        return $item;
    }

    public function get_content_type() {
        $sql = "select  *  from  zpraba_content_type ";
        try {
            $item = $this->_db->fetchAll($sql);
        }
        catch(Exception $e) {
        }
        return $item;
    }

    public function get_action_content_type($id) {
        if(is_numeric($id)) {
            $sql = "select  *  from   zpraba_action  a inner join 	zpraba_content_type b on b.type = a.attr1  where a.id = ? ";
        } else {
            $sql = "select  *  from   zpraba_action  a inner join 	zpraba_content_type b on b.type = a.attr1  where a.attr1 = ? ";
        }
        try {
            //echo $sql;
            $item = $this->_db->fetchRow($sql, $id);
            //  Zend_Debug::dump($item); die();
            //if(1==2) {
            if(trim($item['table_name'])!= "" && $item['table_field_zip']) {
                $item['field'] = unserialize($item['table_field_zip']);
            } else {
                $data = $this->get_content_type_field($item['id']);

                foreach($data as $k => $v) {
                    $new[$k] = $v;
                    $new[$k]['detail'] = unserialize($v['data']);
                }
                $item['field'] = $new;
            }
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($item); die();
        return $item;
    }

    public function update_branch($data, $id) {
        $sql = "update zpraba_running_action set branch_act= ? where idtime=? ";
        try {
            $this->_db->query($sql, array($data, $id));
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($sql);
        }
    }

    public function update_current_running($data, $id) {
        $sql = "update zpraba_running_action set curr_pid= ? where idtime=? ";
        try {
            $this->_db->query($sql, array($data, $id));
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($sql);
        }
    }

    public function get_running($id) {
        $sql = "select * from zpraba_running_action  where idtime=? order by id desc";
        try {
            $item = $this->_db->fetchAll($sql, $id);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($sql);
        }
        return $item;
    }

    public function insert_running($data) {
        $sql = "insert into zpraba_running_action (idtime, act_id, pid, v_input,v_output, curr_pid, branch_act) values (?, ?, ?, ?, ?, ?,?)";
        try {
            $item = $this->_db->query($sql, $data);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($sql);
        }
        return $data['idtime'];
    }

    public function get_wf_process_debug($id, $cek=true) {
		$aty = $this->_db->fetchRow("select * from zpraba_action where id=? ", $id);
        if($cek) {
            if($aty['act_type'] == "2") {
                $params = array('input' =>$input,
                                'zzzid' =>$id);
                $jobId = Resque::enqueue('default', 'Model_Executequeueaction', $params, true);
                return "Jobid  : " . $jobId;
            }
        }
		
		
		   try {      
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();   
            $by = $identity->fullname;
            if($identity->fullname==''){$by = ' system ';}
                        
            $ins=array('log_type'=>'info',
                        'subject'=>'action_log',
                        'refference'=>'',
                        'reff_id'=>$id,
                        'description'=>'Running Action '.$aty['action_name'].' ('.$id.')',
                        'x1'=>'',
                        'x2'=>'by:'.$by,
                        'x3'=>'',
                        'x4'=>'',
                        'xint1'=>'',
                        'xint2'=>'');
            $f  = new Model_Actlogs();
            $f->insert_log($ins);
            
             }catch(Exception $e) {
                //Zend_Debug::dump($e->getMessage()); die(); 
                }
        $sql = "select * from zpraba_process where act_id= ?  ";
        try {
            $item = $this->_db->fetchAll($sql, $id);

            foreach($item as $k => $v) {
                if($v['class_type'] == 'Start') {
                    $data['start'] = $v['pid'];
                }
                if($v['class_type'] == 'End') {
                    $data['end'] = $v['pid'];
                }
                $data['data'][$v['pid']] = $v;
            }
        }
        catch(Exception $e) {
        }
        $step = $this->get_wf_process_next($id);
        return array('data' =>$data,
                     'step' =>$step);
    }

    public function get_wf_process($id, $input = array(), $cek = true, $debug = false) {
        $ns = new Zend_Session_Namespace('prabawf');
        $aty = $this->_db->fetchRow("select * from zpraba_action where id=? ", $id);
        if($cek) {
            if($aty['act_type'] == "2") {
                $params = array('input' =>$input,
                                'zzzid' =>$id);
                $jobId = Resque::enqueue('default', 'Model_Executequeueaction', $params, true);
                return "Jobid  : " . $jobId;
            }
        }
        $item = $this->get_a_wf_process($id, $aty);
		       try {      
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();   
            $by = $identity->fullname;
            if($identity->fullname==''){$by = ' system ';}
                        
            $ins=array('log_type'=>'info',
                        'subject'=>'action_log',
                        'refference'=>'',
                        'reff_id'=>$id,
                        'description'=>'Running Action '.$aty['action_name'].' ('.$id.')',
                        'x1'=>'',
                        'x2'=>'by:'.$by,
                        'x3'=>'',
                        'x4'=>'',
                        'xint1'=>'',
                        'xint2'=>'');
            $f  = new Model_Actlogs();
            $f->insert_log($ins);
            
             }catch(Exception $e) {
                //Zend_Debug::dump($e->getMessage()); die(); 
                }
		
		
		
		
		
		
        foreach($item as $k => $v) {
            if($v['class_type'] == 'Start') {
				
				
                $data['start'] = $v['pid'];
            }
            if($v['class_type'] == 'End') {
                $data['end'] = $v['pid'];
            }
            $data['data'][$v['pid']] = $v;
        }
        $step = $this->get_wf_process_next($id);
        $ns->prabawf['step'] = 1;
        return $this->exec_process($data['start'], $input, $data, $step, $result, $debug);
    }

    public function exec_type_operator($start, $input, $data, $step, $result, $k) {
        //	Zend_Debug::dump($start);
        $c = new Model_Zprabawfoperator();
        $var = unserialize($data['data'][$start]['zparams_serialize']);
        $data = $c->$var['OperatorType']($start, $input, $data, $step, $result, $k);
        return $data;
    }

    public function exec_process_debug($start, $input, $data, $step, $result, $key = 0) {
        $get_process_to = null;
        $v = $step[$start][$key];
        $get_process_to = $v['process_to'];
        if($start == $data['start']) {
            //   $input[$get_process_to] =$input;    
        }
        
        switch($data['data'][$get_process_to]['class_type']) {
            //call api
            case 'Task' : //Zend_Debug::dump($input); die();
            $vz1 = $this->exec_type_task($get_process_to, $input, $data, $step, $result);
            //Zend_Debug::dump($vz1); die();
            $vres[$get_process_to] = $vz1;
            //$vinput[$get_process_to]  = $vz1['data'];
            break;
            case 'Prahu' : $vz1 = $this->exec_type_prahu($get_process_to, $input, $data, $step, $result);
            $vres[$get_process_to] = $vz1;
            //	return $this->exec_process($get_process_to, $vinput, $data, $step, $vres); 
            break;
            case 'Operator' : //Zend_Debug::dump($input); die();
            $vz1 = $this->exec_type_operator($get_process_to, $input, $data, $step, $result);
            $vres[$get_process_to] = $vz1;
            //$vinput[$get_process_to] = $vz1;
            break;
            case 'Start' : $vres[$get_process_to] = array();
            //$vinput[$get_process_to]  = array();
            break;
            case 'End' : $vres[$get_process_to] =($input);
           
           
            $vres[$get_process_to] =($result);
            break;
        }
        return $vres;
        //$this->exec_process($get_process_to, $vinput, $data, $step, $vres);
    }

    public function exec_process($start, $input, $data, $step, $result, $debug = null) {
        $get_process_to = null;
        $ns = new Zend_Session_Namespace('prabawf');
        $nz = Zend_Session::namespaceGet('prabawf');
        $count = (int) $nz['prabawf']['step'] + 1;
        $ns->prabawf['step'] = $count;
        $pos = strpos($start, "jump");
        if($pos !== false) {
            $jump = explode("-", $start);
            $key = $jump[2];
            unset($ns->prabawf['branchstart'][$jump[3]]);
            unset($step[$jump[0]][$key]);
            $start = $jump[0];
            if(count($step[$start])== 1) {
                $ns->prabawf['branchstart'][$nz['prabawf']['step']]['step'] = $nz['prabawf']['step'];
                $ns->prabawf['branchstart'][$nz['prabawf']['step']]['task'] = $start;
                $ns->prabawf['branchstart'][$nz['prabawf']['step']]['sumbranch'] = count($step[$start]);
                $ns->prabawf['branchstart'][$nz['prabawf']['step']]['next'] = $step[$start];
                $ns->prabawf['branchstart'][$nz['prabawf']['step']]['input'] = $input;
                $ns->prabawf['branchstart'][$nz['prabawf']['step']]['last'] = 1;
            }
            //$ns->prabawf['branchstart']['key']=2;
        }
        if(count($step[$start])> 1) {
            $ns->prabawf['branchstart'][$nz['prabawf']['step']]['last'] = 0;
            $ns->prabawf['branchstart'][$nz['prabawf']['step']]['step'] = $nz['prabawf']['step'];
            $ns->prabawf['branchstart'][$nz['prabawf']['step']]['task'] = $start;
            $ns->prabawf['branchstart'][$nz['prabawf']['step']]['sumbranch'] = count($step[$start]);
            $ns->prabawf['branchstart'][$nz['prabawf']['step']]['next'] = $step[$start];
            $ns->prabawf['branchstart'][$nz['prabawf']['step']]['input'] = $input;
        }
        $nz = Zend_Session::namespaceGet('prabawf');

        foreach($step[$start] as $k => $v) {
            $get_process_to = $v['process_to'];
            if(!in_array($get_process_to, $input["branchwrong"])) {
                switch($data['data'][$get_process_to]['class_type']) {
                    case 'Task' : $vz1 = $this->exec_type_task($get_process_to, $input, $data, $step, $result, $k);
                    $vres = $vz1;
                    $vinput = $vz1;
                    break;
                    case 'Prahu' : $vz1 = $this->exec_type_prahu($get_process_to, $input, $data, $step, $result, $k);
                    $vres = $vz1;
                    $vinput = $vz1;
                    break;
                    case 'Operator' : $vz1 = $this->exec_type_operator($get_process_to, $input, $data, $step, $result, $k);
                    $vres = $vz1;
                    $vinput = $vz1;
                    break;
                    case 'Start' : $vres = array();
                    $vinput = array();
             
                    break;
                    case 'End' : 
             
            
            return $result;
                    break;
                }
                if(isset($vres['varjump'])&& $vres['varjump'] == 1) {
                    unset($vres['varjump']);
                    return $vres;
                }
                if(isset($vres['jumpto'])&& $vres['jumpto'] != "") {
                    $get_process_to = $vres['jumpto'];
                }
                if($debug) {
                    echo "<span style='padding:2px;font-size:10px;background-color:#ccc'><b>STEP : $get_process_to=>INPUT</b><br>";
                    Zend_Debug::dump($input);
                    echo "</span>";
                    echo "<span style='padding:2px;font-size:10px;background-color:#ADD8E6'><b>OUTPUT</b><br>";
                    Zend_Debug::dump($vres);
                    echo "</span>";
                }
                return $this->exec_process($get_process_to, $vinput, $data, $step, $vres, $debug);
            }
        }
    }

    public function exec_type_prahu($start, $input, $data, $step, $result, $k) {
        if(count($input)== 0) {
            return array("transaction" => false,
                         'error' => 'not sending',
                         'is_sent' =>$input['is_sent'],
                         'msg' =>$input['msg']);
        }
        #Zend_Debug::dump($input); 
        $cc = new CMS_General();
        $vz =(unserialize($data['data'][$start]['zparams_serialize']));
        $m1 = new Model_Zprahu();
        $url = $m1->get_sending_url($vz['Data']['type'], $vz['Data']['account']);
        preg_match_all("/\[(.*?)\]/", $vz['Data']['body'], $match);
        if(isset($input['transaction'])) {
            if(!$input['transaction']) {
                return array("transaction" => false,
                             'error' => 'not sending',
                             'is_sent' =>$input['is_sent'],
                             'msg' =>$input['msg']);
            }
        }
        if(count($input)>= 1 && count($match[1])>= 1) {

            foreach($match[1] as $k => $v) {
                if(isset($input[$v])&& $input[$v] != "") {
                    $vz['Data']['body'] = str_replace($match[0][$k], $input[$v], $vz['Data']['body']);
                }
            }
        }
        if($url != "" && count($vz['Data']['group'])>= 1 && $vz['Data']['body'] != "") {
            $urlz = "";
            $urlw = "";

            foreach($vz['Data']['group'] as $dd) {
                $urlz = str_replace(':target', $dd, $url);
                $urlw = str_replace(':msg', urlencode($vz['Data']['body']), $urlz);
                //echo $url;die(); 
                $Arr = json_decode(file_get_contents(str_replace(" ", "", trim($urlw))));
            }
        }
        return array('msg' =>$vz['Data']['body'],
                     'is_sent' => 1,
                     "transaction" => true);
    }

    public function exec_type_task($start, $input, $data, $step, $result, $k) {
        if($data['data'][$start]['p_type'] == 'action') {
            $data['data'][$start]['ext_id'] = 'act-' . $data['data'][$start]['ext_id'];
        }
        $m1 = new Model_Zpraba4api();
        $dd = new Model_Zprabapage();
        $varC = $dd->get_api($data['data'][$start]['ext_id']);
        $conn = unserialize($varC['conn_params']);
        $res = $m1->execute_api($conn, $varC, $input);
        return $res;
    }

    public function create_new_task($task_type, $template_id, $offset_left, $offset_top) {
        try {
            $sql = "insert into zpraba_process (act_id, class_type, offset_top, offset_left) 
			values ($template_id, '$task_type', $offset_top,  $offset_left)";
            //die($sql);
            $this->_db->query($sql);
            return $this->_db->lastInsertId();
        }
        catch(Exception $e) {
            return false;
        }
    }

    public function get_wf_process_next($id) {
        $sql = "select distinct a.*  from zpraba_process_next a  inner  join zpraba_process b  on (a.process_to = b.pid or a.process_from = b.pid) where act_id= ?  ";
        try {
            $item = $this->_db->fetchAll($sql, $id);

            foreach($item as $k => $v) {
                $data[$v['process_from']][] = $v;
            }
        }
        catch(Exception $e) {
        }
        //Zend_Debug::dump($data); die();   
        return $data;
    }

    public function get_conn_name() {
        $sql = "select distinct(conn_name) as conn from zpraba_api";
        try {
            $item = $this->_db->fetchAll($sql);
        }
        catch(Exception $e) {
        }
        return $item;
    }

    public function get_api_by_id_for_conn($id) {
        $sql = "select * from zpraba_api  where conn_id=(select conn_id from zpraba_api where id=  " . (int) $id . ")";
        //  die($sql);
        try {
            $item = $this->_db->fetchAll($sql);
        }
        catch(Exception $e) {
        }
        return $item;
    }

    public function get_api_by_id($id) {
        $sql = "select *  from zpraba_api where id =  " . (int) $id;
        //  die($sql);
        try {
            $item = $this->_db->fetchRow($sql);
        }
        catch(Exception $e) {
        }
        return $item;
    }

    public function get_action_by_id_with_conn($id) {
        if(is_numeric($id)) {
            $var = serialize(array('adapter' => 'action'));
            $sql = "select id, action_name  as sql_text , concat_ws('-', 'ACTION', action_name)  as conn_name, '" . $var . "' as conn_params  from zpraba_action where   id =  " . (int) $id;
        }
        try {
            $item = $this->_db->fetchRow($sql);
        }
        catch(Exception $e) {
        }
        return $item;
    }

    public function get_api_by_id_with_conn($id) {
        if(is_numeric($id)) {
            $sql = "select a.*, b.conn_params from zpraba_api a inner join zpraba_conn  b on a.conn_id =  b.id  where   a.id =  " . (int) $id;
        }
        try {
            $item = $this->_db->fetchRow($sql);
        }
        catch(Exception $e) {
        }
        return $item;
    }

    public function get_conn_id() {
        $sql = "select distinct(conn_name) as conn, id from zpraba_api order by 1 asc";
        try {
            $item = $this->_db->fetchAll($sql);
        }
        catch(Exception $e) {
        }
        return $item;
    }

    public function get_api_by_conn($name) {
        $sql = "select * from zpraba_api where conn_name = '$name'";
        try {
            $item = $this->_db->fetchAll($sql);
        }
        catch(Exception $e) {
        }
        return $item;
    }

    public function get_api_by_conn_id($name) {
        if(is_numeric($name)) {
            $sql = "select * from zpraba_api where conn_id = $name";
        } elseif($name == 'ACTION') {
            $sql = "select concat_ws('-', 'act', id ) as id, action_name as sql_text  from zpraba_action where act_type in (1, 2)";
        }
        //die($sql);
        try {
            $item = $this->_db->fetchAll($sql);
        }
        catch(Exception $e) {
        }
        return $item;
    }

    public function create_new_conn($from, $to, $false, $line_type = 'default') {
        try {
            $sql = "insert into zpraba_process_next (process_from, process_to, process_to_false, line_type) value ($from, $to, $false, '$line_type')";
            $this->_db->query($sql);
            return true;
        }
        catch(Exception $e) {
            return false;
        }
    }

    public function del1($id1, $id2, $id3) {
        $this->_db->query("delete from zpraba_process_next where  process_to=$id1 or process_from=$id2 or process_to_false = $id3 ");
        return true;
    }

    public function delete_process($id) {
        //die($id);
        $this->_db->query("delete from zpraba_process where pid=$id");
        $this->_db->query("delete from zpraba_process_next where (process_to=$id  or process_from=$id) ");
        return true;
    }

    public function cek_new_con($taskid, $postid) {
        $sql = "select * from  zpraba_process_next where ((process_from = $taskid )  and( (process_to = $postid) or (process_to_false = $postid) ))   or( (process_from = $postid ) and ( (process_to = $taskid) or (process_to_false = $taskid)))";
        // die($sql);
        $items = $this->_db->fetchAll($sql);
        //Zend_Debug::dump($items);die();
        if(count($items)> 0) {
            return $items;
        } else {
            return null;
        }
    }

    public function move($id, $left, $top) {
        try {
            $sql = "update  zpraba_process set offset_top =$top, offset_left = $left where pid= " . (int) $id;
            $this->_db->query($sql);
            return true;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($sql);
        }
    }

    public function get_process($id) {
        try {
            $sql = "select * from zpraba_process where act_id = " . (int) $id;
            $data = $this->_db->fetchAll($sql);
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($sql);
        }
    }

    public function update_process($data) {
        try {
            #Zend_Debug::dump($data);die();
            switch($data['task_class']) {
                case 'PrabaTask' : if($data['conn'] == 'ACTION') {
                    $zz = explode('-', $data['api']);
                    $sql = "update  zpraba_process set ext_id = ?, p_type ='action' where pid= " . (int) $data['template_data_id'];
                    if((int) $zz[1] > 0 && (int) $data['template_data_id'] > 0) {
                        $this->_db->query($sql, $zz[1]);
                    }
                } else {
                    $sql = "update  zpraba_process set ext_id =" . $data['api'] . " where pid= " . (int) $data['template_data_id'];
                    if((int) $data['api'] > 0 && (int) $data['template_data_id'] > 0) {
                        $this->_db->query($sql);
                    }
                }
                break;
                case 'PrabaContent' : $vchan = implode(",", $data['channel']);
                $varserialize = array('Name' =>$data['pname'],
                                      'Data' =>$data);
                $sql = "update  zpraba_process set process_name ='" . $data['pname'] . "', zparams_serialize ='" . serialize($varserialize). "' where pid= " . (int) $data['template_data_id'];
                if((int) $data['template_data_id'] > 0) {
                    $this->_db->query($sql);
                    $this->_db->query("delete from  zpraba_wf_notification where pid=? ", $data['template_data_id']);
                    //	die("s");
                    if(count($data['assign_role'])>= 1) {
                        //Zend_Debug::dump()	
                        $this->_db->query("delete  from  zpraba_wf_assignment where pid=? ", $data['template_data_id']);

                        foreach($data['assign_role'] as $z) {
                            $rtype = 1;
                            if($z == 'assignee' || $z == 'initiator') {
                                $rtype = 2;
                                $var = array('assignee' => 1,
                                             'initiator' => 2);
                                $this->_db->query("insert into zpraba_wf_assignment (pid, assign_type, assign_by, assign_target) values (?, ?, ?, ?)", array((int) $data['template_data_id'], $rtype, 0, $var[$z]));
                            } else {
                                $this->_db->query("insert into zpraba_wf_assignment (pid, assign_type, assign_by, assign_target) values (?, ?, ?, ?)", array((int) $data['template_data_id'], $rtype, 0, $z));
                            }
                        }
                    }
                    if(count($data['notify_assign_role'])>= 1) {

                        foreach($data['notify_assign_role'] as $z) {
                            $rtype = 1;
                            if($z == 'assignee' || $z == 'initiator') {
                                $rtype = 2;
                                $var = array('assignee' => 1,
                                             'initiator' => 2);
                                $this->_db->query("insert into zpraba_wf_notification (pid, notify_type, notify_by, notify_when, notify_target, channel) values (?, ?, ?, ?, ?, ?)", array((int) $data['template_data_id'], $rtype, 0, 1, $var[$z], $vchan));
                            } else {
                                $this->_db->query("insert into zpraba_wf_notification (pid, notify_type, notify_by, notify_when, notify_target, channel) values (?, ?, ?, ?, ?, ?)", array((int) $data['template_data_id'], $rtype, 0, 1, $z, $vchan));
                            }
                        }
                    }
                    if(count($data['notify_complete_role'])>= 1) {

                        foreach($data['notify_complete_role'] as $z) {
                            $rtype = 1;
                            if($z == 'assignee' || $z == 'initiator') {
                                $rtype = 2;
                                $var = array('assignee' => 1,
                                             'initiator' => 2);
                                $this->_db->query("insert into zpraba_wf_notification (pid, notify_type, notify_by, notify_when, notify_target, channel) values (?, ?, ?, ?, ?, ?)", array((int) $data['template_data_id'], $rtype, 0, 2, $var[$z], $vchan));
                            } else {
                                $this->_db->query("insert into zpraba_wf_notification (pid, notify_type, notify_by, notify_when, notify_target, channel) values (?, ?, ?, ?, ?, ?)", array((int) $data['template_data_id'], $rtype, 0, 2, $z, $vchan));
                            }
                        }
                    }
                    if(count($data['notify_reminder_role'])>= 1) {

                        foreach($data['notify_reminder_role'] as $z) {
                            $rtype = 1;
                            if($z == 'assignee' || $z == 'initiator') {
                                $rtype = 2;
                                $var = array('assignee' => 1,
                                             'initiator' => 2);
                                $this->_db->query("insert into zpraba_wf_notification (pid, notify_type, notify_by, notify_when, notify_target, channel) values (?, ?, ?, ?, ?, ?)", array((int) $data['template_data_id'], $rtype, 0, 3, $var[$z], $vchan));
                            } else {
                                $this->_db->query("insert into zpraba_wf_notification (pid, notify_type, notify_by, notify_when, notify_target, channel) values (?, ?, ?, ?, ?, ?)", array((int) $data['template_data_id'], $rtype, 0, 3, $z, $vchan));
                            }
                        }
                    }
                    if(count($data['notify_escalation_role'])>= 1) {

                        foreach($data['notify_escalation_role'] as $z) {
                            $rtype = 1;
                            if($z == 'assignee' || $z == 'initiator') {
                                $rtype = 2;
                                $var = array('assignee' => 1,
                                             'initiator' => 2);
                                $this->_db->query("insert into zpraba_wf_notification (pid, notify_type, notify_by, notify_when, notify_target, channel) values (?, ?, ?, ?, ?, ?)", array((int) $data['template_data_id'], $rtype, 0, 4, $var[$z], $vchan));
                            } else {
                                $this->_db->query("insert into zpraba_wf_notification (pid, notify_type, notify_by, notify_when, notify_target, channel) values (?, ?, ?, ?, ?, ?)", array((int) $data['template_data_id'], $rtype, 0, 4, $z, $vchan));
                            }
                        }
                    }
                }
                break;
                case 'PrabaOperator' : $varserialize = array('OperatorType' =>$data['opr_type'],
                                                             'OperatorData' =>$data,
                                                            );
                $sql = "update  zpraba_process set zparams_serialize ='" . serialize($varserialize). "' where pid= " . (int) $data['template_data_id'];
                if((int) $data['template_data_id'] > 0) {
                    $this->_db->query($sql);
                }
                break;
                case 'PrabaPrahu' : case 'PrabaTrigger' : $varserialize = array('Type' =>$data['type'],
                                                                                'Data' =>$data,
                                                                               );
                $sql = "update  zpraba_process set zparams_serialize ='" . serialize($varserialize). "' where pid= " . (int) $data['template_data_id'];
                //die($sql);
                if((int) $data['template_data_id'] > 0) {
                    $this->_db->query($sql);
                }
                break;
            }
            return true;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($sql);
        }
    }

    public function get_process_by_pid($id) {
        try {
            $sql = "select * from zpraba_process where pid = " . (int) $id;
            $data = $this->_db->fetchRow($sql);
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($sql);
        }
    }

    public function fetchdatafrom($id) {
        try {
            $sql = "select * from zpraba_process_next where  process_from = " . (int) $id;
            $data = $this->_db->fetchAll($sql);
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($sql);
        }
    }
}
