<?php

/*
 * Copyright (C) Prabtech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class Model_Zprabawfoperator extends Zend_Db_Table_Abstract {

    public function custom($start, $input, $data, $step, $result, $k) {


        $vin = unserialize($data['data'][$start]['zparams_serialize']);
        if ($vin['OperatorData']['inputcustom'] != "") {
            $varin = explode(',', $vin['OperatorData']['inputcustom']);
            $input['inputcustom'] = $varin;
        }

        $newObject = $vin['OperatorData']['model'];
        $obj = new $newObject ();
        $handler = array(
            $obj,
            $vin['OperatorData']['method']
            );
        if (is_callable($handler)) {
            try {
                $data = call_user_func_array($handler, array($input));
                //Zend_Debug::dump($data); die();
            } catch (Exception $e) {
                //Zend_Debug::dump($e); die();	
            }
        }
        return $data;
    }

    public function startlooping($start, $input, $data, $step, $result, $k) {
        $debug = Zend_Controller_Front::getInstance()->getRequest()->getParam('debug');

        $ns = new Zend_Session_Namespace('loop');
        $vin = unserialize($data['data'][$start]['zparams_serialize']);
        $kk = $vin['OperatorData']['varstart'];
        if ($kk != "") {
            $pos = strpos($kk, '[');
            if ($pos !== false) {
                $kk = str_replace(']', "", $kk);
                $g = explode("[", $kk);
                $z = count($g);

                switch ($z) {
                    case 2:
                    if (isset($input[$g[0]][$g[1]])) {
                        $newinput = $input[$g[0]][$g[1]];
                    }
                    break;

                    case 3:
                    if (isset($input[$g[0]][$g[1]][$g[2]])) {
                        $newinput = $input[$g[0]][$g[1]][$g[2]];
                    }
                    break;

                    case 4:
                    if (isset($input[$g[0]][$g[1]][$g[2]][$g[3]])) {
                        $newinput = $input[$g[0]][$g[1]][$g[2]][$g[3]];
                    }
                    break;

                    case 5:
                    if (isset($input[$g[0]][$g[1]][$g[2]][$g[3]][$g[4]])) {
                        $newinput = $input[$g[0]][$g[1]][$g[2]][$g[3]][$g[4]];
                    }


                    break;
                }
            } else {
                if (isset($input[$kk])) {
                    $newinput = $input[$kk];
                }
            }
        } else {
            $newinput = $input;
        }


        $dd = new Model_Zprabawf();
        $i = 1;
        foreach ($newinput as $k => $v) {
            #Zend_Debug::dump($v); 
            $v['keyloop'] = $k;
            $v['countloop'] = count($newinput);
            $v['currentloop'] = $i;
            $ns->loop['keyloop'] = $k;
            $ns->loop['currentloop'] = $i;
            $ns->loop['countloop'] = count($newinput);
            #echo $step[$start][0]['process_to']; die();
            #Zend_Debug::dump($v);
            $out[$i] = $dd->exec_process($start, $v, $data, $step, $result, $debug);
            //$Vdata= array_merge($Vdata, $v);
            $ns->loop['finalresult'] = $out;
            $i++;
        }

        $out['jumpto'] = $out[1]['endloop'];
        return ($out);
    }

    public function endlooping($start, $input, $data, $step, $result, $k) {

        $input['varjump'] = 1;
        $input['endloop'] = $start;
        return ($input);
    }

    public function mergeAll($start, $input, $data, $step, $result, $k) {

        $dd = new Model_Zprabawf();
        $debug = Zend_Controller_Front::getInstance()->getRequest()->getParam('debug');
        $ns = Zend_Session::namespaceGet('prabawf');
        $na = new Zend_Session_Namespace('prabawf');
        #Zend_Debug::dump($ns['prabawf']);
        $get = end($ns['prabawf']['branchstart']);

        #Zend_Debug::dump($ns['prabawf']);
        $na->prabawf['result'][$get['task']][$get['step']] = $result;
        reset($get['next']);
        $first_key = key($get['next']);
        $start = $get['task'] . "-jump-" . $first_key . '-' . $get['step'];
        $dd->exec_process($start, $get['input'], $data, $step, $result, $debug);
        $na->prabawf['pairbranch'];
        if (isset($get['last']) && $get['last'] == 1) {
            return $na->prabawf['result'][$get['task']];
        }
        exit();
    }

    public function map($start, $input, $data, $step, $result, $k) {

        $vin = unserialize($data['data'][$start]['zparams_serialize']);
        $map = array();

        $i = 0;
        foreach ($vin['OperatorData']['awal'] as $vz) {
            $map[$vz] = $vin['OperatorData']['akhir'][$i];
            $i++;
        }

        $newinput = array();
        foreach ($map as $kk => $vv) {
            $pos = strpos($kk, '[');
            if ($pos !== false) {
                $kk = str_replace(']', "", $kk);
                $g = explode("[", $kk);
                $z = count($g);

                switch ($z) {
                    case 2:
                    if (isset($input[$g[0]][$g[1]])) {
                        $newinput[$vv] = $input[$g[0]][$g[1]];
                    }
                    break;

                    case 3:
                    if (isset($input[$g[0]][$g[1]][$g[2]])) {
                        $newinput[$vv] = $input[$g[0]][$g[1]][$g[2]];
                    }
                    break;

                    case 4:
                    if (isset($input[$g[0]][$g[1]][$g[2]][$g[3]])) {
                        $newinput[$vv] = $input[$g[0]][$g[1]][$g[2]][$g[3]];
                    }
                    break;

                    case 5:
                    if (isset($input[$g[0]][$g[1]][$g[2]][$g[3]][$g[4]])) {
                        $newinput[$vv] = $input[$g[0]][$g[1]][$g[2]][$g[3]][$g[4]];
                    }


                    break;
                }
            } else {
                if (isset($input[$kk])) {
                    $newinput[$vv] = $input[$kk];
                }
            }
        }
        return ($newinput);
    }

    public function branch($start, $input, $data, $step, $result, $k) {

        //die("123123");

        $vin = unserialize($data['data'][$start]['zparams_serialize']);
        //Zend_Debug::dump ($vin); die();

        $zar = array();
        foreach ($step[$start] as $zz) {
            $zar[$zz['line_type']] = $zz['process_to'];
        }


        //  Zend_Debug::dump($zar); die("23123");
        //cek kondisi per line
        if ($vin['OperatorData']['ltype']['default'] != "") {
            //echo "1";
            $riil = $this->get_string($vin['OperatorData']['ltype']['default'], $input);
            //Zend_Debug::dump($riil);//die("123");
            $wr = $this->operatorplus($vin['OperatorData']['ltype']['default'], $riil);
            //Zend_Debug::dump($wr);die();
            if (!$wr) {
                $branchwrong[] = $zar['default'];
            }
        }


        if ($vin['OperatorData']['ltype']['red'] != "") {
            //echo "2";
            $riil = $this->get_string($vin['OperatorData']['ltype']['red'], $input);
            //Zend_Debug::dump($riil);
            $wr = $this->operatorplus($vin['OperatorData']['ltype']['red'], $riil);
            //Zend_Debug::dump($wr);
            if (!$wr) {
                $branchwrong[] = $zar['red'];
            }
        }

        if ($vin['OperatorData']['ltype']['green'] != "") {

            $riil = $this->get_string($vin['OperatorData']['ltype']['green'], $input);
            $wr = $this->operatorplus($vin['OperatorData']['ltype']['green'], $riil);
            if (!$wr) {
                $branchwrong[] = $zar['green'];
            }
        }

        if ($vin['OperatorData']['ltype']['orange'] != "") {

            $riil = $this->get_string($vin['OperatorData']['ltype']['orange'], $input);
            $wr = $this->operatorplus($vin['OperatorData']['ltype']['orange'], $riil);
            if (!$wr) {
                $branchwrong[] = $zar['orange'];
            }
        }
        //	Zend_Debug::dump(array_merge(array("branchwrong"=>$branchwrong), $input)); die();
        return array_merge(array("branchwrong" => $branchwrong), $input);
    }

    public function setreturn($start, $input, $data, $step, $result, $k) {
        #	Zend_Debug::dump($input); //die();
        $vin = unserialize($data['data'][$start]['zparams_serialize']);
        //Zend_Debug::dump($vin['OperatorData']['ret']); 
        $cc = new CMS_General();
        $str = $cc->get_string_between($vin['OperatorData']['ret'], "(", ")");
        $str = str_replace('"', "", $str);
        //$z2[1] = str_replace('"', "", $z2[1]);
        $z1 = explode(",", $str);
        $new = array();

        foreach ($z1 as $k2 => $v2) {
            $z2 = explode("=>", $v2);
            $pos = strpos(trim($z2[0]), '$');
            if ($pos !== false) {
                $ss2 = $cc->get_string_between(trim($z2[0]), "$", "$");
                if (isset($input[$ss2])) {
                    $z2[0] = $input[$ss2];
                }
            }
            $pos = strpos(trim($z2[1]), '$');
            if ($pos !== false) {
                $ss2 = $cc->get_string_between(trim($z2[1]), "$", "$");
                if (isset($input[$ss2])) {
                    $z2[1] = $input[$ss2];
                }
            }
            $new[trim($z2[0])] = trim($z2[1]);
        }

        return $new;
    }

    function get_string($string, $input) {
        $cc = new CMS_General();
        $str = $cc->get_string_between($string, "$", "$");
        //Zend_Debug::dump($str); die("asd");
        $pos = strpos($str, '[');
        if ($pos !== false) {
            $kk = str_replace(']', "", $kk);
            $g = explode("[", $kk);
            $z = count($g);

            switch ($z) {
                case 2:
                if (isset($input[$g[0]][$g[1]])) {
                    $newinput[$vv] = $input[$g[0]][$g[1]];
                }
                break;

                case 3:
                if (isset($input[$g[0]][$g[1]][$g[2]])) {
                    $newinput[$vv] = $input[$g[0]][$g[1]][$g[2]];
                }
                break;

                case 4:
                if (isset($input[$g[0]][$g[1]][$g[2]][$g[3]])) {
                    $newinput[$vv] = $input[$g[0]][$g[1]][$g[2]][$g[3]];
                }
                break;

                case 5:
                if (isset($input[$g[0]][$g[1]][$g[2]][$g[3]][$g[4]])) {
                    $newinput[$vv] = $input[$g[0]][$g[1]][$g[2]][$g[3]][$g[4]];
                }


                break;
            }
        } else {
            //Zend_Debug::dump($str); die("ssssssss");
            if (isset($input[$str])) {
                return $input[$str];
            }
        }
    }

    public function operatorplus($string, $var) {

        $string = str_replace('"', "", $string);
        $string = str_replace("'", "", $string);

        $cek = strpos($string, "==");
        if ($cek !== false) {
            $v = explode("==", $string);

            if ($v[1] == "true") {
                $v[1] = true;
            } elseif ($v[1] == "false") {
                $v[1] = false;
            }
            //if($v[1]=="false"){$v[1]=false;}

            $ret = false;
            if ($var == $v[1]) {

                $ret = true;
            }


            return $ret;
        }

        $cek = strpos($string, "!=");
        if ($cek !== false) {
            $v = explode("!=", $string);

            $ret = false;
            if ($var != $v[1]) {
                $ret = true;
            }


            return $ret;
        }

        $cek = strpos($string, "<");
        if ($cek !== false) {
            $v = explode("<", $string);

            $ret = false;
            if ($var < $v[1]) {
                $ret = true;
            }


            return $ret;
        }

        $cek = strpos($string, "<=");
        if ($cek !== false) {
            $v = explode("<=", $string);

            $ret = false;
            if ($var <= $v[1]) {
                $ret = true;
            }


            return $ret;
        }


        $cek = strpos($string, ">");
        if ($cek !== false) {
            $v = explode(">", $string);

            $ret = false;
            if ($var > $v[1]) {
                $ret = true;
            }


            return $ret;
        }
        $cek = strpos($string, ">=");
        if ($cek !== false) {
            $v = explode(">=", $string);

            $ret = false;
            if ($var >= $v[1]) {
                $ret = true;
            }


            return $ret;
        }
    }

}
