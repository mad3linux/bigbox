<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Zproject extends Zend_Db_Table_Abstract {

 

    public function get_template() 
    {
       
       $sql ="SELECT * FROM Z_WFTEMPLATE";
       
       try {
		$data = $this->_db->fetchAll($sql);
       } catch (Exception $e) {
		  Zend_Debug::dump($e->getMessage()); die($sql); 
		}
       return $data;
       
    }

	public function get_customers($data) 
    {
       
       $sql ="SELECT * FROM SW_CUSTOMER_DATA";
       
       try {
		$data = $this->_db->fetchAll($sql);
       } catch (Exception $e) {
		  Zend_Debug::dump($e->getMessage()); die($sql); 
		}
       return $data;
       
    }
    public function get_users_ubis($id) 
    {
       
       $sql ="SELECT A.* FROM Z_USERS A INNER JOIN Z_USERS_UBIS B ON A.ID= B.USER_ID WHERE B.UBIS_ID=$id";
      //die($sql);
       try {
		$data = $this->_db->fetchAll($sql);
       } catch (Exception $e) {
		  Zend_Debug::dump($e->getMessage()); die($sql); 
		}
       return $data;
       
    }
    
    
    public function create_project($data) 
    {
	 	
	 $sql = "INSERT INTO Z_WFNODE (ID, PROJECT_DESCR, TEMPLATE_ID) VALUES (Z_WFNODE_ID_SEQ.NEXTVAL, '".$data['descr']."', '".$data['temp']."' )";
	
	 //Zend_Debug::dump($data); die();
	
	 try {
		 $this->_db->query($sql);
		 
		 $newID = $this->_db->lastSequenceId('Z_WFNODE_ID_SEQ');
		
			
			$var =  explode(',', $data['customer']);
			
			//Zend_Debug::dump($var);die();
			
			foreach($var as $vv){
				if(trim($vv)!=""){
				$vf = explode('|', $vv);
				$sqlx ="INSERT INTO SW_WFNODE_CUSTOMER (ZWF_NODE_ID, CUSTOMER_ID) VALUES ($newID, ".$vf[0].")";
				 //echo $sqlx; die();
				$this->_db->query($sqlx);
				}
			}
		
				$data = array('success'=>true, 'ID'=>$newID, 'message'=>'Order berhasil dibuat');  
		 } catch (Exception $e) {
				$data = array('success'=>false, 'message'=> $e->getMessage());  
			 
		}
		return $data;
	}
   
   
   public function get_task_inbox() 
   {
	$sql ="SELECT DISTINCT(C.ID),
		  D.TASK_NAME,
		  C.*,
		  E.TEMPLATE_NAME,
		  A.COMPLETED_DATE,
		 
		  A.STARTED_DATE,
		  A.TEMPLATE_DATA_ID,
		  A.PROCESS_ID                      AS PID,
		  TO_CHAR(B.DUE_DATE, 'DD/MM/YYYY') AS DUE_DATE
		FROM Z_WFQUEUE A

		INNER JOIN Z_WFTEMPLATE_ASSIGNMENT_NODE B
		ON A.PROCESS_ID = B.PROCESS_ID
		INNER JOIN Z_WFNODE C
		ON B.NODE_ID= C.ID
		INNER JOIN Z_WFTEMPLATE_DATA D
		ON D.ID =A.TEMPLATE_DATA_ID
		INNER JOIN Z_WFTEMPLATE E
		ON D.TEMPLATE_ID= E.ID
		WHERE B.ASSIGN_TYPE    =2
		AND B.ASSIGN_ID        = 1
		AND B.TEMPLATE_DATA_ID = A.TEMPLATE_DATA_ID" ;
		
		
	try {
		
		$data = $this->_db->fetchAll($sql);
		$newdata = array();
		
		
		foreach($data as $k=>$v) 
		{
			$newdata[$k]=$v;
			$newdata[$k]['CUSTOMER']=$this->_db->fetchAll("SELECT * FROM SW_WFNODE_CUSTOMER A INNER JOIN SW_CUSTOMER_DATA B  ON A.CUSTOMER_ID=B.ID WHERE A.ZWF_NODE_ID = ".$v['ID']);
			
		}
		
			}catch(Exception $e) {
		//Zend_Debug::dump($e); die($sql);
		
		}	  
		
		//Zend_Debug::dump($newdata);
	   return $newdata;
	}
	
	public function get_task_box($uid) 
    {
       
       $sql_out ="SELECT COUNT(*)
  
FROM Z_MESSAGES Z
INNER JOIN Z_WFQUEUE A
ON Z.QID= A.ID
INNER JOIN Z_WFPROCESS Q
ON Q.ID             =A.PROCESS_ID
WHERE Z.USER_ID     =$uid
AND (Q.STATUS       =1
OR Q.STATUS        IS NULL
OR Q.STATUS         =98
OR Q.STATUS         =999)
AND Q.CURRENT_QUEUE!=Z.QID
AND A.STATUS        =1";

$sql_in ="SELECT COUNT(*)
FROM Z_MESSAGES Z
INNER JOIN Z_WFQUEUE A
ON Z.QID= A.ID
INNER JOIN Z_WFPROCESS Q
ON Q.ID            =A.PROCESS_ID
WHERE Z.USER_ID    =$uid
AND (Q.STATUS      =1
OR Q.STATUS       IS NULL
OR Q.STATUS        =98)
AND Q.CURRENT_QUEUE=Z.QID";


$sql_draft ="SELECT COUNT(*)
FROM Z_MESSAGES Z
INNER JOIN Z_WFQUEUE A
ON Z.QID= A.ID
INNER JOIN Z_WFPROCESS Q
ON Q.ID            =A.PROCESS_ID
WHERE Z.USER_ID    =$uid
AND (Q.STATUS        =9)
AND Q.CURRENT_QUEUE=Z.QID";
      //die($sql);
       try {
		$out = $this->_db->fetchOne($sql_out);
		$in = $this->_db->fetchOne($sql_in);
		$draft = $this->_db->fetchOne($sql_draft);
		
		$data=array(
		'in'=>$in,
		'out'=>$out,
		'draft'=>$draft,
		);
		
       } catch (Exception $e) {
		  Zend_Debug::dump($e->getMessage()); die($sql); 
		}
       return $data;
       
    }
   
}
