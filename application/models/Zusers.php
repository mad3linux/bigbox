<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Zusers extends Zend_Db_Table_Abstract {
	public function getuser($uname) {
		$sql = "select * from z_users where uname =" . $this->_db->quote ( $uname ) . "";
		try {
			$user = $this->_db->fetchRow ( $sql );
			return $user;
		} catch ( Exception $e ) {
			Zend_Debug::dump ( $e->getMessage () );
			die ( $sql );
		}
	}
	public function update_insert_user($data) {
		#Zend_Debug::dump($data); die();
		$with = array ();
		($data ['id'] != "") ? $action = "update" : $action = 'create';
		$mainrole = (isset ( $data ['mainrole'] )) ? explode ( '-selection', $data ['mainrole'] ) : '';
	
		$result = array (
				'result' => false,
				'message' => '' 
		);
		
		if ($action == 'update') {
			$cek = $this->_db->fetchOne ( "select count(*) from z_users where id != ? and uname = ?", array (
					$data ['id'],
					$data ['uname'] 
			) );
			if ($cek >= 1) {
				return array (
						'result' => false,
						'message' => "User name '" . $data ['uname'] . "' has been exist." 
				);
			}
			$sql = "update z_users set uname =?, fullname =?, jabatan =?, email=?, mobile_phone=?, fixed_phone=?, 
				ubis_id=?, sub_ubis_id=?,  is_telkom=?, isldap=?, upassword=?,  main_role =? , app=? where id =?";
		
			if($data ['pass']=="") {
				$sql = "update z_users set uname =?, fullname =?, jabatan =?, email=?, mobile_phone=?, fixed_phone=?, 
				ubis_id=?, sub_ubis_id=?,  is_telkom=?, isldap=?,  main_role =? , app=? where id =?";
			}
		} elseif ($action == 'create') {
			$cek = $this->_db->fetchOne ( "select count(*) from z_users where uname = ?", $data ['uname'] );
			if ($cek >= 1) {
				return array (
						'result' => false,
						'message' => "User name '" . $data ['uname'] . "' has been exist." 
				);
			}
			$sql = "insert into z_users (uname, fullname, jabatan, email, mobile_phone, fixed_phone, ubis_id, sub_ubis_id, is_telkom, isldap, upassword, main_role, app) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		}
		
		try {
			if ($action == 'update') {
				// die($sql);
				$with = array (
						$data ['uname'],
						$data ['fullname'],
						$data ['position'],
						$data ['email'],
						$data ['mobile'],
						$data ['flexi'],
						$data ['ubis'],
						$data ['sububis'],
						((isset ( $data ['istelkom'] )) ? '1' : '0'),
						((isset ( $data ['isldap'] )) ? '1' : '0'),
						md5($data ['pass']),
						((isset ( $mainrole [0] ) && $mainrole [0] != '0') ? $mainrole [0] : ''),
						$data ['apps'],
						$data ['id']
						
				);
				if($data ['pass']=="") { 
					$with = array (
						$data ['uname'],
						$data ['fullname'],
						$data ['position'],
						$data ['email'],
						$data ['mobile'],
						$data ['flexi'],
						$data ['ubis'],
						$data ['sububis'],
						((isset ( $data ['istelkom'] )) ? '1' : '0'),
						((isset ( $data ['isldap'] )) ? '1' : '0'),
					
						((isset ( $mainrole [0] ) && $mainrole [0] != '0') ? $mainrole [0] : ''),
						$data ['apps'],
						$data ['id']
						
				);
				}
				
				
				$this->_db->query ( $sql, $with );
				$person_arr = array (
						$data ['fullname'],
						$data ['position'],
						$data ['email'],
						$data ['mobile'],
						$data ['flexi'],
						$data ['pid'] 
				);
				$sqlupda_pers = "update z_person set first_name=?, position=?, email=?, mobile_phone=?, fix_phone=? where person_id=?";
				
				$this->_db->query ( $sqlupda_pers, $person_arr );
				$this->_db->query ( "delete from z_users_groups where uid= ?", $data ['id'] );
				
				if (is_array ( $data ['roles'] )) {
					foreach ( $data ['roles'] as $k => $v ) {
						$this->_db->query ( "insert into  z_users_groups (uid, gid) values (?, ?) ", array (
								$data ['id'],
								$v 
						) );
					}
				}
				
				$result = array (
						'result' => true,
						'message' => 'User ' . $data ['fullname'] . ' has been updated.' 
				);
			} else {
				$with = array (
						$data ['uname'],
						$data ['fullname'],
						$data ['position'],
						$data ['email'],
						$data ['mobile'],
						$data ['flexi'],
						$data ['ubis'],
						$data ['sububis'],
						((isset ( $data ['istelkom'] )) ? '1' : '0'),
						((isset ( $data ['isldap'] )) ? '1' : '0'),
						md5($data ['pass']),
						((isset ( $mainrole [0] ) && $mainrole [0] != '0') ? $mainrole [0] : ''),
						$data ['apps'], 
				);
				// Zend_Debug::dump($with); die();
				
				$this->_db->query ( $sql, $with );
				$uid = $this->_db->lastInsertId ();
				
				$sql_person = "insert into z_person (first_name, position, email, mobile_phone, fix_phone) values (?, ?, ?, ?, ?)";
				$input_person = array (
						$data ['fullname'],
						$data ['position'],
						$data ['email'],
						$data ['mobile'],
						$data ['flexi'] 
				);
				
				$this->_db->query ( $sql_person, $input_person );
				$pid = $this->_db->lastInsertId ();
				$this->_db->query ( "update z_users set person_id = ? where id = ?", array (
						$pid,
						$uid 
				) );
				
				if (is_array ( $data ['roles'] )) {
					foreach ( $data ['roles'] as $k => $v ) {
						$this->_db->query ( "insert into  z_users_groups (uid, gid) values (?, ?) ", array (
								$uid,
								$v 
						) );
					}
				}
				
				// echo $uid;
				$result = array (
						'result' => true,
						'message' => 'User ' . $data ['fullname'] . ' has been created.' 
				);
			}
		} catch ( Exception $e ) {
			$result = array (
					'result' => false,
					'message' => $e->getMessage () 
			);
		}
		
		return $result;
	}
	public function delete_user($uid) {
		$result = array (
				'result' => false,
				'message' => '' 
		);
		$sql1 = "delete from  z_users where id = ?";
		$sql2 = "delete from  z_person where person_id = ?";
		$sql3 = "delete from  z_users_groups where uid = ?";
		try {
			$cek = $this->_db->fetchRow ( "select * from z_users where id = ?", $uid );
			// Zend_Debug::dump($cek); die();
			$this->_db->query ( $sql1, $uid );
			$this->_db->query ( $sql2, $cek ['person_id'] );
			$this->_db->query ( $sql3, $uid );
			$result = array (
					'result' => true,
					'message' => 'User ' . $cek ['fullname'] . ' has been deleted.' 
			);
		} catch ( Exception $e ) {
			$result = array (
					'result' => false,
					'message' => $e->getMessage () 
			);
		}
		return $result;
	}
	public function update_c_act($uname) {
		$qry = "update c_act_user set last_activity = current_timestamp() where nik = ?";
		// die($qry);
		try {
			$result = $this->_db->query ( $qry, $uname );
			// Zend_Debug::dump($result->rowCount()); die($qry);
			if ($result == false || $result->rowCount () == 0) {
				$qry = "insert into  c_act_user (nik,last_activity) values (?,current_timestamp())";
				$this->_db->query ( $qry, $uname );
			}
		} catch ( Zend_Db_Adapter_Exception $e ) {
			Zend_Debug::dump ( $e );
			die ( $qry );
		}
	}
	public function clear_logout($nik) {
		$sql = "delete from c_act_user where nik='$nik' ";
		// die($sql);
		try {
			$this->_db->query ( $sql );
			return true;
		} catch ( Exception $e ) {
			return false;
			Zend_Debug::dump ( $e->getMessage () );
			die ( $sql );
		}
	}
	
	public function insert_users($data) {
		// Zend_Debug::dump($data);
		 $sql = "insert into z_users (uname, upassword, isldap, email, fullname, status, user_type, descr, work_unit, person_id, current_loc, last_update, ubis_id, sub_ubis_id, jabatan, fixed_phone, mobile_phone, redirect_url, main_role, telegram, picture, app, is_telkom, employment_category, position_name, location_name, grade, umur, masa_bakti, sex, grade_name, loc_id, pendidikan, lokasi_tip, status_tip) values (
		 '".$data[0]."', '".md5($data[1])."', null, null, '".$data[2]."', null, null, '".$data[1]."', null, null, null, null, null, null, null, null, null, null, 1570, null, null, 'survey', null, null, null, null, null, null, null, null, null, ".$data[3].", null, null, null)";
		 $this->_db->query($sql);
	
		 $uid = $this->_db->lastInsertId();
		 $this->_db->query ( "insert into  z_users_groups (uid, gid) values (?, ?) ", array (
								 $uid,
								 '15' 
						 ) );
		return true;
	}
	
	public function cek_users($data) {
		// Zend_Debug::dump($data);
		$sql1 = "select * from z_users_groups where uid > 680113"; 
		$z = $this->_db->fetchAll($sql1);
		return $z;
		
	}
}
