<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Zwall extends Zend_Db_Table_Abstract{

public function get_allgroups_by_uid($uid){
	$sql = "select a.* from z_groups a inner join z_users_groups b on a.gid=b.gid where  b.uid=? order by a.group_name asc "; 
	try {
	   $user = $this->_db->fetchAll($sql, $uid);
	   return $user;
	} catch (Exception $e) {
	   Zend_Debug::dump($e->getMessage());die($sql);		
	}	
}
	
public function get_wallgroups($id){
	$q= "select 
			if(
				(select max(c.created_date) from z_group_wall_comment c where c.mgid = a.mgid group by c.mgid) is not null, 
				(select max(c.created_date) from z_group_wall_comment c where c.mgid = a.mgid group by c.mgid), 
				a.created_date
			) cdate, a.*, xx.uname, xx.fullname 
		from 
			z_group_wall a 
			inner join 
				z_groups b 
				on 
				a.gid=b.gid 
			left join 
				z_users xx 
				on 
				a.uid=xx.id 
		where 
			a.gid=? 
		order by 
			a.seq desc,cdate desc, a.mgid desc";	

	//die($q);
	try {
	   $data = $this->_db->fetchAll($q, (int)$id);
	   $var =array();
	   $com=array();
	   foreach($data as $k=>$v) 
	   {
			$sql = "select count(a.mgid_com) from z_group_wall_comment a left join z_users xx on a.uid=xx.id where a.mgid=?";   
		    $com = $this->_db->fetchOne($sql, (int)$v['mgid']);
		    //Zend_Debug::dump($com); die();
			$var[$k]=$v;
			$var[$k]['comment']=(int)$com;	
	   
	   }
	   
	   //Zend_Debug::dump($var); die();
	   return $var;
	} catch (Exception $e) {
	   Zend_Debug::dump($e->getMessage());die($sql);		
	}	
}
	
public function get_comments($id){
	$q= "select a.*, xx.uname, xx.fullname from z_group_wall_comment a left join z_users xx on a.uid=xx.id where a.mgid=? order by a.created_date asc";	

	//die($q);
	try {
	   $data = $this->_db->fetchAll($q, (int)$id);
	   
	   //Zend_Debug::dump($data); die();
	   return $data;
	} catch (Exception $e) {
	   Zend_Debug::dump($e->getMessage());die($sql);		
	}	
}
	
public function insert_wall($data, $uid, $image=false,$type = "Post"){
	$sql="insert into z_group_wall  (mess_text,uid, gid, created_date, type) values (?, ?, ?, now(), ?)";
	
	$var=array($data['xhpc_message'], $uid, $data['gid'],$type);	
		
	try {
	   $this->_db->query($sql, $var);
	   return true;
	} catch (Exception $e) {
	   Zend_Debug::dump($e->getMessage());die($sql);		
	}	
}
	
public function insert_wall_comment($data, $uid){
	$sql="insert into z_group_wall_comment  (comment_text, uid, mgid, created_date, type) values (?, ?, ?, now(), 'text')";
	
	$var=array($data['comment'], $uid, $data['mgid']);	
	try {
	   $this->_db->query($sql, $var);
	   return true;
	} catch (Exception $e) {
	   Zend_Debug::dump($e->getMessage());die($sql);		
	}	
}
	
}
