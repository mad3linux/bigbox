<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Zwfnode extends Zend_Db_Table_Abstract {

    protected $_name = 'Z_WFNODE';
    protected $_primary = 'ID';
    protected $_sequence = 'Z_WFNODE_ID_SEQ';

    public function getById($id){
        $sql = "SELECT * FROM Z_WFNODE WHERE ID = $id ";
        $items = $this->_db->fetchRow($sql);
        return $items;
    }
    
    public function fetchdata($id) {
        $sql = "SELECT A.TEMPLATE_ID,
                   
                    A.PROJECT_DESCR
                 
                    FROM Z_WFNODE A
                    WHERE ID = $id";
        //die($sql);
        $items = $this->_db->fetchRow($sql);
        
        //Zend_Debug::dump($items);die("s");
        return $items;
    }

    public function fetchdataparent() {
        $sql = "SELECT DISTINCT(A.ID),
            A.TEMPLATE_ID,
            A.ACCOUNT_MANAGER,
            A.ACCOUNT_MANAGER_NAME,
            A.BILLING_ESTIMATE,
            A.CATEGORY_ID,
            TO_CHAR(A.END_DATE,'DD/MON/YYYY' ) AS END_DATE,
           
            A.PARENT_ID,
            A.PROJECT_CODE,
            A.PROJECT_DESCR,
            A.PROJECT_NAME,
            A.REVENUE_ESTIMATE,
            A.SPONSOR,
            A.SPONSOR_NAME,
            TO_CHAR(A.START_DATE,'DD/MON/YYYY' ) AS START_DATE
            FROM Z_WFNODE A
            WHERE PARENT_ID IS  NULL
            ORDER BY  ID DESC ";
        $items = $this->_db->fetchAll($sql);
        return $items;
    }

    public function fetchdatabytdid($id) {
        $select = $this->select();
        $select->where("TEMPLATE_DATA_ID = ?", $id);
        $items = $this->fetchAll($select);
        if ($items->count() > 0) {
            return $items;
        } else {
            return null;
        }
    }

    public function deletedata($id) {

        $row = $this->find($id)->current();
        if ($row) {
            $row->delete();
            return 1;
        } else {
            return 0;
        }
    }

    public function createitem($data) {

       //Zend_Debug::dump($data); die();

        $am = explode('|', $data['ACCOUNT_MANAGER']);
        $sponsor = explode('|', $data['SPONSOR']);

        $rowUser = $this->createRow();
        if ($rowUser) {
            $rowUser->PROJECT_NAME = $data['PROJECT_NAME'];
            $rowUser->PROJECT_DESCR = $data['PROJECT_DESCR'];
            $rowUser->ACCOUNT_MANAGER = $am[1];
            $rowUser->SPONSOR = $sponsor[1];
            $rowUser->ACCOUNT_MANAGER_NAME = $am[0];
            $rowUser->SPONSOR_NAME = $sponsor[0];
            $rowUser->START_DATE = new Zend_Db_Expr("TO_DATE ('" . $data['START_DATE'] . "','DD/MM/YYYY')");
            $rowUser->END_DATE = new Zend_Db_Expr("TO_DATE ('" . $data['END_DATE'] . "','DD/MM/YYYY')");
            $rowUser->REVENUE_ESTIMATE = $data['REVENUE_ESTIMATE'];
            $rowUser->NOKES_ESTIMATE = $data['NOKES_ESTIMATE'];
            $rowUser->BILLING_ESTIMATE = $data['BILLING_ESTIMATE'];
            $rowUser->PARENT_ID = $data['PARENT_ID'];
            $rowUser->CATEGORY_ID = $data['CATEGORY_ID'];
            $rowUser->TEMPLATE_ID = $data['TEMPLATE_ID'];
            $rowUser->ORDER_ID = $data['ORDER_ID'];
            $rowUser->SOLD2PARTY = $data['SOLD2PARTY'];
            $rowUser->BILL2PARTY = $data['BILL2PARTY'];

            //Zend_Debug::dump($rowUser);     die();
            $rowUser->save();
            // Zend_Debug::dump($rowUser);     die();
            return $rowUser;
        } else {
            return false;
        }
    }

    public function deltbytid($id) {
        $table = new self();
        $where = $table->getAdapter()->quoteInto('TEMPLATE_ID = ?', $id);
        $del = $table->delete($where);

        return $del;
    }

    public function updatedata($data) {
			//Zend_Debug::dump($data);die();
        $am = explode('|', $data['ACCOUNT_MANAGER']);
        $sponsor = explode('|', $data['SPONSOR']);
        $rowUser = $this->find($data['SERVICE_ID'])->current();
        if ($rowUser) {
            $rowUser->PROJECT_NAME = $data['PROJECT_NAME'];
            $rowUser->PROJECT_DESCR = $data['PROJECT_DESCR'];
            $rowUser->ACCOUNT_MANAGER = $am[1];
            $rowUser->SPONSOR = $sponsor[1];
            $rowUser->ACCOUNT_MANAGER_NAME = $am[0];
            $rowUser->SPONSOR_NAME = $sponsor[0];
            $rowUser->START_DATE = new Zend_Db_Expr("TO_DATE ('" . $data['START_DATE'] . "','dd/mm/yyyy')");
            $rowUser->END_DATE = new Zend_Db_Expr("TO_DATE ('" . $data['END_DATE'] . "','dd/mm/yyyy')");
            $rowUser->REVENUE_ESTIMATE = $data['REVENUE_ESTIMATE'];
            $rowUser->NOKES_ESTIMATE = $data['NOKES_ESTIMATE'];
            $rowUser->BILLING_ESTIMATE = $data['BILLING_ESTIMATE'];
            $rowUser->PARENT_ID = $data['PARENT_ID'];
            $rowUser->TEMPLATE_ID = $data['TEMPLATE_ID'];
			//Zend_Debug::dump($rowUser);die();
            $rowUser->save();
            //return the updated user
            return $rowUser;
        } else {
            return null;
        }
    }

    public function updatedata2($data) {
			//Zend_Debug::dump($data);die();
        $am = explode('|', $data['ACCOUNT_MANAGER']);
        $sponsor = explode('|', $data['SPONSOR']);
        $rowUser = $this->find($data['ID'])->current();
        if ($rowUser) {
            $rowUser->PROJECT_NAME = $data['PROJECT_NAME'];
            $rowUser->PROJECT_DESCR = $data['PROJECT_DESCR'];
            $rowUser->ACCOUNT_MANAGER = $am[1];
            $rowUser->SPONSOR = $sponsor[1];
            $rowUser->ACCOUNT_MANAGER_NAME = $am[0];
            $rowUser->SPONSOR_NAME = $sponsor[0];
            $rowUser->START_DATE = new Zend_Db_Expr("TO_DATE ('" . $data['START_DATE'] . "','dd/mm/yyyy')");
            $rowUser->END_DATE = new Zend_Db_Expr("TO_DATE ('" . $data['END_DATE'] . "','dd/mm/yyyy')");
            $rowUser->REVENUE_ESTIMATE = $data['REVENUE_ESTIMATE'];
            $rowUser->NOKES_ESTIMATE = $data['NOKES_ESTIMATE'];
            $rowUser->BILLING_ESTIMATE = $data['BILLING_ESTIMATE'];
            $rowUser->PARENT_ID = $data['PARENT_ID'];
            $rowUser->TEMPLATE_ID = $data['TEMPLATE_ID'];
			//Zend_Debug::dump($rowUser);die();
            $rowUser->save();
            //return the updated user
            return $rowUser;
        } else {
            return null;
        }
    }

    public function fetchdatabytpid($id) {

        $sql = "SELECT DISTINCT(A.ID),
                    B.PROCESS_ID,
                    A.TEMPLATE_ID,
                    A.ACCOUNT_MANAGER,
                    A.ACCOUNT_MANAGER_NAME,
                    A.BILLING_ESTIMATE,
                    A.CATEGORY_ID,
                    TO_CHAR(A.END_DATE,'DD/MM/YYYY' ) AS END_DATE,
                    A.PARENT_ID,
                    A.PROJECT_CODE,
                    A.PROJECT_DESCR,
                    A.PROJECT_NAME,
                    A.REVENUE_ESTIMATE,
                    A.SPONSOR,
                    A.SPONSOR_NAME,
                    TO_CHAR(A.START_DATE,'DD/MM/YYYY' ) AS START_DATE
                    FROM Z_WFNODE A
                    INNER JOIN Z_WFTEMPLATE_ASSIGNMENT_NODE B
                    ON A.ID         =B.NODE_ID
                    WHERE PARENT_ID =$id";
        //    die($sql);

        $items = $this->_db->fetchAll($sql);

        return $items;
    }

    public function inboxservicebyparent($id) {

        $sql = "SELECT DISTINCT(A.ID),
                B.PROCESS_ID,
                D.ID AS TDID,
                A.PROJECT_NAME,
                Z.STATUS,
                A.TEMPLATE_ID,
                A.ACCOUNT_MANAGER,
                A.ACCOUNT_MANAGER_NAME,
                A.BILLING_ESTIMATE,
                A.CATEGORY_ID,
                TO_CHAR(A.END_DATE,'DD/MM/YYYY' ) AS END_DATE,
                A.PARENT_ID,
                A.PROJECT_CODE,
                A.PROJECT_DESCR,
                A.PROJECT_NAME,
                A.REVENUE_ESTIMATE,
                A.SPONSOR,
                A.SPONSOR_NAME,
                TO_CHAR(A.START_DATE,'DD/MM/YYYY' ) AS START_DATE,
                D.TASK_NAME
                FROM Z_WFNODE A
                INNER JOIN Z_WFTEMPLATE_ASSIGNMENT_NODE B
                ON A.ID         =B.NODE_ID
                LEFT JOIN  Z_WFPROJECTS Z ON Z.PROJECT_CONTENT_ID=A.ID
                LEFT JOIN  Z_WFPROCESS Y ON Z.ID=Y.TRACKING_ID
                LEFT JOIN Z_WFQUEUE W ON Y.CURRENT_QUEUE=W.ID
                LEFT JOIN Z_WFTEMPLATE_DATA D ON D.ID=W.TEMPLATE_DATA_ID
                WHERE PARENT_ID =".$id." ORDER BY A.ID DESC";
         // die($sql);

        $items = $this->_db->fetchAll($sql);

        return $items;
    }

}
