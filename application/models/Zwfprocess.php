<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Zwfprocess extends Zend_Db_Table_Abstract {

    protected $_name = 'Z_WFPROCESS';
    protected $_primary = 'ID';
    protected $_sequence = 'Z_WFPROCESS_ID_SEQ';

    public function createdata($data) {
        $row = $this->createRow();

        if ($row) {
            $row->TEMPLATE_ID = $data['TEMPLATE_ID'];
            $row->FLOW_NAME = $data['FLOW_NAME'];
            $row->COMPLETE = $data['COMPLETE'];
            $row->INITIATOR_UID = $data['INITIATOR_UID'];
            $row->PID = $data['PID'];
            $row->INITIATING_PID = $data['INITIATING_PID'];
            $row->TRACKING_ID = $data['TRACKING_ID'];
            $row->INITIATED_DATE = new Zend_Db_Expr('SYSDATE');

            $row->save();

            return $row;
        } else {
            return 0;
        }
    }

    public function fetchdatafrom($id) {
        $select = $this->select();
        $select->where("ID = ?", $id);
        $items = $this->fetchAll($select);
        if ($items->count() > 0) {
            return $items;
        } else {
            return null;
        }
    }

    public function fetchdata() {
        $select = $this->select();
        $items = $this->fetchAll($select)->order('ID');
        if ($items->count() > 0) {
            return $items;
        } else {
            return null;
        }
    }

    public function deletedata($id) {
        $row = $this->find($id)->current();
        if ($row) {
            $row->delete();

            return 1;
        } else {
            return 0;
        }
    }

    public function querylaunch($UID) {

        $sql = "SELECT F.TEMPLATE_DATA_ID AS TDID,
            A.*,
            B.COMPLETE,
            B.COMPLETED_DATE,
            B.FLOW_NAME,
            B.INITIATING_PID,
            B.INITIATOR_UID,
            TO_CHAR(B.INITIATED_DATE,'DD/MM/YYYY HH24:MM' ) AS INITIATED_DATE,
            U.ID                                            AS NDID
            FROM Z_WFPROJECTS A
            INNER JOIN Z_WFPROCESS B
            ON A.ID= B.TRACKING_ID
            INNER JOIN Z_WFQUEUE F
            ON F.PROCESS_ID = B.ID
            INNER JOIN Z_WFNODE U
            ON U.ID               = A.PROJECT_CONTENT_ID
            WHERE A.ORIGINATOR_UID=" . $UID . "
            AND F.STATUS          = 0
            AND F.TASK_CLASS_NAME ='MaestroTaskTypeStart'";
        //die($sql);

        $items = $this->_db->fetchAll($sql);

        if (count($items) > 0) {
            return $items;
        } else {
            return null;
        }
    }

    public function querylaunch2($UID, $NID) {

        $sql = "SELECT F.TEMPLATE_DATA_ID AS TDID,
            A.*,
            B.COMPLETE,
            B.COMPLETED_DATE,
            B.FLOW_NAME,
            B.INITIATING_PID,
            B.INITIATOR_UID,
            TO_CHAR(B.INITIATED_DATE,'DD/MM/YYYY HH24:MM' ) AS INITIATED_DATE,
            U.ID                                            AS NDID
            FROM Z_WFPROJECTS A
            INNER JOIN Z_WFPROCESS B
            ON A.ID= B.TRACKING_ID
            INNER JOIN Z_WFQUEUE F
            ON F.PROCESS_ID = B.ID
            INNER JOIN Z_WFNODE U
            ON U.ID                 = A.PROJECT_CONTENT_ID
            WHERE A.ORIGINATOR_UID  =" . $UID . "
            AND A.PROJECT_CONTENT_ID=" . $NID . "
            AND F.STATUS            = 0
            AND F.TASK_CLASS_NAME   ='MaestroTaskTypeStart'";

        $items = $this->_db->fetchRow($sql);

        if (count($items) > 0) {
            return $items;
        } else {
            return null;
        }
    }

    public function querydata($id) {
        $sql = "SELECT C.PARENT_ID,
                    C.ACCOUNT_MANAGER,
                    C.ACCOUNT_MANAGER_NAME,
                    C.BILLING_ESTIMATE,
                    TO_CHAR(C.END_DATE, 'DD/MM/YYYY') AS END_DATE,
                    C.ID,
                    C.NOKES_ESTIMATE,
                    C.PROJECT_DESCR,
                    C.PROJECT_NAME,
                    C.REVENUE_ESTIMATE,
                    C.SPONSOR,
                    C.SPONSOR_NAME,
                    TO_CHAR(C.START_DATE, 'DD/MM/YYYY') AS START_DATE,
                    A.COMPLETE,
                    A.COMPLETED_DATE,
                    A.FLOW_NAME,
                    TO_CHAR(A.INITIATED_DATE, 'DD/MM/YYYY') AS INITIATED_DATE,
                    A.INITIATING_PID,
                    A.INITIATOR_UID,
                    A.PID,
                    A.TEMPLATE_ID,
                    B.DESCRIPTION,
                    B.PROJECT_NUM,
                    B.PROJECT_CONTENT_ID,
                    B.STATUS
                    FROM Z_WFPROCESS A
                    INNER JOIN Z_WFPROJECTS B
                    ON A.TRACKING_ID = B.ID
                    INNER JOIN Z_WFNODE C
                    ON B.PROJECT_CONTENT_ID=C.ID
                    WHERE A.ID             =" . $id . "";

        $items = $this->_db->fetchRow($sql);

        return $items;
    }

    public function querydataparent($id) {
        $sql = "SELECT Z.PROJECT_NAME               AS PPROJECT_NAME,
                Z.ACCOUNT_MANAGER                   AS PACCOUNT_MANAGER,
                Z.ACCOUNT_MANAGER_NAME              AS PACCOUNT_MANAGER_NAME,
                Z.BILLING_ESTIMATE                  AS PBILLING_ESTIMATE,
                Z.BILLING_ESTIMATE                  AS PBILLING_ESTIMATE,
                TO_CHAR(Z.END_DATE, 'DD/MM/YYYY')   AS PEND_DATE,
                Z.NOKES_ESTIMATE                    AS PNOKES_ESTIMATE,
                Z.PROJECT_DESCR                     AS PPROJECT_DESCR,
                Z.PROJECT_NAME                      AS PPROJECT_NAME ,
                Z.REVENUE_ESTIMATE                  AS PREVENUE_ESTIMATE,
                Z.SPONSOR                           AS PSPONSOR,
                Z.SPONSOR_NAME                      AS PSPONSOR_NAME,
                TO_CHAR(Z.START_DATE, 'DD/MM/YYYY') AS PSTART_DATE,
                C.BILLING_ESTIMATE,
                C.PARENT_ID,
                C.ACCOUNT_MANAGER,
                C.ACCOUNT_MANAGER_NAME,
                C.BILLING_ESTIMATE,
                TO_CHAR(C.END_DATE, 'DD/MM/YYYY') AS END_DATE,
                C.ID,
                C.NOKES_ESTIMATE,
                C.PROJECT_DESCR,
                C.PROJECT_NAME,
                C.REVENUE_ESTIMATE,
                C.SPONSOR,
                C.SPONSOR_NAME,
                TO_CHAR(C.START_DATE, 'DD/MM/YYYY') AS START_DATE,
                A.COMPLETE,
                A.COMPLETED_DATE,
                A.FLOW_NAME,
                TO_CHAR(A.INITIATED_DATE, 'dd/mm/yyyy') AS INITIATED_DATE,
                A.INITIATING_PID,
                A.INITIATOR_UID,
                A.PID,
                A.TEMPLATE_ID,
                B.DESCRIPTION,
                B.PROJECT_NUM,
                B.PROJECT_CONTENT_ID,
                B.STATUS
                FROM Z_WFPROCESS A
                INNER JOIN Z_WFPROJECTS B
                ON A.TRACKING_ID = B.ID
                INNER JOIN Z_WFNODE C
                ON B.PROJECT_CONTENT_ID=C.ID
                LEFT JOIN Z_WFNODE Z
                ON C.PARENT_ID=Z.ID
                WHERE A.ID    =" . $id . "";
        $items = $this->_db->fetchRow($sql);
        return $items;
    }

    public function updatecurrent($data) {
        $rowUser = $this->find($data['ID'])->current();
        if ($rowUser) {
            $rowUser->CURRENT_QUEUE = $data['CURRENT_QUEUE'];
            $rowUser->COMPLETE = $data['COMPLETE'];
            $rowUser->COMPLETED_DATE = $data['COMPLETED_DATE'];
            $rowUser->save();
            return $rowUser;
        } else {
            return null;
        }
    }
    
    
 public function completed($id, $uid) {

		$upd ="UPDATE Z_WFPROCESS SET STATUS=999, COMPLETED_DATE=SYSDATE WHERE ID=$id";
		
			
		$sql ="INSERT INTO Z_WFPROCESS_STATUS (PID, STATUS, USER_ID, UPDATED_DATE, QID) VALUES ('', 999, '$uid', SYSDATE,  '')" ;
		
		try {
			$this->_db->query($upd);
			$this->_db->query($sql);
			//die("s");
			return true;
				}catch(Exception $e) {
					Zend_Debug::dump($e); die($sql);
			}	  

    }


}
