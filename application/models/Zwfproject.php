<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Zwfproject extends Zend_Db_Table_Abstract {

    protected $_name = 'Z_WFPROJECTS';
    protected $_primary = 'ID';
    protected $_sequence = 'Z_WFPROJECTS_ID_SEQ';

    public function createdata($data) {
        // Zend_Debug::dump($data);die();
        $row = $this->createRow();
        if ($row) {
            $row->PROJECT_NUM = $data['PROJECT_NUM'];
            $row->ORIGINATOR_UID = $data['ORIGINATOR_UID'];
            $row->DESCRIPTION = $data['DESCRIPTION'];
            $row->STATUS = $data['STATUS'];
            $row->PREV_STATUS = $data['PREV_STATUS'];
            $row->RELATED_PROCESSES = $data['RELATED_PROCESSES'];
            $row->PROJECT_CONTENT_ID = $data['PROJECT_CONTENT_ID'];
            $row->save();

            return $row;
        } else {
            return 0;
        }
    }

    public function fetchdatafrom($id) {
        $select = $this->select();
        $select->where("ID = ?", $id);
        $items = $this->fetchAll($select);
        if ($items->count() > 0) {
            return $items;
        } else {
            return null;
        }
    }

    public function fetchdata() {
        $select = $this->select();
        $items = $this->fetchAll($select)->order('WEIGHT ASC');
        if ($items->count() > 0) {
            return $items;
        } else {
            return null;
        }
    }

    public function deletedata($id) {
        $row = $this->find($id)->current();
        if ($row) {
            $row->delete();

            return 1;
        } else {
            return 0;
        }
    }

    public function del1($id1, $id2, $id3) {
        $table = new self();
        $where = $table->getAdapter()->quoteInto('TEMPLATE_DATA_TO = ? OR TEMPLATE_DATA_TO_FALSE = ? OR TEMPLATE_DATA_FROM = ?', $id1, $id2, $id3);

        $del = $table->delete($where);

        return $del;
    }

    public function updatestat($id, $status) {

        $table = new self();
        $where = $table->getAdapter()->quoteInto('PROJECT_CONTENT_ID = ?', $id);

        $update = array(
            'STATUS' => $status,
        );

        $i = $table->update($update, $where);
        return $i;
    }

}
