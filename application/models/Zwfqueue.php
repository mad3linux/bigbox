<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Zwfqueue extends Zend_Db_Table_Abstract {

    protected $_name = 'Z_WFQUEUE';
    protected $_primary = 'ID';
    protected $_sequence = 'Z_WFQUEUE_ID_SEQ';

    public function fetchdata($id) {
        $select = $this->select();
        $select->where("ID = ?", $id);
        $items = $this->fetchAll($select);
        if ($items->count() > 0) {
            return $items;
        } else {
            return null;
        }
    }

    public function fetchdatabytdid($id) {
        $select = $this->select();
        $select->where("TEMPLATE_DATA_ID = ?", $id);
        $items = $this->fetchAll($select);
        if ($items->count() > 0) {
            return $items;
        } else {
            return null;
        }
    }

    public function deletedata($id) {

        $row = $this->find($id)->current();
        if ($row) {
            $row->delete();
            return 1;
        } else {
            return 0;
        }
    }

    public function createitem($data) {

        //   Zend_Debug::dump($data);die();

        $rowUser = $this->createRow();
        if ($rowUser) {

            $rowUser->PROCESS_ID = $data['PROCESS_ID'];
            $rowUser->TEMPLATE_DATA_ID = $data['TEMPLATE_DATA_ID'];
            $rowUser->TASK_CLASS_NAME = $data['TASK_CLASS_NAME'];
            $rowUser->IS_INTERACTIVE = $data['IS_INTERACTIVE'];
            $rowUser->SHOW_IN_DETAIL = $data['SHOW_IN_DETAIL'];
            $rowUser->HANDLER = $data['HANDLER'];
            $rowUser->TASK_DATA = $data['TASK_DATA'];
            $rowUser->TEMP_DATA = $data['TEMP_DATA'];
            $rowUser->STATUS = $data['STATUS'];
            $rowUser->ARCHIVED = $data['ARCHIVED'];
            $rowUser->RUN_ONCE = $data['RUN_ONCE'];
            $rowUser->ZUID = $data['ZUID'];
            $rowUser->PREPOPULATE = $data['PREPOPULATE'];
            $rowUser->CREATED_DATE = new Zend_Db_Expr('SYSDATE');
//        $rowUser->STARTED_DATE = $data['STARTED_DATE'];
            $rowUser->COMPLETED_DATE = $data['COMPLETED_DATE'];
            $rowUser->NEXT_REMINDER_TIME = $data['NEXT_REMINDER_TIME'];
            $rowUser->NUM_REMINDER_SENT = $data['NUM_REMINDER_SENT'];
			 $rowUser->TASK_NAME = $data['TASK_NAME'];
            $rowUser->save();

            return $rowUser;
        } else {
            return null;
        }
    }

    public function updatecomplete($data) {
//  	Zend_Debug::dump($data); die();
        $rowUser = $this->find($data['ID'])->current();
        if ($rowUser) {
            $rowUser->STATUS = 1;
            $rowUser->STATUS_DESCR = $data['STATUS_DESCR'];
            $rowUser->ZUID = $data['ZUID'];
            $rowUser->COMPLETED_DATE = new Zend_Db_Expr('SYSDATE');
            ;
            $rowUser->save();
            return $rowUser;
        } else {
            return null;
        }
    }

    public function deltbytid($id) {
        $table = new self();
        $where = $table->getAdapter()->quoteInto('TEMPLATE_ID = ?', $id);
        $del = $table->delete($where);

        return $del;
    }

    public function fetchdatastarted($id) {
        $select = $this->select();
        $select->where("PROCESS_ID = ? AND TASK_CLASS_NAME = 'MaestroTaskTypeStart'", $id);
        //die($select);
        $items = $this->fetchRow($select);
        return $items;
    }

    public function fetchdatacurrent($id) {
        $select = $this->select();
        $select->where("PROCESS_ID = ? AND STATUS= 0", $id);
        $items = $this->fetchRow($select)->toArray();
        return $items;
    }

    public function taskinbox($data) {

        $sql = "SELECT DISTINCT(C.ID), D.TASK_NAME,
            C.*,
            E.TEMPLATE_NAME,
            A.COMPLETED_DATE,
            U.PROJECT_NAME AS PARENTSERV,
            A.STARTED_DATE,
            A.TEMPLATE_DATA_ID,
            A.PROCESS_ID AS PID,
            TO_CHAR(B.DUE_DATE, 'DD/MM/YYYY') AS DUE_DATE
            FROM Z_WFQUEUE A
            INNER JOIN Z_WFTEMPLATE_ASSIGNMENT_NODE B
            ON A.PROCESS_ID = B.PROCESS_ID
            INNER JOIN Z_WFNODE C
            ON B.NODE_ID= C.ID
            INNER JOIN Z_WFTEMPLATE_DATA D
            ON D.ID =A.TEMPLATE_DATA_ID
            INNER JOIN Z_WFTEMPLATE E
            ON D.TEMPLATE_ID= E.ID
            LEFT JOIN Z_WFNODE U
            ON C.PARENT_ID         =U.ID
            INNER JOIN Z_WFPROJECTS O ON O.PROJECT_CONTENT_ID=C.ID

            WHERE B.ASSIGN_TYPE    =1
            AND B.ASSIGN_ID        = " . $data['UID'] . "
            AND A.STATUS           =0
            AND B.TEMPLATE_DATA_ID = A.TEMPLATE_DATA_ID
            AND O.STATUS!=9";
            
               $sql = "SELECT DISTINCT(C.ID), D.TASK_NAME,
            C.*,
            E.TEMPLATE_NAME,
            A.COMPLETED_DATE,
            U.PROJECT_NAME AS PARENTSERV,
            A.STARTED_DATE,
            A.TEMPLATE_DATA_ID,
            A.PROCESS_ID AS PID,
            TO_CHAR(B.DUE_DATE, 'DD/MM/YYYY') AS DUE_DATE
            FROM Z_WFQUEUE A
            INNER JOIN Z_WFTEMPLATE_ASSIGNMENT_NODE B
            ON A.PROCESS_ID = B.PROCESS_ID
            INNER JOIN Z_WFNODE C
            ON B.NODE_ID= C.ID
            INNER JOIN Z_WFTEMPLATE_DATA D
            ON D.ID =A.TEMPLATE_DATA_ID
            INNER JOIN Z_WFTEMPLATE E
            ON D.TEMPLATE_ID= E.ID
            LEFT JOIN Z_WFNODE U
            ON C.PARENT_ID         =U.ID
            INNER JOIN Z_WFPROJECTS O ON O.PROJECT_CONTENT_ID=C.ID

            WHERE B.ASSIGN_TYPE    =1
          
            AND A.STATUS           =0
            AND B.TEMPLATE_DATA_ID = A.TEMPLATE_DATA_ID
            AND O.STATUS!=9";
         // die($sql);

        $items = $this->_db->fetchAll($sql);

        if (count($items) > 0) {
            return $items;
        } else {

            return NULL;
        }
    }

    public function fetchbypidtdid($pid, $tdid) {
        $select = $this->select();
        $select->where("PROCESS_ID = ?", $pid)->where("TEMPLATE_DATA_ID= ?", $tdid);
        //die($select);
        $var = $this->fetchRow($select);
        
        //Zend_Debug::dump($items);die();
        return $var;
        
        
    }

}
