<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Zwfqueuefrom extends Zend_Db_Table_Abstract
{
  protected $_name = 'Z_WFQUEUE_FROM';
  protected $_primary = 'ID';
  protected $_sequence = 'Z_WFQUEUE_FROM_ID_SEQ';

  public function fetchdata($id)
  {
    $select=$this->select();
    $select->where("ID = ?", $id);
    $items = $this->fetchAll($select);
    if ($items->count() > 0) {
    return $items;
    } else {
    return null;
    }
  }

  public function deletedata($id)
  {   

    $row = $this->find($id)->current();
    if($row) {
    $row->delete();
    return 1;
    }else{
    return 0;
    }
  }
  public function createitem ($data)
  {

   //  Zend_Debug::dump($data);die();
      
    $rowUser = $this->createRow();
    if ($rowUser) {
    
        
        $rowUser->QUEUE_ID= $data['QUEUE_ID'];
        $rowUser->FROM_QUEUE_ID= $data['FROM_QUEUE_ID'];
     
      
      $rowUser->save();

        return $rowUser;
    } else {
        return null;
    }

  }
  
   public function updatecomplete($data)
  {

    $rowUser = $this->find($data['ID'])->current();
      
    $rowUser = $this->createRow();
    if ($rowUser) {
    
       
        $rowUser->STATUS = 1;
        $rowUser->ZUID = $data['ZUID'];
        $rowUser->COMPLETED_DATE= new Zend_Db_Expr('SYSDATE');;
      $rowUser->save();

        return $rowUser;
    } else {
        return null;
    }

  }
  
  
    public function deltbytid($id)
    {   
        $table = new self();
        $where = $table->getAdapter()->quoteInto('TEMPLATE_ID = ?', $id);
        $del=$table->delete($where);

        return $del;

    }
 
    
     public function fetchdatastarted($id) {
        $select = $this->select();
        $select->where("PROCESS_ID = ? AND TASK_CLASS_NAME = 'MaestroTaskTypeStart'", $id);
        $items = $this->fetchRow($select)->toArray();
            return $items;
       }

}
