<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Zwftaskproject extends Zend_Db_Table_Abstract
  {
  protected $_name = 'Z_WFTASK_PROJECTS';
  protected $_primary = 'ID';
  protected $_sequence = 'Z_WFTASK_PROJECTS_ID_SEQ';
  
  
  
  public function createdata($data)
  {
         
      $row = $this->createRow();

      if ($row) {
      $row->TASK_NAME = $data['TASK_NAME'];
      $row->TASK_DESCR= $data['TASK_DESCR'];
      $row->PROJECT_TYPE= $data['PROJECT_TYPE'];
      
      $row->save();

      return $row;
      } else {
      return 0;
      }

  }
  public function fetchdatafrom($id)
  {
    $select=$this->select();
    $select->where("TEMPLATE_DATA_FROM = ?", $id);
    $items = $this->fetchAll($select);
    if ($items->count() > 0) {
      return $items;
    } else {
      return null;
    }
  }
public function fetchdata()
  {
    $select=$this->select();
    $items = $this->fetchAll($select)->order('WEIGHT ASC');
    if ($items->count() > 0) {
      return $items;
    } else {
      return null;
    }
  }
    public function deletedata($id)
    {   
        $row = $this->find($id)->current();
        if($row) {  
        $row->delete();

        return 1;
        }else{
        return 0;
        }
    }
    
    public function del1($id1, $id2, $id3)
    {
     $table = new self();
     $where = $table->getAdapter()->quoteInto('TEMPLATE_DATA_TO = ? OR TEMPLATE_DATA_TO_FALSE = ? OR TEMPLATE_DATA_FROM = ?', $id1, $id2, $id3);
        
     $del=$table->delete($where);

     return $del;
        
        
    }        
    
  
  
  



  }
