<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Zwftdnodeassign extends Zend_Db_Table_Abstract {

    protected $_name = 'Z_WFTEMPLATE_ASSIGNMENT_NODE';
    protected $_primary = 'ID';
    protected $_sequence = 'Z_WFTEMPLATE_ASSIGN_ND_ID_SEQ';

    public function fetchdata($id) {
        $select = $this->select();
        $select->where("ID = ?", $id);
        $items = $this->fetchAll($select);
        if ($items->count() > 0) {
            return $items;
        } else {
            return null;
        }
    }

    public function fetchdatabynid($id) {
//  	echo $id;die();
        $select = $this->select();
        $select->where("NODE_ID = ?", $id);

        //die($select);
        $items = $this->fetchAll($select);
        $items = $items->toArray();

        if (count($items) > 0) {
            return $items;
        } else {
            return null;
        }
    }

    public function fetchdatabynidtdid($id, $tdid) {

        $sql = "SELECT A.*,
                    B.FULLNAME,
                    B.NIK
                    FROM Z_WFTEMPLATE_ASSIGNMENT_NODE A
                    INNER JOIN Z_USERS B
                    ON A.ASSIGN_ID        =B.ID
                    WHERE A.ASSIGN_TYPE   =1
                    AND A.NODE_ID         =" . $id . "
                    AND A.TEMPLATE_DATA_ID=" . $tdid . "";
         ///die($sql);
        $items = $this->_db->fetchAll($sql);
        if (count($items) > 0) {
            return $items;
        } else {
            return null;
        }
    }

    public function fetchdatabynidbyuser($id) {
        $sql = "SELECT DISTINCT A.ID,
                    A.TEMPLATE_DATA_ID,
                    ASSIGN_ID,
                    NODE_ID,
                    A.PROCESS_ID,
                    TO_CHAR(A.DUE_DATE, 'dd/mm/yyyy') AS DUE_DATE,
                    B.FULLNAME,
                    B.NIK,
                    F.FILE_NAME,
                    F.FILE_SIZE
                    FROM Z_WFTEMPLATE_ASSIGNMENT_NODE A
                    INNER JOIN Z_USERS B
                    ON A.ASSIGN_ID=B.ID
                    LEFT JOIN Z_FILES F
                    ON A.TEMPLATE_DATA_ID=F.TEMPLATE_DATA_ID
                    WHERE A.ASSIGN_TYPE  =1
                    AND A.NODE_ID        = " . $id . "";

        $items = $this->_db->fetchAll($sql);
        if (count($items) > 0) {
            return $items;
        } else {
            return null;
        }
    }

    public function deletedata($id) {

        $row = $this->find($id)->current();
        
        if ($row) {
            $row->delete();
            return 1;
        } else {
            return 0;
        }
    }

    public function deletedatabynid($id) {
    	//die($id);
        $table = new self();
        $where = $table->getAdapter()->quoteInto('NODE_ID = ?', $id);
        $del = $table->delete($where);

        return $del;
    }

    public function createitem($data) {
	//	Zend_Debug::dump($data);die();
        $rowUser = $this->createRow();
        if ($rowUser) {
            $rowUser->TEMPLATE_DATA_ID = $data['TEMPLATE_DATA_ID'];
            $rowUser->ASSIGN_TYPE = $data['ASSIGN_TYPE'];
            $rowUser->ASSIGN_BY = $data['ASSIGN_BY'];
            $rowUser->ASSIGN_ID = $data['ASSIGN_ID'];
            $rowUser->NODE_ID = $data['NODE_ID'];
            $rowUser->PROCESS_ID = $data['PROCESS_ID'];
            $rowUser->DUE_DATE = $data['DUE_DATE'];
            $rowUser->ESC_TO = $data['ESC_TO'];
            $rowUser->NOTIF_TEMP = $data['NOTIF_TEMP'];
            $rowUser->ESC_TEMP = $data['ESC_TEMP'];
            //Zend_Debug::dump($rowUser); die();
            $rowUser->save();
            // Zend_Debug::dump($rowUser); die();
            return $rowUser;
        } else {
            return false;
        }
    }

    public function deltbytid($id) {
        $table = new self();
        $where = $table->getAdapter()->quoteInto('TEMPLATE_ID = ?', $id);
        $del = $table->delete($where);

        return $del;
    }

    public function updatedata($data) {


        $am = explode('|', $data['ACCOUNT_MANAGER']);
        $sponsor = explode('|', $data['SPONSOR']);
        //Zend_Debug::dump($data); die();   
        $rowUser = $this->find($data['nid'])->current();
        if ($rowUser) {


            $rowUser->PROJECT_NAME = $data['PROJECT_NAME'];
            $rowUser->PROJECT_DESCR = $data['PROJECT_DESCR'];
            $rowUser->ACCOUNT_MANAGER = $am[1];
            $rowUser->SPONSOR = $sponsor[1];
            $rowUser->ACCOUNT_MANAGER_NAME = $am[0];
            $rowUser->SPONSOR_NAME = $sponsor[0];
            $rowUser->START_DATE = new Zend_Db_Expr("TO_DATE ('" . $data['START_DATE'] . "','dd/mm/yyyy')");
            $rowUser->END_DATE = new Zend_Db_Expr("TO_DATE ('" . $data['END_DATE'] . "','dd/mm/yyyy')");
            $rowUser->REVENUE_ESTIMATE = $data['REVENUE_ESTIMATE'];
            $rowUser->NOKES_ESTIMATE = $data['NOKES_ESTIMATE'];
            $rowUser->BILLING_ESTIMATE = $data['BILLING_ESTIMATE'];

            $rowUser->save();
            //return the updated user
            return $rowUser;
        } else {
            return null;
        }
    }

    public function updatelaunch($data) {



        $table = new self();
        $where = $table->getAdapter()->quoteInto('NODE_ID = ?', $data['ID']);


        $update = array(
            'PROCESS_ID' => $data['PROCESS_ID'],
        );

        $i = $table->update($update, $where);
        return $i;
    }

}
