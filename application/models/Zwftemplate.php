<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Zwftemplate extends Zend_Db_Table_Abstract {

    protected $_name = 'Z_WFTEMPLATE';
    protected $_primary = 'ID';
    protected $_sequence = 'Z_WFTEMPLATE_ID_SEQ';

    public function fetchdata($id) {
        $select = $this->select();
        $select->where("ID = ?", $id);
        $items = $this->fetchRow($select);
        return $items;
    }

    public function createdatax($data, $name) {
        //        Zend_Debug::dump($data); die();
        unset($data['ID']);

        $rowUser = $this->createRow();

        if ($rowUser) {
            $rowUser->TEMPLATE_NAME = $name;
            $rowUser->CANVAS_HEIGHT = $data['CANVAS_HEIGHT'];
            $rowUser->APP_GROUP = $data['APP_GROUP'];
            $rowUser->save();
            //Zend_Debug::dump($rowUser); die();

            return $rowUser->ID;
        } else {

            return 0;
        }
    }

    public function fetchalldata() {
        $select = $this->select();
        
        try {
			$items = $this->fetchAll($select);
			} catch (Exception $e) 
			{
				Zend_Debug::dump($e); die($select);	
			}
        
        
        
        return $items;
    }

    public function deleteitem($id) {
        $row = $this->find($id)->current();
        if ($row) {
            $row->delete();
            return 1;
        } else {
            return 0;
        }
    }

    public function updatedata($data) {
        $rowUser = $this->find($data['ID'])->current();
        if ($rowUser) {
            $rowUser->TEMPLATE_NAME = $data['TEMPLATE_NAME'];

            $rowUser->save();

            return $rowUser;
        } else {
            throw new Zend_Exception("User update failed.  User not found!");
        }
    }

    public function updatestatus($data) {
        $rowUser = $this->find($data['ID'])->current();
        if ($rowUser) {
            $rowUser->STATUS = $data['STATUS'];

            $rowUser->save();

            return $rowUser;
        } else {
            throw new Zend_Exception("User update failed.  User not found!");
        }
    }
    
      public function fetchalldataunarch() {
        $select = $this->select()->where("STATUS IS NULL");
        // $select->where("ID = ?", $id);
        $items = $this->fetchAll($select);
        return $items;
    }
    
}
