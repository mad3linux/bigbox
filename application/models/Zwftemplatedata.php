<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Zwftemplatedata extends Zend_Db_Table_Abstract {

    protected $_name = 'Z_WFTEMPLATE_DATA';
    protected $_primary = 'ID';
    protected $_sequence = 'Z_WFTEMPLATE_DATA_ID_SEQ';

    
    
    public function fetchdatabytid($id) {
        $select = $this->select('Z_WFTEMPLATE_DATA')->setIntegrityCheck(false);
        $select->where("TEMPLATE_ID = ?", $id);
        $items = $this->fetchAll($select);
        if ($items->count() > 0) {
            return $items;
        } else {
            return null;
        }
    }

    public function fetchdatabyid($id) {
        $select = $this->select()->setIntegrityCheck(false)
        ->from(array('A' => 'Z_WFTEMPLATE_DATA'),
                    array('*'))
             ->joinLeft(array('B' => 'Z_WFTEMPLATE_ASSIGNMENT'), 'A.ID = B.TEMPLATE_DATA_ID',
                    array('ZID'=>'ID', 'ASSIGN_TYPE', 'ASSIGN_BY', 'ASSIGN_ID'))->where("A.ID = ?", $id);
       // echo ($select); die();
        $items = $this->fetchRow($select);


        //Zend_Debug::dump($items);die();
        return $items;
    }

    public function createdata($data) {

        // Zend_Debug::dump($data); die("asdad");
        if ($data[ID]) {

            unset($data[ID]);
        }

        $rowUser = $this->createRow();

        if ($rowUser) {
            $rowUser->TEMPLATE_ID = $data['TEMPLATE_ID'];
            $rowUser->TASK_NAME = $data['TASK_NAME'];
            $rowUser->TASK_CLASS_NAME = $data['TASK_CLASS_NAME'];
            $rowUser->IS_INTERACTIVE = $data['IS_INTERACTIVE'];
            $rowUser->TASK_CLASS_NAME = $data['TASK_CLASS_NAME'];
            $rowUser->IS_INTERACTIVE = $data['IS_INTERACTIVE'];

            if ($data['IS_INTERACTIVE']) {
                //$rowUser->ASSIGNED_BY_VARIABLE= 1;
                $rowUser->SHOW_IN_DETAIL = 1;
            }

            $rowUser->OFFSET_LEFT = $data['OFFSET_LEFT'];
            $rowUser->OFFSET_TOP = $data['OFFSET_TOP'];
            ($data['FIRST_TASK']) ? $v = $data['FIRST_TASK'] : $v = 0;
            $rowUser->FIRST_TASK = $v;

            $rowUser->save();

            return $rowUser;
        } else {
            return 0;
        }
    }

    public function move($id, $left, $top) {
        $rowUser = $this->find($id)->current();
        if ($rowUser) {
            $rowUser->OFFSET_LEFT = $left;
            $rowUser->OFFSET_TOP = $top;

            $rowUser->save();

            return $rowUser;
        } else {
            throw new Zend_Exception("data update failed.  User not found!");
        }
    }

    public function deletedata($id) {
        $rowUser = $this->find($id)->current();
        if ($rowUser) {
            $rowUser->delete();

            return 1;
        } else {
            return 0;
        }
    }

    public function deltbytid($id) {
        $table = new self();
        $where = $table->getAdapter()->quoteInto('TEMPLATE_ID = ?', $id);
        $del = $table->delete($where);

        return $del;
    }
  public function steppingworkflow_with_pic($tid) {
        $sql1 = "SELECT *
                    FROM
                    (SELECT A.TASK_NAME,
                        A.ID,
                        A.FIRST_TASK,
                        B.TEMPLATE_DATA_FROM,
                        B.TEMPLATE_DATA_TO
                    FROM Z_WFTEMPLATE_DATA A
                    INNER JOIN Z_WFTEMPLATE_DATA_NEXT B
                    ON A.ID           =B.TEMPLATE_DATA_FROM
                    WHERE TEMPLATE_ID =" . $tid . "
                    )
                    START WITH FIRST_TASK             =1
                    CONNECT BY PRIOR TEMPLATE_DATA_TO =ID";
	
	
		try{
	
			$item = $this->_db->fetchAll($sql1);
			$new =array();
			foreach ($item as $k=>$i) 
			{ 
				$new[$k]=$i;
$new[$k]['pic']=$this->_db->fetchAll("SELECT * FROM Z_WFTEMPLATE_ASSIGNMENT  WHERE  TEMPLATE_DATA_ID =".$i['TEMPLATE_DATA_FROM']);		
			}
			
		} catch (Exception $e){
			
			Zend_Debug::dump($e); die();
		}
        
        return $new;
    }

    public function steppingworkflow($tid) {
        $sql1 = "SELECT *
                    FROM
                    (SELECT A.TASK_NAME,
                        A.ID,
                        A.FIRST_TASK,
                        B.TEMPLATE_DATA_FROM,
                        B.TEMPLATE_DATA_TO
                    FROM Z_WFTEMPLATE_DATA A
                    INNER JOIN Z_WFTEMPLATE_DATA_NEXT B
                    ON A.ID           =B.TEMPLATE_DATA_FROM
                    WHERE TEMPLATE_ID =" . $tid . "
                    )
                    START WITH FIRST_TASK             =1
                    CONNECT BY PRIOR TEMPLATE_DATA_TO =ID";
//    	echo $sql1,'<br><br>';
        $item = $this->_db->fetchAll($sql1);
//    Zend_Debug::dump($item);die();
        return $item;
    }

    public function steppingworkflowcurrent($tid, $curid) {
        $sql1 = "SELECT *
                    FROM
                    (SELECT A.TASK_NAME,
                        A.ID,
                        A.FIRST_TASK,
                        B.TEMPLATE_DATA_FROM,
                        B.TEMPLATE_DATA_TO
                    FROM Z_WFTEMPLATE_DATA A
                    INNER JOIN Z_WFTEMPLATE_DATA_NEXT B
                    ON A.ID           =B.TEMPLATE_DATA_FROM
                    WHERE TEMPLATE_ID =" . $tid . "
                    )
                    START WITH FIRST_TASK             =1
                    CONNECT BY PRIOR TEMPLATE_DATA_TO =ID";

        $var = array();
        $item = $this->_db->fetchAll($sql1);
        foreach ($item as $key => $y) {
            $var[$key] = $y;
            $var[$key]['finish'] = NULL;

            if ($var[$key]['ID'] == $curid) {

                $var[$key]['finish'] = $key;
            }
        }

        foreach ($var as $key => $y) {
            $varx[$key] = $y;
            if ($varx[$key]['finish'] == $curid) {

                $varx[$key]['finish'] = $key;
            }
        }


        return $var;
    }

    public function getstarted($tid) {
        $select = $this->select('Z_WFTEMPLATE_DATA');
        $select->where("TEMPLATE_ID = ? AND FIRST_TASK=1", $tid);
        //  die($select);
        $items = $this->fetchRow($select);

        return $items;
    }

    public function updatewf($data) {
	$data['UID']=1;
        $rowUser = $this->find($data['ID'])->current();
        if ($rowUser) {
            $rowUser->TASK_NAME = $data['TASKNAME'];
            $rowUser->REMINDER_INTERVAL = $data['SPACE'];
            $rowUser->POST_NOTIFY_MESSAGE = $data['MESSNOTIF'];
            $rowUser->POST_NOTIFY_SUBJECT = $data['SUBNOTIF'];
            $rowUser->REMINDER_SUBJECT = $data['SUBALERT'];
            $rowUser->REMINDER_MESSAGE = $data['MESSALERT'];
            $rowUser->SEND_INTERVAL = $data['INTERVAL'];
			$rowUser->save();
            
            if($data[ZID]==""){
				$this->_db->query("INSERT INTO Z_WFTEMPLATE_ASSIGNMENT (ID, TEMPLATE_DATA_ID, ASSIGN_TYPE, ASSIGN_BY, ASSIGN_ID) VALUES (Z_WFTEMPLATE_ASSIGN_ID_SEQ.NEXTVAL, ".$data['ID'].",2, ".$data['UID'].", ".$data['PIC'].")");
			} else {
				$this->_db->query("UPDATE Z_WFTEMPLATE_ASSIGNMENT SET ASSIGN_ID=".$data['PIC']." WHERE ID=".$data['ZID']."");
				
			}
            
            return $rowUser;
        } else {
            throw new Zend_Exception("data update failed.  User not found!");
        }
    }

}
