<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class Model_Zwftemplatedatanext extends Zend_Db_Table_Abstract {

    protected $_name = 'Z_WFTEMPLATE_DATA_NEXT';
    protected $_primary = 'ID';
    protected $_sequence = 'Z_WFTEMPLATE_DATA_NEXT_ID_SEQ';

    public function createdata($data) {


        // print_r($data); die();
        $rowUser = $this->createRow();

        if ($rowUser) {
            $rowUser->TEMPLATE_DATA_FROM = $data['TEMPLATE_DATA_FROM'];
            $rowUser->TEMPLATE_DATA_TO = $data['TEMPLATE_DATA_TO'];
            $rowUser->TEMPLATE_DATA_TO_FALSE = $data['TEMPLATE_DATA_TO_FALSE'];


            $rowUser->save();
// print_r($rowUser->ID); die("as");
            return $rowUser;
        } else {
            return 0;
        }
    }

    public function fetchdatafrom($id) {
        $select = $this->select();
        $select->where("TEMPLATE_DATA_FROM = ?", $id);
        $items = $this->fetchAll($select);
        return $items;
    }

    public function fetchdatafromarr($id) {
        $select = $this->select();
        $select->where("TEMPLATE_DATA_FROM = ?", $id);
        $items = $this->fetchRow($select)->toArray();
        return $items;
    }

	public function fetchdata_to_arr($id) {
        $select = $this->select();
        $select->where("TEMPLATE_DATA_TO = ?", $id);
        $items = $this->fetchRow($select)->toArray();
        return $items;
    }

    public function deletedata($id) {
        $rowUser = $this->find($id)->current();
        if ($rowUser) {
            $rowUser->delete();

            return 1;
        } else {
            return 0;
        }
    }

    public function del1($id1, $id2, $id3) {
        $table = new self();
        $where = $table->getAdapter()->quoteInto('TEMPLATE_DATA_TO = ? OR TEMPLATE_DATA_TO_FALSE = ? OR TEMPLATE_DATA_FROM = ?', $id1, $id2, $id3);

        $del = $table->delete($where);

        return $del;
    }

    public function fetchallbyids($taskid, $postid) {
        //die($postid);
        $select = $this->select();
        $select->where("((TEMPLATE_DATA_FROM = $taskid )  AND( (TEMPLATE_DATA_TO = $postid) OR (TEMPLATE_DATA_TO_FALSE = $postid) ))   OR( (TEMPLATE_DATA_FROM = $postid ) AND ( (TEMPLATE_DATA_TO = $taskid) OR (TEMPLATE_DATA_TO_FALSE = $taskid)))");
        // die($select);
        $items = $this->fetchAll($select);

        //Zend_Debug::dump($items);die();
        if ($items->count() > 0) {
            return $items;
        } else {
            return null;
        }
    }

}
