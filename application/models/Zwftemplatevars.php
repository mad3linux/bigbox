<?php


/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class Model_Zwftemplatevars extends Zend_Db_Table_Abstract {
    protected $_name = 'Z_WFTEMPLATE_VARIABLES';
    protected $_primary = 'ID';
    protected $_sequence = 'Z_WFTEMPLATE_VARIABLES_ID_SEQ';

    public function fetchdatabytid($id) {
        $select = $this->select();
        $select->where("TEMPLATE_ID = ?", $id);
        $items = $this->fetchAll($select);
        if($items->count()> 0) {
            return $items;
        } else {
            return null;
        }
    }

    public function fetchdatabyid($id) {
        $select = $this->select();
        $select->where("ID = ?", $id);
        $items = $this->fetchRow($select);
        return $items;
    }

    public function createdata($data) {
        $rowUser = $this->createRow();
        if($rowUser) {
            $rowUser->TEMPLATE_ID = $data['TEMPLATE_ID'];
            $rowUser->TASK_NAME = $data['TASK_NAME'];
            $rowUser->TASK_CLASS_NAME = $data['TASK_CLASS_NAME'];
            $rowUser->IS_INTERACTIVE = $data['IS_INTERACTIVE'];
            $rowUser->TASK_CLASS_NAME = $data['TASK_CLASS_NAME'];
            $rowUser->IS_INTERACTIVE = $data['IS_INTERACTIVE'];
            if($data['IS_INTERACTIVE']) {
                //$rowUser->ASSIGNED_BY_VARIABLE= 1;
                $rowUser->SHOW_IN_DETAIL = 1;
            }
            $rowUser->OFFSET_LEFT = $data['OFFSET_LEFT'];
            $rowUser->OFFSET_TOP = $data['OFFSET_TOP'];
            $rowUser->FIRST_TASK = 0;
            $rowUser->save();
            return $rowUser;
        } else {
            return 0;
        }
    }

    public function deletedata($id) {
        $rowUser = $this->find($id)->current();
        if($rowUser) {
            $rowUser->delete();
            return 1;
        } else {
            return 0;
        }
    }

    public function deltbytid($id) {
        $table = new self();
        $where = $table->getAdapter()->quoteInto('TEMPLATE_ID = ?', $id);
        $del = $table->delete($where);
        return $del;
    }
}
