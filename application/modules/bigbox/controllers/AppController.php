<?php
/*
 * Copyright (C) bigbox, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by bacanghaneut <rohimfikri@gmail.com>,
*/
// ini_set("display_errors", "on");
class Bigbox_AppController extends Zend_Controller_Action{

public function init() {
	// die();
}

public function systemAction(){
	$params = $this->getRequest()->getParams();
	// Zend_Debug::dump($params);die();
	$app = array("home","servers");
	if(!in_array($params['menu'],$app)){
		$this->_redirect("/bigbox");
	}
	try {
		$authAdapter = Zend_Auth::getInstance();
		$identity = $authAdapter->getIdentity();
	} catch (Exception $e) {
		
	}
	$this->view->params = $params;
	$this->view->identity = $identity;
	// Zend_Debug::dump($identity);die();
	$this->view->headScript()->appendScript("
	jQuery(document).ready(function(){
		var ctn = $('.m-grid__item.m-grid__item--fluid.m-wrapper');
		var h = ctn.innerHeight();
		$('iframe').css('height',h-10);
	});
	");
	$this->renderScript ( 'app/'.$params['action'].'/'.$params['menu'].'.phtml' );
}

public function yarnAction(){
	$params = $this->getRequest()->getParams();
	// Zend_Debug::dump($params);die();
	$app = array("home","applications","mrjobhistoryserver","llapdaemon","nodemanagers","queues","resourcemanager","timelineserver");
	if(!in_array($params['menu'],$app)){
		$this->_redirect("/bigbox");
	}
	try {
		$authAdapter = Zend_Auth::getInstance();
		$identity = $authAdapter->getIdentity();
	} catch (Exception $e) {
		
	}
	$this->view->params = $params;
	$this->view->identity = $identity;
	// Zend_Debug::dump($identity);die();
	$this->view->headScript()->appendScript("
	var windowWidth = window.innerWidth;
	var windowHeight = window.innerHeight;
	jQuery(document).ready(function(){
		var ctn = $('.m-grid__item.m-grid__item--fluid.m-wrapper');
		var h = ctn.innerHeight();
		$('iframe').css('height',h-10);
	});
	");
	$this->renderScript ( 'app/'.$params['action'].'/'.$params['menu'].'.phtml' );
}

public function hiveAction(){
	$params = $this->getRequest()->getParams();
	// Zend_Debug::dump($params);die();
	$app = array("home","hivemetastore","hiveserver2","llapdaemon","llapheatmaps","llapoverview");
	if(!in_array($params['menu'],$app)){
		$this->_redirect("/bigbox");
	}
	try {
		$authAdapter = Zend_Auth::getInstance();
		$identity = $authAdapter->getIdentity();
	} catch (Exception $e) {
		
	}
	$this->view->params = $params;
	$this->view->identity = $identity;
	// Zend_Debug::dump($identity);die();
	$this->view->headScript()->appendScript("
	jQuery(document).ready(function(){
		var ctn = $('.m-grid__item.m-grid__item--fluid.m-wrapper');
		var h = ctn.innerHeight();
		$('iframe').css('height',h-10);
	});
	");
	$this->renderScript ( 'app/'.$params['action'].'/'.$params['menu'].'.phtml' );
}

public function hdfsAction(){
	$params = $this->getRequest()->getParams();
	// Zend_Debug::dump($params);die();
	$app = array("home","datanodes","namenodes","topn","users");
	if(!in_array($params['menu'],$app)){
		$this->_redirect("/bigbox");
	}
	try {
		$authAdapter = Zend_Auth::getInstance();
		$identity = $authAdapter->getIdentity();
	} catch (Exception $e) {
		
	}
	$this->view->params = $params;
	$this->view->identity = $identity;
	// Zend_Debug::dump($identity);die();
	$this->view->headScript()->appendScript("
	jQuery(document).ready(function(){
		var ctn = $('.m-grid__item.m-grid__item--fluid.m-wrapper');
		var h = ctn.innerHeight();
		$('iframe').css('height',h-10);
		
		// var frame = $('#iframe');
		// frame.load(function () {
			// frame.contents().find('body').css('font-family', 'Poppins');
		// });
	});
	");
	$this->renderScript ( 'app/'.$params['action'].'/'.$params['menu'].'.phtml' );
}

public function hbaseAction(){
	$params = $this->getRequest()->getParams();
	// Zend_Debug::dump($params);die();
	$app = array("home","misc","regionservers","tables","users");
	if(!in_array($params['menu'],$app)){
		$this->_redirect("/bigbox");
	}
	try {
		$authAdapter = Zend_Auth::getInstance();
		$identity = $authAdapter->getIdentity();
	} catch (Exception $e) {
		
	}
	$this->view->params = $params;
	$this->view->identity = $identity;
	// Zend_Debug::dump($identity);die();
	$this->view->headScript()->appendScript("
	jQuery(document).ready(function(){
		var ctn = $('.m-grid__item.m-grid__item--fluid.m-wrapper');
		var h = ctn.innerHeight();
		$('iframe').css('height',h-10);
	});
	");
	$this->renderScript ( 'app/'.$params['action'].'/'.$params['menu'].'.phtml' );
}

// action
public function externaleventAction(){
    $this->view->headLink()->appendStylesheet('/assets/m51/dist/default/assets/vendors/custom/jquery-ui/jquery-ui.bundle.css');
    $this->view->headLink()->appendStylesheet('/assets/m51/dist/default/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css');
	
	$this->view->headScript()->appendFile('/assets/m51/dist/default/assets/vendors/custom/jquery-ui/jquery-ui.bundle.js');
	$this->view->headScript()->appendFile('/assets/m51/dist/default/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js');
	$this->view->headScript()->appendFile('/assets/m51/dist/default/assets/demo/default/custom/components/calendar/external-events.js');
}
public function listviewAction(){
    $this->view->headLink()->appendStylesheet('/assets/m51/dist/default/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css');

	$this->view->headScript()->appendFile('/assets/m51/dist/default/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js');
	$this->view->headScript()->appendFile('/assets/m51/dist/default/assets/demo/default/custom/components/calendar/list-view.js');

}

}