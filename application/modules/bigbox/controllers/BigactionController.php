<?php
/*
 * Copyright (C) bigbox, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by bacanghaneut <rohimfikri@gmail.com>,
*/
class Bigbox_BigactionController extends Zend_Controller_Action{

    public function init() {
    	
    }

    public function indexAction(){
        
    }

public function grafanaAction(){
    $params = $this->getRequest()->getParams();
    // Zend_Debug::dump($params);die();

    $val2 = end($params); //Zend_Debug::dump($val2);
    $val1 = array_search($val2, $params); //Zend_Debug::dump($val1);
    // die();

    $iframeUrl = "http://jt-hdp02i0403.telkom.co.id:3000/dashboard/db/".$val1."-".$val2."?theme=light";

    try {
        $authAdapter = Zend_Auth::getInstance();
        $identity = $authAdapter->getIdentity();
    } catch (Exception $e) {
        
    }
    $this->view->params = $params;
    $this->view->iframeUrl = $iframeUrl;
    $this->view->identity = $identity;
    // Zend_Debug::dump($identity);die();
    $this->view->headScript()->appendScript("
    jQuery(document).ready(function(){
        var ctn = $('.m-grid__item.m-grid__item--fluid.m-wrapper');
        var h = ctn.innerHeight();
        $('iframe').css('height',h-30);
    });
    ");
    $this->renderScript ( 'grafana/index.phtml' );
}

    public function treefolderAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/fuelux/js/tree.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $cc = new Model_Zprabagensys();
        $cc->get_nested_model();
        //$this->view->headScript ()->appendFile ( '/assets/core/js/d3.v3.min.js' );
    }

    public function frontAction() {
        $this->view->headScript()->appendFile('/assets/core/js/d3.v3.min.js');
    }

    public function frontajaxAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        $this->view->varams = $params;
    }

    public function addappAction() {
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        // <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        // $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
		$this->view->headScript()->appendScript('
		$( "#addconn" ).validate({
            // define validation rules
            rules: {
                short: {
                    required: true 
                },
				long: {
                    required: true 
                }
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                var alert = $("#alertinfo");
                alert.removeClass("m--hide").show();
                mApp.scrollTo(alert, -200);
            },

            submitHandler: function (form) {
                //form[0].submit(); // submit the form
				$("#processbar").show();
            }
        });  
		');
		
        $params = $this->getRequest()->getParams();
        $sys = new Model_Prabasystem();
        $page = new Model_Zprabapage();
        $userapp = $page->list_users_api();
        // Zend_Debug::dump($userapp); die();
        $params = $sys->get_params('api_type');
        $modes = $sys->get_params('api_mode');
        // Zend_Debug::dump($modes); die();
        $conns = $sys->get_conn();
        $this->view->modes = $modes;
        $this->view->params = $params;
        $this->view->conns = $conns;
        $this->view->userapp = $userapp;
    }
	
	public function listpagejAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $this->_helper->layout->disableLayout();

        $mdl_admin = new Model_Zprabapage ();
        $countgraph = $mdl_admin->count_listpage($_GET);

        $listgraph = $mdl_admin->listpage_ajax($_GET);
        // Zend_Debug::dump($listgraph);die();
        $iTotalRecords = intval($countgraph);
        $iDisplayLength = intval($_GET ['iDisplayLength']);
        $pageSize = intval($_GET ['pageSize']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_GET ['iDisplayStart']);
        $sEcho = intval($_GET ['sEcho']);

        $records = array();
		// $records ["meta"] = array();
        //$records ["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;

        $mdl = new Model_Prabasystem ();
        $apps = $mdl->get_apps();
		// $records ["meta"] = array(
			// "page"=> 1,
			// "pages"=> 1,
			// "perpage"=> 10,
			// "total"=> $iTotalRecords,
			// "sort"=> "asc",
			// "field"=> "No",
		// );
        foreach ($listgraph as $k => $v) {
            $records[] = array(
				'No' => $iDisplayStart + $k + 1,
				'id' =>  $v ['id'],
                'name_alias' =>$v ['name_alias'],
                'layout_id' =>$v ['layout_id'],
                'workspace_id' =>$v ['workspace_id'],
                'app_id' =>$apps [$v ['app_id']],
                'title' =>$v ['title']
            );
        }

        // $records ["sEcho"] = $sEcho;
        // $records ["iTotalRecords"] = $iTotalRecords;
        // $records ["iTotalDisplayRecords"] = $iTotalRecords;

        /*
         * $gg = new Escort_Model_Kpu(); $res = $gg->get_datakpu($data2['witel']); //var_dump($res);die();
         */
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }
	
	public function listactiondAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $this->_helper->layout->disableLayout();

        $mdl_admin = new Model_Zprabapage ();
        $countgraph = $mdl_admin->count_listactionbigaction($_GET, $params['wf']);

        $listgraph = $mdl_admin->listaction_ajaxbigaction($_GET, $params['wf']);
        // Zend_Debug::dump($listgraph);die();
        $iTotalRecords = intval($countgraph);
		
        $getdatatable	= $_GET['datatable'];
		$page			= round($getdatatable['pagination']['page']);
		$perpages		= round($getdatatable['pagination']['perpage']);
		$pages			= ceil($iTotalRecords/$perpages);
		
		$iDisplayLength = intval($perpages);
        //$pageSize = intval($_GET ['pageSize']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval(($page-1)*$perpages);
        //$sEcho = intval($_GET ['sEcho']);

        $records = array();
		$records ["meta"] = array();

		$end = $iDisplayStart + $iDisplayLength;
		$records ["meta"] = array(
			"page"=> $page,
			"pages"=> $pages,
			"perpage"=> $perpages,
			"total"=> $iTotalRecords,
			"sort"=> "asc",
			"field"=> "No",
		);

        $mdl = new Model_Prabasystem ();
        $apps = $mdl->get_apps();
		
		if ($params['wf'] == 1) {
			foreach ($listgraph as $k => $v) {
				$records['data'][] = array(
					'No' => $iDisplayStart + $k + 1,
					'id' =>  $v ['id'],
					'action_name' => $v ['action_name'],
					'action' =>  '
							<a href="/mms/prabadoc/launch/id/' . $v ['id'] . '" data-id="' . $v ['id'] . '" class="m-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon m-btn--icon-only m-btn--pill" title="Edit Action">
								<i class="la la-search"></i>
							</a>
							<a href="/mms/prabadoc/builderform/id/' . $v ['id'] . '" data-id="' . $v ['id'] . '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="View Action">
								<i class="la la-edit"></i>
							</a>' . '
							<a href="/prabadata/action/display/id/' . $v ['id'] . '?wf=1" data-id="' . $v ['id'] . '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Delete Api"> 
								<i class="la la-search"></i>
							</a>' . '
							<a href="javascript:;" data-id="' . $v ['id'] . '"  class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Clone">
								<i class="la la-clone"></i>
							</a>'
				);
			}
		} else {
			foreach ($listgraph as $k => $v) {
				$records['data'][] = array(
					'No' => $iDisplayStart + $k + 1,
					'id' =>  $v ['id'],
					'action_name' => $v ['action_name'],
					'action' => '
						<a href="/bigbox/bigaction/editaction/id/' . $v ['id'] . '" data-id="' . $v ['id'] . '" class="m-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon m-btn--icon-only m-btn--pill" title="Edit Action">
							<i class="la la-edit"></i>
						</a>' . '
						<a href="/prabawf/display/id/' . $v ['id'] . '" data-id="' . $v ['id'] . '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="View Action">
							<i class="la la-search"></i>
						</a>' . '
						<a href="javascript:;" data-id="' . $v ['id'] . '"  class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btn-delete" title="Delete Api">
							<i class="la la-trash"></i>
						</a>
						<a href="javascript:;" data-id="' . $v ['id'] . '"  class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" title="Clone">
							<i class="la la-clone"></i>
						</a>'
				);
			}
		}

        // $records ["sEcho"] = $sEcho;
        // $records ["iTotalRecords"] = $iTotalRecords;
        // $records ["iTotalDisplayRecords"] = $iTotalRecords;

        /*
         * $gg = new Escort_Model_Kpu(); $res = $gg->get_datakpu($data2['witel']); //var_dump($res);die();
         */
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }
	
	public function listportletjAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $this->_helper->layout->disableLayout();

        $mdl_admin = new Model_Zprabapage ();
        $countgraph = $mdl_admin->count_listportlet($_GET);

        $listgraph = $mdl_admin->listportlet_ajax($_GET);
        // Zend_Debug::dump($listgraph);die();
        $iTotalRecords = intval($countgraph);
        $iDisplayLength = intval($_GET ['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_GET ['iDisplayStart']);
        $sEcho = intval($_GET ['sEcho']);

        $records = array();
		//$records ["meta"] = array();
       

        $end = $iDisplayStart + $iDisplayLength;
		// $records ["meta"] = array(
			// "page"=> 1,
			// "pages"=> 1,
			// "perpages"=> 10,
			// "total"=> $iTotalRecords,
			// "sort"=> "asc",
			// "field"=> "No",
		// );
        $mdl = new Model_Prabasystem ();
        // $apps = $mdl->get_apps();
        foreach ($listgraph as $k => $v) {
            $records[]  = array(
                'No' => $iDisplayStart + $k + 1,
				'id' => $v ['id'],
                'portlet_name' => $v ['portlet_name'],
                'portlet_title' => $v ['portlet_title'],
                'portlet_type' => $v ['portlet_type'],
                'custom_view' => $v ['custom_view']
            );
        }

        // $records ["sEcho"] = $sEcho;
        // $records ["iTotalRecords"] = $iTotalRecords;
        // $records ["iTotalDisplayRecords"] = $iTotalRecords;

        /*
         * $gg = new Escort_Model_Kpu(); $res = $gg->get_datakpu($data2['witel']); //var_dump($res);die();
         */
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }

    public function addapiAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        // <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $params = $this->getRequest()->getParams();
        $sys = new Model_Prabasystem();
        $page = new Model_Zprabapage();
        $userapp = $page->list_users_api();
        // Zend_Debug::dump($userapp); die();
        $params = $sys->get_params('api_type');
        $modes = $sys->get_params('api_mode');
        // Zend_Debug::dump($modes); die();
        $conns = $sys->get_conn();
        $this->view->modes = $modes;
        $this->view->params = $params;
        $this->view->conns = $conns;
        $this->view->userapp = $userapp;
    }

    public function websocketAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        $dd = new Model_Prabasystemaddon();
        if(isset($params['act'])) {
            $data = $m2->get_portlet($params['id']);
            // Zend_Debug::dump($data); die();
        }
        $apis = $dd->get_api_grouping();
        if(!isset($params['row'])) {
            $params['row'] = 1;
        }
        // Zend_Debug::dump($apis); die();
        $this->view->lay = $m->get_layout();
        $this->view->apis = $apis;
        $this->view->data = $data;
        $this->view->apps = $m->get_apps();
        $this->view->ws = $m->get_workspace();
        $this->view->varams = $params;
        $this->view->classicon = $m->get_params('classicon');
        $this->view->area = $m->get_params('formarea');
    }

    public function builderhtmlAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        $dd = new Model_Prabasystemaddon();
        if(isset($params['act'])) {
            $data = $m2->get_portlet($params['id']);
            // Zend_Debug::dump($data); die();
        }
        $apis = $dd->get_api_grouping();
        if(!isset($params['row'])) {
            $params['row'] = 1;
        }
        // Zend_Debug::dump($apis); die();
        $this->view->lay = $m->get_layout();
        $this->view->apis = $apis;
        $this->view->data = $data;
        $this->view->apps = $m->get_apps();
        $this->view->ws = $m->get_workspace();
        $this->view->varams = $params;
        $this->view->classicon = $m->get_params('classicon');
        $this->view->area = $m->get_params('formarea');
    }

    public function buildergraphicAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        $dd = new Model_Prabasystemaddon();
        $apis = $dd->get_api_grouping();
        if(!isset($params['row'])) {
            $params['row'] = 1;
        }
        if(isset($params['act'])) {
            $data = $m2->get_portlet($params['id']);
            // Zend_Debug::dump($data); die();
        }
        // Zend_Debug::dump($apis); die();
        $this->view->lay = $m->get_layout();
        $this->view->data = $data;
        $this->view->apis = $apis;
        $this->view->apps = $m->get_apps();
        $this->view->ws = $m->get_workspace();
        $this->view->varams = $params;
        // $this->view->classicon = $m->get_params('classicon');
        // $this->view->area = $m->get_params('formarea');
    }

    public function buildergraphicplusAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-colorpicker/css/colorpicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/fuelux/js/spinner.min.js');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        $dd = new Model_Prabasystemaddon();
        $apis = $dd->get_api_grouping();
        $par = new Model_Zparams();
        if(!isset($params['row'])) {
            $params['row'] = 1;
        }
        if(isset($params['act'])) {
            $data = $m2->get_portlet($params['id']);
            // Zend_Debug::dump($data); die();
        }
        // Zend_Debug::dump($apis); die();
        $this->view->lay = $m->get_layout();
        $this->view->data = $data;
        $this->view->apis = $apis;
        $this->view->apps = $m->get_apps();
        $this->view->ws = $m->get_workspace();
        $this->view->varams = $params;
        $this->view->chartType = $par->get_chart_type();
        $this->view->chartTheme = $par->get_chart_theme();
        // $this->view->classicon = $m->get_params('classicon');
        // $this->view->area = $m->get_params('formarea');
    }

    public function builderformAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        $dd = new Model_Prabasystemaddon();
        $apis = $dd->get_api_grouping();
        if(!isset($params['row'])) {
            $params['row'] = 1;
        }
        $data = array();
        if(isset($params['act'])) {
            $data = $m2->get_portlet($params['id']);
        }
        $this->view->lay = $m->get_layout();
        $this->view->data = $data;
        $this->view->apis = $apis;
        $this->view->apps = $m->get_apps();
        $this->view->ws = $m->get_workspace();
        $this->view->varams = $params;
        $this->view->classicon = $m->get_params('classicon');
        $this->view->area = $m->get_params('formarea');
    }

    public function buildertableAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        // $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/table-ajax.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/ui-extended-modals.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        $dd = new Model_Prabasystemaddon();
        $apis = $dd->get_api_grouping();
        $this->view->apis = $apis;
        $data = array();
        if(isset($params['act'])) {
            $data = $m2->get_portlet($params['id']);
        }
        // Zend_Debug::dump($data); die();
        $apis = $m2->get_all_api();
        $this->view->data = $data;
        $this->view->apps = $m->get_apps();
        $this->view->ws = $m->get_workspace();
        // Zend_Debug::dump($m->get_apps());die();
        $this->view->varams = $params;
    }

    public function listappAction() {
		$this->view->headScript()->appendScript('
		$( "#addconn" ).validate({
            // define validation rules
            rules: {
                short: {
                    required: true 
                },
				long: {
                    required: true 
                }
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                var alert = $("#m_form_1_msg");
                alert.removeClass("m--hide").show();
                mApp.scrollTo(alert, -200);
            },

            submitHandler: function (form) {
                //form[0].submit(); // submit the form
				$("#processbar").show();
            }
        });
		
		var $modal = $("#ajax-modal");
		
		$(".ajax-demo").on("click", function(){
			var id =  $(this).attr("rel");
			
			// alert(id);
			// create the backdrop and wait for next modal to be triggered
			//  $("body").modalmanager("loading");
			
			setTimeout(function(){
				$modal.load("/bigbox/bigbuilder/frontajax/id/"+id, "", function(){
					$modal.modal();
				});
			}, 1000);
		});
		');
        $mm = new Model_Zprabapage();
        $cc = new Model_System();
        $apps = $cc->get_app_modules();
        
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $sys = new Model_Prabasystem();
        $datacon = $sys->get_applications();

        foreach($datacon as $k => $v) {
            if(in_array($v['s_app'], $apps)) {
                $datacon2[$k] = $v;
            }
        }
        $cc = new Model_System();
        $dd = new Model_Zparams();
        $themes = $cc->get_themes();
        $lays = $dd->get_layout();
        $skins = $dd->get_skins();
        $this->view->lays = $lays;
        $this->view->themes = $themes;
        $this->view->skins = $skins;
        //Zend_Debug::dump($datacon2); die();
        $this->view->conns = $datacon;
    }

    public function saveformAction() {
        $xmlData = simplexml_load_string($_POST['data']);
        $config = new Zend_Config_Xml($_POST['data']);
    }

    public function addtablection() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        // <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        $params = $this->getRequest()->getParams();
    }

    public function listconnAction() {
        $mm = new Model_Zprabapage();
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/table-ajax.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/ui-extended-modals.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $sys = new Model_Prabasystem();
        $pa = new Model_Zparams();
        $this->view->adapts = $pa->get_adapters();
        // Zend_Debug::dump($pa->get_adapters()); //die();
        $datacon = $sys->get_conns_serialize();
        // Zend_Debug::dump($datacon); die();
        $this->view->conns = $datacon;
    }

    public function builderAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/builder/css/rbl_forms.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/builder/css/ui-lightness/jquery-ui-1.7.2.custom.css');
        $this->view->headScript()->appendFile('/assets/core/builder/js/rbl_base.js');
        $this->view->headScript()->appendFile('/assets/core/builder/js/rbl_form.js');
        $this->view->headScript()->appendFile('/assets/core/builder/js/rbl_col.js');
        $this->view->headScript()->appendFile('/assets/core/builder/js/rbl_elt.js');
        // $this->view->headScript()->appendFile('/assets/core/builder/js/rbl_build.js');
        $this->view->headScript()->appendFile('/assets/core/builder/js/rbl_base.js');
        // $this->view->headScript()->appendFile('/assets/core/builder/js/rbl_base.js');
        $this->view->headScript()->appendFile('/assets/core/builder/js/lib/chili/jquery.chili-2.2.js');
    }

    public function loginappAction() {
        $params = $this->getRequest()->getParams();
		
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        // $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
		
		$this->view->headScript()->appendScript('
		$( "#m_form_1" ).validate({
            // define validation rules
            rules: {
                short: {
                    required: true 
                },
				long: {
                    required: true 
                }
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                var alert = $("#m_form_1_msg");
                alert.removeClass("m--hide").show();
                mApp.scrollTo(alert, -200);
            },

            submitHandler: function (form) {
                //form[0].submit(); // submit the form
				$("#processbar").show();
            }
        });  
		');
        
		try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $cc = new Model_System();
        $dd = new Model_Zparams();
        $themes = $dd->get_login_theme();
        $lays = $dd->get_login_position();
        $mdl_sys = new Model_Prabasystem();
        $data = $mdl_sys->get_a_app($params['id']);
        //Zend_Debug::dump($data); die();
        $this->view->params = $params;
        $this->view->lays = $lays;
        $this->view->data = $data;
        $this->view->themes = $themes;
    }

    public function editappAction() {
        $params = $this->getRequest()->getParams();
        // $this->view->headScript()->appendFile('/assets/m51/dist/default/assets/demo/default/custom/components/forms/validation/form-validation.js');
		
		$this->view->headScript()->appendScript('
		$( "#m_form_1" ).validate({
            // define validation rules
            rules: {
                short: {
                    required: true 
                },
				long: {
                    required: true 
                }
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                var alert = $("#m_form_1_msg");
                alert.removeClass("m--hide").show();
                mApp.scrollTo(alert, -200);
            },

            submitHandler: function (form) {
                //form[0].submit(); // submit the form
				$("#processbar").show();
            }
        });  
		');
		
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $cc = new Model_System();
        $dd = new Model_Zparams();
        $themes = $cc->get_themes();
        $lays = $dd->get_layout();
        $skins = $dd->get_skins();
        $mdl_sys = new Model_Prabasystem();
        $data = $mdl_sys->get_a_app($params['id']);
        // Zend_Debug::dump($data); die();
        $this->view->params = $params;
        $this->view->lays = $lays;
        $this->view->data = $data;
        $this->view->skins = $skins;
        $this->view->themes = $themes;
    }

    public function editconnAction() {
        $params = $this->getRequest()->getParams();
        if(!isset($params['id'])|| $params['id'] == '') {
            $this->_redirect('/prabaeditor/listapi');
        }
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $mdl_sys = new Model_Prabasystem();
        $data = $mdl_sys->get_a_conn($params['id']);
        $page = new Model_Zprabapage();
        $conns = $mdl_sys->get_conn();
        $pa = new Model_Zparams();
        $this->view->adapts = $pa->get_adapters();
        $this->view->modes = $modes;
        $this->view->params = $params;
        $this->view->conns = $conns;
        $this->view->data = $data;
    }

    public function getUrlAction() {
        //Zend_Debug::dump($_SERVER);
        $params = $this->getRequest()->getParams();
        $this->view->varams = $params;
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $dd = new Model_Zprabapage();
        $this->view->listu = $dd->list_users_api();
    }

    public function editapiuserAction() {
        $params = $this->getRequest()->getParams();
        if(!isset($params['id'])|| $params['id'] == '') {
            $this->_redirect('/prabaeditor/listapi');
        }
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $mdl_sys = new Model_Prabasystem();
        $data = $mdl_sys->get_a_api_user($params['id']);
        $page = new Model_Zprabapage();
        // Zend_Debug::dump($data); die();
        $this->view->data = $data;
    }

    public function editapiAction() {
        $params = $this->getRequest()->getParams();
        if(!isset($params['id'])|| $params['id'] == '') {
            $this->_redirect('/prabaeditor/listapi');
        }
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $mdl_sys = new Model_Prabasystem();
        $data = $mdl_sys->get_a_api($params['id']);
        $page = new Model_Zprabapage();
        $userapp = $page->list_users_api();
        // Zend_Debug::dump($data); die();
        $modes = $mdl_sys->get_params('api_mode');
        $params = $mdl_sys->get_params('api_type');
        $conns = $mdl_sys->get_conn();
        $this->view->modes = $modes;
        $this->view->params = $params;
        $this->view->conns = $conns;
        $this->view->data = $data;
        $this->view->userapp = $userapp;
    }

    public function listapiuserAction() {
        // $mm = new Model_Prabasystem();
        // Zend_Debug::dump($mm->get_models_class()); die();
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        // $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/table-ajax.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/ui-extended-modals.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $sys = new Model_Prabasystem();
        $page = new Model_Zprabapage();
        $userapp = $page->list_users_api();
        // Zend_Debug::dump($userapp); die();
        $params = $sys->get_params('api_type');
        $modes = $sys->get_params('api_mode');
        // Zend_Debug::dump($modes); die();
        $conns = $sys->get_conn();
        $this->view->modes = $modes;
        $this->view->params = $params;
        $this->view->conns = $conns;
        $this->view->userapp = $userapp;
    }

    public function listapiAction() {
        // $mm = new Model_Prabasystem();
        // Zend_Debug::dump($mm->get_models_class()); die();
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        // $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/table-ajax.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/ui-extended-modals.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $sys = new Model_Prabasystem();
        $page = new Model_Zprabapage();
        $userapp = $page->list_users_api();
        // Zend_Debug::dump($userapp); die();
        $params = $sys->get_params('api_type');
        $modes = $sys->get_params('api_mode');
        // Zend_Debug::dump($modes); die();
        $conns = $sys->get_conn();
        $this->view->modes = $modes;
        $this->view->params = $params;
        $this->view->conns = $conns;
        $this->view->userapp = $userapp;
    }

    public function editpageAction() {
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
		
		$this->view->headScript()->appendFile('/assets/m51/dist/default/assets/demo/default/custom/components/forms/widgets/select2custom.js');
		
		$this->view->headScript()->appendScript('
		$("#jml").on("change", function (e) {
		   var value = $( this ).val();
		  // alert(value);
			 window.location.href = "/bigbox/bigbuilder/addpage/row/"+value;
		
		});
		
		$( "#addapi" ).validate({
            // define validation rules
            rules: {
                pagealias: {
                    required: true 
                },
				title: {
                    required: true 
                }
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                var alert = $("#alertinfo");
                alert.removeClass("m--hide").show();
                mApp.scrollTo(alert, -200);
            },

            submitHandler: function (form) {
                //form[0].submit(); // submit the form
				$("#processbar").show();
            }
        });
		');
        
		$params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        $data = $m2->get_page($params['id']);
        $data['id'] = $params['id'];
        
		//Zend_Debug::dump($data);die("ss");
        $arel = unserialize($data['element']);
        if(!isset($params['row'])&& count($arel)> 1) {
            $params['row'] = count($arel);
        } elseif(!isset($params['row'])) {
            $params['row'] = 1;
        }
        $this->view->apps = $m->get_apps();
        $this->view->ws = $m->get_workspace();
        $this->view->lay = $m->get_layout();
        // Zend_Debug::dump($m->get_layout());die();
        $this->view->varams = $params;
        $this->view->data = $data;
    }
	
	public function listcronAction() {
		$this->view->headScript()->appendFile('/assets/bigbox/js/bigaction_listcron.js');
		
        $auth = Zend_Auth::getInstance();
        $identity = $auth->getIdentity();
        $params = $this->getRequest()->getParams();
        $callert = new Model_Zprabacrons ();
        $allert = $callert->get_all_allerts_v1();
        if ($params ['act'] == 'del') {
            $callert->deleteallert($params ['ID']);
            $this->_redirect('/pm/parentwf/listallerts');
        }
        $this->view->data = $allert;
        $this->view->cat = $cat;
    }

    public function editactionAction() {
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
		// $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
		$this->view->headScript()->appendScript('
		$( "#addapi" ).validate({
            // define validation rules
            rules: {
                conn: {
                    required: true 
                }
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                var alert = $("#m_form_1_msg");
                alert.removeClass("m--hide").show();
				$("html,body").scrollTop(0);
            },

            submitHandler: function (form) {
                //form[0].submit(); // submit the form
				var proses = $("#processbar");
				$("#m_form_1_msg").addClass("m--hide");
				$("#processbar").removeClass("m--hide");
				$("html,body").scrollTop(0);
				$("#m_form_1_msg").removeClass("alert-success");
				$("#m_form_1_msg").removeClass("alert-warning");
				$("#m_form_1_msg").removeClass("alert-danger");
				$("#m_form_1_msg").removeClass("alert-info");
				submitForm();
				return false;
            }
        });
		var submitForm = function(){
		//console.log($("form#addapi").serialize());
		$.ajax({
			url:"/prabajax/addaction",
			content:"json",
			type:"post",
			data:$("form#addapi").serialize(),
			beforeSend:function(){
				$("#m_form_1_msg").addClass("m--hide");
				$("#processbar").removeClass("m--hide");
				$("html,body").scrollTop(0);
				$("#m_form_1_msg").removeClass("alert-success");
				$("#m_form_1_msg").removeClass("alert-warning");
				$("#m_form_1_msg").removeClass("alert-danger");
				$("#m_form_1_msg").removeClass("alert-info");
			},
			complete:function(result){
				$("#processbar").addClass("m--hide");
			},
			success: function(data){
				//console.log(data);
				if(data.result!=undefined){
					if(data.result==true){
						$("#m_form_1_msg").removeClass("m--hide");
						$("#m_form_1_msg").addClass("alert-success");
						$("#alertmsg").html("<strong>Success!</strong> "+data.retMsg);
						$("#static2").scrollTop(0);
						$("#reload").click();
						document.getElementById("addapi").reset();
						//resetForm();
						window.location.replace("/prabawf/display/id/"+data.data.id);
					}else{
						$("#m_form_1_msg").addClass("alert-warning");
						$("#alertmsg").html("<strong>Failed!</strong> "+data.retMsg);
					}
				}else{
					$("#m_form_1_msg").addClass("alert-info");
					$("#alertmsg").html("<strong>Error!</strong> System is busy, please try again.");
				}
				
				
			},
			error:function(){
				$("#m_form_1_msg").addClass("alert-info");
				$("#alertmsg").html("<strong>Error!</strong> System is busy, please try again.");
				$("#m_form_1_msg").removeClass("m--hide");
			}
		});
		}
		');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $this->view->data = $m->get_a_action($params['id']);
    }

    public function socketAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        // <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $params = $this->getRequest()->getParams();
        $mdl_sys = new Model_Prabasystem();
        $data = $mdl_sys->get_a_api($params['id']);
        //Zend_Debug::dump($data); 
        $this->view->varams = $params;
        $this->view->data = $data;
    }

    public function addactionAction() {
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
		
		$this->view->headScript()->appendScript('
		$(".cancel").click(function(){
			window.history.back();
			return false;
		}); 
		$( "#addapi" ).validate({
            // define validation rules
            rules: {
                action_name: {
                    required: true 
                },
				act_type: {
                    required: true 
                }
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                var alert = $("#alertinfo");
                alert.removeClass("m--hide").show();
                mApp.scrollTo(alert, -200);
            },

            submitHandler: function (form) {
                //form[0].submit(); // submit the form
				var proses = $("#processbar");
				proses.show();
				$("#m_form_1_msg").addClass("m--hide");
				$("#processbar").removeClass("m--hide");
				$("html,body").scrollTop(0);
				$("#m_form_1_msg").removeClass("alert-success");
				$("#m_form_1_msg").removeClass("alert-warning");
				$("#m_form_1_msg").removeClass("alert-danger");
				$("#m_form_1_msg").removeClass("alert-info");
				submitForm();
				return false;
            }
			
        });
		var submitForm = function(){
		//console.log($("form#addapi").serialize());
		$.ajax({
			url:"/prabajax/addaction",
			content:"json",
			type:"post",
			data:$("form#addapi").serialize(),
			beforeSend:function(){
				$("#m_form_1_msg").addClass("m--hide");
				$("#processbar").removeClass("m--hide");
				$("html,body").scrollTop(0);
				$("#m_form_1_msg").removeClass("alert-success");
				$("#m_form_1_msg").removeClass("alert-warning");
				$("#m_form_1_msg").removeClass("alert-danger");
				$("#m_form_1_msg").removeClass("alert-info");
			},
			complete:function(result){
				$("#processbar").addClass("m--hide");
			},
			success: function(data){
				//console.log(data);
				if(data.result!=undefined){
					if(data.result==true){
						$("#m_form_1_msg").removeClass("m--hide");
						$("#m_form_1_msg").addClass("alert-success");
						$("#alertmsg").html("<strong>Success!</strong> "+data.retMsg);
						$("#static2").scrollTop(0);
						$("#reload").click();
						document.getElementById("addapi").reset();
						//resetForm();
						window.location.replace("/prabawf/display/id/"+data.data.id);
					}else{
						$("#m_form_1_msg").addClass("alert-warning");
						$("#alertmsg").html("<strong>Failed!</strong> "+data.retMsg);
					}
				}else{
					$("#m_form_1_msg").addClass("alert-info");
					$("#alertmsg").html("<strong>Error!</strong> System is busy, please try again.");
				}
				
				
			},
			error:function(){
				$("#m_form_1_msg").addClass("alert-info");
				$("#alertmsg").html("<strong>Error!</strong> System is busy, please try again.");
				$("#m_form_1_msg").removeClass("m--hide");
			}
		});
		}
		');
		
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        // Zend_Debug::dump($m2->list_page());die();
        if(!isset($params['row'])) {
            $params['row'] = 1;
        }
        $this->view->lay = $m->get_layout();
    }

    public function addpageAction() {
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
		
        $this->view->headScript()->appendFile('/assets/m51/dist/default/assets/demo/default/custom/components/forms/widgets/select2custom.js');
		
		$this->view->headScript()->appendScript('
		$("#jml").on("change", function (e) {
		   var value = $( this ).val();
		  // alert(value);
			 window.location.href = "/bigbox/bigbuilder/addpage/row/"+value;
		
		});
		
		$( "#addapi" ).validate({
            // define validation rules
            rules: {
                pagealias: {
                    required: true 
                },
				title: {
                    required: true 
                }
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                var alert = $("#alertinfo");
                alert.removeClass("m--hide").show();
                mApp.scrollTo(alert, -200);
            },

            submitHandler: function (form) {
                //form[0].submit(); // submit the form
				$("#processbar").show();
            }
        });
		');
		
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        // Zend_Debug::dump($m2->list_page());die();
        if(!isset($params['row'])) {
            $params['row'] = 1;
        }
        $this->view->lay = $m->get_layout();
        $this->view->apps = $m->get_apps();
        $this->view->ws = $m->get_workspace();
        // Zend_Debug::dump($m->get_apps());die();
        $this->view->varams = $params;
    }

    public function listpageAction() {
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        // $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        // // $this->view->headScript()->appendFile('/assets/core/scripts/table-ajax.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // // $this->view->headScript()->appendFile('/assets/core/scripts/ui-extended-modals.js');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        // $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
		
		$this->view->headScript()->appendFile('/assets/bigbox/js/bigbuilder_listpage.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $sys = new Model_Zprabapage();
        $mdl = new Model_Prabasystem();
        $params = $sys->list_page();
        // Zend_Debug::dump($params);die();
        $apps = $mdl->get_apps();
        $this->view->params = $data;
        $this->view->apps = $apps;
    }

    public function listactionAction() {
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        // $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
		// $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        // $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
		
		$this->view->headScript()->appendFile('/assets/bigbox/js/bigaction_listaction.js');
		$this->view->headScript()->appendScript('
			var $modal2 = $("#static");
			$("body").on("click", "#listpage .btn-delete", function() {
			  var id =  $(this).attr("data-id");
			 
			  if($modal2.find("td#static_id")!=undefined){
				$modal2.find("td#static_id").html(id);
			  }
			  $modal2.modal();                
			});
			
			$modal2.on("click", "button#deleteconfirm", function(){
				var id = $modal2.find("td#static_id").html();
				// alert(id);
				$.ajax({
					url:"/prabajax/deleteaction",
					content:"json",
					type:"post",
					data:{uid:id},
					beforeSend:function(){
						$("#processbar2").removeClass("m--hide");
						$("#alertinfo2").removeClass("alert-success");
						$("#alertinfo2").removeClass("alert-warning");
						$("#alertinfo2").removeClass("alert-danger");
						$("#alertinfo2").removeClass("alert-info");
					},
					complete:function(result){
						$("#processbar2").addClass("m--hide");
					},
					success: function(data){
						//console.log(data);
						if(data.result!=undefined){
							if(data.result==true){
								$("#alertinfo2").removeClass("m--hide");
								$("#alertinfo2").addClass("alert-success");
								$("#alertmsg2").html("<strong>Success!</strong> "+data.retMsg);
								$("#processbar2").addClass("m--hide");
								$("#reload").click();
								$modal2.modal("hide");
								
							}else{
								$("#alertinfo2").addClass("alert-warning");
								$("#alertmsg2").html("<strong>Failed!</strong> "+data.retMsg);
							}
						}else{
							$("#alertinfo2").addClass("alert-info");
							$("#alertmsg2").html("<strong>Error!</strong> System is busy, please try again.");
						}
						$("#alertinfo2").addClass("m--hide");
					},
					error:function(){
						$("#alertinfo2").addClass("alert-info");
						$("#alertmsg2").html("<strong>Error!</strong> System is busy, please try again.");
						$("#alertinfo2").removeClass("m--hide");
					}
				});
            });
		');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $sys = new Model_Zprabapage();
        $mdl = new Model_Prabasystem();
        $params = $sys->list_action();
        // Zend_Debug::dump($params);die();
        //$apps = $mdl->get_apps ();
        $this->view->params = $data;
        //$this->view->apps = $apps;
    }
	public function getscriptAction() {
			$this->_helper->layout->disableLayout();
			$params = $this->getRequest()->getParams();
			//Zend_Debug::dump($_SERVER); die();
			// $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
			echo '

				<html>
				<head>	
				<link rel="stylesheet" href="/assets/core/global/plugins/jqueryhighlight/jquery.highlight.css">
				<script src="/assets/core/global/plugins/jqueryhighlight/jquery.highlight.js"></script>
				<script>
				$(document).ready(function(){
							$(\'pre.code\').highlight({source:1, zebra:1, indent:\'space\', list:\'ol\'});
						});
				</script>
				</head>
				<div class="modal-dialog modal-lg" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLongTitle">
								</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">
										&times;
									</span>
								</button>
							</div>
							<div class="modal-body">
							<pre class="code" lang="php">
							' . file_get_contents('http://' . $_SERVER['HTTP_HOST'] . '/prabapublish/viewportlet/id/' . $params['id'] . '/modal/1') . '
							</code></pre>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn default" data-dismiss="modal">Close</button>
							</div>
				</div>
				</html>
				';
			die();
	}
    public function listportletAction() {
		
		//###Disable Script From Old
			// $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
			// $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
			// $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
			// $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
			// $this->view->headScript()->appendFile('/assets/core/plugins/jquery-1.10.2.min.js');
			// $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
			// $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
			// $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
			// // $this->view->headScript()->appendFile('/assets/core/scripts/table-ajax.js');
			// $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
			// $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
			// // $this->view->headScript()->appendFile('/assets/core/scripts/ui-extended-modals.js');
			// $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
			// $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
			// $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
			// $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
			// $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
			// $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
			// $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
			// $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
		//###Disable Script From Old
		
		//###New Script Javascript
		$this->view->headScript()->appendFile('/assets/bigbox/js/bigbuilder_listportlet.js');
		
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $sys = new Model_Zprabapage();
        $mdl = new Model_Prabasystem();
        $params = $sys->list_page();
        // Zend_Debug::dump($params);die();
        $apps = $mdl->get_apps();
        $this->view->params = $data;
        $this->view->apps = $apps;
    }
    public function addportletAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        // <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $params = $this->getRequest()->getParams();
        $dd = new Model_Prabasystemaddon();
        $apis = $dd->get_api_grouping();
        $this->view->apis = $apis;
    }

    public function editportletAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        // <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $dd = new Model_Prabasystemaddon();
        $apis = $dd->get_api_grouping();
        $params = $this->getRequest()->getParams();
        $cd = new Model_Zprabapage();
        $data = $cd->get_portlet($params['id']);
        $this->view->data = $data;
        $dd = new Model_Prabasystemaddon();
        $apis = $dd->get_api_grouping();
        $this->view->apis = $apis;
        // Zend_Debug::dump($data);
        // die();
    }

    public function buildactAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/maestro/css/maestro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/maestro/css/maestro_ctools.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/maestro/js/wz_jsgraphics.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/maestro/js/admin_template_editor.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/maestro/js/jquery.contextmenu.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/maestro/js/jquery.simplemodal.min.js');
    }

    public function inboxAction(){
        $this->view->headLink()->appendStylesheet('/assets/core/apps/css/inbox.min.css');
    }
    public function calendarAction(){
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/fullcalendar/fullcalendar.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/moment.min.js');        
        $this->view->headScript()->appendFile('/assets/core/global/plugins/fullcalendar/fullcalendar.min.js');        
        $this->view->headScript()->appendFile('/assets/core/apps/scripts/calendar.min.js');        
    }
	
	public function viewportletAction() {
		
        Zend_Session::namespaceUnset('api');
        $ns = new Zend_Session_Namespace('api');
        $ns->api = array();
        $params = $this->getRequest()->getParams();
        if (isset($params['modal']) && $params['modal'] == 1) {
            $this->_helper->layout->disableLayout();
        }

       $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
		$this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
		$this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
		$this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jquery-multi-select/css/multi-select.css');
		$this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
		$this->view->headLink ()->appendStylesheet ( '/assets/core/global/css/plugins.css' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css' );
		
		$this->view->headLink ()->appendStylesheet ( '/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css' );
		
		
		$this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
		$this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
		$this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
		$this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
		$this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
		// $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
		$this->view->headScript ()->appendFile ( '/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js' );
		


        $this->view->element = $params['id'];
        $this->view->varams = $params;
    }

}
