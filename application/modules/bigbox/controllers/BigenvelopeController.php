<?php
/*
 * Copyright (C) bigbox, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by bacanghaneut <rohimfikri@gmail.com>,
*/
class Bigbox_BigenvelopeController extends Zend_Controller_Action{

    public function init() {
    	
    }

    public function indexAction(){
        
    }

public function grafanaAction(){
    $params = $this->getRequest()->getParams();
    // Zend_Debug::dump($params);die();

    $val2 = end($params); //Zend_Debug::dump($val2);
    $val1 = array_search($val2, $params); //Zend_Debug::dump($val1);
    // die();

    $iframeUrl = "http://jt-hdp02i0403.telkom.co.id:3000/dashboard/db/".$val1."-".$val2."?theme=light";

    try {
        $authAdapter = Zend_Auth::getInstance();
        $identity = $authAdapter->getIdentity();
    } catch (Exception $e) {
        
    }
    $this->view->params = $params;
    $this->view->iframeUrl = $iframeUrl;
    $this->view->identity = $identity;
    // Zend_Debug::dump($identity);die();
    $this->view->headScript()->appendScript("
    jQuery(document).ready(function(){
        var ctn = $('.m-grid__item.m-grid__item--fluid.m-wrapper');
        var h = ctn.innerHeight();
        $('iframe').css('height',h-30);
    });
    ");
    $this->renderScript ( 'grafana/index.phtml' );
}
    
	public function getmethodsAction() {


        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();


        switch ($params['apitype']) {
            case 4 :
                $out = $this->get_method_zend($params);
                break;

            case 3:
            case 2 :
                $out = $this->get_method_proc($params);
                break;
        }

        echo $out;
        die();
    }

    function get_method_proc($params) {

        //Zend_Debug::dump($params); die();
        $mm = new Model_Prabasystem ();
        $dd = new Model_Prabasystemdriver ();
        $data = $mm->get_a_conn_by_name($params['conn']);
        $dat = $dd->get_list_proc($data, null, $params['xxxid']);

        $this->_helper->layout->disableLayout();



        // $out ="";
        $out = "<option></option>";
        foreach ($dat as $k => $v) {
            $out .= "<option value='" . $v . "'>" . $v . "</option>";
        }

        echo $out;
        die();
    }

    function get_method_zend($params) {


        $this->_helper->layout->disableLayout();


        $mm = new Model_Prabasystem ();
        $dat = $mm->get_method_class_model($params ['xxxid']);
        // $out ="";
        $out = "<option></option>";
        foreach ($dat as $k => $v) {
            $out .= "<option value='" . $v . "'>" . $v . "</option>";
        }

        echo $out;
        die();
    }

    public function getmodelsAction() {

        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();


        switch ($params['xxxid']) {
            case 4 :
                $out = $this->modelZend($params);
                break;

            case 3:
            case 2 :
                $out = $this->modelProc($params);
                break;
        }

        echo $out;
        die();
    }

    function modelProc($params) {

        $mm = new Model_Prabasystem ();
        $dd = new Model_Prabasystemdriver ();
        $data = $mm->get_a_conn($params['conn']);
        //Zend_Debug::dump($data);die();

        $list = $dd->get_list_proc($data, 'user');
        $out = '	<div class="resType row">
						<div class="col-md-6">
							<div class="form-group m-form__group">
							<label for="model">Schema</label>
								<select onchange=changeModel(this)  name="model" id="model" class="form-control" placeholder="Model" >
									<option></option>';
									foreach ($list as $v) {
										$out .= "<option value='" . $v . "'>" . $v . "</option>";
									}
									$out .= '</select>
									<span class="m-form__help">
										Select Model Class
									</span>
							</div>
						</div>
						<div class="col-md-offset-1 col-md-6 ">
							<div class="form-group m-form__group">
								<label for="method">Proc</label>
								<select  onchange=changeMethod(this)  name="method" id="method" class="form-control" placeholder="Method" >
								</select>
							</div>
						</div>
					</div>';


        return $out;
    }

    function modelZend($params) {



        $mm = new Model_Prabasystem ();
        $dat = $mm->get_models_class();

        $out = '<div class="resType row"><div  class="col-md-6">
										<div class="form-group">
											<label for="model">Class Model</label><select onchange=changeModel(this)  name="model" id="model" class="form-control" placeholder="Model" >
											<option></option>';

        foreach ($dat as $k => $v) {
            // Zend_Debug::dump($v);
            $out .= "<optgroup label='" . $k . "'>";
            foreach ($v as $vv) {
                $out .= "<option value='" . $vv . "'>" . $vv . "</option>";
            }

            $out .= "</optgroup>";
        }
        $out .= '</select>
											<span class="m-form__help">
												Select Model Class
											</span>
										</div>
									</div>
									<div class="col-md-offset-1 col-md-6 ">
										<div class="form-group">
											<label for="method">Method Class</label><select  onchange=changeMethod(this)  name="method" id="method" class="form-control" placeholder="Method" >
											</select>
										</div>
									</div></div>';


        return $out;
    }
    public function listconnAction() {
		$this->view->headScript()->appendScript('
		$( "#addconn" ).validate({
            // define validation rules
            rules: {
                conn: {
                    required: true 
                }
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                var alert = $("#m_form_1_msg");
                alert.removeClass("m--hide").show();
               $("#static2").scrollTop(0);
            },

            submitHandler: function (form) {
                //form[0].submit(); // submit the form
				var proses = $("#processbar");
				$("#m_form_1_msg").hide();
				$("#processbar").show();
				$("#static2").scrollTop(0);
				submitForm();
	            return false;
            }
        });
		var submitForm = function(){
			//console.log($("form#addconn").serialize());
			$.ajax({
				url:"/prabajax/addconn",
				content:"json",
				type:"post",
				data:$("form#addconn").serialize(),
				beforeSend:function(){
					// $("#m_form_1_msg").addClass("m--hide");
					// $("#processbar").removeClass("m--hide");
					// //mApp.scrollTo(proses, 800);
					// $("#m_form_1_msg").removeClass("alert-success");
					// $("#m_form_1_msg").removeClass("alert-warning");
					// $("#m_form_1_msg").removeClass("alert-danger");
					// $("#m_form_1_msg").removeClass("alert-info");
				},
				complete:function(result){
					$("#processbar").addClass("m--hide");
				},
				success: function(data){
					//console.log(data);
					if(data.result!=undefined){
						if(data.result==true){
							var alert = $("#m_form_1_msg");
							alert.addClass("alert-success");
							$("#alertmsg").html("<strong>Success!</strong> "+data.retMsg);
							alert.removeClass("m--hide").show();
							$("#static2").scrollTop(0);
							$("#reload").click();
							$("#static2").modal("hide");
							document.getElementById("addconn").reset();
							//resetForm();
						}else{
							$("#m_form_1_msg").addClass("alert-warning");
							$("#alertmsg").html("<strong>Failed!</strong> "+data.retMsg);
						}
					}else{
						$("#m_form_1_msg").addClass("alert-info");
						$("#alertmsg").html("<strong>Error!</strong> System is busy, please try again.");
					}
					$("#m_form_1_msg").addClass("m--hide");
					
				},
				error:function(){
					$("#m_form_1_msg").addClass("alert-info");
					$("#alertmsg").html("<strong>Error!</strong> System is busy, please try again.");
					$("#m_form_1_msg").removeClass("m--hide");
				}
			});
			}
		function tescon(me){
			var $me = $(me);
			var $val = $me.val();
			$( "#addconn" ).validate({
				// define validation rules
				rules: {
					conn: {
						required: true 
					}
				},
				
				//display error alert on form submit  
				invalidHandler: function(event, validator) {     
					var alert = $("#m_form_1_msg");
					alert.removeClass("m--hide").show();
				   $("#static2").scrollTop(0);
				},

				submitHandler: function (form) {
					return false;
				}
			});
			
			$.ajax({
				url:"/prabajax/testconn",
				content:"json",
				type:"post",
				data:$("form#addconn").serialize(),
				beforeSend:function(){
					$("#m_form_1_msg").addClass("m--hide");
					$("#processbar").removeClass("m--hide");
					$("#static2").scrollTop(0);
					$("#m_form_1_msg").removeClass("alert-success");
					$("#m_form_1_msg").removeClass("alert-warning");
					$("#m_form_1_msg").removeClass("alert-danger");
					$("#m_form_1_msg").removeClass("alert-info");
				},
				complete:function(result){
					$("#processbar").addClass("m--hide");
				},
				success: function(data){
					console.log(data);
					if(data.result!=undefined){
						if(data.result==true){
							$("#m_form_1_msg").removeClass("m--hide");
							$("#m_form_1_msg").addClass("alert-success");
							$("#alertmsg").html("<strong>Success!</strong> "+data.retMsg);
						}else{
							$("#m_form_1_msg").removeClass("m--hide");
							$("#m_form_1_msg").addClass("alert-warning");
							$("#alertmsg").html("<strong>Failed!</strong> "+data.retMsg);
						}
					}else{
						$("#m_form_1_msg").removeClass("m--hide");
						$("#m_form_1_msg").addClass("alert-info");
						$("#alertmsg").html("<strong>Error!</strong> System is busy, please try again.");
					}
					$("#m_form_1_msg").removeClass("m--hide");
				},
				error:function(){
					$("#m_form_1_msg").addClass("alert-info");
					$("#alertmsg").html("<strong>Error!</strong> System is busy, please try again.");
					$("#m_form_1_msg").removeClass("m--hide");
				}
			});
		}
		');
		$this->view->headScript()->appendFile('/assets/bigbox/js/bigenvelope_listconn.js');
        $mm = new Model_Zprabapage();
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        // $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        // // $this->view->headScript()->appendFile('/assets/core/scripts/table-ajax.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // // $this->view->headScript()->appendFile('/assets/core/scripts/ui-extended-modals.js');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        // $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $sys = new Model_Prabasystem();
        $pa = new Model_Zparams();
        $this->view->adapts = $pa->get_adapters();
        // Zend_Debug::dump($pa->get_adapters()); //die();
        $datacon = $sys->get_conns_serialize();
        // Zend_Debug::dump($datacon); die();
        $this->view->conns = $datacon;
    }
	public function listapijAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }

        $this->_helper->layout->disableLayout();

        $mdl_admin = new Model_Prabasystem ();
        $countgraph = $mdl_admin->count_listapibigenvelope($_GET);

        $params = $mdl_admin->get_params('api_type');

        foreach ($params as $v) {
            $vparams [$v ['value_param']] = $v ['display_param'];
        }

        $listgraph = $mdl_admin->listapibigenvelope($_GET);
        // Zend_Debug::dump($countgraph);die();
        $iTotalRecords = intval($countgraph);
		
		$getdatatable	= $_GET['datatable'];
		$page			= round($getdatatable['pagination']['page']);
		$perpages		= round($getdatatable['pagination']['perpage']);
		$pages			= ceil($iTotalRecords/$perpages);
		
		$iDisplayLength = intval($perpages);
        //$pageSize = intval($_GET ['pageSize']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval(($page-1)*$perpages);
        //$sEcho = intval($_GET ['sEcho']);

        $records = array();
		$records ["meta"] = array();

		$end = $iDisplayStart + $iDisplayLength;
		$records ["meta"] = array(
			"page"=> $page,
			"pages"=> $pages,
			"perpage"=> $perpages,
			"total"=> $iTotalRecords,
			"sort"=> "asc",
			"field"=> "No",
		);

        foreach ($listgraph as $k => $v) {
            if ($v['is_running_socket'] != 1) {
                $run = '<br><span class="label label-sm label-danger">
													socket server not running
												</span>';
            } elseif ($v['is_running_socket'] == 1) {
                $run = '<br><span class="label label-sm label-success">
													socket server  running
												</span>';
            }

            if ($v['is_freeze'] != 1) {
                $soc = '<div style="text-align:center"><a href="javascript:;" data-id="' . $v ['id'] . '" class="btn btn-xs blue btn-freeze"><i class="fa fa-search"></i> freeze </a>';
            } elseif ($v['is_running_socket'] == 1) {
                $soc = '';
            }

            $records['data'][] = array(
                "NO" => $iDisplayStart + $k + 1,
                "id" => $v ['id'],
                "conn_name" =>$v ['conn_name'],
                "sql_text" =>$v ['sql_text'],
                "api_desc" =>$v ['api_desc'],
                "api_type" =>$vparams [$v ['api_type']],
                "api_str_replace" =>$v ['api_str_replace'],
                "attrs1" =>$v ['attrs1'],
                "cache_time" =>$v ['cache_time'],
				'action' =>  '
							<a href="/bigbox/bigenvelope/editapi/id/' . $v ['id'] . '" data-id="' . $v ['id'] . '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit Action">
								<i class="la la-edit"></i>
							</a>
							<a data-toggle="modal" data-target="#static" data-id="' . $v ['id'] . '" data-uname="' . $v ['uname'] . '" data-fullname="' . $v ['fullname'] . '" data-email="' . $v ['email'] . '" data-ubis="' . $v ['ubis'] . '" data-sububis="' . $v ['sub_ubis'] . '" data-sububisid="' . $v ['sub_ubis_id'] . '" data-position="' . $v ['jabatan'] . '" data-mobilephone="' . $v ['mobile_phone'] . '" data-fixedphone="' . $v ['fixed_phone'] . '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btn-delete" title="Delete">
								<i class="la la-trash"></i>
							</a>'
            );
        }

        // $records ["sEcho"] = $sEcho;
        // $records ["iTotalRecords"] = $iTotalRecords;
        // $records ["iTotalDisplayRecords"] = $iTotalRecords;

        /*
         * $gg = new Escort_Model_Kpu(); $res = $gg->get_datakpu($data2['witel']); //var_dump($res);die();
         */
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }
	public function listapiAction() {
        // $mm = new Model_Prabasystem();
        // Zend_Debug::dump($mm->get_models_class()); die();
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        // // $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        // // $this->view->headScript()->appendFile('/assets/core/scripts/table-ajax.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // // $this->view->headScript()->appendFile('/assets/core/scripts/ui-extended-modals.js');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        // $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
		
		//###New Script Javascript
		$this->view->headScript()->appendScript('
			$( "#addapi" ).validate({
				rules: {
					sqltext: {
						required: true 
					},
					apidesc: {
						required: true
					}
				},
				
				//display error alert on form submit  
				invalidHandler: function(event, validator) {     
					var alert = $("#m_form_1_msg");
					alert.removeClass("m--hide").show();
					$("#static2").scrollTop(0);
				},

				submitHandler: function (form) {
					//form[0].submit(); // submit the form
					var proses = $("#processbar");
					$("#m_form_1_msg").addClass("m--hide");
					$("#processbar").removeClass("m--hide");
					$("#static2").scrollTop(0);
					$("#m_form_1_msg").removeClass("alert-success");
					$("#m_form_1_msg").removeClass("alert-warning");
					$("#m_form_1_msg").removeClass("alert-danger");
					$("#m_form_1_msg").removeClass("alert-info");
					submitForm();
					return false;
				}
			});
			var $modal2 = $("#static");
			$("body").on("click", "#listapi .btn-delete", function() {
			  var id =  $(this).attr("data-id");
			 
			  if($modal2.find("td#static_id")!=undefined){
				$modal2.find("td#static_id").html(id);
			  }
			  $modal2.modal();                
			});
			
			$modal2.on("click", "button#deleteconfirm", function(){
				var id = $modal2.find("td#static_id").html();
				// alert(id);
				$.ajax({
					url:"/prabajax/deleteapi",
					content:"json",
					type:"post",
					data:{uid:id},
					beforeSend:function(){
						$("#processbar2").removeClass("m--hide");
						$("#alertinfo2").removeClass("alert-success");
						$("#alertinfo2").removeClass("alert-warning");
						$("#alertinfo2").removeClass("alert-danger");
						$("#alertinfo2").removeClass("alert-info");
					},
					complete:function(result){
						$("#processbar2").addClass("m--hide");
					},
					success: function(data){
						//console.log(data);
						if(data.result!=undefined){
							if(data.result==true){
								$("#alertinfo2").removeClass("m--hide");
								$("#alertinfo2").addClass("alert-success");
								$("#alertmsg2").html("<strong>Success!</strong> "+data.retMsg);
								$("#processbar2").addClass("m--hide");
								$("#reload").click();
								$modal2.modal("hide");
								
							}else{
								$("#alertinfo2").addClass("alert-warning");
								$("#alertmsg2").html("<strong>Failed!</strong> "+data.retMsg);
							}
						}else{
							$("#alertinfo2").addClass("alert-info");
							$("#alertmsg2").html("<strong>Error!</strong> System is busy, please try again.");
						}
						$("#alertinfo2").addClass("m--hide");
					},
					error:function(){
						$("#alertinfo2").addClass("alert-info");
						$("#alertmsg2").html("<strong>Error!</strong> System is busy, please try again.");
						$("#alertinfo2").removeClass("m--hide");
					}
				});
            });
			
			var submitForm = function(){
			//console.log($("form#addapi").serialize());
			$.ajax({
				url:"/prabajax/addapi",
				content:"json",
				type:"post",
				data:$("form#addapi").serialize(),
				beforeSend:function(){
					// $("#m_form_1_msg").addClass("m--hide");
					// $("#processbar").removeClass("m--hide");
					// //mApp.scrollTo(proses, 800);
					// $("#m_form_1_msg").removeClass("alert-success");
					// $("#m_form_1_msg").removeClass("alert-warning");
					// $("#m_form_1_msg").removeClass("alert-danger");
					// $("#m_form_1_msg").removeClass("alert-info");
				},
				complete:function(result){
					$("#processbar").addClass("m--hide");
				},
				success: function(data){
					//console.log(data);
					if(data.result!=undefined){
						if(data.result==true){
							var alert = $("#m_form_1_msg");
							alert.addClass("alert-success");
							$("#alertmsg").html("<strong>Success!</strong> "+data.retMsg);
							alert.removeClass("m--hide").show();
							$("#static2").scrollTop(0);
							$("#reload").click();
							$("#static2").modal("hide");
							document.getElementById("addapi").reset();
							//resetForm();
						}else{
							$("#m_form_1_msg").addClass("alert-warning");
							$("#alertmsg").html("<strong>Failed!</strong> "+data.retMsg);
						}
					}else{
						$("#m_form_1_msg").addClass("alert-info");
						$("#alertmsg").html("<strong>Error!</strong> System is busy, please try again.");
					}
					$("#m_form_1_msg").addClass("m--hide");
					
				},
				error:function(){
					$("#m_form_1_msg").addClass("alert-info");
					$("#alertmsg").html("<strong>Error!</strong> System is busy, please try again.");
					$("#m_form_1_msg").removeClass("m--hide");
				}
			});
			}
			
			
			$( "#testig" ).click(function() {
			 $.ajax({
					url:"/prabajax/tesapi",
					content:"html",
					type:"post",
					data:$("form#addapi").serialize(),
					
					complete:function(result){
								
					},
					success: function(result){
						//alert("ok");
						//var jsonObj = $.parseJSON("[" + result + "]");
						//alert (jsonObj); 
						//console.log(result);
						
						
						$("#outputsqltext").html(result);
							$.ajax({
							
							url:"/prabajax/getout",
							content:"html",
							type:"post",
							data:$("form#addapi").serialize(),
							
							complete:function(data){
										
							},
							success: function(data){
								//alert("ok");
								//var jsonObj = $.parseJSON("[" + result + "]");
								//alert (jsonObj); 
								//console.log(result);
								
								//	$outputsqltext.html(result);
								$("#arrayout").html(data);
								
								
								
							},
							error:function(){
								
							}
							});
							
						
						
						
					},
					error:function(){
						
					}
					});
				});
				
		');
		$this->view->headScript()->appendFile('/assets/bigbox/js/bigenvelope_listapi.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $sys = new Model_Prabasystem();
        $page = new Model_Zprabapage();
        $userapp = $page->list_users_api();
        // Zend_Debug::dump($userapp); die();
        $params = $sys->get_params('api_type');
        $modes = $sys->get_params('api_mode');
        // Zend_Debug::dump($modes); die();
        $conns = $sys->get_conn();
        $this->view->modes = $modes;
        $this->view->params = $params;
        $this->view->conns = $conns;
        $this->view->userapp = $userapp;
    }
	public function listapiuserAction() {
        // $mm = new Model_Prabasystem();
        // Zend_Debug::dump($mm->get_models_class()); die();
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        // $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/table-ajax.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/ui-extended-modals.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $sys = new Model_Prabasystem();
        $page = new Model_Zprabapage();
        $userapp = $page->list_users_api();
        // Zend_Debug::dump($userapp); die();
        $params = $sys->get_params('api_type');
        $modes = $sys->get_params('api_mode');
        // Zend_Debug::dump($modes); die();
        $conns = $sys->get_conn();
        $this->view->modes = $modes;
        $this->view->params = $params;
        $this->view->conns = $conns;
        $this->view->userapp = $userapp;
    }
	public function addapiAction() {
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        // // <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        // $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
		$this->view->headScript()->appendScript('
		$( "#addapi" ).validate({
			// define validation rules
			rules: {
				sqltext: {
					required: true 
				},
				apidesc: {
					required: true
				}
			},
			
			//display error alert on form submit  
			invalidHandler: function(event, validator) {     
				var alert = $("#m_form_1_msg");
				alert.removeClass("m--hide").show();
				$("html,body").scrollTop(0);
			},

			submitHandler: function (form) {
				//form[0].submit(); // submit the form
				var proses = $("#processbar");
				$("#m_form_1_msg").addClass("m--hide");
				$("#processbar").removeClass("m--hide");
				$("html,body").scrollTop(0);
				$("#m_form_1_msg").removeClass("alert-success");
				$("#m_form_1_msg").removeClass("alert-warning");
				$("#m_form_1_msg").removeClass("alert-danger");
				$("#m_form_1_msg").removeClass("alert-info");
				submitForm();
				return false;
			}
		});
		var submitForm = function(){
			//console.log($("form#addapi").serialize());
			$.ajax({
				url:"/prabajax/addapi",
				content:"json",
				type:"post",
				data:$("form#addapi").serialize(),
				beforeSend:function(){
					$("#m_form_1_msg").addClass("m--hide");
					$("#processbar").removeClass("m--hide");
					$("html,body").scrollTop(0);
					$("#m_form_1_msg").removeClass("alert-success");
					$("#m_form_1_msg").removeClass("alert-warning");
					$("#m_form_1_msg").removeClass("alert-danger");
					$("#m_form_1_msg").removeClass("alert-info");
				},
				complete:function(result){
					$("#processbar").addClass("m--hide");
				},
				success: function(data){
					//console.log(data);
					if(data.result!=undefined){
						if(data.result==true){
							$("#m_form_1_msg").removeClass("m--hide");
							$("#m_form_1_msg").addClass("alert-success");
							$("#alertmsg").html("<strong>Success!</strong> "+data.retMsg);
							$("#static2").scrollTop(0);
							$("#reload").click();
							document.getElementById("addapi").reset();
							//resetForm();
							location.reload();
						}else{
							$("#m_form_1_msg").addClass("alert-warning");
							$("#alertmsg").html("<strong>Failed!</strong> "+data.retMsg);
						}
					}else{
						$("#m_form_1_msg").addClass("alert-info");
						$("#alertmsg").html("<strong>Error!</strong> System is busy, please try again.");
					}
					
					
				},
				error:function(){
					$("#m_form_1_msg").addClass("alert-info");
					$("#alertmsg").html("<strong>Error!</strong> System is busy, please try again.");
					$("#m_form_1_msg").removeClass("m--hide");
				}
			});
			}
			
			
			$( "#testig" ).click(function() {
			 $.ajax({
					url:"/prabajax/tesapi",
					content:"html",
					type:"post",
					data:$("form#addapi").serialize(),
					
					complete:function(result){
								
					},
					success: function(result){
						//alert("ok");
						//var jsonObj = $.parseJSON("[" + result + "]");
						//alert (jsonObj); 
						//console.log(result);
						
						
						$("#outputsqltext").html(result);
							$.ajax({
							
							url:"/prabajax/getout",
							content:"html",
							type:"post",
							data:$("form#addapi").serialize(),
							
							complete:function(data){
										
							},
							success: function(data){
								//alert("ok");
								//var jsonObj = $.parseJSON("[" + result + "]");
								//alert (jsonObj); 
								//console.log(result);
								
								//	$outputsqltext.html(result);
								$("#arrayout").html(data);
							},
							error:function(){
								
							}
							});
							
						
						
						
					},
					error:function(){
						
					}
					});
				});
		');
        $params = $this->getRequest()->getParams();
        $sys = new Model_Prabasystem();
        $page = new Model_Zprabapage();
        $userapp = $page->list_users_api();
        // Zend_Debug::dump($userapp); die();
        $params = $sys->get_params('api_type');
        $modes = $sys->get_params('api_mode');
        // Zend_Debug::dump($modes); die();
        $conns = $sys->get_conn();
        $this->view->modes = $modes;
        $this->view->params = $params;
        $this->view->conns = $conns;
        $this->view->userapp = $userapp;
    }
	public function editconnAction() {
        $params = $this->getRequest()->getParams();
        if(!isset($params['id'])|| $params['id'] == '') {
            $this->_redirect('/prabaeditor/listapi');
        }
		$this->view->headScript()->appendScript('
		$( "#editconn" ).validate({
            // define validation rules
            rules: {
                conn: {
                    required: true 
                }
            },
            
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                var alert = $("#m_form_1_msg");
                alert.removeClass("m--hide").show();
				$("html,body").scrollTop(0);
            },

            submitHandler: function (form) {
                //form[0].submit(); // submit the form
				var proses = $("#processbar");
				$("#m_form_1_msg").addClass("m--hide");
				$("#processbar").removeClass("m--hide");
				$("html,body").scrollTop(0);
				$("#m_form_1_msg").removeClass("alert-success");
				$("#m_form_1_msg").removeClass("alert-warning");
				$("#m_form_1_msg").removeClass("alert-danger");
				$("#m_form_1_msg").removeClass("alert-info");
				submitForm();
				return false;
            }
        });
		var submitForm = function(){
		//console.log($("form#editconn").serialize());
		$.ajax({
			url:"/prabajax/editconn",
			content:"json",
			type:"post",
			data:$("form#editconn").serialize(),
			beforeSend:function(){
				$("#m_form_1_msg").addClass("m--hide");
				$("#processbar").removeClass("m--hide");
				$("html,body").scrollTop(0);
				$("#m_form_1_msg").removeClass("alert-success");
				$("#m_form_1_msg").removeClass("alert-warning");
				$("#m_form_1_msg").removeClass("alert-danger");
				$("#m_form_1_msg").removeClass("alert-info");
			},
			complete:function(result){
				$("#processbar").addClass("m--hide");
			},
			success: function(data){
				//console.log(data);
				if(data.result!=undefined){
					if(data.result==true){
						$("#m_form_1_msg").removeClass("m--hide");
						$("#m_form_1_msg").addClass("alert-success");
						$("#alertmsg").html("<strong>Success!</strong> "+data.retMsg);
						$("#static2").scrollTop(0);
						$("#reload").click();
						document.getElementById("editconn").reset();
						//resetForm();
						location.reload();
					}else{
						$("#m_form_1_msg").addClass("alert-warning");
						$("#alertmsg").html("<strong>Failed!</strong> "+data.retMsg);
					}
				}else{
					$("#m_form_1_msg").addClass("alert-info");
					$("#alertmsg").html("<strong>Error!</strong> System is busy, please try again.");
				}
				
				
			},
			error:function(){
				$("#m_form_1_msg").addClass("alert-info");
				$("#alertmsg").html("<strong>Error!</strong> System is busy, please try again.");
				$("#m_form_1_msg").removeClass("m--hide");
			}
		});
		}
		function tescon(me){
			var $me = $(me);
			var $val = $me.val();
			$( "#editconn" ).validate({
				// define validation rules
				rules: {
					conn: {
						required: true 
					}
				},
				
				//display error alert on form submit  
				invalidHandler: function(event, validator) {     
					var alert = $("#m_form_1_msg");
					alert.removeClass("m--hide").show();
					$("html,body").scrollTop(0);
				},

				submitHandler: function (form) {
					return false;
				}
			});
			
				$.ajax({
					url:"/prabajax/testconn",
					content:"json",
					type:"post",
					data:$("form#editconn").serialize(),
					beforeSend:function(){
						$("#m_form_1_msg").addClass("m--hide");
						$("#processbar").removeClass("m--hide");
						$("#static2").scrollTop(0);
						$("#m_form_1_msg").removeClass("alert-success");
						$("#m_form_1_msg").removeClass("alert-warning");
						$("#m_form_1_msg").removeClass("alert-danger");
						$("#m_form_1_msg").removeClass("alert-info");
					},
					complete:function(result){
						$("#processbar").addClass("m--hide");
					},
					success: function(data){
						console.log(data);
						if(data.result!=undefined){
							if(data.result==true){
								$("#m_form_1_msg").addClass("alert-success");
								$("#alertmsg").html("<strong>Success!</strong> "+data.retMsg);
							}else{
								$("#m_form_1_msg").addClass("alert-warning");
								$("#alertmsg").html("<strong>Failed!</strong> "+data.retMsg);
							}
						}else{
							$("#m_form_1_msg").addClass("alert-info");
							$("#alertmsg").html("<strong>Error!</strong> System is busy, please try again.");
						}
						$("html,body").scrollTop(0);
						$("#m_form_1_msg").removeClass("m--hide");
					},
					error:function(){
						$("#m_form_1_msg").addClass("alert-info");
						$("#alertmsg").html("<strong>Error!</strong> System is busy, please try again.");
						$("#m_form_1_msg").removeClass("m--hide");
					}
				});
		}
		');
		$this->view->headScript()->appendFile('/assets/bigbox/js/bigenvelope_editcon.js');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        // $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $mdl_sys = new Model_Prabasystem();
        $data = $mdl_sys->get_a_conn($params['id']);
        $page = new Model_Zprabapage();
        $conns = $mdl_sys->get_conn();
        $pa = new Model_Zparams();
        $this->view->adapts = $pa->get_adapters();
        $this->view->modes = $modes;
        $this->view->params = $params;
        $this->view->conns = $conns;
        $this->view->data = $data;
    }
	public function editapiuserAction() {
        $params = $this->getRequest()->getParams();
        if(!isset($params['id'])|| $params['id'] == '') {
            $this->_redirect('/prabaeditor/listapi');
        }
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $mdl_sys = new Model_Prabasystem();
        $data = $mdl_sys->get_a_api_user($params['id']);
        $page = new Model_Zprabapage();
        // Zend_Debug::dump($data); die();
        $this->view->data = $data;
    }

    public function editapiAction() {
		$this->view->headScript()->appendScript('
			$( "#addapi" ).validate({
				// define validation rules
				rules: {
					sqltext: {
						required: true 
					},
					apidesc: {
						required: true
					}
				},
				
				//display error alert on form submit  
				invalidHandler: function(event, validator) {     
					var alert = $("#m_form_1_msg");
					alert.removeClass("m--hide").show();
					$("html,body").scrollTop(0);
				},

				submitHandler: function (form) {
					//form[0].submit(); // submit the form
					var proses = $("#processbar");
					$("#m_form_1_msg").addClass("m--hide");
					$("#processbar").removeClass("m--hide");
					$("html,body").scrollTop(0);
					$("#m_form_1_msg").removeClass("alert-success");
					$("#m_form_1_msg").removeClass("alert-warning");
					$("#m_form_1_msg").removeClass("alert-danger");
					$("#m_form_1_msg").removeClass("alert-info");
					submitForm();
					return false;
				}
			});
			var submitForm = function(){
			//console.log($("form#addapi").serialize());
			$.ajax({
				url:"/prabajax/editapi",
				content:"json",
				type:"post",
				data:$("form#addapi").serialize(),
				beforeSend:function(){
					$("#m_form_1_msg").addClass("m--hide");
					$("#processbar").removeClass("m--hide");
					$("html,body").scrollTop(0);
					$("#m_form_1_msg").removeClass("alert-success");
					$("#m_form_1_msg").removeClass("alert-warning");
					$("#m_form_1_msg").removeClass("alert-danger");
					$("#m_form_1_msg").removeClass("alert-info");
				},
				complete:function(result){
					$("#processbar").addClass("m--hide");
				},
				success: function(data){
					//console.log(data);
					if(data.result!=undefined){
						if(data.result==true){
							$("#m_form_1_msg").removeClass("m--hide");
							$("#m_form_1_msg").addClass("alert-success");
							$("#alertmsg").html("<strong>Success!</strong> "+data.retMsg);
							$("#static2").scrollTop(0);
							$("#reload").click();
							document.getElementById("addapi").reset();
							location.reload();
						}else{
							$("#m_form_1_msg").addClass("alert-warning");
							$("#alertmsg").html("<strong>Failed!</strong> "+data.retMsg);
						}
					}else{
						$("#m_form_1_msg").addClass("alert-info");
						$("#alertmsg").html("<strong>Error!</strong> System is busy, please try again.");
					}
					
					
				},
				error:function(){
					$("#m_form_1_msg").addClass("alert-info");
					$("#alertmsg").html("<strong>Error!</strong> System is busy, please try again.");
					$("#m_form_1_msg").removeClass("m--hide");
				}
			});
			}
			
			
			$( "#testig" ).click(function() {
			 $.ajax({
					url:"/prabajax/tesapi",
					content:"html",
					type:"post",
					data:$("form#addapi").serialize(),
					
					complete:function(result){
								
					},
					success: function(result){
						//alert("ok");
						//var jsonObj = $.parseJSON("[" + result + "]");
						//alert (jsonObj); 
						//console.log(result);
						
						
						$("#outputsqltext").html(result);
							$.ajax({
							
							url:"/prabajax/getout",
							content:"html",
							type:"post",
							data:$("form#addapi").serialize(),
							
							complete:function(data){
										
							},
							success: function(data){
								//alert("ok");
								//var jsonObj = $.parseJSON("[" + result + "]");
								//alert (jsonObj); 
								//console.log(result);
								
								//	$outputsqltext.html(result);
								$("#arrayout").html(data);
							},
							error:function(){
								
							}
							});
							
						
						
						
					},
					error:function(){
						
					}
					});
				});
		');
        $params = $this->getRequest()->getParams();
        if(!isset($params['id'])|| $params['id'] == '') {
            $this->_redirect('/prabaeditor/listapi');
        }
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );	
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $mdl_sys = new Model_Prabasystem();
        $data = $mdl_sys->get_a_api($params['id']);
        $page = new Model_Zprabapage();
        $userapp = $page->list_users_api();
        // Zend_Debug::dump($data); die();
        $modes = $mdl_sys->get_params('api_mode');
        $params = $mdl_sys->get_params('api_type');
        $conns = $mdl_sys->get_conn();
        $this->view->modes = $modes;
        $this->view->params = $params;
        $this->view->conns = $conns;
        $this->view->data = $data;
        $this->view->userapp = $userapp;
    }

}
