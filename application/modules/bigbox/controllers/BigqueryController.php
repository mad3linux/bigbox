<?php
/*
 * Copyright (C) bigbox, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by bacanghaneut <rohimfikri@gmail.com>,
*/
class Bigbox_BigqueryController extends Zend_Controller_Action{

    public function init() {	
    }

    public function indexAction(){
        
    }

public function grafanaAction(){
    $params = $this->getRequest()->getParams();
    // Zend_Debug::dump($params);die();

    $val2 = end($params); //Zend_Debug::dump($val2);
    $val1 = array_search($val2, $params); //Zend_Debug::dump($val1);
    // die();

    $iframeUrl = "http://jt-hdp02i0403.telkom.co.id:3000/dashboard/db/".$val1."-".$val2."?theme=light";

    try {
        $authAdapter = Zend_Auth::getInstance();
        $identity = $authAdapter->getIdentity();
    } catch (Exception $e) {
        
    }
    $this->view->params = $params;
    $this->view->iframeUrl = $iframeUrl;
    $this->view->identity = $identity;
    // Zend_Debug::dump($identity);die();
    $this->view->headScript()->appendScript("
    jQuery(document).ready(function(){
        var ctn = $('.m-grid__item.m-grid__item--fluid.m-wrapper');
        var h = ctn.innerHeight();
        $('iframe').css('height',h-30);
    });
    ");
    $this->renderScript ( 'grafana/index.phtml' );
}

    public function editorAction(){

        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();

        if($_POST){
            $this->_helper->layout->disableLayout();
            if( isset($params['ajax']) && (int)$params['ajax']==1){

                // Zend_Debug::dump($_POST);die();

                $arr = array(
                    230=>'columns',
                    231=>'properties',
                    232=>'samples',
                );

                $function = "get".ucfirst($_POST['tab_name']);

                $mdlHive = new Model_Hive();
                $res = $mdlHive->$function($_POST['database_name'],$_POST['table_name'],$_POST['clear_cache']);
                // Zend_Debug::dump($res);die();

                $result = array();
                $result[array_search($_POST['tab_name'], $arr)] = $res;
                if($_POST['tab_name']=='samples'){
                    $res = $mdlHive->getCount($_POST['database_name'],$_POST['table_name'],$_POST['clear_cache']);
                    $result[233] = $res;
                }

                header("Access-Control-Allow-Origin: *");
                header('Content-Type: application/json');
                echo json_encode($result);
                die();
            } else if(isset($params['ajax']) && (int)$params['ajax']==2){

                // Zend_Debug::dump($_POST);die();

                $sql = $_POST['qry'];
                
                $mdlHive = new Model_Hive();
                $result = $mdlHive->executeQueryEditor($sql); //Zend_Debug::dump($result);die();

                // $p = array(
                //     'sql_text'=>$result['text'],
                //     'sql_transaction'=>$result['transaction'],
                //     'message'=>$result['message'],
                // );
                // $mdlHive->insert_bigquery_editor_log($p);

                $result['tabId'] = (int)$_POST['tabId'];
                $result['parseData']['displayStart'] = (int)$_POST['offset'];
                if($result['transaction']){
                    $tmpAoColumns = array_keys($result['data'][0]);
                    // Zend_Debug::dump($tmpAoColumns);die();

                    $alias = (strpos($tmpAoColumns[0],'a.')!==FALSE?TRUE:FALSE);

                    $aoColumns = $tmpAoColumns;
                    if($alias){
                        $aoColumns = array();
                        foreach ($tmpAoColumns as $column) {
                            $aoColumns[] = substr($column, 2);
                        }
                    }

                    $columns = array();
                    foreach ($aoColumns as $k => $column) {
                        $columns[$k] = array(
                            'field'=>$column,
                            'title'=>$column,
                        );
                    }

                    $aaData = array();
                    foreach ($result['data'] as $k => $v) {
                        foreach ($aoColumns as $key => $value) {
                            $aaData[$k][$key] = ($v[$value]?$v[$value]:$v['a.'.$value]);
                        }
                    }

                    // Zend_Debug::dump($aaData);die();
                    $result['parseData']['aaData'] = $aaData;
                    $result['parseData']['aoColumns'] = $aoColumns;
                    $result['parseData']['columns'] = $columns;
                }

                header("Access-Control-Allow-Origin: *");
                header('Content-Type: application/json');
                echo json_encode($result);
                die();
            } else if(isset($params['ajax']) && (int)$params['ajax']==3){
                $exe = new Model_Zpraba4api();
                $dd = new Model_Zprabapage();
                $varC = $dd->get_api(235);
                $conn = unserialize($varC ['conn_params']);
                $result = $exe->execute_api($conn, $varC, array());
                // Zend_Debug::dump($result);die();

                header("Access-Control-Allow-Origin: *");
                header('Content-Type: application/json');
                echo json_encode($result);
                die();
            }

            $result = array(
                'transaction'=>false,
                'message'=>'invalid identifier',
            );
            header("Access-Control-Allow-Origin: *");
            header('Content-Type: application/json');
            echo json_encode($result);
            die();
        }

        $z = new Model_Cache();
        $allow_datatypes = array(
            'INT',
            'TINYINT',
            'SMALLIINT',
            'BIGINT',
            'BOOLEAN',
            'FLOAT',
            'DOUBLE',
            'STRING',
            'TIMESTAMP',
            'BINARY',
            'ARRAY',
            'MAP',
            'STRUCT',
            'UNION',
            'DECIMAL',
            'CHAR',
            'VARCHAR',
            'DATE',
        );

        $allow_datatypes = array_map('strtolower', $allow_datatypes);
        
        $cid = '279b1958a0062c5dddfa96292bc3cb84';
        
        $cache = $z->cachefunc(86400);
        $tree = $z->get_cache($cache, $cid);
        // Zend_Debug::dump($tree);die();  

        $hintOptions = array(
            'tables'=>array('SELECT * FROM'=>array()),
        );

        foreach ($tree as $k => $v) {
            foreach ($v['tables'] as $k1 => $v1) {
                $hintOptions['tables']['FROM '.$v['database_name'].'.'] = array();
                $hintOptions['tables'][$v['database_name']] = array();
                $hintOptions['tables'][$v['tab_name']] = array();
                foreach ($v1['columns'] as $k2 => $v2) {
                    $hintOptions['tables'][$v['tab_name']][] = $v2['col_name'];
                    $hintOptions['tables'][$v['database_name'].'.'.$v1['tab_name']][] = $v2['col_name'];
                }
            }
        }

        $this->view->allow_datatypes = $allow_datatypes;
        $this->view->tree = $tree;
        $this->view->hintOptions = json_encode($hintOptions);
        $this->view->dbtbcl = json_encode($dbtbcl);

        // datatable
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/scripts/datatable.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');        

        // codemirror
        $this->view->headLink ()->appendStylesheet ( '/assets/bigbox/plugins/codemirror-5.33.0/lib/codemirror.css');
        $this->view->headLink ()->appendStylesheet ( '/assets/bigbox/plugins/codemirror-5.33.0/addon/hint/show-hint.css');
        $this->view->headScript ()->appendFile ( '/assets/bigbox/plugins/codemirror-5.33.0/lib/codemirror.js');
        $this->view->headScript ()->appendFile ( '/assets/bigbox/plugins/codemirror-5.33.0/mode/sql/sql.js');
        $this->view->headScript ()->appendFile ( '/assets/bigbox/plugins/codemirror-5.33.0/addon/display/placeholder.js');
        $this->view->headScript ()->appendFile ( '/assets/bigbox/plugins/codemirror-5.33.0/addon/hint/show-hint.js');
        $this->view->headScript ()->appendFile ( '/assets/bigbox/plugins/codemirror-5.33.0/addon/hint/sql-hint.js');
        $this->view->headScript ()->appendFile ( '/assets/bigbox/plugins/codemirror-5.33.0/addon/selection/active-line.js');

        // custom js
        $this->view->headScript ()->appendFile ( '/assets/bigbox/js/bigquery_editor.js');
    }

    public function zeppelinAction(){

        $this->view->headScript()->appendScript('
            var h = $(\'.m-wrapper\').height();
            $(\'iframe\').css(\'height\',h);
        ');
        
    }

    public function getstcAction(){        
        $this->_helper->layout->disableLayout();
        $time_start = explode(' ',microtime());

        if($_POST){

            $input = array(
                'function'=>'SchemasTablesColumns'
            );

            $z = new Model_Cache();
            $cid = $z->get_id('Hive',$input);
            $cache = $z->cachefunc(604800);
            $data = $z->get_cache($cache, $cid);
            // Zend_Debug::dump($data);die();

            if(!$data || !is_array($data)){
                
                $time_end = explode(' ',microtime());
                $duration = number_format ( ($time_end [1] + $time_end [0] - ($time_start [1] + $time_start [0])), 3 );

                $result = array(
                    'transaction'=>false,
                    'time'=>date('Y-m-d H:i:s'),
                    'duration'=>$duration,
                    'message'=>'No Data available.',
                    'data'=>false,
                );

                header("Access-Control-Allow-Origin: *");
                header('Content-Type: application/json');
                echo json_encode($result);
                die();
            } else {

                $file = APPLICATION_PATH.'/../cache/zend_cache---'.$cid;
                if(file_exists($file)){
                    $last_cache_time = date('Y-m-d H:i:s',filemtime($file));
                } else {
                    $last_cache_time = date('Y-m-d H:i:s');
                }

                if($_POST['search']!=''){
                    $tmp = $data;
                    $data = array();

                    $list_database = array_column($tmp, 'database_name');
                    $input = preg_quote((string)$_POST['search'], '~');
                    $found_in_database = array_values(preg_grep('~' . $input . '~', $list_database));

                    if(count($found_in_database)>0){
                        foreach ($found_in_database as $key => $value) {
                            $alias = str_replace($_POST['search'], '<b>'.$_POST['search'].'</b>', $value);
                            $data[] = array(
                                'database_name'=>$value,
                                'alias'=>$alias,
                                'tables'=>array(),
                            );
                        }
                    }

                    $found_in_table = array();
                    $found_in_column = array();
                    foreach ($tmp as $k => $v) {

                        $list_table = array_column($v['tables'], 'tab_name');
                        $input = preg_quote((string)$_POST['search'], '~');
                        $tmp_found_in_table = preg_grep('~' . $input . '~', $list_table);

                        if(count($tmp_found_in_table)>0){

                            if(!in_array($v['database_name'], $found_in_database)){
                                $found_in_database[] = $v['database_name'];
                                $data[] = array(
                                    'database_name'=>$v['database_name'],
                                    'tables'=>array(),
                                );
                            }

                            $kx = array_search($v['database_name'], $found_in_database);

                            $found_in_table[$kx] = array_values($tmp_found_in_table);

                            foreach ($found_in_table[$kx] as $key => $value) {
                                $alias = str_replace($_POST['search'], '<b>'.$_POST['search'].'</b>', $value);
                                $data[$kx]['tables'][] = array(
                                    'tab_name'=>$value,
                                    'alias'=>$alias,
                                    'show'=>true,
                                    'columns'=>array(),
                                );
                            }
                        }


                        foreach ($v['tables'] as $k1 => $v1) {

                            $list_column = array_column($v1['columns'], 'col_name'); //Zend_Debug::dump($list_column);
                            $input = preg_quote((string)$_POST['search'], '~');
                            $tmp_found_in_column = preg_grep('~' . $input . '~', $list_column);

                            if(count($tmp_found_in_column)>0){

                                if(!in_array($v['database_name'], $found_in_database)){
                                    $found_in_database[] = $v['database_name'];
                                    $data[] = array(
                                        'database_name'=>$v['database_name'],
                                        'tables'=>array(),
                                    );
                                }

                                $kx = array_search($v['database_name'], $found_in_database); //Zend_Debug::dump($kx);

                                if(!in_array($v1['tab_name'], $found_in_table[$kx])){
                                    $found_in_table[$kx][] = $v1['tab_name'];
                                    $data[$kx]['tables'][] = array(
                                        'tab_name'=>$v1['tab_name'],
                                        'show'=>true,
                                        'columns'=>array(),
                                    );
                                }

                                $kx1 = array_search($v1['tab_name'], $found_in_table[$kx]); //Zend_Debug::dump($kx1);


                                foreach ($tmp_found_in_column as $key => $value) {

                                    $kx2 = array_search($value, $list_column); //Zend_Debug::dump($kx2);
                                    
                                    $alias = str_replace($_POST['search'], '<b>'.$_POST['search'].'</b>', $value);

                                    $data[$kx]['tables'][$kx1]['columns'][] = array(
                                        'col_name'=>$value,
                                        'alias'=>$alias,
                                        'show'=>true,
                                        'data_type'=>$tmp[$k]['tables'][$k1]['columns'][$kx2]['data_type'],
                                        'comment'=>$tmp[$k]['tables'][$k1]['columns'][$kx2]['comment'],
                                    );
                                }
                            }
                        }
                    }
                }

                $time_end = explode(' ',microtime());
                $duration = number_format ( ($time_end [1] + $time_end [0] - ($time_start [1] + $time_start [0])), 3 );

                $result = array(
                    'transaction'=>true,
                    'time'=>date('Y-m-d H:i:s'),
                    'duration'=>$duration,
                    'message'=>'Success load cache.',
                    'data'=>$data,
                    'cache'=>array(
                        'id'=>$cid,
                        'time'=>$last_cache_time,
                    ),
                );

                header("Access-Control-Allow-Origin: *");
                header('Content-Type: application/json');
                echo json_encode($result);
                die();
            }
        }

        $time_end = explode(' ',microtime());
        $duration = number_format ( ($time_end [1] + $time_end [0] - ($time_start [1] + $time_start [0])), 3 );

        $result = array(
            'transaction'=>false,
            'time'=>date('Y-m-d H:i:s'),
            'duration'=>$duration,
            'message'=>'Invalid parameters.',
            'data'=>false,
        );

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function solrAction(){

    }

}