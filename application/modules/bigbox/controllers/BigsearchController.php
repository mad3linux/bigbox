<?php
/*
 * Copyright (C) bigbox, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by bacanghaneut <rohimfikri@gmail.com>,
*/
class Bigbox_BigsearchController extends Zend_Controller_Action{

public function init() {	
}

public function indexAction(){	
}

public function grafanaAction(){
	$params = $this->getRequest()->getParams();
	// Zend_Debug::dump($params);die();

	$val2 = end($params); //Zend_Debug::dump($val2);
	$val1 = array_search($val2, $params); //Zend_Debug::dump($val1);
	// die();

	$iframeUrl = "http://jt-hdp02i0403.telkom.co.id:3000/dashboard/db/".$val1."-".$val2."?theme=light";

    try {
        $authAdapter = Zend_Auth::getInstance();
        $identity = $authAdapter->getIdentity();
    } catch (Exception $e) {
        
    }
    $this->view->params = $params;
    $this->view->iframeUrl = $iframeUrl;
    $this->view->identity = $identity;
    // Zend_Debug::dump($identity);die();
    $this->view->headScript()->appendScript("
    jQuery(document).ready(function(){
        var ctn = $('.m-grid__item.m-grid__item--fluid.m-wrapper');
        var h = ctn.innerHeight();
        $('iframe').css('height',h-30);
    });
    ");
    $this->renderScript ( 'grafana/index.phtml' );
}

public function getallAction() {
	$this->_helper->layout->disableLayout();
	$params = $this->getRequest()->getParams();
	// Zend_Debug::dump($params);die();
	$params["start"] = (int) $params['page'] * 10;
	// $params["next"] = (int)  + 1;
	$arr = array("lap" => 6,
				 "news" => 2,
				 "socmed" => 5,
				);
	// $params["x"] = $params["type"];
	$s = new Domas_Model_Solranalis();
	$g = new Domas_Model_Pdash();
	$logo = $g->get_logo();
	$z = new Model_Cache();
	$cache = $z->cachefunc(4000);
	$id = $z->get_id("Domas_Model_Solranalis_get_latest_all", $params);
	$data = $z->get_cache($cache, $id);
	if(!$data) {
		$data = $s->get_latest_all($params);
		$cache->save($data, $id, array('systembin'));
	}
	// Zend_Debug::dump($data);die();
	$cc = new CMS_General();
	$c2 = new CMS_LIB_parse();
	$i=1;
	$cx = 0;
	
	$pattern = array (
		'/Mon[^day]/','/Tue[^sday]/','/Wed[^nesday]/','/Thu[^rsday]/',
		'/Fri[^day]/','/Sat[^urday]/','/Sun[^day]/','/Monday/','/Tuesday/',
		'/Wednesday/','/Thursday/','/Friday/','/Saturday/','/Sunday/',
		'/Jan[^uary]/','/Feb[^ruary]/','/Mar[^ch]/','/Apr[^il]/','/May/',
		'/Jun[^e]/','/Jul[^y]/','/Aug[^ust]/','/Sep[^tember]/','/Oct[^ober]/',
		'/Nov[^ember]/','/Dec[^ember]/','/January/','/February/','/March/',
		'/April/','/June/','/July/','/August/','/September/','/October/',
		'/November/','/December/',
	);
	$replace = array ( 'Sen','Sel','Rab','Kam','Jum','Sab','Min',
		'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu',
		'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Okt','Nov','Des',
		'Januari','Februari','Maret','April','Juni','Juli','Agustus','Sepember',
		'Oktober','November','Desember',
	);
	$color = array(0=>"accent",1=>"danger",2=>"success");
	$color2 = array("news"=>"accent","lap"=>"danger","socmed"=>"success");
	foreach($data as $v) {
		$timestamp = strtotime($v["content_date"]);
		$content_date = gmdate("l, j F Y | H:i", $timestamp);
		$content_date = preg_replace ($pattern, $replace, $content_date)." WIB";
		
		$cl = "in";
		if($i%2==0) {
			$cl = "out";
		}
		if($v['entity_id']==9 || $v['entity_id']==1){
			$img = "/images/pdf.png";
			$outtext = '<a href="javascript:;" class="name" target="_blank">'.$v["sm_what"][0].'</a>';
			$outtext .= '<br>'.$c2->remove($v['content'], "<!--", "</style>");
			if($outtext!=""){
				$outtext = str_replace("\n","<br />",$outtext); 
			} else {
				$outtext = str_replace("\n","<br />",$v['content']); 
			}
		}else {
			$outtext = '<a href="'.$v['ss_httpurl'].'" class="name" target="_blank">'.$v['label'].'</a>';
			if($v['entity_id']==3){
				if($v['ss_icon']!=""){
					$img = $cc->get_string_between($v['ss_icon'], "<img src=\"","\" align");
				} else { 
					$img = "/images/pdf.png";
				}
			} else if($v['entity_id']==2){
				$img = $logo[$v['bundle']];
				$outtext = '<a href="'.$v["id"].'" class="name" target="_blank">'.($v["label"]!=""?$v["label"]:$v["id"]).'</a>';
			} else if($v['entity_id']==5){
				if($v['ss_user_profile_imageurl']!=""){
					$img = $v['ss_user_profile_imageurl'];
				} else {
					$img = "/assets/pmas/images/media/twitter.png";
				}
				$outtext = '<a href="https://twitter.com/'.$v["ss_userScreenName"].'/status/'.$v["id"].'" class="name" target="_blank">'.$v["label"].' @'.$v["ss_userScreenName"].'</a>';
			} else {
				$outtext = '<a href="'.$v['ss_httpurl'].'" class="name" target="_blank">'.$v['label'].'</a>';
			}
			$output = preg_replace('/\s{2,}/u', ' ', $v['content']);
			// Zend_Debug::dump($output);//die();
			// Zend_Debug::dump($cc->chop_string($v['content'], 200));//die();
			$outtext .= '<br>'.(($params["x"]=="socmed")?$output:$cc->chop_string($output, 200));
			// Zend_Debug::dump($outtext);//die();
		}
		// die($outtext);
		$data_id = $v["id"];
		$data_entity = $params["x"];
		$cx++;
		echo '<div class="m-widget4__item">
				<div class="m-widget4__img m-widget4__img--pic">
					<img src="'.$img.'" alt="">
				</div>
				<div class="m-widget4__info">
					<span class="m-widget4__title">
						'.$content_date.'
					</span>
					<br>
					<span class="m-widget4__sub">
						'.$outtext.'
					</span>
				</div>
				<div class="m-widget4__ext">
				'.(($v['entity_id']==9 || $v['entity_id']==1 || $v['entity_id']==2)?'
					<a href="#"  class="m-btn m-btn--pill m-btn--hover-accent btn btn-sm btn-secondary" data-toggle="modal" data-toggle="modal" data-id="'.$data_id.'" data-entity="'.$data_entity.'" data-color="danger" data-target="#m_modal_1">
						<i class="fa fa-search"></i>
					</a>':'').'
				</div>
			</div>';
		$i++;
	}
	if(count($data)==10){
		$next = (int) $params['page'] + 1;
		$param = "page=".$next."&skey=".$params["skey"]."&last=".$params["last"];
		echo '<div class="m-widget12__item">
				<button class="btn btn-block btn-metal load" data-entity="'.$data_entity.'" data-param="'.$param.'" onclick="readMore(this)">
					Read More
				</button>
			</div>';
	}
	die();
}

public function searchkeyAction(){
	$params = $this->getRequest()->getParams();
    // Zend_Debug::dump($params);die();
	$params["start"] = 0;
	// $params["next"] = (int) $params['page'] + 1;
	$arr = array(
		"doc" => 1,
		"news" => 2,
		"metadata" => 3,
		"socmed" => 5,
	);

	foreach($params['fk'] as $vz) {
		$nmed[] = $arr[$vz];
	}
	// Zend_Debug::dump($nmed);die();            
	$c3 = new Domas_Model_Manage();
	$c3->logs_keyws(strtolower($params['skey']));
	
	$fil = $nmed;//implode(" OR ", $nmed);
	$params["x"] = $fil;
   // Zend_Debug::dump($params);die();
	$s = new Domas_Model_Solranalis();
	$g = new Domas_Model_Pdash();
	$z = new Model_Cache();
	$cache = $z->cachefunc(4000);
	$id = $z->get_id("Domas_Model_Solranalis_get_latest", $params);
	$data = $z->get_cache($cache, $id);
	// Zend_Debug::dump($data);die();
	$data = false;
	if(!$data) {

        // Zend_Debug::dump($params);die();
		$data = $s->get_latest_all($params);
		$cache->save($data, $id, array('systembin'));
	}
	// Zend_Debug::dump($data);die();
	// Zend_Debug::dump($params);die();
	$this->view->params = $params;
	$this->view->data = $data;
	$this->view->logo = $g->get_logo();
	$this->view->headScript()->appendScript("
		var datasource = 'nasional';
		var Page = function () {
		
			//== Private functions
			var searchkey = function () {
				var xhr_extract=null;
				$('#m_modal_1').on('show.bs.modal', function (e) {
					var a = $(e.relatedTarget);
					//console.log(a.data());
					//console.log(a.data('link'));
					var formData = {}
					var color = a.data('color');
					
					var xhr1 = $.ajax({
						url: '/bigbox/bigsearch/extract',
						data: {'entity':a.data('entity'),'id':a.data('id')},
						content: 'json',
						method: 'post',
						beforeSend: function() {
							$('#head_modal1').removeClass('m--bg-accent m--bg-danger m--bg-success');
							$('#head_modal1').addClass('m--bg-'+color);
							mApp.blockPage({
								overlayColor: '#000000',
								type: 'loader',
								state: 'success',
								message: 'Please wait...'
							});
						},
						complete: function(result) {
							mApp.unblockPage();
						},
						success: function(data) {
							$('#title_modal1').html(data.title);
							if($('#scroller_modal1').hasClass('mCustomScrollbar')){
								$('#scroller_modal1').mCustomScrollbar('destroy');
							}
							$('#scroller_modal1').html(data.html);
						},
						error: function() {
							if($('#scroller_modal1').hasClass('mCustomScrollbar')){
								$('#scroller_modal1').mCustomScrollbar('destroy');
							}
							$('#scroller_modal1').html('No Data Available');
						},
						async : false
					});
					
					$('#scroller_modal1').mCustomScrollbar({
						alwaysShowScrollbar:true,
						autoScrollOnFocus : false,
						updateOnContentResize : false,
						updateOnImageLoad : false,
						setWidth : '100%',
						setHeight : 450,
						theme:'minimal-dark'
					});
				});
				
				$('#m_modal_1').on('hidden.bs.modal', function (e) {
					$('#title_modal1').html('');
					if($('#scroller_modal1').hasClass('mCustomScrollbar')){
						$('#scroller_modal1').mCustomScrollbar('destroy');
					}
					$('#scroller_modal1').html('No Data Available');
					$('#head_modal1').removeClass('m--bg-accent m--bg-danger m--bg-success');
				});}

			return {
				// public functions
				init: function() {
					searchkey(); 
				}
			};
		}();

		jQuery(document).ready(function() {  
			$('a[href$=\"#\"]').on('click',function(e){
				e.preventDefault();
			});
			Page.init();
		});
	");
}

public function dashboardAction() {
	$this->view->headScript()->appendFile('/assets/core/global/plugins/counterup/jquery.waypoints.min.js');
	$this->view->headScript()->appendFile('/assets/core/global/plugins/counterup/jquery.counterup.min.js');
	$this->view->headScript()->appendFile('/assets/core/global/plugins/amcharts/amcharts/amcharts.js');
    $this->view->headScript()->appendFile('/assets/core/global/plugins/amcharts/amcharts/serial.js');
    $this->view->headScript()->appendFile('/assets/core/global/plugins/echarts/echarts.js');

	$z = new Model_Cache();
	$mdlSolr = new Domas_Model_Solr();

    $prosDM = $mdlSolr->prosentaseDigitalMedia();
    // Zend_Debug::dump($prosDM);die();

    $legendData = array_keys($prosDM['facet_counts']['facet_fields']['bundle']);

    $starData = array(
        array(
            'name'=>"Max",
            'value'=>$prosDM['response']['numFound'],
            'x'=>"80%",
            'y'=>50,
            'symbolSize'=>32
        ),        
    );

    $pieData1 = array(); $pieData2 = array(); $pieData3 = array();
    foreach ($prosDM['facet_counts']['facet_fields']['bundle'] as $bundle => $value) {
        if($value>=10000){
            $pieData1[] = array(
                'name'=>$bundle,
                'value'=>$value,
            );
        } else if($value>=1000 && $value<10000){
            $pieData2[] = array(
                'name'=>$bundle,
                'value'=>$value,
            );
        } else {
            $pieData3[] = array(
                'name'=>$bundle,
                'value'=>$value,
            );
        }
    }    
    
    $enddate = date("Y-m-d");
    $startdate = date("Y-m-d", strtotime($enddate." -30 days"));
    $p = array("startdate"=>$startdate, "enddate"=>$enddate,);

    $this->view->start= $startdate;
	$this->view->end= $enddate;
	$cache = $z->cachefunc(3000);
	$id = $z->get_id("Domas_Solr_HomeDashboard", $p);
	$data = $z->get_cache($cache, $id);
    // Zend_Debug::dump($data);die();
	$data = false;
	if(!$data) {

        // Zend_Debug::dump($p);die();
		// die("s");
		$entity = $mdlSolr->get_entity_desc();

		$count = $mdlSolr->get_count_by_entity();
		// Zend_Debug::dump($count);//die("S");

		$sentiment = $mdlSolr->get_count_ranges_sentiment($p);
		// Zend_Debug::dump($sentiment);die();
		
		$data = array(
			"params"=>$p,
			"entity"=>$entity,
			"count"=>$count,
			"ranges"=>$sentiment,
		);
	   // Zend_Debug::dump($data); //die();
		$cache->save($data, $id, array('systembin'));
	}

	// Zend_Debug::dump($data);//die();

	$this->view->data = $data;
	
	$this->view->headScript()->appendScript('
	jQuery(document).ready(function(){
		$(\'a[href$="#"]\').on(\'click\',function(e){
			e.preventDefault();
		});
		
		if (typeof(AmCharts) === "undefined") {
	        return;
	    }

        var chartData_2 = '.json_encode($data["ranges"][2]).';
        var chart = AmCharts.makeChart("chart_entity_2", {
            type: "serial",
            fontSize: 12,
            fontFamily: "Open Sans",
            dataDateFormat: "YYYY-MM-DD",
            dataProvider: chartData_2,

            addClassNames: true,
            startDuration: 1,
            color: "#6c7b88",
            marginLeft: 0,

            categoryField: "date",
            categoryAxis: {
                parseDates: true,
            },

            valueAxes: [{
                id: "a1",
                title: "NETRAL",
                gridAlpha: 0,
                axisAlpha: 0,
            }, {
                id: "a2",
                title: "POSITIF/NETRAL",
                position: "right",
                gridAlpha: 0,
                axisAlpha: 0
            },],
            graphs: [{
                id: "g1",
                valueField: "netral",
                title: "NETRAL",
                type: "column",
                fillAlphas: 0.7,
                valueAxis: "a1",
                balloonText: "[[value]]",
                legendValueText: "[[value]]",
                legendPeriodValueText: "[[value.sum]]",
                lineColor: "#08a3cc",
                alphaField: "alpha",
            }, {
                id: "g2",
                valueField: "positif",
                classNameField: "bulletClass",
                title: "POSITIF",
                type: "line",
                valueAxis: "a2",
                lineColor: "#36D7B7",
                lineThickness: 1,
                legendValueText: "[[value]]",
                bullet: "round",
                bulletBorderColor: "#26C281",
                bulletBorderAlpha: 1,
                bulletBorderThickness: 2,
                bulletColor: "#36D7B7",
                labelPosition: "right",
                balloonText: "POSITIF:[[value]]",
                legendPeriodValueText: "[[value.sum]]",
                showBalloon: true,
                animationPlayed: true,
            }, {
                id: "g3",
                title: "NEGATIF",
                valueField: "negatif",
                classNameField: "bulletClass",
                type: "line",
                valueAxis: "a2",
                lineAlpha: 0.8,
                lineColor: "#e26a6a",
                balloonText: "[[value]]",
                lineThickness: 1,
                legendValueText: "[[value]]",
                legendPeriodValueText: "[[value.sum]]",
                bullet: "square",
                bulletBorderColor: "#e26a6a",
                bulletBorderThickness: 1,
                bulletBorderAlpha: 0.8,
                dashLengthField: "dashLength",
                animationPlayed: true
            }],


          	chartScrollbar: {},

            chartCursor: {
                valueLineEnabled: true,
                valueLineBalloonEnabled: false,
                valueLineAlpha: 0.5,
                fullWidth: true,
                cursorAlpha: 0.05
            },
            legend: {
                bulletType: "round",
                equalWidths: false,
                valueWidth: 120,
                useGraphSettings: true,
                color: "#6c7b88"
            }
        });

        var chartData_3 = '.json_encode($data["ranges"][3]).';
        var chart = AmCharts.makeChart("chart_entity_3", {
            type: "serial",
            fontSize: 12,
            fontFamily: "Open Sans",
            dataDateFormat: "YYYY-MM-DD",
            dataProvider: chartData_3,

            addClassNames: true,
            startDuration: 1,
            color: "#6c7b88",
            marginLeft: 0,

            categoryField: "date",
            categoryAxis: {
                parseDates: true,
            },

            valueAxes: [{
                id: "a1",
                title: "NETRAL",
                gridAlpha: 0,
                axisAlpha: 0,
            }, {
                id: "a2",
                title: "POSITIF/NETRAL",
                position: "right",
                gridAlpha: 0,
                axisAlpha: 0
            },],
            graphs: [{
                id: "g1",
                valueField: "netral",
                title: "NETRAL",
                type: "column",
                fillAlphas: 0.7,
                valueAxis: "a1",
                balloonText: "[[value]]",
                legendValueText: "[[value]]",
                legendPeriodValueText: "[[value.sum]]",
                lineColor: "#08a3cc",
                alphaField: "alpha",
            }, {
                id: "g2",
                valueField: "positif",
                classNameField: "bulletClass",
                title: "POSITIF",
                type: "line",
                valueAxis: "a2",
                lineColor: "#36D7B7",
                lineThickness: 1,
                legendValueText: "[[value]]",
                bullet: "round",
                bulletBorderColor: "#26C281",
                bulletBorderAlpha: 1,
                bulletBorderThickness: 2,
                bulletColor: "#36D7B7",
                labelPosition: "right",
                balloonText: "POSITIF:[[value]]",
                legendPeriodValueText: "[[value.sum]]",
                showBalloon: true,
                animationPlayed: true,
            }, {
                id: "g3",
                title: "NEGATIF",
                valueField: "negatif",
                type: "line",
                valueAxis: "a2",
                lineAlpha: 0.8,
                lineColor: "#e26a6a",
                balloonText: "[[value]]",
                lineThickness: 1,
                legendValueText: "[[value]]",
                legendPeriodValueText: "[[value.sum]]",
                bullet: "square",
                bulletBorderColor: "#e26a6a",
                bulletBorderThickness: 1,
                bulletBorderAlpha: 0.8,
                dashLengthField: "dashLength",
                animationPlayed: true
            }],


          	chartScrollbar: {},

            chartCursor: {
                valueLineEnabled: true,
                valueLineBalloonEnabled: false,
                valueLineAlpha: 0.5,
                fullWidth: true,
                cursorAlpha: 0.05
            },
            legend: {
                bulletType: "round",
                equalWidths: false,
                valueWidth: 120,
                useGraphSettings: true,
                color: "#6c7b88"
            }
        });

        var chartData_5 = '.json_encode($data["ranges"][5]).';
        var chart = AmCharts.makeChart("chart_entity_5", {
            type: "serial",
            fontSize: 12,
            fontFamily: "Open Sans",
            dataDateFormat: "YYYY-MM-DD",
            dataProvider: chartData_5,

            addClassNames: true,
            startDuration: 1,
            color: "#6c7b88",
            marginLeft: 0,

            categoryField: "date",
            categoryAxis: {
                parseDates: true,
            },

            valueAxes: [{
                id: "a1",
                title: "NETRAL",
                gridAlpha: 0,
                axisAlpha: 0,
            }, {
                id: "a2",
                title: "POSITIF/NETRAL",
                position: "right",
                gridAlpha: 0,
                axisAlpha: 0
            },],
            graphs: [{
                id: "g1",
                valueField: "netral",
                title: "NETRAL",
                type: "column",
                fillAlphas: 0.7,
                valueAxis: "a1",
                balloonText: "[[value]]",
                legendValueText: "[[value]]",
                legendPeriodValueText: "[[value.sum]]",
                lineColor: "#08a3cc",
                alphaField: "alpha",
            }, {
                id: "g2",
                valueField: "positif",
                classNameField: "bulletClass",
                title: "POSITIF",
                type: "line",
                valueAxis: "a2",
                lineColor: "#36D7B7",
                lineThickness: 1,
                legendValueText: "[[value]]",
                bullet: "round",
                bulletBorderColor: "#26C281",
                bulletBorderAlpha: 1,
                bulletBorderThickness: 2,
                bulletColor: "#36D7B7",
                labelPosition: "right",
                balloonText: "POSITIF:[[value]]",
                legendPeriodValueText: "[[value.sum]]",
                showBalloon: true,
                animationPlayed: true,
            }, {
                id: "g3",
                title: "NEGATIF",
                valueField: "negatif",
                type: "line",
                valueAxis: "a2",
                lineAlpha: 0.8,
                lineColor: "#e26a6a",
                balloonText: "[[value]]",
                lineThickness: 1,
                legendValueText: "[[value]]",
                legendPeriodValueText: "[[value.sum]]",
                bullet: "square",
                bulletBorderColor: "#e26a6a",
                bulletBorderThickness: 1,
                bulletBorderAlpha: 0.8,
                dashLengthField: "dashLength",
                animationPlayed: true
            }],


          	chartScrollbar: {},

            chartCursor: {
                valueLineEnabled: true,
                valueLineBalloonEnabled: false,
                valueLineAlpha: 0.5,
                fullWidth: true,
                cursorAlpha: 0.05
            },
            legend: {
                bulletType: "round",
                equalWidths: false,
                valueWidth: 120,
                useGraphSettings: true,
                color: "#6c7b88"
            }
        });

        var chartData_1 = '.json_encode($data["ranges"][1]).';
        var chart = AmCharts.makeChart("chart_entity_1", {
            type: "serial",
            fontSize: 12,
            fontFamily: "Open Sans",
            dataDateFormat: "YYYY-MM-DD",
            dataProvider: chartData_1,

            addClassNames: true,
            startDuration: 1,
            color: "#6c7b88",
            marginLeft: 0,

            categoryField: "date",
            categoryAxis: {
                parseDates: true,
            },

            valueAxes: [{
                id: "a1",
                title: "NETRAL",
                gridAlpha: 0,
                axisAlpha: 0,
            }, {
                id: "a2",
                title: "POSITIF/NETRAL",
                position: "right",
                gridAlpha: 0,
                axisAlpha: 0
            },],
            graphs: [{
                id: "g1",
                valueField: "netral",
                title: "NETRAL",
                type: "column",
                fillAlphas: 0.7,
                valueAxis: "a1",
                balloonText: "[[value]]",
                legendValueText: "[[value]]",
                legendPeriodValueText: "[[value.sum]]",
                lineColor: "#08a3cc",
                alphaField: "alpha",
            }, {
                id: "g2",
                valueField: "positif",
                classNameField: "bulletClass",
                title: "POSITIF",
                type: "line",
                valueAxis: "a2",
                lineColor: "#36D7B7",
                lineThickness: 1,
                legendValueText: "[[value]]",
                bullet: "round",
                bulletBorderColor: "#26C281",
                bulletBorderAlpha: 1,
                bulletBorderThickness: 2,
                bulletColor: "#36D7B7",
                labelPosition: "right",
                balloonText: "POSITIF:[[value]]",
                legendPeriodValueText: "[[value.sum]]",
                showBalloon: true,
                animationPlayed: true,
            }, {
                id: "g3",
                title: "NEGATIF",
                valueField: "negatif",
                type: "line",
                valueAxis: "a2",
                lineAlpha: 0.8,
                lineColor: "#e26a6a",
                balloonText: "[[value]]",
                lineThickness: 1,
                legendValueText: "[[value]]",
                legendPeriodValueText: "[[value.sum]]",
                bullet: "square",
                bulletBorderColor: "#e26a6a",
                bulletBorderThickness: 1,
                bulletBorderAlpha: 0.8,
                dashLengthField: "dashLength",
                animationPlayed: true
            }],


          	chartScrollbar: {},

            chartCursor: {
                valueLineEnabled: true,
                valueLineBalloonEnabled: false,
                valueLineAlpha: 0.5,
                fullWidth: true,
                cursorAlpha: 0.05
            },
            legend: {
                bulletType: "round",
                equalWidths: false,
                valueWidth: 120,
                useGraphSettings: true,
                color: "#6c7b88"
            }
        });


        // ECHARTS
        require.config({
            paths: {
                echarts: "/assets/core/global/plugins/echarts/"
            }
        });
        require(
            [
                "echarts",
                "echarts/chart/pie",
            ],
            function(ec) {

                // -- PIE --
                var myChart5 = ec.init(document.getElementById("echarts_pie"));
                myChart5.setOption({
                    tooltip: {
                        show: true,
                        formatter: "{a} <br/>{b} : {c} ({d}%)"
                    },
                    legend: {
                        orient: "vertical",
                        x: "bottom",
                        data: '.json_encode($legendData).'
                    },
                    toolbox: {
                        show: true,
                        feature: {
                            mark: {
                                show: true
                            },
                            dataView: {
                                show: true,
                                readOnly: false
                            },
                            restore: {
                                show: true
                            },
                            saveAsImage: {
                                show: true
                            }
                        }
                    },
                    calculable: true,
                    series: [{
                        name: "Source",
                        type: "pie",
                        center: ["35%", 200],
                        radius: 80,
                        itemStyle: {
                            normal: {
                                label: {
                                    position: "inner",
                                    formatter: function(params) {
                                        return (params.percent - 0).toFixed(0) + "%"
                                    }
                                },
                                labelLine: {
                                    show: false
                                }
                            },
                            emphasis: {
                                label: {
                                    show: true,
                                    formatter: "{b}\n{d}%"
                                }
                            }

                        },
                        data: '.json_encode($pieData1).'
                    }, {
                        name: "Source",
                        type: "pie",
                        center: ["35%", 200],
                        radius: [110, 140],
                        data: '.json_encode($pieData2).'
                    }, {
                        name: "Source",
                        type: "pie",
                        clockWise: true,
                        startAngle: 135,
                        center: ["75%", 200],
                        radius: [80, 120],
                        itemStyle: {
                            normal: {
                                label: {
                                    show: false
                                },
                                labelLine: {
                                    show: false
                                }
                            },
                            emphasis: {
                                color: (function() {
                                    var zrColor = require("zrender/tool/color");
                                    return zrColor.getRadialGradient(
                                        650, 200, 80, 650, 200, 120, [
                                            [0, "rgba(255,255,0,1)"],
                                            [1, "rgba(255,0,0,1)"]
                                        ]
                                    )
                                })(),
                                label: {
                                    show: true,
                                    position: "center",
                                    formatter: "{d}%",
                                    textStyle: {
                                        color: "red",
                                        fontSize: "30",
                                        fontFamily: "Open Sans",
                                        fontWeight: "bold"
                                    }
                                }
                            }
                        },
                        data: '.json_encode($pieData3).',
                        markPoint: {
                            symbol: "star",
                            data: '.json_encode($starData).'
                        }
                    }]
                });
            }
        );
	});
	');
}

public function extractAction() {
	$this->_helper->layout->disableLayout();
	$params = $this->getRequest()->getParams();
   //  Zend_Debug::dump($params);die();
	$n = new Domas_Model_Withsolrdash();
	$id = 'id:"' . $params['id'] . '"';
	$filter = array($id);
	$tid = array();
	// Zend_Debug::dump($filter);die();
	if($params["entity"] == "lap") {
		// $data = $n->get_news_bin_fact("", 0, 1,3, $tid,$filter);
		// $label = $data[0]["title"];
		// // $content = str_replace('\n','<br/>',$data[0]["doc_tx"]);
		// $content = $data[0]["doc_tx"];
		$data = $n->get_news_bin("", 0, 1, 3, $tid, $filter);
	   // Zend_Debug::dump($data);die();
		 $label =($data[0]["label"] != "" ? $data[0]["label"] : $data[0]["id"]);
		$content = str_replace('\n', '<br/>', $data[0]["content"]);
	} else if($params["entity"] == "1"){
		
		  $data = $n->get_news_bin("", 0, 1, 1, $tid, $filter);
	   // Zend_Debug::dump($data);die();
		 $label =($data[0]["label"] != "" ? $data[0]["label"] : $data[0]["id"]);
		$content = str_replace('\n', '<br/>', $data[0]["content"]);
		
		
	}else {

		// Zend_Debug::dump($_POST);die();
		$entity_id = $_POST["entity"];
	   
		$data = $n->get_news_bin("", 0, 1, $entity_id, $tid, $filter);
		// Zend_Debug::dump($data);die("xxx");
		$label =($data[0]["label"] != "" ? $data[0]["label"] : $data[0]["id"]);
		$content = str_replace('\n', '<br/>', $data[0]["content"]);
	}
   // Zend_Debug::dump($data); die();
	$content .= "<br><span style='color:white;'>Tags : ".implode(" | ", $data[0]["taxonomy_names"] )."</span>";
	$content_date = $data[0]["content_date"];
	
	
	//Zend_Debug::dump($data);die();
	// echo $content;die();
	// echo "<pre>".$content."</pre>";
	// Zend_Debug::dump($content);die();
	// Zend_Debug::dump($label);die();
	if($label == null) {
		$label = " ";
	}
	// Zend_Debug::dump($label);die();
	$timestamp = strtotime($content_date);
	$pattern = array (
		'/Mon[^day]/','/Tue[^sday]/','/Wed[^nesday]/','/Thu[^rsday]/',
		'/Fri[^day]/','/Sat[^urday]/','/Sun[^day]/','/Monday/','/Tuesday/',
		'/Wednesday/','/Thursday/','/Friday/','/Saturday/','/Sunday/',
		'/Jan[^uary]/','/Feb[^ruary]/','/Mar[^ch]/','/Apr[^il]/','/May/',
		'/Jun[^e]/','/Jul[^y]/','/Aug[^ust]/','/Sep[^tember]/','/Oct[^ober]/',
		'/Nov[^ember]/','/Dec[^ember]/','/January/','/February/','/March/',
		'/April/','/June/','/July/','/August/','/September/','/October/',
		'/November/','/December/',
	);
	$replace = array ( 'Sen','Sel','Rab','Kam','Jum','Sab','Min',
		'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu',
		'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Okt','Nov','Des',
		'Januari','Februari','Maret','April','Juni','Juli','Agustus','Sepember',
		'Oktober','November','Desember',
	);
	$content_date = date ("l, j F Y | H:i", $timestamp);
	$content_date = preg_replace ($pattern, $replace, $content_date);
	
	$c2 = new CMS_LIB_parse();
	$outtext = $c2->remove($content, "<!--", "</style>");
	if($outtext!=""){
		$outtext = trim(str_replace("\n","<br />",$outtext)); 
	} else {
		$outtext = trim(str_replace("\n","<br />",$content)); 
	}
	
	foreach ($this->rawdata[0] as $v=>$valu) {
		if(substr($v, 0, 3)=='ss_' ||substr($v, 0, 3)=='is_'||substr($v, 0, 3)=='ts_') {
		  $outtext.= "<br><span style='color:white;'>".substr($v,3)." : ".$valu."</span>";
		}
	}
	
	
	header("Access-Control-Allow-Origin: *");
	header('Content-Type: application/json');
	echo json_encode(array('title'=>$label,'date'=>$content_date,'html'=>$outtext));
	die();
}

public function latestAction() {
	$this->_helper->layout->disableLayout();
	$params = $this->getRequest()->getParams();
	// Zend_Debug::dump($params);die();
	$params["start"] = 0;
	// $params["next"] = (int) $params['page'] + 1;
	$arr = array("lap" => 3,"news" => 2,"socmed" => 5);
	$x = $arr[$params["l"]];
	$params["x"] = $x;
	$s = new Domas_Model_Solranalis();
	$g = new Domas_Model_Pdash();
	$z = new Model_Cache();
	$cache = $z->cachefunc(4000);
	$id = $z->get_id("Domas_Model_Solranalis_get_latest", $params);
	$data = $z->get_cache($cache, $id);
	// Zend_Debug::dump($data);die();
	$data = false;
	if(!$data) {
		$data = $s->get_latest($params);
		// Zend_Debug::dump($data);die();
		$cache->save($data, $id, array('systembin'));
	}
	// Zend_Debug::dump($data);die();
	$this->view->params = $params;
	$this->view->data = $data;
	$this->view->logo = $g->get_logo();
}

public function discoveryAction() {
	$params = $this->getRequest()->getParams();
	//Zend_Debug::dump($params); die();
	if(!isset($params["startdate"])) {
		// $params["startdate"] = date("Y-m-d", strtotime('monday last week'));
		$params["startdate"] = date("Y-m-d");
	}
	if(!isset($params["enddate"])) {
		$params["enddate"] = date("Y-m-d");
	}
	$arr_map = array("lat" => - 1.131695,
					 "lng" => 119.503078,
					 "zoom" => 5,
					);
	if(isset($params["media_type"])&& $params["media_type"] == "internasional") {
		$arr_map = array(
			"lat" => 26.4578763,
			 "lng" => 32.0579421,
			 "zoom" => 1,
		);
	}
	// Zend_Debug::dump($arr_map);//die();
	// $as = new Domas_Model_General();
	//koneksi ke oracle
	// $data_alertfr = $as->alertfr();  
	$data = array();
	$cc = new Domas_Model_Pdash();
	$dd = new Model_Cache();
	$cache = $dd->cachefunc(3000);
	$id = $dd->get_id("Domas_Model_Pdash_get_tags_pmas");
	$dash = $dd->get_cache($cache, $id);
	if(!$dash) {
		$dash = $cc->get_tags_pmas();
		$cache->save($dash, $id, array('systembin'));
	}
	//Zend_Debug::dump($dash);die();
	$this->view->cat = $dash;
	$this->view->varams = $params;
	$this->view->map = $arr_map;
	// $this->view->alertfr = $data_alertfr;
	
	$this->view->headLink()->appendStylesheet("/assets/core/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css");
	$this->view->headScript()->appendFile("/assets/core/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js");
	$this->view->headScript()->appendFile('/assets/core/plugins/highcharts4.0.4/js/highcharts.js');
	$this->view->headScript()->appendFile('/assets/core/plugins/highcharts4.0.4/js/highcharts-3d.js');
	$this->view->headScript()->appendFile('/assets/core/plugins/highcharts4.0.4/js/themes/dark-unica.js');
	$this->view->headScript()->appendFile('/assets/core/plugins/highcharts4.0.4/js/modules/exporting.js');
    $this->view->headScript()->appendFile('/assets/core/js/d3.v3.min.js');
	$this->view->headScript()->appendScript("
		var datasource = 'nasional';
		var Page = function () {
		
			//== Private functions
			var discovery = function () {
				var txt = $('.m_autosize');
				autosize(txt);
				
				// console.log(moment.locale());
				var _start = moment().subtract(1, 'days');
				var _end = moment();
				//console.log(_start);
				//console.log(_end);
				$('#m_daterangepicker_1').daterangepicker({
					buttonClasses: 'm-btn btn',
					applyClass: 'btn-primary',
					cancelClass: 'btn-secondary',
					startDate: _start,
					endDate: _end,
					maxDate : moment(),
					ranges: {
					   'Today': [moment(), moment()],
					   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
					   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
					   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
					   'This Month': [moment().startOf('month'), moment().endOf('month')],
					   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
					}
				}, function(start, end, label) {
					$('#startdate').val(start.format('YYYY-MM-DD'));
					$('#enddate').val(end.format('YYYY-MM-DD'));
					$('#m_daterangepicker_1 .form-control').val( start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
				});
				
				$('.tags_input').tagsinput({
				  trimValue: true,
				  tagClass: 'filtertag'
				});
				$('.m-widget4__item').remove();
				
				$('#formFilter').on('submit', function(){
					mApp.blockPage({
						overlayColor: '#000000',
						type: 'loader',
						state: 'success',
						message: 'Please wait...'
					});
				});
				
				$('#formFilter').on('keyup keypress', function(e) {
					var keyCode = e.keyCode || e.which;
					if (keyCode === 13) { 
						e.preventDefault();
						return false;
					}
				});
				
				
				$('.m_selectpicker').selectpicker();
				
				".((isset($this->varams["catsearch"])&& $this->varams["catsearch"] != "")?"var cat = '".$this->varams["catsearch"]."'":"var cat =''").";
				if(cat!=undefined && cat!=null){
					getTopic(cat);
				}
				
				".((isset($this->varams["datasource"])&& $this->varams["datasource"] != "")?"datasource = '".$this->varams["datasource"]."'":"datasource ='nasional'").";
				if(datasource!=undefined && datasource!=null){
					changeSource(datasource)
				}
				
				getSentiment('','','News');
				getTrendline();
				getLink();
											
				getLatest('lap','','',1);
				getLatest('news','','',1);
				getLatest('socmed','','',1);
				
				var portlet_sentiment = $('#portlet_sentiment').mPortlet();
				// console.log(portlet_sentiment);
				
				portlet_sentiment.on('reload', function(portlet) {
					console.log(portlet);
					getSentiment('','',$('#sentiment_select').val());
					return false;
				});
				
				var xhr_extract=null;
				$('#m_modal_1').on('show.bs.modal', function (e) {
					var a = $(e.relatedTarget);
					//console.log(a.data());
					//console.log(a.data('link'));
					var formData = {}
					var param = '';
					param +='&entity='+a.data('entity');
					param +='&id='+a.data('id');
					var color = a.data('color');
					
					var xhr1 = $.ajax({
						url: '/bigbox/bigsearch/extract?'+param,
						data: formData,
						content: 'json',
						method: 'post',
						beforeSend: function() {
							$('#head_modal1').removeClass('m--bg-accent m--bg-danger m--bg-success');
							$('#head_modal1').addClass('m--bg-'+color);
							mApp.blockPage({
								overlayColor: '#000000',
								type: 'loader',
								state: 'success',
								message: 'Please wait...'
							});
						},
						complete: function(result) {
							mApp.unblockPage();
						},
						success: function(data) {
							$('#title_modal1').html(data.title);
							if($('#scroller_modal1').hasClass('mCustomScrollbar')){
								$('#scroller_modal1').mCustomScrollbar('destroy');
							}
							$('#scroller_modal1').html(data.html);
						},
						error: function() {
							if($('#scroller_modal1').hasClass('mCustomScrollbar')){
								$('#scroller_modal1').mCustomScrollbar('destroy');
							}
							$('#scroller_modal1').html('No Data Available');
						},
						async : false
					});
					
					$('#scroller_modal1').mCustomScrollbar({
						alwaysShowScrollbar:true,
						autoScrollOnFocus : false,
						updateOnContentResize : false,
						updateOnImageLoad : false,
						setWidth : '100%',
						setHeight : 450,
						theme:'minimal-dark'
					});
				});
				
				$('#m_modal_1').on('hidden.bs.modal', function (e) {
					$('#title_modal1').html('');
					if($('#scroller_modal1').hasClass('mCustomScrollbar')){
						$('#scroller_modal1').mCustomScrollbar('destroy');
					}
					$('#scroller_modal1').html('No Data Available');
					$('#head_modal1').removeClass('m--bg-accent m--bg-danger m--bg-success');
				});
			}

			return {
				// public functions
				init: function() {
					discovery(); 
				}
			};
		}();

		jQuery(document).ready(function() {  
			$('a[href$=\"#\"]').on('click',function(e){
				e.preventDefault();
			});
			Page.init();
		});
	");
	if(!isset($params["startdate"])) {
		$params["startdate"] = date("Y-m-d", strtotime('yesterday'));
	}
	if(!isset($params["enddate"])) {
		$params["enddate"] = date("Y-m-d");
	}

	$this->view->params = $params;
}

public function advdiscoveryAction() {
	$params = $this->getRequest()->getParams();

    // $startdate = $_POST['startdate'];
    if(!isset($params['startdate']) || $params['startdate']==''){
        $params['startdate'] = date("Y-m-d", strtotime('yesterday'));
    }
    // $enddate = $_POST['enddate'];
    if(!isset($_POST['enddate']) || $_POST['enddate']==''){
        $params['enddate'] = date("Y-m-d");
    }

    $g = new Domas_Model_Pdash();
    $z = new Model_Cache();
	$mdlSolr = new Domas_Model_Solr();
	$n = new Domas_Model_Withsolrpmas();

	$datasource = array(
		"2"=>"DIGITAL MEDIA",
		"3"=>"DOCUMENT",
		"4"=>"MESSAGING",
		"5"=>"SOCIAL MEDIA TWITTER",
		"6"=>"OTHERS",
	);
	$sentiment = array(
		"netral",
		"negatif",
		"positif",
	);

	$andsearch = array();
	if($params["andsearch"]!="" && $params["andsearch"]!=null){
		$andsearch = explode(",", $params["andsearch"]);
	}
		
	if($params["exsearch"]!="" && $params["exsearch"]!=null){
		$excsearch = explode(",", $params["exsearch"]);
		// Zend_Debug::dump($keysearch);die();
		$exception = array();
		foreach($excsearch as $k=>$v){
			$vv = explode("<>",$v);
			// Zend_Debug::dump($vv);die();
			$exception[$vv[0]][] = $vv[1];
		}
	}
		// Zend_Debug::dump($exception);die();

    $jsdata = array();
	if($params["keysearch"]!="" &&$params["keysearch"]!=null){
		$keysearch = explode(",", $params["keysearch"]);
		$andsearch = array();
		if($params["andsearch"]!=""){				
			$andsearch = explode(",", $params["andsearch"]);
		}
		$exsearch = array();
		if($params["exsearch"]!=""){				
			$exsearch = explode(",", $params["exsearch"]);
		}

		$data = array();
		foreach($datasource as $entity_id=>$entity_name){   	
			$jsdata[$entity_id] = array();
            $tmpArr = array();
			foreach ($keysearch as $k => $key) {

                $p = array(
                    'entity_id'=>$entity_id,
                    'startdate'=>$_POST['startdate'],
                    'enddate'=>$_POST['enddate'],
                    'searchkey'=>$key,
                    'andsearch'=>$andsearch,
                    'exsearch'=>$exsearch,
                );
        
                $cache = $z->cachefunc(3000);
                $id = $z->get_id("Domas_Model_Solr_prosentaseDigitalMedia",$p);
                $tmp = $z->get_cache($cache, $id);
                if(!$tmp) {
                    $tmp = $mdlSolr->prosentaseDigitalMedia($p);
                    $cache->save($tmp, $id, array('systembin'));
                }

                // Zend_Debug::dump($tmp);die();
                $tmpArr[] = $tmp;
				$data[$entity_id]["entity"][$key]["count"] = $tmp["response"]["numFound"];
				$jsdata[$entity_id]["key"][] = array(
                    'name'=>$key,
                    'value'=>(int)$tmp["response"]["numFound"],
                );
			}

			// Zend_Debug::dump($entity_id);//die();
			// Zend_Debug::dump($tmpArr);die();

            $bundle = array();
            foreach ($tmpArr as $k => $v) {
                foreach ($v['facet_counts']['facet_fields']['bundle'] as $k1 => $v1) {
                    if($v1>0){                        
                        if(!in_array($k1, $bundle)){
                            $jsdata[$entity_id]["bundle"][] = array(
                                'name'=>$k1,
                                'value'=>0
                            );
                            $bundle[] = $k1;
                        }

                        $name = array_column($jsdata[$entity_id]["bundle"], 'name');
                        $key = array_search($k1, $name);

                        $jsdata[$entity_id]["bundle"][$key]['value'] += $v1;
                    }
                }
            }

            // Zend_Debug::dump($jsdata);die();
            // $jsdata[$entity_id]["bundle"][$i]["value"] = $v;
            // $jsdata[$entity_id]["bundle"][$i]["name"] = $tmp["facet_counts"]["facet_fields"]["bundle"][$k-1];
            // $data[$entity_id]["bundle"][$tmp["facet_counts"]["facet_fields"]["bundle"][$k-1]] = $v;

			// $jsdata[$entity_id]["sentiment"] = array();
			$i = 0;
			foreach ($sentiment as $sentiment_id => $sentiment_name) {

				$and = "";
				$and .= "&fq=content%3A%5B*+TO+*%5D";
				$and .= "&fq=content_date%3A%5B".$params["startdate"]."T00%3A00%3A00Z+TO+".$params["enddate"]."T23%3A59%3A59Z%5D";
				$and .= "&fq=entity_id%3A".$entity_id;
				$and .= "&fq=content%3A%22".urlencode(implode('" OR "',$keysearch))."%22";
				$and .= "&fq=is_sentiment%3A".$sentiment_id;
				$and .= '&fq=is_mediatype%3A1+OR+2';
				if(count($andsearch)>0){
					foreach($andsearch as $x=>$y){
						$and .='&fq=content%3A%22'.urlencode($y).'%22';
					}
				}
				if(count($exsearch)>0){
					foreach($exsearch as $x=>$y){
						$and .='&fq=-content%3A%22'.urlencode($y).'%22';
					}
				}

				$query = "select?q=*%3A*".$and."&rows=0&wt=json&indent=true&facet=true";
				$query2 = "select?q=*%3A*".$and."&rows=10&wt=json&indent=true";

				$tmp = $n->get_by_rawurl($query);
				$tmp2 = $n->get_by_rawurl($query2);
				// Zend_Debug::dump($tmp);die();

				if($tmp["response"]["numFound"]>0){
					$data[$entity_id]["sentiment"][$sentiment_name]["count"] = $tmp2["response"]["numFound"];
					$data[$entity_id]["sentiment"][$sentiment_name]["data"] = $tmp2["response"]["docs"];
					$jsdata[$entity_id]["sentiment"][$i]["value"] = (int)$tmp["response"]["numFound"];
					$jsdata[$entity_id]["sentiment"][$i]["name"] = $sentiment_name;
					$jsdata[$entity_id]["sentiment"][$i]["color"] = "#000000";
					$i++;
				}
			}

		}

		// Zend_Debug::dump($jsdata[2]["sentiment"]);die();
	} else {

        $startdate = date("Y-m-d", strtotime('yesterday'));
        if(isset($_POST['startdate']) && $_POST['startdate']!=''){
            $startdate = $_POST['startdate'];
        }
        $enddate = date("Y-m-d");
        if(isset($_POST['enddate']) && $_POST['enddate']!=''){
            $enddate = $_POST['enddate'];
        }

        $p = array(
        'entity_id'=>2,
        'startdate'=>$startdate,
        'enddate'=>$enddate,
        );
        $prosDM = $mdlSolr->prosentaseDigitalMedia($p);
        // Zend_Debug::dump($prosDM);die();

        $legendData = array_keys($prosDM['facet_counts']['facet_fields']['bundle']);

        $starData = array(
            array(
                'name'=>"Max",
                'value'=>$prosDM['response']['numFound'],
                'x'=>"80%",
                'y'=>50,
                'symbolSize'=>32
            ),        
        );

        $pieData1 = array(); $pieData2 = array(); $pieData3 = array();
        foreach ($prosDM['facet_counts']['facet_fields']['bundle'] as $bundle => $value) {
            if($value>=10000){
                $pieData1[] = array(
                    'name'=>$bundle,
                    'value'=>$value,
                );
            } else if($value>=1000 && $value<10000){
                $pieData2[] = array(
                    'name'=>$bundle,
                    'value'=>$value,
                );
            } else {
                $pieData3[] = array(
                    'name'=>$bundle,
                    'value'=>$value,
                );
            }
        }  
    }

	// Zend_Debug::dump($jsdata);die();
	// Zend_Debug::dump($datasource);die();
	$this->view->params = $params;
	$this->view->datasource = $datasource;
	$this->view->data = $data;
	$this->view->jsdata = $jsdata;
	$this->view->logo = $g->get_logo();
    $this->view->headScript()->appendFile('/assets/core/global/plugins/echarts/echarts.js');
    $this->view->headScript()->appendFile('/assets/core/global/plugins/echarts/chart/pie.js');
	$this->view->headLink()->appendStylesheet("/assets/core/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css");
	$this->view->headScript()->appendFile("/assets/core/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js");
	$chartjs = "";
    $default = "";
	if($jsdata){
        $default = "";
		foreach($jsdata as $k=>$v){
			$chartjs .="
			var pie_".$k." = ec.init(document.getElementById('pie_".$k."'));
			pie_".$k.".setOption({
				tooltip: {
					show: true,
					formatter: '{b} : {c} ({d}%)'
				},
				legend: {
					orient: 'vertical',
					x: 'left',
					y:'bottom',
					data: [{
							name:'key',
							textStyle : {
								fontSize:15,
								icon:'circle'
							}
						},
						{
							name:'bundle',
							textStyle : {
								fontSize:15,
								icon:'circle'
							}
						},
						{
							name:'sentiment',
							textStyle : {
								fontSize:15,
								icon:'circle'
							}
						},
						{
							name:'netral',
							textStyle : {
								fontSize:15,
								icon:'circle'
							}
						},
						{
							name:'negatif',
							textStyle : {
								fontSize:15,
								icon:'circle'
							}
						},
						{
							name:'positif',
							textStyle : {
								fontSize:15,
								icon:'circle'
							}
						}
					]
				},
				toolbox: {
					show: true,
					orient: 'horizontal',
					x: 'center',
					y:'top',
					feature: {
						mark: {
							show: false
						},
						dataView: {
							show: true,
							readOnly: false
						},
						restore: {
							show: true
						},
						saveAsImage: {
							show: true
						}
					}
				},
				calculable: true,
				loadingOption:{
					text : 'Loading Data...'
				},
				noDataLoadingOption:{
					text : 'No Data Found.'
				},
				series: [
					{
						type:'pie',
						name:'bundle',
						center: ['35%', 200],
						radius:['60%', '75%'],
						data:".($jsdata[$k]['bundle']?json_encode($jsdata[$k]['bundle']):'{}')."
					},
					{
						type:'pie',
						name:'key',
						center: ['35%', 200],
						radius:['30%', '45%'],
						data:".($jsdata[$k]['key']?json_encode($jsdata[$k]['key']):'{}')."
					},
					{
						name: 'sentiment',
						type: 'pie',
						clockWise: true,
						startAngle: 135,
						center: ['75%', 200],
						radius: [80, 120],
						itemStyle: {
							normal: {
								label: {
									show : true
								},
								labelLine: {
									show: true
								}
							},
						},
						data:".($jsdata[$k]['sentiment']?json_encode($jsdata[$k]['sentiment']):'{}')."
					}
				]
			});
			window.onresize = pie_".$k.".resize;

			pie_".$k.".on('click', function (params){
				getlistbysentiment(".$k.",0,params.seriesName,params.name);
				getlistbysentiment(".$k.",1,params.seriesName,params.name);
				getlistbysentiment(".$k.",2,params.seriesName,params.name);
			});
			";
		}
	} else {
        $chartjs = '
        var echarts_pie = ec.init(document.getElementById("echarts_pie"));

        echarts_pie.setOption({
                    tooltip: {
                        show: true,
                        formatter: "{a} <br/>{b} : {c} ({d}%)"
                    },
                    legend: {
                        orient: "vertical",
                        x: "bottom",
                        data: '.json_encode($legendData).'
                    },
                    toolbox: {
                        show: true,
                        feature: {
                            mark: {
                                show: true
                            },
                            dataView: {
                                show: true,
                                readOnly: false
                            },
                            restore: {
                                show: true
                            },
                            saveAsImage: {
                                show: true
                            }
                        }
                    },
                    calculable: true,
                    series: [{
                        name: "Source",
                        type: "pie",
                        center: ["35%", 200],
                        radius: 80,
                        itemStyle: {
                            normal: {
                                label: {
                                    position: "inner",
                                    formatter: function(params) {
                                        return (params.percent - 0).toFixed(0) + "%"
                                    }
                                },
                                labelLine: {
                                    show: false
                                }
                            },
                            emphasis: {
                                label: {
                                    show: true,
                                    formatter: "{b}\n{d}%"
                                }
                            }

                        },
                        data: '.json_encode($pieData1).'
                    }, {
                        name: "Source",
                        type: "pie",
                        center: ["35%", 200],
                        radius: [110, 140],
                        data: '.json_encode($pieData2).'
                    }, {
                        name: "Source",
                        type: "pie",
                        clockWise: true,
                        startAngle: 135,
                        center: ["75%", 200],
                        radius: [80, 120],
                        itemStyle: {
                            normal: {
                                label: {
                                    show: false
                                },
                                labelLine: {
                                    show: false
                                }
                            },
                            emphasis: {
                                color: (function() {
                                    var zrColor = require("zrender/tool/color");
                                    return zrColor.getRadialGradient(
                                        650, 200, 80, 650, 200, 120, [
                                            [0, "rgba(255,255,0,1)"],
                                            [1, "rgba(255,0,0,1)"]
                                        ]
                                    )
                                })(),
                                label: {
                                    show: true,
                                    position: "center",
                                    formatter: "{d}%",
                                    textStyle: {
                                        color: "red",
                                        fontSize: "30",
                                        fontFamily: "Open Sans",
                                        fontWeight: "bold"
                                    }
                                }
                            }
                        },
                        data: '.json_encode($pieData3).',
                        markPoint: {
                            symbol: "star",
                            data: '.json_encode($starData).'
                        }
                    }]
        });
        window.onresize = echarts_pie.resize;

        echarts_pie.on("click", function (params){
            getlistbysentiment(0,0,params.seriesName,params.name);
            getlistbysentiment(0,1,params.seriesName,params.name);
            getlistbysentiment(0,2,params.seriesName,params.name);
        });';
    }
	
	// die($chartjs);
	$this->view->headScript()->appendScript('
		var Page = function () {
		
			//== Private functions
			var advdiscovery = function () {
				var txt = $(".m_autosize");
				autosize(txt);
				
				// console.log(moment.locale());
				var _start = moment().subtract(1, "days");
				var _end = moment();
				//console.log(_start);
				//console.log(_end);
				$("#m_daterangepicker_1").daterangepicker({
					buttonClasses: "m-btn btn",
					applyClass: "btn-primary",
					cancelClass: "btn-secondary",
					startDate: _start,
					endDate: _end,
					maxDate : moment(),
					ranges: {
					   "Today": [moment(), moment()],
					   "Yesterday": [moment().subtract(1, "days"), moment().subtract(1, "days")],
					   "Last 7 Days": [moment().subtract(6, "days"), moment()],
					   "Last 30 Days": [moment().subtract(29, "days"), moment()],
					   "This Month": [moment().startOf("month"), moment().endOf("month")],
					   "Last Month": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
					}
				}, function(start, end, label) {
					$("#startdate").val(start.format("YYYY-MM-DD"));
					$("#enddate").val(end.format("YYYY-MM-DD"));
					$("#m_daterangepicker_1 .form-control").val( start.format("YYYY-MM-DD") + " / " + end.format("YYYY-MM-DD"));
				});
				
				$(".tags_input").tagsinput({
				  trimValue: true,
				  tagClass: "filtertag"
				});
				$(".m-widget4__item").remove();
				
				$("#formFilter").on("submit", function(){
					mApp.blockPage({
						overlayColor: "#000000",
						type: "loader",
						state: "success",
						message: "Please wait..."
					});
				});
				
				$("#formFilter").on("keyup keypress", function(e) {
					var keyCode = e.keyCode || e.which;
					if (keyCode === 13) { 
						e.preventDefault();
						return false;
					}
				});
				
				$("#m_modal_1").on("show.bs.modal", function (e) {
					var a = $(e.relatedTarget);
					//console.log(a.data());
					//console.log(a.data("link"));
					
					var id = a.data("link");
					var title = a.data("title");
					var color = a.data("color");
					
					$("#title_modal1").html(title);
					
					var xhr1 = $.ajax({
						url: "/domas/index/continuereading",
						content: "html",
						method: "post",
						data: {id:id,},
						beforeSend: function() {
							$("#extsource_modal1").removeClass("m--hide");
							$("#head_modal1").removeClass("m--bg-accent m--bg-danger m--bg-success");
							$("#head_modal1").addClass("m--bg-"+color);
							mApp.blockPage({
								overlayColor: "#000000",
								type: "loader",
								state: "success",
								message: "Please wait..."
							});
						},
						complete: function(result) {
							mApp.unblockPage();
						},
						success: function(data) {
							if($("#scroller_modal1").hasClass("mCustomScrollbar")){
								$("#scroller_modal1").mCustomScrollbar("destroy");
							}
							$("#scroller_modal1").html(data);
							$("#extsource_modal1").attr("href",id);
						},
						error: function() {
							if($("#scroller_modal1").hasClass("mCustomScrollbar")){
								$("#scroller_modal1").mCustomScrollbar("destroy");
							}
							$("#scroller_modal1").html("No Data Available");
							$("#extsource_modal1").addClass("m--hide");
						},
						async : false
					});
					
					$("#scroller_modal1").mCustomScrollbar({
						alwaysShowScrollbar:true,
						autoScrollOnFocus : false,
						updateOnContentResize : false,
						updateOnImageLoad : false,
						setWidth : "100%",
						setHeight : 450,
						theme:"minimal-dark"
					});
				});
				
				$("#m_modal_1").on("hidden.bs.modal", function (e) {
					$("#title_modal1").html("");
					if($("#scroller_modal1").hasClass("mCustomScrollbar")){
						$("#scroller_modal1").mCustomScrollbar("destroy");
					}
					$("#scroller_modal1").html("No Data Available");
					$("#head_modal1").removeClass("m--bg-accent m--bg-danger m--bg-success");
					$("#extsource_modal1").attr("href","#");
					$("#extsource_modal1").addClass("m--hide");
				});
				
				// ECHARTS
				require.config({
					paths: {
						echarts: "/assets/core/global/plugins/echarts/"
					}
				});

				require(
					[
						"echarts",
						"echarts/chart/pie"
					],
					function(ec) {
				'.$chartjs.'
						}
				);
			}

			return {
				// public functions
				init: function() {
					advdiscovery(); 
				}
			};
		}();

		jQuery(document).ready(function() {    
			Page.init();
            '.$default.'
		});'
    );
}

public function getdiscoverylistAction(){

	$this->_helper->layout->disableLayout();
	$params = $this->getRequest()->getParams();
	// Zend_Debug::dump($params);die();


	$params["start"] = 0;
	$params["nextpage"] = 0;
	if(isset($params["page"]) && $params["page"]!=""){
		$params["start"] = (int) $params['page'] * 10;
		$params["nextpage"] = (int) $params['page'] + 1;
		// die("s");
	}

	$g = new Domas_Model_Pdash();
	$n = new Domas_Model_Withsolrpmas();


	$entity_id = $params["entity"];
	// Zend_Debug::dump($entity_id);

	$sentiment_id = $params["sentiment"];
	// Zend_Debug::dump($sentiment_id);
	$andsearch = array();
	if($params["andsearch"]!=""){				
		$andsearch = explode(",", $params["andsearch"]);
	}
	$exsearch = array();
	if($params["exsearch"]!=""){				
		$exsearch = explode(",", $params["exsearch"]);
	}
	
	// Zend_Debug::dump($andsearch);
	// Zend_Debug::dump($exsearch);
	// die();
	

	$and = "";
	$and .= "&fq=content%3A[*+TO+*]";
	$and .= "&fq=content_date%3A%5B".$params["startdate"]."T00%3A00%3A00Z+TO+".$params["enddate"]."T23%3A59%3A59Z%5D";
	$and .= "&fq=entity_id%3A".$entity_id;
	$and .= "&fq=is_sentiment%3A".$sentiment_id;
	$and .= '&fq=is_mediatype%3A1+OR+2';
	if(count($andsearch)>0){
		foreach($andsearch as $x=>$y){
			$and .='&fq=content%3A%22'.urlencode($y).'%22';
		}
	}
	if(count($exsearch)>0){
		foreach($exsearch as $x=>$y){
			$and .='&fq=-content%3A%22'.urlencode($y).'%22';
		}
	}
	if($params["series"]=="key"){
		$keysearch = explode(",", $params["name"]);
		// Zend_Debug::dump($keysearch);//die();
		$and .= '&fq=content%3A%22'.urlencode(implode('" OR "',$keysearch)).'%22';
	} else if($params["series"]=="bundle"){
		
		$keysearch = explode(",", $params["keysearch"]);
		$and .= '&fq=content%3A%22'.urlencode(implode('" OR "',$keysearch)).'%22';
		$and .= '&fq=bundle%3A'.$params["name"];
	} else if($params["series"]=="sentiment"){
		
		$sentiment = array("netral"=>0,"negatif"=>1,"positif"=>2);
		
		$keysearch = explode(",", $params["keysearch"]);
		$and .= '&fq=content%3A%22'.urlencode(implode('" OR "',$keysearch)).'%22';
		$and .= '&fq=is_sentiment%3A'.$sentiment[$params["name"]];
	} else if($params["series"]=="words"){
		$keysearch = explode(",", $params["keysearch"]);
		$and .= '&fq=content%3A%22'.urlencode(implode('" OR "',$keysearch)).'%22';
		$and .= '&fq=content:"'.$params["name"].'"';
		$and .= '&fq=is_sentiment%3A'.$params["sentiment"];
	}
	
	// echo $and;die();
	// echo urldecode($and);die();

	$query = "select?q=*%3A*".$and."&start=".$params["start"]."&rows=10&wt=json&indent=true";
	// echo urldecode($query);die();

	$tmp = $n->get_by_rawurl($query);
	// Zend_Debug::dump($tmp);die();

	if($tmp["response"]["numFound"]>0){
		$data[$entity_id]["sentiment"][$sentiment_id][0]["count"] = $tmp["response"]["numFound"];
		$data[$entity_id]["sentiment"][$sentiment_id][0]["data"] = $tmp["response"]["docs"];
		// $i++;
	}

	// Zend_Debug::dump($tmp["response"]["numFound"]);//die();
	// Zend_Debug::dump($tmp["response"]["start"]);//die();

	$params["next"] = false;
	if((int)$tmp["response"]["numFound"]>10 && ((int)$tmp["response"]["numFound"]-(int)$tmp["response"]["start"])>10){
		$params["next"] = true;
	}

	// Zend_Debug::dump($data);die();

	$this->view->varams = $params;
	$this->view->data = $data;
	$this->view->logo = $g->get_logo();
}

public function wordcloudAction() {
	$params = $this->getRequest()->getParams();
	// Zend_Debug::dump($_POST);die();
	$wcdata = array();
	$wc2data = array();
	if(isset($params["keysearch"]) && $params["keysearch"]!=""){
		// Zend_Debug::dump($params);//die();

		$keysearch = array_map('trim',explode(",",$params["keysearch"]));
		// Zend_Debug::dump($keysearch);//die();

		$dd = new Model_Cache();
		$mdlSolr = new Domas_Model_Solr();

		// $exclude_words = $mdlSolr->get_katadasar(array("tipe_katadasar"=>"adverbia,konjungsi,preposisi,pronomina,lain-lain"));
		
		// Zend_Debug::dump($exclude_words);die();
		
		$p = array();
		
		$cache = $dd->cachefunc(3000);
		$id = $dd->get_id("Domas_Model_Solr_get_katadasar",$p);
		$exclude_words = $dd->get_cache($cache, $id);
		if(!$exclude_words) {
			$exclude_words = $mdlSolr->get_katadasar();
			$cache->save($exclude_words, $id, array('systembin'));
		}

		foreach($keysearch as $k=>$v){

			// Zend_Debug::dump($v);

			$start = 0;
			$rows = 0;
			$tmpx = $mdlSolr->get_count_keysearch($v,$params,$start,$rows);

			$rows = $tmpx->numFound;
			$tmp = $tmp = $mdlSolr->get_count_keysearch($v,$params,$start,$rows);
			// Zend_Debug::dump($tmp);die();

			$wcdata[$k]["name"] = $v;
			$wcdata[$k]["value"] = $tmp->numFound;

			$words = array();
			$content = "";
			foreach($tmp->docs as $k1=>$v1){
				$content .= preg_replace("/[^a-zA-Z0-9 ]/", '', strtolower($v1["content"]))." ";
			}

			// Zend_Debug::dump($content);die();
			$words = array_count_values(str_word_count($content, 1));
			arsort($words);

			$tmp = $words;
			$words = array();
			foreach ($tmp as $k1 => $v1) {
				if($k1!="" && !in_array(strtolower($k1), $exclude_words) && !is_numeric($k1) && $v1>1){
					$words[$k1] = $v1;
				}					
			}

			// $words = array_slice($words,0,100);				
			// Zend_Debug::dump($words);die("s");

			$i=0;
			foreach($words as $k2=>$v2){

				// if($i<100 && $v2>1){
					$wc2data[$v][$i]["name"] = $k2;
					$wc2data[$v][$i]["value"] = $v2;

					$i++;						
				// }

			}
			// Zend_Debug::dump($tmp);die("s");
		}
		// Zend_Debug::dump($words);die("s");
		// Zend_Debug::dump($wc2data);die();
	}

	if(!isset($params["startdate"])) {
		$params["startdate"] = date("Y-m-d", strtotime('yesterday'));
	}
	if(!isset($params["enddate"])) {
		$params["enddate"] = date("Y-m-d");
	}

	$this->view->params = $params;
	$this->view->wcdata = $wcdata;
	$this->view->wc2data = $wc2data;
	
	$this->view->headLink()->appendStylesheet("/assets/core/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css");
	$this->view->headScript()->appendFile("/assets/core/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js");
	$this->view->headScript()->appendFile('/assets/core/plugins/echarts/echarts.simple.js');
	$this->view->headScript()->appendFile('/assets/core/plugins/echarts/wordcloud/dist/echarts-wordcloud.js');
	
	$chartjs = "";
	
    $i = 0;
    foreach($wc2data as $k=>$v){
		$chartjs .="
        var chart_".$i." = echarts.init(document.getElementById('wc_".$i."'));
        chart_".$i.".setOption({              
            tooltip : {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    animation: false,
                    label: {
                        backgroundColor: '#505765'
                    }
                }
            },
            series: [ {
                name: 'words',
                type: 'wordCloud',
                gridSize: 2,
                sizeRange: [12, 50],
                rotationRange: [0,0],
                width: '100%',
                height: '100%',
                textStyle: {
                    normal: {
                        color: function () {
                            return 'rgb(' + [
                                Math.round(Math.random() * 160),
                                Math.round(Math.random() * 160),
                                Math.round(Math.random() * 160)
                            ].join(',') + ')';
                        }
                    },
                    emphasis: {
                        shadowBlur: 10,
                        shadowColor: '#333'
                    }
                },
                data: ".json_encode($v).",
            } ]
        });
        window.onresize = chart_".$i.".resize;

        chart_".$i.".on('click', function(params){
            // console.log(params);
			getlistbysentiment(2,0,params.seriesName,params.name);
			getlistbysentiment(2,1,params.seriesName,params.name);
			getlistbysentiment(2,2,params.seriesName,params.name);
        });
		";
        $i++;
    }
	
	$this->view->headScript()->appendScript("
		var Page = function () {
		
			//== Private functions
			var wordcloud = function () {
				// basic demo
				var txt = $('.m_autosize');
				autosize(txt);
				
				// console.log(moment.locale());
				var _start = moment().subtract(1, 'days');
				var _end = moment();
				//console.log(_start);
				//console.log(_end);
				$('#m_daterangepicker_1').daterangepicker({
					buttonClasses: 'm-btn btn',
					applyClass: 'btn-primary',
					cancelClass: 'btn-secondary',
					startDate: _start,
					endDate: _end,
					maxDate : moment(),
					ranges: {
					   'Today': [moment(), moment()],
					   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
					   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
					   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
					   'This Month': [moment().startOf('month'), moment().endOf('month')],
					   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
					}
				}, function(start, end, label) {
					$('#startdate').val(start.format('YYYY-MM-DD'));
					$('#enddate').val(end.format('YYYY-MM-DD'));
					$('#m_daterangepicker_1 .form-control').val( start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
				});
				
				$('.tags_input').tagsinput({
				  trimValue: true,
				  tagClass: 'filtertag'
				});
				$('.m-widget4__item').remove();
				
				$('#formFilter').on('submit', function(){
					mApp.blockPage({
						overlayColor: '#000000',
						type: 'loader',
						state: 'success',
						message: 'Please wait...'
					});
				});
				
				$('#formFilter').on('keyup keypress', function(e) {
					var keyCode = e.keyCode || e.which;
					if (keyCode === 13) { 
						e.preventDefault();
						return false;
					}
				});
				
				$('#m_modal_1').on('show.bs.modal', function (e) {
					var a = $(e.relatedTarget);
					//console.log(a.data());
					//console.log(a.data('link'));
					
					var id = a.data('link');
					var title = a.data('title');
					var color = a.data('color');
					
					$('#title_modal1').html(title);
					
					var xhr1 = $.ajax({
						url: '/domas/index/continuereading',
						content: 'html',
						method: 'post',
						data: {id:id,},
						beforeSend: function() {
							$('#extsource_modal1').removeClass('m--hide');
							$('#head_modal1').removeClass('m--bg-accent m--bg-danger m--bg-success');
							$('#head_modal1').addClass('m--bg-'+color);
							mApp.blockPage({
								overlayColor: '#000000',
								type: 'loader',
								state: 'success',
								message: 'Please wait...'
							});
						},
						complete: function(result) {
							mApp.unblockPage();
						},
						success: function(data) {
							if($('#scroller_modal1').hasClass('mCustomScrollbar')){
								$('#scroller_modal1').mCustomScrollbar('destroy');
							}
							$('#scroller_modal1').html(data);
							$('#extsource_modal1').attr('href',id);
						},
						error: function() {
							if($('#scroller_modal1').hasClass('mCustomScrollbar')){
								$('#scroller_modal1').mCustomScrollbar('destroy');
							}
							$('#scroller_modal1').html('No Data Available');
							$('#extsource_modal1').addClass('m--hide');
						},
						async : false
					});
					
					$('#scroller_modal1').mCustomScrollbar({
						alwaysShowScrollbar:true,
						autoScrollOnFocus : false,
						updateOnContentResize : false,
						updateOnImageLoad : false,
						setWidth : '100%',
						setHeight : 450,
						theme:'minimal-dark'
					});
				});
				
				$('#m_modal_1').on('hidden.bs.modal', function (e) {
					$('#title_modal1').html('');
					if($('#scroller_modal1').hasClass('mCustomScrollbar')){
						$('#scroller_modal1').mCustomScrollbar('destroy');
					}
					$('#scroller_modal1').html('No Data Available');
					$('#head_modal1').removeClass('m--bg-accent m--bg-danger m--bg-success');
					$('#extsource_modal1').attr('href','#');
					$('#extsource_modal1').addClass('m--hide');
				});
				
				var chart = echarts.init(document.getElementById('ec_wordcloud'));
				chart.setOption({                
					tooltip : {
						trigger: 'axis',
						axisPointer: {
							type: 'cross',
							animation: false,
							label: {
								backgroundColor: '#505765'
							}
						}
					},
					series: [ {
						type: 'wordCloud',
						gridSize: 2,
						sizeRange: [12, 50],
						rotationRange: [0,0],
						shape: 'pentagon',
						width: '100%',
						height: '100%',
						textStyle: {
							normal: {
								color: function () {
									return 'rgb(' + [
										Math.round(Math.random() * 160),
										Math.round(Math.random() * 160),
										Math.round(Math.random() * 160)
									].join(',') + ')';
								}
							},
							emphasis: {
								shadowBlur: 10,
								shadowColor: '#333'
							}
						},
						data: ".json_encode($wcdata).",
					} ]
				});
				window.onresize = chart.resize;
				
				".$chartjs."
				
			}

			return {
				// public functions
				init: function() {
					wordcloud(); 
				}
			};
		}();

		jQuery(document).ready(function() {    
			$('a[href$=\"#\"]').on('click',function(e){
				e.preventDefault();
			});
			Page.init();
		});
	");
}

public function mediaanalyticAction(){

    $params = $this->getRequest()->getParams();

    if(isset($_POST['ajax'])){

	    if(isset($_POST['ajax']) && (int)$_POST['ajax']==1){

	        // Zend_Debug::dump($_POST);//die();
	        $p = array(
	            'fq'=>array(
	                'startdate'=>$_POST['startdate'],
	                'enddate'=>$_POST['enddate'],
	                'keys'=>array_filter(explode(',',$_POST['keysearch'])),
	                'and'=>array_filter(explode(',',$_POST['andsearch'])),
	                'exception'=>array_filter(explode(',',$_POST['exsearch'])),
	                'entity_id'=>$_POST['entity_id'],
	                'field'=>$_POST['field'],
	                'value'=>$_POST['value'],
	            ),
	        );

	        $solr = new Bigbox_Model_Solr();
	        $res = $solr->get_mediaanalytic_trend($p);
	        // Zend_Debug::dump($res);die();

	        $result = $res;
	        $result['data'] = array();
	        $result['dataOthers'] = array('entity_id'=>$_POST['entity_id'],'field'=>$_POST['field'],'value'=>$_POST['value']);
	        foreach ($res['data']['facet_counts']['facet_ranges']['content_date']['counts'] as $key => $value) {
	            $date = strtotime(substr($key, 0,10).' +7 hours')*1000;
	            $result['data'][] = array($date,$value);
	        }
	        // Zend_Debug::dump($result);die();




	        header("Access-Control-Allow-Origin: *");
	        header('Content-Type: application/json');
	        echo json_encode($result);
	        die();
	    } else if(isset($_POST['ajax']) && (int)$_POST['ajax']==2){

	        // Zend_Debug::dump($_POST);die();

	        $p = array(
	        	'fq'=>array(
	        		'keysearch'=>($_POST['keysearch']!=''?explode(',', $_POST['keysearch']):array()),
	        		'andsearch'=>($_POST['andsearch']!=''?explode(',', $_POST['andsearch']):array()),
	        		'exsearch'=>($_POST['exsearch']!=''?explode(',', $_POST['exsearch']):array()),
	        		'user_location'=>$_POST['user_location'],
	        		'startdate'=>$_POST['startdate'],
	        		'enddate'=>$_POST['enddate'],
	        		'entity_id'=>$_POST['entity_id'],
	        		'content_date'=>date('Y-m-d',($_POST['timestamp']/1000)),
	        		'field'=>$_POST['field'],
	        		'value'=>$_POST['value'],
	        	),
	        );

	        // Zend_Debug::dump($p);die();

	        $solr = new Bigbox_Model_Solr();
	        $data = $solr->get_docMediaDigital($p);
	        // Zend_Debug::dump($data);die();

	        $result = array();
	        $result = $data;
	        $result["data"] = array(
	        	"numFound"=>$data["data"]->response->numFound,
	        	"docs"=>array(),
	        );

	        foreach ((array)$data["data"]->response->docs as $key => $value) {

	        	$content_date = date("Y-m-d H:i:s",strtotime($value["content_date"]));

	        	$result["data"]["docs"][$key]  = $value;
	        	$result["data"]["docs"][$key]["content_date_format"]  = $content_date;

		    	if(isset($value["ss_icon"]) && $value["ss_icon"]==""){
		    		$result["data"]["docs"][$key]["ss_icon"] = "/assets/pmas/images/media/".$value["bundle"].".png";
		    	}

	        }



	        header("Access-Control-Allow-Origin: *");
	        header('Content-Type: application/json');
	        echo json_encode($result);
	        die();
	    } else {

	    	$result = array(
	    		"transaction"=>false,
	    		"params"=>$_POST,
	    		"data"=>false,
	    		"message"=>"invalid identifier",
	    	);


	        header("Access-Control-Allow-Origin: *");
	        header('Content-Type: application/json');
	        echo json_encode($result);
	        die();
	    }

    }

    if(!isset($_POST['startdate']) || $_POST['startdate']==''){
        $startdate = date('Y-m-d', strtotime(date('Y-m-d').' -1 days'));
    } else {
        $startdate = $_POST['startdate'];
    }
    if(!isset($params['enddate']) || $params['enddate']==''){
        $enddate = date('Y-m-d');
    } else {
        $enddate = $_POST['enddate'];
    }
    // Zend_Debug::dump($params);die();

    $solr = new Bigbox_Model_Solr();

    $p = array(
        'startdate'=>$startdate,
        'enddate'=>$enddate,
        'fq'=>array(
            'keys'=>array(),
            'and'=>array(),
            'exception'=>array(),
        ),
    );
    if(isset($_POST['andsearch']) && $_POST['andsearch']!=''){
        $p['fq']['and'] = explode(',', $_POST['andsearch']);
    }
    if(isset($_POST['exsearch']) && $_POST['exsearch']!=''){
        $p['fq']['exception'] = explode(',', $_POST['exsearch']);
    }
    if(isset($_POST['keysearch']) && $_POST['keysearch']!=''){
        $p['fq']['keys'] = explode(',', $_POST['keysearch']); //Zend_Debug::dump($keysearch);die();
    }

    $data = $solr->get_mediaanalytic($p);
    // Zend_Debug::dump($data);die();


    $starData = array(); $legendData = array();
    $pie = array(
        'top3'=>array(
            'name'=>'Digital Media',
            'data'=>array(),
        ),
        'top10'=>array(
            'name'=>'Digital Media',
            'data'=>array(),
        ),
        'others'=>array(
            'name'=>'Digital Media',
            'data'=>array(),
        ),
    );
    if($data['transaction']){       
        $i = 0; $numFound = $data['data']['numFound'];
        if(count($data['data']['keywords'])==0){
            foreach ($data['data']['bundle'] as $name => $value) {
                $percentOfTotal = round($value*100/$numFound,2);
                if($i<3){
                    $pie['top3']['data'][] = array(
                        'name'=>$name,
                        'value'=>$value,
                        'percentOfTotal'=>$percentOfTotal,
                    );
                } else if($i<10){
                    $pie['top10']['data'][] = array(
                        'name'=>$name,
                        'value'=>$value,
                        'percentOfTotal'=>$percentOfTotal,
                    );
                } else {
                    $pie['others']['data'][] = array(
                        'name'=>$name,
                        'value'=>$value,
                        'percentOfTotal'=>$percentOfTotal,
                    );
                }
                $legendData[] = $name;
                $i++;
            }
        } else if(count($data['data']['keywords'])>0){
            $pie['top3']['name'] = 'Keyword';
            $pie['top10']['name'] = 'Digital Media';
            $pie['others']['name'] = 'Sentiment';
            foreach ($data['data']['keywords'] as $name => $value) {
                $percentOfTotal = round($value*100/$numFound,2);
                $pie['top3']['data'][] = array(
                    'name'=>$name,
                    'value'=>$value,
                    'percentOfTotal'=>$percentOfTotal,
                );
                $legendData[] = $name;
            }
            foreach ($data['data']['bundle'] as $name => $value) {
                $percentOfTotal = round($value*100/$numFound,2);
                $pie['top10']['data'][] = array(
                    'name'=>$name,
                    'value'=>$value,
                    'percentOfTotal'=>$percentOfTotal,
                );
                $legendData[] = $name;
            }
            foreach ($data['data']['sentiment'] as $name => $value) {
                $percentOfTotal = round($value*100/$numFound,2);
                $pie['others']['data'][] = array(
                    'name'=>$name,
                    'value'=>$value,
                    'percentOfTotal'=>$percentOfTotal,
                );
                $legendData[] = $name;
            }
        }

        $starData = array(
            array(
                'name'=>"Total",
                'value'=>$numFound,
                'x'=>"55%",
                'y'=>50,
                'symbolSize'=>32
            ),        
        );
    }

    // Zend_Debug::dump($pie);die();

    $this->view->p = $p;

    $this->view->headScript()->appendFile('/assets/core/global/plugins/echarts/echarts.js');
    $this->view->headScript()->appendFile('/assets/core/global/plugins/echarts/chart/pie.js');
    $this->view->headLink()->appendStylesheet("/assets/core/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css");
    $this->view->headScript()->appendFile("/assets/core/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js");
    $this->view->headScript()->appendFile('/assets/bigbox/plugins/Highcharts-6.0.7/highcharts.js');
    $this->view->headScript()->appendFile('/assets/bigbox/plugins/Highcharts-6.0.7/modules/exporting.js');

    $chartjs = '
    var echarts_pie = ec.init(document.getElementById("echarts_pie"));

    echarts_pie.setOption({
                tooltip: {
                    show: true,
                    formatter: function(params) {
                        if(params.seriesName=="Digital Media" && params.name=="Total"){
                            return "Docs<br\>"+params.name+": "+params.value
                        } else {
                            return params.seriesName+"<br\>"+params.name+": "+params.value
                        }
                    }
                },
                legend: {
                    orient: "vertical",
                    x: "left",
                    data: '.json_encode($legendData).'
                },
                toolbox: {
                    show: true,
                    feature: {
                        mark: {
                            show: true
                        },
                        dataView: {
                            show: true,
                            readOnly: false
                        },
                        restore: {
                            show: true
                        },
                        saveAsImage: {
                            show: true
                        }
                    }
                },
                calculable: true,
                series: [{
                    name: "'.$pie['top3']['name'].'",
                    type: "pie",
                    center: ["35%", 200],
                    radius: 80,
                    itemStyle: {
                        normal: {
                            label: {
                                position: "inner",
                                formatter: function(params) {
                                    return params.name+"\n"+params.data.percentOfTotal+"%"
                                }
                            },
                            labelLine: {
                                show: false
                            }
                        },
                        emphasis: {
                            label: {
                                show: true,
                                formatter: "{b}\n{c}"
                            }
                        }
                    },
                    data: '.json_encode($pie['top3']['data']).',
                }, {
                    name: "'.$pie['top10']['name'].'",
                    type: "pie",
                    center: ["35%", 200],
                    radius: [110, 140],
                    itemStyle: {
                        normal: {
                            label: {
                                formatter: function(params) {
                                    return params.name+"\n"+params.data.percentOfTotal+"%"
                                }
                            },
                        },
                    },
                    data: '.json_encode($pie['top10']['data']).',
                    markPoint: {
                        symbol: "star",
                        data: '.json_encode($starData).'
                    }
                }, {
                    name: "'.$pie['others']['name'].'",
                    type: "pie",
                    clockWise: true,
                    startAngle: 135,
                    center: ["75%", 200],
                    radius: [80, 120],
                    itemStyle: {
                        normal: {
                            label: {
                                formatter: function(params) {
                                    return params.name+"\n"+params.data.percentOfTotal+"%"
                                }
                            },
                        },
                    },
                    data: '.json_encode($pie['others']['data']).',
                }]
    });
    window.onresize = echarts_pie.resize;

    echarts_pie.on("click", function (params){
        console.log(params);
        getDetailByPeriod(params);
    });


    $(".btn-modal").on("click", function(){
    	// console.log($(this).data());

        var formData = $("#formFilter").serializeArray(); //console.log(formData);
        formData.push({ name: "ajax", value: 2 });
        formData.push({ name: "entity_id", value: 2 });
        formData.push({ name: "field", value: $(this).data("field") });
        formData.push({ name: "value", value: $(this).data("value") });
        formData.push({ name: "timestamp", value: $(this).data("x") });

        var modaltitle = $(this).data("value");

        $.ajax({
            url: "/bigbox/bigsearch/mediaanalytic",
            content: "json",
            method: "post",
            data: formData,
            beforeSend: function(){
            	$("#m_modal_4 .modal-dialog .modal-content .modal-header .modal-title").html(modaltitle);
	            mApp.block("#m_modal_4 .modal-content", {
	                overlayColor: "#000000",
	                type: "loader",
	                state: "success",
	                size: "lg",
	            });
            },
            error: function(){
            },
            complete: function(res){
                mApp.unblock("#m_modal_4 .modal-content");
            },
            success: function(res){
            	// console.log(res);

            	var html = "";
            	if(res.data!=undefined && res.data.docs!=undefined && res.data.docs.length>0){
            	html +=""+
            	"<div id=\"scroller_fk\" class=\"m-widget4 m-scrollable\" data-scrollbar-shown=\"true\">"+
            		"<div class=\"m-widget3\">";
            	$.each(res.data.docs, function(k,v){
            		html +=""+
            			"<div class=\"m-widget3__item\">"+
							"<div class=\"m-widget3__header\">"+
								"<div class=\"m-widget3__user-img\">"+
									"<img class=\"m-widget3__img\" src=\""+(v.ss_user_profile_image_url_https!=""?v.ss_user_profile_image_url_https:"/assets/pmas/images/media/twitter.png")+"\">"+
								"</div>"+
								"<div class=\"m-widget3__info\">"+
									"<span class=\"m-widget3__username\">"+
										v.bundle+": <a href=\""+v.id+"\" target=\"_blank\">"+v.label+"</a>"+
									"</span>"+
									"<span class=\"m-widget3__time pull-right\">"+
										v.content_date_format+										
									"</span>"+
									"<br>"+
									v.label+
								"</div>"+
								"<span class=\"m-widget3__status m--font-info\">"+
								"</span>"+
							"</div>"+
						"</div>";

            	});
            	html +=""+
					"</div>"+
            	"</div>"+
            	"";
            	}

            	$("#m_modal_4 > .modal-dialog .modal-content .modal-body").html(html);
            },
        });
    });

    ';

    $this->view->headScript()->appendScript('
        var Page = function () {
        
            //== Private functions
            var advdiscovery = function () {
                var txt = $(".m_autosize");
                autosize(txt);
                
                // console.log(moment.locale());
                var _start = moment().subtract(1, "days");
                var _end = moment();
                //console.log(_start);
                //console.log(_end);
                $("#m_daterangepicker_1").daterangepicker({
                    buttonClasses: "m-btn btn",
                    applyClass: "btn-primary",
                    cancelClass: "btn-secondary",
                    startDate: _start,
                    endDate: _end,
                    maxDate : moment(),
                    ranges: {
                       "Today": [moment(), moment()],
                       "Yesterday": [moment().subtract(1, "days"), moment().subtract(1, "days")],
                       "Last 7 Days": [moment().subtract(6, "days"), moment()],
                       "Last 30 Days": [moment().subtract(29, "days"), moment()],
                       "This Month": [moment().startOf("month"), moment().endOf("month")],
                       "Last Month": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
                    }
                }, function(start, end, label) {
                    $("#startdate").val(start.format("YYYY-MM-DD"));
                    $("#enddate").val(end.format("YYYY-MM-DD"));
                    $("#m_daterangepicker_1 .form-control").val( start.format("YYYY-MM-DD") + " / " + end.format("YYYY-MM-DD"));
                });
                
                $(".tags_input").tagsinput({
                  trimValue: true,
                  tagClass: "filtertag"
                });
                $(".m-widget4__item").remove();
                
                $("#formFilter").on("submit", function(){
                    mApp.blockPage({
                        overlayColor: "#000000",
                        type: "loader",
                        state: "success",
                        message: "Please wait..."
                    });
                });
                
                $("#formFilter").on("keyup keypress", function(e) {
                    var keyCode = e.keyCode || e.which;
                    if (keyCode === 13) { 
                        e.preventDefault();
                        return false;
                    }
                });
                
                // ECHARTS
                require.config({
                    paths: {
                        echarts: "/assets/core/global/plugins/echarts/"
                    }
                });

                require(
                    [
                        "echarts",
                        "echarts/chart/pie"
                    ],
                    function(ec) {
                        '.$chartjs.'
                    }
                );
            }

            return {
                // public functions
                init: function() {
                    advdiscovery(); 
                }
            };
        }();

        var i = 0;
        function getDetailByPeriod(me){

            var cportlet = (me.seriesName+"-"+me.name).replace(/\ /g, "_");
            if($(".m-content > #portleDetail").find("."+cportlet).length>0){
                return false;
            }

            var field="";
            var value=me.name;
            if(me.seriesName=="Keyword"){
                field="content";
            } else if(me.seriesName=="Digital Media"){
                field="bundle";
            } else if(me.seriesName=="Sentiment"){
                field="is_sentiment";
            }

            var formData = $("#formFilter").serializeArray(); //console.log(formData);
            formData.push({ name: "ajax", value: 1 });
            formData.push({ name: "field", value: field });
            formData.push({ name: "value", value: value });

            $.ajax({
                url: "/bigbox/bigsearch/mediaanalytic",
                content: "json",
                method: "post",
                data: formData,
                beforeSend: function(){
                    mApp.blockPage({
                        overlayColor: "#000000",
                        type: "loader",
                        state: "success",
                        message: "Please wait..."
                    });
                },
                error: function(){
                },
                complete: function(res){
                    mApp.unblockPage();
                },
                success: function(res){
                    // console.log(res.data);

                    $(".m-content > #portleDetail").append(
                        "<div class=\"col-md-6\">"+
                            "<div class=\"m-portlet m-portlet--head-sm "+cportlet+" \">"+
                                "<div class=\"m-portlet__head\">"+
                                    "<div class=\"m-portlet__head-caption\">"+
                                        "<div class=\"m-portlet__head-title\">"+
                                            "<h3 class=\"m-portlet__head-text\">"+
                                                value+
                                                "<span class=\"m-portlet__head-desc\">"+
                                                    me.seriesName+
                                                "</span>"+
                                            "</h3>"+
                                        "</div>"+
                                    "</div>"+
                                    "<div class=\"m-portlet__head-tools\">"+
                                        "<ul class=\"m-portlet__nav\">"+
                                            "<li class=\"m-portlet__nav-item\">"+
                                                "<a class=\"m-portlet__nav-link m-portlet__nav-link--icon\" onclick=\"removePortlet(this);\">"+
                                                    "<i class=\"la la-close\"></i>"+
                                                "</a>"+
                                            "</li>"+
                                        "</ul>"+
                                    "</div>"+
                                "</div>"+
                                "<div class=\"m-portlet__body\">"+
                                    "<div id=\"container_"+i+"\" style=\"height:200px;\"></div>"+
                                "</div>"+
                            "</div>"+
                        "</div>"
                    );

                    $("."+cportlet).mPortlet().on("beforeRemove", function() {
                        $("."+cportlet).parent().remove();
                    });

                    Highcharts.chart("container_"+i, {
                        chart: {
                            zoomType: "x"
                        },
                        title: {
                            text: ""
                        },
                        subtitle: {
                            text: ""
                        },
                        xAxis: {
                            type: "datetime"
                        },
                        yAxis: {
                            title: {
                                text: ""
                            },
                            min: 0,
                            allowDecimals: false,
                        },
                        legend: {
                            enabled: false
                        },
                        plotOptions: {
                            column: {
                                fillColor: {
                                    linearGradient: {
                                        x1: 0,
                                        y1: 0,
                                        x2: 0,
                                        y2: 1
                                    },
                                    stops: [
                                        [0, Highcharts.getOptions().colors[0]],
                                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get("rgba")]
                                    ]
                                },
                                marker: {
                                    radius: 2
                                },
                                lineWidth: 1,
                                states: {
                                    hover: {
                                        lineWidth: 1
                                    }
                                },
                                threshold: null
                            },
                            series : {
					            cursor: "pointer",
					            point: {
					                events: {
					                    click: function () {

					                    	// console.log(this);

					                    	$(".btn-modal").data("entity_id",res.dataOthers.entity_id);
					                    	$(".btn-modal").data("x",this.x);
					                    	$(".btn-modal").data("field",field);
					                    	$(".btn-modal").data("value",value);
					                    	$(".btn-modal").data("formserialize",$("#formFilter").serialize());
											
											setTimeout(function(){
					                        	$(".btn-modal").click();
									        }, 300);
					                    }
					                }
					            }
                            }
                        },

                        series: [{
                            type: "column",
                            name: "Docs",
                            data: res.data
                        }]
                    });
                    i++;
                }

            });
        }

        function removePortlet(me){
            $(me).parent().parent().parent().parent().parent().parent().remove();
        }

        jQuery(document).ready(function(){
            Page.init();
        });');
}
public function twitteranalyticAction(){

    // if($_POST){
    //     // Zend_Debug::dump($_POST);die();
    // }

    $params = $this->getRequest()->getParams();

    if(isset($_POST["ajax"])){
		if(isset($_POST['ajax']) && (int)$_POST['ajax']==1){

		    // Zend_Debug::dump($_POST);//die();
		    $p = array(
		        'fq'=>array(
		            'startdate'=>$_POST['startdate'],
		            'enddate'=>$_POST['enddate'],
		            'keys'=>array_filter(explode(',',$_POST['keysearch'])),
		            'and'=>array_filter(explode(',',$_POST['andsearch'])),
		            'exception'=>array_filter(explode(',',$_POST['exsearch'])),
		            'user_location'=>$_POST['user_location'],
		            'entity_id'=>$_POST['entity_id'],
		            'field'=>$_POST['field'],
		            'value'=>$_POST['value'],
		        ),
		    );

		    // Zend_Debug::dump($p);die();

		    $solr = new Bigbox_Model_Solr();
		    $res = $solr->get_mediaanalytic_trend($p);
		    // Zend_Debug::dump($res);die();

		    $result = $res;
		    $result['data'] = array();
		    $result['dataOthers'] = array('entity_id'=>$_POST['entity_id'],'field'=>$_POST['field'],'value'=>$_POST['value']);
		    foreach ($res['data']['facet_counts']['facet_ranges']['content_date']['counts'] as $key => $value) {
		        $date = strtotime(substr($key, 0,10).' +7 hours')*1000;
		        $result['data'][] = array($date,$value);
		    }
		    // Zend_Debug::dump($result);die();




		    header("Access-Control-Allow-Origin: *");
		    header('Content-Type: application/json');
		    echo json_encode($result);
		    die();
		} else if(isset($_POST['ajax']) && (int)$_POST['ajax']==2){

		    // Zend_Debug::dump($_POST);die();

		    $p = array(
		    	'fq'=>array(
		    		'keysearch'=>($_POST['keysearch']!=''?explode(',', $_POST['keysearch']):array()),
		    		'andsearch'=>($_POST['andsearch']!=''?explode(',', $_POST['andsearch']):array()),
		    		'exsearch'=>($_POST['exsearch']!=''?explode(',', $_POST['exsearch']):array()),
		    		'user_location'=>$_POST['user_location'],
		    		'startdate'=>$_POST['startdate'],
		    		'enddate'=>$_POST['enddate'],
		    		'entity_id'=>$_POST['entity_id'],
		    		'content_date'=>date('Y-m-d',($_POST['timestamp']/1000)),
		    		'field'=>$_POST['field'],
		    		'value'=>$_POST['value'],
		    	),
		    );

		    // Zend_Debug::dump($p);die();

		    $solr = new Bigbox_Model_Solr();
		    $data = $solr->get_docTwitter($p);
		    // Zend_Debug::dump($data);die();

		    $result = array();
		    $result = $data;
		    $result["data"] = array(
		    	"numFound"=>$data["data"]->response->numFound,
		    	"docs"=>array(),
		    );

		    foreach ((array)$data["data"]->response->docs as $key => $value) {

		    	$content_date = date("Y-m-d H:i:s",strtotime($value["content_date"]));

		    	$result["data"]["docs"][$key]  = $value;
		    	$result["data"]["docs"][$key]["content_date_format"]  = $content_date;

		    	if(isset($value["ss_icon"]) && $value["ss_icon"]==""){
		    		$result["data"]["docs"][$key]["ss_icon"] = "/assets/pmas/images/media/twitter.png";
		    	}

		    }



		    header("Access-Control-Allow-Origin: *");
		    header('Content-Type: application/json');
		    echo json_encode($result);
		    die();
		} else {

			$result = array(
				"transaction"=>false,
				"params"=>$_POST,
				"data"=>false,
				"message"=>"invalid identifier",
			);


		    header("Access-Control-Allow-Origin: *");
		    header('Content-Type: application/json');
		    echo json_encode($result);
		    die();
		}
	}

    if(!isset($_POST['startdate']) || $_POST['startdate']==''){
        $startdate = date('Y-m-d', strtotime(date('Y-m-d').' -1 days'));
    } else {
        $startdate = $_POST['startdate'];
    }
    if(!isset($params['enddate']) || $params['enddate']==''){
        $enddate = date('Y-m-d');
    } else {
        $enddate = $_POST['enddate'];
    }
    // Zend_Debug::dump($params);die();

    $solr = new Bigbox_Model_Solr();

    $userLoc = $solr->getTwitterUserLocation();
    // Zend_Debug::dump($userLoc);die();



    $p = array(
        'startdate'=>$startdate,
        'enddate'=>$enddate,
        'fq'=>array(
            'keys'=>array(),
            'and'=>array(),
            'exception'=>array(),
            'user_location'=>$_POST['user_location'],
        ),
    );
    if(isset($_POST['andsearch']) && $_POST['andsearch']!=''){
        $p['fq']['and'] = explode(',', $_POST['andsearch']);
    }
    if(isset($_POST['exsearch']) && $_POST['exsearch']!=''){
        $p['fq']['exception'] = explode(',', $_POST['exsearch']);
    }
    if(isset($_POST['keysearch']) && $_POST['keysearch']!=''){
        $p['fq']['keys'] = explode(',', $_POST['keysearch']); //Zend_Debug::dump($keysearch);die();
    }
    // Zend_Debug::dump($p);die();
    $data = $solr->get_twitteranalytic($p);
    // Zend_Debug::dump($data);die();


    $starData = array(); $legendData = array();
    $pie = array(
        'top3'=>array(
            'name'=>'Username',
            'data'=>array(),
        ),
        'top10'=>array(
            'name'=>'Username',
            'data'=>array(),
        ),
        'others'=>array(
            'name'=>'Username',
            'data'=>array(),
        ),
    );
    if($data['transaction']){       
        //==================================
        $i = 0; $numFound = $data['data']['numFound']; 
        $pie['top3']['name'] = 'Type';
        if(count($data['data']['keywords'])==0){
            $legendData[] = "--TYPE--";
            foreach ($data['data']['ss_is_quote_status'] as $name => $value) {
                $percentOfTotal = round($value*100/$numFound,2);
                $pie['top3']['data'][] = array(
                    'name'=>$name,
                    'value'=>$value,
                    'percentOfTotal'=>$percentOfTotal,
                );
                $legendData[] = $name;
            }
        } else if(count($data['data']['keywords'])>0){
            $pie['top3']['name'] = 'Keyword';
            $legendData[] = "--KEYWORDS--";
            foreach ($data['data']['keywords'] as $name => $value) {
                $percentOfTotal = round($value*100/$numFound,2);
                $pie['top3']['data'][] = array(
                    'name'=>$name,
                    'value'=>$value,
                    'percentOfTotal'=>$percentOfTotal,
                );
                $legendData[] = $name;
            }
        }
        //==================================
        $i = 0;
        if($p['fq']['user_location']=='none'){
            $pie['top10']['name'] = 'Location';
            $legendData[] = "--LOCATION--";
            foreach ($data['data']['ss_user_location'] as $name => $value) {
                if($i<10){
                    $percentOfTotal = round($value*100/$numFound,2);
                    $pie['top10']['data'][] = array(
                        'name'=>$name,
                        'value'=>$value,
                        'percentOfTotal'=>$percentOfTotal,
                    );
                    $legendData[] = $name;
                } else {
                    break;
                }
                $i++;
            }
        } else {
            $pie['top10']['name'] = 'Username';
            $legendData[] = "---USERNAME-";
            foreach ($data['data']['ss_userScreenName'] as $name => $value) {
                if($i<10){
                    $percentOfTotal = round($value*100/$numFound,2);
                    $pie['top10']['data'][] = array(
                        'name'=>$name,
                        'value'=>$value,
                        'percentOfTotal'=>$percentOfTotal,
                    );
                    $legendData[] = $name;
                } else {
                    break;
                }
                $i++;
            }
        }

        //==================================
        $pie['others']['name'] = 'Sentiment';
        $legendData[] = "---SENTIMENT-";
        foreach ($data['data']['is_sentiment'] as $name => $value) {
            $percentOfTotal = round($value*100/$numFound,2);
            $pie['others']['data'][] = array(
                'name'=>$name,
                'value'=>$value,
                'percentOfTotal'=>$percentOfTotal,
            );
            $legendData[] = $name;
        }

        $starData = array(
            array(
                'name'=>"Total",
                'value'=>$numFound,
                'x'=>"55%",
                'y'=>50,
                'symbolSize'=>32
            ),        
        );
    }

    // Zend_Debug::dump($pie);die();

    $this->view->p = $p;
    $this->view->userLoc = $userLoc;

    $this->view->headScript()->appendFile('/assets/core/global/plugins/echarts/echarts.js');
    $this->view->headScript()->appendFile('/assets/core/global/plugins/echarts/chart/pie.js');
    $this->view->headLink()->appendStylesheet("/assets/core/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css");
    $this->view->headScript()->appendFile("/assets/core/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js");
    $this->view->headScript()->appendFile('/assets/bigbox/plugins/Highcharts-6.0.7/highcharts.js');
    $this->view->headScript()->appendFile('/assets/bigbox/plugins/Highcharts-6.0.7/modules/exporting.js');

    $chartjs = '
    var echarts_pie = ec.init(document.getElementById("echarts_pie"));

    echarts_pie.setOption({
                tooltip: {
                    show: true,
                    formatter: function(params) {
                        if(params.seriesName=="Digital Media" && params.name=="Total"){
                            return "Docs<br\>"+params.name+": "+params.value
                        } else {
                            return params.seriesName+"<br\>"+params.name+": "+params.value
                        }
                    }
                },
                legend: {
                    orient: "vertical",
                    x: "left",
                    data: '.json_encode($legendData).'
                },
                toolbox: {
                    show: true,
                    feature: {
                        mark: {
                            show: true
                        },
                        dataView: {
                            show: true,
                            readOnly: false
                        },
                        restore: {
                            show: true
                        },
                        saveAsImage: {
                            show: true
                        }
                    }
                },
                calculable: true,
                series: [{
                    name: "'.$pie['top3']['name'].'",
                    type: "pie",
                    center: ["35%", 200],
                    radius: 80,
                    itemStyle: {
                        normal: {
                            label: {
                                position: "inner",
                                formatter: function(params) {
                                    return params.name+"\n"+params.data.percentOfTotal+"%"
                                }
                            },
                            labelLine: {
                                show: false
                            }
                        },
                        emphasis: {
                            label: {
                                show: true,
                                formatter: "{b}\n{c}"
                            }
                        }
                    },
                    data: '.json_encode($pie['top3']['data']).',
                }, {
                    name: "'.$pie['top10']['name'].'",
                    type: "pie",
                    center: ["35%", 200],
                    radius: [110, 140],
                    itemStyle: {
                        normal: {
                            label: {
                                formatter: function(params) {
                                    return params.name+"\n"+params.data.percentOfTotal+"%"
                                }
                            },
                        },
                    },
                    data: '.json_encode($pie['top10']['data']).',
                    markPoint: {
                        symbol: "star",
                        data: '.json_encode($starData).'
                    }
                }, {
                    name: "'.$pie['others']['name'].'",
                    type: "pie",
                    clockWise: true,
                    startAngle: 135,
                    center: ["75%", 200],
                    radius: [80, 120],
                    itemStyle: {
                        normal: {
                            label: {
                                formatter: function(params) {
                                    return params.name+"\n"+params.data.percentOfTotal+"%"
                                }
                            },
                        },
                    },
                    data: '.json_encode($pie['others']['data']).',
                }]
    });
    window.onresize = echarts_pie.resize;

    echarts_pie.on("click", function (params){
        // console.log(params);
        getDetailByPeriod(params);
    });

    $(".btn-modal").on("click", function(){
    	// console.log($(this).data());

        var formData = $("#formFilter").serializeArray(); //console.log(formData);
        formData.push({ name: "ajax", value: 2 });
        formData.push({ name: "entity_id", value: 5 });
        formData.push({ name: "field", value: $(this).data("field") });
        formData.push({ name: "value", value: $(this).data("value") });
        formData.push({ name: "timestamp", value: $(this).data("x") });

        var modaltitle = $(this).data("value");

        $.ajax({
            url: "/bigbox/bigsearch/twitteranalytic",
            content: "json",
            method: "post",
            data: formData,
            beforeSend: function(){
            	$("#m_modal_4 .modal-dialog .modal-content .modal-header .modal-title").html(modaltitle);
	            mApp.block("#m_modal_4 .modal-content", {
	                overlayColor: "#000000",
	                type: "loader",
	                state: "success",
	                size: "lg",
	            });
            },
            error: function(){
            },
            complete: function(res){
                mApp.unblock("#m_modal_4 .modal-content");
            },
            success: function(res){
            	// console.log(res);

            	var html = "";
            	if(res.data!=undefined && res.data.docs!=undefined && res.data.docs.length>0){
            	html +=""+
            	"<div id=\"scroller_fk\" class=\"m-widget4 m-scrollable\" data-scrollbar-shown=\"true\">"+
            		"<div class=\"m-widget3\">";
            	$.each(res.data.docs, function(k,v){
            		html +=""+
            			"<div class=\"m-widget3__item\">"+
							"<div class=\"m-widget3__header\">"+
								"<div class=\"m-widget3__user-img\">"+
									"<img class=\"m-widget3__img\" src=\""+(v.ss_user_profile_image_url_https!=""?v.ss_user_profile_image_url_https:"/assets/pmas/images/media/twitter.png")+"\">"+
								"</div>"+
								"<div class=\"m-widget3__info\">"+
									"<span class=\"m-widget3__username\">"+
										"<a href=\"https://twitter.com/"+v.ss_userScreenName+"/status/"+v.id+"\" target=\"_blank\">"+v.ss_user_name+" @"+v.ss_userScreenName+"</a>"+
									"</span>"+
									"<span class=\"m-widget3__time pull-right\">"+
										v.content_date_format+										
									"</span>"+
									"<br>"+
									v.content+
								"</div>"+
								"<span class=\"m-widget3__status m--font-info\">"+
								"</span>"+
							"</div>"+
						"</div>";

            	});
            	html +=""+
					"</div>"+
            	"</div>"+
            	"";
            	}

            	$("#m_modal_4 > .modal-dialog .modal-content .modal-body").html(html);
            },
        });
    });

    ';



    $this->view->headScript()->appendScript('
        var Page = function () {
        
            //== Private functions
            var advdiscovery = function () {
                var txt = $(".m_autosize");
                autosize(txt);
                
                // console.log(moment.locale());
                var _start = moment().subtract(1, "days");
                var _end = moment();
                //console.log(_start);
                //console.log(_end);
                $("#m_daterangepicker_1").daterangepicker({
                    buttonClasses: "m-btn btn",
                    applyClass: "btn-primary",
                    cancelClass: "btn-secondary",
                    startDate: _start,
                    endDate: _end,
                    maxDate : moment(),
                    ranges: {
                       "Today": [moment(), moment()],
                       "Yesterday": [moment().subtract(1, "days"), moment().subtract(1, "days")],
                       "Last 7 Days": [moment().subtract(6, "days"), moment()],
                       "Last 30 Days": [moment().subtract(29, "days"), moment()],
                       "This Month": [moment().startOf("month"), moment().endOf("month")],
                       "Last Month": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
                    }
                }, function(start, end, label) {
                    $("#startdate").val(start.format("YYYY-MM-DD"));
                    $("#enddate").val(end.format("YYYY-MM-DD"));
                    $("#m_daterangepicker_1 .form-control").val( start.format("YYYY-MM-DD") + " / " + end.format("YYYY-MM-DD"));
                });
                
                $(".tags_input").tagsinput({
                  trimValue: true,
                  tagClass: "filtertag"
                });
                $(".m-widget4__item").remove();
                
                $("#formFilter").on("submit", function(){
                    mApp.blockPage({
                        overlayColor: "#000000",
                        type: "loader",
                        state: "success",
                        message: "Please wait..."
                    });
                });
                
                $("#formFilter").on("keyup keypress", function(e) {
                    var keyCode = e.keyCode || e.which;
                    if (keyCode === 13) { 
                        e.preventDefault();
                        return false;
                    }
                });
                
                // ECHARTS
                require.config({
                    paths: {
                        echarts: "/assets/core/global/plugins/echarts/"
                    }
                });

                require(
                    [
                        "echarts",
                        "echarts/chart/pie"
                    ],
                    function(ec) {
                        '.$chartjs.'
                    }
                );
            }

            return {
                // public functions
                init: function() {
                    advdiscovery(); 
                }
            };
        }();

        var i = 0;
        function getDetailByPeriod(me){

            var cportlet = (me.seriesName+"-"+me.name).replace(/\ /g, "_");
            if($(".m-content > #portleDetail").find("."+cportlet).length>0){
                return false;
            }

            var field="";
            var value=me.name;
            if(me.seriesName=="Keyword"){
                field="content";
            } else if(me.seriesName=="Type"){
                field="ss_is_quote_status";
                if(value=="Tweet"){
                	value="1";
                } else {
                    value="";
                }
            } else if(me.seriesName=="Username"){
                field="ss_userScreenName";
            } else if(me.seriesName=="Location"){
                field="ss_user_location";
                if(value=="NULL"){
                    value="";
                }
            } else if(me.seriesName=="Sentiment"){
                field="is_sentiment";
            }

            var formData = $("#formFilter").serializeArray(); //console.log(formData);
            formData.push({ name: "ajax", value: 1 });
            formData.push({ name: "entity_id", value: 5 });
            formData.push({ name: "field", value: field });
            formData.push({ name: "value", value: value });

            // console.log(formData);

            $.ajax({
                url: "/bigbox/bigsearch/twitteranalytic",
                content: "json",
                method: "post",
                data: formData,
                beforeSend: function(){
                    mApp.blockPage({
                        overlayColor: "#000000",
                        type: "loader",
                        state: "success",
                        message: "Please wait..."
                    });
                },
                error: function(){
                },
                complete: function(res){
                    mApp.unblockPage();
                },
                success: function(res){
                    // console.log(res);

                    $(".m-content > #portleDetail").append(
                        "<div class=\"col-md-6\">"+
                            "<div class=\"m-portlet m-portlet--head-sm "+cportlet+" \">"+
                                "<div class=\"m-portlet__head\">"+
                                    "<div class=\"m-portlet__head-caption\">"+
                                        "<div class=\"m-portlet__head-title\">"+
                                            "<h3 class=\"m-portlet__head-text\">"+
                                                value+
                                                "<span class=\"m-portlet__head-desc\">"+
                                                    me.seriesName+
                                                "</span>"+
                                            "</h3>"+
                                        "</div>"+
                                    "</div>"+
                                    "<div class=\"m-portlet__head-tools\">"+
                                        "<ul class=\"m-portlet__nav\">"+
                                            "<li class=\"m-portlet__nav-item\">"+
                                                "<a class=\"m-portlet__nav-link m-portlet__nav-link--icon\" onclick=\"removePortlet(this);\">"+
                                                    "<i class=\"la la-close\"></i>"+
                                                "</a>"+
                                            "</li>"+
                                        "</ul>"+
                                    "</div>"+
                                "</div>"+
                                "<div class=\"m-portlet__body\">"+
                                    "<div id=\"container_"+i+"\" style=\"height:200px;\"></div>"+
                                "</div>"+
                            "</div>"+
                        "</div>"
                    );

                    $("."+cportlet).mPortlet().on("beforeRemove", function() {
                        $("."+cportlet).parent().remove();
                    });

                    Highcharts.chart("container_"+i, {
                        chart: {
                            zoomType: "x"
                        },
                        title: {
                            text: ""
                        },
                        subtitle: {
                            text: ""
                        },
                        xAxis: {
                            type: "datetime"
                        },
                        yAxis: {
                            title: {
                                text: ""
                            },
                            min: 0,
                            allowDecimals: false,
                        },
                        legend: {
                            enabled: false
                        },
                        plotOptions: {
                            column: {
                                fillColor: {
                                    linearGradient: {
                                        x1: 0,
                                        y1: 0,
                                        x2: 0,
                                        y2: 1
                                    },
                                    stops: [
                                        [0, Highcharts.getOptions().colors[0]],
                                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get("rgba")]
                                    ]
                                },
                                marker: {
                                    radius: 2
                                },
                                lineWidth: 1,
                                states: {
                                    hover: {
                                        lineWidth: 1
                                    }
                                },
                                threshold: null
                            },
                            series : {
					            cursor: "pointer",
					            point: {
					                events: {
					                    click: function () {

					                    	// console.log(this);

					                    	$(".btn-modal").data("entity_id",res.dataOthers.entity_id);
					                    	$(".btn-modal").data("x",this.x);
					                    	$(".btn-modal").data("field",field);
					                    	$(".btn-modal").data("value",value);
					                    	$(".btn-modal").data("formserialize",$("#formFilter").serialize());
											
											setTimeout(function(){
					                        	$(".btn-modal").click();
									        }, 300);
					                    }
					                }
					            }
                            }
                        },

                        series: [{
                            type: "column",
                            name: "Docs",
                            data: res.data
                        }]
                    });
                    i++;
                }

            });
        }

        function removePortlet(me){
            $(me).parent().parent().parent().parent().parent().parent().remove();
        }

        jQuery(document).ready(function(){
            Page.init();
        });');
}

public function analyticAction(){

    $params = $this->getRequest()->getParams(); 
    // Zend_Debug::dump($params);//die();

    $source = array(
    	array(
    		"entity_id"		=>1,
    		"entity_name"	=>"metadata",
    		"display"		=>"Metadata",
    	),
    	array(
    		"entity_id"		=>2,
    		"entity_name"	=>"digital-media",
    		"display"		=>"Digital Media",
    	),
    	array(
    		"entity_id"		=>3,
    		"entity_name"	=>"document",
    		"display"		=>"Document",
    	),
    	array(
    		"entity_id"		=>5,
    		"entity_name"	=>"twitter",
    		"display"		=>"Twitter",
    	),
    );
    // Zend_Debug::dump($source);

    $entity_id = -1;
    $entity_name = "";
    $display = "";
    $list_entity_name = array_column($source, "entity_name");
    if(in_array($params["source"], $list_entity_name)){
    	$key = array_search($params["source"], $list_entity_name);
    	$entity_id = $source[$key]["entity_id"];
    	$entity_name = $source[$key]["entity_name"];
    	$display = $source[$key]["display"];

	    $solr = new Bigbox_Model_Solr();
    }
    // Zend_Debug::dump($entity_id);die();

	$startdate = date("Y-m-d", strtotime(date("Y-m-d")." -1 days"));
	$enddate = date("Y-m-d");
	if(isset($_POST["startdate"]) && $_POST["startdate"]!=""){
		$startdate = $_POST["startdate"];
	}
	if(isset($_POST["enddate"]) && $_POST["enddate"]!=""){
		$enddate = $_POST["enddate"];
	}
    // Zend_Debug::dump($params);die();


    $p = array(
    	"entity_id"		=>$entity_id,
    	"entity_name"	=>$entity_name,
    	"display"		=>$display,
    	"startdate"		=>$startdate,
    	"enddate"		=>$enddate,
    	"keysearch" 	=>$_POST["keysearch"],
    	"andsearch" 	=>$_POST["andsearch"],
    	"excsearch" 	=>$_POST["excsearch"],
    );
    // Zend_Debug::dump($p);die();

    $starData = array(); $legendData = array();
    $pie1 = array("name"=>"","data"=>array()); $pie2=$pie1; $pie3=$pie1;

    $o = array(
    	"setRows"=>0,
    	"setFacet"=>true,
    	"addFilterQuery"=>array(
    		'entity_id:"'.$p["entity_id"].'"',
    		'content_date:['.$p["startdate"].'T00:00:00Z TO '.$p["enddate"].'T23:59:59Z]',
    	),
    );

	if($p["entity_id"]==2){

    	// Zend_Debug::dump($p);die();

		$pie1["name"] = "Digital Media"; $pie2["name"] = "Digital Media"; $pie3["name"] = "Digital Media";

		$o ["addFacetField"] = array(
    		"bundle",
    		"is_sentiment"
    	);

    	if(isset($p["keysearch"]) && $p["keysearch"]!=""){
    		$keysearch = explode(",", $p["keysearch"]);

    		$o["addFilterQuery"][] = 'content:("*'.implode('*" OR "*', $keysearch).'*")';

    		foreach ($keysearch as $key => $value) {
    			$o["addFacetQuery"][] = 'content:"*'.$value.'*"';
    		}
    	}
    	if(isset($p["andsearch"]) && $p["andsearch"]!=""){
    		$andsearch = explode(",", $p["andsearch"]);

    		$o["addFilterQuery"][] = 'content:("*'.implode('*" OR "*', $andsearch).'*")';
    	}

	    $res = $solr->execute($o);
	    // Zend_Debug::dump($res);die();

	    if($res["transaction"]){
	    	$res["data"]->facet_counts->facet_fields->bundle = (object)array_filter((array)$res["data"]->facet_counts->facet_fields->bundle);

	    	$numFound = $res["data"]->response->numFound;

	    	$starData = array(
	            array(
	                'name'=>"Total",
	                'value'=>$numFound,
	                'x'=>"55%",
	                'y'=>50,
	                'symbolSize'=>32
	            ),
	    	);

	    	// Zend_Debug::dump($res["data"]->facet_counts->facet_fields->bundle);

    		if(isset($p["keysearch"]) && $p["keysearch"]!=""){
	    		$pie1["name"] = "Keyword";
    			$legendData[] = "--[Keyword]--";
	    		foreach ($res["data"]->facet_counts->facet_queries as $key => $value) {

	    			$key = substr($key, 10,-2);
	    			$legendData[] = $key;

	    			$percentOfTotal = round($value*100/$numFound,2);
	    			$pie1["data"][] = array(
	    				"name"				=>$key,
	    				"value"				=>$value,
	    				"percentOfTotal"	=>$percentOfTotal,
	    			);
	    		}

	    		$pie2["name"] = "Digital Media";
    			$legendData[] = "--[Digital Media]--";
		    	foreach ($res["data"]->facet_counts->facet_fields->bundle as $key => $value) {
	    			$legendData[] = $key;
	    			$percentOfTotal = round($value*100/$numFound,2);
	    			$pie2["data"][] = array(
	    				"name"				=>$key,
	    				"value"				=>$value,
	    				"percentOfTotal"	=>$percentOfTotal,
	    			);
	    		}

				$pie3["name"] = "Sentiment";
    			$legendData[] = "--[Sentiment]--";
	    		$sentiment = array("netral","positif","negatif");
	    		foreach ($res["data"]->facet_counts->facet_fields->is_sentiment as $key => $value) {

	    			$key = $sentiment[$key];
	    			$legendData[] = $key;

	    			$percentOfTotal = round($value*100/$numFound,2);
	    			$pie3["data"][] = array(
	    				"name"				=>$key,
	    				"value"				=>$value,
	    				"percentOfTotal"	=>$percentOfTotal,
	    			);
	    		}

	    	} else {
		    	$i = 0;
		    	foreach ($res["data"]->facet_counts->facet_fields->bundle as $key => $value) {
		    		if($value>0){
		    			$legendData[] = $key;
		    			$percentOfTotal = round($value*100/$numFound,2);
			    		if($i < 3){
			    			$pie1["data"][] = array(
			    				"name"				=>$key,
			    				"value"				=>$value,
			    				"percentOfTotal"	=>$percentOfTotal,
			    			);
			    		} else if($i < 10){
			    			$pie2["data"][] = array(
			    				"name"				=>$key,
			    				"value"				=>$value,
			    				"percentOfTotal"	=>$percentOfTotal,
			    			);
			    		} else {
			    			$pie3["data"][] = array(
			    				"name"				=>$key,
			    				"value"				=>$value,
			    				"percentOfTotal"	=>$percentOfTotal,
			    			);
			    		}
			    		$i++;
		    		} else {
		    			break;
		    		}
		    	}
	    	}
	    }
	} else if($p["entity_id"]==5){

		$pie1["name"] = "Source"; $pie2["name"] = "Source"; $pie3["name"] = "Source";

		$o["addFacetField"] = array(
    		"is_sentiment",
    		"ss_source",
    	);
    	// $o["addFacetQuery"] = array(
    	// 	'ss_retweeted_status_id:["" TO *]',
    	// 	'ss_in_reply_to_status_id:["" TO *]',
    	// 	'ss_quoted_status_id:["" TO *]',
    	// );

    	if(isset($p["keysearch"]) && $p["keysearch"]!=""){
    		$keysearch = explode(",", $p["keysearch"]);

    		$o["addFilterQuery"][] = 'content:("*'.implode('*" OR "*', $keysearch).'*")';

    		foreach ($keysearch as $key => $value) {
    			$o["addFacetQuery"][] = 'content:"*'.$value.'*"';
    		}
    	}
    	if(isset($p["andsearch"]) && $p["andsearch"]!=""){
    		$andsearch = explode(",", $p["andsearch"]);

    		$o["addFilterQuery"][] = 'content:("*'.implode('*" OR "*', $andsearch).'*")';
    	}

    	// Zend_Debug::dump($o);die();
	    $res = $solr->execute($o);
	    // Zend_Debug::dump($res);die();

	    if($res["transaction"]){
	    	$res["data"]->facet_counts->facet_fields->ss_source = (object)array_filter((array)$res["data"]->facet_counts->facet_fields->ss_source);
	    	// Zend_Debug::dump($res);die();

	    	$numFound = $res["data"]->response->numFound;

	    	$starData = array(
	            array(
	                'name'=>"Total",
	                'value'=>$numFound,
	                'x'=>"55%",
	                'y'=>50,
	                'symbolSize'=>32
	            ),
	    	);

	    	// Zend_Debug::dump(count($res["data"]->facet_counts->facet_queries));die();
    		if(isset($p["keysearch"]) && $p["keysearch"]!=""){
	    		$pie1["name"] = "Keyword";
    			$legendData[] = "--[Keyword]--";
	    		foreach ($res["data"]->facet_counts->facet_queries as $key => $value) {

	    			$key = substr($key, 10,-2);
	    			$legendData[] = $key;

	    			$percentOfTotal = round($value*100/$numFound,2);
	    			$pie1["data"][] = array(
	    				"name"				=>$key,
	    				"value"				=>$value,
	    				"percentOfTotal"	=>$percentOfTotal,
	    			);
	    		}

	    		$pie2["name"] = "Source";
    			$legendData[] = "--[Source]--";
    			$i = 0;
		    	foreach ($res["data"]->facet_counts->facet_fields->ss_source as $key => $value) {		    		
		    		$key = substr($key, (strpos($key, ">")+1), -4);
	    			$legendData[] = $key;
	    			$percentOfTotal = round($value*100/$numFound,2);
	    			$pie2["data"][] = array(
	    				"name"				=>$key,
	    				"value"				=>$value,
	    				"percentOfTotal"	=>$percentOfTotal,
	    			);

	    			$i++;
	    			if($i>9){
	    				break;
	    			}
	    		}

				$pie3["name"] = "Sentiment";
    			$legendData[] = "--[Sentiment]--";
	    		$sentiment = array("netral","positif","negatif");
	    		foreach ($res["data"]->facet_counts->facet_fields->is_sentiment as $key => $value) {

	    			$key = $sentiment[$key];
	    			$legendData[] = $key;

	    			$percentOfTotal = round($value*100/$numFound,2);
	    			$pie3["data"][] = array(
	    				"name"				=>$key,
	    				"value"				=>$value,
	    				"percentOfTotal"	=>$percentOfTotal,
	    			);
	    		}

	    	} else {
	    		// Zend_Debug::dump($res["data"]->facet_counts->facet_fields->ss_source);die();
		    	$i = 0;
			    foreach ($res["data"]->facet_counts->facet_fields->ss_source as $key => $value) {

			    	if($value>1){
			    		$key = substr($key, (strpos($key, ">")+1), -4);
		    			
		    			$percentOfTotal = round($value*100/$numFound,2);
			    		if($i < 3){
			    			$pie1["data"][] = array(
			    				"name"				=>$key,
			    				"value"				=>$value,
			    				"percentOfTotal"	=>$percentOfTotal,
			    			);
			    		} else if($i < 10){
			    			$pie2["data"][] = array(
			    				"name"				=>$key,
			    				"value"				=>$value,
			    				"percentOfTotal"	=>$percentOfTotal,
			    			);
			    		} else if($i < 20){
			    			$pie3["data"][] = array(
			    				"name"				=>$key,
			    				"value"				=>$value,
			    				"percentOfTotal"	=>$percentOfTotal,
			    			);
			    		} else {
			    			break;
			    		}

		    			$legendData[] = $key;
			    		$i++;
			    	} else {
			    		break;
			    	}
		    	}
	    	}
	    }
	}

    // Zend_Debug::dump($pie1);
    // Zend_Debug::dump($pie2);
    // Zend_Debug::dump($pie3);
    // die();

    $this->view->p = $p;

    $this->view->headScript()->appendFile('/assets/core/global/plugins/echarts/echarts.js');
    $this->view->headScript()->appendFile('/assets/core/global/plugins/echarts/chart/pie.js');
    $this->view->headLink()->appendStylesheet("/assets/core/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css");
    $this->view->headScript()->appendFile("/assets/core/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js");
	$this->view->headScript()->appendFile('/assets/bigbox/plugins/Highcharts-6.0.7/highcharts.js');
	$this->view->headScript()->appendFile('/assets/bigbox/plugins/Highcharts-6.0.7/modules/exporting.js');

    $chartjs = '
    var echarts_pie = ec.init(document.getElementById("echarts_pie"));

    echarts_pie.setOption({
                tooltip: {
                    show: true,
                    formatter: function(params) {
                        if(params.seriesName=="Digital Media" && params.name=="Total"){
                            return "Docs<br\>"+params.name+": "+params.value
                        } else {
                            return params.seriesName+"<br\>"+params.name+": "+params.value
                        }
                    }
                },
                legend: {
                    orient: "vertical",
                    x: "left",
                    // y: "top",
                    data: '.json_encode($legendData).'
                },
                toolbox: {
                    show: true,
                    // x: "left",
                    // y: "top",
                    feature: {
                        // mark: {
                        //     show: true
                        // },
                        // dataView: {
                        //     show: true,
                        //     readOnly: false
                        // },
                        restore: {
                            show: true
                        },
                        saveAsImage: {
                            show: true
                        }
                    }
                },
                calculable: true,
                series: [{
                    name: "'.$pie1['name'].'",
                    type: "pie",
                    center: ["35%", 200],
                    radius: 80,
                    itemStyle: {
                        normal: {
                            label: {
                                position: "inner",
                                formatter: function(params) {
                                    return params.name+"\n"+params.data.percentOfTotal+"%"
                                }
                            },
                            labelLine: {
                                show: false
                            }
                        },
                        emphasis: {
                            label: {
                                show: true,
                                formatter: "{b}\n{c}"
                            }
                        }
                    },
                    data: '.json_encode($pie1['data']).',
                }, {
                    name: "'.$pie2['name'].'",
                    type: "pie",
                    center: ["35%", 200],
                    radius: [110, 140],
                    itemStyle: {
                        normal: {
                            label: {
                                formatter: function(params) {
                                    return params.name+"\n"+params.data.percentOfTotal+"%"
                                }
                            },
                        },
                    },
                    data: '.json_encode($pie2['data']).',
	                markPoint: {
	                    symbol: "star",
	                    data: '.json_encode($starData).'
	                }
                }, {
                    name: "'.$pie3['name'].'",
                    type: "pie",
                    clockWise: true,
                    startAngle: 135,
                    center: ["75%", 200],
                    radius: [80, 120],
                    itemStyle: {
                        normal: {
                            label: {
                                formatter: function(params) {
                                    return params.name+"\n"+params.data.percentOfTotal+"%"
                                }
                            },
                        },
                    },
                    data: '.json_encode($pie3['data']).',
                }]
    });
    window.onresize = echarts_pie.resize;

    echarts_pie.on("click", function (params){
    	// console.log(params);
        getDetailByPeriod(params);
    });';

    $this->view->headScript()->appendScript('
        var Page = function () {
        
            //== Private functions
            var advdiscovery = function () {
                var txt = $(".m_autosize");
                autosize(txt);
                
                // console.log(moment.locale());
                var _start = moment().subtract(1, "days");
                var _end = moment();
                //console.log(_start);
                //console.log(_end);
                $("#m_daterangepicker_1").daterangepicker({
                    buttonClasses: "m-btn btn",
                    applyClass: "btn-primary",
                    cancelClass: "btn-secondary",
                    startDate: _start,
                    endDate: _end,
                    maxDate : moment(),
                    ranges: {
                       "Today": [moment(), moment()],
                       "Yesterday": [moment().subtract(1, "days"), moment().subtract(1, "days")],
                       "Last 7 Days": [moment().subtract(6, "days"), moment()],
                       "Last 30 Days": [moment().subtract(29, "days"), moment()],
                       "This Month": [moment().startOf("month"), moment().endOf("month")],
                       "Last Month": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
                    }
                }, function(start, end, label) {
                    $("#startdate").val(start.format("YYYY-MM-DD"));
                    $("#enddate").val(end.format("YYYY-MM-DD"));
                    $("#m_daterangepicker_1 .form-control").val( start.format("YYYY-MM-DD") + " / " + end.format("YYYY-MM-DD"));
                });
                
                $(".tags_input").tagsinput({
                  trimValue: true,
                  tagClass: "filtertag"
                });
                $(".m-widget4__item").remove();
                
                $("#formFilter").on("submit", function(){
                    mApp.blockPage({
                        overlayColor: "#000000",
                        type: "loader",
                        state: "success",
                        message: "Please wait..."
                    });
                });
                
                $("#formFilter").on("keyup keypress", function(e) {
                    var keyCode = e.keyCode || e.which;
                    if (keyCode === 13) { 
                        e.preventDefault();
                        return false;
                    }
                });
                
                // ECHARTS
                require.config({
                    paths: {
                        echarts: "/assets/core/global/plugins/echarts/"
                    }
                });

                require(
                    [
                        "echarts",
                        "echarts/chart/pie"
                    ],
                    function(ec) {
                        '.$chartjs.'
                    }
                );
            }

            return {
                // public functions
                init: function() {
                    advdiscovery(); 
                }
            };
        }();

        var i = 0;
        function getDetailByPeriod(me){

        	console.log(me); return;

        	var cportlet = (me.seriesName+"-"+me.name).replace(/\ /g, "_");
        	if($(".m-content > #portleDetail").find("."+cportlet).length>0){
        		return false;
        	}

            var field="";
            var value=me.name;
            if(me.seriesName=="Keyword"){
                field="content";
            } else if(me.seriesName=="Digital Media"){
                field="bundle";
            } else if(me.seriesName=="Sentiment"){
                field="is_sentiment";
            }

            var formData = $("#formFilter").serializeArray(); //console.log(formData);
            formData.push({ name: "ajax", value: 1 });
            formData.push({ name: "entity_id", value: 2 });
            formData.push({ name: "field", value: field });
            formData.push({ name: "value", value: value });

            $.ajax({
                url: "/bigbox/bigsearch/mediaanalytic",
                content: "json",
                method: "post",
                data: formData,
                beforeSend: function(){
                    mApp.blockPage({
                        overlayColor: "#000000",
                        type: "loader",
                        state: "success",
                        message: "Please wait..."
                    });
                },
                error: function(){
                },
                complete: function(res){
                    mApp.unblockPage();
                },
                success: function(res){
                    // console.log(res.data);

                	$(".m-content > #portleDetail").append(
                		"<div class=\"col-md-6\">"+
	                		"<div class=\"m-portlet m-portlet--head-sm "+cportlet+" \">"+
								"<div class=\"m-portlet__head\">"+
									"<div class=\"m-portlet__head-caption\">"+
										"<div class=\"m-portlet__head-title\">"+
											"<h3 class=\"m-portlet__head-text\">"+
												value+
												"<span class=\"m-portlet__head-desc\">"+
													me.seriesName+
												"</span>"+
											"</h3>"+
										"</div>"+
									"</div>"+
									"<div class=\"m-portlet__head-tools\">"+
										"<ul class=\"m-portlet__nav\">"+
											"<li class=\"m-portlet__nav-item\">"+
												"<a class=\"m-portlet__nav-link m-portlet__nav-link--icon\" onclick=\"removePortlet(this);\">"+
													"<i class=\"la la-close\"></i>"+
												"</a>"+
											"</li>"+
										"</ul>"+
									"</div>"+
								"</div>"+
								"<div class=\"m-portlet__body\">"+
                                    "<div id=\"container_"+i+"\" style=\"height:200px;\"></div>"+
	                			"</div>"+
                			"</div>"+
                		"</div>"
                	);

                	$("."+cportlet).mPortlet().on("beforeRemove", function() {
                		$("."+cportlet).parent().remove();
			        });

		            Highcharts.chart("container_"+i, {
			            chart: {
			                zoomType: "x"
			            },
			            title: {
			                text: ""
			            },
			            subtitle: {
			                text: ""
			            },
			            xAxis: {
			                type: "datetime"
			            },
			            yAxis: {
			                title: {
			                    text: ""
			                },
			                min: 0,
    						allowDecimals: false,
			            },
			            legend: {
			                enabled: false
			            },
			            plotOptions: {
			                column: {
			                    fillColor: {
			                        linearGradient: {
			                            x1: 0,
			                            y1: 0,
			                            x2: 0,
			                            y2: 1
			                        },
			                        stops: [
			                            [0, Highcharts.getOptions().colors[0]],
			                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get("rgba")]
			                        ]
			                    },
			                    marker: {
			                        radius: 2
			                    },
			                    lineWidth: 1,
			                    states: {
			                        hover: {
			                            lineWidth: 1
			                        }
			                    },
			                    threshold: null
			                }
			            },

			            series: [{
			                type: "column",
			                name: "Docs",
			                data: res.data
			            }]
			        });
		            i++;
                }

            });
        }

        function removePortlet(me){
        	$(me).parent().parent().parent().parent().parent().parent().remove();
        }

        jQuery(document).ready(function(){
            Page.init();
        });');
}

}