<?php
/*
 * Copyright (C) bigbox, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by bacanghaneut <rohimfikri@gmail.com>,
*/
class Bigbox_ErrorController extends Zend_Controller_Action{

public function init() {
	
}

public function error404Action (){
   $errors = $this->_getParam('error_handler');
   // Zend_Debug::dump($this); die();
   switch ($errors) {
      case '404':
         // 404 error -- controller or action not found
         $this->getResponse()->setHttpResponseCode(404);
         // $this->view->message = 'Page not found';
         $this->view->message = "Looks like something went wrong. We're working on it";
      break;
      default:
         // application error 
         $this->getResponse()->setHttpResponseCode(500);
         // $this->view->message = 'Application error';
         $this->view->message = "Looks like something went wrong. We're working on it";
      break;
   }
   $this->view->request = $errors;
}

public function errorAction (){
   //$errors = $this->_getParam('error_handler');
   //Zend_Debug::dump($errors); die();
   $this->getResponse()->setHttpResponseCode(500);
   // Zend_Debug::dump($this); die();
   //$this->view->request = $errors;
}
    
public function error403Action (){
   $errors = $this->_getParam('error_handler');
   // Zend_Debug::dump($errors); die();
   // Zend_Debug::dump($this); die();
   // Zend_Debug::dump($this); die();

   switch ($errors) {
      case '403':
         // 404 error -- controller or action not found
         $this->getResponse()->setHttpResponseCode(403);
         $this->view->message = 'You Dont Have Authorize to Access This Page';
      break;
      default:
         // application error 
        $this->getResponse()->setHttpResponseCode(500);
         // $this->view->message = 'Application error';
         $this->view->message = "Looks like something went wrong. We're working on it";
      break;
  }
   $this->view->request = $errors;
}

}