<?php
/*
 * Copyright (C) bigbox, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by bacanghaneut <rohimfikri@gmail.com>,
*/
class Bigbox_GeneralController extends Zend_Controller_Action{

public function init() {
	
}

public function headerAction() {
	try {
		$authAdapter = Zend_Auth::getInstance();
		$identity = $authAdapter->getIdentity();
		// Zend_Debug::dump($identity);die();
	} catch (Exception $e) {
		
	}
	$params = $this->getRequest()->getParams();
	// Zend_Debug::dump($params);die();

	$roles = array(
		46=>'bigquery',
		47=>'bigaction',
		48=>'bigbuilder',
		49=>'bigenvelope',
		50=>'bigflow',
		51=>'bigspider',
		52=>'bigsearch',
		53=>'biglake',
	);

	$mdlSystem = new Model_System();

	$maincontroller = "bigquery";
	if($identity->mainrole!=1){
		$roles = $mdlSystem->get_roles();
		// Zend_Debug::dump($roles);die();

		$gids = array_column($roles, 'gid'); //Zend_Debug::dump($gids);die();
		$group_name = strtolower($roles[array_search($identity->mainrole, $gids)]["group_name"]);
		// Zend_Debug::dump(substr($group_name, 0,3));die();

		$maincontroller = "";
		if(substr($group_name, 0,3)=="big"){
			$group_name = explode(" ", $group_name);
			$maincontroller = $group_name[0];
		}
		// Zend_Debug::dump($maincontroller);die();

	}

	$apps = $mdlSystem->get_apps_bigbox();
	// Zend_Debug::dump($apps);die();

	$arrlogo = array(
		'bigaction'=>'BigAction_dark.png',
		'bigbuilder'=>'BigBuilder_dark.png',
		'bigenvelope'=>'BigEnvelope_dark.png',
		'bigflow'=>'BigFlow_dark.png',
		'biglake'=>'BigLake_dark.png',
		'bignatural'=>'BigNatural_dark.png',
		'bigquery'=>'BigQuery_dark.png',
		'bigsearch'=>'BigSearch_dark.png',
		'bigspider'=>'BigSpider_dark.png',
	);

	$logo = 'BigBox_logo.png';
	if(array_key_exists($params['controller'], $arrlogo)){
		$logo = $arrlogo[$params['controller']];
	// } else if(array_key_exists($identity->mainrole, $roles)){
	// 	$logo = $arrlogo[$roles[$identity->mainrole]];
	}

	$this->view->apps = $apps;
	$this->view->logo = $logo;
	$this->view->maincontroller = $maincontroller;

	$this->view->params = $params;
	$this->view->identity = (array)$identity;
	// Zend_Debug::dump($identity);die();
	$this->renderScript ( 'theme/'.$params['theme'].'/'.$params['layout'].'/header.phtml' );
}

public function sidebarAction() {
	try {
		$authAdapter = Zend_Auth::getInstance();
		$identity = $authAdapter->getIdentity();
	} catch (Exception $e) {
		
	}
	$params = $this->getRequest()->getParams();
	// Zend_Debug::dump($params);//die('x');
	$this->view->params = $params;
	$this->view->identity = (array)$identity;
	// Zend_Debug::dump($identity);die();
	$this->renderScript ( 'theme/'.$params['theme'].'/'.$params['layout'].'/sidebar.phtml' );
}

public function navAction() {
	try {
		$authAdapter = Zend_Auth::getInstance();
		$identity = $authAdapter->getIdentity();
	} catch (Exception $e) {
		
	}
	$params = $this->getRequest()->getParams();
	// Zend_Debug::dump($params);die();
	$this->view->params = $params;
	$this->view->identity = $identity;
	// Zend_Debug::dump($identity);die();
	$this->renderScript ( 'theme/'.$params['theme'].'/'.$params['layout'].'/nav.phtml' );
}

public function sidemenuAction() {
	try {
		$authAdapter = Zend_Auth::getInstance();
		$identity = $authAdapter->getIdentity();
		// Zend_Debug::dump($identity);die();
	} catch (Exception $e) {
		
	}
	$params = $this->getRequest()->getParams();
	// Zend_Debug::dump($params);die();();

	$mdlSystem = new Model_System();

	$tmp = $mdlSystem->get_menus_bigbox();
	// Zend_Debug::dump($tmp);die();

	$menu = array();
	foreach ($tmp as $key => $value) {

		$gname = strtolower(substr($value['group_name'], 0,-5));
		// Zend_Debug::dump($gname);die();

		$menus[$value['group_id']][$gname]['url'] = '/bigbox/'.$gname;
		$menus[$value['group_id']][$gname]['sub-menu'][] = array(
			'menu_id'=>$value['menu_id'],
			'menu_name'=>$value['menu_name'],
			'menu_link'=>$value['menu_link'],
			'class'=>$value['class'],
		);
	}
	// Zend_Debug::dump($menus);die();

	$roles = array(
		46=>'bigquery',
		47=>'bigaction',
		48=>'bigbuilder',
		49=>'bigenvelope',
		50=>'bigflow',
		51=>'bigspider',
		52=>'bigsearch',
		53=>'biglake',
	);

	$gid = array_search($params['controller'], $roles);

	// Zend_Debug::dump(array_search($params['controller'], $roles));die();

	$menu = $menus;
	if(is_numeric($gid)){
		$menu = $menus[$gid];
	} else if(array_key_exists($identity->mainrole, $menus)){
		$gid = $identity->mainrole;
		$menu = $menus[$gid];
	}

	$this->view->menu = $menu;

	$this->view->params = $params;
	$this->view->identity = (array)$identity;
	// Zend_Debug::dump($identity);die();
	$this->renderScript ( 'theme/'.$params['theme'].'/'.$params['layout'].'/sidemenu.phtml' );
}

public function footerAction() {
	try {
		$authAdapter = Zend_Auth::getInstance();
		$identity = $authAdapter->getIdentity();
	} catch (Exception $e) {
		
	}
	$params = $this->getRequest()->getParams();
	// Zend_Debug::dump($params);die();
	$this->view->params = $params;
	$this->view->identity = $identity;
	// Zend_Debug::dump($identity);die();
	$this->renderScript ( 'theme/'.$params['theme'].'/'.$params['layout'].'/footer.phtml' );
}

}