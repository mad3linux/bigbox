<?php
/*
 * Copyright (C) bigbox, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by bacanghaneut <rohimfikri@gmail.com>,
*/
class Bigbox_GrafanayarnController extends Zend_Controller_Action{

    public function homeAction(){
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();

        $app = substr($params['controller'], 7);
        // Zend_Debug::dump($app);die();

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->view->params = $params;
        $this->view->identity = $identity;
        // Zend_Debug::dump($identity);die();
        $this->view->headScript()->appendScript("
        jQuery(document).ready(function(){
            var ctn = $('.m-grid__item.m-grid__item--fluid.m-wrapper');
            var h = ctn.innerHeight();
            $('iframe').css('height',h-10);
        });
        ");
        $this->renderScript ( 'app/'.$app.'/'.$params['action'].'.phtml' );
    }

    public function applicationsAction(){
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();

        $app = substr($params['controller'], 7);
        // Zend_Debug::dump($app);die();

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->view->params = $params;
        $this->view->identity = $identity;
        // Zend_Debug::dump($identity);die();
        $this->view->headScript()->appendScript("
        jQuery(document).ready(function(){
            var ctn = $('.m-grid__item.m-grid__item--fluid.m-wrapper');
            var h = ctn.innerHeight();
            $('iframe').css('height',h-10);
        });
        ");
        $this->renderScript ( 'app/'.$app.'/'.$params['action'].'.phtml' );
    }

    public function mrjobhistoryserverAction(){
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();

        $app = substr($params['controller'], 7);
        // Zend_Debug::dump($app);die();

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->view->params = $params;
        $this->view->identity = $identity;
        // Zend_Debug::dump($identity);die();
        $this->view->headScript()->appendScript("
        jQuery(document).ready(function(){
            var ctn = $('.m-grid__item.m-grid__item--fluid.m-wrapper');
            var h = ctn.innerHeight();
            $('iframe').css('height',h-10);
        });
        ");
        $this->renderScript ( 'app/'.$app.'/'.$params['action'].'.phtml' );
    }

    public function nodemanagersAction(){
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();

        $app = substr($params['controller'], 7);
        // Zend_Debug::dump($app);die();

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->view->params = $params;
        $this->view->identity = $identity;
        // Zend_Debug::dump($identity);die();
        $this->view->headScript()->appendScript("
        jQuery(document).ready(function(){
            var ctn = $('.m-grid__item.m-grid__item--fluid.m-wrapper');
            var h = ctn.innerHeight();
            $('iframe').css('height',h-10);
        });
        ");
        $this->renderScript ( 'app/'.$app.'/'.$params['action'].'.phtml' );
    }

    public function queuesAction(){
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();

        $app = substr($params['controller'], 7);
        // Zend_Debug::dump($app);die();

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->view->params = $params;
        $this->view->identity = $identity;
        // Zend_Debug::dump($identity);die();
        $this->view->headScript()->appendScript("
        jQuery(document).ready(function(){
            var ctn = $('.m-grid__item.m-grid__item--fluid.m-wrapper');
            var h = ctn.innerHeight();
            $('iframe').css('height',h-10);
        });
        ");
        $this->renderScript ( 'app/'.$app.'/'.$params['action'].'.phtml' );
    }

    public function resourcemanagerAction(){
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();

        $app = substr($params['controller'], 7);
        // Zend_Debug::dump($app);die();

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->view->params = $params;
        $this->view->identity = $identity;
        // Zend_Debug::dump($identity);die();
        $this->view->headScript()->appendScript("
        jQuery(document).ready(function(){
            var ctn = $('.m-grid__item.m-grid__item--fluid.m-wrapper');
            var h = ctn.innerHeight();
            $('iframe').css('height',h-10);
        });
        ");
        $this->renderScript ( 'app/'.$app.'/'.$params['action'].'.phtml' );
    }    

    public function timelineserverAction(){
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();

        $app = substr($params['controller'], 7);
        // Zend_Debug::dump($app);die();

        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        } catch (Exception $e) {
            
        }
        $this->view->params = $params;
        $this->view->identity = $identity;
        // Zend_Debug::dump($identity);die();
        $this->view->headScript()->appendScript("
        jQuery(document).ready(function(){
            var ctn = $('.m-grid__item.m-grid__item--fluid.m-wrapper');
            var h = ctn.innerHeight();
            $('iframe').css('height',h-10);
        });
        ");
        $this->renderScript ( 'app/'.$app.'/'.$params['action'].'.phtml' );
    }    
}
