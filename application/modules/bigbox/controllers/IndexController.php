<?php
/*
 * Copyright (C) bigbox, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by bacanghaneut <rohimfikri@gmail.com>,
*/
class Bigbox_IndexController extends Zend_Controller_Action{

public function init() {
	
}

public function indexAction() {
	$this->view->headLink()->appendStylesheet("/assets/m51/dist/default/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css");
	$this->view->headScript()->appendFile("/assets/m51/dist/default/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js");
	$this->view->headScript()->appendFile("/assets/m51/dist/default/assets/app/js/dashboard.js");
}

}