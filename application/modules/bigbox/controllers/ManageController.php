<?php

/**
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>,Jul 15, 2014
 *
 */

class Domas_ManageController extends Zend_Controller_Action {

    public function init() {
    }

    public function listxpathAction() {
        $mm = new Model_Zprabapage();
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $sys = new Domas_Model_Manage();
        $this->view->data = $sys->list_xpath();
    }

    public function updatexpathAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($params); die();
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            $cc = new Domas_Model_Manage();
            parse_str($_POST['varx'], $data);
           // Zend_Debug::dump($data);
            //die();
            $cc->add_update_xpath($data);
            $result = array('retCode' => '00',
                            'retMsg' => 'add xpath succed',
                            'result' => true,
                           );
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function xpathAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/css/pendem.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/dropzone/dropzone.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/dropzone/dropzone.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/dropzone/basic.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.full.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-toastr/toastr.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2-bootstrap.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/infinitescroll/jquery.jscroll.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jstree/dist/themes/default/style.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jstree/dist/jstree_c.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-toastr/toastr.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js');
        $params = $this->getRequest()->getParams();
        $cc = new Domas_Model_Manage();
        if(!empty($params['id'])) {
            $this->view->data = $cc->get_a_xpath($params['id']);
        }
        // die("xx");
    }

    public function ajaxsearchallAction() {
        $params = $this->getRequest()->getParams();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if(isset($params['keywords'])&& $params['keywords'] != "") {
            $cc = new Pendem_Model_Withsolr();
            //  Zend_Debug::dump($params); die();
            $html = "";
            $g = new Pendem_Model_General();
            $ar = $g->get_icons();
            $params = $this->getRequest()->getParams();
            if($params['prev'] != $params['keywords']) {
                $params['page'] = 0;
                //$params['keywords']=;
            }
            $page = !empty($params['page'])? (int) $params['page'] : 1;
            //echo $page; die();
            $limit = 10;
            //per page    
            $start =((int) $page - 1)* $limit;
            // echo $start; die();
            $data = $cc->searchingall($params['keywords'], $start, $limit);
            //Zend_Debug::dump($data); die();
            $showpager = 10;
            $total = $data->response->numFound;
            //total items in array    
            //  $total=500;
            #echo $total; die();   
            $totalPages = ceil($total / $limit);
            //calculate total pages
            //echo $totalPages ; die();   
            $page = max($page, 1);
            //get 1 page when $_GET['page'] <= 0
            $page = min($page, $totalPages);
            //get last page when $_GET['page'] > $totalPages
            $offset =($page - 1)* $limit;
            if($offset < 0)
                $offset = 0;
            $html .= '<ul class="search-container">';

            foreach($data->response->docs as $k => $v) {
                $html .= '<li class="search-item clearfix">
								<div class="search-content text-left">
									
									<h2 class="search-title">	
										<i class="fa  ' . $ar[$v['bundle']] . '"></i>									
										<a data-toggle="modal" href="#basic"><i>  ' . $v['bundle'] . '<br>' . $v['bundle_name'] . '	</i></a>
									</h2>';
                if(isset($v['label'])&& $v['label'] != "") {
                    $html .= '<h2><a href = "#">' . $v['label'] . '</a></h2>';
                }
                $p = "";
                $html .= '<p class="search-desc">';

                foreach($data->highlighting->$v['id']->content as $z) {
                    $p .= "" . $z . "<br>";
                }
                $html .= '<p>' . $p . '';
                // $pos =false;    
                $keys = array();
                //$keys =array_keys($v);
                $keys =(array_keys(get_object_vars($v)));

                foreach($keys as $k3) {
                    $pos = strpos($k3, "ss_");
                    if($pos !== false) {
                        //die("xx");
                        $html .= '<em><i class = "fa  fa-check-square"></i>' . substr($k3, 3). ':&nbsp;&nbsp;&nbsp; ' . $v[$k3] . '</em>&nbsp;&nbsp;&nbsp;&nbsp; ';
                    }
                }
                '</p>
								</div>
							</li>
							
							
						';
            }
            $html .= '</ul>';
            $prev = (int) $page - 1;
            $next = (int) $page + 1;
            $result = array('retCode' => '10',
                            'retMsg' => 'Invalid Parameter',
                            'result' => true,
                            'data' =>$html);
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function searchAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/css/pendem.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.full.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-toastr/toastr.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2-bootstrap.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/infinitescroll/jquery.jscroll.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jstree/dist/themes/default/style.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/pages/css/search.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jstree/dist/jstree_c.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-toastr/toastr.min.js');
        $params = $this->getRequest()->getParams();
    }

    public function allfilesAction() {
        $authAdapter = Zend_Auth::getInstance();
        $identity = $authAdapter->getIdentity();
        $cc = new Pendem_Model_Withsolrgeneral();
        $dd = $cc->searching("", 0, 100, array('entity_id:3'));
        $this->view->data = $dd;
    }

    public function myfilesAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/css/pendem.css');
        $authAdapter = Zend_Auth::getInstance();
        $identity = $authAdapter->getIdentity();
        $cc = new Pendem_Model_Withsolrgeneral();
        $dd = $cc->searching("", 0, 100, array('is_uid:' . $identity->uid . '', 'entity_id:3'));
        $this->view->data = $dd;
    }

    public function addAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/css/pendem.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/dropzone/dropzone.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/dropzone/dropzone.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/dropzone/basic.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.full.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-toastr/toastr.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2-bootstrap.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/infinitescroll/jquery.jscroll.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jstree/dist/themes/default/style.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jstree/dist/jstree_c.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-toastr/toastr.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js');
        $params = $this->getRequest()->getParams();
    }

    public function xuploaderAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        //        
        //  die();
        //      Zend_Debug::dump($_FILES); die(); 
        $cc = new Domas_Model_Zfiles();
        if($_FILES) {
            $dest_dir = constant("APPLICATION_PATH"). "/../public/repositories/" . $identity->uname . "/";
            // die($dest_dir );
            if(!is_dir($dest_dir)) {
                mkdir($dest_dir, 0777);
            }
            $upload = new Zend_File_Transfer_Adapter_Http();
            $upload->setDestination($dest_dir);
            $files = $upload->getFileInfo();
            //  Zend_Debug::dump($files);
            // die();
            try {
                $upload->receive();
                //  $url = $dest_dir . $files['file']['name'];
                $id = $cc->insert_file($files);
                die($id);
            }
            catch(Zend_File_Transfer_Exception $e) {
                die($e->getMessage());
            }
        }
        //die();
    }
}
