<?php


class Bigbox_Model_Pdictionary extends Zend_Db_Table_Abstract {

   function count_listdictionary($data) {
		$aColumns = array(
			'no',
			'kata',
			'bahasa',
			'topik',
			'sentiment'
		);
		
		$sLimit = "";
		
		$getdatatable	= $data;
		$page			= round($getdatatable['pagination']['page']);
		$perpages		= round($getdatatable['pagination']['perpage']);
		$pages			= ceil($iTotalRecords/$perpages);
		$sorttype		= $getdatatable['sort']['sort'];
		$sortby			= $getdatatable['sort']['field'];
		
		$iDisplayLength = intval($perpages);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart 	= intval(($page-1)*$perpages);
		$sSearch		= $getdatatable['query'];
		
		
		if(isset($iDisplayStart)&& $iDisplayLength != '-1') {
			$sLimit = "LIMIT " . intval($iDisplayStart). ", " . intval($iDisplayLength);
		}
		
		// ORDERING
		if(isset($sortby)) {
			if ($sortby=='No') {
				$filsort	= 'id';
			} else {
				$filsort	= $sortby;
			}
			$sOrder = "ORDER BY ".$filsort." ".$sorttype;
		} else {
			$sOrder = "";
		}
			
		/*
		 * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
		 */
		// $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
		$whapp = "";
		$sWhere = "";
		// if($app != "") {
				// $whapp = "AND app = '$app'";
		// }
		
		if(isset($sSearch['generalSearch'])&& $sSearch['generalSearch'] != "") {
			$sWhere .= "WHERE (";
			for($i = 1;
			$i < count ($aColumns );
			$i ++ ) {
				$sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $sSearch['generalSearch'] . "%' OR ";
			}
			// die($sWhere);
			$sWhere = substr_replace($sWhere, "", - 3);
			$sWhere .= ')';
		} else if(isset($sSearch['bahasa'])&& $sSearch['bahasa'] != "") {
			$sWhere .= "WHERE (";
			// for($i = 1;
			// $i < count ($aColumns );
			// $i ++ ) {
				$sWhere .= "`bahasa` LIKE '%" . $sSearch['bahasa'] . "%' OR ";
			// }
			// die($sWhere);
			$sWhere = substr_replace($sWhere, "", - 3);
			$sWhere .= ')';
		}
		// echo $sWhere.'<br>';
		// echo $sOrder.'<br>';
		// echo $sLimit.'<br>';
		// die();
		$qry = "SELECT count(id) as rowsd 
		FROM zpraba_dictionary 
			 " . $sWhere . " " . $sOrder;
		 // die($qry);
		try {
			$qry = str_ireplace("`", "", $qry);
			// Zend_Debug::dump($qry);die();
				$data = $this->_db->fetchOne($qry);
				// Zend_Debug::dump($sWhere);die();
				return $data;
		}
		catch(Exception $e) {
				Zend_Debug::dump($e->getMessage());
				die($q);
		}
	}
	
	function get_listdictionary($data) {
			$aColumns = array(
				'no',
				'kata',
				'bahasa',
				'topik',
				'sentiment'
			);
			
			$sLimit = "";
		
			$getdatatable	= $data;
			$page			= round($getdatatable['pagination']['page']);
			$perpages		= round($getdatatable['pagination']['perpage']);
			$pages			= ceil($iTotalRecords/$perpages);
			$sorttype		= $getdatatable['sort']['sort'];
			$sortby			= $getdatatable['sort']['field'];
			
			$iDisplayLength = intval($perpages);
			$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
			$iDisplayStart 	= intval(($page-1)*$perpages);
			$sSearch		= $getdatatable['query'];
			
			
			if(isset($iDisplayStart)&& $iDisplayLength != '-1') {
				$sLimit = "LIMIT " . intval($iDisplayStart). ", " . intval($iDisplayLength);
			}
			
			// ORDERING
			if(isset($sortby)) {
				if ($sortby=='No') {
					$filsort	= 'id';
				} else {
					$filsort	= $sortby;
				}
				$sOrder = "ORDER BY ".$filsort." ".$sorttype;
			} else {
				$sOrder = "";
			}
				
			/*
			 * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
			 */
			// $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
			$whapp = "";
			$sWhere = "";
			// if($app != "") {
					// $whapp = "AND app = '$app'";
			// }
			
			if(isset($sSearch['generalSearch'])&& $sSearch['generalSearch'] != "") {
				$sWhere .= "WHERE (";
				for($i = 1;
				$i < count ($aColumns );
				$i ++ ) {
					$sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . $sSearch['generalSearch'] . "%' OR ";
				}
				// die($sWhere);
				$sWhere = substr_replace($sWhere, "", - 3);
				$sWhere .= ')';
			} else if(isset($sSearch['bahasa'])&& $sSearch['bahasa'] != "") {
				$sWhere .= "WHERE (";
				// for($i = 1;
				// $i < count ($aColumns );
				// $i ++ ) {
					$sWhere .= "`bahasa` LIKE '%" . $sSearch['bahasa'] . "%' OR ";
				// }
				// die($sWhere);
				$sWhere = substr_replace($sWhere, "", - 3);
				$sWhere .= ')';
			}
			// echo $sWhere.'<br>';
			// echo $sOrder.'<br>';
			// echo $sLimit.'<br>';
			// die();
			$qry = "SELECT *
			FROM zpraba_dictionary 
				 " . $sWhere . " " . $sOrder . " " . $sLimit;
			 // die($qry);
			try {
				$qry = str_ireplace("`", "", $qry);
					$data = $this->_db->fetchAll($qry);
					// Zend_Debug::dump($qry);die();
					return $data;
			}
			catch(Exception $e) {
					Zend_Debug::dump($e->getMessage());
					die($q);
			}
	}

	
 
    public function get_dictionary($bahasa="") {
		$sWhere ="";
		if($bahasa!=""){
			$sWhere ="and bahasa = '".$bahasa."'";
		}
		
        try {
            $sql = "select * from zpraba_dictionary where 1=1 ".$sWhere." order by id desc limit 10";

            $data = $this->_db->fetchAll($sql);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
        return $data;
    }
		
		
	
	public function get_edit_dict($id_data="") {
		$sWhere ="";
		if($id_data!=""){
			$sWhere ="and id = ".$id_data;
		}
		
        try {
            $sql = "select * from zpraba_dictionary where 1=1 ".$sWhere."";
            $data = $this->_db->fetchAll($sql);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
        return $data;
    }
	
	public function get_topik() {
		
        try {
            $sql = "select tid,name from zpraba_taxonomy_term_data where level=2";
            $data = $this->_db->fetchAll($sql);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
        return $data;
    }
	
	
	public function add_dictionary($data) {
        try {
			//Zend_Debug::dump($data); die();
            if($data['id'] == "") {
             $this->_db->query("insert into zpraba_dictionary (kata, bahasa,topik, sentiment) values (?, ?, ?, ?)", array($data['kata'],$data['bahasa'], $data['topik'], $data['sentiment']));  
                return $this->_db->lastInsertId();
            } else {
                $this->_db->query("update zpraba_dictionary set kata=?, bahasa=?, topik=?, sentiment=?  where id=?", array($data['kata'],$data['bahasa'], $data['topik'],$data['sentiment'],$data['id']));
                // return $this->_db->lastInsertId();
                return $data['id'];
            }
        }   
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
	}
		
	public function add_predictive($data) {
        try {
			// Zend_Debug::dump($data);
			// Zend_Debug::dump("insert into zpraba_predictive (id, text, tema, topik, prediksi, nilai) values (?, ?, ?, ?, ?, ?)", array(null,$data['text'], $data['tema'], $data['topik'], $data['prediksi'], $data['nilai'])); die('as');
            if($data['id'] == "") {
             $this->_db->query("insert into zpraba_predictive (id, id_content,text, tema, topik, prediksi, nilai) values (?, ?, ?, ?, ?, ?, ?)", array(null, $data['id_content'],$data['text'], $data['tema'], $data['topik'], $data['prediksi'], $data['nilai']));  
               //return $this->_db->lastInsertId();
            } else {
                $this->_db->query("update zpraba_predictive set text=?, tema=?, topik=?, nilai=?  where id_content=?", array($data['id_content'],$data['text'], $data['tema'],$data['topik'],$data['nilai']));
                // return $this->_db->lastInsertId();
               // return $data['id'];
            }
        }   
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
		
 }
	
}
