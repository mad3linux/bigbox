<?php

class Bigbox_Model_Pmedia extends Zend_Db_Table_Abstract {

    public function get_groupsname($id) {
        try {
            $sql = "select groupname from zprahu_groups_target  where id=" . $id . "";
            $data = $this->_db->fetchAll($sql);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
        return $data;
    }

    public function get_logs_cnt() {
        try {
            $sql = "select count(*) as cnt, stype  from zpraba_alert a  group by stype";
            $data = $this->_db->fetchAll($sql);

            foreach($data as $z) {
                $new[$z['stype']] = $z['cnt'];
            }
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
        return $new;
    }

    public function get_logs_trend($type) {
        try {
            $stype = '';
            if(isset($type)&& $type != "") {
                $stype = "AND stype LIKE '" . $type . "'";
            }
            $sql = "select count(*) as cnt, DATE_FORMAT(a.updated_date,'%e %M %Y') as vdate, stype  from zpraba_alert a WHERE 1=1 " . $stype . "  group by DATE_FORMAT(a.updated_date,'%m-%d-%Y'), stype ORDER BY updated_date";
            // Zend_Debug::dump($sql); die();
            $data = $this->_db->fetchAll($sql);
            // Zend_Debug::dump($data); die();
            date_default_timezone_set('Europe/London');

            foreach($data as $v) {
                $new[$v['stype']][strtotime($v['vdate'], "+1 days")* 1000] = $v['cnt'];
                //echo $v['vdate'];
                //	echo strtotime($v['vdate']); die();
                $timeline[] = strtotime($v['vdate'], "+1 days")* 1000;
                // $timeline[] =strtotime($v['vdate'])*1000 ;  
            }
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
        // arsort($new);
        // Zend_Debug::dump($timeline); die();
        return array('data' =>$new,
                     'time' =>$timeline);
    }

    public function get_logs($type) {
        try {
            $stype = '';
            if(isset($type)&& $type != "") {
                $stype = "AND stype LIKE '" . $type . "'";
            }
            $sql = "select * from zpraba_alert WHERE 1=1 " . $stype . " order by updated_date desc";
            $data = $this->_db->fetchAll($sql);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
        // Zend_Debug::dump($data); die();
        return $data;
    }

    public function log($data) {
        # Zend_Debug::dump($data);die();
        try {
            //Zend_Debug::dump($data); die();
            $this->_db->query("insert into zpraba_alert (subject, message, updated_date, target, sender_uid, stype) values(?, ?, now(), ?,?, ?)", array($data['subject'], $data['message'], $data['target'], $data['sender_uid'], $data['stype']));
            // return $this->_db->lastInsertId();
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    public function get_groups() {
        try {
            $sql = "select a.*, b.account_name from zprahu_groups_target  a inner join zprahu_service_attrs b  on a.service_id=b.id_service order by 1 ";
            $data = $this->_db->fetchAll($sql);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
        return $data;
    }

    public function addgroup($data) {
        # Zend_Debug::dump($data);die();
        try {
            //Zend_Debug::dump($data); die();
            if($data['id_group'] == "") {
                $this->_db->query("insert into zprahu_groups_target (groupname, messaging_type, group_code,service_id, updated_date) values (?,?,?,?, now())", array($data['gr_name'], 1, $data['gr_id'], $data['opt_nomor']));
                return $this->_db->lastInsertId();
            } else {
                $this->_db->query("update zprahu_groups_target set groupname=?, group_code=?,service_id=?, updated_date=now()  where id=?", array($data['gr_name'], $data['gr_id'], $data['opt_nomor'], $data['id_group']));
                // return $this->_db->lastInsertId();
                return $data['id'];
            }
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    public function updated_main_media($id, $url) {
        // echo "update pmas_media set logo='$url' where id=$id";die();
        try {
            $this->_db->query("update pmas_media set logo='$url' where id=$id");
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    public function addmedia($data) {
        //Zend_Debug::dump($data);die();
        try {
            //Zend_Debug::dump($data); die();
            if($data['id'] == "") {
                $this->_db->query("insert into pmas_media (media_name, slugname, url,media_type,language,update_routine, updated_date) values (?, ?, ?, ?,?,?,now())", array($data['medianame'], $data['machinename'], $data['url'], $data['media_type'], $data['language'], $data['update_routine']));
                return $this->_db->lastInsertId();
            } else {
                $this->_db->query("update pmas_media set media_name=?, media_type=?,slugname=?, language=?,url=?, update_routine=?, updated_date=now()  where id=?", array($data['medianame'], $data['media_type'], $data['machinename'], $data['language'], $data['url'], $data['update_routine'], $data['id']));
                // return $this->_db->lastInsertId();
                return $data['id'];
            }
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    public function addtelgr($data) {
        try {
            //Zend_Debug::dump($data);
            if($data['id_service'] == "") {
                $this->_db->query("insert into zprahu_services (service_number,service_descr,service_operator,updated_date ) values(?,?,?,now())", array($data['no_service'], $data['desk_service'], $data['opt_service']));
            } else {
                $this->_db->query("update zprahu_services set service_number=?,service_descr=?,service_operator=?,updated_date=now() where id=?", array($data['no_service'], $data['desk_service'], $data['opt_service'], $data['id_service']));
            }
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
    }

    public function addtelg($data) {
        //Zend_Debug::dump($data);die();    
        try {
            if($data['id_servis'] == "") {
                // Zend_Debug::dump($data);die("s");   
                $this->_db->query("insert into zprahu_service_attrs (account_name,attr1,s_type,updated_date) values(?,?,?,now())", array($data['acc_name'], $data['acc_bot'], $data['opt_nomor']));
            } else {
                $this->_db->query("update zprahu_service_attrs set account_name=?,attr1=?,s_type=? where id_service=?", array($data['acc_name'], $data['acc_bot'], $data['opt_nomor'], $data['id_servis']));
            }
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    public function addapi($data) {
        try {
            if($data['id'] == "") {
                $this->_db->query("insert into pmas_apikey (consumer_key,consumer_secret,access_token,
       access_token_secret,update_date) values (?,?,?,?,now())", array($data['con_key'], $data['con_secret'], $data['acc_token'], $data['acc_token2']));
                return $data;
            } else {
                $this->_db->query("update pmas_apikey set consumer_key=?,consumer_secret=?,access_token=?,
        access_token_secret=?,update_date=now() where id=?", array($data['con_key'], $data['con_secret'], $data['acc_token'], $data['acc_token2'], $data['id']));
                return $data;
            }
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    public function addalert($data) {
        $param = $data['param_name'];
        $id_param = array($data['name_id'],
                         $data['email_id'],
                         $data['smtp_id'],
                         $data['smtpp_id'],
                         $data['ssl_id'],
                         $data['time_id']);
        $value_param = array($data['send_name'],
                            $data['send_email'],
                            $data['smtp_dat'],
                            $data['smtpp_dat'],
                            $data['ssl_dat'],
                            $data['time_dat']);
        $i = 0;
        //Zend_Debug::dump($id_param[$i]);die();
        try {

            foreach($value_param as $k => $v) {
                $this->_db->query("update zpraba_params set value_param='$v' where param='$param' and id='$id_param[$i]'");
                $i ++;
            }
            //die('ww');
            return array('transaksi' => true);
        }
        catch(Exception $e) {
            return array('transaksi' => false);
            Zend_Debug::dump($e);
            die();
        }
    }

    public function deleteapi($data) {
        //Zend_Debug::dump($data);die();
        try {
            if($data['id'] != "") {
                $this->_db->query("delete from pmas_apikey where id=?", array($data['id']));
            }
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    public function update_sub_media($data) {
        // Zend_Debug::dump($data); die();
        try {
            $params = $data;
            unset($params['controller']);
            unset($params['action']);
            unset($params['module']);
            unset($params['c_name']);
            unset($params['media_name']);
            unset($params['c_type']);
            unset($params['c_status']);
            unset($params['index']);
            unset($params['r_limit']);
            $attrs = base64_encode(serialize($params));
            if($data['id'] != "") {
                $this->_db->query("update pmas_media_sub_crawl set status=?, subdomain=?, media_id=?, url_crawl=?, type_crawl=?, limit_rows=?, attrs=? where id=?", array($data['c_status'], $data['c_name'], $data['media_name'], $data['index'], $data['c_type'], $data['r_limit'], $attrs, $data['id']));
            } else {
                $this->_db->query("insert into pmas_media_sub_crawl (status, subdomain, media_id, url_crawl, type_crawl, limit_rows, attrs) values(?, ?, ?, ?, ?, ?, ?)", array($data['c_status'], $data['c_name'], $data['media_name'], $data['index'], $data['c_type'], $data['r_limit'], $attrs));
                $data['id'] = $this->_db->lastInsertId();
            }
        }
        catch(Exception $e) {
            return array("result" => false,
                         "retMsg" =>$e->getMessage(),
                         "id" =>$data['id']);
        }
        return array("result" => true,
                     "retMsg" => "update succed",
                     "id" =>$data['id']);
    }

    public function get_all_main_media($media_type = "") {
        $sWhere = "";
        if($media_type != "") {
            $sWhere = "and media_type = " . $media_type;
        }
        try {
            $sql = "select * from pmas_media where 1=1 " . $sWhere . " order by media_name";
            $data = $this->_db->fetchAll($sql);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
        return $data;
    }

    public function get_all_media($media_type = "") {
        $sWhere = "";
        if($media_type != "") {
            $sWhere = "and media_type = " . $media_type;
        }
        try {
            $sql = "select a.*,b.id as bid, b.url_crawl, b.type_crawl, b.subdomain, b.id as subid, b.attrs	as battr from pmas_media a inner join pmas_media_sub_crawl b on a.id= b. media_id where 1=1 " . $sWhere . " order by slugname ";
            $data = $this->_db->fetchAll($sql);

            foreach($data as $k => $v) {
                $new[$k] = $v;
                $new[$k]['params'] = unserialize(base64_decode($v['battr']));
            }
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
        return $new;
    }

    public function get_api_twit() {
        try {
            $sql = "select * from pmas_apikey";
            $new = $this->_db->fetchAll($sql);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
        return $new;
    }

    public function get_alertsystem() {
        try {
            $sql = "select * from zpraba_params where param ='config'";
            $new = $this->_db->fetchAll($sql);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
        return $new;
    }

    public function get_telg_srv() {
        try {
            $sql = "select * from zprahu_services";
            $data = $this->_db->fetchAll($sql);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
        return $data;
    }

    public function get_telg_attr() {
        try {
            $sql = "select a.*,b.* from zprahu_services a inner join zprahu_service_attrs b on a.id= b.id_service order by b.id_service asc";
            $data = $this->_db->fetchAll($sql);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
        return $data;
    }

    public function get_a_apikey_by_id($id) {
        try {
            $sql = "select * from pmas_apikey where id=?";
            $data = $this->_db->fetchRow($sql, $id);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
        return $data;
    }

    public function get_a_apialert_by_id($id) {
        try {
            $sql = "select * from zpraba_params where param ='config' and id=?";
            $data = $this->_db->fetchRow($sql, $id);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
        return $data;
    }

    public function get_a_media_by_id($id) {
        try {
            $sql = "select * from pmas_media where id=?";
            $data = $this->_db->fetchRow($sql, $id);
            $data['params'] = unserialize(base64_decode($data['attrs']));
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
        return $data;
    }

    public function get_a_media_by_slug($slug) {
        try {
            $sql = "select * from pmas_media where slugname=?";
            $data = $this->_db->fetchRow($sql, $slug);
            $data['params'] = unserialize(base64_decode($data['attrs']));
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
        return $data;
    }

    public function get_active_crawling($id) {
        try {
            $sql = "select * from pmas_media_sub_crawl where status=1 ";
            $data = $this->_db->fetchRow($sql, $id);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
        return $data;
    }

    public function get_submedia_by_sub_id($id) {
        try {
            $sql = "select * from pmas_media_sub_crawl where  id=?";
            $data = $this->_db->fetchRow($sql, $id);
            $data['params'] = unserialize(base64_decode($data['attrs']));
            //Zend_Debug::dump($data); die();
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
        return $data;
    }

    public function get_submedia_by_id($id) {
        try {
            $sql = "select * from pmas_media_sub_crawl where  media_id=?";
            $data = $this->_db->fetchAll($sql, $id);

            foreach($data as $k => $v) {
                $new[$k] = $v;
                $new[$k]['params'] = unserialize(base64_decode($v['attrs']));
            }
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
        return $new;
    }
}
