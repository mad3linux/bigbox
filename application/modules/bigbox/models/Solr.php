<?php

class Bigbox_Model_Solr extends Zend_Db_Table_Abstract {

    function connect_solr() {
        $config = new Zend_Config_Ini(APPLICATION_PATH . 
                                      '/configs/application.ini', 'production');
        $config = $config->toArray();
        $options = array('hostname' =>$config['data']['pmas']['solr']['host'],
                         'login' =>$config['data']['pmas']['solr']['host'],
                         'user' =>$config['data']['pmas']['solr']['user'],
                         'password' =>$config['data']['pmas']['solr']['password'],
                         'port' =>$config['data']['pmas']['solr']['port'],
                         'path' =>$config['data']['pmas']['solr']['path']
                     );
        try {

            return new SolrClient($options);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    function objToArray($obj, &$arr){

        if(!is_object($obj) && !is_array($obj)){
            $arr = $obj;
            return $arr;
        }

        // Zend_Debug::dump($obj);

        // Zend_Debug::dump($obj->ss_is_quote_status);die();

        $obj = (array)$obj;

        foreach ($obj as $key => $value)
        {
        	// Zend_Debug::dump($key);
        	if($key==''||$key==null){
    			$key = 'NULL';
        	}

            if (!empty($value))
            {
                $arr[$key] = array();
                $this->objToArray($value, $arr[$key]);
            }
            else
            {
            $arr[$key] = $value;
            }
        }
        return $arr;
    }

    function execute($p=array()){

        // Zend_Debug::dump($p);die();

        $z = new Model_Cache();
        $cache = $z->cachefunc(3600);
        $id = $z->get_id("Bigbox_Model_Solr::execute", $p);
        $data = $z->get_cache($cache, $id);

        // $data = false;

        if(!$data){
            try {

                $client = $this->connect_solr();
                $query = new SolrQuery();
                $query->setQuery($query);

                if(isset($p["setStart"])){
                    $query->setRows((int)$p["setStart"]);
                }
                if(isset($p["setRows"])){
                    $query->setRows((int)$p["setRows"]);
                }
                if(isset($p["setFacet"]) && is_bool($p["setFacet"])){
                    $query->setFacet($p["setFacet"]);
                }
                if(isset($p["addField"]) && is_array($p["addField"]) && count($p["addField"])>0){
                    foreach ($p["addField"] as $key => $value) {
                        $query->addField($value);
                    }
                }
                if(isset($p["addFacetField"]) && is_array($p["addFacetField"]) && count($p["addFacetField"])>0){
                    foreach ($p["addFacetField"] as $key => $value) {
                        $query->addFacetField($value);
                    }
                }
                if(isset($p["addFilterQuery"]) && is_array($p["addFilterQuery"]) && count($p["addFilterQuery"])>0){
                    foreach ($p["addFilterQuery"] as $key => $value) {
                        $query->addFilterQuery($value);
                    }
                }
                if(isset($p["addFacetQuery"]) && is_array($p["addFacetQuery"]) && count($p["addFacetQuery"])>0){
                    foreach ($p["addFacetQuery"] as $key => $value) {
                        $query->addFacetQuery($value);
                    }
                }

                $query_response = $client->query($query); //Zend_Debug::dump($response);die();
                $response = $query_response->getResponse();

                return array(
                    "transaction"=>true,
                    "data"=>$response,
                    "message"=>"Success load cache.",
                );

            } catch (Exception $e) {

                return array(
                    "transaction"=>false,
                    "data"=>false,
                    "message"=>$e->getMessage(),
                );
                
            }
        }

        return array(
            "transaction"=>true,
            "data"=>$data,
            "message"=>"Success load cache.",
        );
    }

    function get_count_by_entity($p=array()){

        $z = new Model_Cache();
        $cache = $z->cachefunc(3600);
        $id = $z->get_id("Bigbox_Model_Solr::get_count_by_entity", $p);
        $data = $z->get_cache($cache, $id);
        // Zend_Debug::dump($data);die();

        if(!$data){

            $client = $this->connect_solr();
            $query = new SolrQuery();
            $query->setQuery($query);
            $query->setRows(0);
            $query->setFacet(true);
            $query->addFacetField('entity_id');

            if((isset($p['startdate']) && $p['startdate']!='') && (isset($p['enddate']) && $p['enddate']!='')){
                $query->addFilterQuery('content_date:['.$p['startdate'].'T00:00:00Z TO '.$p['enddate'].'T23:59:59Z]');
            }

            try {

                $query_response = $client->query($query); //Zend_Debug::dump($response);die();
                $tmp = (array)$query_response->getResponse();

                $response = array();
                $this->objToArray($tmp,$response);
                // Zend_Debug::dump($response);die();

                $data = array(
                    'numFound'=>$response['response']['numFound'],
                    'entity_id'=>$response['facet_counts']['facet_fields']['entity_id'],
                );
                
                $cache->save($data, $id, array('bigboxsystem'));

                return array(
                    'transaction'=>true,
                    'message'=>'Success',
                    'data'=>$data,
                );              
            } catch (Exception $e) {
                return array(
                    'transaction'=>false,
                    'message'=>$e->getMessage(),
                    'data'=>false,
                );
            }
        }

        return array(
            'transaction'=>true,
            'message'=>'Success load Cache',
            'data'=>$data,
        );
    }

    function get_mediaanalytic($p=array()){

        $z = new Model_Cache();
        $cache = $z->cachefunc(3600);
        $id = $z->get_id("Bigbox_Model_Solr::get_mediaanalytic", $p);
        $data = $z->get_cache($cache, $id);
        // Zend_Debug::dump($data);die();

        $data = false;

        if(!$data){
            $client = $this->connect_solr();
            $query = new SolrQuery();
            $query->setQuery($query);
            $query->setRows(0);
            $query->setFacet(true);
            $query->addFacetField('bundle')->addFacetField('is_sentiment');
            $query->addFilterQuery('entity_id:2');
            if((isset($p['startdate']) && $p['startdate']!='') && (isset($p['enddate']) && $p['enddate']!='')){
                $query->addFilterQuery('content_date:['.$p['startdate'].'T00:00:00Z TO '.$p['enddate'].'T23:59:59Z]');
            }
            if(isset($p['fq']['keys']) && count($p['fq']['keys'])>0){
                $query->addFilterQuery('content:("'.implode('" OR "', $p['fq']['keys']).'")');
            }
            if(isset($p['fq']['and']) && count($p['fq']['and'])>0){
                $query->addFilterQuery('content:('.implode(' OR ',$p['fq']['and']).')');
            }
            if(isset($p['fq']['exception']) && count($p['fq']['exception'])>0){
                $query->addFilterQuery('-content:('.implode(' OR ',$p['fq']['exception']).')');
            }            
            if(isset($p['fq']['keys']) && count($p['fq']['keys'])>0){
                foreach ($p['fq']['keys'] as $k => $keys) {
                    $query->addFacetQuery('content:"'.$keys.'"');
                }
            }

            try {

                $query_response = $client->query($query); //Zend_Debug::dump($response);die();
                $tmp = (array)$query_response->getResponse();

                $response = array();
                $this->objToArray($tmp,$response);
                // Zend_Debug::dump($response);die();

                $keywords = array();
                foreach($response['facet_counts']['facet_queries'] as $key=>$value){
                    $name = substr($key, 9,-1);
                    $keywords[$name] = $value;
                }

                $bundle = array_filter($response['facet_counts']['facet_fields']['bundle'], function($value){
                    return $value > 0;
                });

                $sentiment = $response['facet_counts']['facet_fields']['is_sentiment'];
                $sentiment = array(
                    'netral'=>$sentiment[0],
                    'positif'=>$sentiment[1],
                    'negatif'=>$sentiment[2],
                );

                $data = array(
                    'numFound'=>$response['response']['numFound'],
                    'keywords'=>$keywords,
                    'bundle'=>$bundle,
                    'sentiment'=>$sentiment,
                );
                
                $cache->save($data, $id, array('bigboxsystem'));

                return array(
                    'transaction'=>true,
                    'message'=>'Success',
                    'params'=>$p,
                    'data'=>$data,
                );              
            } catch (Exception $e) {
                return array(
                    'transaction'=>false,
                    'message'=>$e->getMessage(),
                    'params'=>$p,
                    'data'=>false,
                );
            }
        }

        return array(
            'transaction'=>true,
            'message'=>'Success load cache.',
            'params'=>$p,
            'data'=>$data,
        );
    }

    function getTwitterUserLocation($p=array()){

        $z = new Model_Cache();
        $cache = $z->cachefunc(3600);
        $id = $z->get_id("Bigbox_Model_Solr::getTwitterUserLocation", $p);
        $data = $z->get_cache($cache, $id);
        // Zend_Debug::dump($data);die();

        $data = false;

        if(!$data){

            try {

                $client = $this->connect_solr();
                $query = new SolrQuery();
                $query->setQuery($query);
                $query->setRows(0);
                $query->setFacet(true);
                $query->addFacetField('ss_user_location');

                $query_response = $client->query($query); //Zend_Debug::dump($response);die();
                $tmp = $query_response->getResponse(); //Zend_Debug::dump($tmp);die();

                $data = array();
                foreach ($tmp->facet_counts->facet_fields->ss_user_location as $key => $value) {
                    $data[] = $key;
                }

                $cache->save($data, $id, array('bigboxsystem'));

                return array(
                    'transaction'=>true,
                    'message'=>'Success load cache.',
                    'params'=>$p,
                    'data'=>$data,
                );
                
            } catch (Exception $e) {
                return array(
                    'transaction'=>false,
                    'message'=>$e->getMessage(),
                    'params'=>$p,
                    'data'=>false,
                );                
            }
        }

        return array(
            'transaction'=>true,
            'message'=>'Success load cache.',
            'params'=>$p,
            'data'=>$data,
        );
    }

    function get_twitteranalytic($p=array()){

        // Zend_Debug::dump($p);die();

        $z = new Model_Cache();
        $cache = $z->cachefunc(3600);
        $id = $z->get_id("Bigbox_Model_Solr::get_twitteranalytic", $p);
        $data = $z->get_cache($cache, $id);
        // Zend_Debug::dump($data);die();

        $data = false;

        if(!$data){
            $client = $this->connect_solr();
            $query = new SolrQuery();
            $query->setQuery($query);
            $query->setRows(0);
            $query->setFacet(true);
            $query->addFacetField('ss_is_quote_status')->addFacetField('is_sentiment')->addFacetField('ss_userScreenName')->addFacetField('ss_user_location');
            $query->addFilterQuery('entity_id:5');
            if((isset($p['startdate']) && $p['startdate']!='') && (isset($p['enddate']) && $p['enddate']!='')){
                $query->addFilterQuery('content_date:['.$p['startdate'].'T00:00:00Z TO '.$p['enddate'].'T23:59:59Z]');
            }
            if(isset($p['fq']['keys']) && count($p['fq']['keys'])>0){
                $query->addFilterQuery('content:("'.implode('" OR "', $p['fq']['keys']).'")');
            }
            if(isset($p['fq']['and']) && count($p['fq']['and'])>0){
                $query->addFilterQuery('content:('.implode(' OR ',$p['fq']['and']).')');
            }
            if(isset($p['fq']['exception']) && count($p['fq']['exception'])>0){
                $query->addFilterQuery('-content:('.implode(' OR ',$p['fq']['exception']).')');
            }
            if(isset($p['fq']['user_location']) && $p['fq']['user_location']!='none'){
                $query->addFilterQuery('ss_user_location:"'.$p['fq']['user_location'].'"');
            }
            if(isset($p['fq']['keys']) && count($p['fq']['keys'])>0){
                foreach ($p['fq']['keys'] as $k => $keys) {
                    $query->addFacetQuery('content:"'.$keys.'"');
                }
            }

            try {
                $query_response = $client->query($query); //Zend_Debug::dump($response);die();
                $tmp = $query_response->getResponse(); //Zend_Debug::dump($tmp);die();

                $keywords = (array)$tmp->facet_counts->facet_queries;
                $ss_is_quote_status = (array)$tmp->facet_counts->facet_fields->ss_is_quote_status;
                $is_sentiment = (array)$tmp->facet_counts->facet_fields->is_sentiment;
                $ss_userScreenName = (array)$tmp->facet_counts->facet_fields->ss_userScreenName;
                $ss_user_location = (array)$tmp->facet_counts->facet_fields->ss_user_location;
                // Zend_Debug::dump($ss_user_location);die();
                
                $data = array(
                    "numFound"=>$tmp->response->numFound,
                    "keywords"=>array(),
                    "ss_is_quote_status"=>array(),
                    "is_sentiment"=>array(),
                    "ss_userScreenName"=>array(),
                    "ss_user_location"=>array(),
                );
                foreach ($keywords as $key => $value) {
                    $name = substr($key, 9,-1);
                    $data["keywords"][$name]=$value;
                }
                foreach ($ss_is_quote_status as $key => $value) {
                    $name = ($key==""?"Retweet":"Tweet");
                    $data["ss_is_quote_status"][$name]=$value;
                }

                $sentiment = array("netral","positif","negatif");
                foreach ($is_sentiment as $key => $value) {
                    $data["is_sentiment"][$sentiment[$key]]=$value;
                }

                foreach ($ss_userScreenName as $key => $value) {
                    if($value>0){
                        $name = ($key==""?"NULL":$key);
                        $data["ss_userScreenName"][$name]=$value;
                    }
                }

                foreach ($ss_user_location as $key => $value) {
                    if($value>0){
                        $name = ($key==""?"NULL":$key);
                        $data["ss_user_location"][$name]=$value;
                    }
                }

                // Zend_Debug::dump($data);die();
                
                $cache->save($data, $id, array('bigboxsystem'));

                return array(
                    'transaction'=>true,
                    'message'=>'Success',
                    'params'=>$p,
                    'data'=>$data,
                );
            } catch (Exception $e) {
                return array(
                    'transaction'=>false,
                    'message'=>$e->getMessage(),
                    'params'=>$p,
                    'data'=>false,
                );
            }
        }

        return array(
            'transaction'=>true,
            'message'=>'Success load cache.',
            'params'=>$p,
            'data'=>$data,
        );
    }

    function get_docTwitter($p=array()){
            
        $z = new Model_Cache();
        $cache = $z->cachefunc(3600);
        $id = $z->get_id("Bigbox_Model_Solr::get_docTwitter", $p);
        $data = $z->get_cache($cache, $id);

        $data = false;

        if(!$data){

            $client = $this->connect_solr();
            $query = new SolrQuery();
            $query->setQuery($query);

            if(isset($p['fq']['entity_id']) && (int)$p['fq']['entity_id']>0){
                $query->addFilterQuery('entity_id:"'.($p['fq']['entity_id']).'"');
            }

            if(isset($p['fq']['keysearch']) && count($p['fq']['keysearch'])>0){
                $query->addFilterQuery('content:("'.implode('" OR "', $p['fq']['keysearch']).'")');
            }
            if(isset($p['fq']['andsearch']) && count($p['fq']['andsearch'])>0){
                $query->addFilterQuery('content:("'.implode('" AND "', $p['fq']['andsearch']).'")');
            }
            if(isset($p['fq']['andsearch']) && count($p['fq']['exsearch'])>0){
                $query->addFilterQuery('-content:("'.implode('" OR "', $p['fq']['exsearch']).'")');
            }

            if(
                (isset($p['fq']['startdate']) && $p['fq']['startdate']!='') &&
                (isset($p['fq']['enddate']) && $p['fq']['enddate']!='')
            ){
                $query->addFilterQuery('content_date:['.$p['fq']['startdate'].'T00:00:00Z TO '.$p['fq']['enddate'].'T23:59:59Z]');
            }

            if(isset($p['fq']['content_date']) && $p['fq']['content_date']!=''){
                $query->addFilterQuery('content_date:['.$p['fq']['content_date'].'T00:00:00Z TO '.$p['fq']['content_date'].'T23:59:59Z]');
            }
            if(isset($p['fq']['user_location']) && $p['fq']['user_location']!='none'){
                $query->addFilterQuery('ss_user_location:"'.$p['fq']['user_location'].'"');
            }
            if(isset($p['fq']['field']) && isset($p['fq']['value'])){

                if($p['fq']['field']=='is_sentiment'){
                    $sentiment = array('netral','positif','negatif');
                    $p['fq']['value'] = array_search($p['fq']['value'], $sentiment);
                } else if($p['fq']['field']=='ss_user_location' && $p['fq']['value']!='none'){
                    $p['fq']['value'] = $p['fq']['value'];
                }


                $query->addFilterQuery($p['fq']['field'].':"'.$p['fq']['value'].'"');
            }

            try {

                $query->addField('id')->addField('content')->addField('content_date')->addField('ss_userScreenName')->addField('ss_user_name')->addField('ss_user_profile_image_url_https');
                $query->addSortField('content_date',SolrQuery::ORDER_DESC);
                $query_response = $client->query($query);
                $data = $query_response->getResponse(); //Zend_Debug::dump($data);die();
                
                $cache->save($data, $id, array('bigboxsystem'));
                
                return array(
                    'transaction'=>true,
                    'message'=>'Success',
                    'params'=>$p,
                    'data'=>$data,
                );              
            } catch (Exception $e) {
                return array(
                    'transaction'=>false,
                    'message'=>$e->getMessage(),
                    'params'=>$p,
                    'data'=>false,
                );
            }

        }

        return array(
            'transaction'=>true,
            'message'=>'Success load cache.',
            'params'=>$p,
            'data'=>$data,
        );
    }
    function get_docMediaDigital($p=array()){
            
        $z = new Model_Cache();
        $cache = $z->cachefunc(3600);
        $id = $z->get_id("Bigbox_Model_Solr::get_docTwitter", $p);
        $data = $z->get_cache($cache, $id);

        $data = false;

        if(!$data){

            $client = $this->connect_solr();
            $query = new SolrQuery();
            $query->setQuery($query);

            if(isset($p['fq']['entity_id']) && (int)$p['fq']['entity_id']>0){
                $query->addFilterQuery('entity_id:"'.($p['fq']['entity_id']).'"');
            }

            if(isset($p['fq']['keysearch']) && count($p['fq']['keysearch'])>0){
                $query->addFilterQuery('content:("'.implode('" OR "', $p['fq']['keysearch']).'")');
            }
            if(isset($p['fq']['andsearch']) && count($p['fq']['andsearch'])>0){
                $query->addFilterQuery('content:("'.implode('" AND "', $p['fq']['andsearch']).'")');
            }
            if(isset($p['fq']['andsearch']) && count($p['fq']['exsearch'])>0){
                $query->addFilterQuery('-content:("'.implode('" OR "', $p['fq']['exsearch']).'")');
            }

            if(
                (isset($p['fq']['startdate']) && $p['fq']['startdate']!='') &&
                (isset($p['fq']['enddate']) && $p['fq']['enddate']!='')
            ){
                $query->addFilterQuery('content_date:['.$p['fq']['startdate'].'T00:00:00Z TO '.$p['fq']['enddate'].'T23:59:59Z]');
            }

            if(isset($p['fq']['content_date']) && $p['fq']['content_date']!=''){
                $query->addFilterQuery('content_date:['.$p['fq']['content_date'].'T00:00:00Z TO '.$p['fq']['content_date'].'T23:59:59Z]');
            }
            if(isset($p['fq']['user_location']) && $p['fq']['user_location']!='none'){
                $query->addFilterQuery('ss_user_location:"'.$p['fq']['user_location'].'"');
            }
            if(isset($p['fq']['field']) && isset($p['fq']['value'])){

                if($p['fq']['field']=='is_sentiment'){
                    $sentiment = array('netral','positif','negatif');
                    $p['fq']['value'] = array_search($p['fq']['value'], $sentiment);
                } else if($p['fq']['field']=='ss_user_location' && $p['fq']['value']!='none'){
                    $p['fq']['value'] = $p['fq']['value'];
                }


                $query->addFilterQuery($p['fq']['field'].':"'.$p['fq']['value'].'"');
            }

            try {

                $query  ->addField('id')
                        ->addField('content')
                        ->addField('content_date')
                        ->addField('bundle')
                        ->addField('bundle_name')
                        ->addField('label')
                        ->addField('ss_icon');
                $query->addSortField('content_date',SolrQuery::ORDER_DESC);
                $query_response = $client->query($query);
                $data = $query_response->getResponse(); //Zend_Debug::dump($data);die();
                
                $cache->save($data, $id, array('bigboxsystem'));
                
                return array(
                    'transaction'=>true,
                    'message'=>'Success',
                    'params'=>$p,
                    'data'=>$data,
                );              
            } catch (Exception $e) {
                return array(
                    'transaction'=>false,
                    'message'=>$e->getMessage(),
                    'params'=>$p,
                    'data'=>false,
                );
            }

        }

        return array(
            'transaction'=>true,
            'message'=>'Success load cache.',
            'params'=>$p,
            'data'=>$data,
        );
    }

    function get_mediaanalytic_trend($p=array()){

        // Zend_Debug::dump($p);die();

        $z = new Model_Cache();
        $cache = $z->cachefunc(3600);
        $id = $z->get_id("Bigbox_Model_Solr::get_mediaanalytic_trend", $p);
        $data = $z->get_cache($cache, $id);
        // Zend_Debug::dump($data);die();

        $data = false;

        if(!$data){
            $client = $this->connect_solr();
            $query = new SolrQuery();
            $query->setQuery($query);
            $query->setRows(10);
            $query->setFacet(true);
            if(isset($p['fq']['entity_id']) && (int)$p['fq']['entity_id']>0){
                $query->addFilterQuery('entity_id:"'.(int)$p['fq']['entity_id'].'"');

                if((int)$p['fq']['entity_id']==5){
                    $query->addField('id')->addField('ss_userScreenName')->addField('ss_user_profile_image_url_https')->addField('ss_user_profile_image_url_https')->addField('content')->addField('content_date');
                }
            }
            if((isset($p['fq']['startdate']) && $p['fq']['startdate']!='') && (isset($p['fq']['enddate']) && $p['fq']['enddate']!='')){
                $query->addFilterQuery('content_date:['.$p['fq']['startdate'].'T00:00:00Z TO '.$p['fq']['enddate'].'T23:59:59Z]');
            }
            if(isset($p['fq']['keys']) && count($p['fq']['keys'])>0){
                $query->addFilterQuery('content:("'.implode('" OR "', $p['fq']['keys']).'")');
            }
            if(isset($p['fq']['and']) && count($p['fq']['and'])>0){
                $query->addFilterQuery('content:("'.implode('" OR "',$p['fq']['and']).'")');
            }
            if(isset($p['fq']['exception']) && count($p['fq']['exception'])>0){
                $query->addFilterQuery('-content:("'.implode('" OR "',$p['fq']['exception']).'")');
            }
            if(isset($p['fq']['user_location']) && $p['fq']['user_location']!='none'){
                $query->addFilterQuery('ss_user_location:"'.$p['fq']['user_location'].'"');
            }
            if(isset($p['fq']['field']) && isset($p['fq']['value'])){

                if($p['fq']['field']=='is_sentiment'){
                    $sentiment = array('netral','positif','negatif');
                    $p['fq']['value'] = array_search($p['fq']['value'], $sentiment);
                } else if($p['fq']['field']=='ss_user_location' && $p['fq']['value']!='none'){
                    $p['fq']['value'] = $p['fq']['value'];
                }


                $query->addFilterQuery($p['fq']['field'].':"'.$p['fq']['value'].'"');
            }

            $query->setParam('facet.range','content_date');
            $query->setParam('facet.range.start',$p['fq']['startdate'].'T00:00:00Z');
            $query->setParam('facet.range.end',$p['fq']['enddate'].'T23:59:59Z');
            $query->setParam('facet.range.gap','+1DAY');
            // $query->setFacetOffset(0);

            try {

                $query_response = $client->query($query);
                $data = $query_response->getResponse(); //Zend_Debug::dump($data);die();

                // $data = array();
                // $this->objToArray($tmp,$data);
                // Zend_Debug::dump($data);die('X');
                
                $cache->save($data, $id, array('bigboxsystem'));

                return array(
                    'transaction'=>true,
                    'message'=>'Success',
                    'params'=>$p,
                    'data'=>$data,
                );              
            } catch (Exception $e) {
                return array(
                    'transaction'=>false,
                    'message'=>$e->getMessage(),
                    'params'=>$p,
                    'data'=>false,
                );
            }
        }

        return array(
            'transaction'=>true,
            'message'=>'Success load cache.',
            'params'=>$p,
            'data'=>$data,
        );
    }


}