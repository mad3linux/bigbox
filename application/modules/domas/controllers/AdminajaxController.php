<?php

/**
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>,Jul 15, 2014
 *
 */

class Domas_AdminajaxController extends Zend_Controller_Action {

    public function init() {
    }

    public function editapiAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            $mdl = new Model_Prabasystem();
            $update = $mdl->update_api($_POST);
            if($update['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$update['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function geturlrawAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        if($_POST) {
            //Zend_Debug::dump($_POST); die();	
            $uname = explode("::", $_POST['uname']);
            $input = strtolower(preg_replace("/[^a-zA-Z]+/", "", $_POST['input']));
            if($_POST['urltype'] == '2') {
                
                //Zend_Debug::dump($_POST); die();
                $find = array("1",
                              "2",
                              "3",
                              "4",
                              "5",
                              "6",
                              "7",
                              "8",
                              "9",
                              "0");
                $replace = array("c",
                                 "e",
                                 "g",
                                 "i",
                                 "m",
                                 "n",
                                 "q",
                                 "r",
                                 "u",
                                 "x");
                $api = str_replace($find, $replace, $_POST['api']);
                //Zend_Debug::dump($uname);die();
                $ptype = str_replace($find, $replace, $_POST['ptype']);
                $uname = str_replace($find, $replace, $uname[0]);
                $format = array('json' => 1,
                                'xml' => 2);
             
               
               if($_POST['aptype']=='post') {
                   
                    $e = $api . "z" . $format[$_POST['format']] . "z" . $ptype;
                   $result = array('retCode' => '00',
                                'retMsg' => '<a href="http://' .$_SERVER['HTTP_HOST']. '/domaspublish/post/e/' .$e. '/id/' .$input. '" >link</a>',
                                'result' => true,
                                'data' =>$_POST);
                } else {
               
                $result = array('retCode' => '00',
                                'retMsg' => '<a href="http://' .$_SERVER['HTTP_HOST']. '/domaspublish/esb/e/' .$e. '/id/' .$input. '" >link</a>',
                                'result' => true,
                                'data' =>$_POST);
                }
                                
            } else {
                $input = $_POST['input'];
                $var = explode('~', $_POST['input']);

                foreach($var as $k => $v) {
                    $zz = explode(':', $v);
                    $new['input:' . $zz[0]] = $zz[1];
                }
                $text = array('username' =>$_POST['uname'],
                              'api_id' =>$_POST['api'],
                              'format' =>$_POST['format'],
                              'ptype' =>$_POST['ptype'],
                             );
                //$text = array_merge($text, $new);
                //Zend_Debug::dump($text);die();
                // 'input:customer_name'=>'ARY_DANAMON');
                
                if($_POST['aptype']=='post') {
                    $result = array('retCode' => '00',
                                'retMsg' => '<a href="http://' .$_SERVER['HTTP_HOST']. '/domaspublish/post/id/' .$_POST['api']. '/ptype/' .$_POST['ptype']. '/format/' .$_POST['format']. '" >link</a>',
                                'result' => true,
                'data' =>$_POST);
                }else {
                $result = array('retCode' => '00',
                                'retMsg' => '<a href="http://' .$_SERVER['HTTP_HOST']. '/domaspublish/esb/id/' .$_POST['api']. '/ptype/' .$_POST['ptype']. '/uname/' .$uname[1]. '/format/' .$_POST['format']. '" >link</a>',
                                'result' => true,
                'data' =>$_POST);
                }
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function deleteportletAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            // Zend_Debug::dump($_POST); die();
            $mdl_zusr = new Model_Prabasystem();
            $delete = $mdl_zusr->delete_portlet($_POST['uid']);
            if($delete['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$delete['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $delete = array('retCode' => '11',
                                'retMsg' =>$delete['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function initialformAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $params = $this->getRequest()->getParams();
        $this->_helper->layout->disableLayout();
        $cc = new Prabacontent_Model_Builder();
        $dta = $cc->get_initial_form($params['id']);
        //Zend_Debug::dump($dta); die();
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        // echo json_encode($records);
        $json = json_encode($dta);
        print $json;
        die();
    }

    public function updateformAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $c2 = new CMS_LIB_parse();
        $this->_helper->layout->disableLayout();
        $mdl_admin = new Model_Prabasystem();
        $params = $this->getRequest()->getParams();
        if($_POST) {
            $content = $_POST['form'];
            $content = $c2->remove($content, "<!--", "-->");
            $content = $c2->remove($content, "<style", "</style>");
            $content = $c2->remove($content, "<script", "</script>");
            $content = preg_replace('/\s+/', ' ', $content);
            //Zend_Debug::dump($content); die();
            $dom_1 = new Zend_Dom_Query($content);
            $page = $dom_1->query('.control-label');

            foreach($page as $v) {
                $id[$v->getAttribute('for')] =($v->getAttribute('for'));
                $label[$v->getAttribute('for')] =($v->textContent);
            }
            //	Zend_Debug::dump($id);die();		
            $formattr = $dom_1->query('#formatform');
            $datapostapi = $formattr->current()->getAttribute('data-postapi');
            $datapostmessage = $formattr->current()->getAttribute('data-postmessage');
            $dataposturl = $formattr->current()->getAttribute('data-posturl');
            $datagetapi = $formattr->current()->getAttribute('data-getapi');

            foreach($id as $c) {
                $x[$c] = $dom_1->query('#' . 
                                       $c);
                $y[$c] = $dom_1->query("#" . 
                                       $c . 
                                       " option");
                //$y[$c] = $dom_1->query("option");	

                foreach($x[$c] as $v2) {
                    $api[$c] =($v2->getAttribute('data-api'));
                    $type[$c] =($v2->getAttribute('type'));
                    $initval[$c] =($v2->getAttribute('data-initval'));
                    $placeholder[$c] =($v2->getAttribute('placeholder'));
                    $class[$c] =($v2->getAttribute('class'));
                    $required[$c] =($v2->getAttribute('required'));
                    $typeid[$c] =($v2->getAttribute('data-typeid'));
                    $value[$c][] =($v2->getAttribute('value'));
                    //$typeid =$formattr->current()->getAttribute('data-typeid');
                }
                if($y[$c]->current()->textContent != "") {
                    $value[$c] = array();

                    foreach($y[$c] as $v2) {
                        $value[$c][] = $v2->textContent;
                    }
                }
            }
            $help[] = $dom_1->query('.help-block')->current()->textContent;
            $i = 0;

            foreach($id as $v) {
                $field[$i]['id'] = $v;
                $field[$i]['type'] = $type[$v];
                $field[$i]['name'] = $v;
                $field[$i]['class'] = $class[$v];
                $field[$i]['typeid'] = $typeid[$v];
                $field[$i]['label'] = $label[$v];
                $field[$i]['initvalue'] = $initval[$v];
                $field[$i]['initApi'] = $api[$v];
                $field[$i]['required'] = $required[$v];
                $field[$i]['area'] = 'form-body';
                $field[$i]['help'] = $help[$i];
                $field[$i]['placeholder'] = $placeholder[$v];
                if(is_array($value[$v])and count($value[$v])> 0) {
                    $field[$i]['initvalue'] = implode("~", $value[$v]);
                }
                $field[$i]['placeholder'] = $placeholder[$v];
                $i ++;
            }
            $var = array("id" =>$_POST['pid'],
                         "postmessage" =>$datapostmessage,
                         "api_id" => implode(',',
                        $datagetapi),
                         "post_api" =>$datapostapi,
                         "post_url" =>$dataposturl,
                         "field" =>$field);
            #Zend_Debug::dump($var);die();		
            $cc = new Prabacontent_Model_Builder();
            $update = $cc->formbuilder($_POST['pid'], $var);
            if($update['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$update['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function listportletjsonAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $mdl_admin = new Model_Prabasystem();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params); die();
        $list = $mdl_admin->listapiportletjson($params);
        // Zend_Debug::dump($list); die();
        $zz = 1;

        foreach($list as $k => $v) {
            $records['total'] = 10;
            $records['movies'][0]['id'] = "";
            $records['movies'][0]['title'] = "";
            if(isset($params['id'])) {
                $records['total'] = 1;
            }
            $records['movies'][$zz]['id'] = $v['id'];
            if(is_numeric($v['id'])) {
                $records['movies'][$zz]['title'] = $v['id'] . '_' . $v['portlet_name'];
            } else {
                $records['movies'][$zz]['title'] = $v['portlet_name'];
            }
            $zz ++;
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        // echo json_encode($records);
        $jsonp_callback = isset($_GET['callback'])? $_GET['callback'] : null;
        $json = json_encode($records);
        print $jsonp_callback ? "$jsonp_callback($json)" : $json;
        die();
    }

    public function updateportletAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            // Zend_Debug::dump($_POST); die();
            $mdl = new Model_Prabasystem();
            $_POST['name'] = strtolower($_POST['name']);
            $_POST['name'] = str_replace(array("/", ",", "%", " ", ".", ">", "<", "-", ":", ")", "(", "_", "&",), "", $_POST['name']);
            $update = $mdl->update_portlet($_POST);
            if($update['result'] === true) {
                $_POST['id'] = $update['id'];
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' => "eor",
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function addportletAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            $mdl = new Model_Prabasystem();
            $_POST['name'] = strtolower($_POST['name']);
            $_POST['name'] = str_replace(array("/", ",", "%", " ", ".", ">", "<", "-", ":", ")", "(", "_", "&",), "", $_POST['name']);
            $update = $mdl->add_portlet($_POST);
            if($update['result'] === true) {
                $_POST['id'] = $update['id'];
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' => "eor",
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function listportletAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $mdl_admin = new Model_Zprabapage();
        $countgraph = $mdl_admin->count_listportlet($_GET);
        $listgraph = $mdl_admin->listportlet_ajax($_GET);
        // Zend_Debug::dump($listgraph);die();
        $iTotalRecords = intval($countgraph);
        $iDisplayLength = intval($_GET['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_GET['iDisplayStart']);
        $sEcho = intval($_GET['sEcho']);
        $records = array();
        $records["aaData"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $mdl = new Model_Prabasystem();
        // $apps = $mdl->get_apps();

        foreach($listgraph as $k => $v) {
            $records["aaData"][] = array($iDisplayStart+$k+ 1,
                                        $v['id'],
                                        $v['portlet_name'],
                                        $v['portlet_title'],
                                        $v['portlet_type'],
                                        $v['custom_view'],
                                         '<div style="text-align:center"><a href="/prabagen/viewportlet/id/' .$v['id']. '" data-id="' .$v['id']. '" class="btn btn-xs blue btn-edit"><i class="fa fa-search"></i> View</a><a href="/prabacontent/prabaeditor/editportlet/id/' .$v['id']. '" data-id="' .$v['id']. '" class="btn btn-xs green btn-edit"><i class="fa fa-search"></i> Edit</a>' . '<a  class="btn btn-xs yellow getscript" data-id="' .$v['id']. '" data-toggle="modal"><i class="fa fa-times"></i>Get Script</a>' . '<a href="javascript:;" data-id="' .$v['id']. '"  class="btn btn-xs red btn-delete"><i class="fa fa-times"></i> Delete</a></div>');
        }
        $records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;
        
        /*
         * $gg = new Escort_Model_Kpu(); $res = $gg->get_datakpu($data2['witel']); //var_dump($res);die();
         */
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }

    public function addpageAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            $mdl = new Model_Prabasystem();
            // Zend_Debug::dump($_POST); die();
            $_POST['pagealias'] = strtolower($_POST['pagealias']);
            $_POST['pagealias'] = str_replace(array("/", ",", "%", " ", ".", ">", "<", "-", ":", ")", "(", "_", "&",), "", $_POST['pagealias']);
            $br = array();
            for($i = 0;
            $i < count ($_POST['name'] );
            $i ++ ) {
                $br[$i]['name'] = $_POST['name'][$i];
                $br[$i]['href'] = $_POST['href'][$i];
                $br[$i]['class'] = $_POST['class'][$i];
            }
            $el = array();
            $k = 0;
            for($i = 1;
            $i <= count ($_POST['col1'] );
            $i ++ ) {
                if($_POST['col1'][$k] != "") {
                    $el[$i][1] = $_POST['col1'][$k];
                }
                if($_POST['col2'][$k] != "") {
                    $el[$i][2] = $_POST['col2'][$k];
                }
                if($_POST['col3'][$k] != "") {
                    $el[$i][3] = $_POST['col3'][$k];
                }
                if($_POST['col4'][$k] != "") {
                    $el[$i][4] = $_POST['col4'][$k];
                }
                if($_POST['col5'][$k] != "") {
                    $el[$i][5] = $_POST['col5'][$k];
                }
                if($_POST['col6'][$k] != "") {
                    $el[$i][6] = $_POST['col6'][$k];
                }
                $k ++;
            }
            //Zend_Debug::dump($el); die();
            $update = $mdl->add_page($_POST, $br, $el);
            if($update) {
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$update['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function deletepageAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            // Zend_Debug::dump($_POST); die();
            $mdl_zusr = new Model_Prabasystem();
            $delete = $mdl_zusr->delete_page($_POST['uid']);
            if($delete['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$delete['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $delete = array('retCode' => '11',
                                'retMsg' =>$delete['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function listpageAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $mdl_admin = new Model_Zprabapage();
        $countgraph = $mdl_admin->count_listpage($_GET);
        $listgraph = $mdl_admin->listpage_ajax($_GET);
        // Zend_Debug::dump($listgraph);die();
        $iTotalRecords = intval($countgraph);
        $iDisplayLength = intval($_GET['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_GET['iDisplayStart']);
        $sEcho = intval($_GET['sEcho']);
        $records = array();
        $records["aaData"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $mdl = new Model_Prabasystem();
        $apps = $mdl->get_apps();

        foreach($listgraph as $k => $v) {
            $records["aaData"][] = array($iDisplayStart+$k+ 1,
                                        $v['id'],
                                        $v['name_alias'],
                                        $v['layout_id'],
                                        $v['workspace_id'],
                                        $apps[$v['app_id']],
                                        $v['title'],
                                         '<div style="text-align:center"><a href="/prabacontent/prabaeditor/editpage/id/' .$v['id']. '" data-id="' .$v['id']. '" class="btn btn-xs green btn-edit"><i class="fa fa-search"></i> Edit Page</a>' . '<a href="/prabagen/index/page/' .$v['id']. '" data-id="' .$v['id']. '" class="btn btn-xs yellow btn-add"><i class="fa fa-search"></i> View Page</a>' . '<a href="javascript:;" data-id="' .$v['id']. '"  class="btn btn-xs red btn-delete"><i class="fa fa-times"></i> Delete Page</a></div>');
        }
        $records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;
        
        /*
         * $gg = new Escort_Model_Kpu(); $res = $gg->get_datakpu($data2['witel']); //var_dump($res);die();
         */
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }

    public function editloginappAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($_POST); die();
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            $mdl = new Model_Prabasystem();
            $cc = new Model_System();
            $update = $mdl->update_applogin($_POST);
            if($update['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
                $cc->generate_themes($_POST['id']);
            } else {
                $re1sult = array('retCode' => '11',
                                 'retMsg' =>$update['message'],
                                 'result' => false,
                                 'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function editappAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($_POST); die();
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            $mdl = new Model_Prabasystem();
            $update = $mdl->update_app($_POST);
            if($update['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$update['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function addappAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($_POST); die();
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            $cc = new Model_System();
            $mdl = new Model_Prabasystem();
            $_POST['short'] = strtolower($_POST['short']);
            $_POST['short'] = str_replace(array("/", ",", "%", " ", ".", ">", "<", "-", ":", ")", "(", "_", "&",), "", $_POST['short']);
            $cc->generate_modules($_POST['short'], $_POST);
            $update = $mdl->add_app($_POST);
            if($update['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$update['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function editconnAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            $mdl = new Model_Prabasystem();
            $update = $mdl->update_conn($_POST);
            if($update['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$update['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function testconnAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            $mdl = new Model_Prabasystem();
            $update = $mdl->test_conn($_POST);
            // Zend_Debug::dump($_POST); die();
            if($update['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$update['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function addconnAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            $mdl = new Model_Prabasystem();
            $update = $mdl->add_conn($_POST);
            if($update['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$update['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function delconnAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            // Zend_Debug::dump($_POST); die();
            $mdl_zusr = new Model_Prabasystem();
            $delete = $mdl_zusr->delete_conn($_POST['id']);
            if($delete['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$delete['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $delete = array('retCode' => '11',
                                'retMsg' =>$delete['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function deleteapiAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            // Zend_Debug::dump($_POST); die();
            $mdl_zusr = new Model_Prabasystem();
            $delete = $mdl_zusr->delete_api($_POST['uid']);
            if($delete['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$delete['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $delete = array('retCode' => '11',
                                'retMsg' =>$delete['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function addapiAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            //Zend_Debug::dump($_POST); die();
            $mdl = new Model_Prabasystem();
            $update = $mdl->add_api($_POST);
            if($update['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$update['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function getmethodsAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        switch($params['apitype']) {
            case 4 : $out = $this->get_method_zend($params);
            break;
            case 3 : case 2 : $out = $this->get_method_proc($params);
            break;
        }
        echo $out;
        die();
    }

    function get_method_proc($params) {
        //Zend_Debug::dump($params); die();
        $mm = new Model_Prabasystem();
        $dd = new Model_Prabasystemdriver();
        $data = $mm->get_a_conn_by_name($params['conn']);
        $dat = $dd->get_list_proc($data, null, $params['xxxid']);
        $this->_helper->layout->disableLayout();
        // $out ="";
        $out = "<option></option>";

        foreach($dat as $k => $v) {
            $out .= "<option value='" . $v . "'>" . $v . "</option>";
        }
        echo $out;
        die();
    }

    function get_method_zend($params) {
        $this->_helper->layout->disableLayout();
        $mm = new Model_Prabasystem();
        $dat = $mm->get_method_class_model($params['xxxid']);
        // $out ="";
        $out = "<option></option>";

        foreach($dat as $k => $v) {
            $out .= "<option value='" . $v . "'>" . $v . "</option>";
        }
        echo $out;
        die();
    }

    public function getmodelsAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        switch($params['xxxid']) {
            case 4 : $out = $this->modelZend($params);
            break;
            case 3 : case 2 : $out = $this->modelProc($params);
            break;
        }
        echo $out;
        die();
    }

    function modelProc($params) {
        $mm = new Model_Prabasystem();
        $dd = new Model_Prabasystemdriver();
        $data = $mm->get_a_conn($params['conn']);
        //Zend_Debug::dump($data);die();
        $list = $dd->get_list_proc($data, 'user');
        $out = '<div class="resType"><div  class="col-md-5">
										<div class="form-group">
											<label for="model">Schema</label><select onchange=changeModel(this)  name="model" id="model" class="form-control" placeholder="Model" >
											<option></option>';

        foreach($list as $v) {
            $out .= "<option value='" . $v . "'>" . $v . "</option>";
        }
        $out .= '</select>
					<span class="help-block">
												Select Model Class
											</span>
										</div>
									</div>
									<div class="col-md-offset-1 col-md-5 ">
										<div class="form-group">
											<label for="method">Proc</label><select  onchange=changeMethod(this)  name="method" id="method" class="form-control" placeholder="Method" >
											</select>
										</div>
									</div></div>';
        return $out;
    }

    function modelZend($params) {
        $mm = new Model_Prabasystem();
        $dat = $mm->get_models_class();
        $out = '<div class="resType"><div  class="col-md-5">
										<div class="form-group">
											<label for="model">Class Model</label><select onchange=changeModel(this)  name="model" id="model" class="form-control" placeholder="Model" >
											<option></option>';

        foreach($dat as $k => $v) {
            // Zend_Debug::dump($v);
            $out .= "<optgroup label='" . $k . "'>";

            foreach($v as $vv) {
                $out .= "<option value='" . $vv . "'>" . $vv . "</option>";
            }
            $out .= "</optgroup>";
        }
        $out .= '</select>
											<span class="help-block">
												Select Model Class
											</span>
										</div>
									</div>
									<div class="col-md-offset-1 col-md-5 ">
										<div class="form-group">
											<label for="method">Method Class</label><select  onchange=changeMethod(this)  name="method" id="method" class="form-control" placeholder="Method" >
											</select>
										</div>
									</div></div>';
        return $out;
    }

    public function getoutAction() {
        $zk = new Model_Prabasystemaddon();
        $m1 = new Model_Zpraba4api();
        $dd = new Model_Zprabapage();
        $params = $this->getRequest()->getParams();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            $dd = new Model_Zprabapage();
            //$varC = $dd->get_api ($_POST['id'] );
            //Zend_Debug::dump($_POST );
            $conn = $dd->get_conn($_POST['conname']);
            //Zend_Debug::dump($conn );die();
            $data = $_POST;
            //b.conn_name, b.conn_params, b.desc as optionals, b.is_deactivate 
            $data['conn_name'] = $conn['conn_name'];
            $data['conn_params'] = $conn['conn_params'];
            $data['optionals'] = $conn['optionals'];
            $data['is_deactivate'] = $conn['is_deactivate'];
            $data['backendcache'] = $_POST['backendcache'];
            $data['sql_text'] = $_POST['sqltext'];
            $data['api_desc'] = $_POST['apidesc'];
            $data['api_type'] = $_POST['apitype'];
            //	$data['api_str_replace'] =  $_POST['is_deactivate'];
            $data['attrs1'] = $_POST['attrs1'];
            $data['cache_time'] = $_POST['cache'];
            $data['api_mode'] = $_POST['mode'];
            $mdl = new Model_Prabasystem();
            $conn = unserialize($conn['conn_params']);
            //Zend_Debug::dump($conn );die();	
            $vdata = explode('~', $_POST['inputsqltext']);
            $input = array();

            foreach($vdata as $k => $v) {
                $jj[$k] = explode('=', $v);
                $input[$jj[$k][0]] = $jj[$k][1];
            }
            $update = $m1->execute_api($conn, $data, $input);
            //Zend_Debug::dump($input);
            //Zend_Debug::dump($result);die();
        }
        //header ( "Access-Control-Allow-Origin: *" );
        //header ( 'Content-Type: application/json' );
        $out = array();
        if($_POST['mode'] == 2) {

            foreach($update['data'][0] as $k => $v) {
                $out[] = $k;
            }
        }
        if($_POST['mode'] == 3) {

            foreach($update['data'] as $k => $v) {
                $out[] = $k;
            }
        }
        die(implode(',', $out));
        die();
    }

    public function tesapiAction() {
        $zk = new Model_Prabasystemaddon();
        $m1 = new Model_Zpraba4api();
        $dd = new Model_Zprabapage();
        $params = $this->getRequest()->getParams();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            $dd = new Model_Zprabapage();
            //$varC = $dd->get_api ($_POST['id'] );
            $conn = $dd->get_conn_id($_POST['conname']);
            //Zend_Debug::dump($conn );die();
            $data = $_POST;
            //b.conn_name, b.conn_params, b.desc as optionals, b.is_deactivate 
            $data['conn_name'] = $conn['conn_name'];
            $data['conn_params'] = $conn['conn_params'];
            $data['optionals'] = $conn['optionals'];
            $data['is_deactivate'] = $conn['is_deactivate'];
            $_POST['cache'] = "0";
            $data['backendcache'] = $_POST['backendcache'];
            $data['sql_text'] = $_POST['sqltext'];
            $data['api_desc'] = $_POST['apidesc'];
            $data['api_type'] = $_POST['apitype'];
            //	$data['api_str_replace'] =  $_POST['is_deactivate'];
            $data['attrs1'] = $_POST['attrs1'];
            $data['cache_time'] = $_POST['cache'];
            $data['api_mode'] = $_POST['mode'];
            $mdl = new Model_Prabasystem();
            $conn = unserialize($conn['conn_params']);
            //Zend_Debug::dump($data );die();	
            $vdata = explode('~', $_POST['inputsqltext']);
            $input = array();

            foreach($vdata as $k => $v) {
                $jj[$k] = explode('=', $v);
                $input[$jj[$k][0]] = $jj[$k][1];
            }
            //Zend_Debug::dump($conn); 
            //Zend_Debug::dump($data);die(); 
            $update = $m1->execute_api($conn, $data, $input);
            //Zend_Debug::dump($input);
            //Zend_Debug::dump($result);die();
        }
        //header ( "Access-Control-Allow-Origin: *" );
        //header ( 'Content-Type: application/json' );
        Zend_Debug::dump($update);
        die();
    }

    public function listapiAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $mdl_admin = new Model_Prabasystem();
        $countgraph = $mdl_admin->count_listapi($_GET);
        $params = $mdl_admin->get_params('api_type');

        foreach($params as $v) {
            $vparams[$v['value_param']] = $v['display_param'];
        }
        $listgraph = $mdl_admin->listapi($_GET);
        // Zend_Debug::dump($countgraph);die();
        $iTotalRecords = intval($countgraph);
        $iDisplayLength = intval($_GET['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_GET['iDisplayStart']);
        $sEcho = intval($_GET['sEcho']);
        $records = array();
        $records["aaData"] = array();
        $end = $iDisplayStart + $iDisplayLength;

        foreach($listgraph as $k => $v) {
            if($v['is_running_socket'] != 1) {
                $run = '<br><span class="label label-sm label-danger">
													socket server not running
												</span>';
            } elseif($v['is_running_socket'] == 1) {
                $run = '<br><span class="label label-sm label-success">
													socket server  running
												</span>';
            }
            if($v['is_freeze'] != 1) {
                // $soc = '<div style="text-align:center"><a href="javascript:;" data-id="' . $v['id'] . '" class="btn btn-xs blue btn-freeze"><i class="fa fa-search"></i> freeze </a>';
            } elseif($v['is_running_socket'] == 1) {
                //$soc = '';
            }
            $records["aaData"][] = array($iDisplayStart+$k+ 1,
                                        $v['id'],
                                        $v['conn_name'],
                                        $v['sql_text'],
                                        $v['api_desc'],
                                        $vparams[$v['api_type']],
                                        $v['api_str_replace'],
                                        $v['attrs1'],
                                        $v['cache_time'],
                                         '<div style="text-align:center"><a href="/domas/editor/get-url/id/' .$v['id']. '"  class="btn btn-xs green btn-edit"><i class="fa fa- share-alt"></i> Publish Api</a><a href="/domas/editor/editapi/id/' .$v['id']. '" data-id="' .$v['id']. '" class="btn btn-xs green btn-edit"><i class="fa fa-search"></i> Edit Api</a>' .$soc. '<a href="javascript:;" data-id="' .$v['id']. '" data-uname="' .$v['uname']. '" data-fullname="' .$v['fullname']. '" data-email="' .$v['email']. '" data-ubis="' .$v['ubis']. '" data-sububis="' .$v['sub_ubis']. '" data-sububisid="' .$v['sub_ubis_id']. '" data-position="' .$v['jabatan']. '" data-mobilephone="' .$v['mobile_phone']. '" data-fixedphone="' .$v['fixed_phone']. '" class="btn btn-xs red btn-delete"><i class="fa fa-times"></i> Delete Api</a></div>');
        }
        $records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;
        
        /*
         * $gg = new Escort_Model_Kpu(); $res = $gg->get_datakpu($data2['witel']); //var_dump($res);die();
         */
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }
}
