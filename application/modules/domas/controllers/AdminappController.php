<?php

/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class Domas_AdminappController extends Zend_Controller_Action {

    public function init() {
        
        /* Initialize action controller here */
    }
 public function crawlogsAction() {
        //  die("x");
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
    }
    public function logsAction() {
        //  die("x");
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
    }

    public function manageparamsAction() {
        $params = $this->getRequest()->getParams();
        $mdl_sys = new Model_System();
        $mdl_page = new Model_Zprabapage();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $cc = new Domas_Model_Params();
        $data = $cc->get_params_pmas();
        //Zend_Debug::dump($data); die();
        $this->view->data = $data;
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2-bootstrap.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/css/custom.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jquery-nestable/jquery.nestable.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/uniform/css/uniform.default.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-nestable/jquery.nestable.js');
    }

    public function manageusersappsAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/table-ajax.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/ui-extended-modals.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        //$this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-touchspin/bootstrap.touchspin.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $params = $this->getRequest()->getParams();
        $mdl_sys = new Model_System();
        $apps = $mdl_sys->get_app_modules();
        //Zend_Debug::dump($apps); die();
        $roles = $mdl_sys->get_roles();
        $this->view->roles = $roles;
        $mdl_gsys = new Model_Generalsystem();
        $ubis = $mdl_gsys->get_raw_ubis();
        $this->view->ubisarr = $ubis;
        $sub_ubis = array();
        $sub_ubis = $mdl_gsys->get_raw_sub_ubis($ubis[0]['id']);
        $this->view->sububisarr = $sububis;
        $this->view->varams = $params;
        $this->view->apps = $apps;
    }

    public function compactAction() {
        $c = new Model_Admin();
        $c->compact_users();
        die("oke");
    }

    public function testAction() {
        $dd = new Pmas_Model_Pubtwitter();
        $dd->pub_pos_twitter2();
        $cc = new Model_Admin();
        $cc->synch_users();
        $cc = new Model_Zprabaexec();
        $data = $cc->get_metadata_conn(1);
        $c = new Metadata_Model_Hadoop();
        $c->get_metadata(array());
    }

    public function manageusersAction() {
        //  die("x");
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $params = $this->getRequest()->getParams();
        $mdl_sys = new Model_System();
        $apps = $mdl_sys->get_app_modules();
        $roles = $mdl_sys->get_roles();
        $this->view->roles = $roles;
        $mdl_gsys = new Model_Generalsystem();
        $ubis = $mdl_gsys->get_raw_ubis();
        $this->view->ubisarr = $ubis;
        $sub_ubis = array();
        $sub_ubis = $mdl_gsys->get_raw_sub_ubis($ubis[0]['id']);
        //Zend_Debug::dump($sub_ubis ); die();
        $this->view->sububisarr = $sub_ubis;
        $this->view->apps = $apps;
        $this->view->varams = $params;
    }

    public function manageuserAction() {
        //$this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($params);die();
        if(!isset($params['uname'])|| $params['uname'] == '') {
            $this->_redirect('/admin/manageusers');
        }
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        //Zend_Debug::dump($identity);die();
        $this->view->me = $identity;
        $mdl_sys = new Model_System();
        $roles = $mdl_sys->get_roles();
        //Zend_Debug::dump($roles); die();
        $this->view->roles = $roles;
        $mdl_zusr = new Model_Zusers();
        $data = $mdl_zusr->getuser($params['uname']);
        //Zend_Debug::dump($data);die();
        $mdl_gsys = new Model_Generalsystem();
        $ubis = $mdl_gsys->get_raw_ubis();
        //Zend_Debug::dump($ubis); die();
        $this->view->ubisarr = $ubis;
        $sub_ubis = array();
        //Zend_Debug::dump($data);
        if(isset($data['ubis_id'])&& $data['ubis_id'] != '') {
            $sub_ubis = $mdl_gsys->get_raw_sub_ubis($data['ubis_id']);
            //Zend_Debug::dump($sub_ubis); die();
        } else {
            $sub_ubis = $mdl_gsys->get_raw_sub_ubis($ubis[0]['id']);
            //Zend_Debug::dump($sub_ubis); die();
        }
        $this->view->sububisarr = $sub_ubis;
        //Zend_Debug::dump($data);
        $rols = $mdl_sys->get_roles_by_uid($data['id']);
        //Zend_Debug::dump($rols); die('xxx');
        $rolearr2 = array();

        foreach($rols as $k => $v) {
            array_push($rolearr2, $v['gid']);
        }
        $apps = $mdl_sys->get_app_modules();
        $this->view->params = $params;
        $this->view->apps = $apps;
        $this->view->data = $data;
        $this->view->rolearr = $rolearr2;
    }

    public function managemenusAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/global/css/custom.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/css/plugins.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-toastr/toastr.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-toastr/toastr.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/fuelux/js/spinner.min.js');
        $mdl_sys = new Model_System();
        $menu = $mdl_sys->get_menus_by_app();
        //die();
        //Zend_Debug::dump($menu); die();
        $menus = array();
        $unmenus = array();
        $main = array();
        $main2 = array();
        $childs1 = array();
        $child1 = array();
        $childs2 = array();
        $child2 = array();
        //level1

        foreach($menu as $k => $v) {
            if($v['parent_id'] == "" || $v['parent_id'] == "0") {
                $menus[$v['id']] = $v;
                $main[] = $v['id'];
            } else {
                $childs1[$v['parent_id']][$v['id']] = $v;
            }
        }
        ksort($menus);
        ksort($childs1);
        //Zend_Debug::dump($menus);//die();
        //echo '<hr>';
        //Zend_Debug::dump($childs1);//die();
        //level2

        foreach($childs1 as $k => $v) {
            if(in_array($k, $main)) {

                foreach($v as $kk => $vv) {
                    $child1[$vv['id']] = $vv;
                    $main2[] = $vv['id'];
                }
            } else {
                $childs2[$k] = $v;
            }
        }
        ksort($child1);
        ksort($childs2);

        foreach($childs2 as $k => $v) {
            if(in_array($k, $main2)) {
                $child1[$k]['child'] = $v;
                //Zend_Debug::dump($child1[$k]);
                //echo $k.'<hr>';
            } else {

                foreach($v as $kk => $vv) {
                    $unmenus[] = $vv;
                }
            }
        }

        foreach($child1 as $k => $v) {
            $menus[$v['parent_id']]['child'][] = $v;
        }
        $cc1 = new Model_Zparams();
        $class_icon = $cc1->get_icons();
        //Zend_Debug::dump($class_icon);die();
        $this->view->icon = $class_icon;
        $this->view->menus = $menus;
        $this->view->unmenus = $unmenus;
        //Zend_Debug::dump($armenu);die();
    }

    public function managerolesAction() {
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $mdl_sys = new Model_System();
        $roles = $mdl_sys->get_roles();
        //die();
        $this->view->roles = $roles;
    }

    public function manageroleAction() {
        $params = $this->getRequest()->getParams();
        $mdl_sys = new Model_System();
        $mdl_page = new Model_Zprabapage();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2-bootstrap.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/css/custom.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jquery-nestable/jquery.nestable.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/uniform/css/uniform.default.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-nestable/jquery.nestable.js');
        //   $this->view->headScript()->appendFile(' /assets/core/global/plugins/uniform/jquery.uniform.min.js');
        $app = $mdl_sys->get_controllers(true);
        //Zend_Debug::dump($app); die();
        $menu = $mdl_sys->get_menus_by_app();
        //die();
        //Zend_Debug::dump($menu); die();
        $pages = $mdl_page->list_page();
        // Zend_Debug::dump($pages);die("s");
        $menus = array();
        $unmenus = array();
        $main = array();
        $main2 = array();
        $childs1 = array();
        $child1 = array();
        $childs2 = array();
        $child2 = array();
        //level1

        foreach($menu as $k => $v) {
            if($v['parent_id'] == "" || $v['parent_id'] == "0") {
                $menus[$v['id']] = $v;
                $main[] = $v['id'];
            } else {
                $childs1[$v['parent_id']][$v['id']] = $v;
            }
        }
        ksort($menus);
        ksort($childs1);
        //Zend_Debug::dump($menus);//die();
        //echo '<hr>';
        //Zend_Debug::dump($childs1);//die();
        //level2

        foreach($childs1 as $k => $v) {
            if(in_array($k, $main)) {

                foreach($v as $kk => $vv) {
                    $child1[$vv['id']] = $vv;
                    $main2[] = $vv['id'];
                }
            } else {
                $childs2[$k] = $v;
            }
        }
        ksort($child1);
        ksort($childs2);

        foreach($childs2 as $k => $v) {
            if(in_array($k, $main2)) {
                $child1[$k]['child'] = $v;
                //Zend_Debug::dump($child1[$k]);
                //echo $k.'<hr>';
            } else {

                foreach($v as $kk => $vv) {
                    $unmenus[] = $vv;
                }
            }
        }

        foreach($child1 as $k => $v) {
            $menus[$v['parent_id']]['child'][] = $v;
        }
        $role = array();
        if(isset($params['gid'])) {
            $role = $mdl_sys->get_role_by_id($params['gid']);
            //Zend_Debug::dump($role);die();
        }
        $activemenus = array();
        if(isset($params['gid'])) {
            $actmenu = $mdl_sys->get_menus_by_role($params['gid']);

            foreach($actmenu as $k => $v) {
                $activemenus[] = $v['id'];
            }
            //Zend_Debug::dump($activemenus);die();
        }
        $roles = $mdl_sys->get_roles();
        //die();
        //Zend_Debug::dump($roles); die();
        $this->view->roles = $roles;
        $this->view->role = $role;
        $this->view->menus = $menus;
        $this->view->unmenus = $unmenus;
        $this->view->activemenus = $activemenus;
        $this->view->app = $app;
        $this->view->params = $params;
        $this->view->pages = $pages;
        $this->view->getpages = unserialize($role['pages']);
    }

    public function historylogAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        $cc = new Model_Zlogs();
        $data = array('level' => 'WARNING',
                      'message' => 'alert system',
                      'context' => array('ip' => '198.1.1.20'),
                      'logger' => 'SYSTEM');
        $t = $cc->insert_log($data);
        // Zend_Debug::dump($t); die();   
    }
}
