<?php

/**
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>,Jul 15, 2014
 *
 */
// ini_set("display_errors", "on");

class Domas_Ajax1Controller extends Zend_Controller_Action {

    

    public function wordcloudAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        if($params["cate"] != "") {
            $params["cat"] = $params["cate"];
        }
        $parameter = array();

        foreach($params as $k => $v) {
            if($k != "module" && $k != "controller" && $k != "action") {
                $parameter[$k] = $v;
            }
        }
        // Zend_Debug::dump($parameter);die();
        $s = new Domas_Model_Solranalis();
        $z = new Model_Cache();
        $cache = $z->cachefunc(4000);
        $id = $z->get_id("Domas_Model_Solranalis_get_wordcloud", $parameter);
        $data = $z->get_cache($cache, $id);
        // $data = false;
        if(!$data) {
            $data = $s->get_wordcloud($parameter);
            // Zend_Debug::dump($data);die("s");
            $cache->save($data, $id, array('systembin'));
        }
        // Zend_Debug::dump($data);die("s");
        $tmp1 = array();
        $tmp2 = array();
        $i = 0;

        foreach($data as $k => $v) {
            if($v > 0) {
                $p = "?is_sentiment=" . $params["is_sentiment"] . "&is_mediatype=" . $params["is_mediatype"] . "&reg=" . $params["reg"] . "&cat=" . $params["cat"] . "&startdate=" . $params["d1"] . "&enddate=" . $params["d2"] . "&searchkey=" . $params["searchkey"] . "&search=" . str_replace("(ORG)", "", str_replace("(PER)", "", $k));
                if($params["skey"] != "") {
                    $p = "?is_sentiment=" . $params["is_sentiment"] . "&is_mediatype=" . $params["is_mediatype"] . "&reg=" . $params["reg"] . "&cat=" . $params["cat"] . "&startdate=" . $params["d1"] . "&enddate=" . $params["d2"] . "&searchkey=" . $params["searchkey"] . "&search=" . str_replace('"', '', str_replace(" AND ", ",", $params["skey"] . "," . str_replace("(ORG)", "", str_replace("(PER)", "", $k))));
                }
                // Zend_Debug::dump($params["searchkey"]);
                // trim(str_replace("(ORG)","",str_replace("(PER)","",$params["skey"])))
                $tmp1[$i]["text"] = $k;
                $tmp1[$i]["weight"] = $v;
                $tmp1[$i]["link"] = $p;
                $tmp2[$i][0] = $k;
                $tmp2[$i][1] = $v;
                $i ++;
            }
        }
        $this->view->tmp1 = $tmp1;
        $this->view->tmp2 = $tmp2;
        $this->view->varams = $params;
        // Zend_Debug::dump($tmp1);die();
    }

    public function wordcloudhariAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        $parameter = array();

        foreach($params as $k => $v) {
            if($k != "module" && $k != "controller" && $k != "action") {
                $parameter[$k] = $v;
            }
        }
        if($params["cate"] != "") {
            $params["cat"] = $params["cate"];
        }
        // if($params["hr"]=="1"){
        // $parameter["d2"] = date("Y-m-d", strtotime($params["d2"]));
        // $parameter["d1"] = $parameter["d2"];
        // } else if($params["hr"]=="2"){
        // $parameter["d2"] = date("Y-m-d", strtotime($params["d2"].' -1 day'));
        // $parameter["d1"] = $parameter["d2"];
        // } else if($params["hr"]=="3"){
        // $parameter["d2"] = date("Y-m-d", strtotime($params["d2"].' -2 day'));
        // $parameter["d1"] = $parameter["d2"];
        // }
        $parameter["buzz"] = 'buzzer';
        // Zend_Debug::dump($parameter);die();
        $as = new Domas_Model_Solranalis();
        $z = new Model_Cache();
        $cache = $z->cachefunc(4000);
        $id = $z->get_id("Domas_Model_Solranalis_get_toptren_twitter", $parameter);
        $data = $z->get_cache($cache, $id);
        // Zend_Debug::dump($data);die();
        // $data = false;
        if(!$data) {
            $data = $as->get_toptren_twitter($parameter);
            $cache->save($data, $id, array('systembin'));
        }
        // Zend_Debug::dump($data);die("s");
        $tmp1 = array();
        $tmp2 = array();
        $i = 0;

        foreach($data as $k => $v) {
            if($v > 0) {
                if($i <= 9) {
                    $p = "?is_sentiment=" . $params["is_sentiment"] . "&is_mediatype=" . $params["is_mediatype"] . "&reg=" . $params["reg"] . "&cat=" . $params["cat"] . "&startdate=" . $parameter["d1"] . "&enddate=" . $parameter["d2"] . "&searchkey=" . $params["searchkey"] . "&search=" . $v["ss_retweeted_user_screenname"];
                   ;
                    if($params["skey"] != "") {
                        $p = "?is_sentiment=" . $params["is_sentiment"] . "&is_mediatype=" . $params["is_mediatype"] . "&reg=" . $params["reg"] . "&cat=" . $params["cat"] . "&startdate=" . $parameter["d1"] . "&enddate=" . $parameter["d2"] . "&searchkey=" . $params["searchkey"] . "&search=" . $v["ss_retweeted_user_screenname"];
                       ;
                    }
                    // Zend_Debug::dump($v);die("s");
                    // Zend_Debug::dump($params["searchkey"]);
                    // trim(str_replace("(ORG)","",str_replace("(PER)","",$params["skey"])))
                    $tmp1[$i]["text"] = $v["ss_retweeted_user_screenname"];
                    $tmp1[$i]["weight"] = $v["count"];
                    $tmp1[$i]["link"] = $p;
                    $tmp2[$i][0] = $v["ss_retweeted_user_screenname"];
                    $tmp2[$i][1] = $v["count"];
                    $i ++;
                }
            }
        }
        $this->view->tmp1 = $tmp1;
        $this->view->tmp2 = $tmp2;
        $this->view->varams = $parameter;
        // Zend_Debug::dump($tmp1);die();
    }

    public function buzzerAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        $parameter = array();

        foreach($params as $k => $v) {
            if($k != "module" && $k != "controller" && $k != "action") {
                $parameter[$k] = $v;
            }
        }
        $data = array();
        for($i = 0;
        $i <= 2;
        $i ++ ) {
            if($i == "0") {
                $parameter["d2"] = date("Y-m-d", strtotime($params["d2"]));
                $parameter["d1"] = $parameter["d2"];
                $parameter["t"] = '1';
            } else if($i == "1") {
                $parameter["d2"] = date("Y-m-d", strtotime($params["d2"] . ' -1 day'));
                $parameter["d1"] = $parameter["d2"];
                $parameter["t"] = '2';
            } else if($i == "2") {
                $parameter["d2"] = date("Y-m-d", strtotime($params["d2"] . ' -2 day'));
                $parameter["d1"] = $parameter["d2"];
                $parameter["t"] = '3';
            }
            $parameter["buzz"] = 'buzzer';
            // Zend_Debug::dump($parameter);die();
            $as = new Domas_Model_Solranalis();
            $z = new Model_Cache();
            $cache = $z->cachefunc(4000);
            $id = $z->get_id("Domas_Model_Solranalis_get_toptren_twitter", $parameter);
            $data = $z->get_cache($cache, $id);
            // if($i=="2"){
            // Zend_Debug::dump($data);die();
            // }
            //
            // $data = false;
            if(!$data) {
                $data = $as->get_toptren_twitter($parameter);
                $cache->save($data, $id, array('systembin'));
            }
            $temp[$i] = $data;
        }
        // die();
        $tmp1 = array();
        $tmp2 = array();
        $i = 0;

        foreach($temp[0] as $k1 => $v1) {
            // Zend_Debug::dump('1 === '.$v1["ss_retweeted_user_screenname"]);

            foreach($temp[1] as $k2 => $v2) {
                // Zend_Debug::dump($v2["ss_retweeted_user_screenname"]);
                if($v1["ss_retweeted_user_screenname"] == $v2["ss_retweeted_user_screenname"]) {
                    $tmp1[$i] = $v1;
                    $i ++;
                }
            }
        }
        $i = 0;

        foreach($tmp1 as $k3 => $v3) {
            // Zend_Debug::dump($v3["ss_retweeted_user_screenname"]);

            foreach($temp[2] as $k4 => $v4) {
                // Zend_Debug::dump($v4["ss_retweeted_user_screenname"]);
                if($v3["ss_retweeted_user_screenname"] == $v4["ss_retweeted_user_screenname"]) {
                    $tmp2[$i] = $v3;
                    $i ++;
                }
            }
        }
        // Zend_Debug::dump($temp[0]);
        // Zend_Debug::dump($temp[1]);
        // Zend_Debug::dump($temp[2]);
        // Zend_Debug::dump($tmp1);
        // Zend_Debug::dump($tmp2);
        // die();
        // // foreach($data as $k=>$v){
        // // if($v>0){
        // // if($i<=9){
        // // $p = "?is_sentiment=".$params["is_sentiment"]."&is_mediatype=".$params["is_mediatype"]."&reg=".$params["reg"]."&cat=".$params["cat"]."&startdate=".$parameter["d1"]."&enddate=".$parameter["d2"]."&searchkey=".$params["searchkey"]."&search=".$v["ss_retweeted_user_screenname"];;
        // // if($params["skey"]!=""){
        // // $p = "?is_sentiment=".$params["is_sentiment"]."&is_mediatype=".$params["is_mediatype"]."&reg=".$params["reg"]."&cat=".$params["cat"]."&startdate=".$parameter["d1"]."&enddate=".$parameter["d2"]."&searchkey=".$params["searchkey"]."&search=".$v["ss_retweeted_user_screenname"];;
        // // } 
        // // // Zend_Debug::dump($v);die("s");
        // // // Zend_Debug::dump($params["searchkey"]);
        // // // trim(str_replace("(ORG)","",str_replace("(PER)","",$params["skey"])))
        // // $tmp1[$i]["text"] = $v["ss_retweeted_user_screenname"];
        // // $tmp1[$i]["weight"] = $v["count"];
        // // $tmp1[$i]["link"] = $p;
        // // $tmp2[$i][0] = $v["ss_retweeted_user_screenname"];
        // // $tmp2[$i][1] = $v["count"];
        // // $i++;
        // // }
        // // }
        // // }
        $this->view->tmp1 = $tmp1;
        $this->view->tmp2 = $tmp2;
        $this->view->varams = $parameter;
        // Zend_Debug::dump($tmp1);die();
    }

    public function smarkersAction() {
    }

    public function sentimentAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        if($params["cate"] != "") {
            $params["cat"] = $params["cate"];
        }
        $parameter = array();

        foreach($params as $k => $v) {
            if($k != "module" && $k != "controller" && $k != "action") {
                $parameter[$k] = $v;
            }
        }
        // Zend_Debug::dump($parameter);die();
        $s = new Domas_Model_Solranalis();
        $z = new Model_Cache();
        $cache = $z->cachefunc(4000);
        $id = $z->get_id("Domas_Model_Solranalis_get_sentimentx", $parameter);
        $data = $z->get_cache($cache, $id);
        // Zend_Debug::dump($params);die("s");
         $data = false;
        if(!$data) {
            $data = $s->get_sentimentx($parameter);
            // if($data!=NULL) {
            $cache->save($data, $id, array('systembin'));
            // }
        }
        // die("s");
        // Zend_Debug::dump($data);die("s");
        $arr_color = array("#3598DC",
                           "#E43A45",
                           "#26C281",
                          );
        $series = array();
        $i = 0;

        foreach($data as $k => $v) {
            $ii = 0;

            foreach($v as $k1 => $v1) {
                if($k1 % 2 == 0) {
                    $xx = strtotime(date("Y-m-d H:i:s", strtotime($v1)));
                }
                if($k1 % 2 == 1) {
                    // $data[$i] = array((int)$xx*1000 , (int)$v+$positif_ndigmed[$k]);
                    $datax[$ii] = array((int)$xx* 1000,
                                        (int)$v1);
                    $ii ++;
                }
            }
            $series[$i]["name"] = $k;
            $series[$i]["type"] = "column";
            $series[$i]["data"] = $datax;
            $series[$i]["color"] = $arr_color[$i];
            $i ++;
        }
        // Zend_Debug::dump($series);
        $this->view->params = $params;
        $this->view->vpos = $series;
    }

    public function trendlineAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        
        $s = new Domas_Model_Solranalis();
        $z = new Model_Cache();
        $cache = $z->cachefunc(4000);
        $id = $z->get_id("Domas_Model_Solranalis_get_trendline", $params);
        $data = $z->get_cache($cache, $id);
        // Zend_Debug::dump($data);die("s");
        // $data = false;
        if(!$data) {
            $data = $s->get_trendline($params);
            $cache->save($data, $id, array('systembin'));
        }
        // die("s");
        // Zend_Debug::dump($data);die("s");
        $series = array();
        $i = 0;

        foreach($data as $k0 => $v0) {
            $series[$i]["name"] = $k0;
            $ii = 0;

            foreach($v0 as $k1 => $v1) {
                if($k1 % 2 == 0) {
                    $xx = strtotime(date("Y-m-d H:i:s", strtotime($v1)));
                }
                if($k1 % 2 == 1) {
                    // $tmpnews["data"][] = $v;
                    $series[$i]["data"][$ii] = array((int)$xx* 1000,
                                                     (int)$v1);
                    $ii ++;
                }
            }
            // $series[$i]["data"] = $k0;
            $i ++;
        }
        // Zend_Debug::dump($series);die();
        $this->view->series = $series;
        $this->view->vparams = $params;
    }

    public function trendlinepersenAction() {
        // date_default_timezone_set('Europe/London');
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        // $parameter = array(
        // "skey"=>$params["skey"],
        // "skeyexception"=>$params["skeyexception"],
        // "cat"=>$params["cat"],
        // "searchkey"=>$params["searchkey"],
        // "topik"=>$params["topik"],
        // "is_mediatype"=>$params["is_mediatype"],
        // "gap"=>$params["gap"],
        // "d1"=>$params["d1"],
        // "d2"=>$params["d2"],
        // "is_sentiment"=>$params["is_sentiment"],
        // );
        $params["d1"] = date("Y-m-d", strtotime($params['d1'] . " 00:00:00 +1 days"));
        $params["d2"] = date("Y-m-d", strtotime($params['d2'] . " 23:59:59 -1 days"));
        // Zend_Debug::dump($parameter);die();
        $s = new Domas_Model_Solranalis();
        $z = new Model_Cache();
        $cache = $z->cachefunc(4000);
        $id = $z->get_id("Domas_Model_Solranalis_get_trendline", $params);
        $data = $z->get_cache($cache, $id);
        // Zend_Debug::dump($data);die("s");
        // $data = false;
        if(!$data) {
            $data = $s->get_trendline($params);
            $cache->save($data, $id, array('systembin'));
        }
        // die("s");
        // Zend_Debug::dump($data);die("s");
        $series = array();
        $seriesmax = array();
        $i = 0;
        $a = 0;

        foreach($data as $k0 => $v0) {
            $series[$i]["name"] = $k0;
            $ii = 0;

            foreach($v0 as $nm => $v1) {
                if($nm % 2 == 1) {
                    // $tmpnews["data"][] = $v;
                    $seriesmax[$i][$a] = $v1;
                    $nilaimax = max($seriesmax[$i]);
                    $ii ++;
                    $a ++;
                }
            }
            $iii = 0;
            $nilai = $nilaimax;

            foreach($v0 as $k1 => $v1) {
                if($k1 % 2 == 0) {
                    $xx = strtotime(date("Y-m-d H:i:s", strtotime($v1)));
                }
                if($k1 % 2 == 1) {
                    // $tmpnews["data"][] = $v;
                    $seriesmax[$i][$a] = $v1;
                    $nilaimax = max($seriesmax[$i]);
                    //$series[$i]["data"][$ii] = array((int)$xx*1000 , (int)$v1, round($v1/$nilai*100, 2));
                    $series[$i]["data"][$iii] = array((int)$xx* 1000,
                                                      round($v1/$nilai* 100,
                                                      2));
                    $iii ++;
                    $a ++;
                    // Zend_Debug::dump($nilai);
                    // Zend_Debug::dump($v1/$nilai*100);
                    // Zend_Debug::dump($v1);
                }
            }
            // $series[$i]["data"] = $k0;
            $i ++;
        }
        // die();
        // Zend_Debug::dump($nilai);
        // Zend_Debug::dump($series);die();
        $this->view->series = $series;
        $this->view->nilai = $nilai;
        $this->view->vparams = $params;
    }

    public function trendlinesiiAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        // $parameter = array(
        // "skey"=>$params["skey"],
        // "skeyexception"=>$params["skeyexception"],
        // "cat"=>$params["cat"],
        // "searchkey"=>$params["searchkey"],
        // "topik"=>$params["topik"],
        // "is_mediatype"=>$params["is_mediatype"],
        // "gap"=>$params["gap"],
        // "d1"=>$params["d1"],
        // "d2"=>$params["d2"],
        // "is_sentiment"=>$params["is_sentiment"],
        // );
        // Zend_Debug::dump($parameter);die();
        $s = new Domas_Model_Solranalis();
        $z = new Model_Cache();
        $cache = $z->cachefunc(4000);
        $id = $z->get_id("Domas_Model_Solranalis_get_trendline_sii", $params);
        $data = $z->get_cache($cache, $id);
        // // Zend_Debug::dump($data);die("s");
        // // $data = false;
        if(!$data) {
            $data = $s->get_trendline_sii($params);
            $cache->save($data, $id, array('systembin'));
        }
        // die("s");
        // Zend_Debug::dump($data);die("s");
        $series = array();
        $i = 0;

        foreach($data as $k0 => $v0) {
            $series[$i]["name"] = $k0;
            $ii = 0;

            foreach($v0 as $k1 => $v1) {
                if($k1 % 2 == 0) {
                    $xx = strtotime(date("Y-m-d H:i:s", strtotime($v1)));
                }
                if($k1 % 2 == 1) {
                    // $tmpnews["data"][] = $v;
                    $series[$i]["data"][$ii] = array((int)$xx* 1000,
                                                     (int)$v1);
                    $ii ++;
                }
            }
            // $series[$i]["data"] = $k0;
            $i ++;
        }
        // Zend_Debug::dump($series);die();
        $this->view->series = $series;
    }

    public function wwwwwhAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        $g = new Domas_Model_Pdash();
        $s = new Domas_Model_Solranalis();
        $parameter = array();

        foreach($params as $k => $v) {
            if($k != "module" && $k != "controller" && $k != "action" && $k != "anomali") {
                $parameter[$k] = $v;
            }
        }
        // // if($parameter['t']=='Laporan' && $parameter['cat']!='') {
        // // // Zend_Debug::dump($parameter);die();
        // // $z = new Model_Cache();
        // // $cache = $z->cachefunc(40000);    
        // // $id = $z->get_id("Domas_Model_Solranalis_get_5w1hbdi", $parameter);
        // // $tmp = $z->get_cache($cache, $id);
        // // // die("s"); 
        // // // $tmp = false;
        // // if(!$tmp) {
        // // $tmp = $s->get_5w1hbdi($parameter);
        // // $cache->save ( $tmp, $id, array ('systembin') );
        // // }
        // // }
        // // Zend_Debug::dump($tmp);die();
        $z = new Model_Cache();
        $cache = $z->cachefunc(4000);
        $id = $z->get_id("Domas_Model_Solranalis_get_5w1h", $parameter);
        $tmp = $z->get_cache($cache, $id);
        // Zend_Debug::dump($tmp);die("s");    
        // $tmp = false;
        if(!$tmp) {
            $tmp = $s->get_5w1h($parameter);
            $cache->save($tmp, $id, array('systembin'));
        }
        // Zend_Debug::dump($tmp);die();
        //-============= start get stopword
        $tmp_sw = $g->get_stopword("who");
        // Zend_Debug::dump($tmp_sw);die("s");
        $stopword = array();

        foreach($tmp_sw as $k => $v) {
            // $stopword[$k] = (strlen($v["name"])>1 && substr($v["name"],0,1)=="n"?strtolower(substr($v["name"],1)):strtolower($v["name"]));
            $stopword[$k] = strtolower($v["name"]);
        }
        $c2 = new CMS_LIB_parse();
        $data = array();

        foreach($tmp as $k0 => $v0) {
            $data[$k0]["id"] = $v0["id"];
            $data[$k0]["bundle"] = $v0["bundle"];
            $data[$k0]["bundle_name"] = $v0["bundle_name"];
            $data[$k0]["content_date"] = $v0["content_date"];
            $data[$k0]["title"] = $v0["title"];
            $data[$k0]["content"] = $v0["content"];
            $data[$k0]["ss_user_screenname"] = $v0["ss_user_screenname"];
            $data[$k0]["ss_user_name"] = $v0["ss_user_name"];
            $data[$k0]["label"] = $v0["label"];
            $data[$k0]["ss_author"] = $v0["ss_author"];

            foreach($v0["sm_who"] as $k1 => $v1) {
                if(!in_array(strtolower($v1), $stopword)) {
                    $data[$k0]["sm_who"][] = $v1;
                }
            }
            $data[$k0]["sm_what"] = $v0["sm_what"];
            $data[$k0]["sm_when"] = $v0["sm_when"];
            $data[$k0]["sm_where"] = $v0["sm_where"];
            $data[$k0]["sm_how"] = $v0["sm_how"];
            $data[$k0]["sm_why"] = $v0["sm_why"];
            $ss_summary = $c2->remove($v0["ss_summary"], "<!--", "--/>");
            // ---====
            $ss_summary = $c2->remove($ss_summary, "<xml>", "</xml>");
            // ---====
            $ss_summary = $c2->remove($ss_summary, "<!", "-- >");
            // ---====
            $ss_summary = $c2->remove($ss_summary, "<!--", "   ");
            // ---====
            $data[$k0]["ss_summary"] = trim($ss_summary);
        }
        // Zend_Debug::dump($data);die("s");
        if(isset($params["ajax"])&& $params["ajax"] == 1) {
            // Zend_Debug::dump($data);die("s");
            $yy = $data[0]["content_date"];
            $yy = date("d F Y", strtotime(substr($yy, 0, 10)));
            // echo $yy;die();
            // Zend_Debug::dump($data[0]substr($x["content_date"],0,10));//die("s");
            //1227
            header("Content-type: application/octet-stream");
            header("Content-Disposition: attachment; filename=SUMMARY_" . 
                   $params["t"] . 
                   " " . 
                   $yy . 
                   ".doc");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo "<html>";
            echo "<head>";
            echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">";
            echo "<style>";
            echo "body {
		font-family: Arial, Verdana, Helvetica, Courier, sans-serif;
		font-size:12pt;
	}";
            echo "</style>";
            echo "</head>";
            echo "<body>";
            echo "<span style=\"color:#000000;font-size:12px;font-family:Arial,'Trebuchet MS', Verdana;\">";
            //Zend_Debug::dump($cache);die(); 

            foreach($data as $k => $x) {
                $id = $x["id"];
                $author =($x["ss_author"] != "" ? $x["ss_author"] : "-");
                $publisher =($x["bundle"] != "" ? $x["bundle"] : "-");
                $content_date = date("l, d F Y H:i", strtotime(substr($x["content_date"], 0, 10)));
                $url = "https://twitter.com/" . trim($x["ss_user_name"]). "/status/" . $id;
                $label = $x["ss_user_screenname"] . " @" . $x["ss_user_name"];
                $text = $x["content"];
                if($params["t"] != "Social Media") {
                    $url = $id;
                    $label =($x["label"] != "" ? $x["label"] : $id);
                    $text = "";
                    $text .= "Pada ";
                    $text .= " " . date(" d F Y ", strtotime($content_date)). " ";
                    $text .= " di ";
                    $text .= implode(",", $x["sm_where"]). ",";
                    $text .= implode(",", $x["sm_who"]). ",";
                    if(count($x["sm_what"])> 0 && $x["sm_what"][0] != "") {
                        $text .= ' melakukan ';
                        $text .= implode(",", $x["sm_what"]). ",";
                    }
                    if(count($x["sm_how"])> 0 && $x["sm_how"][0] != "") {
                        $text .= ' dengan ';
                        $text .= implode(",", $x["sm_how"]). ",";
                    }
                    if(count($x["sm_why"])> 0 && $x["sm_why"][0] != "") {
                        $text .= ' karena ';
                        $text .= implode(",", $x["sm_why"]). "<br />";
                    }
                }
                // $url = base64_decode($x["ext_id"]);
                echo '<h3><a href="' . $url . '">' . $label . '</a></h3>';
                if($params["t"] == "News") {
                    echo "sumber : " . $url . "<br>";
                }
                echo '<div style="text-align:justify;text-justify:inter-word">';
                echo $text;
                echo '</div>';
                echo "<hr>";
            }
            echo "</span>";
            echo "</body>";
            echo "</html>";
            die();
        }
        // Zend_Debug::dump($params);die();
        // die("s");
        $this->view->varams = $params;
        $this->view->data = $data;
    }

    public function linkAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);//die();
        $g = new Domas_Model_Pdash();
        $ff = new Domas_Model_Withsolrpmas();
        $str = "*:*";
        //-============= start fq content
        $skey = null;
        $arr_pencarian = array();
        $arr_searchkey = array();
        $arr_skeyexception = array();
        $pencarian = "";
        if($params["skey"] != "") {
            $arr_pencarian = explode(",", $params["skey"]);
        }
        if($params["searchkey"] != "") {
            $arr_searchkey = explode(",", $params["searchkey"]);
        }
        if($params["skeyexception"] != "") {
            $arr_skeyexception = explode(",", $params["skeyexception"]);
        }
        $z = new Model_Cache();
        $cache = $z->cachefunc(4000);
        $id = $z->get_id("Domas_Model_Pdash_get_topik_by_tema", array($params["cat"], $params["topik"]));
        $tmp_tp = $z->get_cache($cache, $id);
        if(!$tmp_tp) {
            $tmp_tp = $g->get_topik_by_tema($params["cat"], $params["topik"]);
            $cache->save($tmp_tp, $id, array('systembin'));
        }
        $tp = array();

        foreach($tmp_tp as $k => $v) {
            $st = $g->get_subtema_by_tema($v["tid"]);

            foreach($st as $k1 => $v1) {
                array_push($tp, $v1);
            }
        }
        // Zend_Debug::dump($tp);die();
        $arr_topik = array();

        foreach($tp as $k => $v) {
            array_push($arr_topik, trim(strtolower($v["name"])));
        }
        if(count(array_unique($arr_topik))> 500) {
            array_splice($arr_topik, 320);
        }
        // Zend_Debug::dump($arr_topik);die();
        $content1 = null;
        $sm_what_and = null;
        if(count($arr_pencarian)> 0) {
            $content1 = '&fq=content:("' . urlencode(implode('" AND "', $arr_pencarian)). '")+OR+sm_what:("' . urlencode(implode('" OR "', $arr_pencarian)). '")+OR+label:("' . urlencode(implode('" OR "', $arr_pencarian)). '")';
        }
        $content2 = null;
        if(count($arr_topik)> 0) {
            $content2 = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_topik))). '")';
        }
        $content3 = null;
        $sm_what_or = null;
        if(count($arr_searchkey)> 0) {
            $content3 = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")+OR+sm_what:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")+OR+label:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")';
        }
        $content4 = null;
        $sm_what_or = null;
        if(count($arr_skeyexception)> 0) {
            $content3 = '&fq=-content:("' . urlencode(implode('" OR "', array_unique($arr_skeyexception))). '")+OR+-sm_what:("' . urlencode(implode('" OR "', array_unique($arr_skeyexception))). '")+OR+-label:("' .(implode('" OR "', array_unique($arr_skeyexception))). '")';
            // $sm_what_or = 'sm_what:("'.(implode('" OR "',array_unique($arr_searchkey))).'")';
        }
        //-============= end fq content
        //-============= start fq content_date range
        $d1 = date("Y-m-d", strtotime('monday last week'));
        $d2 = date("Y-m-d");
        if(isset($params["d1"])&& $params["d1"] != "" && isset($params["d2"])&& $params["d2"] != "") {
            $d1 = date("Y-m-d", strtotime($params['d1']));
            $d2 = date("Y-m-d", strtotime($params['d2']));
            //Zend_Debug::dump($d1);
            //Zend_Debug::dump($d2);
            //die();
        }
        // $filter .="&content_date:[".$d1.$d2."]";
        $cdd = "&fq=content_date:[" . $d1 . "T00:00:00Z+TO+" . $d2 . "T23:59:59Z]";
        //-============= end fq content_date range
        //-============= start fq sentiment
        $sen = null;
        if(isset($params["is_sentiment"])&& $params["is_sentiment"] != "") {
            $sen = "&fq=is_sentiment:0";
            if(strtolower($params["is_sentiment"])== "negatif") {
                $sen = "&fq=is_sentiment:1";
            } else if(strtolower($params["is_sentiment"])== "positif") {
                $sen = "&fq=is_sentiment:2";
            }
        }
        //-============= end fq sentiment
        //-============= start fq media_type
        $mt = null;
        if(isset($params["is_mediatype"])&& $params["is_mediatype"] != "") {
            if($params["is_mediatype"] == "nasional") {
                $mt = "&fq=is_mediatype:(" . urlencode("1 OR 2"). ")";
            } else {
                $mt = "&fq=is_mediatype:3";
            }
        }
        //-============= end fq media_type
        $fq1 = $content1 . $content2 . $content3 . $content4 . $cdd . $sen;
        $fq = $content1 . $content2 . $content3 . $content4 . $cdd . $sen . $mt;
        // die (""select?q=*%3A*&sort=content_date+desc&wt=json&indent=true&entity_id:5&fl=is_sentiment,id,ss_user_screenname,content,ss_retweeted_user_screenname&rows=1000&fq=ss_isretweet:true"".$fq1);
        $id = $z->get_id("Domas_Model_Withsolrpmas_get_by_rawurl", array($fq1));
        $tmp = $z->get_cache($cache, $id);
        $tmp = false;
        if(!$tmp) {
            
            /*
             $tmp = $ff->get_by_rawurl("select?q=*%3A*&sort=content_date+desc&wt=json&indent=true&entity_id:5&fl=is_sentiment,id,ss_user_screenname,content,ss_retweeted_user_screenname&rows=1000&fq=ss_isretweet:true" .
             $fq1);
             *
             "/solr/pmasdev/select?q=airin&fq=entity_id%3A5&sort=timestamp+desc&wt=json&indent=true"                                       * select?q=*%3A*&fq=entity_id%3A5&sort=timestamp+desc&wt=json&indent=true
             */
            $tmp = $ff->get_by_rawurl("select?q=*%3A*&fq=entity_id%3A5&sort=timestamp+desc&wt=json&indent=true");
            $cache->save($tmp, $id, array('systembin'));
        }
        $x = $tmp["response"]["docs"];
        //Zend_Debug::dump(($x));die();
        $data = array();
        $sent = array(0 => 'netral',
                      1 => 'negatif',
                      2 => 'positif');

        foreach($x as $k => $v) {
            $data[$k]["xid"] = $v["id"];
            $data[$k]["sent"] = $sent[$v["is_sentiment"]];
            $data[$k]["source"] = $v["ss_user_screenname"];
            $data[$k]["target"] = $v["ss_retweeted_user_screenname"];
            $data[$k]["type"] = "licensing";
            $data[$k]["content"] = $v["content"];
        }
        //Zend_Debug::dump($data); die();
        $this->view->data = json_encode($data);
        $this->view->cat = $dash;
        // $this->view->markers = json_encode($markers);
        $this->view->varams = $params;
        // Zend_Debug::dump($data);die();
    }

    public function linkke1Action() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        $g = new Domas_Model_Pdash();
        $ff = new Domas_Model_Withsolrpmas();
        $str = "*:*";
        //-============= start fq content
        $skey = null;
        $arr_pencarian = array();
        $arr_searchkey = array();
        $arr_skeyexception = array();
        $pencarian = "";
        if($params["skey"] != "") {
            $arr_pencarian = explode(",", $params["skey"]);
        }
        if($params["searchkey"] != "") {
            $arr_searchkey = explode(",", $params["searchkey"]);
        }
        if($params["skeyexception"] != "") {
            $arr_skeyexception = explode(",", $params["skeyexception"]);
        }
        $z = new Model_Cache();
        $cache = $z->cachefunc(4000);
        $id = $z->get_id("Domas_Model_Pdash_get_topik_by_tema", array($params["cat"], $params["topik"]));
        $tmp_tp = $z->get_cache($cache, $id);
        if(!$tmp_tp) {
            $tmp_tp = $g->get_topik_by_tema($params["cat"], $params["topik"]);
            $cache->save($tmp_tp, $id, array('systembin'));
        }
        $tp = array();

        foreach($tmp_tp as $k => $v) {
            $st = $g->get_subtema_by_tema($v["tid"]);

            foreach($st as $k1 => $v1) {
                array_push($tp, $v1);
            }
        }
        // Zend_Debug::dump($tp);die();
        $arr_topik = array();

        foreach($tp as $k => $v) {
            array_push($arr_topik, trim(strtolower($v["name"])));
        }
        if(count(array_unique($arr_topik))> 500) {
            array_splice($arr_topik, 320);
        }
        // Zend_Debug::dump($arr_topik);die();
        $content1 = null;
        $sm_what_and = null;
        if(count($arr_pencarian)> 0) {
            $content1 = '&fq=content:("' . urlencode(implode('" AND "', $arr_pencarian)). '")+OR+sm_what:("' . urlencode(implode('" OR "', $arr_pencarian)). '")+OR+label:("' . urlencode(implode('" OR "', $arr_pencarian)). '")';
        }
        $content2 = null;
        if(count($arr_topik)> 0) {
            $content2 = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_topik))). '")';
        }
        $content3 = null;
        $sm_what_or = null;
        if(count($arr_searchkey)> 0) {
            $content3 = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")+OR+sm_what:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")+OR+label:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")';
        }
        $content4 = null;
        $sm_what_or = null;
        if(count($arr_skeyexception)> 0) {
            $content3 = '&fq=-content:("' . urlencode(implode('" OR "', array_unique($arr_skeyexception))). '")+OR+-sm_what:("' . urlencode(implode('" OR "', array_unique($arr_skeyexception))). '")+OR+-label:("' .(implode('" OR "', array_unique($arr_skeyexception))). '")';
            // $sm_what_or = 'sm_what:("'.(implode('" OR "',array_unique($arr_searchkey))).'")';
        }
        //-============= end fq content
        //-============= start fq content_date range
        $d1 = date("Y-m-d", strtotime('monday last week'));
        $d2 = date("Y-m-d");
        if(isset($params["d1"])&& $params["d1"] != "" && isset($params["d2"])&& $params["d2"] != "") {
            $d1 = date("Y-m-d", strtotime($params['d1']));
            $d2 = date("Y-m-d", strtotime($params['d2']));
            $d2 = date('Y-m-d', strtotime($params['d2'] . "-1 days"));
            $d1 = $d2;
        }
        // $filter .="&content_date:[".$d1.$d2."]";
        $cdd = "&fq=content_date:[" . $d1 . "T00:00:00Z+TO+" . $d2 . "T23:59:59Z]";
        //-============= end fq content_date range
        //-============= start fq sentiment
        $sen = null;
        if(isset($params["is_sentiment"])&& $params["is_sentiment"] != "") {
            $sen = "&fq=is_sentiment:0";
            if(strtolower($params["is_sentiment"])== "negatif") {
                $sen = "&fq=is_sentiment:1";
            } else if(strtolower($params["is_sentiment"])== "positif") {
                $sen = "&fq=is_sentiment:2";
            }
        }
        //-============= end fq sentiment
        //-============= start fq media_type
        $mt = null;
        if(isset($params["is_mediatype"])&& $params["is_mediatype"] != "") {
            if($params["is_mediatype"] == "nasional") {
                $mt = "&fq=is_mediatype:(" . urlencode("1 OR 2"). ")";
            } else {
                $mt = "&fq=is_mediatype:3";
            }
        }
        //-============= end fq media_type
        $fq1 = $content1 . $content2 . $content3 . $content4 . $cdd . $sen;
        $fq = $content1 . $content2 . $content3 . $content4 . $cdd . $sen . $mt;
        // die ("select?q=*%3A*&sort=content_date+desc&wt=json&indent=true&entity_id:5&fl=is_sentiment,id,ss_user_screenname,content,ss_retweeted_user_screenname&rows=1000&fq=ss_isretweet:true".$fq1);
        $id = $z->get_id("Domas_Model_Withsolrpmas_get_by_rawurl", array($fq1));
        $tmp = $z->get_cache($cache, $id);
        if(!$tmp) {
            $tmp = $ff->get_by_rawurl("select?q=*%3A*&sort=content_date+desc&wt=json&indent=true&fq=entity_id:5&fl=is_sentiment,id,ss_user_screenname,content,ss_retweeted_user_screenname&rows=1000&fq=ss_isretweet:true" . 
                                      $fq1);
            $cache->save($tmp, $id, array('systembin'));
        }
        $x = $tmp["response"]["docs"];
        //Zend_Debug::dump(($x));die();
        $data = array();
        $sent = array(0 => 'netral',
                      1 => 'negatif',
                      2 => 'positif');

        foreach($x as $k => $v) {
            $data[$k]["xid"] = $v["id"];
            $data[$k]["sent"] = $sent[$v["is_sentiment"]];
            $data[$k]["source"] = $v["ss_user_screenname"];
            $data[$k]["target"] = $v["ss_retweeted_user_screenname"];
            $data[$k]["type"] = "licensing";
            $data[$k]["content"] = $v["content"];
        }
        //Zend_Debug::dump($data); die();
        $this->view->data = json_encode($data);
        $this->view->cat = $dash;
        // $this->view->markers = json_encode($markers);
        $this->view->varams = $params;
        // Zend_Debug::dump($data);die();
    }

    public function linkke2Action() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($params);die();
        $g = new Domas_Model_Pdash();
        $ff = new Domas_Model_Withsolrpmas();
        $str = "*:*";
        //-============= start fq content
        $skey = null;
        $arr_pencarian = array();
        $arr_searchkey = array();
        $arr_skeyexception = array();
        $pencarian = "";
        if($params["skey"] != "") {
            $arr_pencarian = explode(",", $params["skey"]);
        }
        if($params["searchkey"] != "") {
            $arr_searchkey = explode(",", $params["searchkey"]);
        }
        if($params["skeyexception"] != "") {
            $arr_skeyexception = explode(",", $params["skeyexception"]);
        }
        $z = new Model_Cache();
        $cache = $z->cachefunc(4000);
        $id = $z->get_id("Domas_Model_Pdash_get_topik_by_tema", array($params["cat"], $params["topik"]));
        $tmp_tp = $z->get_cache($cache, $id);
        if(!$tmp_tp) {
            $tmp_tp = $g->get_topik_by_tema($params["cat"], $params["topik"]);
            $cache->save($tmp_tp, $id, array('systembin'));
        }
        $tp = array();

        foreach($tmp_tp as $k => $v) {
            $st = $g->get_subtema_by_tema($v["tid"]);

            foreach($st as $k1 => $v1) {
                array_push($tp, $v1);
            }
        }
        // Zend_Debug::dump($tp);die();
        $arr_topik = array();

        foreach($tp as $k => $v) {
            array_push($arr_topik, trim(strtolower($v["name"])));
        }
        if(count(array_unique($arr_topik))> 500) {
            array_splice($arr_topik, 320);
        }
        // Zend_Debug::dump($arr_topik);die();
        $content1 = null;
        $sm_what_and = null;
        if(count($arr_pencarian)> 0) {
            $content1 = '&fq=content:("' . urlencode(implode('" AND "', $arr_pencarian)). '")+OR+sm_what:("' . urlencode(implode('" OR "', $arr_pencarian)). '")+OR+label:("' . urlencode(implode('" OR "', $arr_pencarian)). '")';
        }
        $content2 = null;
        if(count($arr_topik)> 0) {
            $content2 = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_topik))). '")';
        }
        $content3 = null;
        $sm_what_or = null;
        if(count($arr_searchkey)> 0) {
            $content3 = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")+OR+sm_what:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")+OR+label:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")';
        }
        $content4 = null;
        $sm_what_or = null;
        if(count($arr_skeyexception)> 0) {
            $content3 = '&fq=-content:("' . urlencode(implode('" OR "', array_unique($arr_skeyexception))). '")+OR+-sm_what:("' . urlencode(implode('" OR "', array_unique($arr_skeyexception))). '")+OR+-label:("' .(implode('" OR "', array_unique($arr_skeyexception))). '")';
            // $sm_what_or = 'sm_what:("'.(implode('" OR "',array_unique($arr_searchkey))).'")';
        }
        //-============= end fq content
        //-============= start fq content_date range
        $d1 = date("Y-m-d", strtotime('monday last week'));
        $d2 = date("Y-m-d");
        if(isset($params["d1"])&& $params["d1"] != "" && isset($params["d2"])&& $params["d2"] != "") {
            $d1 = date("Y-m-d", strtotime($params['d1']));
            $d2 = date("Y-m-d", strtotime($params['d2']));
            $d2 = date('Y-m-d', strtotime($params['d2'] . "-2 days"));
            $d1 = $d2;
        }
        // Zend_Debug::dump($d1);
        // Zend_Debug::dump($d2);
        // die();
        // $filter .="&content_date:[".$d1.$d2."]";
        $cdd = "&fq=content_date:[" . $d1 . "T00:00:00Z+TO+" . $d2 . "T23:59:59Z]";
        //-============= end fq content_date range
        //-============= start fq sentiment
        $sen = null;
        if(isset($params["is_sentiment"])&& $params["is_sentiment"] != "") {
            $sen = "&fq=is_sentiment:0";
            if(strtolower($params["is_sentiment"])== "negatif") {
                $sen = "&fq=is_sentiment:1";
            } else if(strtolower($params["is_sentiment"])== "positif") {
                $sen = "&fq=is_sentiment:2";
            }
        }
        //-============= end fq sentiment
        //-============= start fq media_type
        $mt = null;
        if(isset($params["is_mediatype"])&& $params["is_mediatype"] != "") {
            if($params["is_mediatype"] == "nasional") {
                $mt = "&fq=is_mediatype:(" . urlencode("1 OR 2"). ")";
            } else {
                $mt = "&fq=is_mediatype:3";
            }
        }
        //-============= end fq media_type
        $fq1 = $content1 . $content2 . $content3 . $content4 . $cdd . $sen;
        $fq = $content1 . $content2 . $content3 . $content4 . $cdd . $sen . $mt;
        //die ("select?q=*%3A*&sort=content_date+desc&wt=json&indent=true&entity_id:5&fl=is_sentiment,id,ss_user_screenname,content,ss_retweeted_user_screenname&rows=1000&fq=ss_isretweet:true".$fq1);
        $id = $z->get_id("Domas_Model_Withsolrpmas_get_by_rawurl", array($fq1));
        $tmp = $z->get_cache($cache, $id);
        if(!$tmp) {
            $tmp = $ff->get_by_rawurl("select?q=*%3A*&sort=content_date+desc&wt=json&indent=true&entity_id:5&fl=is_sentiment,id,ss_user_screenname,content,ss_retweeted_user_screenname&rows=1000&fq=ss_isretweet:true" . 
                                      $fq1);
            $cache->save($tmp, $id, array('systembin'));
        }
        $x = $tmp["response"]["docs"];
        //Zend_Debug::dump(($x));die();
        $data = array();
        $sent = array(0 => 'netral',
                      1 => 'negatif',
                      2 => 'positif');

        foreach($x as $k => $v) {
            $data[$k]["xid"] = $v["id"];
            $data[$k]["sent"] = $sent[$v["is_sentiment"]];
            $data[$k]["source"] = $v["ss_user_screenname"];
            $data[$k]["target"] = $v["ss_retweeted_user_screenname"];
            $data[$k]["type"] = "licensing";
            $data[$k]["content"] = $v["content"];
        }
        //Zend_Debug::dump($data); die();
        $this->view->data = json_encode($data);
        $this->view->cat = $dash;
        // $this->view->markers = json_encode($markers);
        $this->view->varams = $params;
        // Zend_Debug::dump($data);die();
    }

    public function linkke3Action() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        $g = new Domas_Model_Pdash();
        $ff = new Domas_Model_Withsolrpmas();
        $str = "*:*";
        //-============= start fq content
        $skey = null;
        $arr_pencarian = array();
        $arr_searchkey = array();
        $arr_skeyexception = array();
        $pencarian = "";
        if($params["skey"] != "") {
            $arr_pencarian = explode(",", $params["skey"]);
        }
        if($params["searchkey"] != "") {
            $arr_searchkey = explode(",", $params["searchkey"]);
        }
        if($params["skeyexception"] != "") {
            $arr_skeyexception = explode(",", $params["skeyexception"]);
        }
        $z = new Model_Cache();
        $cache = $z->cachefunc(4000);
        $id = $z->get_id("Domas_Model_Pdash_get_topik_by_tema", array($params["cat"], $params["topik"]));
        $tmp_tp = $z->get_cache($cache, $id);
        if(!$tmp_tp) {
            $tmp_tp = $g->get_topik_by_tema($params["cat"], $params["topik"]);
            $cache->save($tmp_tp, $id, array('systembin'));
        }
        $tp = array();

        foreach($tmp_tp as $k => $v) {
            $st = $g->get_subtema_by_tema($v["tid"]);

            foreach($st as $k1 => $v1) {
                array_push($tp, $v1);
            }
        }
        // Zend_Debug::dump($tp);die();
        $arr_topik = array();

        foreach($tp as $k => $v) {
            array_push($arr_topik, trim(strtolower($v["name"])));
        }
        if(count(array_unique($arr_topik))> 500) {
            array_splice($arr_topik, 320);
        }
        // Zend_Debug::dump($arr_topik);die();
        $content1 = null;
        $sm_what_and = null;
        if(count($arr_pencarian)> 0) {
            $content1 = '&fq=content:("' . urlencode(implode('" AND "', $arr_pencarian)). '")+OR+sm_what:("' . urlencode(implode('" OR "', $arr_pencarian)). '")+OR+label:("' . urlencode(implode('" OR "', $arr_pencarian)). '")';
        }
        $content2 = null;
        if(count($arr_topik)> 0) {
            $content2 = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_topik))). '")';
        }
        $content3 = null;
        $sm_what_or = null;
        if(count($arr_searchkey)> 0) {
            $content3 = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")+OR+sm_what:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")+OR+label:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")';
        }
        $content4 = null;
        $sm_what_or = null;
        if(count($arr_skeyexception)> 0) {
            $content3 = '&fq=-content:("' . urlencode(implode('" OR "', array_unique($arr_skeyexception))). '")+OR+-sm_what:("' . urlencode(implode('" OR "', array_unique($arr_skeyexception))). '")+OR+-label:("' .(implode('" OR "', array_unique($arr_skeyexception))). '")';
            // $sm_what_or = 'sm_what:("'.(implode('" OR "',array_unique($arr_searchkey))).'")';
        }
        //-============= end fq content
        //-============= start fq content_date range
        $d1 = date("Y-m-d", strtotime('monday last week'));
        $d2 = date("Y-m-d");
        if(isset($params["d1"])&& $params["d1"] != "" && isset($params["d2"])&& $params["d2"] != "") {
            $d1 = date("Y-m-d", strtotime($params['d1']));
            $d2 = date("Y-m-d", strtotime($params['d2']));
            $d2 = date('Y-m-d', strtotime($params['d2'] . "-3 days"));
            $d1 = $d2;
        }
        // $filter .="&content_date:[".$d1.$d2."]";
        $cdd = "&fq=content_date:[" . $d1 . "T00:00:00Z+TO+" . $d2 . "T23:59:59Z]";
        //-============= end fq content_date range
        //-============= start fq sentiment
        $sen = null;
        if(isset($params["is_sentiment"])&& $params["is_sentiment"] != "") {
            $sen = "&fq=is_sentiment:0";
            if(strtolower($params["is_sentiment"])== "negatif") {
                $sen = "&fq=is_sentiment:1";
            } else if(strtolower($params["is_sentiment"])== "positif") {
                $sen = "&fq=is_sentiment:2";
            }
        }
        //-============= end fq sentiment
        //-============= start fq media_type
        $mt = null;
        if(isset($params["is_mediatype"])&& $params["is_mediatype"] != "") {
            if($params["is_mediatype"] == "nasional") {
                $mt = "&fq=is_mediatype:(" . urlencode("1 OR 2"). ")";
            } else {
                $mt = "&fq=is_mediatype:3";
            }
        }
        //-============= end fq media_type
        $fq1 = $content1 . $content2 . $content3 . $content4 . $cdd . $sen;
        $fq = $content1 . $content2 . $content3 . $content4 . $cdd . $sen . $mt;
        //die ("select?q=*%3A*&sort=content_date+desc&wt=json&indent=true&entity_id:5&fl=is_sentiment,id,ss_user_screenname,content,ss_retweeted_user_screenname&rows=1000&fq=ss_isretweet:true".$fq1);
        $id = $z->get_id("Domas_Model_Withsolrpmas_get_by_rawurl", array($fq1));
        $tmp = $z->get_cache($cache, $id);
        if(!$tmp) {
            $tmp = $ff->get_by_rawurl("select?q=*%3A*&sort=content_date+desc&wt=json&indent=true&entity_id:5&fl=is_sentiment,id,ss_user_screenname,content,ss_retweeted_user_screenname&rows=1000&fq=ss_isretweet:true" . 
                                      $fq1);
            $cache->save($tmp, $id, array('systembin'));
        }
        $x = $tmp["response"]["docs"];
        //Zend_Debug::dump(($x));die();
        $data = array();
        $sent = array(0 => 'netral',
                      1 => 'negatif',
                      2 => 'positif');

        foreach($x as $k => $v) {
            $data[$k]["xid"] = $v["id"];
            $data[$k]["sent"] = $sent[$v["is_sentiment"]];
            $data[$k]["source"] = $v["ss_user_screenname"];
            $data[$k]["target"] = $v["ss_retweeted_user_screenname"];
            $data[$k]["type"] = "licensing";
            $data[$k]["content"] = $v["content"];
        }
        //Zend_Debug::dump($data); die();
        $this->view->data = json_encode($data);
        $this->view->cat = $dash;
        // $this->view->markers = json_encode($markers);
        $this->view->varams = $params;
        // Zend_Debug::dump($data);die();
    }

    public function linkke4Action() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        $g = new Domas_Model_Pdash();
        $ff = new Domas_Model_Withsolrpmas();
        $str = "*:*";
        //-============= start fq content
        $skey = null;
        $arr_pencarian = array();
        $arr_searchkey = array();
        $arr_skeyexception = array();
        $pencarian = "";
        if($params["skey"] != "") {
            $arr_pencarian = explode(",", $params["skey"]);
        }
        if($params["searchkey"] != "") {
            $arr_searchkey = explode(",", $params["searchkey"]);
        }
        if($params["skeyexception"] != "") {
            $arr_skeyexception = explode(",", $params["skeyexception"]);
        }
        $z = new Model_Cache();
        $cache = $z->cachefunc(4000);
        $id = $z->get_id("Domas_Model_Pdash_get_topik_by_tema", array($params["cat"], $params["topik"]));
        $tmp_tp = $z->get_cache($cache, $id);
        if(!$tmp_tp) {
            $tmp_tp = $g->get_topik_by_tema($params["cat"], $params["topik"]);
            $cache->save($tmp_tp, $id, array('systembin'));
        }
        $tp = array();

        foreach($tmp_tp as $k => $v) {
            $st = $g->get_subtema_by_tema($v["tid"]);

            foreach($st as $k1 => $v1) {
                array_push($tp, $v1);
            }
        }
        // Zend_Debug::dump($tp);die();
        $arr_topik = array();

        foreach($tp as $k => $v) {
            array_push($arr_topik, trim(strtolower($v["name"])));
        }
        if(count(array_unique($arr_topik))> 500) {
            array_splice($arr_topik, 320);
        }
        // Zend_Debug::dump($arr_topik);die();
        $content1 = null;
        $sm_what_and = null;
        if(count($arr_pencarian)> 0) {
            $content1 = '&fq=content:("' . urlencode(implode('" AND "', $arr_pencarian)). '")+OR+sm_what:("' . urlencode(implode('" OR "', $arr_pencarian)). '")+OR+label:("' . urlencode(implode('" OR "', $arr_pencarian)). '")';
        }
        $content2 = null;
        if(count($arr_topik)> 0) {
            $content2 = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_topik))). '")';
        }
        $content3 = null;
        $sm_what_or = null;
        if(count($arr_searchkey)> 0) {
            $content3 = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")+OR+sm_what:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")+OR+label:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")';
        }
        $content4 = null;
        $sm_what_or = null;
        if(count($arr_skeyexception)> 0) {
            $content3 = '&fq=-content:("' . urlencode(implode('" OR "', array_unique($arr_skeyexception))). '")+OR+-sm_what:("' . urlencode(implode('" OR "', array_unique($arr_skeyexception))). '")+OR+-label:("' .(implode('" OR "', array_unique($arr_skeyexception))). '")';
            // $sm_what_or = 'sm_what:("'.(implode('" OR "',array_unique($arr_searchkey))).'")';
        }
        //-============= end fq content
        //-============= start fq content_date range
        $d1 = date("Y-m-d", strtotime('monday last week'));
        $d2 = date("Y-m-d");
        if(isset($params["d1"])&& $params["d1"] != "" && isset($params["d2"])&& $params["d2"] != "") {
            $d1 = date("Y-m-d", strtotime($params['d1']));
            $d2 = date("Y-m-d", strtotime($params['d2']));
            $d2 = date('Y-m-d', strtotime($params['d2'] . "-4 days"));
            $d1 = $d2;
        }
        // Zend_Debug::dump($d1);
        // Zend_Debug::dump($d2);
        // die();
        // $filter .="&content_date:[".$d1.$d2."]";
        $cdd = "&fq=content_date:[" . $d1 . "T00:00:00Z+TO+" . $d2 . "T23:59:59Z]";
        //-============= end fq content_date range
        //-============= start fq sentiment
        $sen = null;
        if(isset($params["is_sentiment"])&& $params["is_sentiment"] != "") {
            $sen = "&fq=is_sentiment:0";
            if(strtolower($params["is_sentiment"])== "negatif") {
                $sen = "&fq=is_sentiment:1";
            } else if(strtolower($params["is_sentiment"])== "positif") {
                $sen = "&fq=is_sentiment:2";
            }
        }
        //-============= end fq sentiment
        //-============= start fq media_type
        $mt = null;
        if(isset($params["is_mediatype"])&& $params["is_mediatype"] != "") {
            if($params["is_mediatype"] == "nasional") {
                $mt = "&fq=is_mediatype:(" . urlencode("1 OR 2"). ")";
            } else {
                $mt = "&fq=is_mediatype:3";
            }
        }
        //-============= end fq media_type
        $fq1 = $content1 . $content2 . $content3 . $content4 . $cdd . $sen;
        $fq = $content1 . $content2 . $content3 . $content4 . $cdd . $sen . $mt;
        //die ("select?q=*%3A*&sort=content_date+desc&wt=json&indent=true&entity_id:5&fl=is_sentiment,id,ss_user_screenname,content,ss_retweeted_user_screenname&rows=1000&fq=ss_isretweet:true".$fq1);
        $id = $z->get_id("Domas_Model_Withsolrpmas_get_by_rawurl", array($fq1));
        $tmp = $z->get_cache($cache, $id);
        if(!$tmp) {
            $tmp = $ff->get_by_rawurl("select?q=*%3A*&sort=content_date+desc&wt=json&indent=true&entity_id:5&fl=is_sentiment,id,ss_user_screenname,content,ss_retweeted_user_screenname&rows=1000&fq=ss_isretweet:true" . 
                                      $fq1);
            $cache->save($tmp, $id, array('systembin'));
        }
        $x = $tmp["response"]["docs"];
        //Zend_Debug::dump(($x));die();
        $data = array();
        $sent = array(0 => 'netral',
                      1 => 'negatif',
                      2 => 'positif');

        foreach($x as $k => $v) {
            $data[$k]["xid"] = $v["id"];
            $data[$k]["sent"] = $sent[$v["is_sentiment"]];
            $data[$k]["source"] = $v["ss_user_screenname"];
            $data[$k]["target"] = $v["ss_retweeted_user_screenname"];
            $data[$k]["type"] = "licensing";
            $data[$k]["content"] = $v["content"];
        }
        //Zend_Debug::dump($data); die();
        $this->view->data = json_encode($data);
        $this->view->cat = $dash;
        // $this->view->markers = json_encode($markers);
        $this->view->varams = $params;
        // Zend_Debug::dump($data);die();
    }

    public function latestallAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        $params["start"] = 0;
        // $params["next"] = (int) $params['page'] + 1;
        $arr = array("doc" => 3,
                     "news" => 2,
                     "metadata" => 1,
                     "socmed" => 5,
                    );

        foreach($params['fk'] as $vz) {
            $nmed[] = $arr[$vz];
        }
        // Zend_Debug::dump($nmed);die();            
        $c3 = new Domas_Model_Manage();
        $c3->logs_keyws(strtolower($params['skey']));
        
        $fil = implode(" OR ", $nmed);
        $params["x"] = $fil;
       // Zend_Debug::dump($params);die();
        $s = new Domas_Model_Solranalis();
        $g = new Domas_Model_Pdash();
        $z = new Model_Cache();
        $cache = $z->cachefunc(4000);
        $id = $z->get_id("Domas_Model_Solranalis_get_latest", $params);
        $data = $z->get_cache($cache, $id);
        // Zend_Debug::dump($data);die();
        $data = false;
        if(!$data) {
            $data = $s->get_latest_all($params);
            $cache->save($data, $id, array('systembin'));
        }
        // Zend_Debug::dump($data);die();
        $this->view->params = $params;
        $this->view->data = $data;
        $this->view->logo = $g->get_logo();
    }

    public function latestAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        $params["start"] = 0;
        // $params["next"] = (int) $params['page'] + 1;
        $arr = array("lap" => 3,
                     "news" => 2,
                     "socmed" => 5,
                    );
        $x = $arr[$params["l"]];
        $params["x"] = $x;
        // $parameter = array(
        // "l"=>$params["l"],
        // "skey"=>$params["skey"],
        // "skeyexception"=>$params["skeyexception"],
        // "searchkey"=>$params["searchkey"],
        // "cat"=>$params["cat"],
        // "topik"=>$params["topik"],
        // "is_mediatype"=>$params["is_mediatype"],
        // "last"=>$params["last"],
        // "d1"=>$params["d1"],
        // "d2"=>$params["d2"],
        // "is_sentiment"=>$params["is_sentiment"],
        // "tf"=>$params["tf"],
        // "start"=>$params["start"],
        // );
        // Zend_Debug::dump($params['x']);die();
        $s = new Domas_Model_Solranalis();
        $g = new Domas_Model_Pdash();
        $z = new Model_Cache();
        $cache = $z->cachefunc(4000);
        $id = $z->get_id("Domas_Model_Solranalis_get_latest", $params);
        $data = $z->get_cache($cache, $id);
        // Zend_Debug::dump($data);die();
        $data = false;
        if(!$data) {
            $data = $s->get_latest($params);
            $cache->save($data, $id, array('systembin'));
        }
        // Zend_Debug::dump($data);die();
        $this->view->params = $params;
        $this->view->data = $data;
        $this->view->logo = $g->get_logo();
    }

    public function getallAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        $params["start"] = (int) $params['page'] * 10;
        // $params["next"] = (int)  + 1;
        $arr = array("lap" => 6,
                     "news" => 2,
                     "socmed" => 5,
                    );
        // $params["x"] = $params["type"];
        $s = new Domas_Model_Solranalis();
        $g = new Domas_Model_Pdash();
        $z = new Model_Cache();
        $cache = $z->cachefunc(4000);
        $id = $z->get_id("Domas_Model_Solranalis_get_latest_all", $params);
        $data = $z->get_cache($cache, $id);
        if(!$data) {
            $data = $s->get_latest_all($params);
            $cache->save($data, $id, array('systembin'));
        }
        // Zend_Debug::dump($data);die();
        $logo = $g->get_logo();
        $cc = new CMS_General();
        $i = 1;

        foreach($data as $v) {
            if($v['entity_id'] == 2) {
                $timestamp = strtotime($v["content_date"]);
                $date_format = preg_replace("/S/", "", $date_format);
                $pattern = array('/Mon[^day]/',
                                 '/Tue[^sday]/',
                                 '/Wed[^nesday]/',
                                 '/Thu[^rsday]/',
                                 '/Fri[^day]/',
                                 '/Sat[^urday]/',
                                 '/Sun[^day]/',
                                 '/Monday/',
                                 '/Tuesday/',
                                 '/Wednesday/',
                                 '/Thursday/',
                                 '/Friday/',
                                 '/Saturday/',
                                 '/Sunday/',
                                 '/Jan[^uary]/',
                                 '/Feb[^ruary]/',
                                 '/Mar[^ch]/',
                                 '/Apr[^il]/',
                                 '/May/',
                                 '/Jun[^e]/',
                                 '/Jul[^y]/',
                                 '/Aug[^ust]/',
                                 '/Sep[^tember]/',
                                 '/Oct[^ober]/',
                                 '/Nov[^ember]/',
                                 '/Dec[^ember]/',
                                 '/January/',
                                 '/February/',
                                 '/March/',
                                 '/April/',
                                 '/June/',
                                 '/July/',
                                 '/August/',
                                 '/September/',
                                 '/October/',
                                 '/November/',
                                 '/December/',
                                );
                $replace = array('Sen',
                                 'Sel',
                                 'Rab',
                                 'Kam',
                                 'Jum',
                                 'Sab',
                                 'Min',
                                 'Senin',
                                 'Selasa',
                                 'Rabu',
                                 'Kamis',
                                 'Jumat',
                                 'Sabtu',
                                 'Minggu',
                                 'Jan',
                                 'Feb',
                                 'Mar',
                                 'Apr',
                                 'Mei',
                                 'Jun',
                                 'Jul',
                                 'Ags',
                                 'Sep',
                                 'Okt',
                                 'Nov',
                                 'Des',
                                 'Januari',
                                 'Februari',
                                 'Maret',
                                 'April',
                                 'Juni',
                                 'Juli',
                                 'Agustus',
                                 'Sepember',
                                 'Oktober',
                                 'November',
                                 'Desember',
                                );
                $content_date = date("l, j F Y | H:i", $timestamp);
                $content_date = preg_replace($pattern, $replace, $content_date);
                if($v['content'] != "") {
                    $cl = "in";
                    if($i % 2 == 0) {
                        $cl = "out";
                    }
                    echo '<li class="' . $cl . '">';
                    // if($v['ss_icon'] != "") {
                    // // echo '<img src="' . $cc->get_string_between($v['ss_icon'], "<img src=\"", "\" align"). '"  style="width: 100%; height: 200px; display: block;" >';
                    // echo '<img  class="avatar" style="width:45px;" src="'.$v['ss_icon'].'" />';
                    // } else {
                    echo '<img  class="avatar" style="width:45px;" src="' . $logo[$v['bundle']] . '" />';
                    // }
                    echo '<div class="message" >
                                                        <span class="arrow"> </span>
                                                        <a href="' . $v['id'] . '" class="name" target="_blank">' . $v['label'] . '</a>
                                                        <span class="body" >';
                    echo $content_date;
                    echo '</span><span class="body" >' . $cc->chop_string($v['content'], 200). '</span><a href="#basic" class="btn btn-sm btn-circle red easy-pie-chart-reload"  data-toggle="modal" href="#basic" data-id="' . $v["id"] . '" data-entity="' . $this->params["l"] . '" onclick="getExtract(this);">
							<i class="fa fa-search"></i> Lihat </a>
                                                    </div>
                                                </li>';
                }
            } elseif($v['entity_id'] == 5) {
                $timestamp = strtotime($v["content_date"]);
                $date_format = preg_replace("/S/", "", $date_format);
                $pattern = array('/Mon[^day]/',
                                 '/Tue[^sday]/',
                                 '/Wed[^nesday]/',
                                 '/Thu[^rsday]/',
                                 '/Fri[^day]/',
                                 '/Sat[^urday]/',
                                 '/Sun[^day]/',
                                 '/Monday/',
                                 '/Tuesday/',
                                 '/Wednesday/',
                                 '/Thursday/',
                                 '/Friday/',
                                 '/Saturday/',
                                 '/Sunday/',
                                 '/Jan[^uary]/',
                                 '/Feb[^ruary]/',
                                 '/Mar[^ch]/',
                                 '/Apr[^il]/',
                                 '/May/',
                                 '/Jun[^e]/',
                                 '/Jul[^y]/',
                                 '/Aug[^ust]/',
                                 '/Sep[^tember]/',
                                 '/Oct[^ober]/',
                                 '/Nov[^ember]/',
                                 '/Dec[^ember]/',
                                 '/January/',
                                 '/February/',
                                 '/March/',
                                 '/April/',
                                 '/June/',
                                 '/July/',
                                 '/August/',
                                 '/September/',
                                 '/October/',
                                 '/November/',
                                 '/December/',
                                );
                $replace = array('Sen',
                                 'Sel',
                                 'Rab',
                                 'Kam',
                                 'Jum',
                                 'Sab',
                                 'Min',
                                 'Senin',
                                 'Selasa',
                                 'Rabu',
                                 'Kamis',
                                 'Jumat',
                                 'Sabtu',
                                 'Minggu',
                                 'Jan',
                                 'Feb',
                                 'Mar',
                                 'Apr',
                                 'Mei',
                                 'Jun',
                                 'Jul',
                                 'Ags',
                                 'Sep',
                                 'Okt',
                                 'Nov',
                                 'Des',
                                 'Januari',
                                 'Februari',
                                 'Maret',
                                 'April',
                                 'Juni',
                                 'Juli',
                                 'Agustus',
                                 'Sepember',
                                 'Oktober',
                                 'November',
                                 'Desember',
                                );
                $content_date = date("l, j F Y | H:i", $timestamp);
                $content_date = preg_replace($pattern, $replace, $content_date);
                if($v['content'] != "") {
                    $cl = "in";
                    if($i % 2 == 0) {
                        $cl = "out";
                    }
                    echo '<li class="' . $cl . '">';
                    echo '<img class="avatar " src="' . $v['ss_user_profile_imageurl'] . '">';
                    echo '<div class="message" style="background-color:#313131;">
                                                        <span class="arrow"> </span>
                                                        <a href="http://twitter.com/' . $v['ss_user_screenname'] . '/status/' . $v["id"] . '" class="name" target="_blank">' . $v['label'] . ' @' . $v["ss_user_screenname"] . '</a>
                                                        <span class="body" >';
                    echo $content_date;
                    echo '</span><span class="body" >' . $v['content'] . '</span>
                                                    </div>
                                                </li>';
                }
              
            }
			  $i ++;
        }
        if(count($data)== 10) {
            echo '<li class="load">
                                                <span class="ajaxload" style="display:none">
                                            <img src="/assets/core/img/ajax-loading.gif"></span>
                                           <button class="btn btn-circle blue" ><span>Lebih Lanjut</span></button>
                                               </li>';
            $next = (int) $params['page'] + 1;
            echo '<script>
jQuery(document).ready(function() {

            $(".load").click(function(){
             //  $(".news .ajaxload").show();
               // $(".news .load").hide();
               
			var param = "";
	
			param +="";
			param +="skey=' . $params["skey"] . '";
		
			
			$.ajax({
			url: "/domas/ajax1/getall/page/' . $next . '?"+param,
         
            content: "html",
            beforeSend: function() {
            },
            complete: function(result) {
            },
            success: function(data) {
				//$(".news .load").hide();
                jQuery(".chats").append(data);
               // $(".news .ajaxload").hide();
                
            },
            error: function() {
            //$(".news .ajaxload").hide();
            }
            });
    
});

});	</script>';
        }
        die();
    }

    public function getAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        $params["start"] = (int) $params['page'] * 10;
        // $params["next"] = (int)  + 1;
        $arr = array("lap" => 3,
                     "news" => 2,
                     "socmed" => 5,
                    );
        // $params["x"] = $params["type"];
        $s = new Domas_Model_Solranalis();
        $g = new Domas_Model_Pdash();
        $z = new Model_Cache();
        $cache = $z->cachefunc(4000);
        $id = $z->get_id("Domas_Model_Solranalis_get_latest", $params);
        $data = $z->get_cache($cache, $id);
        if(!$data) {
            $data = $s->get_latest($params);
            $cache->save($data, $id, array('systembin'));
        }
        // Zend_Debug::dump($data);die();
        $logo = $g->get_logo();
        $cc = new CMS_General();
        $i = 1;
        switch($params['x']) {

            case 2 : foreach($data as $v) {
                $timestamp = strtotime($v["content_date"]);
                $date_format = preg_replace("/S/", "", $date_format);
                $pattern = array('/Mon[^day]/',
                                 '/Tue[^sday]/',
                                 '/Wed[^nesday]/',
                                 '/Thu[^rsday]/',
                                 '/Fri[^day]/',
                                 '/Sat[^urday]/',
                                 '/Sun[^day]/',
                                 '/Monday/',
                                 '/Tuesday/',
                                 '/Wednesday/',
                                 '/Thursday/',
                                 '/Friday/',
                                 '/Saturday/',
                                 '/Sunday/',
                                 '/Jan[^uary]/',
                                 '/Feb[^ruary]/',
                                 '/Mar[^ch]/',
                                 '/Apr[^il]/',
                                 '/May/',
                                 '/Jun[^e]/',
                                 '/Jul[^y]/',
                                 '/Aug[^ust]/',
                                 '/Sep[^tember]/',
                                 '/Oct[^ober]/',
                                 '/Nov[^ember]/',
                                 '/Dec[^ember]/',
                                 '/January/',
                                 '/February/',
                                 '/March/',
                                 '/April/',
                                 '/June/',
                                 '/July/',
                                 '/August/',
                                 '/September/',
                                 '/October/',
                                 '/November/',
                                 '/December/',
                                );
                $replace = array('Sen',
                                 'Sel',
                                 'Rab',
                                 'Kam',
                                 'Jum',
                                 'Sab',
                                 'Min',
                                 'Senin',
                                 'Selasa',
                                 'Rabu',
                                 'Kamis',
                                 'Jumat',
                                 'Sabtu',
                                 'Minggu',
                                 'Jan',
                                 'Feb',
                                 'Mar',
                                 'Apr',
                                 'Mei',
                                 'Jun',
                                 'Jul',
                                 'Ags',
                                 'Sep',
                                 'Okt',
                                 'Nov',
                                 'Des',
                                 'Januari',
                                 'Februari',
                                 'Maret',
                                 'April',
                                 'Juni',
                                 'Juli',
                                 'Agustus',
                                 'Sepember',
                                 'Oktober',
                                 'November',
                                 'Desember',
                                );
                $content_date = date("l, j F Y | H:i", $timestamp);
                $content_date = preg_replace($pattern, $replace, $content_date);
                if($v['content'] != "") {
                    $cl = "in";
                    if($i % 2 == 0) {
                        $cl = "out";
                    }
                    echo '<li class="' . $cl . '">';
                    // if($v['ss_icon'] != "") {
                    // // echo '<img src="' . $cc->get_string_between($v['ss_icon'], "<img src=\"", "\" align"). '"  style="width: 100%; height: 200px; display: block;" >';
                    // echo '<img  class="avatar" style="width:45px;" src="'.$v['ss_icon'].'" />';
                    // } else {
                    echo '<img  class="avatar" style="width:45px;" src="' . $logo[$v['bundle']] . '" />';
                    // }
                    echo '<div class="message" >
                                                        <span class="arrow"> </span>
                                                        <a href="' . $v['id'] . '" class="name" target="_blank">' . $v['label'] . '</a>
                                                        <span class="body" >';
                    echo $content_date;
                    echo '</span><span class="body" >' . $cc->chop_string($v['content'], 200). '</span><a href="#basic" class="btn btn-sm btn-circle red easy-pie-chart-reload"  data-toggle="modal" href="#basic" data-id="' . $v["id"] . '" data-entity="' . $this->params["l"] . '" onclick="getExtract(this);">
							<i class="fa fa-search"></i> Lihat </a>
                                                    </div>
                                                </li>';
                }
                $i ++;
            }
            if(count($data)== 10) {
                echo '<li class="load">
                                                <span class="ajaxload" style="display:none">
                                            <img src="/assets/core/img/ajax-loading.gif"></span>
                                           <button class="btn btn-circle blue" ><span>Lebih Lanjut</span></button>
                                               </li>';
                $next = (int) $params['page'] + 1;
                echo '<script>
jQuery(document).ready(function() {

            $(".news .load").click(function(){
               $(".news .ajaxload").show();
               // $(".news .load").hide();
               
			var param = "";
	
			param +="";
			param +="l=' . $params["l"] . '";
			param +="&skey=' . $params["skey"] . '";
			param +="&skeyexception=' . $params["skeyexception"] . '";
			param +="&searchkey=' . $params["searchkey"] . '";
			param +="&cat=' . $params["cat"] . '";
			param +="&topik=' . $params["topik"] . '";
			param +="&is_mediatype=' . $params["is_mediatype"] . '";
			param +="&last=' . $params["last"] . '";
			param +="&d1=' . $params["d1"] . '&d2=' . $params["d2"] . '";
			param +="&is_sentiment=' . $params["is_sentiment"] . '";
			param +="&tf=1";
			param +="&x=' . $params["x"] . '";
			
			$.ajax({
			url: "/domas/ajax1/get/page/' . $next . '?"+param,
            //url: "/domas/ajax1/get/page/' . $next . '/type/' . $params['type'] . '/d1/' . $params["d1"] . '/d2/' . $params["d2"] . '/skey/' . urlencode($params["skey"]). '/is_sentiment/' . $params["is_sentiment"] . '/is_mediatype/' . $params["is_mediatype"] . '/last/' . $params["last"] . '/tf/' . $params["tf"] . '/",
            content: "html",
            beforeSend: function() {
            },
            complete: function(result) {
            },
            success: function(data) {
				$(".news .load").hide();
                jQuery(".news").append(data);
                $(".news .ajaxload").hide();
                
            },
            error: function() {
            $(".news .ajaxload").hide();
            }
            });
    
});

});	</script>';
            }
            die();
            break;
            case 3 : $ii = 0;

            foreach($data as $v) {
                
                $timestamp = strtotime($v["content_date"]);
                $date_format = preg_replace("/S/", "", $date_format);
                $pattern = array('/Mon[^day]/',
                                 '/Tue[^sday]/',
                                 '/Wed[^nesday]/',
                                 '/Thu[^rsday]/',
                                 '/Fri[^day]/',
                                 '/Sat[^urday]/',
                                 '/Sun[^day]/',
                                 '/Monday/',
                                 '/Tuesday/',
                                 '/Wednesday/',
                                 '/Thursday/',
                                 '/Friday/',
                                 '/Saturday/',
                                 '/Sunday/',
                                 '/Jan[^uary]/',
                                 '/Feb[^ruary]/',
                                 '/Mar[^ch]/',
                                 '/Apr[^il]/',
                                 '/May/',
                                 '/Jun[^e]/',
                                 '/Jul[^y]/',
                                 '/Aug[^ust]/',
                                 '/Sep[^tember]/',
                                 '/Oct[^ober]/',
                                 '/Nov[^ember]/',
                                 '/Dec[^ember]/',
                                 '/January/',
                                 '/February/',
                                 '/March/',
                                 '/April/',
                                 '/June/',
                                 '/July/',
                                 '/August/',
                                 '/September/',
                                 '/October/',
                                 '/November/',
                                 '/December/',
                                );
                $replace = array('Sen',
                                 'Sel',
                                 'Rab',
                                 'Kam',
                                 'Jum',
                                 'Sab',
                                 'Min',
                                 'Senin',
                                 'Selasa',
                                 'Rabu',
                                 'Kamis',
                                 'Jumat',
                                 'Sabtu',
                                 'Minggu',
                                 'Jan',
                                 'Feb',
                                 'Mar',
                                 'Apr',
                                 'Mei',
                                 'Jun',
                                 'Jul',
                                 'Ags',
                                 'Sep',
                                 'Okt',
                                 'Nov',
                                 'Des',
                                 'Januari',
                                 'Februari',
                                 'Maret',
                                 'April',
                                 'Juni',
                                 'Juli',
                                 'Agustus',
                                 'Sepember',
                                 'Oktober',
                                 'November',
                                 'Desember',
                                );
              //  $content_date = gmdate("l, j F Y | H:i", $timestamp);
                 $content_date = date("l, j F Y | H:i", $timestamp);
                $content_date = preg_replace($pattern, $replace, $content_date);
                if($v['content'] != "") {
                    $cl = "in";
                    if($i % 2 == 0) {
                        $cl = "out";
                    }
                    echo '<li class="' . $cl . '">';
                   echo '<img  class="avatar" style="width:45px;" src="/images/pdf.png" />';
                    echo '<div class="message">
                                                        <span class="arrow"> </span>
                                                        <a href="javascrpt:;" class="name">' . $v["sm_what"][0] . '</a>
                                                        <span class="body" >';
                    echo $content_date;
                    $ett = array(2 => "news",
                                 3 => "lap",
                                 5 => "socmed");
                    $c2 = new CMS_LIB_parse();
                    $outtext = $c2->remove($v['content'], "<!--", "</style>");
                    // Zend_Debug::dump($outtext);die();
                    if($outtext != "") {
                        // die("i");
                        $outtext = str_replace("\n", "<br />", $outtext);
                    } else {
                        // die("e");
                        $outtext = str_replace("\n", "<br />", $v['content']);
                    }
                    echo '</span><span class="body">' . $cc->chop_string($outtext, 200). '</span>
					
	<a href="#basic" class="btn btn-sm btn-circle red easy-pie-chart-reload"  data-toggle="modal" href="#basic" data-id="' . $v["id"] . '" data-entity="lap" onclick="getExtract(this);">
		<i class="fa fa-search"></i> Lihat </a>
                                                    </div>
                                                </li>';
                    $ii ++;
                }
                $i ++;
            }
            if(count($data)== 10) {
                echo '<li class="load">
                                                <span class="ajaxload" style="display:none">
                                            <img src="/assets/core/img/ajax-loading.gif"></span>
                                           <button class="btn btn-circle blue" ><span>Lebih Lanjut</span></button>
                                               </li>';
                $next = (int) $params['page'] + 1;
                echo '<script>
jQuery(document).ready(function() {

            $(".lap .load").click(function(){
               $(".lap .ajaxload").show();
               // $(".lap .load").hide();
               
            var param = "";
	
			param +="";
			param +="l=' . $params["l"] . '";
			param +="&skey=' . $params["skey"] . '";
			param +="&skeyexception=' . $params["skeyexception"] . '";
			param +="&searchkey=' . $params["searchkey"] . '";
			param +="&cat=' . $params["cat"] . '";
			param +="&topik=' . $params["topik"] . '";
			param +="&is_mediatype=' . $params["is_mediatype"] . '";
			param +="&last=' . $params["last"] . '";
			param +="&d1=' . $params["d1"] . '&d2=' . $params["d2"] . '";
			param +="&is_sentiment=' . $params["is_sentiment"] . '";
			param +="&tf=1";
			param +="&x=' . $params["x"] . '";
			
			$.ajax({
			url: "/domas/ajax1/get/page/' . $next . '?"+param,
            //url: "/domas/ajax1/get/page/' . $next . '/type/' . $params['type'] . '/d1/' . $params["d1"] . '/d2/' . $params["d2"] . '/skey/' . urlencode($params["skey"]). '/is_sentiment/' . $params["is_sentiment"] . '/is_mediatype/' . $params["is_mediatype"] . '/last/' . $params["last"] . '/tf/' . $params["tf"] . '/",
            content: "html",
            beforeSend: function() {
            },
            complete: function(result) {
            },
            success: function(data) {
				$(".lap .load").hide();
                jQuery(".lap").append(data);
                $(".lap .ajaxload").hide();
                
            },
            error: function() {
            $(".lap .ajaxload").hide();
            }
            });
    
});

});	</script>';
            }
            die();
            break;

            case 5 : foreach($data as $v) {
                $timestamp = strtotime($v["content_date"]);
                $date_format = preg_replace("/S/", "", $date_format);
                $pattern = array('/Mon[^day]/',
                                 '/Tue[^sday]/',
                                 '/Wed[^nesday]/',
                                 '/Thu[^rsday]/',
                                 '/Fri[^day]/',
                                 '/Sat[^urday]/',
                                 '/Sun[^day]/',
                                 '/Monday/',
                                 '/Tuesday/',
                                 '/Wednesday/',
                                 '/Thursday/',
                                 '/Friday/',
                                 '/Saturday/',
                                 '/Sunday/',
                                 '/Jan[^uary]/',
                                 '/Feb[^ruary]/',
                                 '/Mar[^ch]/',
                                 '/Apr[^il]/',
                                 '/May/',
                                 '/Jun[^e]/',
                                 '/Jul[^y]/',
                                 '/Aug[^ust]/',
                                 '/Sep[^tember]/',
                                 '/Oct[^ober]/',
                                 '/Nov[^ember]/',
                                 '/Dec[^ember]/',
                                 '/January/',
                                 '/February/',
                                 '/March/',
                                 '/April/',
                                 '/June/',
                                 '/July/',
                                 '/August/',
                                 '/September/',
                                 '/October/',
                                 '/November/',
                                 '/December/',
                                );
                $replace = array('Sen',
                                 'Sel',
                                 'Rab',
                                 'Kam',
                                 'Jum',
                                 'Sab',
                                 'Min',
                                 'Senin',
                                 'Selasa',
                                 'Rabu',
                                 'Kamis',
                                 'Jumat',
                                 'Sabtu',
                                 'Minggu',
                                 'Jan',
                                 'Feb',
                                 'Mar',
                                 'Apr',
                                 'Mei',
                                 'Jun',
                                 'Jul',
                                 'Ags',
                                 'Sep',
                                 'Okt',
                                 'Nov',
                                 'Des',
                                 'Januari',
                                 'Februari',
                                 'Maret',
                                 'April',
                                 'Juni',
                                 'Juli',
                                 'Agustus',
                                 'Sepember',
                                 'Oktober',
                                 'November',
                                 'Desember',
                                );
                $content_date = date("l, j F Y | H:i", $timestamp);
                $content_date = preg_replace($pattern, $replace, $content_date);
                if($v['content'] != "") {
                    $cl = "in";
                    if($i % 2 == 0) {
                        $cl = "out";
                    }
                    echo '<li class="' . $cl . '">';
                    
                    if($v['ss_user_profile_imageurl']!=""){
		echo '<img class="avatar" style="width:45px;" src="'.$v['ss_user_profile_imageurl'].'">';		
	} else {
		echo '<img class="avatar" style="width:45px;" src="/assets/pmas/images/media/twitter.png">';
	}
                    echo '<div class="message">
                                                        <span class="arrow"> </span>
                                                        <a href="http://twitter.com/' . $v['ss_user_screenname'] . '/status/' . $v["id"] . '" class="name" target="_blank">' . $v['label'] . ' @' . $v["ss_user_screenname"] . '</a>
                                                        <span class="body">';
                    echo $content_date;
                    echo '</span><span class="body" >' . $v['content'] . '</span>
                                                    </div>
                                                </li>';
                }
                $i ++;
            }
            if(count($data)== 10) {
                echo '<li class="load">
                                                <span class="ajaxload" style="display:none">
                                            <img src="/assets/core/img/ajax-loading.gif"></span>
                                           <button class="btn btn-circle blue" ><span>Lebih Lanjut</span></button>
                                               </li>';
                $next = (int) $params['page'] + 1;
                echo '<script>
jQuery(document).ready(function() {

            $(".socmed .load").click(function(){
               $(".socmed .ajaxload").show();
               // $(".socmed .load").hide();
               
            var param = "";
	
			param +="";
			param +="l=' . $params["l"] . '";
			param +="&skey=' . $params["skey"] . '";
			param +="&skeyexception=' . $params["skeyexception"] . '";
			param +="&searchkey=' . $params["searchkey"] . '";
			param +="&cat=' . $params["cat"] . '";
			param +="&topik=' . $params["topik"] . '";
			param +="&is_mediatype=' . $params["is_mediatype"] . '";
			param +="&last=' . $params["last"] . '";
			param +="&d1=' . $params["d1"] . '&d2=' . $params["d2"] . '";
			param +="&is_sentiment=' . $params["is_sentiment"] . '";
			param +="&tf=1";
			param +="&x=' . $params["x"] . '";
			
			$.ajax({
			url: "/domas/ajax1/get/page/' . $next . '?"+param,
            //url: "/domas/ajax1/get/page/' . $next . '/type/' . $params['type'] . '/d1/' . $params["d1"] . '/d2/' . $params["d2"] . '/skey/' . urlencode($params["skey"]). '/is_sentiment/' . $params["is_sentiment"] . '/is_mediatype/' . $params["is_mediatype"] . '/last/' . $params["last"] . '/tf/' . $params["tf"] . '/",
            content: "html",
            beforeSend: function() {
            },
            complete: function(result) {
            },
            success: function(data) {
				$(".socmed .load").hide();
                jQuery(".socmed").append(data);
                $(".socmed .ajaxload").hide();
                
            },
            error: function() {
            $(".socmed .ajaxload").hide();
            }
            });
    
});

});	</script>';
            }
            die();
            break;
        }
    }

    public function fgcAction() {
        $string = 'Kupang, Nusa Tenggara Timur (ANTARA News) - Gubernur Nusa Tenggara Timur Frans Lebu Raya menyampaikan duka cita untuk pilot asal Soe Kabupaten Timor Tengah Selatan AKP Tonce M yang menjadi korban meninggal dari pesawat meledak milik Polri di laut di Kabupaten Lingga, Kepulauan Riau.

"Saya menyampaikan turut berduka cita atas meninggalnya salah satu pilot putra daerah NTT ini, tetapi menurut saya kita juga perlu berbangga bahwa ada orang NTT yang menjadi pilot di Polri," katanya kepada wartawan di Kupang, Senin.

Ia mengatakan, pemerintah NTT memberikan penghargaan sebesar-besarnya kepada Tonce yang telah mengabdi kepada Polri dengan membawa nama NTT.

"Saya berharap nantinya hal ini akan semakin memotivasi lebih banyak anak-anam muda NTT untuk berani mencoba dan bersekolah mengambil jurusan kepilotan dan memiliki jalan yang lebih mulus," ujar Frans Lebu.

Ia juga menyampaikan duka citanya untuk seluruh awak dan penumpang pesawat Sky Truck itu yang diduga hilang dalam musibah ini.

Sedangkan Kapolda NTT Brigjen Pol E. Widyo Sunaryo mengungkapkan, setelah kejadian itu Polda NTT langsung mendampingi ayah kandung Tonce M.

"Saat ini orang tuanya tinggal bapaknya, jadi pascakejadian itu kami langsung memberikan pendampingan kepada orang tuanya yang berada di Soe," tambahnya.

Terkait lokasi pemakaman jenazah Tonce, da mengatakan belum mengetahui permintaan dari keluarga, namun jika memang di NTT maka Polda NTT akan mengawal dan mempersiapakan tempat pemakamannya.

Pesawat milik Kepolisian RI dengan tipe M28 Sky Truck ini dinyatakan hilang kontak di perairan Kabupaten Lingga.

Kabid Humas Polda Kepri AKBP S Erlangga mengatakan jumlah awak dan penumpang dalam pesawat yang jatuh dalam perjalanan dari Pangkal Pinang menuju Batam itu adalah 15 orang.

Awak yang ada dalam pesawat adalah AKP Budi Waluyo (pilot), AKP Eka Barokah (pilot), AKP Tonce (pilot), Brig Joko Sujarwo (mekanik), Brig Mustofa (mekanik), AKP Abdul Munir, AKP Safran, Bripka Erwin, Briptu Andri, Bripda Rizal, Bripda Eri, Brig Suwarno, Brig Joko Sungatno, Bripda Angga, Brig Samoko, semuanya penumpang.
Editor: Jafar M Sidik';
        // $replacements = array('Jafar M Sidik', 'Jane');
        // foreach($replacements as $k=>$v){
        // echo preg_replace_callback('/[A-Z][^\\.;]*('.$v.')[^\\.;]*/', function($matches) use (&$replacements) {
        // return array_shift('<a href="#">'.$replacements.'</a>');
        // // return array_shift($replacements);
        // }, $string);
        // }
        $regex = array('Jafar M Sidik',
                       'Frans Lebu',
                       'Frans Lebu Raya');

        foreach($regex as $k => $v) {
            $string = str_ireplace($v, '<a href="#' . 
                                   $v . 
                                   '">' . 
                                   $v . 
                                   '</a>', $string);
        }
        echo($string);
        $as = new Domas_Model_Solranalis();
        //antaranews
        $yourentirehtml = file_get_contents("http://www.antaranews.com/");
        $extract = $as->get_top_news_antara('class', 'terpopuler', $yourentirehtml);
        echo $extract;
        //die();
        echo "<hr>";
        //detik
        $yourentirehtml = file_get_contents("https://news.detik.com/");
        $extract = $as->get_top_news_antara('id', 'slide_bu', $yourentirehtml);
        echo $extract;
        //die();
        echo "<hr>";
        //antaranews
        $yourentirehtml = file_get_contents("http://www.kompas.com/");
        $extract = $as->get_top_news_antara('class', 'slides', $yourentirehtml);
        echo $extract;
        //die();
        echo "<hr>";
        $regex = 'Jafar M Sidik';
        $text = $as->highlight_regex_as_html($regex, $text1);
        echo $text;
        die();
    }

    public function trentop10Action() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        $data = array();
        if($params["l"] == "news") {
            $cc = new Analis_Model_Top();
            $as = new Domas_Model_Solranalis();
            $paramsx = array();
            $paramsx["l"] = "news";
            // Zend_Debug::dump($params);die();
            $z = new Model_Cache();
            $cache = $z->cachefunc(4000);
            $id = $z->get_id("Analis_Model_Top_get_top", $paramsx);
            $data = $z->get_cache($cache, $id);
            // Zend_Debug::dump($data);die();
            // $data=false;
            if(!$data) {
                $data = $cc->get_top($paramsx);
                $cache->save($data, $id, array('systembin'));
            }
            $tmp = array();

            foreach($data as $k0 => $v0) {

                foreach($v0["content"] as $k1 => $v1) {
                    $tmp[$k0][$k1]["content"] = $v1;
                    $tmp[$k0][$k1]["url"] =($k0 == "antaranews populer" ? "http://antaranews.com" : ""). $v0["url"][$k1];
                    $tmp[$k0][$k1]["count"] = 0;
                }
            }
            $id2 = $z->get_id("Analis_Model_Top_get_toptren_news", $params);
            $data2 = $z->get_cache($cache, $id2);
            // $data2 = false;
            if(!$data2) {
                $data2 = $as->get_toptren_news($params);
                $cache->save($data2, $id2, array('systembin'));
            }
            $tmp["TOP MEDIA DIGITAL"] = $data2;
            // Zend_Debug::dump($data2);die("s");
            $data = $tmp;
            // Zend_Debug::dump($data);die();
        } else if($params["l"] == "socmed") {
            // Zend_Debug::dump($params);die();
            $as = new Domas_Model_Solranalis();
            $z = new Model_Cache();
            $cache = $z->cachefunc(4000);
            $id = $z->get_id("Domas_Model_Solranalis_get_toptren_twitter", $params);
            $data = $z->get_cache($cache, $id);
            // Zend_Debug::dump($data);die();
            // $data = false;
            if(!$data) {
                $data = $as->get_toptren_twitter($params);
                $cache->save($data, $id, array('systembin'));
            }
            // Zend_Debug::dump($data);die();
        }
        // Zend_Debug::dump($data);die();
        $this->view->varams = $params;
        $this->view->data = $data;
    }

    public function trentop11Action() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        $data = array();
        if($params["l"] == "news") {
            $cc = new Analis_Model_Top();
            $as = new Domas_Model_Solranalis();
            $paramsx = array();
            $paramsx["l"] = "news";
            // Zend_Debug::dump($params);die();
            $z = new Model_Cache();
            $cache = $z->cachefunc(4000);
            $id = $z->get_id("Analis_Model_Top_get_top", $paramsx);
            $data = $z->get_cache($cache, $id);
            // Zend_Debug::dump($data);die();
            if(!$data) {
                $data = $cc->get_top($paramsx);
                $cache->save($data, $id, array('systembin'));
            }
            $tmp = array();

            foreach($data as $k0 => $v0) {

                foreach($v0["content"] as $k1 => $v1) {
                    $tmp[$k0][$k1]["content"] = $v1;
                    $tmp[$k0][$k1]["url"] =($k0 == "antaranews populer" ? "http://antaranews.com" : ""). $v0["url"][$k1];
                    $tmp[$k0][$k1]["count"] = 0;
                }
            }
            $id2 = $z->get_id("Analis_Model_Top_get_toptren_news", $params);
            $data2 = $z->get_cache($cache, $id2);
            // $data2 = false;
            if(!$data2) {
                $data2 = $as->get_toptren_news($params);
                $cache->save($data2, $id2, array('systembin'));
            }
            $tmp["TOP MEDIA DIGITAL"] = $data2;
            // Zend_Debug::dump($data2);die("s");
            $data = $tmp;
            Zend_Debug::dump($data);
            die();
        } else if($params["l"] == "socmed") {
            // Zend_Debug::dump($params);die();
            $as = new Domas_Model_Solranalis();
            $z = new Model_Cache();
            $cache = $z->cachefunc(4000);
            $id = $z->get_id("Domas_Model_Solranalis_get_toptren_twitter", $params);
            $data = $z->get_cache($cache, $id);
            // Zend_Debug::dump($data);die();
            // $data = false;
            if(!$data) {
                $data = $as->get_toptren_twitter($params);
                $cache->save($data, $id, array('systembin'));
            }
            // Zend_Debug::dump($data);die();
        }
        // Zend_Debug::dump($data);die();
        $this->view->varams = $params;
        $this->view->data = $data;
    }
}
