<?php

/**
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>,Jul 15, 2014
 *
 */
ini_set("display_errors", "on");
use Enzim\Lib\TikaWrapper\TikaWrapper;

class Domas_AjaxController extends Zend_Controller_Action {

    public function init() {
    }
     public function delcrawlAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            //  Zend_Debug::dump($_POST);
            $mdl = new Domas_Model_Admin();
            $update = $mdl->del_crawl($_POST['uid']);
            if($update) {
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$update['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }
    public function delmediaAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            //  Zend_Debug::dump($_POST);
            $mdl = new Domas_Model_Admin();
            $update = $mdl->del_media($_POST['uid']);
            if($update) {
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$update['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function delxpathAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            //  Zend_Debug::dump($_POST);
            $mdl = new Domas_Model_Admin();
            $update = $mdl->del_xpath($_POST['uid']);
            if($update) {
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$update['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function deltaxAction() {
        $cc = new Domas_Model_Admin();
        $cc->syncron();
    }

    public function addqueueAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            $mdl = new Domas_Model_Zprabaqueue();
            #Zend_Debug::dump($_POST); die();
            // Zend_Debug::dump($el); die();
            $update = $mdl->add_action($_POST);
            if($update) {
                $_POST['id'] = $update['id'];
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$update['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }
    
     public function ajaxlogscrawlAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $params = $this->getRequest()->getParams();
        //  Zend_Debug::dump($params);die();
        $this->_helper->layout->disableLayout();
        // $this->_helper->viewRenderer->setNoRender(true);
        $mdl_admin = new Model_Actlogs();
         $countgraph = $mdl_admin->count_list_logs_crawl($_GET);
        // Zend_Debug::dump($countgraph);die();
        //die("xxx");
        $listgraph = $mdl_admin->get_list_logs_crawl($_GET);
        // Zend_Debug::dump($listgraph);die();
        $iTotalRecords = intval($countgraph);
        $iDisplayLength = intval($_GET['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_GET['iDisplayStart']);
        $sEcho = intval($_GET['sEcho']);
        $records = array();
        $records["aaData"] = array();
        $end = $iDisplayStart + $iDisplayLength;

        foreach($listgraph as $k => $v) {
            if($v['log_crawl']==1) {
            $records["aaData"][] = array($iDisplayStart+$k+ 1,
                                        $v['id'],
                                        $v['index_url'],
                                        $v['sub_domain'],
                                        '<div><span class="label label-sm label-success"> success </span></div>');
                                        
            }else {
                 $records["aaData"][] = array($iDisplayStart+$k+ 1,
                                        $v['id'],
                                        $v['index_url'],
                                        $v['sub_domain'],
                                        '<div><span class="label label-sm label-warning"> Suspended </span></div>');
                
            }
        }
        // Zend_Debug::dump($records);die();
        $records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;
        
        /*
         * $gg = new Escort_Model_Kpu(); $res = $gg->get_datakpu($data2['witel']); //var_dump($res);die();
         */
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }
    public function ajaxlogsAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $params = $this->getRequest()->getParams();
        //  Zend_Debug::dump($params);die();
        $this->_helper->layout->disableLayout();
        // $this->_helper->viewRenderer->setNoRender(true);
        $mdl_admin = new Model_Actlogs();
         $countgraph = $mdl_admin->count_list_logs($_GET);
        // Zend_Debug::dump($countgraph);die();
        //die("xxx");
        $listgraph = $mdl_admin->get_list_logs($_GET);
        // Zend_Debug::dump($listgraph);die();
        $iTotalRecords = intval($countgraph);
        $iDisplayLength = intval($_GET['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_GET['iDisplayStart']);
        $sEcho = intval($_GET['sEcho']);
        $records = array();
        $records["aaData"] = array();
        $end = $iDisplayStart + $iDisplayLength;

        foreach($listgraph as $k => $v) {
            $records["aaData"][] = array($iDisplayStart+$k+ 1,
                                        $v['log_type'],
                                        $v['subject'],
                                        $v['reff_id'],
                                        $v['log_date'],
                                        $v['description'],
                                        $v['x2']);
        }
        // Zend_Debug::dump($records);die();
        $records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;
        
        /*
         * $gg = new Escort_Model_Kpu(); $res = $gg->get_datakpu($data2['witel']); //var_dump($res);die();
         */
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }

    public function listaction2Action() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        $mdl_admin = new Model_Zprabapage();
        $countgraph = $mdl_admin->count_listaction($_GET, $params['wf']);
        $listgraph = $mdl_admin->listaction_ajax($_GET, $params['wf']);
        # Zend_Debug::dump($listgraph);die();
        $iTotalRecords = intval($countgraph);
        $iDisplayLength = intval($_GET['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_GET['iDisplayStart']);
        $sEcho = intval($_GET['sEcho']);
        $records = array();
        $records["aaData"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        //$mdl = new Model_Prabasystem ();
        //$apps = $mdl->get_apps ();
        if($params['wf'] == 1) {
        } else {
            $freeze = "";

            foreach($listgraph as $k => $v) {
                if($v['is_freeze'] == 1) {
                    $freeze = '<a href="javascript:;" data-id="' . $v['id'] . '"  class="btn btn-xs red btn-freeze"><i class="fa fa-times"></i> Unfreeze</a>';
                } else {
                    $freeze = '<a href="javascript:;" data-id="' . $v['id'] . '"  class="btn btn-xs green btn-freeze"><i class="fa fa-times"></i> Freeze</a>';
                }
                $records["aaData"][] = array($iDisplayStart+$k+ 1,
                                            $v['id'],
                                            $v['action_name'],
                                             '<div style="text-align:center"><a href="/domas/action/editaction/id/' .$v['id']. '" data-id="' .$v['id']. '" class="btn btn-xs green btn-edit"><i class="fa fa-search"></i> Edit Action</a>' . '<a href="/domas/action/display/id/' .$v['id']. '/title/' .$v['action_name']. '" data-id="' .$v['id']. '" class="btn btn-xs yellow btn-add"><i class="fa fa-search"></i> View Action</a>' . '<a href="javascript:;" data-id="' .$v['id']. '"  class="btn btn-xs red btn-delete"><i class="fa fa-times"></i> Delete Action</a><a href="javascript:;" data-id="' .$v['id']. '"  class="btn btn-xs blue btn-clone"><i class="fa fa-times"></i> Clone</a>' .$freeze. '</div>');
            }
        }
        $records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;
        
        /*
         * $gg = new Escort_Model_Kpu(); $res = $gg->get_datakpu($data2['witel']); //var_dump($res);die();
         */
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }

    public function setcrawlinitAction() {
        $params = $this->getRequest()->getParams();
        //  Zend_Debug::dump($_POST); die();
        $e = base64_encode(serialize($params));
        $dd = new Domas_Model_Pmedia();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        // echo json_encode($records);
        $json = json_encode(array("url" => "/pmas/madmin/getcrawler/e/" . $e));
        print $json;
        die();
    }

    public function geturlAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $params = $this->getRequest()->getParams();
        $data = $params;
        $this->_helper->layout->disableLayout();
        $g = new Domas_Model_Pmedia();
        $media = $g->get_a_media_by_id($data['media_name']);
        $attrs = base64_encode(serialize(array('tag-index' => $data['tag-index'], 'tag-title' => $data['tag-title'], 'tag-content' => $data['tag-content'], 'tag-author' => $data['tag-author'], 'tag-date' => $data['tag-date'], 'tag-date2' => $data['tag-date2'], 'tag-date3' => $data['tag-date3'], 'tag-topic' => $data['tag-topic'], 'tag-icon' => $data['tag-icon'], 'processing-date' => $data['processing-date'], 'processing-date2' => $data['processing-date2'], 'processing-date3' => $data['processing-date3'])));
        $vdata = array("type_crawl" =>$data['c_type'],
                       'subdomain' =>$data['c_name'],
                       'url_crawl' =>$data['index'],
                       'type_crawl' =>$data['c_type'],
                       'limit_rows' =>$data['r_limit'],
                       'attrs' =>$attrs);
        $cc = new Domas_Model_Pubmediacrawler();
        $vdat = $cc->pub_get_index($vdata);
        //Zend_Debug::dump($vdat); die();
        $rvdat = $vdat['indexes'];
        unset($vdat['indexes']);
        $vdat['indexes'][0] = $rvdat[0];
        //Zend_Debug::dump($dta); die();
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        // echo json_encode($records);
        $json = json_encode($vdat);
        print $json;
        die();
    }

    public function trainingAction() {
        $params = $this->getRequest()->getParams();
        $lang_detect = new Domas_Model_Predictive();
        $ret = $lang_detect->train($params['tid'], $params['stype']);
        $lang_detect->saveToFile($ret, APPLICATION_PATH . 
                                 '/../public/predictive/' . 
                                 $params['stype'] . 
                                 "/" . 
                                 $params['tid']);
    }

    public function updateparamsAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            # Zend_Debug::dump($_POST); die();
            $mdl = new Domas_Model_Params();
            $update = $mdl->update_params($_POST);
            if($update['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$update['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function editapiuserAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            $mdl = new Model_Prabasystem();
            $update = $mdl->update_api_user($_POST);
            if($update['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$update['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function listapiuserAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $mdl_admin = new Model_Prabasystem();
        $countgraph = $mdl_admin->count_listapiuser($_GET);
        $params = $mdl_admin->get_params('api_type');

        foreach($params as $v) {
            $vparams[$v['value_param']] = $v['display_param'];
        }
        $listgraph = $mdl_admin->listapiuser($_GET);
        // Zend_Debug::dump($countgraph);die();
        $iTotalRecords = intval($countgraph);
        $iDisplayLength = intval($_GET['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_GET['iDisplayStart']);
        $sEcho = intval($_GET['sEcho']);
        $records = array();
        $records["aaData"] = array();
        $end = $iDisplayStart + $iDisplayLength;

        foreach($listgraph as $k => $v) {
            $records["aaData"][] = array($iDisplayStart+$k+ 1,
                                        $v['id'],
                                        $v['user_name'],
                                        $v['user_ip'],
                                        $v['user_desc'],
                                         '<div style="text-align:center"><a href="/domas/editor/editapiuser/id/' .$v['id']. '" data-id="' .$v['id']. '" class="btn btn-xs green btn-edit"><i class="fa fa-search"></i> Edit Api</a>' . '<a href="javascript:;" data-id="' .$v['id']. '" data-uname="' .$v['user_name']. '" class="btn btn-xs red btn-delete"><i class="fa fa-times"></i> Delete Api</a></div>');
        }
        $records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;
        
        /*
         * $gg = new Escort_Model_Kpu(); $res = $gg->get_datakpu($data2['witel']); //var_dump($res);die();
         */
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }

    public function testAction() {
        
        /*
         Zend_Debug::dump(unserialize(base64_decode("YToxOntzOjc6InJzc2l0ZW0iO2E6NDp7czo0OiJpY29uIjtzOjExOiJkZXNjcmlwdGlvbiI7czo1OiJ0aXRsZSI7czo1OiJ0aXRsZSI7czo0OiJsaW5rIjtzOjQ6ImxpbmsiO3M6NDoiZGF0ZSI7czo3OiJwdWJEYXRlIjt9fQ=="))); die();
         */
        //Zend_Debug::dump($vdat); die();
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params); die();
        $data = $params;
        $g = new Domas_Model_Pmedia();
        $media = $g->get_a_media_by_id($data['media_name']);
        $paramsx = $data;
        unset($paramsx['controller']);
        unset($paramsx['action']);
        unset($paramsx['module']);
        unset($paramsx['c_name']);
        unset($paramsx['media_name']);
        unset($paramsx['c_type']);
        unset($paramsx['c_status']);
        unset($paramsx['index']);
        unset($paramsx['r_limit']);
        $attrs = base64_encode(serialize($paramsx));
        $vdata = array("type_crawl" =>$data['c_type'],
                       'subdomain' =>$data['c_name'],
                       'url_crawl' =>$data['index'],
                       'type_crawl' =>$data['c_type'],
                       'limit_rows' =>$data['r_limit'],
                       'attrs' =>$attrs);
        $cc = new Domas_Model_Pubmediacrawler();
        $vdat = $cc->pub_get_index($vdata);
        $rvdat = $vdat['indexes'];
        unset($vdat['indexes']);
        $vdat['indexes'][0] = $rvdat[0];
        $vdat['main_media'] = $media['slugname'];
        $content = $cc->pub_get_item($vdat);
        //Zend_Debug::dump($content['content']);
        //die("uuuuuuu");
        //  Zend_Debug::dump($inar); die();
        $out = '<div id="basic-modal-content" class="simplemodal-data" style="display: block;">
    <h3>Results</h3>

       
        <table width="100%" class="table table-hover">
          <tbody>
           <tr>
            <td style="vertical-align: top;">indexurl</td>
            <td style="width: 70%;">' . $content['indexurl'] . '</td>
            </tr>
            <tr>
            <td style="vertical-align: top;">links</td>
            <td style="width: 70%;">' . $content['indexes'][0] . '</td>
            </tr>
            ';
        //  Zend_Debug::dump($content['content']); die();

        foreach($content['content'] as $k => $v) {

            foreach($v as $k2 => $v2) {
                if(!is_array($v2)) {
                    if($k2 == 'icon') {
                        $out .= ' <tr>
            <td style="vertical-align: top;">' . $k2 . '</td>
            <td style="width: 70%;"><img  width="200px" src="' . $v2 . '" /></td>
            </tr>';
                    } else {
                        $out .= ' <tr>
            <td style="vertical-align: top;">' . $k2 . '</td>
            <td style="width: 70%;">' . $v2 . '</td>
            </tr>';
                    }
                } else {

                    foreach($v2 as $k3 => $v3) {
                        $out .= ' <tr>
            <td style="vertical-align: top;">' . $k3 . '</td>
            <td style="width: 70%;">' . $v3 . '</td>
            </tr>';
                        //}
                    }
                }
            }
        }
        $out .= '</tbody></table>';
        $out .= '
    <div id="load"></div>
    <div id="debug"></div>
        </div>';
        $records = array('out' => array('html' =>$out));
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }

    public function updatecrawlAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $params = $this->getRequest()->getParams();
        $this->_helper->layout->disableLayout();
        $cc = new Domas_Model_Pmedia();
        $dta = $cc->update_sub_media($params);
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        $json = json_encode($dta);
        print $json;
        die();
    }

    public function table1Action() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $params = $this->getRequest()->getParams();
        $this->_helper->layout->disableLayout();
        // $this->_helper->viewRenderer->setNoRender(true);
        $mdl_admin = new Model_Admin();
        $countgraph = $mdl_admin->count_listuser($params['app'], $_GET);
        // Zend_Debug::dump($countgraph);die();
        //die("xxx");
        $listgraph = $mdl_admin->get_listuser($params['app'], $_GET);
        // Zend_Debug::dump($listgraph);die();
        $iTotalRecords = intval($countgraph);
        $iDisplayLength = intval($_GET['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_GET['iDisplayStart']);
        $sEcho = intval($_GET['sEcho']);
        $records = array();
        $records["aaData"] = array();
        $end = $iDisplayStart + $iDisplayLength;

        foreach($listgraph as $k => $v) {
            $records["aaData"][] = array($iDisplayStart+$k+ 1,
                                        $v['id'],
                                        $v['uname'],
                                        $v['fullname'],
                                        $v['email'],
                                        $v['ubis'],
                                        $v['sub_ubis'],
                                        $v['jabatan'],
                                        $v['mobile_phone'],
                                        $v['fixed_phone'],
                                         '<div style="text-align:center"><a href="/domas/adminapp/manageuser/uname/' .$v['uname']. '" data-id="' .$v['id']. '" data-uname="' .$v['uname']. '" data-fullname="' .$v['fullname']. '" data-email="' .$v['email']. '" data-ubis="' .$v['ubis']. '" data-sububis="' .$v['sub_ubis']. '" data-sububisid="' .$v['sub_ubis_id']. '" data-position="' .$v['jabatan']. '" data-mobilephone="' .$v['mobile_phone']. '" data-fixedphone="' .$v['fixed_phone']. '" class="btn btn-xs green btn-edit"><i class="fa fa-search"></i> Update User</a>' . '<!--<a href="javascript:;" data-id="' .$v['id']. '" data-uname="' .$v['uname']. '" data-fullname="' .$v['fullname']. '" data-email="' .$v['email']. '" data-ubis="' .$v['ubis']. '" data-sububis="' .$v['sub_ubis']. '" data-sububisid="' .$v['sub_ubis_id']. '" data-position="' .$v['jabatan']. '" data-mobilephone="' .$v['mobile_phone']. '" data-fixedphone="' .$v['fixed_phone']. '" class="btn btn-xs blue btn-edit"><i class="fa fa-pencil"></i> Edit User</a>-->' . '<a href="javascript:;" data-id="' .$v['id']. '" data-uname="' .$v['uname']. '" data-fullname="' .$v['fullname']. '" data-email="' .$v['email']. '" data-ubis="' .$v['ubis']. '" data-sububis="' .$v['sub_ubis']. '" data-sububisid="' .$v['sub_ubis_id']. '" data-position="' .$v['jabatan']. '" data-mobilephone="' .$v['mobile_phone']. '" data-fixedphone="' .$v['fixed_phone']. '" class="btn btn-xs red btn-delete"><i class="fa fa-times"></i> Delete User</a></div>');
        }
        // Zend_Debug::dump($records);die();
        $records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;
        
        /*
         * $gg = new Escort_Model_Kpu(); $res = $gg->get_datakpu($data2['witel']); //var_dump($res);die();
         */
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }

    public function getserverAction() {
        $params = $this->getRequest()->getParams();
        $c = new Domas_Model_Server();
        if($params['nested'] != 1) {
            $data = $c->get_server();
            $this->_helper->layout->disableLayout();

            foreach($data as $k => $v) {
                $result[$k]['id'] = $v['id'];
                $result[$k]['text'] = $v['server_name'] . "-" . $v['server_ip'];
                $result[$k]['parent'] = "#";
                $result[$k]['children'] = true;
                $result[$k]['bunde'] = '';
            }
        } else {
            if(is_numeric($params['id'])) {
                $data = $c->get_a_server($params['id']);
                $docs = unserialize($data['root_loc']);
                $k = 0;

                foreach($docs as $v) {
                    $result[$k]['id'] = base64_encode(serialize(array($params['id'], $v)));
                    $result[$k]['text'] = $v;
                    $result[$k]['parent'] = $params['id'];
                    $result[$k]['children'] = true;
                    $k ++;
                }
            } else {
                $v_id = unserialize(base64_decode($params['id']));
                $result = $c->get_file_server($v_id);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function getcateAction() {
    }

    public function addtaxAction() {
    }

    public function updatemapAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $params = $this->getRequest()->getParams();
        //   Zend_Debug::dump($params); die();
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            $cc = new Domas_Model_Zfiles();
            $ff = new Domas_Model_Withsolrpendem();
            parse_str($_POST['varx'], $data);
            // Zend_Debug::dump($data);
            //die();
            $params = $data;
            // Zend_Debug::dump($params); die();
            $file = array();
            $dd = new Domas_Model_Generalpendem();
            $tags = $dd->get_tags_for_insert($_POST['treecek']);
            // Zend_Debug::dump($tags); die();

            foreach($tags as $k => $v) {

                foreach($v as $k2 => $v2) {
                    $idata['tm']["tm_vid_" . $k . "_names"][] = $v2;
                    $idata['im']["im_vid_" . $k . "_names"][] = $k2;
                    $idata['tid'][] = $k2;
                }
            }
            //  Zend_Debug::dump($idata);

            foreach($data['idfiles'] as $v) {
                $file = $cc->get_a_file($v);
                //Zend_Debug::dump($file);die();
                
                /* $file['raw_url']="/Users/techmayantaraasia/Workingspace/prabacontent/prabacontent/application/../public/repositories/JASAMAR1/Making\ Sense of Data.pdf";*/
                if(file_exists($file['raw_url'])) {
                    //die("xxx"); 
                    if($data['ocr'] == 'on') {
                        $content =(new TesseractOCR(realpath($file['raw_url'])))->run();
                        $v_meta = explode("\n", $meta);
                        $list = array();
                        // Zend_Debug::dump(realpath($file['raw_url']));die();

                        foreach($v_meta as $c) {
                            $vz = array();
                            if($vz != "") {
                                $vz = explode(": ", $c);
                                if($vz[0] != "") {
                                    $list[$vz[0]] = $vz[1];
                                }
                            }
                        }
                    } else {
                        $content = TikaWrapper::getText(realpath($file['raw_url']));
                        $meta = TikaWrapper::getMetadata(realpath($file['raw_url']));
                        $v_meta = explode("\n", $meta);
                        $list = array();
                        // Zend_Debug::dump(realpath($file['raw_url']));die();

                        foreach($v_meta as $c) {
                            $vz = array();
                            if($vz != "") {
                                $vz = explode(": ", $c);
                                if($vz[0] != "") {
                                    $list[$vz[0]] = $vz[1];
                                }
                            }
                        }
                    }
                }
                // die("yyy");
                // Zend_Debug::dump($list);
                if(count($list)> 0)

                    foreach($list as $k => $v) {
                    $idata['ss']['ss_' . $k] = $v;
                }

                foreach($params['fname'] as $k => $v) {
                    if($params['ftype'][$k] == 'string' || $params['ftype'][$k] == "") {
                        $idata['ss']['ss_' . $v] = $params['fvalue'][$k];
                    } else if($params['ftype'][$k] == 'integer') {
                        $idata['is']['is_' . $v] = $params['fvalue'][$k];
                    } else if($params['ftype'][$k] == 'date') {
                        $vf = array();
                        $vf = explode(" ", $params['fvalue'][$k]);
                        //  Zend_Debug::dump($vf); die();
                        if(!isset($vf[1])) {
                            $vf[1] = "00:00:00";
                        }
                        $date = $vf[0] . "T" . $vf[1] . "Z";
                        $idata['ds']['ds_' . $v] = $date;
                    }
                }
                $idata['id'] = 'localhost' . "~" . $file['raw_url'];
                $idata['bundle'] = 'document';
                $idata['label'] = $params['docname'];
                $idata['content'] = $content;
                $idata['bundle_name'] = $file['raw_url'];
                $idata['entity_type'] = 'data-documents';
                $idata['entity_id'] = '3';
                $idata['ss']['ss_description'] = $params['doc_desc'];
                $idata['ss']['ss_httpurl'] = $file['http_url'];
                $idata['is']['is_uid'] = $identity->uid;
                $idata['is']['is_sentiment'] = 0;
                $idata['is']['is_flag'] = 0;
                //Zend_Debug::dump($idata);die();
                $update = $ff->insertdata($idata);
                // Zend_Debug::dump($update);
                //die();
            }
            $result = array('retCode' => '00',
                            'retMsg' => 'add documents succed',
                            'result' => true,
                           );
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function updateatrvocaAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $params = $this->getRequest()->getParams();
        //   Zend_Debug::dump($params); die();
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            //     Zend_Debug::dump($_POST); die();
            $mdl_zusr = new Domas_Model_Admin();
            $delete = $mdl_zusr->add_update_voca($_POST);
            if($delete['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$delete['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $delete = array('retCode' => '11',
                                'retMsg' =>$delete['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function updateatrAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $params = $this->getRequest()->getParams();
        //   Zend_Debug::dump($params); die();
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
           # Zend_Debug::dump($_POST); die();
            $mdl_zusr = new Domas_Model_Admin();
            $delete = $mdl_zusr->add_update($_POST);
            if($delete['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$delete['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $delete = array('retCode' => '11',
                                'retMsg' =>$delete['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function updatedelAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $params = $this->getRequest()->getParams();
        //   Zend_Debug::dump($params); die();
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            $mdl_zusr = new Domas_Model_Admin();
            if(in_array($_POST['parent'], array('tax', 'tag'))) {
                $delete = $mdl_zusr->del_voc($_POST['id']);
            } else {
                $delete = $mdl_zusr->del_tax($_POST['id']);
            }
            //  Zend_Debug::dump($delete); die();
            if($delete['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$delete['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $delete = array('retCode' => '11',
                                'retMsg' =>$delete['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function getformAction() {
        $params = $this->getRequest()->getParams();
        $this->_helper->layout->disableLayout();
        $arr = array('tags',
                     'TAXONOMY');
        if(is_numeric($params['id'])) {
            $this->get_term_form($params['id']);
        } elseif(!in_array($params['id'], $arr)) {
            $this->get_voca($params['id']);
        } else {
            echo "";
        }
        die();
    }
	
	public function getformbigspiderAction() {
        $params = $this->getRequest()->getParams();
        $this->_helper->layout->disableLayout();
        $arr = array('tags',
                     'TAXONOMY');
        if(is_numeric($params['id'])) {
            $this->get_term_formbigspider($params['id']);
        } elseif(!in_array($params['id'], $arr)) {
            $this->get_vocabigspider($params['id']);
        } else {
            echo "";
        }
        die();
    }

    function get_voca($id) {
        $ff = new Model_System();
        $id = substr($id, 4);
        $cc = new Domas_Model_Admin();
        $rl = $ff->get_roles();
        $vardat = $cc->get_vocabulary_by_id($id);
        //Zend_Debug::dump($vardat); die();
        $ee = new Domas_Model_Params();
        $lang = $ee->get_params_by_key("pmas_language");
        echo ' <div class="portlet light">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-puzzle font-grey-gallery"></i>
                                            <span class="caption-subject bold font-grey-gallery uppercase">New </span>
                                        </div>
                                        <div class="tools">
                                            <a href="" class="collapse" data-original-title="" title=""> </a>
                                            <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                                            <a href="" class="reload" data-original-title="" title=""> </a>
                                            <a href="" class="fullscreen" data-original-title="" title=""> </a>
                                            <a href="" class="remove" data-original-title="" title=""> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body form">
                                       
                                        <form class="form-horizontal" id="updating" role="form">
                                            <input type="hidden"  name="id" value="' . $id . '"/>
                                            <div class="form-body">
                                                 <div class="form-group">
                                                    <label class="col-md-3 control-label">Type</label>
                                                    <div class="col-md-9">
                                                        <select name="parent" class="form-control">
                                                           <option ';
        if($vardat['hierarchy'] == '1') {
            echo ' selected ';
        }
        echo ' value="tax">Taxonomy</option>
                                                           <option ';
        if($vardat['hierarchy'] == '0') {
            echo ' selected ';
        }
        echo 'value="tag">Tagging</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Name</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="t_name" class="form-control" placeholder="Name" value="' . $vardat['name'] . '"> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">ShortName</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="s_name" class="form-control" placeholder="ShortName" value="' . $vardat['machine_name'] . '"> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Description</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="t_desc" value="' . $vardat['description'] . '" class="form-control" placeholder="Description"> </div>
                                                </div> 
                                                
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Weight</label>
                                                    <div class="col-md-9">
                                                        <select name="weight" class="form-control">';
        for($i = 1;
        $i < 20;
        $i ++ ) {
            $sel = "";
            if($i == $vardat['weight']) {
                $sel = "selected";
            }
            echo '<option ' . $sel . ' value="' . $i . '">' . $i . '</i></option>';
        }
        echo '</select>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Language</label>
                                                    <div class="col-md-9">
                                                        <select name="language" class="form-control">';

        foreach($lang as $v) {
            $sel = "";
            if($v['value_param'] == $vardat['language']) {
                $sel = " selected ";
            }
            echo '<option ' . $sel . ' value="' . $v['value_param'] . '">' . $v['display_param'] . '</i></option>';
        }
        echo '</select>
                                                    </div>
                                                </div>
                                                
                                                
                                                 <div class="form-group">
                                                    <label class="col-md-3 control-label">Roles</label>
                                                    <div class="col-md-9">
                                                       <select  name="roles[]" id="roles" class="form-control" placeholder="Roles" multiple>';

        foreach($rl as $z) {
            $sel = "";
            if(in_array($z['gid'], $perms)) {
                $sel = " selected ";
            }
            echo '<option ' . $sel . ' value="' . $z['gid'] . '">' . $z['group_name'] . '</i></option>';
        }
        echo '</select>
                                                    </div>
                                                </div>
										
                                               
                                            </div>
                                            <div class="form-actions right1">
                                            
                                                <button type="submit" class="btn green">Submit</button>
                                                <button type="button" class="btn del red">Delete</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>';
        echo "<script>
                
                $('.del').click(function(){
                        $('#ajax-loader').show();
                        $.ajax({
                                url:'/domas/ajax/updatedel',
                                content:'json',
                                type:'post',
                                data:$('form#updating').serialize(),
                                beforeSend:function(){
                                
                                },
                                complete:function(result){
                                    
                                },
                                success: function(data){
                                   $('#tax').jstree('refresh');
                                 
                                    if(data.result!=undefined){
                                        if(data.result==true){
                                            
                                        toastr.info('Delete succed');
                                       $('#ajax-loader').hide();
                                       
                                        }else{
                                            
                                        }
                                    }else{
                                        
                                    }
                                    $('#alertinfo').removeClass('hidden');
                                },
                                error:function(){
                                    $('#alertinfo').addClass('alert-info');
                                    $('#alertmsg').html('<strong>Error!</strong> System is busy, please try again.');
                                    $('#alertinfo').removeClass('hidden');
                                }
                            });
                    
                });
                  $('#ajax-loader').hide();
                
                $('form#updating').validate({
	            
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	                t_name: {
	                    required: true
	                }
	            },

	            messages: {
					
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
	               
					$('span.help-block').hide();
	                var errorlist = validator.errorList;
	                if(errorlist.length>0){
						for(var i = 0; i<errorlist.length; i++){
							//console.log(errorlist[i].element);
							var el = errorlist[i].element; 
							var next = $(el).next('span.help-block'); 
							$(next).slideDown();
						}
						$('.modal-scrollable:visible').animate({
							scrollTop: $(el).offset().top
						}, 800);
					}
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	            },

	            errorPlacement: function (error, element) {
					$('span.help-block').hide();
					var next = element.next('span.help-block'); 
				    if(error[0].textContent!=\" \"){
						$(next).children('span.label').html(\"<i class='fa fa-warning'></i> \"+ error[0].textContent);
						$(element).next().slideDown();
						$('.modal-scrollable:visible').animate({
							scrollTop: $(element).offset().top
						}, 800);
					}
	            },

	            submitHandler: function (form) {
				//	alert('ok');
                    $('#alertinfo').addClass('hidden');
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	               
                submitForm();
	                return false;
	            }
	        });

	        $('form#updating input').keypress(function (e) {
	            if (e.which == 13) {
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	                if ($('form#edituser').validate().form()) {
	                   //$('form#edituser').submit(); //form validation success, call ajax form submit
	                   submitForm();
	                }
	                return false;
	            }
	        });
            
            var submitForm = function(){
		
		$.ajax({
			url:'/domas/ajax/updateatrvoca',
			content:'json',
			type:'post',
			data:$('form#updating').serialize(),
			beforeSend:function(){
				$('#alertinfo').addClass('hidden');
				$('#processbar').removeClass('hidden');
				
				$('#alertinfo').removeClass('alert-success');
				$('#alertinfo').removeClass('alert-warning');
				$('#alertinfo').removeClass('alert-danger');
				$('#alertinfo').removeClass('alert-info');
			},
			complete:function(result){
				$('#processbar').addClass('hidden');
			},
			success: function(data){
               $('#tax').jstree('refresh');
				//console.log(data);
				if(data.result!=undefined){
					if(data.result==true){
					toastr.info('Update succed');
					//	resetForm();
					}else{
						toastr.alert('Failed');
					}
				}else{
					$('#alertinfo').addClass('alert-info');
					$('#alertmsg').html('<strong>Error!</strong> System is busy, please try again.');
				}
				$('#alertinfo').removeClass('hidden');
			},
			error:function(){
				$('#alertinfo').addClass('alert-info');
				$('#alertmsg').html('<strong>Error!</strong> System is busy, please try again.');
				$('#alertinfo').removeClass('hidden');
			}
		});
	}
            
$('#roles').select2();
            </script>";
        die();
    }
	
	function get_vocabigspider($id) {
        $ff = new Model_System();
        $id = substr($id, 4);
        $cc = new Domas_Model_Admin();
        $rl = $ff->get_roles();
        $vardat = $cc->get_vocabulary_by_id($id);
        //Zend_Debug::dump($vardat); die();
        $ee = new Domas_Model_Params();
        $lang = $ee->get_params_by_key("pmas_language");
        echo ' 
		<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							<i class="flaticon-puzzle"></i> New
						</h3>
						
					</div>
				</div>
				<div class="m-portlet__head-tools">
				</div>
			</div>
				
			<div class="m-portlet__body">
		
				<form class="form-horizontal" id="updating" role="form">
					<input type="hidden"  name="id" value="' . $id . '"/>
					<div class="form-body">
						 <div class="form-group">
							<label class="col-md-3 control-label">Type</label>
							<div class="col-md-9">
								<select name="parent" class="form-control">
								   <option ';
        if($vardat['hierarchy'] == '1') {
            echo ' selected ';
        }
        echo ' value="tax">Taxonomy</option>
                                                           <option ';
        if($vardat['hierarchy'] == '0') {
            echo ' selected ';
        }
        echo 'value="tag">Tagging</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Name</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="t_name" class="form-control" placeholder="Name" value="' . $vardat['name'] . '"> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">ShortName</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="s_name" class="form-control" placeholder="ShortName" value="' . $vardat['machine_name'] . '"> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Description</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="t_desc" value="' . $vardat['description'] . '" class="form-control" placeholder="Description"> </div>
                                                </div> 
                                                
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Weight</label>
                                                    <div class="col-md-9">
                                                        <select name="weight" class="form-control">';
        for($i = 1;
        $i < 20;
        $i ++ ) {
            $sel = "";
            if($i == $vardat['weight']) {
                $sel = "selected";
            }
            echo '<option ' . $sel . ' value="' . $i . '">' . $i . '</i></option>';
        }
        echo '</select>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Language</label>
                                                    <div class="col-md-9">
                                                        <select name="language" class="form-control">';

        foreach($lang as $v) {
            $sel = "";
            if($v['value_param'] == $vardat['language']) {
                $sel = " selected ";
            }
            echo '<option ' . $sel . ' value="' . $v['value_param'] . '">' . $v['display_param'] . '</i></option>';
        }
        echo '</select>
                                                    </div>
                                                </div>
                                                
                                                
                                                 <div class="form-group">
                                                    <label class="col-md-3 control-label">Roles</label>
                                                    <div class="col-md-9">
                                                       <select  name="roles[]" id="roles" class="form-control" placeholder="Roles" multiple>';

        foreach($rl as $z) {
            $sel = "";
            if(in_array($z['gid'], $perms)) {
                $sel = " selected ";
            }
            echo '<option ' . $sel . ' value="' . $z['gid'] . '">' . $z['group_name'] . '</i></option>';
        }
        echo '</select>
                                                    </div>
                                                </div>
										
                                               
                                            </div>
                                            <div class="form-actions right1">
                                            
                                                <button type="submit" class="btn btn-success">Submit</button>
                                                <button type="button" class="btn del btn-danger">Delete</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>';
        echo "<script>
                
                $('.del').click(function(){
                        $('#ajax-loader').show();
                        $.ajax({
                                url:'/domas/ajax/updatedel',
                                content:'json',
                                type:'post',
                                data:$('form#updating').serialize(),
                                beforeSend:function(){
                                
                                },
                                complete:function(result){
                                    
                                },
                                success: function(data){
                                   $('#tax').jstree('refresh');
                                 
                                    if(data.result!=undefined){
                                        if(data.result==true){
                                            
                                        toastr.info('Delete succed');
                                       $('#ajax-loader').hide();
                                       
                                        }else{
                                            
                                        }
                                    }else{
                                        
                                    }
                                    $('#alertinfo').removeClass('hidden');
                                },
                                error:function(){
                                    $('#alertinfo').addClass('alert-info');
                                    $('#alertmsg').html('<strong>Error!</strong> System is busy, please try again.');
                                    $('#alertinfo').removeClass('hidden');
                                }
                            });
                    
                });
                  $('#ajax-loader').hide();
                
                $('form#updating').validate({
	            
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	                t_name: {
	                    required: true
	                }
	            },

	            messages: {
					
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
	               
					$('span.help-block').hide();
	                var errorlist = validator.errorList;
	                if(errorlist.length>0){
						for(var i = 0; i<errorlist.length; i++){
							//console.log(errorlist[i].element);
							var el = errorlist[i].element; 
							var next = $(el).next('span.help-block'); 
							$(next).slideDown();
						}
						$('.modal-scrollable:visible').animate({
							scrollTop: $(el).offset().top
						}, 800);
					}
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	            },

	            errorPlacement: function (error, element) {
					$('span.help-block').hide();
					var next = element.next('span.help-block'); 
				    if(error[0].textContent!=\" \"){
						$(next).children('span.label').html(\"<i class='fa fa-warning'></i> \"+ error[0].textContent);
						$(element).next().slideDown();
						$('.modal-scrollable:visible').animate({
							scrollTop: $(element).offset().top
						}, 800);
					}
	            },

	            submitHandler: function (form) {
				//	alert('ok');
                    $('#alertinfo').addClass('hidden');
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	               
                submitForm();
	                return false;
	            }
	        });

	        $('form#updating input').keypress(function (e) {
	            if (e.which == 13) {
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	                if ($('form#edituser').validate().form()) {
	                   //$('form#edituser').submit(); //form validation success, call ajax form submit
	                   submitForm();
	                }
	                return false;
	            }
	        });
            
            var submitForm = function(){
		
		$.ajax({
			url:'/domas/ajax/updateatrvoca',
			content:'json',
			type:'post',
			data:$('form#updating').serialize(),
			beforeSend:function(){
				$('#alertinfo').addClass('hidden');
				$('#processbar').removeClass('hidden');
				
				$('#alertinfo').removeClass('alert-success');
				$('#alertinfo').removeClass('alert-warning');
				$('#alertinfo').removeClass('alert-danger');
				$('#alertinfo').removeClass('alert-info');
			},
			complete:function(result){
				$('#processbar').addClass('hidden');
			},
			success: function(data){
               $('#tax').jstree('refresh');
				//console.log(data);
				if(data.result!=undefined){
					if(data.result==true){
					toastr.info('Update succed');
					//	resetForm();
					}else{
						toastr.alert('Failed');
					}
				}else{
					$('#alertinfo').addClass('alert-info');
					$('#alertmsg').html('<strong>Error!</strong> System is busy, please try again.');
				}
				$('#alertinfo').removeClass('hidden');
			},
			error:function(){
				$('#alertinfo').addClass('alert-info');
				$('#alertmsg').html('<strong>Error!</strong> System is busy, please try again.');
				$('#alertinfo').removeClass('hidden');
			}
		});
	}
            
$('#roles').select2();
            </script>";
        die();
    }

    function get_term_form($id) {
        //die("xx");
        $cc = new Domas_Model_Admin();
        $data = $cc->taxonomy_strc();
        $cc1 = new Model_Zparams();
        $icon = $cc1->get_icons();
        $vardat = $cc->get_term_data($id);
        $ff = new Model_System();
        $rl = $ff->get_roles();
        // Zend_Debug::dump($vardat); 
        #die();
        $perms = unserialize($vardat['permissions']);
        if($vardat['hierarchy'] == 1 && $vardat['parent'] > 0) {
            $valparent = $vardat['parent'];
        } else {
            $valparent = "vid_" . $vardat['vid'];
        }
        $var = "";
        // echo $valparent;die();

        foreach($data as $k => $v) {
            if($k == "TAGGING") {

                foreach($v['key'] as $k2 => $v2) {
                    $sel = "";
                    if($valparent == 'vid_' . 
                       $k2) {
                        $sel = "selected";
                    }
                    $var .= '<option ' . $sel . ' value="vid_' . $k2 . '">' . $data['TAGGING']['val'][$k2]['name'] . '</option>';
                }
            } else if($k == "TAXONOMY") {
                if(count($v['key'])> 0) {

                    foreach($v['key'] as $k2 => $v2) {
                        $var .= '<option value="vid_' . $k2 . '">' . $data['TAXONOMY']['val'][$k2]['name'] . '</option>';

                        foreach($data['TAXONOMY']['key'][$k2][0][0] as $z3 => $v3) {
                            $sel = "";
                            if($valparent == $v3['tid']) {
                                $sel = "selected";
                            }
                            $var .= '<option ' . $sel . ' value="' . $v3['tid'] . '">-' . $v3['name'] . '</option>';
                            if(count($data['TAXONOMY']['key'][$k2][1][$v3['tid']])> 0) {

                                foreach($data['TAXONOMY']['key'][$k2][1][$v3['tid']] as $z4 => $v4) {
                                    $sel = "";
                                    if($valparent == $v4['tid']) {
                                        $sel = "selected";
                                    }
                                    $var .= '<option ' . $sel . ' value="' . $v4['tid'] . '">--' . $v4['name'] . '</option>';
                                    if(count($data['TAXONOMY']['key'][$k2][2][$v4['tid']])> 0) {

                                        foreach($data['TAXONOMY']['key'][$k2][2][$v4['tid']] as $z5 => $v5) {
                                            $sel = "";
                                            if($valparent == $v5['tid']) {
                                                $sel = "selected";
                                            }
                                            $var .= '<option ' . $sel . ' value="' . $v5['tid'] . '">---' . $v5['name'] . '</option>';
                                            if(count($data['TAXONOMY']['key'][$k2][3][$v5['tid']])> 0) {

                                                foreach($data['TAXONOMY']['key'][$k2][3][$v5['tid']] as $z6 => $v6) {
                                                    $sel = "";
                                                    if($valparent == $v6['tid']) {
                                                        $sel = "selected";
                                                    }
                                                    $var .= '<option ' . $sel . ' value="' . $v6['tid'] . '">---' . $v6['name'] . '</option>';
                                                    if(count($data['TAXONOMY']['key'][$k2][4][$v6['tid']])> 0) {

                                                        foreach($data['TAXONOMY']['key'][$k2][3][$v6['tid']] as $z7 => $v7) {
                                                            $sel = "";
                                                            if($valparent == $v7['tid']) {
                                                                $sel = "selected";
                                                            }
                                                            $var .= '<option ' . $sel . ' value="' . $v7['tid'] . '">---' . $v7['name'] . '</option>';
                                                            if(count($data['TAXONOMY']['key'][$k2][4][$v7['tid']])> 0) {

                                                                foreach($data['TAXONOMY']['key'][$k2][3][$v7['tid']] as $z8 => $v8) {
                                                                    $sel = "";
                                                                    if($valparent == $v8['tid']) {
                                                                        $sel = "selected";
                                                                    }
                                                                    $var .= '<option ' . $sel . ' value="' . $v8['tid'] . '">---' . $v8['name'] . '</option>';
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
       // Zend_Debug::dump($vardat); die();
        echo ' <div class="portlet light">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-puzzle font-grey-gallery"></i>
                                            <span class="caption-subject bold font-grey-gallery uppercase">' . $varx . '</span>
                                        </div>
                                        <div class="tools">
                                            <a href="" class="collapse" data-original-title="" title=""> </a>
                                            <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                                            <a href="" class="reload" data-original-title="" title=""> </a>
                                            <a href="" class="fullscreen" data-original-title="" title=""> </a>
                                            <a href="" class="remove" data-original-title="" title=""> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body form">
                                       
                                        <form class="form-horizontal" id="updating" role="form">
                                            <input type="hidden"  name="id" value="' . $id . '"/>
                                            <div class="form-body">
                                                 <div class="form-group">
                                                    <label class="col-md-3 control-label">Parent</label>
                                                    <div class="col-md-9">
                                                        <select name="parent" class="form-control">' . $var . '
                                                           
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Name</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="t_name" class="form-control" placeholder="Name" value="' . $vardat['name'] . '"> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Description</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="t_desc" value="' . $vardat['description'] . '" class="form-control" placeholder="Description"> </div>
                                                </div> 
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Attr1</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="attr1" value="' . $vardat['attr1'] . '" class="form-control" placeholder="Attr1"> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Attr2</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="attr2" value="' . $vardat['attr2'] . '" class="form-control" placeholder="Attr2"> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Attr3</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="attr3" value="' . $vardat['attr3'] . '" class="form-control" placeholder="Attr3"> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Attr4</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="attr4" value="' . $vardat['attr4'] . '" class="form-control" placeholder="Attr4"> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Attr5</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="attr5" value="' . $vardat['attr5'] . '" class="form-control" placeholder="Attr5"> </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Class</label>
                                                    <div class="col-md-9">
                                                        <select name="class" class="form-control">';

        foreach($icon as $v) {
            echo '<option value="' . $v . '"><i class="fa ' . $v . '">' . $v . '</i></option>';
        }
        echo '</select>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Weight</label>
                                                    <div class="col-md-9">
                                                        <select name="weight" class="form-control">';
        for($i = 1;
        $i < 20;
        $i ++ ) {
            $sel="";
            if($i==$vardat['weight']) {
                $sel =" selected ";
            }
            echo '<option '.$sel.' value="' . $i . '">' . $i . '</i></option>';
        }
        echo '</select>
                                                    </div>
                                                </div>
                                                  <div class="form-group">
                                                    <label class="col-md-3 control-label">Roles</label>
                                                    <div class="col-md-9">
                                                       <select  name="roles[]" id="roles" class="form-control" placeholder="Roles" multiple>';

        foreach($rl as $z) {
           // Zend_Debug::dump($z);
            $sel = "";
            if(in_array($z['gid'], $perms)) {
                $sel = " selected ";
            }
               
            echo '<option ' . $sel . ' value="' . $z['gid'] . '">' . $z['group_name'] . '</i></option>';
        }
        
         $cek ="";
             if($vardat['is_crawl']==1) {
                $cek  = " checked ";
            }
        echo '</select>
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                <label class="col-md-3 control-label">Is Keyword</label>
                                               
                                                    <div class="md-checkbox col-md-6">
                                                        <input type="checkbox" id="checkbox6" name="kyw" value="1" '.$cek .' class="md-check">
                                                        <label for="checkbox6">
                                                            <span class="inc"></span>
                                                            <span class="check"></span>
                                                            <span class="box"></span> Yes</label>
                                                   
                                                    
                                                </div>
                                            </div>
                                                
                                                 <div class="form-group">
                                                    <label class="col-md-3 control-label">Rules</label>
                                                    <div class="col-md-9">
                                                       <select  name="rules[]" id="rules" class="form-control" placeholder="Roles" multiple>';

        foreach($rl as $z) {
            $sel = "";
            if(in_array($z['gid'], $perms)) {
                $sel = " selected ";
            }
         //   echo '<option ' . $sel . ' value="' . $z['gid'] . '">' . $z['group_name'] . '</i></option>';
        }
        echo '</select>
                                                    </div>
                                                </div>
                                               
                                            </div>
                                            <div class="form-actions right1">
                                            
                                                <button type="submit" class="btn green">Submit</button>
                                                <button type="button" class="btn  del red">Hapus</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>';
        echo "<script>
                
                $('#roles').select2();
                $('#rules').select2();
                $('.del').click(function(){
                        $('#ajax-loader').show();
                        $.ajax({
                                url:'/domas/ajax/updatedel',
                                content:'json',
                                type:'post',
                                data:$('form#updating').serialize(),
                                beforeSend:function(){
                                
                                },
                                complete:function(result){
                                    
                                },
                                success: function(data){
                                   $('#tax').jstree('refresh');
                                 
                                    if(data.result!=undefined){
                                        if(data.result==true){
                                            
                                        toastr.info('Delete succed');
                                       $('#ajax-loader').hide();
                                       
                                        }else{
                                            
                                        }
                                    }else{
                                        
                                    }
                                    $('#alertinfo').removeClass('hidden');
                                },
                                error:function(){
                                    $('#alertinfo').addClass('alert-info');
                                    $('#alertmsg').html('<strong>Error!</strong> System is busy, please try again.');
                                    $('#alertinfo').removeClass('hidden');
                                }
                            });
                    
                });
                  $('#ajax-loader').hide();
                
                $('form#updating').validate({
	            
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	                t_name: {
	                    required: true
	                }
	            },

	            messages: {
					
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
	               
					$('span.help-block').hide();
	                var errorlist = validator.errorList;
	                if(errorlist.length>0){
						for(var i = 0; i<errorlist.length; i++){
							//console.log(errorlist[i].element);
							var el = errorlist[i].element; 
							var next = $(el).next('span.help-block'); 
							$(next).slideDown();
						}
						$('.modal-scrollable:visible').animate({
							scrollTop: $(el).offset().top
						}, 800);
					}
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	            },

	            errorPlacement: function (error, element) {
					$('span.help-block').hide();
					var next = element.next('span.help-block'); 
				    if(error[0].textContent!=\" \"){
						$(next).children('span.label').html(\"<i class='fa fa-warning'></i> \"+ error[0].textContent);
						$(element).next().slideDown();
						$('.modal-scrollable:visible').animate({
							scrollTop: $(element).offset().top
						}, 800);
					}
	            },

	            submitHandler: function (form) {
				//	alert('ok');
                    $('#alertinfo').addClass('hidden');
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	               
                submitForm();
	                return false;
	            }
	        });

	        $('form#updating input').keypress(function (e) {
	            if (e.which == 13) {
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	                if ($('form#edituser').validate().form()) {
	                   //$('form#edituser').submit(); //form validation success, call ajax form submit
	                   submitForm();
	                }
	                return false;
	            }
	        });
            
            var submitForm = function(){
		
		$.ajax({
			url:'/domas/ajax/updateatr',
			content:'json',
			type:'post',
			data:$('form#updating').serialize(),
			beforeSend:function(){
				$('#alertinfo').addClass('hidden');
				$('#processbar').removeClass('hidden');
				
				$('#alertinfo').removeClass('alert-success');
				$('#alertinfo').removeClass('alert-warning');
				$('#alertinfo').removeClass('alert-danger');
				$('#alertinfo').removeClass('alert-info');
			},
			complete:function(result){
				$('#processbar').addClass('hidden');
			},
			success: function(data){
               $('#tax').jstree('refresh');
				//console.log(data);
				if(data.result!=undefined){
					if(data.result==true){
					toastr.info('Update succed');
					//	resetForm();
					}else{
						toastr.alert('Failed');
					}
				}else{
					$('#alertinfo').addClass('alert-info');
					$('#alertmsg').html('<strong>Error!</strong> System is busy, please try again.');
				}
				$('#alertinfo').removeClass('hidden');
			},
			error:function(){
				$('#alertinfo').addClass('alert-info');
				$('#alertmsg').html('<strong>Error!</strong> System is busy, please try again.');
				$('#alertinfo').removeClass('hidden');
			}
		});
	}
            
            </script>";
    }
	
	function get_term_formbigspider($id) {
        //die("xx");
        $cc = new Domas_Model_Admin();
        $data = $cc->taxonomy_strc();
        $cc1 = new Model_Zparams();
        $icon = $cc1->get_icons();
        $vardat = $cc->get_term_data($id);
        $ff = new Model_System();
        $rl = $ff->get_roles();
        // Zend_Debug::dump($vardat); 
        #die();
        $perms = unserialize($vardat['permissions']);
        if($vardat['hierarchy'] == 1 && $vardat['parent'] > 0) {
            $valparent = $vardat['parent'];
        } else {
            $valparent = "vid_" . $vardat['vid'];
        }
        $var = "";
        // echo $valparent;die();

        foreach($data as $k => $v) {
            if($k == "TAGGING") {

                foreach($v['key'] as $k2 => $v2) {
                    $sel = "";
                    if($valparent == 'vid_' . 
                       $k2) {
                        $sel = "selected";
                    }
                    $var .= '<option ' . $sel . ' value="vid_' . $k2 . '">' . $data['TAGGING']['val'][$k2]['name'] . '</option>';
                }
            } else if($k == "TAXONOMY") {
                if(count($v['key'])> 0) {

                    foreach($v['key'] as $k2 => $v2) {
                        $var .= '<option value="vid_' . $k2 . '">' . $data['TAXONOMY']['val'][$k2]['name'] . '</option>';

                        foreach($data['TAXONOMY']['key'][$k2][0][0] as $z3 => $v3) {
                            $sel = "";
                            if($valparent == $v3['tid']) {
                                $sel = "selected";
                            }
                            $var .= '<option ' . $sel . ' value="' . $v3['tid'] . '">-' . $v3['name'] . '</option>';
                            if(count($data['TAXONOMY']['key'][$k2][1][$v3['tid']])> 0) {

                                foreach($data['TAXONOMY']['key'][$k2][1][$v3['tid']] as $z4 => $v4) {
                                    $sel = "";
                                    if($valparent == $v4['tid']) {
                                        $sel = "selected";
                                    }
                                    $var .= '<option ' . $sel . ' value="' . $v4['tid'] . '">--' . $v4['name'] . '</option>';
                                    if(count($data['TAXONOMY']['key'][$k2][2][$v4['tid']])> 0) {

                                        foreach($data['TAXONOMY']['key'][$k2][2][$v4['tid']] as $z5 => $v5) {
                                            $sel = "";
                                            if($valparent == $v5['tid']) {
                                                $sel = "selected";
                                            }
                                            $var .= '<option ' . $sel . ' value="' . $v5['tid'] . '">---' . $v5['name'] . '</option>';
                                            if(count($data['TAXONOMY']['key'][$k2][3][$v5['tid']])> 0) {

                                                foreach($data['TAXONOMY']['key'][$k2][3][$v5['tid']] as $z6 => $v6) {
                                                    $sel = "";
                                                    if($valparent == $v6['tid']) {
                                                        $sel = "selected";
                                                    }
                                                    $var .= '<option ' . $sel . ' value="' . $v6['tid'] . '">---' . $v6['name'] . '</option>';
                                                    if(count($data['TAXONOMY']['key'][$k2][4][$v6['tid']])> 0) {

                                                        foreach($data['TAXONOMY']['key'][$k2][3][$v6['tid']] as $z7 => $v7) {
                                                            $sel = "";
                                                            if($valparent == $v7['tid']) {
                                                                $sel = "selected";
                                                            }
                                                            $var .= '<option ' . $sel . ' value="' . $v7['tid'] . '">---' . $v7['name'] . '</option>';
                                                            if(count($data['TAXONOMY']['key'][$k2][4][$v7['tid']])> 0) {

                                                                foreach($data['TAXONOMY']['key'][$k2][3][$v7['tid']] as $z8 => $v8) {
                                                                    $sel = "";
                                                                    if($valparent == $v8['tid']) {
                                                                        $sel = "selected";
                                                                    }
                                                                    $var .= '<option ' . $sel . ' value="' . $v8['tid'] . '">---' . $v8['name'] . '</option>';
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
       // Zend_Debug::dump($vardat); die();
        echo '
		<div class="m-portlet">
				
			<div class="m-portlet__body">   
                                        <form class="form-horizontal" id="updating" role="form">
                                            <input type="hidden"  name="id" value="' . $id . '"/>
                                            <div class="form-body">
                                                 <div class="form-group">
                                                    <label class="col-md-3 control-label">Parent</label>
                                                    <div class="col-md-9">
                                                        <select name="parent" class="form-control">' . $var . '
                                                           
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Name</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="t_name" class="form-control" placeholder="Name" value="' . $vardat['name'] . '"> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Description</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="t_desc" value="' . $vardat['description'] . '" class="form-control" placeholder="Description"> </div>
                                                </div> 
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Attr1</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="attr1" value="' . $vardat['attr1'] . '" class="form-control" placeholder="Attr1"> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Attr2</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="attr2" value="' . $vardat['attr2'] . '" class="form-control" placeholder="Attr2"> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Attr3</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="attr3" value="' . $vardat['attr3'] . '" class="form-control" placeholder="Attr3"> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Attr4</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="attr4" value="' . $vardat['attr4'] . '" class="form-control" placeholder="Attr4"> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Attr5</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="attr5" value="' . $vardat['attr5'] . '" class="form-control" placeholder="Attr5"> </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Class</label>
                                                    <div class="col-md-9">
                                                        <select name="class" class="form-control">';

        foreach($icon as $v) {
            echo '<option value="' . $v . '"><i class="fa ' . $v . '">' . $v . '</i></option>';
        }
        echo '</select>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Weight</label>
                                                    <div class="col-md-9">
                                                        <select name="weight" class="form-control">';
        for($i = 1;
        $i < 20;
        $i ++ ) {
            $sel="";
            if($i==$vardat['weight']) {
                $sel =" selected ";
            }
            echo '<option '.$sel.' value="' . $i . '">' . $i . '</i></option>';
        }
        echo '</select>
                                                    </div>
                                                </div>
                                                  <div class="form-group">
                                                    <label class="col-md-3 control-label">Roles</label>
                                                    <div class="col-md-9">
                                                       <select  name="roles[]" id="roles" class="form-control" placeholder="Roles" multiple>';

        foreach($rl as $z) {
           // Zend_Debug::dump($z);
            $sel = "";
            if(in_array($z['gid'], $perms)) {
                $sel = " selected ";
            }
               
            echo '<option ' . $sel . ' value="' . $z['gid'] . '">' . $z['group_name'] . '</i></option>';
        }
        
         $cek ="";
             if($vardat['is_crawl']==1) {
                $cek  = " checked ";
            }
        echo '</select>
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                <label class="col-md-3 control-label">Is Keyword</label>
                                               
                                                    <div class="md-checkbox col-md-6">
                                                        <input type="checkbox" id="checkbox6" name="kyw" value="1" '.$cek .' class="md-check">
                                                        <label for="checkbox6">
                                                            <span class="inc"></span>
                                                            <span class="check"></span>
                                                            <span class="box"></span> Yes</label>
                                                   
                                                    
                                                </div>
                                            </div>
                                                
                                                 <div class="form-group">
                                                    <label class="col-md-3 control-label">Rules</label>
                                                    <div class="col-md-9">
                                                       <select  name="rules[]" id="rules" class="form-control" placeholder="Roles" multiple>';

        foreach($rl as $z) {
            $sel = "";
            if(in_array($z['gid'], $perms)) {
                $sel = " selected ";
            }
         //   echo '<option ' . $sel . ' value="' . $z['gid'] . '">' . $z['group_name'] . '</i></option>';
        }
        echo '</select>
                                                    </div>
                                                </div>
                                               
                                            </div>
                                            <div class="form-actions right1">
                                            
                                                <button type="submit" class="btn btn-success">Submit</button>
                                                <button type="button" class="btn  del btn-danger">Hapus</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>';
        echo "<script>
                
                $('#roles').select2();
                $('#rules').select2();
                $('.del').click(function(){
                        $('#ajax-loader').show();
                        $.ajax({
                                url:'/domas/ajax/updatedel',
                                content:'json',
                                type:'post',
                                data:$('form#updating').serialize(),
                                beforeSend:function(){
                                
                                },
                                complete:function(result){
                                    
                                },
                                success: function(data){
                                   $('#tax').jstree('refresh');
                                 
                                    if(data.result!=undefined){
                                        if(data.result==true){
                                            
                                        toastr.info('Delete succed');
                                       $('#ajax-loader').hide();
                                       
                                        }else{
                                            
                                        }
                                    }else{
                                        
                                    }
                                    $('#alertinfo').removeClass('hidden');
                                },
                                error:function(){
                                    $('#alertinfo').addClass('alert-info');
                                    $('#alertmsg').html('<strong>Error!</strong> System is busy, please try again.');
                                    $('#alertinfo').removeClass('hidden');
                                }
                            });
                    
                });
                  $('#ajax-loader').hide();
                
                $('form#updating').validate({
	            
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	                t_name: {
	                    required: true
	                }
	            },

	            messages: {
					
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
	               
					$('span.help-block').hide();
	                var errorlist = validator.errorList;
	                if(errorlist.length>0){
						for(var i = 0; i<errorlist.length; i++){
							//console.log(errorlist[i].element);
							var el = errorlist[i].element; 
							var next = $(el).next('span.help-block'); 
							$(next).slideDown();
						}
						$('.modal-scrollable:visible').animate({
							scrollTop: $(el).offset().top
						}, 800);
					}
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	            },

	            errorPlacement: function (error, element) {
					$('span.help-block').hide();
					var next = element.next('span.help-block'); 
				    if(error[0].textContent!=\" \"){
						$(next).children('span.label').html(\"<i class='fa fa-warning'></i> \"+ error[0].textContent);
						$(element).next().slideDown();
						$('.modal-scrollable:visible').animate({
							scrollTop: $(element).offset().top
						}, 800);
					}
	            },

	            submitHandler: function (form) {
				//	alert('ok');
                    $('#alertinfo').addClass('hidden');
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	               
                submitForm();
	                return false;
	            }
	        });

	        $('form#updating input').keypress(function (e) {
	            if (e.which == 13) {
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	                if ($('form#edituser').validate().form()) {
	                   //$('form#edituser').submit(); //form validation success, call ajax form submit
	                   submitForm();
	                }
	                return false;
	            }
	        });
            
            var submitForm = function(){
		
		$.ajax({
			url:'/domas/ajax/updateatr',
			content:'json',
			type:'post',
			data:$('form#updating').serialize(),
			beforeSend:function(){
				$('#alertinfo').addClass('hidden');
				$('#processbar').removeClass('hidden');
				
				$('#alertinfo').removeClass('alert-success');
				$('#alertinfo').removeClass('alert-warning');
				$('#alertinfo').removeClass('alert-danger');
				$('#alertinfo').removeClass('alert-info');
			},
			complete:function(result){
				$('#processbar').addClass('hidden');
			},
			success: function(data){
               $('#tax').jstree('refresh');
				//console.log(data);
				if(data.result!=undefined){
					if(data.result==true){
					toastr.info('Update succed');
					//	resetForm();
					}else{
						toastr.alert('Failed');
					}
				}else{
					$('#alertinfo').addClass('alert-info');
					$('#alertmsg').html('<strong>Error!</strong> System is busy, please try again.');
				}
				$('#alertinfo').removeClass('hidden');
			},
			error:function(){
				$('#alertinfo').addClass('alert-info');
				$('#alertmsg').html('<strong>Error!</strong> System is busy, please try again.');
				$('#alertinfo').removeClass('hidden');
			}
		});
	}
            
            </script>";
    }
    //end pendem
    public function ajaxsearchallAction() {
    }

    public function ocrAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
            //echo APPLICATION_PATH;
            $shell = shell_exec("tesseract -h");
            $var = file_get_contents('/home/himawijaya/Workingspaces/metrasys/prabacontent/public/xxx.png');
            echo $var;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
        }
        echo $shell;
        //$params['api']
        die("ccc");
    }

    public function getalertAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $params = $this->getRequest()->getParams();
        $this->_helper->layout->disableLayout();
        //$params['api']
        die();
    }

    public function genAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $params = $this->getRequest()->getParams();
        $this->_helper->layout->disableLayout();
        //$params['api']
        $cc = new Pmas_Model_Pubwho();
        $cc->generate();
        die();
    }

    public function notifAction() {
        echo '<h4>LAPORAN 2</h4>
			<em>Thu, 18 Aug 2016</em>			
			<p style="margin: 0px;">				
				Di Indonesia kelahiran PKI juga tidak dipicu oleh menentang agama , melainkan tujuannya.....
			</p>
			
			<a href="page_news_item.html" class="news-block-btn">
			Read more <i class="m-icon-swapright m-icon-black"></i>
			</a>
				';
        die();
    }

    public function postmediaAction() {
        if($_POST) {
            // Zend_Debug::dump($_POST); die();   
            $cc = new Domas_Model_Pmedia();
            $id = $cc->addmedia($_POST);
            if(!empty($_FILES['logo']['name'])) {
                //die("cc");
                //   Zend_Debug::dump($_FILES); die();
                $dest_dir = constant("APPLICATION_PATH"). "/../public/assets/pmas/images/media";
                $upload = new Zend_File_Transfer_Adapter_Http();
                $upload->setDestination($dest_dir);
                $files = $upload->getFileInfo();
                //Zend_Debug::dump($files); //die("xx");
                try {
                    $upload->receive();
                    //	$_POST['RAWFILES'] =$_FILES;
                    $url = "/assets/pmas/images/media/" . $files['logo']['name'];
                    //$_POST["logo_name"] = $url;
                    $cc->updated_main_media($id, $url);
                    //die("s");
                }
                catch(Zend_File_Transfer_Exception $e) {
                    $e->getMessage();
                    Zend_Debug::dump($e);
                    die();
                }
            }
            //  Zend_Debug::dump($_POST); die();   
            $this->_redirect("/domas/index/listmedia");
        }
        //Zend_Debug::dump($_POST);
        //Zend_Debug::dump($_FILES);
        die();
    }

    public function postdictionaryAction() {
        if($_POST) {
            //Zend_Debug::dump($_POST); die();
            $cc = new Domas_Model_Pdictionary();
            $id = $cc->add_dictionary($_POST);
        }
        $this->_redirect("/domas/index/listdictionary");
    }

    public function postpredictiveAction() {
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        $cc = new Domas_Model_Withsolariumtopic();
        $dd = new Domas_Model_Topics();
        $gg = new Domas_Model_General();
        // Zend_Debug::dump($gg); die();
        if($_POST) {
            $stat = array(0 => "meningkat",
                          1 => "menurun",
                          2 => "normal");
            $tema = $dd->get_tema($_POST['data'][0]['tema'] . 
                                  "," . 
                                  $_POST['data'][0]['topik']);
            // Zend_Debug::dump($tema);die();

            foreach($_POST['data'] as $k2 => $v2) {
                // Zend_Debug::dump($v2); die();
                // Zend_Debug::dump($_POST); die();
                if($v2['prediksi'] != "" || $v2['prediksi'] != NULL) {
                    $topic = $v2['prediksi'] . " " . $stat[$v2['nilai']];
                    // $cc->update_topic($v2['id_content'], $topic, $v2['nilai']);
                    //$cc->update_topic($v2['id_content'], $topic, $v2['nilai']);
                    $gg->insert_contetnt($v2);
                    // Zend_Debug::dump($v2);die();
                }
            }
        }
        $this->_redirect("/domas/home/trainingsetpredictive");
    }

    public function postapiAction() {
        if($_POST) {
            //Zend_Debug::dump($_POST); die();
            $cc = new Domas_Model_Pmedia();
            $id = $cc->addapi($_POST);
        }
        $this->_redirect("/domas/index/listtoken");
    }

    public function postalertAction() {
        //Zend_Debug::dump($_POST);die();
        if($_POST) {
            //Zend_Debug::dump($_POST); die();
            $cc = new Domas_Model_Pmedia();
            $id = $cc->addalert($_POST);
        }
        $this->_redirect("/domas/alertsytem/config");
    }

    public function delapiAction() {
        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($params);die();  
        //Zend_Debug::dump($_POST); die();
        $cc = new Domas_Model_Pmedia();
        $id = $cc->deleteapi($params);
        $this->_redirect("/domas/index/listtoken");
    }

    public function getAction() {
    }

    public function gettopikAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        $pdash = new Domas_Model_Pdash();
        $data = $pdash->get_topik_by_tema($params["id"], "");
        // Zend_Debug::dump($data);die();
        echo '<option value="">SEMUA TOPIK</option>';

        foreach($data as $k => $v) {
            echo '<option value="' . $v["tid"] . '" ' .($params["topik"] == $v["tid"] ? "selected" : ""). '>' . $v["name"] . '</option>';
        }
        die();
    }
}
