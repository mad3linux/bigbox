<?php

/**
* Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>,Jul 15, 2014
*
*/
// ini_set("display_errors", "on");

class Domas_AjaxdomasController extends Zend_Controller_Action {


	public function chartsentimentAction(){
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        if($params["cate"] != "") {
            $params["cat"] = $params["cate"];
        }
        $parameter = array();

        foreach($params as $k => $v) {
            if($k != "module" && $k != "controller" && $k != "action") {
                $parameter[$k] = $v;
            }
        }
        // Zend_Debug::dump($parameter);die();
        $s = new Domas_Model_Solranalis();
        $z = new Model_Cache();
        $cache = $z->cachefunc(4000);
        $id = $z->get_id("Analis_Model_Solr_get_sentimentx", $parameter);
        $data1 = $z->get_cache($cache, $id);
        // Zend_Debug::dump($params);die("s");
        // $data1 = false;
        if(!$data1) {
            $data1 = $s->get_sentimentx($parameter);
            // if($data!=NULL) {
            $cache->save($data1, $id, array('systembin'));
            // }
        }

        // die("s");

        $parameter["ano"] = 0;
        $parameter["gap"] = "+1DAY";


        $cache = $z->cachefunc(4000);
        $id = $z->get_id("Analis_Model_Solr_get_trendline", $parameter);
        $data2 = $z->get_cache($cache, $id);
        // Zend_Debug::dump($data);die("s");
        // $data2 = false;
        if(!$data2) {
            $data2 = $s->get_trendline($parameter);
            $cache->save($data2, $id, array('systembin'));
        }

        // Zend_Debug::dump($data1);//die();
        // Zend_Debug::dump($data2);//die();

        $data = array_merge($data1, $data2);
        // Zend_Debug::dump($data);die("sc");

        $chartData = array();
        $tmp = array();
        $i = 0;
        foreach ($data as $k => $v) {
            foreach ($v as $k1 => $v1) {                
                if($k1 % 2 == 0) {
                    $date = date("Y-m-d", strtotime($v1));
                }
                if($k1 % 2 == 1) {
                    $tmp[strtoupper($k)][$date] = $v1;
                    $i++;
                }
            }
        }

        // Zend_Debug::dump($tmp);die("s");

        $i = 0;
        foreach ($tmp["NEGATIF"] as $k => $v) {
            $chartData[$i]["DATE"] = $k;
            $chartData[$i]["NEGATIF"] = $v;
            $chartData[$i]["NETRAL"] = $tmp["NETRAL"][$k];
            $chartData[$i]["POSITIF"] = $tmp["POSITIF"][$k];
            $chartData[$i]["DIGITAL_MEDIA"] = $tmp["NEWS"][$k];
            $chartData[$i]["DOKUMEN"] = $tmp["LAPORAN"][$k];
            $chartData[$i]["SOCIAL_MEDIA"] = $tmp["MEDIA SOSIAL"][$k];
            // $chartData[$i]["date"] = $k;
            $i++;
        }

        // Zend_Debug::dump($chartData);die();

        $this->view->params = $params;
        $this->view->chartData = json_encode($chartData);
	}

    public function charttrendlineAction(){
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        Zend_Debug::dump($params);die();
        // $parameter = array(
        // "skey"=>$params["skey"],
        // "skeyexception"=>$params["skeyexception"],
        // "cat"=>$params["cat"],
        // "searchkey"=>$params["searchkey"],
        // "topik"=>$params["topik"],
        // "is_mediatype"=>$params["is_mediatype"],
        // "gap"=>$params["gap"],
        // "d1"=>$params["d1"],
        // "d2"=>$params["d2"],
        // "is_sentiment"=>$params["is_sentiment"],
        // );
        // Zend_Debug::dump($parameter);die();
        $s = new Domas_Model_Solranalis();
        $z = new Model_Cache();
        $cache = $z->cachefunc(4000);
        $id = $z->get_id("Analis_Model_Solr_get_trendline", $params);
        $data = $z->get_cache($cache, $id);
        // Zend_Debug::dump($data);die("s");
        // $data = false;
        if(!$data) {
            $data = $s->get_trendline($params);
            $cache->save($data, $id, array('systembin'));
        }
        // die("s");
        // Zend_Debug::dump($data);die("s");

        $chartData = array();
        $tmp = array();
        $i = 0;
        foreach ($data as $k => $v) {
            foreach ($v as $k1 => $v1) {                
                if($k1 % 2 == 0) {
                    $date = date("Y-m-d", strtotime($v1));
                }
                if($k1 % 2 == 1) {
                    $tmp[$k][$date] = $v1;
                    $i++;
                }
            }
        }

        // Zend_Debug::dump($tmp);//die();

        $tmp1 = array();
        foreach ($tmp as $k => $v) {
            $tmp1[strtoupper($k)] = $v;
        }

        // Zend_Debug::dump($tmp1);//die();

        $i = 0;
        foreach ($tmp1["NEWS"] as $k => $v) {
            $chartData[$i]["date"] = $k;
            $chartData[$i]["digital_media"] = $v;
            $chartData[$i]["dokumen"] = (int)$tmp1["LAPORAN"][$k];
            $chartData[$i]["social_media"] = (int)$tmp1["MEDIA SOSIAL"][$k];
            // $chartData[$i]["date"] = $k;
            $i++;
        }

        // Zend_Debug::dump($chartData);die();

        $this->view->params = $params;
        $this->view->chartData = json_encode($chartData);

    }









}