<?php

/**
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>,Jul 15, 2014
 *
 */
ini_set("display_errors", "off");

class Domas_HomeController extends Zend_Controller_Action {

    public function init() {
        
        /* Initialize action controller here */
    }

    public function indexAction() {
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);//die();
        $params["skey"] = $params["search"];
        if(!isset($params["media_type"])|| $params["media_type"] == "") {
            $params["media_type"] = "nasional";
        }
        $arr_mt = array("nasional" => 1,
                        "lokal" => 2,
                        "internasional" => 3,
                        "blog" => 4,
                       );
        $mt = null;
        $mt2 = null;
        if($params["media_type"] == "nasional") {
            $mt = "is_mediatype:1 OR is_mediatype:2";
        } else {
            $mt = "is_mediatype:" . $arr_mt[$params["media_type"]];
        }
        $this->view->headScript()->appendFile('http://maps.google.com/maps/api/js');
        $this->view->headScript()->appendFile('/assets/core/plugins/gmaps/gmaps.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/jqcloud/jqcloud-1.0.4.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jqcloud/jqcloud.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/highcharts4.0.4/js/highcharts.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/highcharts4.0.4/js/highcharts-3d.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/highcharts4.0.4/js/themes/dark-unica.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/highcharts4.0.4/js/modules/exporting.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $cc = new Domas_Model_Pdash();
        $dash = $cc->get_tags_pmas();
        //Zend_Debug::dump($dash);die();
        if(!isset($params["startdate"])) {
            $params["startdate"] = date("Y-m-d", strtotime('monday last week'));
        }
        if(!isset($params["enddate"])) {
            $params["enddate"] = date("Y-m-d");
        }
        $this->view->cat = $dash;
        $this->view->varams = $params;
    }

    public function timelineAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/css/components.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/horizontal-timeline/horozontal-timeline.min.js');
        $ff = new Domas_Model_Withsolr();
        $d1 = gmdate("Y-m-d\TH:i:s\Z", strtotime('monday last week'));
        $d2 = "NOW";
        if(isset($params["startdate"])&& $params["startdate"] != "" && isset($params["enddate"])&& $params["enddate"] != "") {
            $d1 = gmdate("Y-m-d\TH:i:s\Z", strtotime($params["startdate"]));
            $d2 = gmdate("Y-m-d\TH:i:s\Z", strtotime($params["enddate"])+ 1);
        }
        $tmp = $ff->get_by_rawurl("select?q=*%3A*&wt=json&rows=0&facet.range=timestamp&facet=true&facet.range.start=" . 
                                  $d1 . 
                                  "&facet.range.end=" . 
                                  $d2 . 
                                  "&facet.range.gap=%2B1DAY");
        // Zend_Debug::dump($data);die();
        $timeline = array();
        $i = 0;

        foreach($tmp["facet_counts"]["facet_ranges"]["timestamp"]["counts"] as $k => $v) {
            if($k % 2 == 0) {
                $timeline[$i] = date("Y-m-d", strtotime($v));
                $i ++;
            }
        }
        // Zend_Debug::dump($timeline);die();
        $this->view->timeline = $timeline;
    }

    public function trainingsetAction() {
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);//die();
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        $cc = new Domas_Model_Pdash();
        $dash = $cc->get_tags_pmas();
        // Zend_Debug::dump($dash);die();
        $this->view->varams = $params;
        $this->view->cat = $dash;
    }

    public function listpredictiveAction() {
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
        $params = $this->getRequest()->getParams();
        $this->view->varams = $params;
    }

    public function tablelistpredictiveAction() {
    }

    public function generatepredictivemodelAction() {
        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($connection); die();
        $zz = new Domas_Model_Top();
        $data = $zz->get_model();
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        try {

            foreach($data as $v) {
                $upload = APPLICATION_PATH . '/../public/predictive/' . $v['stype'] . "/";
                //echo $upload;
                $mode = 0755;
                if(!is_dir($upload)) {
                    mkdir($upload, 0777);
                }
                $lang_detect = new Domas_Model_Predictive();
                $ret = $lang_detect->train($v['stype'], $v['tema']);
                $lang_detect->saveToFile($ret, APPLICATION_PATH . 
                                         '/../public/predictive/' . 
                                         $v['stype'] . 
                                         "/" . 
                                         $v['tema']);
            }
            $result = array('retCode' => '11',
                            'retMsg' => "Generate Model's done",
                            'result' => true,
                            'data' =>$_POST);
        }
        catch(Exception $e) {
            $result = array('retCode' => '11',
                            'retMsg' =>$e->getMessage(),
                            'result' => false,
                            'data' =>$_POST);
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function trainingsetpredictiveAction() {
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/jquery.dataTables.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/data-tables/DT_bootstrap.js');
        $cc = new Domas_Model_Pdash();
        $cc2 = new Domas_Model_General();
        $dash = $cc->get_tags_pmas();
        $data3 = $cc2->get_isi_prediksi();
        // Zend_Debug::dump($cc);die();
        // Zend_Debug::dump($dash);die();
        $this->view->varams = $params;
        $this->view->cat = $dash;
        // Zend_Debug::dump($cat);die();
        $this->view->isi_prediksi = $data3;
    }

    public function select456Action() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        $pdash = new Domas_Model_Pdash();
        $data = $pdash->get_topik_by_tema($params["id"]);
        // Zend_Debug::dump($data);die();

        foreach($data as $k => $v) {
            echo '<option value="' . $v["tid"] . '" ' .($params["topik"] == $v["tid"] ? "selected" : ""). '>' . $v["name"] . '</option>';
        }
        die();
    }

    public function getkotaAction() {
    }

    public function generatesolrAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        $filter = null;
        if($params["topik"] != "") {
            $pdash = new Domas_Model_Pdash();
            $name_tema = $pdash->get_name($params["topik"]);

            foreach($name_tema as $k => $v) {
                $filter .= '"' . $v["name"] . '" OR ';
            }
        }
        $filter = rtrim($filter, " OR ");
        $filter = "content:" . $filter;
        // if($params["cat"]!=""){
        // $pdash = new Domas_Model_Pdash();
        // $name_topic = $pdash->get_name($params["cat"]);
        // $filter .= "' OR'".$name_topic;
        // }
        // Zend_Debug::dump('content:'.$filter);die();
        // Zend_Debug::dump($params["topic"]);die();
        $limit = 10;
        if(isset($params["quantity"])&& $params["quantity"]) {
            $limit = $params["quantity"];
        }
        // $topic = array();
        // if(isset($params["topik"])&&$params["topik"]!=""){
        // $cc = new Domas_Model_Pdash();
        // $topic = $cc->get_keys_pmas(533);
        // $topic = array($params["topik"],$params["cat"]);
        // $filter = "content:'".$name_topic;
        // }
        // Zend_Debug::dump($topic);die();
        $entity = - 1;
        if(isset($params["type"])&& $params["type"] != "") {
            $entity = 'entity_id:' . $params["type"];
        }
        // Zend_Debug::dump($params);
        // Zend_Debug::dump($start);
        // Zend_Debug::dump($limit);
        // Zend_Debug::dump($entity);
        // Zend_Debug::dump($filter);
        // Zend_Debug::dump("select?q=*%3A*&fq=".urlencode($entity)."&fq=".urlencode($filter)."&rows=".$limit."&wt=json&indent=true"); 
        // die();
        $n = new Domas_Model_Withsolr();
        $n2 = new Domas_Model_General();
        $data = $n->get_by_rawurl("select?q=*%3A*&fq=" . 
                                  urlencode($entity). 
                                            "&sort=timestamp+desc&fq=" . 
                                            urlencode($filter). 
                                                      "&rows=" . 
                                                      urlencode($limit). 
                                                                "&wt=json&indent=true");
        $data2 = $n2->get_dictionary($params['cat'], $params['topik'], $params['type']);
        // Zend_Debug::dump($data["response"]["docs"]);die();
        $this->view->varams = $params;
        $this->view->filter = $filter;
        $this->view->data = $data["response"]["docs"];
    }

    public function generatepredictiveAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        $filter = null;
        $keyword = null;
        if($params["keyword"] != "") {
            $keyword = "content:" . '"' . $params["keyword"] . '"';
        }
        // if ($params["filter"]) {
        // $filter = "content:".'"'.$params["filter"].'"';
        // }
        if($params["topik"] != "") {
            $pdash = new Domas_Model_Pdash();
            $name_tema = $pdash->get_name($params["topik"]);

            foreach($name_tema as $k => $v) {
                $filter .= '"' . $v["name"] . '" OR ';
            }
            $filter = rtrim($filter, " OR ");
            $filter = "content:" . $filter;
        }
        // var_dump($keyword); var_dump($filter);die();
        // if($params["cat"]!=""){
        // $pdash = new Domas_Model_Pdash();
        // $name_topic = $pdash->get_name($params["cat"]);
        // $filter .= "' OR'".$name_topic;
        // }
        $mulaidari = 0;
        if(isset($params["mulai"])&& $params["mulai"]) {
            $mulaidari = $params["mulai"];
        }
        // Zend_Debug::dump('content:'.$filter);die();
        // Zend_Debug::dump($params["topic"]);die();
        $limit = 10;
        if(isset($params["quantity"])&& $params["quantity"]) {
            $limit = $params["quantity"];
        }
        // $topic = array();
        // if(isset($params["topik"])&&$params["topik"]!=""){
        // $cc = new Domas_Model_Pdash();
        // $topic = $cc->get_keys_pmas(533);
        // $topic = array($params["topik"],$params["cat"]);
        // $filter = "content:'".$name_topic;
        // }
        // Zend_Debug::dump($topic);die();
        $entity = - 1;
        if(isset($params["type"])&& $params["type"] != "") {
            $entity = 'entity_id:' . $params["type"];
        }
        // Zend_Debug::dump($e_id);
        // Zend_Debug::dump($limit);
        // Zend_Debug::dump($entity);
        // Zend_Debug::dump($filter);
        // Zend_Debug::dump("select?q=*%3A*&fq=".urlencode($entity)."&sort=timestamp+desc&fq=".urlencode($keyword)."&fq=".urlencode($filter)."&rows=".urlencode($limit)."&start=".urlencode($mulaidari)."&wt=json&indent=true"); 
        // die('ihiuhuh');
        $n = new Domas_Model_Withsolrpmas();
        $n2 = new Domas_Model_General();
        $data = $n->get_by_rawurl("select?q=*%3A*&fq=" . 
                                  urlencode($entity). 
                                            "&sort=timestamp+desc&fq=" . 
                                            urlencode($keyword). 
                                                      "&fq=" . 
                                                      urlencode($filter). 
                                                                "&rows=" . 
                                                                urlencode($limit). 
                                                                          "&start=" . 
                                                                          urlencode($mulaidari). 
                                                                                    "&wt=json&indent=true");
        // Zend_Debug::dump($data);die();
        $data2 = $n2->get_prediksi($params);
        // Zend_Debug::dump($data);die();
        $this->view->prediksi = $data2;
        $this->view->varams = $params;
        $this->view->filter = $filter;
        $this->view->data = $data["response"]["docs"];
    }

    public function countAction() {
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
        $n = new Domas_Model_Withsolr();
        $str = "*:*";
        $str2 = 'content:"jangan biarkan" OR content:"diam saja" OR content:"dijajah china" OR (content:"PKI" AND content:"diatur") OR content:"dikendalikan" OR content:"drama politik" OR content:"adu domba" OR content:"kacamata kuda" OR content:"penista" OR content:"pembalasan"';
        $data_media = $n->get_by_rawurl("select?q=" . 
                                        urlencode($str). 
                                                  "&fq=" . 
                                                  urlencode($str2). 
                                                            "&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.gap=%2B1DAY&facet.range.start=2016-10-01T00:00:00Z&facet.range.end=2016-12-29T23:59:59Z&fq=is_mediatype:(1+OR+2)&fq=entity_id:2");
        $data_sosial = $n->get_by_rawurl("select?q=" . 
                                         urlencode($str). 
                                                   "&fq=" . 
                                                   urlencode($str2). 
                                                             "&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.gap=%2B1DAY&facet.range.start=2016-10-01T00:00:00Z&facet.range.end=2016-12-29T23:59:59Z&fq=entity_id:5");
        $tmp_media = $data_media["facet_counts"]["facet_ranges"]["content_date"]["counts"];
        $tmp_sosial = $data_sosial["facet_counts"]["facet_ranges"]["content_date"]["counts"];
        $i = 0;

        foreach($tmp_media as $k => $v) {
            if($k % 2 == 0) {
                $xx = $v;
            }
            if($k % 2 == 1) {
                $media[$i] = array($xx,
                                   (int)$v);
                $i ++;
            }
        }
        $i = 0;

        foreach($tmp_sosial as $k => $v) {
            if($k % 2 == 0) {
                $xx = $v;
            }
            if($k % 2 == 1) {
                $sosial[$i] = array($xx,
                                    (int)$v);
                $i ++;
            }
        }
        $this->view->media = $media;
        $this->view->sosial = $sosial;
    }
}
