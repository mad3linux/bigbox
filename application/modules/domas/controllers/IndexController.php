<?php

/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
// ini_set("display_errors", "On"); 

class Domas_IndexController extends Zend_Controller_Action {

    public function init() {
        
        /* Initialize action controller here */
    }

    public function testAction(){
        
        $cc= new Domas_Model_Pubsearch();
        $cc->pub_get_suggestion_solr("telk");

    	$text ='BANDUNG, (PR).- Wali Kota Bandung Ridwan Kamil memberangkatkan 9 bus mudik menuju sejumlah daerah di Jawa Barat di Pendopo di Bandung, Rabu, 21 Juni 2017. Kesembilan bus tersebut itu akan mengantarkan pemudik asal Kota Bandung menuju ke kota asalnya masing-masing. Kegiatan tersebut digagas oleh komunitas masyarakat, bekerja sama dengan sektor swasta dalam bentuk tanggung jawab sosial perusahaan. Sebagai wali kota, Ridwan Kamil mengaku sangat mendukung penuh kegiatan tersebut. "Kegiatan ini sangat baik dan patut diapresiasi. Tugas saya hanyalah memberi kemudahan," ungkap Ridwan saat memberi sambutan pada para pemudik. Ia menjelaskan, bus-bus tersebut akan secara simultan mengantarkan pemudik ke berbagai daerah di Jawa Barat. "Ada 9 bus rutin bolak balik," imbuhnya. Lebih lanjut kepada para pemudik, Ridwan menitipkan agar baik selama perjalanan maupun ketika tiba di kampung halaman agar menceritakan kebaikan-kebaikan di Kota Bandung. "Jadi mereka menyebarkan berita positif tentang Bandung, dengan timbal balik ya Anda kami mudikkan ke daerah masing-masing," katanya. Jangan ajak sanak saudara Selain itu, Ridwan juga berharap agar para pemudik itu tidak mengajak serta sanak saudaranya untuk turut tinggal di Bandung. "Kalau bisa tidak membawa saudaranya untuk pindah ke Bandung. Kecuali dengan dua syarat, ia punya kemampuan untuk bekerja, dan punya biaya sendiri untuk hidupnya," ujarnya. Ia berharap, setibanya mereka di kampung halaman, para pemudik bisa tetap produktif, berlaku positif, dan bisa membawa kebahagiaan bagi orang-orang di sekitarnya. "Karena ciri orang yang paling bahagia adalah yang memberi kebahagiaan bagi orang lain," ujarnya. ***' ;


    	// echo str_word_count(strtolower($str), 'ridwan kamil');die();

    	$mdlSolr = new Domas_Model_Solr();

    	$key = 'ridwan kamil';
    	$and = array('entity_id:2','content_date:[2017-06-20T00:00:00Z TO 2017-06-21T23:59:59Z]');
    	$excep = array('-content:"bisa"','-content:"ppp"');

    	$data = $mdlSolr->solrquery_select($key,$and,$excep);

    	Zend_Debug::dump($data);die();
    }

    public function dashAction() {

    	$this->_redirect("/domas/index/newhome");


        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($params); die();
    	if(isset($params["ajax"]) && $params["ajax"]==1){
    		$this->_helper->layout->disableLayout();

    		$mdlSolr = new Domas_Model_Solr();

            $enddate = date("Y-m-d");
            $startdate = date("Y-m-d", strtotime($enddate." -6 days"));

    		$p = array(
    			"startdate"=>$startdate,
    			"enddate"=>$enddate,
    		);



    		$entity = $mdlSolr->get_entity_desc();

    		$count = $mdlSolr->get_count_by_entity($p);
    		// Zend_Debug::dump($count);//die("S");

    		$sentiment = $mdlSolr->get_count_ranges_sentiment($p);
    		// Zend_Debug::dump($sentiment);die();

    		$result = array(
    			"params"=>$p,
    			"entity"=>$entity,
    			"count"=>$count,
    			"ranges"=>$sentiment,
			);

	        header("Access-Control-Allow-Origin: *");
	        header('Content-Type: application/json');
			echo json_encode($result);
    		die();
    	}
		
		
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        //-== JS
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/highcharts4.0.4/js/highcharts.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/highcharts4.0.4/js/highcharts-3d.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/highcharts4.0.4/js/themes/dark-unica.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/highcharts4.0.4/js/modules/exporting.js');
        $this->view->headScript()->appendFile('http://maps.google.com/maps/api/js?language=id');
        $this->view->headScript()->appendFile('/assets/core/plugins/gmaps/gmaps.js');
        $this->view->headScript()->appendFile('/assets/core/js/d3.v3.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
		
		
		$this->view->headScript()->appendFile('/assets/core/global/plugins/amcharts/amcharts/amcharts.js');
		$this->view->headScript()->appendFile('/assets/core/global/plugins/amcharts/amcharts/serial.js');
		
		
        $cc = new Domas_Model_Dash();
        $datax = $cc->get_a_dash();
		
		$tmp = array();
		foreach($datax["grouped"]["entity_id"]["groups"] as $k=>$v){
			$tmp[$v["groupValue"]] = $v["doclist"]["numFound"];	
		}
		
		$entity = array(
			"2"=>"Digital Media",
			"3"=>"Dokumen",
			"5"=>"Social Media",
		);
		
		$data = array();
		$i = 0;
		foreach($entity as $entity_id=>$entity_desc){
			$data[$entity_id] = ($tmp[$entity_id]!=""?(int)$tmp[$entity_id]:0);
		}
		
		// Zend_Debug::dump($data);die();
		
       // Zend_Debug::dump($datax); die();
        $this->view->datax = $data;
		
		
		$enddate = date("Y-m-d");
		$startdate = date("Y-m-d", strtotime($enddate." -6 days"));
		// Zend_Debug::dump($min7);die();
		
        if(!isset($params["startdate"])) {
            // $params["startdate"] = date("Y-m-d", strtotime('monday last week'));
            $params["startdate"] = $startdate;
        }
        if(!isset($params["enddate"])) {
            $params["enddate"] = $enddate;
        }
        $arr_map = array("lat" => - 1.131695,
                         "lng" => 119.503078,
                         "zoom" => 5,
                        );
        if(isset($params["media_type"])&& $params["media_type"] == "internasional") {
            $arr_map = array("lat" => 26.4578763,
                             "lng" => 32.0579421,
                             "zoom" => 1,
                            );
        }
        // Zend_Debug::dump($arr_map);//die();
        $as = new Domas_Model_General();
        //koneksi ke oracle
        // $data_alertfr = $as->alertfr();  
        $data = array();
        $cc = new Domas_Model_Pdash();
        $dd = new Model_Cache();
        $cache = $dd->cachefunc(3000);
        $id = $dd->get_id("Domas_Model_Pdash_get_tags_pmas");
        $dash = $dd->get_cache($cache, $id);
        if(!$dash) {
            $dash = $cc->get_tags_pmas();
            $cache->save($dash, $id, array('systembin'));
        }
        //Zend_Debug::dump($dash);die();
        $this->view->cat = $dash;
        $this->view->varams = $params;
        $this->view->map = $arr_map;
        // $this->view->alertfr = $data_alertfr;
    }

    public function newhomeAction(){

    	// die("s");

        $this->view->headScript()->appendFile('/assets/core/global/plugins/counterup/jquery.waypoints.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/counterup/jquery.counterup.min.js');
		$this->view->headScript()->appendFile('/assets/core/global/plugins/amcharts/amcharts/amcharts.js');
		$this->view->headScript()->appendFile('/assets/core/global/plugins/amcharts/amcharts/serial.js');


		$z = new Model_Cache();
		$mdlSolr = new Domas_Model_Solr();

        $enddate = date("Y-m-d");
        $startdate = date("Y-m-d", strtotime($enddate." -30 days"));

		$p = array(
			"startdate"=>$startdate,
			"enddate"=>$enddate,
		);
          $this->view->start= $startdate;
        $this->view->end= $enddate;
        $cache = $z->cachefunc(3000);
        $id = $z->get_id("Domas_Solr_HomeDashboard", $p);
        $data = $z->get_cache($cache, $id);
        $data = false;
        if(!$data) {
        	// die("s");
			$entity = $mdlSolr->get_entity_desc();

			$count = $mdlSolr->get_count_by_entity($p);
			// Zend_Debug::dump($count);//die("S");

			$sentiment = $mdlSolr->get_count_ranges_sentiment($p);
			// Zend_Debug::dump($sentiment);die();
            
			$data = array(
				"params"=>$p,
				"entity"=>$entity,
				"count"=>$count,
				"ranges"=>$sentiment,
			);
           // Zend_Debug::dump($data); //die();
            $cache->save($data, $id, array('systembin'));
        }

		// Zend_Debug::dump($data);//die();

		$this->view->data = $data;
    }

    public function discovery2Action(){
    	$params = $this->getRequest()->getParams();
		// Zend_Debug::dump($params);die();
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        //-== JS
        $this->view->headScript()->appendFile('/assets/core/global/plugins/echarts/echarts.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript()->appendFile('http://maps.google.com/maps/api/js?language=id');
        $this->view->headScript()->appendFile('/assets/core/plugins/gmaps/gmaps.js');
        $this->view->headScript()->appendFile('/assets/core/js/d3.v3.min.js');
		
        $n = new Domas_Model_Withsolrpmas();

		$data = array();
		$jsdata = array();
		if($params['keysearch']!=null & $params['keysearch']!=""){
			$tmpa = explode(",",$params['keysearch']);
			$params['key'] = $tmpa;
			foreach($tmpa as $k=>$v){
				//entity
				// Zend_Debug::dump($data);//die();
				$data[$v]['tag'] = "key".$k;
				$data[$v]['entity'] = array();
				$jsdata[$v]['entity'] = array();
				$start = 0;
				$offset = 100;
				$tmp = array();
				while(true){
					$and = "&fq=content%3A".$v;
					if($params['andsearch']!=null & $params['andsearch']!=""){
						$tmpb = explode(",",$params['andsearch']);
						foreach($tmpb as $vb){
							$and .= "&fq=content%3A".$vb;
						}
					}
					if($params['exsearch']!=null & $params['exsearch']!=""){
						$tmpb = explode(",",$params['exsearch']);
						foreach($tmpb as $vb){
							$and .= "&fq=-content%3A".$vb;
						}
					}
					$and .= "&fq=content_date:[".$params['startdate']."T00:00:00.000Z%20TO%20".$params['enddate']."T23:59:59.999Z]";
					$query = "select?q=*%3A*".$and."&sort=entity_type+asc&start=".$start."&rows=".($start+$offset)."&fl=entity_type&wt=json";
					$query .= "&indent=true&group=true&group.field=entity_type";
					$ent = $n->get_by_rawurl($query);
					// Zend_Debug::dump($ent);die($query);
					if(isset($ent['grouped']['entity_type']['groups']) & count($ent['grouped']['entity_type']['groups'])>0){
						foreach($ent['grouped']['entity_type']['groups'] as $v2){
							if($v2['groupValue']!=null){
								$tmp[$v2['groupValue']] = array(
									'name'=> strtoupper($v2['groupValue']),
									'val' => $v2['doclist']["numFound"]
								);
								$jsdata[$v]['entity'][] = array(
									'value'=>$v2['doclist']["numFound"],
									'name'=>$v2['groupValue'],
									'itemStyle'=>array(
										'normal'=>array(
											'label'=>array(
												'textStyle'=>array(
													'color'=>'black'
												)
											)
										)
									)
								);
							}
						}
					}
					if(count($ent['grouped']['entity_type']['groups'])==$offset){
						$start += $offset;
					}else{
						break;
					}
				}
				// Zend_Debug::dump($tmp);//die();
				$data[$v]['entity'] = $tmp;
				// Zend_Debug::dump($data);
				
				//bundle
				// Zend_Debug::dump($data);//die();
				$data[$v]['bundle'] = array();
				$jsdata[$v]['bundle'] = array();
				$start = 0;
				$offset = 100;
				$tmp = array();
				while(true){
					$and = "&fq=content%3A".$v;
					if($params['andsearch']!=null & $params['andsearch']!=""){
						$tmpb = explode(",",$params['andsearch']);
						foreach($tmpb as $vb){
							$and .= "&fq=content%3A".$vb;
						}
					}
					if($params['exsearch']!=null & $params['exsearch']!=""){
						$tmpb = explode(",",$params['exsearch']);
						foreach($tmpb as $vb){
							$and .= "&fq=-content%3A".$vb;
						}
					}
					$and .= "&fq=content_date:[".$params['startdate']."T00:00:00.000Z%20TO%20".$params['enddate']."T23:59:59.999Z]";
					$query = "select?q=*%3A*".$and."&sort=bundle+asc&start=".$start."&rows=".($start+$offset)."&fl=bundle&wt=json";
					$query .= "&indent=true&group=true&group.field=bundle";
					$ent = $n->get_by_rawurl($query);
					// Zend_Debug::dump($ent);die($query);
					if(isset($ent['grouped']['bundle']['groups']) & count($ent['grouped']['bundle']['groups'])>0){
						foreach($ent['grouped']['bundle']['groups'] as $v2){
							if($v2['groupValue']!=null){
								$tmp[$v2['groupValue']] = array(
									'name'=> strtoupper($v2['groupValue']),
									'val' => $v2['doclist']["numFound"]
								);
								$jsdata[$v]['bundle'][] = array(
									'value'=>$v2['doclist']["numFound"],
									'name'=>$v2['groupValue'],
									'itemStyle'=>array(
										'normal'=>array(
											'label'=>array(
												'textStyle'=>array(
													'color'=>'black'
												)
											),
											'labelLine'=>array(
												'length'=>3
											)
										)
									)
								);
							}
						}
					}
					if(count($ent['grouped']['bundle']['groups'])==$offset){
						$start += $offset;
					}else{
						break;
					}
				}
				// Zend_Debug::dump($tmp);//die();
				$data[$v]['bundle'] = $tmp;
				// Zend_Debug::dump($data);
				
				//sentiment
				// Zend_Debug::dump($data);//die();
				$data[$v]['sentiment'] = array();
				$jsdata[$v]['sentiment'] = array();
				$start = 0;
				$offset = 100;
				$tmp = array();
				while(true){
					$and = "&fq=content%3A".$v;
					if($params['andsearch']!=null & $params['andsearch']!=""){
						$tmpb = explode(",",$params['andsearch']);
						foreach($tmpb as $vb){
							$and .= "&fq=content%3A".$vb;
						}
					}
					if($params['exsearch']!=null & $params['exsearch']!=""){
						$tmpb = explode(",",$params['exsearch']);
						foreach($tmpb as $vb){
							$and .= "&fq=-content%3A".$vb;
						}
					}
					$and .= "&fq=content_date:[".$params['startdate']."T00:00:00.000Z%20TO%20".$params['enddate']."T23:59:59.999Z]";
					$query = "select?q=*%3A*".$and."&sort=is_sentiment+asc&start=".$start."&rows=".($start+$offset)."&fl=is_sentiment&wt=json";
					$query .= "&indent=true&group=true&group.field=is_sentiment";
					$ent = $n->get_by_rawurl($query);
					// Zend_Debug::dump($ent);die($query);
					if(isset($ent['grouped']['is_sentiment']['groups']) & count($ent['grouped']['is_sentiment']['groups'])>0){
						foreach($ent['grouped']['is_sentiment']['groups'] as $v2){
							if($v2['groupValue']!=null){
								$tmp[$v2['groupValue']] = array(
									'name'=> strtoupper($v2['groupValue']),
									'val' => $v2['doclist']["numFound"]
								);
								$jsdata[$v]['sentiment'][] = array(
									'value'=>$v2['doclist']["numFound"],
									'name'=>($v2['groupValue']===1)?"negative":(($v2['groupValue']===2)?"positive":"netral"),
									// 'name'=>$v2['groupValue'],
									'itemStyle'=>array(
										'normal'=>array(
											'label'=>array(
												'textStyle'=>array(
													'color'=>'black'
												)
											),
											'labelLine'=>array(
												'length'=>3
											)
										)
									)
								);
							}
						}
					}
					if(count($ent['grouped']['is_sentiment']['groups'])==$offset){
						$start += $offset;
					}else{
						break;
					}
				}
				// Zend_Debug::dump($tmp);//die();
				$data[$v]['sentiment'] = $tmp;
				// Zend_Debug::dump($data);
			}
			// Zend_Debug::dump($data);die();
			// Zend_Debug::dump($jsdata);die();
		}
        if(!isset($params["startdate"])) {
            $params["startdate"] = date("Y-m-d", strtotime('yesterday'));
        }
        if(!isset($params["enddate"])) {
            $params["enddate"] = date("Y-m-d");
        }

        $this->view->params = $params;
        $this->view->data = $data;
        $this->view->jsdata = $jsdata;
	}

    public function discoveryAction(){

		// date_default_timezone_set('Asia/Jakarta');

    	$params = $this->getRequest()->getParams();
		// Zend_Debug::dump($_POST);die();
        
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        //-== JS
        $this->view->headScript()->appendFile('/assets/core/global/plugins/echarts/echarts.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        // $this->view->headScript()->appendFile('http://maps.google.com/maps/api/js?language=id');
        // $this->view->headScript()->appendFile('/assets/core/plugins/gmaps/gmaps.js');
        // $this->view->headScript()->appendFile('/assets/core/js/d3.v3.min.js');


        $g = new Domas_Model_Pdash();
        $n = new Domas_Model_Withsolrpmas();

        $datasource = array(
        	"2"=>"DIGITAL MEDIA",
        	"3"=>"DOCUMENT",
        	"4"=>"MESSAGING",
        	"5"=>"SOCIAL MEDIA TWITTER",
        	"6"=>"OTHERS",
    	);
        $sentiment = array(
        	"netral",
        	"negatif",
        	"positif",
    	);

    	$andsearch = array();
    	if($params["andsearch"]!="" && $params["andsearch"]!=null){
    		$andsearch = explode(",", $params["andsearch"]);
    	}
        	
    	if($params["exsearch"]!="" && $params["exsearch"]!=null){
        	$excsearch = explode(",", $params["exsearch"]);
        	// Zend_Debug::dump($keysearch);die();
        	$exception = array();
        	foreach($excsearch as $k=>$v){
        		$vv = explode("<>",$v);
        		// Zend_Debug::dump($vv);die();
        		$exception[$vv[0]][] = $vv[1];
        	}
        }
        	// Zend_Debug::dump($exception);die();

        if($params["keysearch"]!="" &&$params["keysearch"]!=null){
        	$keysearch = explode(",", $params["keysearch"]);
			$andsearch = array();
			if($params["andsearch"]!=""){				
				$andsearch = explode(",", $params["andsearch"]);
			}
			$exsearch = array();
			if($params["exsearch"]!=""){				
				$exsearch = explode(",", $params["exsearch"]);
			}

        	$data = array();        	
        	$jsdata = array();
        	foreach($datasource as $entity_id=>$entity_name){   	
        		// $jsdata[$entity_id] = array();
        		$i = 0;
        		foreach ($keysearch as $k => $key) {

        			$and = "";
        			$and .= "&fq=content:%5B*+TO+*%5D";
        			$and .= "&fq=content_date%3A%5B".$params["startdate"]."T00%3A00%3A00Z+TO+".$params["enddate"]."T23%3A59%3A59Z%5D";
        			$and .= "&fq=entity_id%3A".$entity_id;
        			$and .= '&fq=content%3A%22'.urlencode($key).'%22';
        			$and .= '&fq=is_mediatype%3A1+OR+2';
					if(count($andsearch)>0){
						foreach($andsearch as $x=>$y){
							$and .='&fq=content%3A"'.urlencode($y).'"';
						}
					}
					if(count($exsearch)>0){
						foreach($exsearch as $x=>$y){
							$and .='&fq=-content%3A%22'.urlencode($y).'%22';
						}
					}

        			$query = "select?q=*%3A*".$and."&rows=0&wt=json&indent=true&facet=true";
        			$query2 = "select?q=*%3A*".$and."&rows=10&wt=json&indent=true";
        			// echo $query;
					// echo "<br>";
					// echo urldecode($query);
					// die();

        			$tmp = $n->get_by_rawurl($query);
        			$tmp2 = $n->get_by_rawurl($query2);
        			// Zend_Debug::dump($tmp2);die();
        			if($tmp["response"]["numFound"]>0){
        				$data[$entity_id]["entity"][$key]["count"] = $tmp2["response"]["numFound"];
        				$data[$entity_id]["entity"][$key]["data"] = $tmp2["response"]["docs"];
        				$jsdata[$entity_id]["key"][$i]["value"] = (int)$tmp["response"]["numFound"];
        				$jsdata[$entity_id]["key"][$i]["name"] = $key;
        				$i++;
        			}
        		}

        		// Zend_Debug::dump($entity_id);//die();
        		// Zend_Debug::dump($data);die();

        		// $jsdata[$entity_id]["sentiment"] = array();
    			$i = 0;
        		foreach ($sentiment as $sentiment_id => $sentiment_name) {

	    			$and = "";
        			$and .= "&fq=content%3A%5B*+TO+*%5D";
	    			$and .= "&fq=content_date%3A%5B".$params["startdate"]."T00%3A00%3A00Z+TO+".$params["enddate"]."T23%3A59%3A59Z%5D";
	    			$and .= "&fq=entity_id%3A".$entity_id;
					$and .= "&fq=content%3A%22".urlencode(implode('" OR "',$keysearch))."%22";
	    			$and .= "&fq=is_sentiment%3A".$sentiment_id;
        			$and .= '&fq=is_mediatype%3A1+OR+2';
					if(count($andsearch)>0){
						foreach($andsearch as $x=>$y){
							$and .='&fq=content%3A%22'.urlencode($y).'%22';
						}
					}
					if(count($exsearch)>0){
						foreach($exsearch as $x=>$y){
							$and .='&fq=-content%3A%22'.urlencode($y).'%22';
						}
					}

	    			$query = "select?q=*%3A*".$and."&rows=0&wt=json&indent=true&facet=true";
	    			$query2 = "select?q=*%3A*".$and."&rows=10&wt=json&indent=true";

	    			$tmp = $n->get_by_rawurl($query);
	    			$tmp2 = $n->get_by_rawurl($query2);
	    			// Zend_Debug::dump($tmp);die();

	    			if($tmp["response"]["numFound"]>0){
        				$data[$entity_id]["sentiment"][$sentiment_name]["count"] = $tmp2["response"]["numFound"];
        				$data[$entity_id]["sentiment"][$sentiment_name]["data"] = $tmp2["response"]["docs"];
	    				$jsdata[$entity_id]["sentiment"][$i]["value"] = (int)$tmp["response"]["numFound"];
	    				$jsdata[$entity_id]["sentiment"][$i]["name"] = $sentiment_name;
	    				$jsdata[$entity_id]["sentiment"][$i]["color"] = "#000000";
	    				$i++;
	    			}
        		}
    			// $i = 0;
        		// foreach ($sentiment as $sentiment_id => $sentiment_name) {

    			$and = "";
				$and .= "&fq=content%3A%5B*+TO+*%5D";
    			$and .= "&fq=content_date%3A%5B".$params["startdate"]."T00%3A00%3A00Z+TO+".$params["enddate"]."T23%3A59%3A59Z%5D";
    			$and .= "&fq=entity_id%3A".$entity_id;
    			$and .= "&fq=content%3A%22".urlencode(implode('" OR "',$keysearch))."%22";
				$and .= '&fq=is_mediatype%3A1+OR+2';
				if(count($andsearch)>0){
					foreach($andsearch as $x=>$y){
						$and .='&fq=content%3A%22'.urlencode($y).'%22';
					}
				}
				if(count($exsearch)>0){
					foreach($exsearch as $x=>$y){
						$and .='&fq=-content%3A%22'.urlencode($y).'%22';
					}
				}

    			$query = "select?q=*%3A*".$and."&rows=0&wt=json&indent=true&facet=true&facet.field=bundle";
				// Zend_Debug::dump(urldecode($query));die();

    			$tmp = $n->get_by_rawurl($query);
    			// Zend_Debug::dump($tmp);die();

    			$tmp1 = array();
        		// $jsdata[$entity_id]["bundle"] = array();
    			$i = 0;
    			foreach($tmp["facet_counts"]["facet_fields"]["bundle"] as $k=>$v){
    				if($k%2==1 && $v>0){
    					$jsdata[$entity_id]["bundle"][$i]["value"] = $v;
    					$jsdata[$entity_id]["bundle"][$i]["name"] = $tmp["facet_counts"]["facet_fields"]["bundle"][$k-1];
    					$data[$entity_id]["bundle"][$tmp["facet_counts"]["facet_fields"]["bundle"][$k-1]] = $v;
    					$i++;
    				}

    			}

        	}

        	// Zend_Debug::dump($jsdata[2]["sentiment"]);die();
        }

        if(!isset($params["startdate"])) {
            $params["startdate"] = date("Y-m-d", strtotime('yesterday'));
        }
        if(!isset($params["enddate"])) {
            $params["enddate"] = date("Y-m-d");
        }
    	
    	// Zend_Debug::dump($jsdata);die();
        
        $this->view->params = $params;
        $this->view->datasource = $datasource;
        $this->view->data = $data;
        $this->view->jsdata = $jsdata;
        $this->view->logo = $g->get_logo();
	}

    public function wordcloudAction(){

		// date_default_timezone_set('Asia/Jakarta');

    	$params = $this->getRequest()->getParams();
		// Zend_Debug::dump($_POST);//die();

		$wcdata = array();
		$wc2data = array();
		if(isset($params["keysearch"]) && $params["keysearch"]!=""){
			// Zend_Debug::dump($params);//die();

			$keysearch = array_map('trim',explode(",",$params["keysearch"]));
			// Zend_Debug::dump($keysearch);//die();

			$dd = new Model_Cache();
			$mdlSolr = new Domas_Model_Solr();

			// $exclude_words = $mdlSolr->get_katadasar(array("tipe_katadasar"=>"adverbia,konjungsi,preposisi,pronomina,lain-lain"));
			
			// Zend_Debug::dump($exclude_words);die();
			
			$p = array();
			
			$cache = $dd->cachefunc(3000);
			$id = $dd->get_id("Domas_Model_Solr_get_katadasar",$p);
			$exclude_words = $dd->get_cache($cache, $id);
			if(!$dash) {
				$exclude_words = $mdlSolr->get_katadasar();
				$cache->save($exclude_words, $id, array('systembin'));
			}

			foreach($keysearch as $k=>$v){

				// Zend_Debug::dump($v);

				$start = 0;
				$rows = 0;
				$tmpx = $mdlSolr->get_count_keysearch($v,$params,$start,$rows);

				$rows = $tmpx->numFound;
				$tmp = $tmp = $mdlSolr->get_count_keysearch($v,$params,$start,$rows);
				// Zend_Debug::dump($tmp);die();

				$wcdata[$k]["name"] = $v;
				$wcdata[$k]["value"] = $tmp->numFound;

				$words = array();
				$content = "";
				foreach($tmp->docs as $k1=>$v1){
					$content .= preg_replace("/[^a-zA-Z0-9 ]/", '', strtolower($v1["content"]))." ";
				}

				// Zend_Debug::dump($content);die();
				$words = array_count_values(str_word_count($content, 1));
				arsort($words);

				$tmp = $words;
				$words = array();
				foreach ($tmp as $k1 => $v1) {
					if($k1!="" && !in_array(strtolower($k1), $exclude_words) && !is_numeric($k1) && $v1>1){
						$words[$k1] = $v1;
					}					
				}

				// $words = array_slice($words,0,100);				
				// Zend_Debug::dump($words);die("s");

				$i=0;
				foreach($words as $k2=>$v2){

					// if($i<100 && $v2>1){
						$wc2data[$v][$i]["name"] = $k2;
						$wc2data[$v][$i]["value"] = $v2;

						$i++;						
					// }

				}
				// Zend_Debug::dump($tmp);die("s");
			}
			// Zend_Debug::dump($words);die("s");
			// Zend_Debug::dump($wc2data);die();

		}
        
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        //-== JS
        // $this->view->headScript()->appendFile('/assets/core/global/plugins/echarts/echarts.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/echarts/echarts.simple.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/echarts/wordcloud/dist/echarts-wordcloud.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');



        if(!isset($params["startdate"])) {
            $params["startdate"] = date("Y-m-d", strtotime('yesterday'));
        }
        if(!isset($params["enddate"])) {
            $params["enddate"] = date("Y-m-d");
        }

        $this->view->params = $params;
        $this->view->wcdata = $wcdata;
        $this->view->wc2data = $wc2data;
	}
    public function wordcloud2Action(){

		// date_default_timezone_set('Asia/Jakarta');

    	$params = $this->getRequest()->getParams();
		// Zend_Debug::dump($_POST);//die();

		$wcdata = array();
		$wc2data = array();
		if(isset($params["keysearch"]) && $params["keysearch"]!=""){
			// Zend_Debug::dump($params);//die();

			$keysearch = array_map('trim',explode(",",$params["keysearch"]));
			// Zend_Debug::dump($keysearch);//die();

			$dd = new Model_Cache();
			$mdlSolr = new Domas_Model_Solr();
			
			$include_words = $mdlSolr->get_words();
			// Zend_Debug::dump($include_words);die();

			foreach($keysearch as $k=>$v){

				// Zend_Debug::dump($v);

				$start = 0;
				$rows = 0;
				$tmpx = $mdlSolr->get_count_keysearch($v,$params,$start,$rows);

				$rows = $tmpx->numFound;
				$tmp = $tmp = $mdlSolr->get_count_keysearch($v,$params,$start,$rows);
				// Zend_Debug::dump($tmp);die();

				$wcdata[$k]["name"] = $v;
				$wcdata[$k]["value"] = $tmp->numFound;

				$words = array();
				$content = "";
				foreach($tmp->docs as $k1=>$v1){
					$content .= preg_replace("/[^a-zA-Z0-9 ]/", '', strtolower($v1["content"]))." ";
				}

				// Zend_Debug::dump($content);die();
				$words = array_count_values(str_word_count($content, 1));
				arsort($words);

				// Zend_Debug::dump($words);die();

				$tmp = $words;
				$words = array();
				foreach ($tmp as $k1 => $v1) {
					if($k1!="" && in_array(strtolower($k1), $include_words) && !is_numeric($k1) && $v1>1){
						$words[ucwords($k1)] = $v1;
					}					
				}

				// $words = array_slice($words,0,100);				
				// Zend_Debug::dump($words);die("s");

				$i=0;
				foreach($words as $k2=>$v2){

					// if($i<100 && $v2>1){
						$wc2data[$v][$i]["name"] = $k2;
						$wc2data[$v][$i]["value"] = $v2;

						$i++;						
					// }

				}
				// Zend_Debug::dump($tmp);die("s");
			}
			// Zend_Debug::dump($words);die("s");
			// Zend_Debug::dump($wc2data);die();

		}
        
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        //-== JS
        // $this->view->headScript()->appendFile('/assets/core/global/plugins/echarts/echarts.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/echarts/echarts.simple.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/echarts/wordcloud/dist/echarts-wordcloud.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');



        if(!isset($params["startdate"])) {
            $params["startdate"] = date("Y-m-d", strtotime('yesterday'));
        }
        if(!isset($params["enddate"])) {
            $params["enddate"] = date("Y-m-d");
        }

        $this->view->params = $params;
        $this->view->wcdata = $wcdata;
        $this->view->wc2data = $wc2data;
	}

	public function getdiscoverylistAction(){

		$this->_helper->layout->disableLayout();
    	$params = $this->getRequest()->getParams();
		// Zend_Debug::dump($params);die();


    	$params["start"] = 0;
    	$params["nextpage"] = 0;
		if(isset($params["page"]) && $params["page"]!=""){
			$params["start"] = (int) $params['page'] * 10;
			$params["nextpage"] = (int) $params['page'] + 1;
			// die("s");
		}

        $g = new Domas_Model_Pdash();
        $n = new Domas_Model_Withsolrpmas();


    	$entity_id = $params["entity"];
    	// Zend_Debug::dump($entity_id);

    	$sentiment_id = $params["sentiment"];
    	// Zend_Debug::dump($sentiment_id);
		$andsearch = array();
		if($params["andsearch"]!=""){				
			$andsearch = explode(",", $params["andsearch"]);
		}
		$exsearch = array();
		if($params["exsearch"]!=""){				
			$exsearch = explode(",", $params["exsearch"]);
		}
		
		// Zend_Debug::dump($andsearch);
		// Zend_Debug::dump($exsearch);
		// die();
		

		$and = "";
		$and .= "&fq=content%3A[*+TO+*]";
		$and .= "&fq=content_date%3A%5B".$params["startdate"]."T00%3A00%3A00Z+TO+".$params["enddate"]."T23%3A59%3A59Z%5D";
		$and .= "&fq=entity_id%3A".$entity_id;
		$and .= "&fq=is_sentiment%3A".$sentiment_id;
		$and .= '&fq=is_mediatype%3A1+OR+2';
		if(count($andsearch)>0){
			foreach($andsearch as $x=>$y){
				$and .='&fq=content%3A%22'.urlencode($y).'%22';
			}
		}
		if(count($exsearch)>0){
			foreach($exsearch as $x=>$y){
				$and .='&fq=-content%3A%22'.urlencode($y).'%22';
			}
		}
		if($params["series"]=="key"){
			$keysearch = explode(",", $params["name"]);
			// Zend_Debug::dump($keysearch);//die();
			$and .= '&fq=content%3A%22'.urlencode(implode('" OR "',$keysearch)).'%22';
		} else if($params["series"]=="bundle"){
			
			$keysearch = explode(",", $params["keysearch"]);
			$and .= '&fq=content%3A%22'.urlencode(implode('" OR "',$keysearch)).'%22';
			$and .= '&fq=bundle%3A'.$params["name"];
		} else if($params["series"]=="sentiment"){
			
			$sentiment = array("netral"=>0,"negatif"=>1,"positif"=>2);
			
			$keysearch = explode(",", $params["keysearch"]);
			$and .= '&fq=content%3A%22'.urlencode(implode('" OR "',$keysearch)).'%22';
			$and .= '&fq=is_sentiment%3A'.$sentiment[$params["name"]];
		} else if($params["series"]=="words"){
			$keysearch = explode(",", $params["keysearch"]);
			$and .= '&fq=content%3A%22'.urlencode(implode('" OR "',$keysearch)).'%22';
			$and .= '&fq=content:"'.$params["name"].'"';
			$and .= '&fq=is_sentiment%3A'.$params["sentiment"];
		}
		
		// echo $and;die();
		// echo urldecode($and);die();

		$query = "select?q=*%3A*".$and."&start=".$params["start"]."&rows=10&wt=json&indent=true";
		// echo urldecode($query);die();

		$tmp = $n->get_by_rawurl($query);
		// Zend_Debug::dump($tmp);die();

		if($tmp["response"]["numFound"]>0){
			$data[$entity_id]["sentiment"][$sentiment_id][0]["count"] = $tmp["response"]["numFound"];
			$data[$entity_id]["sentiment"][$sentiment_id][0]["data"] = $tmp["response"]["docs"];
			// $i++;
		}

		// Zend_Debug::dump($tmp["response"]["numFound"]);//die();
		// Zend_Debug::dump($tmp["response"]["start"]);//die();

		$params["next"] = false;
		if((int)$tmp["response"]["numFound"]>10 && ((int)$tmp["response"]["numFound"]-(int)$tmp["response"]["start"])>10){
			$params["next"] = true;
		}

		// Zend_Debug::dump($data);die();

		$this->view->varams = $params;
		$this->view->data = $data;
        $this->view->logo = $g->get_logo();
	}

	public function continuereadingAction(){
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
		
		$pattern = array (
			'/Mon[^day]/','/Tue[^sday]/','/Wed[^nesday]/','/Thu[^rsday]/',
			'/Fri[^day]/','/Sat[^urday]/','/Sun[^day]/','/Monday/','/Tuesday/',
			'/Wednesday/','/Thursday/','/Friday/','/Saturday/','/Sunday/',
			'/Jan[^uary]/','/Feb[^ruary]/','/Mar[^ch]/','/Apr[^il]/','/May/',
			'/Jun[^e]/','/Jul[^y]/','/Aug[^ust]/','/Sep[^tember]/','/Oct[^ober]/',
			'/Nov[^ember]/','/Dec[^ember]/','/January/','/February/','/March/',
			'/April/','/June/','/July/','/August/','/September/','/October/',
			'/November/','/December/',
		);

		$replace = array ( 'Sen','Sel','Rab','Kam','Jum','Sab','Min',
			'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu',
			'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Okt','Nov','Des',
			'Januari','Februari','Maret','April','Juni','Juli','Agustus','Sepember',
			'Oktober','November','Desember',
		);
		
		
		
        $n = new Domas_Model_Withsolrpmas();
		
		$and = "&fq=id%3A%22".$_POST["id"]."%22";
		
		$query = "select?q=*%3A*".$and."&start=0&rows=1&wt=json&indent=true";
		// echo urldecode($query);

		$tmp = $n->get_by_rawurl($query);
		// Zend_Debug::dump($tmp);die();
		$data = $tmp["response"]["docs"][0];
		// Zend_Debug::dump($data);//die();
		
		$timestamp = strtotime($data["content_date"]);
		$content_date = date("l, j F Y | H:i", $timestamp);
		$datetime = preg_replace ($pattern, $replace, $content_date);
		
		$html = '';
		
		$html .='<h3>'.$data["title"].'</h3>';
		$html .='<h4>'.$datetime.'</h4>';
		$html .= '<p>'.$data["content"].'</p>';

		echo $html;
		die();		
	}
    public function homeAction() {
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);//die();
        $params["skey"] = $params["search"];
        if(!isset($params["media_type"])|| $params["media_type"] == "") {
            $params["media_type"] = "nasional";
        }
        $arr_mt = array("nasional" => 1,
                        "lokal" => 2,
                        "internasional" => 3,
                        "blog" => 4,
                       );
        $mt = "media_type:" . $arr_mt[$params["media_type"]];
        // $this->view->headScript()->appendFile('http://maps.google.com/maps/api/js');
        $this->view->headScript()->appendFile('/assets/core/plugins/gmaps/gmaps.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jqcloud/jqcloud-1.0.4.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jqcloud/jqcloud.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/highcharts4.0.4/js/highcharts.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/highcharts4.0.4/js/themes/dark-unica.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/highcharts4.0.4/js/modules/exporting.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $cc = new Domas_Model_Pdash();
        $n = new Domas_Model_Withsolrpmasdash();
        $dash = $cc->get_tags_pmas();
        $gettid = $cc->get_keys_pmas($params['cat']);
        // Zend_Debug::dump($gettid);
        $dar = null;
        if(isset($params['startdate'])&& isset($params['enddate'])) {
            $d1 = gmdate("Y-m-d\TH:i:s\Z", strtotime($params['startdate']));
            $d2 = gmdate("Y-m-d\TH:i:s\Z", strtotime($params['enddate'])+ 1);
            $dar = 'content_date :[' . $d1 . ' TO ' . $d2 . ']';
        }
        $filter = array($dar);
        $soc = $n->get_news_bin($params['skey'], 0, 10, 5, $gettid, $filter);
        // Zend_Debug::dump($soc);die();
        $data = $n->get_news_bin($params['skey'], 0, 10, 2, $gettid, $filter);
        $laps = $n->get_news_bin($params['skey'], 0, 10, 3, $gettid, $filter);
        // Zend_Debug::dump($data);
        // die();
        //$gettid = $cc->get_keys_pmas($params['tag']);
        $this->view->cat = $dash;
        $this->view->soc = $soc;
        $this->view->data = $data;
        $this->view->laps = $laps;
        $this->view->varams = $params;
        $this->view->logo = $cc->get_logo();
    }

    public function listdictionaryAction() {
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/selector/selectorator.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        // $this->view->headScript()->appendFile('/assets/core/global/plugins/selector/selectorator.js');
        // $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        //Zend_Debug::dump($content); die();
        //$dom_1 = new Zend_Dom_Query($content);
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        $this->view->varams = $params;
        $cc = new Domas_Model_Pdictionary();
        $data = $cc->get_dictionary(strtolower($params["bahasa"]));
        $data2 = $cc->get_topik();
        $data3 = $cc->get_edit_dict();
        //Zend_Debug::dump($data2); die();
        $this->view->dictionary = $data;
        $this->view->taxonomy = $data2;
        $this->view->editdictionary = $data3;
    }

    public function tabledictionaryAction() {
        // die("lalala");
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $params = $this->getRequest()->getParams();
        $this->_helper->layout->disableLayout();
        // $this->_helper->viewRenderer->setNoRender(true);
        $mdl_admin = new Domas_Model_Pdictionary();
        // Zend_Debug::dump($params);die();
        $countgraph = $mdl_admin->count_listdictionary($params['search'], $_GET, $params['type']);
        // Zend_Debug::dump($countgraph);die();
        $listgraph = $mdl_admin->get_listdictionary($params['search'], $_GET, $params['type']);
        // die();
        $iTotalRecords = intval($countgraph);
        $iDisplayLength = intval($_GET['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_GET['iDisplayStart']);
        $sEcho = intval($_GET['sEcho']);
        $records = array();
        $records["aaData"] = array();
        $end = $iDisplayStart + $iDisplayLength;

        foreach($listgraph as $k => $v) {
            switch($v['sentiment']) {
                case "0" : $sen = "Netral";
                break;
                case "1" : $sen = "Negatif";
                break;
                case "2" : $sen = "Positif";
                break;
            }
            // Zend_Debug::dump($v);die();
            $records["aaData"][] = array($iDisplayStart+$k+ 1,
                                        $v['kata'],
                                        $v['bahasa'],
                                        $v['topik'],
                                        $sen,
                                         '<a data-id="' .$v['id']. '"
									data-kata="' .$v['kata']. '"
									data-sentiment="' .$v['sentiment']. '"
									data-bah="' .$v['bahasa']. '"
									data-topik="' .$v['topik']. '"
									data-toggle="modal" href="#edit_dict"  onclick="editdict(this);"><font color="white"><i class="btn btn-circle btn-sm blue">Edit</i></font></a>',
                                        );
        }
        // Zend_Debug::dump($records);die();
        $records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;
        
        /*
         * $gg = new Escort_Model_Kpu(); $res = $gg->get_datakpu($data2['witel']); //var_dump($res);die();
         */
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }

    public function listmediaAction() {
        //die("xx");
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/selector/selectorator.js');
        // $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        //Zend_Debug::dump($content); die();
        //$dom_1 = new Zend_Dom_Query($content);
        $params = $this->getRequest()->getParams();
        //   Zend_Debug::dump($params);die();
        $this->view->varams = $params;
        $cc = new Domas_Model_Pmedia();
        $data = $cc->get_all_main_media($params["type"]);
        //   Zend_Debug::dump($data); die();
        $this->view->media = $data;
    }

    public function listtokenAction() {
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/selector/selectorator.js');
        // Zend_Debug::dump($content); die();
        // $dom_1 = new Zend_Dom_Query($content);
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        $this->view->varams = $params;
        $cc = new Domas_Model_Pmedia();
        $data = $cc->get_api_twit();
        // Zend_Debug::dump($data); die();
        $this->view->api = $data;
    }

    public function eapiAction() {
        $params = $this->getRequest()->getParams();
        $cc = new Domas_Model_Pmedia();
        $this->view->data = $cc->get_a_apikey_by_id($params['id']);
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootbox/bootbox.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
    }

    public function emediaAction() {
        $params = $this->getRequest()->getParams();
        $cc = new Domas_Model_Pmedia();
        $this->view->data = $cc->get_a_media_by_id($params['id']);
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootbox/bootbox.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
    }

    public function listcrawlAction() {
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/selector/selectorator.js');
        // $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        //Zend_Debug::dump($content); die();
        //$dom_1 = new Zend_Dom_Query($content);
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params); die();
        $this->view->varams = $params;
        $cc = new Domas_Model_Pmedia();
        $data = $cc->get_all_media($params["type"]);
        //   Zend_Debug::dump($data); die();
        $this->view->media = $data;
    }

    public function setcrawlerAction() {
        
        
        $params = $this->getRequest()->getParams();
        $dd = new Domas_Model_Pmedia();
        $g = new Domas_Model_Admin();
        $this->view->date = $g->get_params_by_param("processing_date");
        if(isset($params['id'])&& $params['id'] != "") {
            $this->view->data = $dd->get_submedia_by_sub_id($params['id']);
        }
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-toastr/toastr.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-toastr/toastr.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/prabawf/metadata/css/basic.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/prabawf/js/jquery.simplemodal.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootbox/bootbox.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/selector/selectorator.js');
        $cc = new Domas_Model_Pmedia();
        $this->view->media = $cc->get_all_main_media();
    }

    public function listcronAction() {
        //  die("s");
        $this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/css/plugins.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        try {
            $auth = Zend_Auth::getInstance();
            $identity = $auth->getIdentity();
            $mdl_crn = new Prabadata_Model_Crontab();
            $params = $this->getRequest()->getParams();
            if(isset($params['id'])&& $params['id'] != '' && isset($params['act'])&& $params['act'] != '') {
                $check = $mdl_crn->checkIDCrontab($params['id']);
                if($check) {
                    switch($params['act']) {
                        case 'deleted' : $check = $mdl_crn->deleteCrontab($params['id']);
                        break;
                        case 'disabled' : $check = $mdl_crn->deactivateCrontab($params['id']);
                        break;
                        case 'enabled' : $check = $mdl_crn->activateCrontab($params['id']);
                        break;
                        default : break;
                    }
                }
            }
            $cronalert = $mdl_crn->getallAlertCrontab();
            $alert = array();

            foreach($cronalert as $k => $v) {
                $tmp1 = explode(" ", $v['info']);
                $tmp2 = explode("|", $tmp1[1]);
                $id = $tmp2[0];
                $user = $tmp2[1];
                $created_at = $tmp2[2];
                $start = $tmp2[3];
                $end = $tmp2[4];
                $interval = $tmp2[5];
                $tmp1 = explode(" ", $v['cron']);
                $status = 'enable';
                $tmp2 = explode("/", $tmp1[5]);
                if($tmp1[0] == '#disable') {
                    $status = 'disable';
                    $tmp2 = explode("/", $tmp1[6]);
                }
                $file = $tmp2[7];
                $alert[$id] = array('id' =>$id,
                                    'user' =>$user,
                                    'created_at' =>$created_at,
                                    'status' =>$status,
                                    'start' =>$start,
                                    'end' =>$end,
                                    'interval' =>$interval,
                                    'file' =>$file,
                                   );
            }
            $this->view->alert = $alert;
            //$this->view->messages = $this->_flashMessenger->getMessages();
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }
    }

    public function getcrawlerAction() {
        $params = $this->getRequest()->getParams();
        try {
            $e = unserialize(base64_decode($params['e']));
            // Zend_Debug::dump($e); die();
            $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
            $this->view->headScript()->appendFile('/assets/core/global/plugins/selector/selectorator.js');
            // $this->_helper->layout->disableLayout();
            $c2 = new CMS_LIB_parse();
            $content = file_get_contents($e['url']);
            // echo $content; die();
            $content = $c2->remove($content, "<!--", "-->");
            $content = $c2->remove($content, "<style", "</style>");
            $content = $c2->remove($content, "<script", "</script>");
            $content = preg_replace('/\s+/', ' ', $content);
            // echo $content; die();
            $dom_1 = new Zend_Dom_Query($content);
            //$results = $dom_1->query("body");
            $this->view->html = $dom_1->getDocument();
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    public function addAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/css/pendem.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');        
        // $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2-bootstrap.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-toastr/toastr.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-toastr/toastr.min.js');
        $this->view->headScript()->appendFile('/assets/core/pages/scripts/ui-toastr.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/infinitescroll/jquery.jscroll.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jstree/dist/themes/default/style.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jstree/dist/jstree_c.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-toastr/toastr.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-toastr/toastr.min.js');
        $params = $this->getRequest()->getParams();
        //    die("s");
        //$cz = new Domas_Model_General();
        //$this->view->sources = $cz->get_params_sources();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
    }

    public function tokohAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/css/pendem.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.full.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2-bootstrap.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/infinitescroll/jquery.jscroll.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jstree/dist/themes/default/style.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jstree/dist/jstree_c.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-toastr/toastr.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-toastr/toastr.min.js');
        $params = $this->getRequest()->getParams();
        //    die("s");
        //$cz = new Domas_Model_General();
        //$this->view->sources = $cz->get_params_sources();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
    }

    public function setrestartAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        $id = 0;
        if(isset($params["id"])&& $params["id"] != "") {
            $id = $params["id"];
        }
        // Zend_Debug::dump($id);die();
        $data = "test";
        if(!function_exists("ssh2_connect"))
            die("function ssh2_connect doesn't exist");
        // log in at server1.example.com on port 22
        if(!($con = ssh2_connect("172.16.3.28", 22))) {
            echo "fail: unable to establish connection\n";
        } else {
            // try to authenticate with username root, password secretpassword
            if(!ssh2_auth_password($con, "root", "TelkomSigma2016")) {
                echo "fail: unable to authenticate\n";
            } else {
                // allright, we're in!
                // echo "okay: logged in...\n";
                // execute a command
                if(!($stream = ssh2_exec($con, "sh /home/admin/twitter/runStreams_config" . $id . ".sh"))) {
                    echo "fail: unable to execute command\n";
                } else {
                    // collect returning data from command
                    stream_set_blocking($stream, true);
                    $data = "";

                    while($buf = fread($stream, 4096)) {
                        $data .= $buf;
                    }
                    fclose($stream);
                }
            }
        }
        echo $data;
        die();
    }

    public function prediksiAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/css/pendem.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2-bootstrap.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/infinitescroll/jquery.jscroll.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jstree/dist/themes/default/style.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jstree/dist/jstree_c.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-toastr/toastr.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-toastr/toastr.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2-bootstrap.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.full.min.js');
        $this->view->headScript()->appendFile('/assets/core/pages/scripts/components-select2.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/chosen_v1.6.2/chosen.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/chosen_v1.6.2/chosen.jquery.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/editable/dist/jquery-editable-select-min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/editable/dist/jquery-editable-select-min.js');
        $params = $this->getRequest()->getParams();
        //    die("s");
        //$cz = new Domas_Model_General();
        //$this->view->sources = $cz->get_params_sources();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
    }

    public function addcronAction() {
        $this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/css/plugins.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/moment.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        #Zend_De
        //Zend_Debug::dump($identity);die();
        //Zend_Debug::dump($_POST);die();
        if($_POST && $_POST["machine_name"] != "") {
            $mdl_crn = new Model_Crontab();
            $mdl_api = new Model_Api();
            if(count($_POST["sch"])> 0) {

                foreach($_POST["sch"] as $k => $v) {
                    $cronconf = '';
                    switch($v) {
                        case 'hourly' : $cronconf = $mdl_crn->getcroninterval($_POST["tgl1"] . 
                                                                              ':00', $_POST["tgl2"] . 
                                                                              ':00', 'hour');
                        break;
                        case 'daily' : $cronconf = $mdl_crn->getcroninterval($_POST["tgl1"] . 
                                                                             ':00', $_POST["tgl2"] . 
                                                                             ':00', 'day');
                        break;
                        case 'weekly' : $cronconf = $mdl_crn->getcroninterval($_POST["tgl1"] . 
                                                                              ':00', $_POST["tgl2"] . 
                                                                              ':00', 'week');
                        break;
                        case 'monthly' : $cronconf = $mdl_crn->getcroninterval($_POST["tgl1"] . 
                                                                               ':00', $_POST["tgl2"] . 
                                                                               ':00', 'month');
                        break;
                        default : break;
                    }
                    if($cronconf == '') {
                        break;
                    }
                    //bug::dump($cronconf.' - '.$v);die();
                    $intcron = $mdl_crn->addcronalert($identity->uid, $cronconf, $_POST["machine_name"], str_replace(" ", "_", $_POST["tgl1"]). 
                                                                                                                     ':00', str_replace(" ", "_", $_POST["tgl2"]). 
                                                                                                                                        ':00', $v);
                }
            } else {
                if($_POST['cron_formula'] != "") {
                    $zvar = explode(" ", trim($_POST['cron_formula']));
                    // Zend_Debug::dump($zvar); die();
                    if(count($zvar)!= 5) {
                        $this->_flashMessenger->addMessage('Not cron formula|danger');
                        $this->_redirect('/prabadata/q/listcron');
                    }
                    $cronconf = $_POST['cron_formula'];
                    $v = 'custom';
                    $intcron = $mdl_crn->addcronalert($identity->uid, $cronconf, $_POST["machine_name"], str_replace(" ", "_", $_POST["tgl1"]). 
                                                                                                                     ':00', str_replace(" ", "_", $_POST["tgl2"]). 
                                                                                                                                        ':00', $v);
                    if($intcron['transaction'] == true) {
                        $this->_flashMessenger->addMessage($intcron['message'] . 
                                                           '|success');
                    } else {
                        $this->_flashMessenger->addMessage($intcron['message'] . 
                                                           '|danger');
                    }
                }
            }
            //die();
            $this->_redirect('/analis/index/listcron');
        }
    }

    public function detailcronAction() {
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        try {
            $auth = Zend_Auth::getInstance();
            $identity = $auth->getIdentity();
        }
        catch(Exception $e) {
        }
        //Zend_Debug::dump($identity);die();
        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($params);die();
        if(!isset($params['id'])|| $params['id'] == '') {
            #	$this->_redirect('/messaging/managealerts');
        }
        $mdl_crn = new Model_Crontab();
        $cronalert = $mdl_crn->getAlertCrontab($params['id']);
        //Zend_Debug::dump($cronalert);die();
        if(!isset($cronalert['info'])) {
            #$this->_redirect('/messaging/managealerts');
        }
        $alert = array();
        $tmp1 = explode(" ", $cronalert['info']);
        $tmp2 = explode("|", $tmp1[1]);
        //Zend_Debug::dump($tmp2);die();
        $id = $tmp2[0];
        $user = $tmp2[1];
        $created_at = $tmp2[2];
        $tmp1 = explode(" ", $cronalert['cron']);
        $status = 'enable';
        $tmp2 = explode("/", $tmp1[5]);
        $config = $tmp1[0] . " " . $tmp1[1] . " " . $tmp1[2] . " " . $tmp1[3] . " " . $tmp1[4];
        $minute = $tmp1[0];
        $hour = $tmp1[1];
        $dayofmonth = $tmp1[2];
        $month = $tmp1[3];
        $dayofweek = $tmp1[4];
        if($tmp1[0] == '#disable') {
            $status = 'disable';
            $tmp2 = explode("/", $tmp1[6]);
            $config = $tmp1[1] . " " . $tmp1[2] . " " . $tmp1[3] . " " . $tmp1[4] . " " . $tmp1[5];
            $minute = $tmp1[1];
            $hour = $tmp1[2];
            $dayofmonth = $tmp1[3];
            $month = $tmp1[4];
            $dayofweek = $tmp1[5];
        }
        //Zend_Debug::dump($tmp2);die();
        $file = $tmp2[7];
        #	$msg = file_get_contents("http://10.62.8.133/cronalert/msg/".str_replace(".sh",".php",$file)."?get=msg");
        $msg = json_decode($msg);
        //Zend_Debug::dump($msg);die();
        #	$target = file_get_contents("http://10.62.8.133/cronalert/msg/".str_replace(".sh",".php",$file)."?get=target");
        $target = json_decode($target, true);
        //Zend_Debug::dump($target);die();
        $alert = array('id' =>$id,
                       'dibuat oleh' =>$user,
                       'tanggal dibuat' =>$created_at,
                       'status' =>$status,
                       'pesan' =>$msg->msg,
                       'file' =>$file,
                       'config' =>$config,
                       'menit' =>$minute,
                       'jam' =>$hour,
                       'hari per bulan' =>$dayofmonth,
                       'bulan' =>$month,
                       'hari per bulan ' =>$dayofweek,
                       'target' =>$target);
        //Zend_Debug::dump($alert);die();
        $this->view->alert = $alert;
        $this->view->act = $mdl_crn->get_action();
        $this->view->cact = $mdl_crn->get_act_action($id);
        $this->view->varams = $params;
    }

    public function searchAction() {
        //-== CSS
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        //-== JS
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/highcharts4.0.4/js/highcharts.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/highcharts4.0.4/js/highcharts-3d.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/highcharts4.0.4/js/themes/dark-unica.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/highcharts4.0.4/js/modules/exporting.js');
        $this->view->headScript()->appendFile('http://maps.google.com/maps/api/js?language=id');
        $this->view->headScript()->appendFile('/assets/core/plugins/gmaps/gmaps.js');
        $this->view->headScript()->appendFile('/assets/core/js/d3.v3.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/table-ajax.js');
        // $i=0;
        $params = $this->getRequest()->getParams();
        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($params); die();
        if(!isset($params["startdate"])) {
            // $params["startdate"] = date("Y-m-d", strtotime('monday last week'));
            $params["startdate"] = date("Y-m-d");
        }
        if(!isset($params["enddate"])) {
            $params["enddate"] = date("Y-m-d");
        }
        $arr_map = array("lat" => - 1.131695,
                         "lng" => 119.503078,
                         "zoom" => 5,
                        );
        if(isset($params["media_type"])&& $params["media_type"] == "internasional") {
            $arr_map = array("lat" => 26.4578763,
                             "lng" => 32.0579421,
                             "zoom" => 1,
                            );
        }
        // Zend_Debug::dump($arr_map);//die();
        $as = new Domas_Model_General();
        //koneksi ke oracle
        // $data_alertfr = $as->alertfr();  
        $data = array();
        $cc = new Domas_Model_Pdash();
        $dd = new Model_Cache();
        $cache = $dd->cachefunc(3000);
        $id = $dd->get_id("Domas_Model_Pdash_get_tags_pmas");
        $dash = $dd->get_cache($cache, $id);
        if(!$dash) {
            $dash = $cc->get_tags_pmas();
            $cache->save($dash, $id, array('systembin'));
        }
        //Zend_Debug::dump($dash);die();
        $this->view->cat = $dash;
        $this->view->varams = $params;
        $this->view->map = $arr_map;
        $this->view->alertfr = $data_alertfr;
    }

    public function temp3Action() {
    }

    public function temp4Action() {
    }

    public function temp5Action() {
    }

    public function indexAction() {
        $authAdapter = Zend_Auth::getInstance();
        $identity = $authAdapter->getIdentity();
        $mdl_zusr = new Model_Zusers();
        $mdl_sys = new Model_System();
        $data = $mdl_zusr->getuser($identity->uname);
        $arr1 = array();
        if(isset($data['id'])) {
            $rols = $mdl_sys->get_roles_by_uid($data['id']);

            foreach($rols as $v) {
                $arr[] = $v['gid'];
                if($data['main_role'] != '' && $data['main_role'] != null) {
                    if($v['landing_page'] != "" && $v['landing_page'] != null && (int) $v['gid'] == (int) $data['main_role']) {
                        $redirect_role = $v['landing_page'];
                    }
                }
                if($v['exec'] != "") {
                    $exec[] = $v['exec'];
                }
            }
        }
        if($redirect_role != "") {
            $this->_redirect($redirect_role);
        }
    }

    public function stopwordAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/css/pendem.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.full.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2-bootstrap.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/infinitescroll/jquery.jscroll.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jstree/dist/themes/default/style.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jstree/dist/jstree_c.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-toastr/toastr.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-toastr/toastr.min.js');
        $params = $this->getRequest()->getParams();
        //    die("s");
        //$cz = new Domas_Model_General();
        //$this->view->sources = $cz->get_params_sources();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
    }

    
    public function getsummaryxAction() {
        ini_set("memory_limit", "-1");
        $params = $this->getRequest()->getParams();
        Zend_Debug::dump($params);
        die();
        $g = new Domas_Model_Pdash();
        $as = new Domas_Model_Solr();
        $tema = $g->get_tema_by_tema_id($params["cat"]);
        if($params["cat"] != "") {
            $fname = $tema[0]["name"];
            $arr_data[$tema[0]["name"]] = $as->get_stopword($params);
        } else {
            // Zend_Debug::dump($tema);die();
            $fname = "NASIONAL";
            $arr_data["Nasional"] = $as->get_stopword($params);
            // Zend_Debug::dump($arr_data);die();
            //foreach($tema as $k=>$v){
            // // Zend_Debug::dump($v);die();
            //$params["cat"] = $v["tid"];
            //$arr_data[$v["name"]] = $as->get_stopword($params);
            // Zend_Debug::dump($arr_data);die();
            // }
        }
        //Zend_Debug::dump($arr_data);die();
        $xxx = array("laporan" => array("LAPORAN",
                     "Laporan"),
                     "digmed" => array("MEDIA DIGITAL",
                     "News"),
                     "socmed" => array("MEDIA SOSIAL",
                     "Social Media"),
                    );
        //Zend_Debug::dump();die();
        $html .= "
		<div style=\"text-align:center;\"><b>LAPORAN INFORMASI</b></div><br /><br />";
        $html .= "Pada hari ini, " . date("d M Y", strtotime($params["d1"])). " pukul " . date("H:i"). ", berdasarkan hasil monitoring Sistem Media Komprehensif Intelijen Analitik secara keseluruhan mendapatkan sepuluh topik yang menjadi tren pada masing-masing media. Selengkapnya dilaporkan sebagai berikut :<br /><br />";

        foreach($arr_data as $k => $v) {
            $html .= "<b>" . strtoupper($k). ":</b><br />";

            foreach($v as $k0 => $v0) {
                $html .= "<b>[" . $xxx[$k0][0] . "]</b><br />";
                //echo $html;
                //Zend_Debug::dump($k0);
                // Zend_Debug::dump($v0);
                // die();
                // $html .= "<b>[".$xxx[$k][0]."]</b><br />";
                $i = 0;
                if(count($v0)> 0) {

                    foreach($v0 as $k1 => $v1) {
                        if($i < 10 && $v1 > 0) {
                            // Zend_Debug::dump($k1);die();
                            $params["t"] = $xxx[$k0][1];
                            $params["skey"] = $k1;
                            // Zend_Debug::dump($params);die();
                            $sentimen = $as->get_sentiment($params);
                            // Zend_Debug::dump($sentimen);
                            $html .=($i + 1). ". " . $k1 . ", menjadi top tren karena diperbincangkan sebanyak <b>" . $v1 . " data source</b> dengan komposisi <span class=\"netral\">" . $sentimen[$params["t"]][$k1]["Netral"] . " sentimen netral</span>, <span class=\"negatif\">" . $sentimen[$params["t"]][$k1]["Negatif"] . " sentimen negatif</span> dan <span class=\"positif\">" . $sentimen[$params["t"]][$k1]["Positif"] . " sentimen positif</span><br />";
                            $i ++;
                        }
                    }
                } else {
                    $html .= "-<br />";
                }
                $html .= "<br />";
            }
            $html .= "<hr>";
        }
        // header("Content-type: application/octet-stream");
        // header("Content-Disposition: attachment; filename=SUMMARY_".$fname.".doc");
        // header("Pragma: no-cache");
        // header("Expires: 0");
        echo "<html>";
        echo "<head>";
        echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">";
        echo "<style>";
        echo "
		body {
				font-family: Arial, Verdana, Helvetica, Courier, sans-serif;
				font-size:12pt;
		}
		.netral{
			font-weight: bold;
			color: blue;
		}
		.negatif{
			font-weight: bold;
			color: red;
		}
		.positif{
			font-weight: bold;
			color: green;
		}
		";
        echo "</style>";
        echo "</head>";
        echo "<body>";
        echo "<span style=\"color:#000000;font-size:12px;font-family:Arial,'Trebuchet MS', Verdana;\">";
        echo $html;
        echo "</span>";
        echo "</body>";
        echo "</html>";
        die();
    }

    public function getsummaryAction() {
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);//
        // die();
        $entity = array("laporan" => array("laporan",
                        "lap",
                        "LAPORAN",
                        "Laporan"),
                        "digmed" => array("digmed",
                        "news",
                        "MEDIA DIGITAL",
                        "News"),
                        "socmed" => array("socmed",
                        "socmed",
                        "MEDIA SOSIAL",
                        "Social Media"),
                       );
        $s = new Analis_Model_Solr();
        $z = new Model_Cache();
        $g = new Domas_Model_Pdash();
        $tema = $g->get_tema_by_tema_id($params["cat"]);
        $arr_data = array();

        foreach($entity as $key => $val) {
            $parameter = array();
            $parameter["ajax"] = "1";
            $parameter["t"] = $val[0];
            $parameter["skey"] = $params["skey"];
            $parameter["searchkey"] = $params["searchkey"];
            $parameter["skeyexception"] = $params["skeyexception"];
            $parameter["cat"] = $params["cat"];
            $parameter["topik"] = $params["topik"];
            $parameter["is_mediatype"] = $params["is_mediatype"];
            $parameter["d1"] = $params["d1"];
            $parameter["d2"] = $params["d2"];
            $parameter["is_sentiment"] = $params["is_sentiment"];
            $parameter["tc"] = $params["tc_" . $val[1]];
            //Zend_Debug::dump($parameter);die();
            $cache = $z->cachefunc(3000);
            $id = $z->get_id("Analis_Model_Solr_get_wordcloud", $parameter);
            $data = $z->get_cache($cache, $id);
            //$data = false;
            if(!$data) {
                $data = $s->get_wordcloud($parameter);
                $cache->save($data, $id, array('systembin'));
            }
            $arr_data[$key] = $data;
            // Zend_Debug::dump($data);die();
        }
        // Zend_Debug::dump($arr_data);//die("s");
        if($params["cat"] != "") {
            $fname = strtoupper($tema[0]["name"]);
            // $arr_data[$tema[0]["name"]] = $as->get_stopword($params);
        } else {
            // Zend_Debug::dump($tema);die();
            $fname = "NASIONAL";
            // $arr_data["Nasional"] = $as->get_stopword($params);
        }
        $html = "";
        $html .= "<div style=\"text-align:center;\"><b>LAPORAN INFORMASI</b></div><br /><br />";
        $html .= "Pada hari ini, " . date("d M Y", strtotime($params["d1"])). " pukul " . date("H:i"). ", berdasarkan hasil monitoring Sistem Media Komprehensif Intelijen Analitik secara keseluruhan mendapatkan sepuluh topik yang menjadi tren pada masing-masing media. Selengkapnya dilaporkan sebagai berikut :<br /><br />";
        $html .= "<b>" . $fname . ":</b><br/>";

        foreach($arr_data as $k0 => $v0) {
            $html .= "<b>[" . $entity[$k0][2] . "]</b><br/>";
            //Zend_Debug::dump($v0);die();
            $wc = "-";
            if(count($v0)> 0) {
                $wc = "";
                $i = 0;

                foreach($v0 as $k1 => $v1) {
                    $parameter = array();
                    // $parameter["skey"] = $params["skey"];
                    $parameter["skey"] =($params["skey"] != "" ? $params["skey"] . 
                                         "," : ""). trim(str_replace("(ORG)", "", str_replace("(PER)", "", $k1)));
                    $parameter["skeyexception"] =($params["skeyexception"] != "" ? $params["skeyexception"] . 
                                                  "," : ""). trim(str_replace("(ORG)", "", str_replace("(PER)", "", $k1)));
                    $parameter["cat"] = $params["cat"];
                    $parameter["searchkey"] =($params["searchkey"] != "" ? $params["searchkey"] . 
                                              "," : ""). trim(str_replace("(ORG)", "", str_replace("(PER)", "", $k1)));
                    $parameter["topik"] = $params["topik"];
                    $parameter["is_mediatype"] = $params["is_mediatype"];
                    $parameter["t"] = $entity[$k0][3];
                    $parameter["d1"] = $params["d1"];
                    $parameter["d2"] = $params["d2"];
                    $parameter["is_sentiment"] = $params["is_sentiment"];
                    // Zend_Debug::dump($parameter);//die();
                    $cache = $z->cachefunc(3000);
                    $id = $z->get_id("Analis_Model_Solr_get_sentimentx", $parameter);
                    $data = $z->get_cache($cache, $id);
                    // Zend_Debug::dump($params);die("s");
                    // $data = false;
                    if(!$data) {
                        $data = $s->get_sentimentx($parameter);
                        $cache->save($data, $id, array('systembin'));
                    }
                    // Zend_Debug::dump($data);//die();
                    $smen = array();

                    foreach($data as $kx => $vx) {

                        foreach($vx as $ky => $vy) {
                            if($ky % 2 == 1) {
                                $smen[$kx] += $vy;
                                $smen["TOTAL"] += $vy;
                            }
                        }
                    }
                    //Zend_Debug::dump($smen);die();
                    $wc .=($i + 1). ". " . $k1;
                    if($entity[$k0][2] != "LAPORAN") {
                        $wc .= ", menjadi top tren karena diperbincangkan sebanyak <b>" . $v1 . " data source</b> dengan komposisi <span class=\"netral\">" . $smen["NETRAL"] . " sentimen netral</span>, <span class=\"negatif\">" . $smen["NEGATIF"] . " sentimen negatif</span> dan <span class=\"positif\">" . $smen["POSITIF"] . " sentimen positif</span>.";
                    } else if($entity[$k0][2] == "LAPORAN") {
                        $wc .= ", menjadi top tren karena diperbincangkan sebanyak <b>" . $v1 . " data source</b>.";
                    }
                    $wc .= "<br/>";
                    $i ++;
                }
            }
            $html .= $wc;
            $html .= "<br/>";
        }
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=SUMMARY_" . 
               $fname . 
               ".doc");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo "<html>";
        echo "<head>";
        echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">";
        echo "<style>";
        echo "
	body {
			font-family: Arial, Verdana, Helvetica, Courier, sans-serif;
			font-size:12pt;
	}
	.netral{
		font-weight: bold;
		color: blue;
	}
	.negatif{
		font-weight: bold;
		color: red;
	}
	.positif{
		font-weight: bold;
		color: green;
	}
	";
        echo "</style>";
        echo "</head>";
        echo "<body>";
        echo "<span style=\"color:#000000;font-size:12px;font-family:Arial,'Trebuchet MS', Verdana;\">";
        echo $html;
        echo "</span>";
        echo "</body>";
        echo "</html>";
        die();
    }

    public function gettopsummaryAction() {
        // die('sss');
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);
        $data = array();
        // // // if($params["l"]=="news"){
        $cc = new Analis_Model_Top();
        $as = new Analis_Model_Solr();
        $paramsx = array();
        $paramsx["l"] = "news";
        // Zend_Debug::dump($params);die();
        $z = new Model_Cache();
        $cache = $z->cachefunc(2000);
        $id = $z->get_id("Analis_Model_Top_get_top", $paramsx);
        $data = $z->get_cache($cache, $id);
        // Zend_Debug::dump($data);die();
        if(!$data) {
            $data = $cc->get_top($paramsx);
            $cache->save($data, $id, array('systembin'));
        }
        $tmp = array();

        foreach($data as $k0 => $v0) {

            foreach($v0["content"] as $k1 => $v1) {
                $tmp[$k0][$k1]["content"] = $v1;
                $tmp[$k0][$k1]["url"] =($k0 == "antaranews populer" ? "http://antaranews.com" : ""). $v0["url"][$k1];
                $tmp[$k0][$k1]["count"] = 0;
            }
        }
        $id2 = $z->get_id("Analis_Model_Top_get_toptren_news", $params);
        $data2 = $z->get_cache($cache, $id2);
        // $data2 = false;
        if(!$data2) {
            $data2 = $as->get_toptren_news($params);
            $cache->save($data2, $id2, array('systembin'));
        }
        $tmp["TOP MEDIA DIGITAL"] = $data2;
        // Zend_Debug::dump($data2);die("s");
        $data = $tmp;
        // Zend_Debug::dump($data);die();
        $html = "";
        $html .= "<div style=\"text-align:center;\"><b>TRENDING BERITA</b></div><br /><br />";

        foreach($data as $k0 => $v0) {
            $html .= "<b>" . strtoupper($k0). "</b><br/>";
            if(count($v0)> 0) {
                $i = 0;
                $wc = "";

                foreach($v0 as $k1 => $v1) {
                    // Zend_Debug::dump($v1);die();
                    $wc .= $v1["content"];
                    $wc .= "<br/>";
                    // $wc .= "URL = ".$v1["url"];
                    // $wc .="<br/>";
                    $i ++;
                }
            }
            $html .= $wc;
            $html .= "<br/>";
        }
        // echo $html;
        // die();
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=SUMMARY_TRENDING BERITA.doc");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo "<html>";
        echo "<head>";
        echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">";
        echo "<style>";
        echo "
		body {
				font-family: Arial, Verdana, Helvetica, Courier, sans-serif;
				font-size:12pt;
		}
		.netral{
			font-weight: bold;
			color: blue;
		}
		.negatif{
			font-weight: bold;
			color: red;
		}
		.positif{
			font-weight: bold;
			color: green;
		}
		";
        echo "</style>";
        echo "</head>";
        echo "<body>";
        echo "<span style=\"color:#000000;font-size:12px;font-family:Arial,'Trebuchet MS', Verdana;\">";
        echo $html;
        echo "</span>";
        echo "</body>";
        echo "</html>";
        die();
        // // // // } else  if($params["l"]=="socmed"){
        // // // // // Zend_Debug::dump($params);die();
        // // // // $as = new Analis_Model_Solr();
        // // // // $z = new Domas_Model_Cache();
        // // // // $cache = $z->cachefunc(2000);
        // // // // $id = $z->get_id("Analis_Model_Solr_get_toptren_twitter", $params);
        // // // // $data = $z->get_cache($cache, $id);
        // // // // // Zend_Debug::dump($data);die();
        // // // // // $data = false;
        // // // // if(!$data){
        // // // // $data = $as->get_toptren_twitter($params);
        // // // // $cache->save ( $data, $id, array (
        // // // // 'systembin') );
        // // // // }
        // // // // // Zend_Debug::dump($data);die();
        // // // // }
        // // // // // Zend_Debug::dump($data);die();
        // // // // // $this->view->varams = $params;
        // // // // // $this->view->data = $data;
        // // // // die('ss');
        // die();
    }

    public function gettoptwitterAction() {
        // die('sss');
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);//die('s');
        $as = new Analis_Model_Solr();
        $z = new Model_Cache();
        $cache = $z->cachefunc(2000);
        $id = $z->get_id("Analis_Model_Solr_get_toptren_twitter", $params);
        $data = $z->get_cache($cache, $id);
        // Zend_Debug::dump($data);die();
        // $data = false;
        if(!$data) {
            $data = $as->get_toptren_twitter($params);
            $cache->save($data, $id, array('systembin'));
        }
        // Zend_Debug::dump($data);die();
        $html = "";
        $html .= "<div style=\"text-align:center;\"><b>TRENDING TWITTER</b></div><br />";

        foreach($data as $k0 => $v0) {
            // Zend_Debug::dump($v0);die();
            $html .= "<b>" . strtoupper(($k0 + 1). 
                                         ". " . 
                                         "@" . 
                                         $v0["ss_retweeted_user_screenname"]). "</b><br/>" .($v0["ss_retweeted_text"] . 
                                                                                             "</b><br/>" . 
                                                                                            ($v0["count"]));
            // echo $html;
            // die();
            // Zend_Debug::dump($v2);die();
            $html .= $wc;
            $html .= "<br/><br/>";
        }
        // echo $html;
        // die();
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=SUMMARY_TOP TWITTER.doc");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo "<html>";
        echo "<head>";
        echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">";
        echo "<style>";
        echo "
		body {
				font-family: Arial, Verdana, Helvetica, Courier, sans-serif;
				font-size:12pt;
		}
		.netral{
			font-weight: bold;
			color: blue;
		}
		.negatif{
			font-weight: bold;
			color: red;
		}
		.positif{
			font-weight: bold;
			color: green;
		}
		";
        echo "</style>";
        echo "</head>";
        echo "<body>";
        echo "<span style=\"color:#000000;font-size:12px;font-family:Arial,'Trebuchet MS', Verdana;\">";
        echo $html;
        echo "</span>";
        echo "</body>";
        echo "</html>";
        die();
    }

    public function getexportAction() {
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);//die();
        $entity = array("laporan" => array("laporan",
                        "lap",
                        "LAPORAN",
                        "Laporan"),
                        "digmed" => array("digmed",
                        "news",
                        "MEDIA DIGITAL",
                        "News"),
                        "socmed" => array("socmed",
                        "socmed",
                        "MEDIA SOSIAL",
                        "Social Media"),
                       );
        $s = new Analis_Model_Solr();
        $z = new Model_Cache();
        $g = new Domas_Model_Pdash();
        $tema = $g->get_tema_by_tema_id($params["cat"]);
        $arr_data = array();

        foreach($entity as $key => $val) {
            $parameter = array();
            $parameter["ajax"] = "1";
            $parameter["t"] = $val[0];
            $parameter["skey"] = $params["skey"];
            $parameter["searchkey"] = $params["searchkey"];
            $parameter["skeyexception"] = $params["skeyexception"];
            $parameter["cat"] = $params["cat"];
            $parameter["topik"] = $params["topik"];
            $parameter["is_mediatype"] = $params["is_mediatype"];
            $parameter["d1"] = $params["d1"];
            $parameter["d2"] = $params["d2"];
            $parameter["is_sentiment"] = $params["is_sentiment"];
            $parameter["tc"] = $params["tc_" . $val[1]];
            // Zend_Debug::dump($parameter);//die();
            $cache = $z->cachefunc(3000);
            $id = $z->get_id("Analis_Model_Solr_get_wordcloud", $parameter);
            $data = $z->get_cache($cache, $id);
            //$data = false;
            if(!$data) {
                $data = $s->get_wordcloud($parameter);
                $cache->save($data, $id, array('systembin'));
            }
            $arr_data[$key] = $data;
            // Zend_Debug::dump($data);die();
        }
        // Zend_Debug::dump($arr_data);//die("s");
        if($params["cat"] != "") {
            $fname = strtoupper($tema[0]["name"]);
            // $arr_data[$tema[0]["name"]] = $as->get_stopword($params);
        } else {
            // Zend_Debug::dump($tema);die();
            $fname = "NASIONAL";
            // $arr_data["Nasional"] = $as->get_stopword($params);
        }
        $html = "";
        $html .= "<div style=\"text-align:center;\"><b>LAPORAN INFORMASI</b></div><br /><br />";
        $html .= "Pada hari ini, " . date("d M Y", strtotime($params["d1"])). " pukul " . date("H:i"). ", berdasarkan hasil monitoring Sistem Media Komprehensif Intelijen Analitik secara keseluruhan mendapatkan sepuluh topik yang menjadi tren pada masing-masing media. Selengkapnya dilaporkan sebagai berikut :<br /><br />";
        $html .= "<b>" . $fname . ":</b><br/>";

        foreach($arr_data as $k0 => $v0) {
            $html .= "<b>[" . $entity[$k0][2] . "]</b><br/>";
            //$wc .="<table border='1'><th>No</th><th>Personal</th><th>Jumlah</th>";
            // Zend_Debug::dump($v0);die();
            $wc = "-";
            if(count($v0)> 0) {
                $wc = "";
                $i = 0;

                foreach($v0 as $k1 => $v1) {
                    $parameter = array();
                    // $parameter["skey"] = $params["skey"];
                    $parameter["skey"] =($params["skey"] != "" ? $params["skey"] . 
                                         "," : ""). trim(str_replace("(ORG)", "", str_replace("(PER)", "", $k1)));
                    $parameter["skeyexception"] =($params["skeyexception"] != "" ? $params["skeyexception"] . 
                                                  "," : ""). trim(str_replace("(ORG)", "", str_replace("(PER)", "", $k1)));
                    $parameter["cat"] = $params["cat"];
                    $parameter["searchkey"] =($params["searchkey"] != "" ? $params["searchkey"] . 
                                              "," : ""). trim(str_replace("(ORG)", "", str_replace("(PER)", "", $k1)));
                    $parameter["topik"] = $params["topik"];
                    $parameter["is_mediatype"] = $params["is_mediatype"];
                    $parameter["t"] = $entity[$k0][3];
                    $parameter["d1"] = $params["d1"];
                    $parameter["d2"] = $params["d2"];
                    $parameter["is_sentiment"] = $params["is_sentiment"];
                    // Zend_Debug::dump($parameter);//die();
                    $cache = $z->cachefunc(3000);
                    $id = $z->get_id("Analis_Model_Solr_get_sentimentx", $parameter);
                    $data = $z->get_cache($cache, $id);
                    // Zend_Debug::dump($params);die("s");
                    // $data = false;
                    if(!$data) {
                        $data = $s->get_sentimentx($parameter);
                        $cache->save($data, $id, array('systembin'));
                    }
                    // Zend_Debug::dump($data);//die();
                    $smen = array();

                    foreach($data as $kx => $vx) {

                        foreach($vx as $ky => $vy) {
                            if($ky % 2 == 1) {
                                $smen[$kx] += $vy;
                                $smen["TOTAL"] += $vy;
                            }
                        }
                    }
                    // Zend_Debug::dump($smen);
                    //$wc .=($i+1).". ".$k1;
                    
                    /* if($entity[$k0][2]!="LAPORAN"){
                     $wc .=$smen["TOTAL"];
                     } else if($entity[$k0][2]=="LAPORAN"){
                     $wc .=$v1;
                     } */
                    $wc .= "<table border='1'>";
                    $wc .= "<tr><td>" .($i + 1). "</td>";
                    $wc .= "<td>" . $k1 . "</td>";
                    $wc .= "<td>" . $v1 . "</td></tr></table>";
                    //$wc .="<br/>";
                    $i ++;
                }
            }
            $html .= $wc;
            $html .= "<br/>";
        }
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=SUMMARY_" . 
               $fname . 
               ".xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo "<html>";
        echo "<head>";
        echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">";
        echo "<style>";
        echo "
	body {
			font-family: Arial, Verdana, Helvetica, Courier, sans-serif;
			font-size:12pt;
	}
	.netral{
		font-weight: bold;
		color: blue;
	}
	.negatif{
		font-weight: bold;
		color: red;
	}
	.positif{
		font-weight: bold;
		color: green;
	}
	";
        echo "</style>";
        echo "</head>";
        echo "<body>";
        echo "<span style=\"color:#000000;font-size:12px;font-family:Arial,'Trebuchet MS', Verdana;\">";
        echo $html;
        echo "</span>";
        echo "</body>";
        echo "</html>";
        die();
    }
}
