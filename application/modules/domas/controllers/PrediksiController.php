<?php

/*
 * Copyright (C) Prabtech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>,
 */

class Domas_PrediksiController extends Zend_Controller_Action {

    public function getcateAction() {
        $params = $this->getRequest()->getParams();
        $c = new Domas_Model_General();
        $data = $c->get_params_category($params['xxxid']);
        $this->_helper->layout->disableLayout();

        foreach($data as $k => $v) {
            echo "<option value='" . $v . "'>" . $v . "</option>";
        }
        die();
    }

    public function addtaxAction() {
        $params = $this->getRequest()->getParams();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        if($_POST) {
            #  Zend_Debug::dump($params); die();
            if(!isset($params['dis'])) {
                $params['dis'] = 0;
            }
            $cc = new Domas_Model_General();
            $data = array($params['tname'],
                         $params['machine'],
                         $params['descr'],
                         $params['dis']);
            $update = $cc->add_tax($data);
            // Zend_Debug::dump($update); die();
            $this->_helper->layout->disableLayout();
            if($update['result']) {
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$update['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function updatemapAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $params = $this->getRequest()->getParams();
        //   Zend_Debug::dump($params); die();
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            $cc = new Domas_Model_Zfiles();
            $ff = new Domas_Model_Withsolrpmas();
            //Zend_Debug::dump($_POST); die();
            parse_str($_POST['varx'], $data);
            //Zend_Debug::dump($data);
            //die();
            $params = $data;
            // Zend_Debug::dump($params); die();
            $file = array();
            $dd = new Domas_Model_General();
            $tags = $dd->get_tags_for_insert($_POST['treecek']);
            // Zend_Debug::dump($tags); die();

            foreach($tags as $k => $v) {

                foreach($v as $k2 => $v2) {
                    $idata['tm']["tm_vid_" . $k . "_names"][] = $v2;
                    $idata['im']["im_vid_" . $k . "_names"][] = $k2;
                    $idata['pid'][] = $k2;
                }
            }
            //  Zend_Debug::dump($idata);

            foreach($data['idfiles'] as $v) {
                $file = $cc->get_a_file($v);
                //Zend_Debug::dump($file);die();
                
                /* $file['raw_url']="/Users/techmayantaraasia/Workingspace/prabacontent/prabacontent/application/../public/repositories/JASAMAR1/Making\ Sense of Data.pdf";*/
                if(file_exists($file['raw_url'])) {
                    //die("xxx");                
                    $content = TikaWrapper::getText(realpath($file['raw_url']));
                    $meta = TikaWrapper::getMetadata(realpath($file['raw_url']));
                    $v_meta = explode("\n", $meta);
                    $list = array();
                    // Zend_Debug::dump(realpath($file['raw_url']));die();

                    foreach($v_meta as $c) {
                        $vz = array();
                        if($vz != "") {
                            $vz = explode(": ", $c);
                            if($vz[0] != "") {
                                $list[$vz[0]] = $vz[1];
                            }
                        }
                    }
                }
                // die("yyy");
                // Zend_Debug::dump($list);
                if(count($list)> 0)

                    foreach($list as $k => $v) {
                    $idata['ss']['ss_' . $k] = $v;
                }

                foreach($params['fname'] as $k => $v) {
                    if($params['ftype'][$k] == 'string' || $params['ftype'][$k] == "") {
                        $idata['ss']['ss_' . $v] = $params['fvalue'][$k];
                    } else if($params['ftype'][$k] == 'integer') {
                        $idata['is']['is_' . $v] = $params['fvalue'][$k];
                    } else if($params['ftype'][$k] == 'date') {
                        $vf = array();
                        $vf = explode(" ", $params['fvalue'][$k]);
                        //  Zend_Debug::dump($vf); die();
                        if(!isset($vf[1])) {
                            $vf[1] = "00:00:00";
                        }
                        $date = $vf[0] . "T" . $vf[1] . "Z";
                        $idata['ds']['ds_' . $v] = $date;
                    }
                }
                $idata['id'] = 'localhost' . "~" . $file['raw_url'];
                $idata['bundle'] = 'document';
                $idata['label'] = $params['docname'];
                $idata['content'] = $content;
                $idata['bundle_name'] = $file['raw_url'];
                $idata['entity_type'] = 'data-documents';
                $idata['entity_id'] = '3';
                $idata['ss']['ss_description'] = $params['doc_desc'];
                $idata['ss']['ss_httpurl'] = $file['http_url'];
                $idata['is']['is_uid'] = $identity->uid;
                //Zend_Debug::dump($idata);die();
                $update = $ff->insertdata($idata);
                //Zend_Debug::dump($update);die();
            }
            $result = array('retCode' => '00',
                            'retMsg' => 'add documents succed',
                            'result' => true,
                           );
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function updateatrAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $params = $this->getRequest()->getParams();
        //   Zend_Debug::dump($params); die();
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            // Zend_Debug::dump($_POST); die();
            $mdl_zusr = new Domas_Model_Prediksi();
            $delete = $mdl_zusr->add_update($_POST);
            if($delete['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$delete['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $delete = array('retCode' => '11',
                                'retMsg' =>$delete['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function updateatrvocaAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $params = $this->getRequest()->getParams();
        //   Zend_Debug::dump($params); die();
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            #Zend_Debug::dump($_POST); die();
            $mdl_zusr = new Domas_Model_Prediksi();
            $delete = $mdl_zusr->add_update_voca($_POST);
            if($delete['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$delete['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $delete = array('retCode' => '11',
                                'retMsg' =>$delete['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function updatedelvocaAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $params = $this->getRequest()->getParams();
        //   Zend_Debug::dump($params); die();
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            $mdl_zusr = new Domas_Model_Prediksi();
            $delete = $mdl_zusr->del_voc($_POST['id']);
            //  Zend_Debug::dump($delete); die();
            if($delete['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$delete['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $delete = array('retCode' => '11',
                                'retMsg' =>$delete['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function updatedelAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $params = $this->getRequest()->getParams();
        //   Zend_Debug::dump($params); die();
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            $mdl_zusr = new Domas_Model_Prediksi();
            $delete = $mdl_zusr->del_tax($_POST['id']);
            //  Zend_Debug::dump($delete); die();
            if($delete['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$delete['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $delete = array('retCode' => '11',
                                'retMsg' =>$delete['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function getformAction() {
        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($params);die();    
        $this->_helper->layout->disableLayout();
        $arr = array('tags',
                     'PREDIKSI');
        if(is_numeric($params['id'])) {
            $this->get_term_form($params['id']);
        } elseif(!in_array($params['id'], $arr)) {
            $this->get_voca($params['id']);
        } else {
            echo "";
        }
        die();
    }

    public function getformstopAction() {
        $params = $this->getRequest()->getParams();
        $this->_helper->layout->disableLayout();
        //Zend_Debug::dump($params);die();   
        //$arr = array('STOPWORD');
        if(is_numeric($params['id'])) {
            $this->get_stop_form($params['id']);
        } else {
            echo "";
        }
        die();
    }

    function get_voca($id) {
        $id = substr($id, 4);
        $cc = new Domas_Model_Prediksi();
        $vardat = $cc->get_vocabulary_by_id($id);
        echo ' <div class="portlet light">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-puzzle " style="color:white;"></i>
                                            <span class="caption-subject bold  uppercase" style="color:white;">Tambah Baru</span>
                                        </div>
                                        <div class="tools">
                                            
                                        </div>
                                    </div>
                                    <div class="portlet-body form" >
                                        <form class="form-horizontal" id="updating" role="form">
                                            <input type="hidden"  name="id" value="' . $id . '"/>
                                            <div class="form-body">
                                                 <div class="form-group">
                                                    <label class="col-md-3 control-label">Tipe</label>
                                                    <div class="col-md-9">
                                                        <select name="parent" class="form-control">
                                                           <option value=1>Prediksi</option>
                                                           <option value=0>Tagging</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Nama Prediksi</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="t_name" class="form-control" placeholder="Nama" value="' . $vardat['name'] . '"> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Deskripsi</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="t_desc" value="' . $vardat['description'] . '" class="form-control" placeholder="Deskripsi"> </div>
                                                </div> 
                                                
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Weight</label>
                                                    <div class="col-md-9">
                                                        <select name="weight" class="form-control">';
        for($i = 1;
        $i < 20;
        $i ++ ) {
            echo '<option value="' . $i . '">' . $i . '</i></option>';
        }
        echo '</select>
                                                    </div>
                                                </div>
                                               
                                            </div>
                                            <div class="form-actions right1">
                                            
                                                <button type="submit" class="btn green">Kirim</button>
                                                <button type="button" class="btn del red">Hapus</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>';
        echo "<script>
                
                $('.del').click(function(){
                        $('#ajax-loader').show();
                        $.ajax({
                                url:'/domas/prediksi/updatedelvoca',
                                content:'json',
                                type:'post',
                                data:$('form#updating').serialize(),
                                beforeSend:function(){
                                
                                },
                                complete:function(result){
                                    
                                },
                                success: function(data){
                                   $('#tax').jstree('refresh');
                                 
                                    if(data.result!=undefined){
                                        if(data.result==true){
                                            
                                        toastr.info('Delete succed');
                                       $('#ajax-loader').hide();
                                       
                                        }else{
                                            
                                        }
                                    }else{
                                        
                                    }
                                    $('#alertinfo').removeClass('hidden');
                                },
                                error:function(){
                                    $('#alertinfo').addClass('alert-info');
                                    $('#alertmsg').html('<strong>Error!</strong> System is busy, please try again.');
                                    $('#alertinfo').removeClass('hidden');
                                }
                            });
                    
                });
                  $('#ajax-loader').hide();
                
                $('form#updating').validate({
	            
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	                t_name: {
	                    required: true
	                }
	            },

	            messages: {
					
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
	               
					$('span.help-block').hide();
	                var errorlist = validator.errorList;
	                if(errorlist.length>0){
						for(var i = 0; i<errorlist.length; i++){
							//console.log(errorlist[i].element);
							var el = errorlist[i].element; 
							var next = $(el).next('span.help-block'); 
							$(next).slideDown();
						}
						$('.modal-scrollable:visible').animate({
							scrollTop: $(el).offset().top
						}, 800);
					}
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	            },

	            errorPlacement: function (error, element) {
					$('span.help-block').hide();
					var next = element.next('span.help-block'); 
				    if(error[0].textContent!=\" \"){
						$(next).children('span.label').html(\"<i class='fa fa-warning'></i> \"+ error[0].textContent);
						$(element).next().slideDown();
						$('.modal-scrollable:visible').animate({
							scrollTop: $(element).offset().top
						}, 800);
					}
	            },

	            submitHandler: function (form) {
				//	alert('ok');
                    $('#alertinfo').addClass('hidden');
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	               
                submitForm();
	                return false;
	            }
	        });

	        $('form#updating input').keypress(function (e) {
	            if (e.which == 13) {
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	                if ($('form#edituser').validate().form()) {
	                   //$('form#edituser').submit(); //form validation success, call ajax form submit
	                   submitForm();
	                }
	                return false;
	            }
	        });
            
            var submitForm = function(){
		
		$.ajax({
			url:'/domas/prediksi/updateatrvoca',
			content:'json',
			type:'post',
			data:$('form#updating').serialize(),
			beforeSend:function(){
				$('#alertinfo').addClass('hidden');
				$('#processbar').removeClass('hidden');
				
				$('#alertinfo').removeClass('alert-success');
				$('#alertinfo').removeClass('alert-warning');
				$('#alertinfo').removeClass('alert-danger');
				$('#alertinfo').removeClass('alert-info');
			},
			complete:function(result){
				$('#processbar').addClass('hidden');
			},
			success: function(data){
               $('#tax').jstree('refresh');
				//console.log(data);
				if(data.result!=undefined){
					if(data.result==true){
					toastr.info('Update succed');
					//	resetForm();
					}else{
						toastr.alert('Failed');
					}
				}else{
					$('#alertinfo').addClass('alert-info');
					$('#alertmsg').html('<strong>Error!</strong> System is busy, please try again.');
				}
				$('#alertinfo').removeClass('hidden');
			},
			error:function(){
				$('#alertinfo').addClass('alert-info');
				$('#alertmsg').html('<strong>Error!</strong> System is busy, please try again.');
				$('#alertinfo').removeClass('hidden');
			}
		});
	}
            
            </script>";
        die();
    }

    function get_term_form($id) {
        //Zend_Debug::dump($id);die();
        //die("xx");
        $cc = new Domas_Model_Prediksi();
        $data = $cc->prediksi_strc();
        $cc1 = new Model_Zparams();
        $icon = $cc1->get_icons();
        $vardat = $cc->get_term_data($id);
        ///  Zend_Debug::dump($data);die();
        if($vardat['hierarchy'] == 1 && $vardat['parent'] > 0) {
            $valparent = $vardat['parent'];
        } else {
            $valparent = "vid_" . $vardat['vid'];
        }
        $var = "";
        // echo $valparent;die();

        foreach($data as $k => $v) {
            if($k == "TAGGING") {
            } else if($k == "PREDIKSI") {
                // if(count($v['key'])> 0) {

                foreach($v['key'] as $k2 => $v2) {
                    $var .= '<option value="vid_' . $k2 . '">' . $data['PREDIKSI']['val'][$k2]['name'] . '</option>';

                    foreach($data['PREDIKSI']['key'][$k2][0][0] as $z3 => $v3) {
                        $sel = "";
                        if($valparent == $v3['pid']) {
                            $sel = "selected";
                        }
                        $var .= '<option ' . $sel . ' value="' . $v3['pid'] . '">-' . $v3['name'] . '</option>';
                        if(count($data['PREDIKSI']['key'][$k2][1][$v3['pid']])> 0) {

                            foreach($data['PREDIKSI']['key'][$k2][1][$v3['pid']] as $z4 => $v4) {
                                $sel = "";
                                if($valparent == $v4['pid']) {
                                    $sel = "selected";
                                }
                                $var .= '<option ' . $sel . ' value="' . $v4['pid'] . '">--' . $v4['name'] . '</option>';
                                if(count($data['PREDIKSI']['key'][$k2][2][$v4['pid']])> 0) {

                                    foreach($data['PREDIKSI']['key'][$k2][2][$v4['pid']] as $z5 => $v5) {
                                        $sel = "";
                                        if($valparent == $v5['pid']) {
                                            $sel = "selected";
                                        }
                                        $var .= '<option ' . $sel . ' value="' . $v5['pid'] . '">---' . $v5['name'] . '</option>';
                                        if(count($data['PREDIKSI']['key'][$k2][3][$v5['pid']])> 0) {

                                            foreach($data['PREDIKSI']['key'][$k2][3][$v5['pid']] as $z6 => $v6) {
                                                $sel = "";
                                                if($valparent == $v6['pid']) {
                                                    $sel = "selected";
                                                }
                                                $var .= '<option ' . $sel . ' value="' . $v6['pid'] . '">---' . $v6['name'] . '</option>';
                                                if(count($data['PREDIKSI']['key'][$k2][4][$v6['pid']])> 0) {

                                                    foreach($data['PREDIKSI']['key'][$k2][3][$v6['pid']] as $z7 => $v7) {
                                                        $sel = "";
                                                        if($valparent == $v7['pid']) {
                                                            $sel = "selected";
                                                        }
                                                        $var .= '<option ' . $sel . ' value="' . $v7['pid'] . '">---' . $v7['name'] . '</option>';
                                                        if(count($data['PREDIKSI']['key'][$k2][4][$v7['pid']])> 0) {

                                                            foreach($data['PREDIKSI']['key'][$k2][3][$v7['pid']] as $z8 => $v8) {
                                                                $sel = "";
                                                                if($valparent == $v8['pid']) {
                                                                    $sel = "selected";
                                                                }
                                                                $var .= '<option ' . $sel . ' value="' . $v8['pid'] . '">---' . $v8['name'] . '</option>';
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //}
        }
        #Zend_Debug::dump($var);die();
        echo '<link rel="stylesheet" type="text/css" href="/assets/core/global/plugins/select2/css/select2.min.css" media="screen" />
		      <link rel="stylesheet" type="text/css" href="/assets/core/global/plugins/select2/css/select2-bootstrap.min.css" media="screen" />
			  <script src = "/assets/core/global/plugins/select2/js/select2.full.min.js" type = "text/javascript"></script>
			  <script src = "/assets/core/pages/scripts/components-select2.min.js" type = "text/javascript"></script> 
			  <div class="portlet light">
                                    <div class="portlet-title"  >
                                        <div class="caption">
                                            <!--<i class="icon-puzzle font-grey-gallery" ></i>-->
											<i class="icon-puzzle"  style="color:white;" ></i>
                                            <span class="caption-subject bold  uppercase" style="color:white;">Tambah</span>
                                        </div>
                                        <div class="tools">
                                           
                                        </div>
                                    </div>
                                    <div class="portlet-body form">
                                       
                                        <form class="form-horizontal" id="updating" role="form">
                                            <input type="hidden"  name="id" value="' . $id . '"/>   
                                            <div class="form-body">
                                                 <div class="form-group">
                                                    <label class="col-md-3 control-label">Induk</label>
                                                    <div class="col-md-9">
                                                        <select name="parent"  id="pilihcat" class="form-control select2">' . $var . '  
                                                                    
                                                        </select>  
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Nama Prediksi</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="t_name" class="form-control" placeholder="Nama" value="' . $vardat['name'] . '"> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Deskripsi</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="t_desc" value="' . $vardat['description'] . '" class="form-control" placeholder="Deskripsi"> </div>
                                                </div> 
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Attr1</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="attr1" value="' . $vardat['attr1'] . '" class="form-control" placeholder="Attr1"> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Attr2</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="attr2" value="' . $vardat['attr2'] . '" class="form-control" placeholder="Attr2"> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Attr3</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="attr3" value="' . $vardat['attr3'] . '" class="form-control" placeholder="Attr3"> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Attr4</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="attr4" value="' . $vardat['attr4'] . '" class="form-control" placeholder="Attr4"> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Attr5</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="attr5" value="' . $vardat['attr5'] . '" class="form-control" placeholder="Attr5"> </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Class</label>
                                                    <div class="col-md-9">
                                                        <select name="class" class="form-control">';

        foreach($icon as $v) {
            echo '<option value="' . $v . '"><i class="fa ' . $v . '">' . $v . '</i></option>';
        }
        echo '</select>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Weight</label>
                                                    <div class="col-md-9">
                                                        <select name="weight" class="form-control">';
        for($i = 1;
        $i < 20;
        $i ++ ) {
            echo '<option value="' . $i . '">' . $i . '</i></option>';
        }
        echo '</select>
                                                    </div>
                                                </div>
												
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Rule</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="role" value="' . $vardat['role'] . '" class="form-control" placeholder="Role"> </div>
                                                </div>
                                               
                                            </div>
                                            <div class="form-actions right1">
                                            
                                                <button type="submit" class="btn green">Kirim</button>
                                                <button type="button" class="btn  del red">Hapus</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>';
        echo "<script>
                  
                			
                $('.del').click(function(){
                        $('#ajax-loader').show();
                        $.ajax({
                                url:'/domas/prediksi/updatedel',
                                content:'json',
                                type:'post',
                                data:$('form#updating').serialize(),
                                beforeSend:function(){
                                
                                },
                                complete:function(result){
                                    
                                },
                                success: function(data){
                                   $('#tax').jstree('refresh');
                                 
                                    if(data.result!=undefined){
                                        if(data.result==true){
                                            
                                        toastr.info('Delete succed');
                                       $('#ajax-loader').hide();
                                       
                                        }else{
                                            
                                        }
                                    }else{
                                        
                                    }
                                    $('#alertinfo').removeClass('hidden');
                                },
                                error:function(){
                                    $('#alertinfo').addClass('alert-info');
                                    $('#alertmsg').html('<strong>Error!</strong> System is busy, please try again.');
                                    $('#alertinfo').removeClass('hidden');
                                }
                            });
                    
                });
                  $('#ajax-loader').hide();
                
                $('form#updating').validate({
	            
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	                t_name: {
	                    required: true
	                }
	            },

	            messages: {
					
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
	               
					$('span.help-block').hide();
	                var errorlist = validator.errorList;
	                if(errorlist.length>0){
						for(var i = 0; i<errorlist.length; i++){
							//console.log(errorlist[i].element);
							var el = errorlist[i].element; 
							var next = $(el).next('span.help-block'); 
							$(next).slideDown();
						}
						$('.modal-scrollable:visible').animate({
							scrollTop: $(el).offset().top
						}, 800);
					}
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	            },

	            errorPlacement: function (error, element) {
					$('span.help-block').hide();
					var next = element.next('span.help-block'); 
				    if(error[0].textContent!=\" \"){
						$(next).children('span.label').html(\"<i class='fa fa-warning'></i> \"+ error[0].textContent);
						$(element).next().slideDown();
						$('.modal-scrollable:visible').animate({
							scrollTop: $(element).offset().top
						}, 800);
					}
	            },

	            submitHandler: function (form) {
				//	alert('ok');
                    $('#alertinfo').addClass('hidden');
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	               
                submitForm();
	                return false;
	            }
	        });

	        $('form#updating input').keypress(function (e) {
	            if (e.which == 13) {
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	                if ($('form#edituser').validate().form()) {
	                   //$('form#edituser').submit(); //form validation success, call ajax form submit
	                   submitForm();
	                }
	                return false;
	            }
	        });
            
            var submitForm = function(){
		
		$.ajax({
			url:'/domas/prediksi/updateatr',
			content:'json',
			type:'post',
			data:$('form#updating').serialize(),
			beforeSend:function(){
				$('#alertinfo').addClass('hidden');
				$('#processbar').removeClass('hidden');
				
				$('#alertinfo').removeClass('alert-success');
				$('#alertinfo').removeClass('alert-warning');
				$('#alertinfo').removeClass('alert-danger');
				$('#alertinfo').removeClass('alert-info');
			},
			complete:function(result){
				$('#processbar').addClass('hidden');
			},
			success: function(data){
               $('#tax').jstree('refresh');
				//console.log(data);
				if(data.result!=undefined){
					if(data.result==true){
					toastr.info('Update succed');
					//	resetForm();
					}else{
						toastr.alert('Failed');
					}
				}else{
					$('#alertinfo').addClass('alert-info');
					$('#alertmsg').html('<strong>Error!</strong> System is busy, please try again.');
				}
				$('#alertinfo').removeClass('hidden');
			},
			error:function(){
				$('#alertinfo').addClass('alert-info');
				$('#alertmsg').html('<strong>Error!</strong> System is busy, please try again.');
				$('#alertinfo').removeClass('hidden');
			}
		});
	}
            
            </script>";
    }

    function get_stop_form($id) {
        $cc = new Domas_Model_Prediksi();
        $data = $cc->stopword_strc2();
        //Zend_Debug::dump($data);die();
        $data2 = array();
        $i = 0;

        foreach($data as $k0 => $v0) {
            //Zend_Debug::dump($k0);
            //Zend_Debug::dump($v0);

            foreach($v0 as $k1 => $v1) {
                //Zend_Debug::dump($k1);
                //Zend_Debug::dump($v1);

                foreach($v1 as $k2 => $v2) {
                    // Zend_Debug::dump($k1);
                    // Zend_Debug::dump($v1);			  
                    $data2[$i] = $v2['name'];
                    $i ++;
                }
            }
        }
        // echo $valparent;die();
        echo ' <div class="portlet light" >
                                    <div class="portlet-title"  >
                                        <div class="caption">
                                            <!--<i class="icon-puzzle font-grey-gallery" ></i>-->
											<i class="icon-puzzle"  style="color:white;" ></i>																						
                                            <span class="caption-subject bold font-grey-gallery uppercase">' . $data2 . '</span>
                                        </div>
                                        <div class="tools">                  
                                        </div>
                                    </div>
                                    <div class="portlet-body form">                                     
                                        <form class="form-horizontal" id="updating" role="form">
                                            <input type="hidden"  name="id" value="' . $id . '"/>
                                            <div class="form-body">
                                                 <div class="form-group">
                                                    <label class="col-md-3 control-label">Induk</label>
                                                    <div class="col-md-9">
                                                        <select name="parent" class="form-control">' . $data2 . '                                                          
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Nama</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="t_name" class="form-control" placeholder="Nama" value="' . $vardat['name'] . '"> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Deskripsi</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="t_desc" value="' . $vardat['description'] . '" class="form-control" placeholder="Deskripsi"> </div>
                                                </div> 
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Attr1</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="attr1" value="' . $vardat['attr1'] . '" class="form-control" placeholder="Attr1"> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Attr2</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="attr2" value="' . $vardat['attr2'] . '" class="form-control" placeholder="Attr2"> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Attr3</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="attr3" value="' . $vardat['attr3'] . '" class="form-control" placeholder="Attr3"> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Attr4</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="attr4" value="' . $vardat['attr4'] . '" class="form-control" placeholder="Attr4"> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Attr5</label>
                                                    <div class="col-md-9">
                                                    <input type="text" name="attr5" value="' . $vardat['attr5'] . '" class="form-control" placeholder="Attr5"> </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Class</label>
                                                    <div class="col-md-9">
                                                        <select name="class" class="form-control">';

        foreach($icon as $v) {
            echo '<option value="' . $v . '"><i class="fa ' . $v . '">' . $v . '</i></option>';
        }
        echo '</select>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Weight</label>
                                                    <div class="col-md-9">
                                                        <select name="weight" class="form-control">';
        for($i = 1;
        $i < 20;
        $i ++ ) {
            echo '<option value="' . $i . '">' . $i . '</i></option>';
        }
        echo '</select>
                                                    </div>
                                                </div>
                                               
                                            </div>
                                            <div class="form-actions right1">
                                            
                                                <button type="submit" class="btn green">Kirim</button>
                                                <button type="button" class="btn  del red">Hapus</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>';
        echo "<script>
                
                $('.del').click(function(){
                        $('#ajax-loader').show();
                        $.ajax({
                                url:'/domas/prediksi/updatedel',
                                content:'json',
                                type:'post',
                                data:$('form#updating').serialize(),
                                beforeSend:function(){
                                
                                },
                                complete:function(result){
                                    
                                },
                                success: function(data){
                                   $('#tax').jstree('refresh');
                                 
                                    if(data.result!=undefined){
                                        if(data.result==true){
                                            
                                        toastr.info('Delete succed');
                                       $('#ajax-loader').hide();
                                       
                                        }else{
                                            
                                        }
                                    }else{
                                        
                                    }
                                    $('#alertinfo').removeClass('hidden');
                                },
                                error:function(){
                                    $('#alertinfo').addClass('alert-info');
                                    $('#alertmsg').html('<strong>Error!</strong> System is busy, please try again.');
                                    $('#alertinfo').removeClass('hidden');
                                }
                            });
                    
                });
                  $('#ajax-loader').hide();
                
                $('form#updating').validate({
	            
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	                t_name: {
	                    required: true
	                }
	            },

	            messages: {
					
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
	               
					$('span.help-block').hide();
	                var errorlist = validator.errorList;
	                if(errorlist.length>0){
						for(var i = 0; i<errorlist.length; i++){
							//console.log(errorlist[i].element);
							var el = errorlist[i].element; 
							var next = $(el).next('span.help-block'); 
							$(next).slideDown();
						}
						$('.modal-scrollable:visible').animate({
							scrollTop: $(el).offset().top
						}, 800);
					}
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	            },

	            errorPlacement: function (error, element) {
					$('span.help-block').hide();
					var next = element.next('span.help-block'); 
				    if(error[0].textContent!=\" \"){
						$(next).children('span.label').html(\"<i class='fa fa-warning'></i> \"+ error[0].textContent);
						$(element).next().slideDown();
						$('.modal-scrollable:visible').animate({
							scrollTop: $(element).offset().top
						}, 800);
					}
	            },

	            submitHandler: function (form) {
				//	alert('ok');
                    $('#alertinfo').addClass('hidden');
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	               
                submitForm();
	                return false;
	            }
	        });

	        $('form#updating input').keypress(function (e) {
	            if (e.which == 13) {
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	                if ($('form#edituser').validate().form()) {
	                   //$('form#edituser').submit(); //form validation success, call ajax form submit
	                   submitForm();
	                }
	                return false;
	            }
	        });
            
            var submitForm = function(){
		
		$.ajax({
			url:'/domas/prediksi/updateatr',
			content:'json',
			type:'post',
			data:$('form#updating').serialize(),
			beforeSend:function(){
				$('#alertinfo').addClass('hidden');
				$('#processbar').removeClass('hidden');
				
				$('#alertinfo').removeClass('alert-success');
				$('#alertinfo').removeClass('alert-warning');
				$('#alertinfo').removeClass('alert-danger');
				$('#alertinfo').removeClass('alert-info');
			},
			complete:function(result){
				$('#processbar').addClass('hidden');
			},
			success: function(data){
               $('#tax').jstree('refresh');
				//console.log(data);
				if(data.result!=undefined){
					if(data.result==true){
					toastr.info('Update succed');
					//	resetForm();
					}else{
						toastr.alert('Failed');
					}
				}else{
					$('#alertinfo').addClass('alert-info');
					$('#alertmsg').html('<strong>Error!</strong> System is busy, please try again.');
				}
				$('#alertinfo').removeClass('hidden');
			},
			error:function(){
				$('#alertinfo').addClass('alert-info');
				$('#alertmsg').html('<strong>Error!</strong> System is busy, please try again.');
				$('#alertinfo').removeClass('hidden');
			}
		});
	}
            
            </script>";
    }

    public function gethtmlAction() {
        //die('s');
        $cc = new Domas_Model_Prediksi();
        $data = $cc->taxonomy_strc();
        #Zend_Debug::dump($data);die();
        $this->_helper->layout->disableLayout();
        $colors = array("icon-state-success",
                        "icon-state-danger",
                        "icon-state-warning");
        $icons = array("fa fa-file",
                       "fa fa-folder",
                       "fa fa-desktop",
                       "fa fa-globe",
                       "fa fa-gear",
                       "fa-quote-left",
                       "fa fa-sitemap",
                       "fa fa-download");
        $arr_mt = array("Ekonomi" => "fa fa-money",
                        "Ideologi" => "fa fa-child",
                        "Pertahanan Keamanan" => "fa fa-shield",
                        "Politik" => "fa fa-balance-scale",
                        "Sosial Budaya" => "fa fa-group",
                        "Teknologi" => "fa fa-laptop",
                        "PANGAN" => "fa fa-leaf",
                        "MARITIM" => "fa fa-anchor",
                        "ENERGI" => "fa fa-bolt",
                        "INFRASTRUKTUR" => "fa fa-industry",
                       );
        $colors[array_rand($colors)];
        echo '<ul>';

        foreach($data as $k => $v) {
            if($k == "TAGGING") {
                
                /*
                 echo "<li class='level1' id='tags' data-jstree='{
                 \"icon\":\"fa fa-tags " . $colors[array_rand($colors)] . "\"}'>  " . $k;
                 echo ' <ul>';
                 foreach($v['key'] as $k2 => $v2) {
                 echo "<li class='level2' id='vid_" . $k2 . "' data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}'>     " . $data['TAGGING']['val'][$k2]['name'];
                 echo '<ul>';
                 foreach($data['TAGGING']['key'][$k2] as $k3 => $v3) {
                 $cnt = 0;
                 if($gr_tax[$v3['pid']] != "") {
                 $cnt = $gr_tax[$v3['pid']];
                 }
                 echo "<li data-vid='" . $k2 . "' id='" . $v3['pid'] . "'  data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " \"}'>" . $v3['name'] . '</li>';
                 
                 
                 }
                 echo '</ul>';
                 echo '</li>';
                 }
                 echo '</ul>';
                 echo "</li>";*/
            } else if($k == "PREDIKSI") {
                echo "<li class='tlevel1' id='PREDIKSI' data-jstree='{\"icon\":\"fa fa-sitemap " . $colors[array_rand($colors)] . "\"}'>" . $k;
                if(count($v['key'])> 0) {
                    echo ' <ul>';

                    foreach($v['key'] as $k2 => $v2) {
                        echo "<li class='tlevel2' id='vid_" . $k2 . "' data-jstree='{\"icon\":\"fa fa-globe " . $colors[array_rand($colors)] . "\"}'>" . $data['PREDIKSI']['val'][$k2]['name'];
                        // if (count($data['PREDIKSI']['key'][$k2][0][0]) > 0) {
                        echo '<ul>';

                        foreach($data['PREDIKSI']['key'][$k2][0][0] as $z3 => $v3) {
                            $iconv3 = $arr_mt[$v3['name']];
                            if($iconv3 == "") {
                                $iconv3 = $icons[array_rand($icons)];
                            }
                            echo "<li data-vid='" . $k2 . "' id='" . $v3['pid'] . "'  data-jstree='{\"icon\":\"" . $iconv3 . " " . $colors[array_rand($colors)] . "\"}'>" . $v3['name'];
                            if(count($data['PREDIKSI']['key'][$k2][1][$v3['pid']])> 0) {
                                echo "<ul>";

                                foreach($data['PREDIKSI']['key'][$k2][1][$v3['pid']] as $z4 => $v4) {
                                    //if ($z4 == $z3) {
                                    echo "<li data-jstree='{\"icon\":\"" . $iconv3 . " " . $colors[array_rand($colors)] . "\"}' data-vid='" . $k2 . "' id='" . $v4['pid'] . "'>" . $v4['name'];
                                    if(count($data['PREDIKSI']['key'][$k2][2][$v4['pid']])> 0) {
                                        echo "<ul>";

                                        foreach($data['PREDIKSI']['key'][$k2][2][$v4['pid']] as $z5 => $v5) {
                                            echo "<li data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}'  data-vid='" . $k2 . "' id='" . $v5['pid'] . "'>" . $v5['name'];
                                            if(count($data['PREDIKSI']['key'][$k2][3][$v5['pid']])> 0) {
                                                echo "<ul>";

                                                foreach($data['PREDIKSI']['key'][$k2][3][$v5['pid']] as $z6 => $v6) {
                                                    echo "<li data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}' data-vid='" . $k2 . "' id='" . $v6['pid'] . "'>" . $v6['name'];
                                                    if(count($data['PREDIKSI']['key'][$k2][4][$v6['pid']])> 0) {
                                                        echo "<ul>";

                                                        foreach($data['PREDIKSI']['key'][$k2][3][$v6['pid']] as $z7 => $v7) {
                                                            echo "<li data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}'  data-vid='" . $k2 . "' id='" . $v7['pid'] . "'>" . $v7['name'];
                                                            if(count($data['PREDIKSI']['key'][$k2][4][$v7['pid']])> 0) {
                                                                echo "<ul>";

                                                                foreach($data['PREDIKSI']['key'][$k2][3][$v7['pid']] as $z8 => $v8) {
                                                                    echo "<li data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}' data-vid='" . $k2 . "' id='" . $v8['pid'] . "'>" . $v8['name'];
                                                                    echo '</li>';
                                                                }
                                                                echo "</ul>";
                                                            }
                                                            echo '</li>';
                                                        }
                                                        echo "</ul>";
                                                    }
                                                    echo '</li>';
                                                }
                                                echo "</ul>";
                                            }
                                        }
                                        echo "</ul>";
                                    }
                                    echo '</li>';
                                }
                                echo "</ul>";
                            }
                            echo '</li>';
                        }
                        echo '</ul>';
                    }
                    echo '</li>';
                }
                echo ' <ul>';
                echo "</li>";
            }
        }
        echo '</ul>';
        die();
    }

    public function gethtmlstopwordAction() {
        $cc = new Domas_Model_Prediksi();
        $data = $cc->stopword_strc();
        //Zend_Debug::dump($data);die();
        $this->_helper->layout->disableLayout();
        $colors = array("icon-state-success",
                        "icon-state-danger",
                        "icon-state-warning");
        $icons = array("fa fa-file",
                       "fa fa-folder",
                       "fa fa-desktop",
                       "fa fa-globe",
                       "fa fa-gear",
                       "fa-quote-left",
                       "fa fa-sitemap",
                       "fa fa-download");
        $arr_mt = array("Ekonomi" => "fa fa-money",
                        "Ideologi" => "fa fa-child",
                        "Pertahanan Keamanan" => "fa fa-shield",
                        "Politik" => "fa fa-balance-scale",
                        "Sosial Budaya" => "fa fa-group",
                        "Teknologi" => "fa fa-laptop",
                        "PANGAN" => "fa fa-leaf",
                        "MARITIM" => "fa fa-anchor",
                        "ENERGI" => "fa fa-bolt",
                        "INFRASTRUKTUR" => "fa fa-industry",
                       );
        $colors[array_rand($colors)];
        echo '<ul>';

        foreach($data as $k => $v) {
            if($k == "TAGGING") {
                echo "<li class='level1' id='tags' data-jstree='{
                \"icon\":\"fa fa-tags " . $colors[array_rand($colors)] . "\"}'>  " . $k;
                echo ' <ul>';

                foreach($v['key'] as $k2 => $v2) {
                    echo "<li class='level2' id='vid_" . $k2 . "' data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}'>     " . $data['TAGGING']['val'][$k2]['name'];
                    echo '<ul>';

                    foreach($data['TAGGING']['key'][$k2] as $k3 => $v3) {
                        $cnt = 0;
                        if($gr_tax[$v3['pid']] != "") {
                            $cnt = $gr_tax[$v3['pid']];
                        }
                        echo "<li data-vid='" . $k2 . "' id='" . $v3['pid'] . "'  data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " \"}'>" . $v3['name'] . '</li>';
                        
                        /*
                         echo "<li class='flat' data-vid='" . $k2 . "' id='" . $v3['pid'] . "'  data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}'><div class='ftext'>as</div></li>";
                         */
                    }
                    echo '</ul>';
                    echo '</li>';
                }
                echo '</ul>';
                echo "</li>";
            } else if($k == "STOPWORD") {
                echo "<li class='tlevel1' id='STOPWORD' data-jstree='{\"icon\":\"fa fa-globe " . $colors[array_rand($colors)] . "\"}'>" . $k;
                if(count($v['key'])> 0) {
                    echo ' <ul>';

                    foreach($data['STOPWORD']['key'][0] as $k2 => $v2) {
                        // echo "<li class='tlevel2' id='vid_" . $k2 . "' data-jstree='{\"icon\":\"fa fa-globe " . $colors[array_rand($colors)] . "\"}'>111" . $data['STOPWORD']['val'][$k2]['name'];
                        // if (count($data['STOPWORD']['key'][$k2][0][0]) > 0) {
                        // echo '<ul>';

                        foreach($data['STOPWORD']['key'][0][$k2] as $z3 => $v3) {
                            //Zend_Debug::dump($v3['pid']);
                            //Zend_Debug::dump($data['STOPWORD']['key'][1]);
                            // $iconv3=$arr_mt[$v3['name'] ];
                            echo "<li data-vid='" . $k2 . "' id='" . $v3['stop_id'] . "'  data-jstree='{\"icon\":\"fa fa-globe " . $colors[array_rand($colors)] . "\"}'>" . $v3['name'];
                            if(count($data['STOPWORD']['key'][1][$v3['stop_id']])> 0) {
                                echo "<ul>";

                                foreach($data['STOPWORD']['key'][1][$v3['stop_id']] as $z4 => $v4) {
                                    //Zend_Debug::dump($v4);
                                    //if ($z4 == $z3) {
                                    //Zend_Debug::dump($v);
                                    echo "<li data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}' data-vid='" . $k2 . "' id='" . $v4['stop_id'] . "'>" . $v4['name'];
                                    if(count($data['STOPWORD']['key'][$k2][2][$v4['stop_id']])> 0) {
                                        echo "<ul>";

                                        foreach($data['STOPWORD']['key'][$k2][2][$v4['stop_id']] as $z5 => $v5) {
                                            echo "<li data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}'  data-vid='" . $k2 . "' id='" . $v5['pid'] . "'>" . $v5['name'];
                                            if(count($data['STOPWORD']['key'][$k2][3][$v5['stop_id']])> 0) {
                                                echo "<ul>";

                                                foreach($data['STOPWORD']['key'][$k2][3][$v5['stop_id']] as $z6 => $v6) {
                                                    echo "<li data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}' data-vid='" . $k2 . "' id='" . $v6['pid'] . "'>" . $v6['name'];
                                                    if(count($data['STOPWORD']['key'][$k2][4][$v6['stop_id']])> 0) {
                                                        echo "<ul>";

                                                        foreach($data['STOPWORD']['key'][$k2][3][$v6['stop_id']] as $z7 => $v7) {
                                                            echo "<li data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}'  data-vid='" . $k2 . "' id='" . $v7['pid'] . "'>" . $v7['name'];
                                                            if(count($data['STOPWORD']['key'][$k2][4][$v7['stop_id']])> 0) {
                                                                echo "<ul>";

                                                                foreach($data['STOPWORD']['key'][$k2][3][$v7['stop_id']] as $z8 => $v8) {
                                                                    echo "<li data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}' data-vid='" . $k2 . "' id='" . $v8['pid'] . "'>" . $v8['name'];
                                                                    echo '</li>';
                                                                }
                                                                echo "</ul>";
                                                            }
                                                            echo '</li>';
                                                        }
                                                        echo "</ul>";
                                                    }
                                                    echo '</li>';
                                                }
                                                echo "</ul>";
                                            }
                                        }
                                        echo "</ul>";
                                    }
                                    echo '</li>';
                                }
                                echo "</ul>";
                            }
                            echo '</li>';
                        }
                        // echo '</ul>';
                    }
                    echo '</li>';
                }
                echo ' <ul>';
                echo "</li>";
            }
        }
        echo '</ul>';
        die();
    }

    public function addAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/css/pendem.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.full.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2-bootstrap.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/infinitescroll/jquery.jscroll.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jstree/dist/themes/default/style.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jstree/dist/jstree_c.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-toastr/toastr.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-toastr/toastr.min.js');
        $params = $this->getRequest()->getParams();
        //    die("s");
        //$cz = new Domas_Model_General();
        //$this->view->sources = $cz->get_params_sources();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
    }

    public function emediaAction() {
        $params = $this->getRequest()->getParams();
        $cc = new Domas_Model_Pmedia();
        $this->view->data = $cc->get_a_media_by_id($params['id']);
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootbox/bootbox.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
    }

    public function setcrawlerAction() {
        $params = $this->getRequest()->getParams();
        $dd = new Domas_Model_Pmedia();
        $g = new Domas_Model_Prediksi();
        $this->view->date = $g->get_params_by_param("processing_date");
        if(isset($params['id'])&& $params['id'] != "") {
            $this->view->data = $dd->get_submedia_by_sub_id($params['id']);
        }
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-toastr/toastr.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-toastr/toastr.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/prabawf/metadata/css/basic.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/prabawf/js/jquery.simplemodal.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootbox/bootbox.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/selector/selectorator.js');
        $cc = new Domas_Model_Pmedia();
        $this->view->media = $cc->get_all_main_media();
    }

    public function getcrawlerAction() {
        $params = $this->getRequest()->getParams();
        try {
            $e = unserialize(base64_decode($params['e']));
            // Zend_Debug::dump($e); die();
            $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
            $this->view->headScript()->appendFile('/assets/core/global/plugins/selector/selectorator.js');
            // $this->_helper->layout->disableLayout();
            $c2 = new CMS_LIB_parse();
            $content = file_get_contents($e['url']);
            // echo $content; die();
            $content = $c2->remove($content, "<!--", "-->");
            $content = $c2->remove($content, "<style", "</style>");
            $content = $c2->remove($content, "<script", "</script>");
            $content = preg_replace('/\s+/', ' ', $content);
            // echo $content; die();
            $dom_1 = new Zend_Dom_Query($content);
            //$results = $dom_1->query("body");
            $this->view->html = $dom_1->getDocument();
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    public function listmediaAction() {
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/selector/selectorator.js');
        // $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        //Zend_Debug::dump($content); die();
        //$dom_1 = new Zend_Dom_Query($content);
        $params = $this->getRequest()->getParams();
        $this->view->varams = $params;
        $cc = new Domas_Model_Pmedia();
        $data = $cc->get_all_main_media();
        //   Zend_Debug::dump($data); die();
        $this->view->media = $data;
    }

    public function listcrawlAction() {
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/selector/selectorator.js');
        // $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        //Zend_Debug::dump($content); die();
        //$dom_1 = new Zend_Dom_Query($content);
        $params = $this->getRequest()->getParams();
        $this->view->varams = $params;
        $cc = new Domas_Model_Pmedia();
        $data = $cc->get_all_media();
        //   Zend_Debug::dump($data); die();
        $this->view->media = $data;
    }

    public function insertlaporanAction() {
        $mdl = new Domas_Model_Pubmediacrawler();
        $x = $mdl->pub_insert_laporan();
        die();
    }

    public function insertdigitalmediaAction() {
        $mdl = new Domas_Model_Pubmediacrawler();
        $x = $mdl->pub_insert_digital_media();
        die();
    }
}
