<?php

/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <himawijaya@gmail.com>, 24.01.2016
 */
#require_once (APPLICATION_PATH) . '/../library/CMS/Workflow/wftaskinterface.php'; 

class Domas_QController extends Zend_Controller_Action {

    public function init() {
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->initView();
    }

    public function sourcecodeAction() {
        if($_POST) {
            Zend_Debug::dump($_POST);
            die();
        }
        $params = $this->getRequest()->getParams();
        $key = "TelkomCare2015";
        $listapp = array(md5(implode('~',
                         array('TelkomCare-129',
                         '10.62.8.129',
                         'admin',
                         '4dm1n',
                         '/var/www/telkomcare')))=> array('/home/himawijaya/Dropbox/scriptsdev/sap/newprabacore/public/docs_scripts',
                         '10.62.8.129',
                         'admin',
                         '4dm1n',
                         '/var/www/telkomcare'),
                        );
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        if(isset($params['ajax'])) {
            $this->_helper->layout->disableLayout();
            // Zend_Debug::dump($listapp);//die();
            // Zend_Debug::dump($params);//die();
            $list = array();
            $counter = 1;
            if($params['ajax'] == '0') {

                foreach($listapp as $k => $v) {
                    $list[$counter - 1]['title'] = $v[0];
                    $list[$counter - 1]['key'] = $counter;
                    $list[$counter - 1]['folder'] = true;
                    $list[$counter - 1]['hideCheckbox'] = true;
                    $list[$counter - 1]['iconclass'] = "fa fa-home";
                    $list[$counter - 1]['tooltip'] = $v[0];
                    $line = scandir($v[0]);

                    foreach($line as $kk => $vv) {
                        if($kk > 1) {
                            if(is_dir($v[0] . "/" . $vv)) {
                                $folder = true;
                                $icon = "fa fa-folder";
                            } else {
                                $folder = false;
                                $icon = "fa fa-file";
                            }
                            $list[$counter - 1]['children'][] = array('title' =>$vv,
                                                                      'key' =>$counter.$idx,
                                                                      'folder' =>$folder,
                                                                      'hideCheckbox' => true,
                                                                      'iconclass' =>$icon,
                                                                      'tooltip' =>$str,
                                                                      'lazy' =>$folder,
                                                                      'md5' =>$k,
                                                                      'nd' => str_replace("/",
                                                                      "~",
                                                                     $v[0]. "/" .$vv));
                        }
                    }
                    $counter ++;
                }
            } else {
                if(isset($listapp[$params['ajax']])&& isset($params['node'])) {
                    $v = $listapp[$params['ajax']];
                    $line = scandir(str_replace("~", "/", $params['node']). 
                                                "");
                    $idx = 0;

                    foreach($line as $kk => $vv) {
                        if($kk > 1) {
                            if(is_dir(str_replace("~", "/", $params['node']). "/" . $vv)) {
                                $folder = true;
                                $icon = "fa fa-folder";
                            } else {
                                $folder = false;
                                $icon = "fa fa-file";
                            }
                            $list[] = array('title' =>$vv,
                                            'key' =>$counter.$idx,
                                            'folder' =>$folder,
                                            'hideCheckbox' => true,
                                            'iconclass' =>$icon,
                                            'tooltip' =>$str,
                                            'lazy' =>$folder,
                                            'md5' =>$params['ajax'],
                                            'nd' => str_replace("/",
                                            "~",
                                           $params['node']). "~" .$vv);
                        }
                    }
                    // Zend_Debug::dump($list);die();
                    fclose($errorStream);
                    fclose($stream);
                    $counter ++;
                }
            }
            header("Access-Control-Allow-Origin: *");
            header('Content-Type: application/json');
            echo json_encode($list);
            die();
        } else if(isset($params['open'])) {
            $this->_helper->layout->disableLayout();
            $txt = "";
            if(isset($params['open'])&& isset($params['node'])) {
                //$v = $listapp[$params['open']];
                $txt = file_get_contents(str_replace("~", "/", $params['node']));
                //Zend_Debug::dump($txt);die();
            }
            die(htmlentities($txt));
        }
        // $this->layout()->nestedLayout = "layouts/metronictopnav";
        // $this->layout('layouts/metronictopnav');
        // $this->layout()->setTemplate('metronictopnav');
        $this->view->headLink()->appendStylesheet('/phplib/fancytree/src/skin-lion/ui.fancytree.css');
        $this->view->headLink()->appendStylesheet('/phplib/contextmenu/css/jquery.contextMenu.css');
        $this->view->headLink()->appendStylesheet('/phplib/jquery-linedtextarea.css');
        $this->view->headScript()->appendFile('/phplib/contextmenu/js/jquery.contextMenu-1.6.5.js');
        $this->view->headScript()->appendFile('/phplib/fancytree/src/jquery.fancytree.js');
        $this->view->headScript()->appendFile('/phplib/fancytree/src/jquery.fancytree.childcounter.js');
        $this->view->headScript()->appendFile('/phplib/fancytree/src/jquery.fancytree.edit.js');
        $this->view->headScript()->appendFile('/phplib/contextmenu/jquery.fancytree.contextMenu.js');
        $this->view->headScript()->appendFile('/phplib/jquery-linedtextarea.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/codemirror/lib/codemirror.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/codemirror/lib/codemirror.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/codemirror/mode/php/php.js');
        // die("end");
    }

    public function addactAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            $mdl = new Model_Crontab();
            $update = $mdl->add_act($_POST, $identity->uid);
            if($update['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$update['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function detailcronAction() {
        try {
            $auth = Zend_Auth::getInstance();
            $identity = $auth->getIdentity();
        }
        catch(Exception $e) {
        }
        //Zend_Debug::dump($identity);die();
        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($params);die();
        if(!isset($params['id'])|| $params['id'] == '') {
            #	$this->_redirect('/messaging/managealerts');
        }
        $mdl_crn = new Model_Crontab();
        $cronalert = $mdl_crn->getAlertCrontab($params['id']);
        //Zend_Debug::dump($cronalert);die();
        if(!isset($cronalert['info'])) {
            #$this->_redirect('/messaging/managealerts');
        }
        $alert = array();
        $tmp1 = explode(" ", $cronalert['info']);
        $tmp2 = explode("|", $tmp1[1]);
        //Zend_Debug::dump($tmp2);die();
        $id = $tmp2[0];
        $user = $tmp2[1];
        $created_at = $tmp2[2];
        $tmp1 = explode(" ", $cronalert['cron']);
        $status = 'enable';
        $tmp2 = explode("/", $tmp1[5]);
        $config = $tmp1[0] . " " . $tmp1[1] . " " . $tmp1[2] . " " . $tmp1[3] . " " . $tmp1[4];
        $minute = $tmp1[0];
        $hour = $tmp1[1];
        $dayofmonth = $tmp1[2];
        $month = $tmp1[3];
        $dayofweek = $tmp1[4];
        if($tmp1[0] == '#disable') {
            $status = 'disable';
            $tmp2 = explode("/", $tmp1[6]);
            $config = $tmp1[1] . " " . $tmp1[2] . " " . $tmp1[3] . " " . $tmp1[4] . " " . $tmp1[5];
            $minute = $tmp1[1];
            $hour = $tmp1[2];
            $dayofmonth = $tmp1[3];
            $month = $tmp1[4];
            $dayofweek = $tmp1[5];
        }
        //Zend_Debug::dump($tmp2);die();
        $file = $tmp2[7];
        #	$msg = file_get_contents("http://10.62.8.133/cronalert/msg/".str_replace(".sh",".php",$file)."?get=msg");
        $msg = json_decode($msg);
        //Zend_Debug::dump($msg);die();
        #	$target = file_get_contents("http://10.62.8.133/cronalert/msg/".str_replace(".sh",".php",$file)."?get=target");
        $target = json_decode($target, true);
        //Zend_Debug::dump($target);die();
        $alert = array('id' =>$id,
                       'created by' =>$user,
                       'created at' =>$created_at,
                       'status' =>$status,
                       'message' =>$msg->msg,
                       'file' =>$file,
                       'config' =>$config,
                       'minute' =>$minute,
                       'hour' =>$hour,
                       'day of month' =>$dayofmonth,
                       'month' =>$month,
                       'day of week' =>$dayofweek,
                       'target' =>$target);
        //Zend_Debug::dump($alert);die();
        $this->view->alert = $alert;
        $this->view->act = $mdl_crn->get_action();
        $this->view->cact = $mdl_crn->get_act_action($id);
        $this->view->varams = $params;
    }

    public function addcronAction() {
        $this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/css/plugins.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/moment.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        #Zend_De
        //Zend_Debug::dump($identity);die();
        //Zend_Debug::dump($_POST);die();
        if($_POST && $_POST["machine_name"] != "") {
            $mdl_crn = new Model_Crontab();
            $mdl_api = new Model_Api();
            if(count($_POST["sch"])> 0) {

                foreach($_POST["sch"] as $k => $v) {
                    $cronconf = '';
                    switch($v) {
                        case 'hourly' : $cronconf = $mdl_crn->getcroninterval($_POST["tgl1"] . 
                                                                              ':00', $_POST["tgl2"] . 
                                                                              ':00', 'hour');
                        break;
                        case 'daily' : $cronconf = $mdl_crn->getcroninterval($_POST["tgl1"] . 
                                                                             ':00', $_POST["tgl2"] . 
                                                                             ':00', 'day');
                        break;
                        case 'weekly' : $cronconf = $mdl_crn->getcroninterval($_POST["tgl1"] . 
                                                                              ':00', $_POST["tgl2"] . 
                                                                              ':00', 'week');
                        break;
                        case 'monthly' : $cronconf = $mdl_crn->getcroninterval($_POST["tgl1"] . 
                                                                               ':00', $_POST["tgl2"] . 
                                                                               ':00', 'month');
                        break;
                        default : break;
                    }
                    if($cronconf == '') {
                        break;
                    }
                    //bug::dump($cronconf.' - '.$v);die();
                    $intcron = $mdl_crn->addcronalert($identity->uid, $cronconf, $_POST["machine_name"], str_replace(" ", "_", $_POST["tgl1"]). 
                                                                                                                     ':00', str_replace(" ", "_", $_POST["tgl2"]). 
                                                                                                                                        ':00', $v);
                }
            } else {
                if($_POST['cron_formula'] != "") {
                    $zvar = explode(" ", trim($_POST['cron_formula']));
                    // Zend_Debug::dump($zvar); die();
                    if(count($zvar)!= 5) {
                        $this->_flashMessenger->addMessage('Not cron formula|danger');
                        $this->_redirect('/domas/q/listcron');
                    }
                    $cronconf = $_POST['cron_formula'];
                    $v = 'custom';
                    $intcron = $mdl_crn->addcronalert($identity->uid, $cronconf, $_POST["machine_name"], str_replace(" ", "_", $_POST["tgl1"]). 
                                                                                                                     ':00', str_replace(" ", "_", $_POST["tgl2"]). 
                                                                                                                                        ':00', $v);
                    if($intcron['transaction'] == true) {
                        $this->_flashMessenger->addMessage($intcron['message'] . 
                                                           '|success');
                    } else {
                        $this->_flashMessenger->addMessage($intcron['message'] . 
                                                           '|danger');
                    }
                }
            }
            //die();
            $this->_redirect('/domas/q/listcron');
        }
    }

    public function addAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            $mdl = new Prabadata_Model_Zprabaqueue();
            $update = $mdl->add_action($_POST);
            if($update) {
                $_POST['id'] = $update['id'];
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$update['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function addqueueAction() {
        $this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        // Zend_Debug::dump($m2->list_page());die();
        if(!isset($params['row'])) {
            $params['row'] = 1;
        }
        $this->view->lay = $m->get_layout();
    }

    public function deletallAction() {
        $params = $this->getRequest()->getParams();
        $cc = new Domas_Model_Zprabaqueue();
        $data = $cc->delete_running_pid($params['id']);
        $this->_redirect('/domas/q/list/msg/' . 
                         urlencode($data['message']));
    }

    public function runningAction() {
        $params = $this->getRequest()->getParams();
        $cc = new Domas_Model_Zprabaqueue();
        $data = $cc->set_running_pid($params['id']);
        $this->_redirect('/domas/q/list/msg/' . 
                         urlencode($data['message']));
    }

    public function stopAction() {
        $params = $this->getRequest()->getParams();
        $cc = new Domas_Model_Zprabaqueue();
        $data = $cc->stop_running_pid($params['id']);
        $this->_redirect('/domas/q/list/msg/' . 
                         urlencode($data['message']));
    }

    public function listAction() {
        $this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/css/plugins.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $cc = new Domas_Model_Zprabaqueue();
        $data = $cc->get_running_temp();
        $data3 = $cc->get_running_pid();
        $data2 = $cc->get_workers();
        $this->view->data = $data;
        $this->view->data2 = $data2;
        $this->view->data3 = $data3;
    }

    public function listcronAction() {
        //  die("s");
        $this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/css/plugins.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        try {
            $auth = Zend_Auth::getInstance();
            $identity = $auth->getIdentity();
            $mdl_crn = new Model_Crontab();
            $params = $this->getRequest()->getParams();
            if(isset($params['id'])&& $params['id'] != '' && isset($params['act'])&& $params['act'] != '') {
                $check = $mdl_crn->checkIDCrontab($params['id']);
                if($check) {
                    switch($params['act']) {
                        case 'deleted' : $check = $mdl_crn->deleteCrontab($params['id']);
                        break;
                        case 'disabled' : $check = $mdl_crn->deactivateCrontab($params['id']);
                        break;
                        case 'enabled' : $check = $mdl_crn->activateCrontab($params['id']);
                        break;
                        default : break;
                    }
                }
            }
            $cronalert = $mdl_crn->getallAlertCrontab();
            $alert = array();

            foreach($cronalert as $k => $v) {
                $tmp1 = explode(" ", $v['info']);
                $tmp2 = explode("|", $tmp1[1]);
                $id = $tmp2[0];
                $user = $tmp2[1];
                $created_at = $tmp2[2];
                $start = $tmp2[3];
                $end = $tmp2[4];
                $interval = $tmp2[5];
                $tmp1 = explode(" ", $v['cron']);
                $status = 'enable';
                $tmp2 = explode("/", $tmp1[5]);
                if($tmp1[0] == '#disable') {
                    $status = 'disable';
                    $tmp2 = explode("/", $tmp1[6]);
                }
                $file = $tmp2[7];
                $alert[$id] = array('id' =>$id,
                                    'user' =>$user,
                                    'created_at' =>$created_at,
                                    'status' =>$status,
                                    'start' =>$start,
                                    'end' =>$end,
                                    'interval' =>$interval,
                                    'file' =>$file,
                                   );
            }
            $this->view->alert = $alert;
            $this->view->messages = $this->_flashMessenger->getMessages();
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }
    }
}
