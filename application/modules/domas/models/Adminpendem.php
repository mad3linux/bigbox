<?php


/**
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>,Jul 15, 2014
 *
 */

class Domas_Model_Adminpendem extends Zend_Db_Table_Abstract {

    public function add_file() {
    }

    public function del_tax($tid) {
        try {
            $this->_db->query("delete from zpraba_taxonomy_term_data where tid=?", $tid);
             return array('result'=>true);
        }
        catch(Exception $e) {
            return array('result'=>false);
        }
    }

    public function del_voc($tid) {
        try {
            $this->_db->query("delete from zpraba_taxonomy_vocabulary where vid=?", $tid);
            $this->_db->query("delete from zpraba_taxonomy_term_data where vid=?", $tid);
            return array('result'=>true);
        }
        catch(Exception $e) {
            return array('result'=>false);
        }
    }
    public function add_update($data) {
        //  echo substr($data['parent'], 4); die();
        $level = 0;
        if(is_numeric($data['parent'])) {
            $vdat = $this->_db->fetchRow("select * from zpraba_taxonomy_term_data where tid= ?", $data['parent']);
            $level = $vdat['level'] + 1;
        } else {
            $vdat = $this->_db->fetchRow("select * from zpraba_taxonomy_vocabulary where  vid=?", substr($data['parent'], 4));
        }
        // Zend_Debug::dump($vdat);
        $data['vid'] = $vdat['vid'];
        //Zend_Debug::dump($data);
        //die();
        if($data['id'] != 0) {
            $upd = array($data['vid'],
                        $data['t_name'],
                        $data['t_desc'],
                        $data['weight'],
                        $data['parent'],
                        $level,
                        $data['attr1'],
                        $data['attr2'],
                        $data['attr3'],
                        $data['attr4'],
                        $data['attr5'],
                        $data['class'],
                        $data['id']);
            $this->_db->query("update zpraba_taxonomy_term_data set vid=?,name=?, description=?,weight=?, parent=?,level=?,attr1=?,attr2=?,attr3=?,attr4=?, attr5=?,	icon_class=? where tid=?", $upd);
        } else {
            $upd = array($data['vid'],
                        $data['t_name'],
                        $data['t_desc'],
                        $data['weight'],
                        $data['parent'],
                        $level,
                        $data['attr1'],
                        $data['attr2'],
                        $data['attr3'],
                        $data['attr4'],
                        $data['attr5'],
                        $data['class'],
                        $data['id']);
            $this->_db->query("insert into  zpraba_taxonomy_term_data (vid,name, description,weight, parent,level,attr1,attr2,attr3,attr4, attr5,	icon_class) values(?, ?, ?,?,?,?,?,?,?,?,?,?)", array($data['vid'], $data['t_name'], $data['t_desc'], $data['weight'], $data['parent'], $level, $data['attr1'], $data['attr2'], $data['attr3'], $data['attr4'], $data['attr5'], $data['class']));
        }
        return array('result' => true,
                     'message' => 'update succed');
    }

    public function main_parent($var) {
        $qry = "select * from temp_metadata where ptype=?";
        try {
            $data = $this->_db->fetchAll($qry, $var);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            $data = array();
        }

        foreach($data as $v3) {
            $new[$v3['pname']] = $v3['id'];
        }
        return $new;
    }

    public function get_term_data($id) {
        $qry = "select a.*, b.hierarchy from zpraba_taxonomy_term_data a inner join zpraba_taxonomy_vocabulary b on a.vid=b.vid where a.tid=?";
        try {
            $data = $this->_db->fetchRow($qry, $id);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            $data = array();
        }
        return $data;
    }

    public function get_vocabulary_by_id($id) {
        $qry = "select * from zpraba_taxonomy_vocabulary where vid=?";
        try {
            $data = $this->_db->fetchRow($qry, $id);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            $data = array();
        }
        return $data;
    }

    public function get_taxonomy_vocabulary() {
        $qry = "select * from zpraba_taxonomy_vocabulary order by 1";
        try {
            $data = $this->_db->fetchAll($qry);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            $data = array();
        }
        return $data;
    }

    public function get_parent_hierarchy() {
        $sql = "select * from zpraba_taxonomy_vocabulary order by name asc";
        try {
            $item = $this->_db->fetchAll($sql);

            foreach($item as $k => $v) {
                $data = array();
                $new[$k] = $v;
                if($v['hierarchy'] == 1) {
                    $data = $this->_db->fetchAll("select * from  zpraba_taxonomy_term_data where vid=?", $v['vid']);

                    foreach($data as $k1 => $v1) {
                        $new2[$v1['level']][$v1['parent']][$v1['tid']] = $v1;
                    }
                    Zend_Debug::dump($new2);
                    die();
                    $new[$k]['children'] = $data;
                }
            }
            Zend_Debug::dump($new);
            die("xxx");
        }
        catch(Exception $e) {
        }
        return $new;
    }

    public function get_tags_hierarchy() {
        $sql = "select * from zpraba_taxonomy_vocabulary where hierarchy=0 order by name asc";
        try {
            $item = $this->_db->fetchAll($sql);

            foreach($item as $k => $v) {
                $data = array();
                $data = $this->_db->fetchAll("select * from  zpraba_taxonomy_term_data where vid=?", $v['vid']);
                // Zend_Debug::dump($data); die(); 
                $new[$k] = $v;
                $new[$k]['children'] = $data;
            }
        }
        catch(Exception $e) {
        }
        return $new;
    }

    public function get_a_taxonomy_vocabulary($id) {
        $qry = "select * from zpraba_taxonomy_vocabulary where vid=?";
        try {
            $data = $this->_db->fetchRow($qry, $id);
        }
        catch(Exception $e) {
            //Zend_Debug::dump($e->getMessage());
            $data = array();
        }
        return $data;
    }

    public function inserting_temp($data) {
        try {
            $content = array($data['contents'],
                            $data['pname'],
                            $data['parent'],
                            $data['taxonomy'],
                            $data['tags'],
                            $data['keychar'],
                            $data['ptype'],
                            $data['attrs1'],
                            $data['attrs2']);
            $this->_db->query("insert into temp_metadata (contents, pname, parent, taxonomy, tags, keychar, ptype, attrs1, attrs2) values (?, ?, ?, ?,?,?,?, ?, ? )", $content);
            return true;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }
    }

    public function taxonomy_strc2() {
        try {
            $sql = "select * from zpraba_taxonomy_vocabulary where hierarchy=0 order by hierarchy, vid ";
            $item = $this->_db->fetchAll($sql);

            foreach($item as $k => $v) {
                $z[$v['vid']] = $v;
                // $tags['key'][$v['vid']] = $nz;
            }
            //  Zend_Debug::dump($z); 
            // die();
            $tags['val'] = $z;
            $sql = "select * from zpraba_taxonomy_vocabulary where hierarchy=1 order by hierarchy, vid ";
            $item = $this->_db->fetchAll($sql);

            foreach($item as $k => $v) {
                $max = $this->_db->fetchOne("select max(level) from zpraba_taxonomy_term_data where vid=?", $v['vid']);
                for($i = 0;
                $i <= $max;
                $i ++ ) {
                    $data = array();
                    $newx = array();
                    $data = $this->_db->fetchAll("select * from zpraba_taxonomy_term_data where vid=? and level=?", array($v['vid'], $i));

                    foreach($data as $k2 => $v2) {
                        if($v2['parent'] == null) {
                            $v2['parent'] = 0;
                        }
                        $newx[$v2['parent']][$v2['tid']] = $v2;
                    }
                    $new['key'][$v['vid']][$i] = $newx;
                }
                $new['val'][$v['vid']] = $v;
            }
        }
        catch(Exception $e) {
            die($sql);
        }
        //Zend_Debug::dump($new); die();
        return array("TAGGING" =>$tags,
                     "TAXONOMY" =>$new);
    }

    public function taxonomy_strc() {
        try {
            $sql = "select * from zpraba_taxonomy_vocabulary where hierarchy=0 order by hierarchy, vid ";
            $item = $this->_db->fetchAll($sql);

            foreach($item as $k => $v) {
                $mx = array();
                $nx = $this->_db->fetchAll("select * from zpraba_taxonomy_term_data where vid=?", $v['vid']);
                $nz = array();

                foreach($nx as $ka => $va) {
                    $nz[$va['tid']] = $va;
                }
                $z[$v['vid']] = $v;
                $tags['key'][$v['vid']] = $nz;
            }
            //  Zend_Debug::dump($z); 
            // die();
            $tags['val'] = $z;
            $sql = "select * from zpraba_taxonomy_vocabulary where hierarchy=1 order by hierarchy, vid ";
            $item = $this->_db->fetchAll($sql);

            foreach($item as $k => $v) {
                $max = $this->_db->fetchOne("select max(level) from zpraba_taxonomy_term_data where vid=?", $v['vid']);
                for($i = 0;
                $i <= $max;
                $i ++ ) {
                    $data = array();
                    $newx = array();
                    $data = $this->_db->fetchAll("select * from zpraba_taxonomy_term_data where vid=? and level=?", array($v['vid'], $i));

                    foreach($data as $k2 => $v2) {
                        if($v2['parent'] == null) {
                            $v2['parent'] = 0;
                        }
                        $newx[$v2['parent']][$v2['tid']] = $v2;
                    }
                    $new['key'][$v['vid']][$i] = $newx;
                }
                $new['val'][$v['vid']] = $v;
            }
        }
        catch(Exception $e) {
            die($sql);
        }
        //Zend_Debug::dump($new); die();
        return array("TAGGING" =>$tags,
                     "TAXONOMY" =>$new);
    }

    public function updating() {
        $sql = "select * from zpraba_conn order by 1";
        try {
            $item = $this->_db->fetchAll($sql);

            foreach($item as $k => $v) {
                $data[$k] = $v;
                $data[$k]['conn'] = unserialize($v['conn_params']);
            }
        }
        catch(Exception $e) {
            die($sql);
        }
        $new = array();

        foreach($data as $v) {
            if($v['conn']['host'] != "") {
                $new[$v['conn']['host']][$v['conn']['adapter']] = $v['conn']['adapter'];
            }
        }
        $var = $this->main_parent('machine');
        //Zend_Debug::dump($new); die();

        foreach($new as $k => $v) {

            foreach($v as $v2) {
                $content = array("contents" => "",
                                 "pname" =>$v2,
                                 "parent" =>$var[$k],
                                 "taxonomy" => "",
                                 "tags" => "",
                                 "keychar" => "",
                                 "ptype" => "adapter",
                                 "attrs1" => "",
                                 "attrs2" => "");
                $this->inserting_temp($content);
            }
        }
        die();
    }
}
