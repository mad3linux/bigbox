<?php

/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class Content_Model_Content extends Zend_Db_Table_Abstract {

    public function get_applications_with_check() {
        $files = scandir(APPLICATION_PATH . 
                         '/modules/');
        unset($files[0]);
        $files[1] = 'core';
        //Zend_Debug::dump($files);
        $sql = "select * from zpraba_app";
        try {
            $data = $this->_db->fetchAll($sql);
            $new = array();

            foreach($data as $k => $v) {
                $new[$k] = $v;
                $new[$k]['deleted_module'] = true;
                if(in_array($v['s_app'], $files)) {
                    $new[$k]['deleted_module'] = false;
                }
            }
            //Zend_Debug::dump($new);die();	
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($sql);
        }
        return $new;
    }
}
