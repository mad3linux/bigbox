<?php

/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class Domas_Model_Crontab extends Zend_Db_Table_Abstract {

    function get_act_action($name) {
        try {
            return $this->_db->fetchAll("select a.* from zpraba_cron_acts a inner join zpraba_crons b on  a.cron_id =b.id where b.cname=?", $name);
        }
        catch(Exception $e) {
            return array();
        }
    }

    function get_distro() {
        $shell = shell_exec("cat /etc/*-release");
        $pos = strpos($shell, 'Ubuntu');
        if($pos !== false) {
            return 'www-data';
        } else {
            $pos1 = strpos($shell, 'CentOS');
            if($pos1 !== false) {
                return '';
            }
        }
    }

    function get_act_by_name($name) {
        try {
            return $this->_db->fetchRow("select * from zpraba_crons where cname=?", $name);
        }
        catch(Exception $e) {
            return false;
        }
    }

    function add_act($data, $uid) {
        try {
            $row = $this->get_act_by_name($data['id_act']);
            if($row) {
                $this->_db->query("delete from zpraba_cron_acts where cron_id =? ", $row['id']);
                $i = 0;
                $new = array();

                foreach($data['act'] as $z) {
                    if(isset($data['arr_input'][$i])&& $data['arr_input'][$i] != "") {
                        $xx = explode(",", $data['arr_input'][$i]);

                        foreach($xx as $zz) {
                            $zzz = explode("=", $zz);
                            $new[$zzz[0]] = $zzz[1];
                        }
                    }
                    $this->_db->query("insert into  zpraba_cron_acts (cron_id, act_id, uid, array_input, updated_date, raws) values (?, ?, ?, ?, now(), ?)	 ", array($row['id'], $z, $uid, serialize($new), $data['arr_input'][$i]));
                    $i ++;
                }
            }
            return array("result" => true,
                         "message" => "succed");
        }
        catch(Exception $e) {
            return array("result" => false,
                         "message" =>$e->getMessage());
        }
    }

    function get_action() {
        try {
            return $this->_db->fetchAll("select * from zpraba_action where act_type in (1, 2)");
        }
        catch(Exception $e) {
            return array();
        }
    }

    function get_cron4exec($cronid) {
        $c = new Model_Zprabawf();
        try {
            $data = $this->_db->fetchAll("select a.* from zpraba_cron_acts a inner join zpraba_crons b on  a.cron_id =b.id where b.cname=?", $cronid);
        }
        catch(Exception $e) {
            $data = array();
        }

        foreach($data as $z) {
            $out[$z['act_id']] = $c->get_wf_process($z['act_id'], unserialize($z['array_input']));
            $out[$z['act_id']]['mtime'] = date("Y-m-d H:i:s", mktime());
        }
        return $out;
    }
    //05 * * * * => setiap menit ke 5
    //05 05 * * * => setiap jam 05:05
    //05 05 5 * * => setiap jam 05:05 di hari ke 5 dalam satu bulan
    //05 05 5 5 * => setiap jam 05:05 di hari ke 5 dalam bulan ke 5
    //05 05 5 5 5 => setiap jam 05:05 di hari ke 5 dalam bulan ke 5 jika merupakan hari jumat

    function addCrontab($file, $user = "www-data") {
        $user = $this->get_distro();
        $shell = shell_exec("crontab -u " . 
                            $user . 
                            " " . 
                            $file);
        if($user == "") {
            $shell = shell_exec("crontab " . 
                                $file);
        }
        return $shell;
    }

    function getallCrontab($user = "www-data") {
        $user = $this->get_distro();
        $shell = shell_exec("crontab -u " . 
                            $user . 
                            " -l");
        if($user == "") {
            $shell = shell_exec("crontab  -l");
        }
        $shell = explode("\n", $shell);
        return $shell;
    }

    function getallNonAlertCrontab($user = "www-data") {
        $user = $this->get_distro();
        $shell = shell_exec("crontab -u " . 
                            $user . 
                            " -l");
        if($user == "") {
            $shell = shell_exec("crontab  -l");
        }
        $shell = explode("\n", $shell);
        //Zend_Debug::dump($shell);die();
        $cron = array();
        $idx = 0;

        foreach($shell as $k => $v) {
            $tmp = explode(' ', $v);
            $idx = $k;
            if(isset($tmp[0])&& $tmp[0] == '#alert') {
                break;
            }
            $cron[] = $v;
        }
        return $cron;
    }

    function getallAlertCrontab($user = "www-data") {
        $path = APPLICATION_PATH . '/../public/bincron/';
        $check = $this->checkIDCrontab($id);
        $cron = fopen($path . 
                      "prabacron", "w")or die("Unable to open file!");
        $shell = file_get_contents(realpath($path . 
                          "prabacron"));
                          echo $path . 
                          "prabacron";
        
       // Zend_Debug::dump($shell); die();
                  
        $shell = explode("\n", $shell);
        $cron = array();

        foreach($shell as $k => $v) {
            $tmp = explode(' ', $v);
            if(isset($tmp[0])&& $tmp[0] == '#alert') {
                $cron[] = array('info' =>$v,
                                'cron' =>$shell[$k+ 1]);
            }
        }
        return $cron;
    }

    function getallAlertCrontab_backup($user = "www-data") {
        $user = $this->get_distro();
        $shell = shell_exec("crontab -u " . 
                            $user . 
                            " -l");
        if($user == "") {
            $shell = shell_exec("crontab  -l");
        }
        $shell = explode("\n", $shell);
        $cron = array();

        foreach($shell as $k => $v) {
            $tmp = explode(' ', $v);
            if(isset($tmp[0])&& $tmp[0] == '#alert') {
                $cron[] = array('info' =>$v,
                                'cron' =>$shell[$k+ 1]);
            }
        }
        return $cron;
    }

    function getAlertCrontab($id, $user = "www-data") {
        $user = $this->get_distro();
        $shell = shell_exec("crontab -u " . 
                            $user . 
                            " -l");
        if($user == "") {
            $shell = shell_exec("crontab  -l");
        }
        $shell = explode("\n", $shell);
        $cron = array();
        #Zend_Debug::dump($shell);die();

        foreach($shell as $k => $v) {
            $tmp = explode(' ', $v);
            if(isset($tmp[0])&& $tmp[0] == '#alert') {
                //Zend_Debug::dump("(\|".$id."\|)");die();
                preg_match_all("(" . 
                                 $id . 
                                 "\|)", $tmp[1], $out);
                //die("/(\|".$id."\|)/");
                //Zend_Debug::dump($out);die();
                if(count($out[0])> 0) {
                    $cron = array('info' =>$v,
                                  'cron' =>$shell[$k+ 1]);
                }
            }
        }
        return $cron;
    }

    function checkIDCrontab($id, $user = "www-data") {
        $user = $this->get_distro();
        $shell = shell_exec("crontab -u " . 
                            $user . 
                            " -l");
        if($user == "") {
            $shell = shell_exec("crontab  -l");
        }
        //Zend_Debug::dump($shell);die();
        preg_match_all("(" . 
                         $id . 
                         "\|)", $shell, $out);
        //die("/(\|".$id."\|)/");
        //Zend_Debug::dump($out);die();
        return(count($out[0])> 0);
    }

    function getactiveAlertCrontab($user = "www-data") {
        $user = $this->get_distro();
        if($user == "") {
            $shell = shell_exec("crontab  -l");
        }
        $shell = shell_exec("crontab -u " . 
                            $user . 
                            " -l");
        $shell = explode("\n", $shell);
        $cron = array();

        foreach($shell as $k => $v) {
            $tmp = explode(' ', $v);
            if(isset($tmp[0])&& $tmp[0] == '#alert') {
                $tmp2 = explode(" ", $shell[$k + 1]);
                if(isset($tmp2[0])&& $tmp2[0] != '#disable') {
                    $cron[] = array('info' =>$v,
                                    'cron' =>$shell[$k+ 1]);
                }
            }
        }
        return $cron;
    }

    function getinactiveAlertCrontab($user = "www-data") {
        $user = $this->get_distro();
        if($user == "") {
            $shell = shell_exec("crontab  -l");
        }
        $shell = shell_exec("crontab -u " . 
                            $user . 
                            " -l");
        $shell = explode("\n", $shell);
        $cron = array();

        foreach($shell as $k => $v) {
            $tmp = explode(' ', $v);
            if(isset($tmp[0])&& $tmp[0] == '#alert') {
                $tmp2 = explode(" ", $shell[$k + 1]);
                if(isset($tmp2[0])&& $tmp2[0] == '#disable') {
                    $cron[] = array('info' =>$v,
                                    'cron' =>$shell[$k+ 1]);
                }
            }
        }
        return $cron;
    }

    function activateCrontab($id, $user = "www-data") {
        $user = $this->get_distro();
        $path = APPLICATION_PATH . '/../public/bincron/';
        $check = $this->checkIDCrontab($id);
        if($check) {
            $nonalert = $this->getallNonAlertCrontab();
            $alert = $this->getallAlertCrontab();
            //Zend_Debug::dump($alert);die();

            foreach($alert as $k => $v) {
                preg_match_all("(" . 
                                 $id . 
                                 "\|)", $v['info'], $out);
                //die("/(\|".$id."\|)/");
                //Zend_Debug::dump($out);die();
                $nonalert[] = $v['info'];
                if(count($out[0])> 0) {
                    $nonalert[] = str_replace("#disable ", "", $v['cron']);
                } else {
                    $nonalert[] = $v['cron'];
                }
            }
            //Zend_Debug::dump($alert);die('www');
            //Zend_Debug::dump($nonalert);die('www');
            //echo $path; die();
            $cron = fopen($path . 
                          "prabacron", "w")or die("Unable to open file!");

            foreach($nonalert as $k => $v) {
                fwrite($cron, $v . 
                       "\n");
            }
            fclose($cron);
            shell_exec("crontab -r");
            shell_exec("crontab " . 
                       $path . 
                       "prabacron");
            //Zend_Debug::dump($this->getallCrontab());die('www');
            return true;
        }
        return false;
    }

    function deactivateCrontab($id, $user = "www-data") {
        $user = $this->get_distro();
        $path = APPLICATION_PATH . '/../public/bincron/';
        $check = $this->checkIDCrontab($id);
        if($check) {
            $row = $this->get_act_by_name($id);
            $nonalert = $this->getallNonAlertCrontab();
            $alert = $this->getallAlertCrontab();
            //Zend_Debug::dump($alert); die();

            foreach($alert as $k => $v) {
                preg_match_all("(" . 
                                 $id . 
                                 "\|)", $v['info'], $out);
                $nonalert[] = $v['info'];
                if(count($out[0])> 0) {
                    //$nonalert[] = str_replace("#disable ","",$v['cron']);
                    $v['cron'] = str_replace("#disable ", "", $v['cron']);
                    $nonalert[] = "#disable " . $v['cron'];
                } else {
                    $nonalert[] = $v['cron'];
                }
            }
            $cron = fopen($path . 
                          "prabacron", "w")or die("Unable to open filex!");

            foreach($nonalert as $k => $v) {
                fwrite($cron, $v . 
                       "\n");
            }
            fclose($cron);
            shell_exec("crontab -r");
            shell_exec("crontab " . 
                       $path . 
                       "prabacron");
            $this->_db->query("update zpraba_crons set status=0 where id =?", $row['id']);
            //Zend_Debug::dump($this->getallCrontab());die('www');
            return true;
        }
        return false;
    }

    function deleteCrontab($id, $user = "www-data") {
        $user = $this->get_distro();
        $check = $this->checkIDCrontab($id);
        $path = APPLICATION_PATH . '/../public/bincron/';
        if($check) {
            try {
                $nonalert = $this->getallNonAlertCrontab();
                $alert = $this->getallAlertCrontab();
                //Zend_Debug::dump($alert);die();

                foreach($alert as $k => $v) {
                    preg_match_all("(" . 
                                     $id . 
                                     "\|)", $v['info'], $out);
                    if(count($out[0])<= 0) {
                        $nonalert[] = $v['info'];
                        $nonalert[] = $v['cron'];
                    }
                }
                $cron = fopen($path . 
                              "prabacron", "w")or die("Unable to open file!");

                foreach($nonalert as $k => $v) {
                    fwrite($cron, $v . 
                           "\n");
                }
                fclose($cron);
                shell_exec("crontab -r");
                shell_exec("crontab " . 
                           $path . 
                           "prabacron");
                $row = $this->get_act_by_name($id);
                $this->_db->query("delete from zpraba_crons where id=?", $row['id']);
                $this->_db->query("delete from zpraba_cron_acts where cron_id=?", $row['id']);
                unlink($path . 
                       "sh/" . 
                       $id . 
                       ".sh");
                unlink($path . 
                       "log/" . 
                       $id . 
                       ".log");
                return true;
            }
            catch(Exception $e) {
                Zend_Debug::dump($e);
                die();
            }
        }
        return false;
    }

    function getdatetimefromnow($interval = '+0 day') {
        $dt = date('Y-m-d-H-i-s', strtotime(' +1 month'));
        return explode('-', $dt);
    }

    function check_cron($id) {
        $cek = $this->_db->fetchRow("select * from zpraba_crons where cname=?", strtolower($id));
        if($cek) {
            return array('transaction' => false,
                         'message' => 'cron name is exist');
        }
        return array('transaction' => true);
        //return array('transaction'=>false, 'message'=>'cron name is exist');
    }

    function addcronalert($user, $config, $id, $start = '', $end = '', $interval = '') {
        $cek = $this->check_cron($id);
        if($cek['transaction'] == false) {
            return $cek;
        }
        
        /*
        try {
        $path = APPLICATION_PATH . '/../public/bincron/';
           $cron = fopen($path . 
                              "prabacron", "a");
                              
                              fwrite($cron, "#alert xxxxx");
        }catch(Exception $e) {
                Zend_Debug::dump($e->getMessage()); die();
        }                 
             */                 
        if($id != '') {
            try {
                $id = strtolower($id);
                $id = str_replace(" ", "", $id);
                //$sh = fopen($path."log/".$id.".log", "w") or die("Unable to open file!");
                $sh = fopen($path . 
                            "log/" . 
                            $id . 
                            ".log", "w");
                fclose($sh);
                shell_exec("chmod 775 " . 
                           $path . 
                           "log/" . 
                           $id . 
                           ".log");
                //$sh = fopen($path."sh/".$id.".sh", "w") or die("Unable to open file");
                $sh = fopen($path . 
                            "sh/" . 
                            $id . 
                            ".sh", "w");
                $txt = "#!/bin/sh" . "\n" . "\n" . '/usr/bin/php  ' . APPLICATION_PATH . '/../bin/cli.php cron execrunningcron id=' . $id . '  > ' . $path . 'log/' . $id . '.log';
                
                
                fwrite($sh, $txt);
                fclose($sh);
                shell_exec("chmod +x " . 
                           $path . 
                           "sh/" . 
                           $id . 
                           ".sh");
                //$cron = fopen($path."prabacron","a")or die("Unable to open file!");
                $cron = fopen($path . 
                              "prabacron", "a");
                fwrite($cron, "#alert " . 
                       $id . 
                       "|" . 
                       $user . 
                       "|" . 
                       date('Y-m-d_H:i:s'). 
                            "|" . 
                            $start . 
                            "|" . 
                            $end . 
                            "|" . 
                            $interval . 
                            "\n");
                fwrite($cron, $config . 
                       " " . 
                       $path . 
                       "sh/" . 
                       $id . 
                       ".sh" . 
                       "\n");
                fclose($cron);
                //die("crontab ".$path."prabacron");
                shell_exec("crontab -r");
                shell_exec("crontab " . 
                           $path . 
                           "prabacron");
                $this->_db->query("insert into zpraba_crons (cname, cron_formula, status,uid, start_cron, end_cron, `interval`, created_date) values (?,?,?,?, ?,?,?, now())", array($id, $config, 1, $user, $start, $end, $interval));
                return array('transaction' => true,
                             'message' =>$id. " Alert has been created!");
            }
            catch(Exception $e) {
                return array('transaction' => false,
                             'message' =>$e->getMessage());
            }
        }
    }

    function addcronalert_backup($user, $msg, $telegram, $email, $gtalk, $whatsapp, $sms, $config, $id, $start = '', $end = '', $interval = '') {
        $path = "/var/www/html/newprabacore/public/cronalert/";
        if(!isset($id)|| $id != null || $id != '' || file_exists($path . "msg/" . $id . ".php")) {
            $id = rand(). '_' . date('YmdHis');
        }
        if(!isset($user)|| $user == null || $user == '') {
            $user = md5(rand());
        }
        if(isset($msg)&& $msg != '') {
            //die($path."msg/".$id.".php");
            $sh = fopen($path . 
                        "msg/" . 
                        $id . 
                        ".php", "w");
            if($sh == false) {
                return "Unable to open file!";
            }
            $txt = "<?php" . "\n";
            $txt .= "if (!function_exists('curl_init')){die('Sorry cURL is not installed!');}" . "\n";
            $txt .= "\$msg=\"" . $msg . "\";" . "\n\n";
            $txt .= "if(isset(\$_GET['get']) && \$_GET['get']=='msg'){" . "\n";
            //$txt .= "\$this->_helper->layout->disableLayout();"."\n";
            $txt .= "\$ret['msg']=\$msg;" . "\n";
            $txt .= "header(\"Access-Control-Allow-Origin: *\");" . "\n";
            $txt .= "header('Content-Type: application/json');" . "\n";
            $txt .= "echo json_encode(\$ret);" . "\n";
            $txt .= "die();" . "\n";
            $txt .= "}else if(isset(\$_GET['get']) && \$_GET['get']=='target'){" . "\n";
            $txt .= "\$ret = array();" . "\n";
            if(isset($telegram)&& $telegram != '') {
                $txt .= "\$ret['telegram']='" . $telegram . "';" . "\n";
            }
            if(isset($whatsapp)&& $whatsapp != '') {
                $txt .= "\$ret['whatsapp']='" . $whatsapp . "';" . "\n";
            }
            if(isset($gtalk)&& $gtalk != '') {
                $txt .= "\$ret['gtalk']='" . $gtalk . "';" . "\n";
            }
            if(isset($email)&& $email != '') {
                $txt .= "\$ret['email']='" . $email . "';" . "\n";
            }
            if(isset($sms)&& $sms != '') {
                $txt .= "\$ret['sms']='" . $sms . "';" . "\n";
            }
            //$txt .= "\$this->_helper->layout->disableLayout();"."\n";
            $txt .= "header(\"Access-Control-Allow-Origin: *\");" . "\n";
            $txt .= "header('Content-Type: application/json');" . "\n";
            $txt .= "echo json_encode(\$ret);" . "\n";
            $txt .= "die();" . "\n";
            $txt .= "}" . "\n\n";
            if(isset($telegram)&& $telegram != '') {
                $txt .= "\n" . "echo \"=== TELEGRAM ===\".\"\\n\";" . "\n";
                $txt .= "\$telegram=\"" . $telegram . "\";" . "\n";
                $txt .= "\$post = array('act'=>'sendmsgtxt','target'=>\$telegram,'msg'=>\$msg);" . "\n";
                $txt .= "\$params = http_build_query(\$post);" . "\n";
                $txt .= "\$ch = curl_init();" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_URL,\"http://10.62.180.6/prahu/telegram/action.php\");" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_USERAGENT, \"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36\");" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_HEADER, 0);" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_POST,1);" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_POSTFIELDS,\$params);" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_RETURNTRANSFER, true);" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_TIMEOUT, 10);" . "\n";
                $txt .= "\$output = curl_exec(\$ch);" . "\n";
                $txt .= "curl_close(\$ch);" . "\n";
                //$txt .= "echo var_dump(\$output).\"<br><hr>\\n\\n\";"."\n";
                $txt .= "echo \"<br><hr>\\n\\n\";" . "\n";
            }
            if(isset($whatsapp)&& $whatsapp != '') {
                $txt .= "\n" . "echo \"=== WHATSAPP ===\".\"\\n\";" . "\n";
                $txt .= "\$whatsapp=\"" . $whatsapp . "\";" . "\n";
                $txt .= "\$post = array('act'=>'sendmessage','target'=>\$whatsapp,'msg'=>\$msg);" . "\n";
                $txt .= "\$post = array('act'=>'sendmsg','target'=>\$whatsapp,'msg'=>\$msg);" . "\n";
                $txt .= "\$params = http_build_query(\$post);" . "\n";
                $txt .= "\$ch = curl_init();" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_URL,\"http://10.62.180.6/prahu/wa/action.php\");" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_USERAGENT, \"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36\");" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_HEADER, 0);" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_POST,1);" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_POSTFIELDS,\$params);" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_RETURNTRANSFER, true);" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_TIMEOUT, 10);" . "\n";
                $txt .= "\$output = curl_exec(\$ch);" . "\n";
                $txt .= "curl_close(\$ch);" . "\n";
                $txt .= "echo var_dump(\$output).\"<br><hr>\\n\\n\";" . "\n";
            }
            if(isset($gtalk)&& $gtalk != '') {
                $txt .= "\n" . "echo \"=== GTALK ===\".\"\\n\";" . "\n";
                $txt .= "\$gtalk=\"" . $gtalk . "\";" . "\n";
                $txt .= "\$post = array('act'=>'sendmessage','target'=>\$gtalk,'msg'=>\$msg);" . "\n";
                $txt .= "\$params = http_build_query(\$post);" . "\n";
                $txt .= "\$ch = curl_init();" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_URL,\"http://10.62.180.6/prahu/gtalk/action.php\");" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_USERAGENT, \"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36\");" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_HEADER, 0);" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_POST,1);" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_POSTFIELDS,\$params);" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_RETURNTRANSFER, true);" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_TIMEOUT, 10);" . "\n";
                $txt .= "\$output = curl_exec(\$ch);" . "\n";
                $txt .= "curl_close(\$ch);" . "\n";
                $txt .= "echo var_dump(\$output).\"<br><hr>\\n\\n\";" . "\n";
            }
            if(isset($email)&& $email != '') {
                $txt .= "\n" . "echo \"=== EMAIL ===\".\"\\n\";" . "\n";
                $txt .= "\$email=\"" . $email . "\";" . "\n";
                $txt .= "require_once \"../Mail-1.2.0/Mail.php\";" . "\n";
                $txt .= "\$from = 'escort.telkom@gmail.com';" . "\n";
                $txt .= "\$to = '" . $email . "';" . "\n";
                $txt .= "\$subject = 'TelkomCare Alert';" . "\n";
                $txt .= "\$body = \"" . $msg . "\";" . "\n";
                $txt .= "\$headers = array('From' => \$from,'To' => \$to,'Subject' => \$subject);" . "\n";
                $txt .= "\$smtp = Mail::factory('smtp', array('host' => 'ssl://smtp.gmail.com','port' => '465','auth' => true,'username' => 'escort.telkom@gmail.com','password' => 'escorttelkom2013'));" . "\n";
                $txt .= "\$mail = \$smtp->send(\$to, \$headers, \$body);" . "\n";
                $txt .= "echo var_dump(\$mail).\"<br><hr>\\n\\n\";" . "\n";
            }
            if(isset($sms)&& $sms != '') {
                $txt .= "\n" . "echo \"=== SMS ===\".\"\\n\";" . "\n";
                $txt .= "\$sms=\"" . $sms . "\";" . "\n";
                $txt .= "\$post = array('msisdn'=>\$sms,'message'=>\$msg);" . "\n";
                $txt .= "\$params = http_build_query(\$post);" . "\n";
                $txt .= "\$ch = curl_init();" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_URL,\"http://servicebus.telkom.co.id:9001/TelkomSystem/SMSGateway/Services/ProxyService/smsBulk\");" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_USERAGENT, \"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36\");" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_HEADER, 0);" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_POST,1);" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_POSTFIELDS,\$params);" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_RETURNTRANSFER, true);" . "\n";
                $txt .= "curl_setopt(\$ch,CURLOPT_TIMEOUT, 10);" . "\n";
                $txt .= "\$output = curl_exec(\$ch);" . "\n";
                $txt .= "curl_close(\$ch);" . "\n";
                $txt .= "echo var_dump(\$output).\"<br><hr>\\n\\n\";" . "\n";
            }
            $txt .= "\n" . "?>";
            //die($txt);
            fwrite($sh, $txt);
            fclose($sh);
            $sh = fopen($path . 
                        "log/" . 
                        $id . 
                        ".log", "w")or die("Unable to open file!");
            fclose($sh);
            shell_exec("chmod 775 " . 
                       $path . 
                       "log/" . 
                       $id . 
                       ".log");
            shell_exec("chown admin " . 
                       $path . 
                       "log/" . 
                       $id . 
                       ".log");
            //chown($path."log/".$id.".log", "admin");
            $sh = fopen($path . 
                        "sh/" . 
                        $id . 
                        ".sh", "w")or die("Unable to open file!");
            $txt = "#!/bin/sh" . "\n" . "\n" . 'curl "http://10.62.180.6/cronalert/msg/' . $id . '.php" >> ' . $path . 'log/' . $id . '.log';
            fwrite($sh, $txt);
            fclose($sh);
            shell_exec("chmod +x " . 
                       $path . 
                       "sh/" . 
                       $id . 
                       ".sh");
            $cron = fopen($path . 
                          "mycrontab", "a")or die("Unable to open file!");
            fwrite($cron, "#alert " . 
                   $id . 
                   "|" . 
                   $user . 
                   "|" . 
                   date('Y-m-d_H:i:s'). 
                        "|" . 
                        $start . 
                        "|" . 
                        $end . 
                        "|" . 
                        $interval . 
                        "\n");
            fwrite($cron, $config . 
                   " " . 
                   $path . 
                   "sh/" . 
                   $id . 
                   ".sh" . 
                   "\n");
            fclose($cron);
            shell_exec("crontab -r");
            shell_exec("crontab " . 
                       $path . 
                       "mycrontab");
            return $id . " Alert has been created!";
        }
        return "Invalid parameter!";
    }

    function getcroninterval($start = 0, $end = 0, $interval = 'hour') {
        // $interval = monthly | weekly | daily | hourly
        $conf = array('minute' => array(),
                      //minute 0-59 'hour' => array(), //hour 0-23 'daymonth' => array(), //day of month 1-31 'month' => array(), //month 1-12 'dayweek' => array()//day of week 0-6 (sunday = 0)); //die($int); //echo $start."<br>";//die(); //echo $end."<br>";//die();$tstart= strtotime($start);$tend= strtotime($end);$tstart2= strtotime($start);$tend2= strtotime($end);$int= 1 ; //echo $tstart."<br>";//die(); //echo $tend."<hr>";//die(); if((int)$tstart> (int)$tend){ return false ; } else if((int)$tstart== (int)$tend){$config= "" ; switch($interval){ case 'minute' :$conf['minute'][]= "*" ;$conf['hour'][]= "*" ;$conf['daymonth'][]= "*" ;$conf['month'][]= "*" ;$conf['dayweek'][]= "*" ; break ; case 'hour' :$conf['minute'][]= date("i",
                     $tstart);$conf['hour'][]= "*" ;$conf['daymonth'][]= "*" ;$conf['month'][]= "*" ;$conf['dayweek'][]= "*" ; break ; case 'day' :$conf['minute'][]= date("i",
                     $tstart);$conf['hour'][]= date("H",
                     $tstart);$conf['daymonth'][]= "*" ;$conf['month'][]= "*" ;$conf['dayweek'][]= "*" ; break ; case 'week' :$conf['minute'][]= date("i",
                     $tstart);$conf['hour'][]= date("H",
                     $tstart);$conf['daymonth'][]= "*" ;$conf['month'][]= "*" ;$conf['dayweek'][]= date("N",
                     $tstart); break ; case 'month' :$conf['minute'][]= date("i",
                     $tstart);$conf['hour'][]= date("H",
                     $tstart);$conf['daymonth'][]= date("d",
                     $tstart);$conf['month'][]= "*" ;$conf['dayweek'][]= "*" ; break ; case 'year' :$conf['minute'][]= date("i",
                     $tstart);$conf['hour'][]= date("H",
                     $tstart);$conf['daymonth'][]= date("d",
                     $tstart);$conf['month'][]= date("m",
                     $tstart);$conf['dayweek'][]= "*" ; break ; default :$conf['minute'][]= date("i",
                     $tstart);$conf['hour'][]= date("H",
                     $tstart);$conf['daymonth'][]= "*" ;$conf['month'][]= "*" ;$conf['dayweek'][]= "*" ; break ; } //die(); } else { while((int)$tstart<= (int)$tend){ //echo $int."<br>";//die(); //echo $tstart."<br>";//die(); //echo $tstart.":".$tend.":".$int."<br>";//die(); if(! in_array(date('i',
                     $tstart),
                     $conf['minute'])){$conf['minute'][]= date('i',
                     $tstart); } if(! in_array(date('H',
                     $tstart),
                     $conf['hour'])){$conf['hour'][]= date('H',
                     $tstart); } if(! in_array(date('d',
                     $tstart),
                     $conf['daymonth'])){$conf['daymonth'][]= date('d',
                     $tstart); } if(! in_array(date('m',
                     $tstart),
                     $conf['month'])){$conf['month'][]= date('m',
                     $tstart); }$conf['dayweek'][0]= "*" ; //echo $interval."<br>";//die();$inc= strtotime(" +" .$int. " " .$interval,
                     $tstart2);$tstart= date('Y-m-d H:i:00',
                     $inc); //echo $int."<hr>";//die(); //echo $tstart."<hr>";//die();$tstart= strtotime($tstart);$int++ ; } }$ret[]= implode(",",
                     $conf['minute']);$ret[]= implode(",",
                     $conf['hour']);$ret[]= implode(",",
                     $conf['daymonth']);$ret[]= implode(",",
                     $conf['month']);$ret[]= implode(",",
                     $conf['dayweek']); //Zend_Debug::dump($conf);die(); return implode(" ",
                     $ret); } } 