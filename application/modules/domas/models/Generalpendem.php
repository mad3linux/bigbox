<?php

/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class Domas_Model_Generalpendem extends Zend_Db_Table_Abstract {

    public function get_a_action_instance($id) {
          // Zend_Debug::dump($data);die();
        try {

            $vdata = $this->_db->fetchRow("select *  from zpraba_action_instance where id=?", $id);
         } catch (Exception $e) {
        
        }
            return $vdata;
    }
    public function get_params_sources() {
        return array(
            '1' => 'data-sources',
            '2' => 'data-html',
            '3' => 'data-documents',
            '4' => 'data-apps'
            );
    }

    public function get_params_category($id) {
        $var = array(
            '1' => array('machine', 'schema', 'table', 'column', 'function', 'procedure'),
            '2' => array(),
            '3' => array('machine', 'document'),
            '4' => array('link', 'api')
            );
        return $var[$id];
    }

    public function get_tags_for_insert($data) {

        $new = array();
        $new2 = array();
        try {
            foreach ($data as $v) {
                if (is_numeric($v)) {
                    $new[] = $v;
                }
            }
            $quer = implode(",", $new);
            if ($quer) {
                $sql = "select * from zpraba_taxonomy_term_data where tid in ($quer) ";
                $datax = $this->_db->fetchAll($sql);

                foreach ($datax as $z) {
                    $new2[$z['vid']][$z['tid']] = $z['name'];
                }
            }
        } catch (Exception $e) {

            Zend_Debug::dump($e);
            die();
        }
        return $new2;
    }
     public function load_instance($data) {
          // Zend_Debug::dump($data);die();
        try {

            $vdata = $this->_db->fetchAll("select *  from zpraba_action_instance where action_id=?", $data['id']);
            

        } catch (Exception $e) {
            return array('result' => false, 'message' => $e->getMessage());
        }
            return array('result' => $vdata, 'message' => 'succed');
    }

    public function save_instance($data) {
        # Zend_Debug::dump($data);die();
        try {
            $i=0;
            foreach ($data['awal'] as $v) {
                $input[$v] = trim($data['akhir'][$i]);
                $i++;
            }
            //Zend_Debug::dump($input);die();
            $is = $this->_db->fetchRow("select *  from zpraba_action_instance where action_id=? and instance_name=?", array($data['id'], $data['ins_name']));
            if($is) {
            //    die("1");
                $this->_db->query("update zpraba_action_instance  set input_array=? where instance_name =? and action_id=?", array(
                    serialize($input),
                    $data['ins_name'],
                    $data['id']));

            } else {
              
                $this->_db->query("insert into zpraba_action_instance (instance_name, action_id, input_array) values (?, ?, ?)", array(
                    $data['ins_name'],
                    $data['id'],
                    serialize($input)
                    ));
            }

        } catch (Exception $e) {
            Zend_Debug::dump($e); die();
            return array('result' => false, 'message' => $e->getMessage());
        }
        return array('result' => true, 'message' => 'succed');
    }
    public function add_tax($data) {

        try {
            $this->_db->query("insert into zpraba_taxonomy_vocabulary (name, machine_name, description, hierarchy) values (?, ?, ?, ?)", $data);
        } catch (Exception $e) {
            return array('result' => false, 'message' => $e->getMessage());
        }
        return array('result' => true, 'message' => 'succed');
    }

    public function get_icons() {
        return array(
            'machine' => 'fa-linux',
            'schema' => 'fa-hdd-o',
            'table' => 'fa-table',
            'column' => 'fa-columns',
            'procedure' => 'fa-reorder',
            'function' => 'fa-reorder',
            'url' => 'fa-html5'
            );
    }

    public function get_description($id) {
        $vid = substr($id, 4); //die();
        try {

            $data = $this->_db->fetchRow("select * from zpraba_process where pid=?", $vid);
            //echo $data['class_type']; die();
            $vdata = $this->$data['class_type']($data);
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
        return $vdata;
    }

    private function Operator($data) {
        $sdata = unserialize($data['zparams_serialize']);
        //Zend_Debug::dump($sdata); die();
        switch ($sdata['OperatorType']) {
            case 'custom':
            return$this->get_reflection($data, $sdata);
            break;
        }
    }

    private function Start($data) {

        return "Start Action Metadata Workflow";
    }

    private function End($data) {

        return "End Action Metadata Workflow";
    }

    private function get_reflection($data, $sdata) {
        try {
            $r = new Zend_Reflection_Method($sdata['OperatorData']['model'], $sdata['OperatorData']['method']);
        //$docblock = $r->getDocblock();
            $desc = $r->getDocblock()->getShortDescription();
        } catch (Exception $e) {
            $desc= "";

        }
        return  $desc. "<br><div class='code'><pre>" . $r->getBody() . "</pre></div>";
    }

    public function del_node($id) {
        // echo ($id);die();
        $tr = strpos($id, 'vid_');

        if ($tr !== false) {

            $vid = substr($id, 4);
            $this->del_vocab($vid);
        } else {

            try {
                $this->_db->query("delete from zpraba_taxonomy_term_data where tid=?", $id);
                return array('result' => true, 'msg' => 'succes');
            } catch (Exception $e) {
                //Zend_Debug::dump($e); die();
                return array('result' => false, 'msg' => $e->getMessage());
            }
        }
    }

    public function add_node($data) {

        $tr = strpos($data['parent'], 'vid_');

        $vid_x = $data['parents'][(count($data['parents']) - 3)];
        // Zend_Debug::dump($data['parents']);die();
        $vid = substr($vid_x, 4);
        // echo $vid; die();
        $level = (int) count($data['parents']) - 3;

        if ($data['parent'] == 'TAGS' || $data['parent'] == 'TAXONOMY') {
            return $this->create_vocab($data);
        }


        if ($tr === false) {
            //create/update child   
            $parent = $data['parent'];
        } else {
            // create parent  
            $parent = 0;
        }

        if ($data['old'] == "New node") {
            //create new

            $idata = array($vid, $data['text'], $parent, $level);
            // die("cc");

            try {
                $this->_db->query("insert into zpraba_taxonomy_term_data (vid, name, parent, level) values (?, ?, ?, ?)", $idata);
                return array('result' => true, 'msg' => 'succes');
            } catch (Exception $e) {
                //  Zend_Debug::dump($e);die();
                return array('result' => false, 'msg' => $e->getMessage());
            }
        } else {

            $idata = array($vid, $data['text'], $parent, $level, $data['old']);
            //Zend_Debug::dump($idata);die();
            $this->_db->query("update zpraba_taxonomy_term_data set vid=?, name=?, parent=?, level=? where name=?", $idata);
            return array('result' => true, 'msg' => 'succes');
            //update 
        }
    }

    private function del_vocab($vid) {
        try {

            $this->_db->query("delete from zpraba_taxonomy_vocabulary where vid=?", $vid);

            return array('result' => true, 'msg' => 'succes');
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            die();
            return array('result' => false, 'msg' => $e->getMessage());
        }
    }

    private function create_vocab($data) {
        $hier = 0;
        if ($data['parent'] == 'TAXONOMY') {
            $hier = 1;
        }
        $pname = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $data['text']));
        $idata = array($data['text'], $pname, $hier);
        try {
            if ($data['old'] == "New node") {

                $this->_db->query("insert into zpraba_taxonomy_vocabulary (name, machine_name, hierarchy, module) values (?, ?, ?, 'taxonomy')", $idata);
            } else {
                $idata = array($data['text'], $pname, $hier, $data['old']);
                $this->_db->query("update zpraba_taxonomy_vocabulary set name=?, machine_name=?, hierarchy=? where name=?", $idata);
            }

            return array('result' => true, 'msg' => 'succes');
        } catch (Exception $e) {

            return array('result' => false, 'msg' => $e->getMessage());
        }
    }

    public function map_server($data) {
        $id = unserialize(base64_decode($data['id']));

        $id[3] = $data['raw0'];
        $cd = new Mdata_Model_Server();
        // Zend_Debug::dump($id); die();
        $cd->get_file_extract($id);
    }

}
