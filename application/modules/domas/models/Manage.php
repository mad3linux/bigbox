<?php

/*
 * Copyright (C) Prabtech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class Domas_Model_Manage extends Zend_Db_Table_Abstract {

    public function logs_keyws($data) {
        try {
            $this->_db->query("insert into z_keyw_logs (keyw) values (?)", $data);
        }
        catch(Exception $e) {
            return false;
        }
        return true;
    }

    public function add_update_xpath($data) {
        try {
            if(!empty($data['id'])) {
                $var = array($data['xname'],
                            $data['xurl'],
                            $data['docname'],
                            $data['title_type'],
                            $data['doc_desc'],
                            $data['doc_type'],
                             serialize($data),
                            $data['id']);
                $this->_db->query("update  zpraba_crawl_custom set cname=?, target_url=?, title=?, title_type=?, content=?, content_type=?, attrs=?, updated_date=now() where id=?", $var);
            } else {
                $var = array($data['xname'],
                            $data['xurl'],
                            $data['docname'],
                            $data['title_type'],
                            $data['doc_desc'],
                            $data['doc_type'],
                             serialize($data));
                $this->_db->query("insert into zpraba_crawl_custom (cname, target_url, title, title_type, content, content_type, attrs, updated_date) values (?, ?, ?, ?, ?, ?, ?, now())", $var);
            }
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
        //return $item;
    }

    public function list_xpath() {
        try {
            $data = $this->_db->fetchAll("select * from zpraba_crawl_custom");
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
        return $data;
    }

    public function get_a_xpath($id) {
        try {
            $data = $this->_db->fetchRow("select * from zpraba_crawl_custom where id=$id");
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
        return $data;
    }
}
