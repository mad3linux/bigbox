<?php
//ini_set('memory_limit', '-1');
//ini_set('max_execution_time', 3000);

class Domas_Model_Params extends Zend_Db_Table_Abstract {

    public function exclude_file() {
        return array('Admin.php',
                     'Messaging.php',
                     'Minbox',
                     'Prabasystemaddon.php',
                     'Zprabapage.php',
                     'Zprabawf.php');
    }

    public function get_params_by_key($key) {
        try {
            $z = new Model_Cache();
            $cache = $z->cachefunc(30000);
            $id = $z->get_id("Domas_Model_Params_bykey" . 
                             $key);
            $data = $z->get_cache($cache, $id);
            //$data = false;
            if(!$data) {
                $data = $this->_db->fetchAll("select * from zpraba_params where param='$key'");
                $cache->save($data, $id, array('domas'));
            }
        }
        catch(Exception $e) {
        }
        return $data;
    }

    public function get_params_pmas($key = false) {
        try {
            $z = new Model_Cache();
            $cache = $z->cachefunc(30000);
            $id = $z->get_id("Domas_Model_Params");
            $data = $z->get_cache($cache, $id);
            //$data = false;
            if(!$data) {
                $data = $this->_db->fetchAll("select * from zpraba_params where param='param_pmas'");
                $cache->save($data, $id, array('domas'));
            }
        }
        catch(Exception $e) {
        }
        if($key) {

            foreach($data as $v) {
                $new[$v['display_param']] = $v['value_param'];
            }
            return $new;
        }
        return $data;
    }

    public function update_params($data) {
        #Zend_Debug::dump($data); die();
        try {
            $this->_db->query("update zpraba_params set value_param=0 where param='param_pmas' and attr4='checkbox'");

            foreach($data as $k => $v) {
                $this->_db->query("update zpraba_params set value_param=? where display_param=?", array($v, $k));
            }
            $z = new Model_Cache();
            $cache = $z->cachefunc(30000);
            $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array("domas"));
        }
        catch(Exception $e) {
            return array("result" => false,
                         'message' =>$e->getMessage());
        }
        return array("result" => true,
                     'message' => 'succed');
    }
}
