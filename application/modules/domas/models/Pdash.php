<?php

class Domas_Model_Pdash extends Zend_Db_Table_Abstract {

    public function get_keys_pmas($keys) {
        try {
            // Zend_Debug::dump("select * from zpraba_taxonomy_term_data where parent=$keys and level=1");die();
            // $data = $this->_db->fetchAll("select * from zpraba_taxonomy_term_data where attr4=?", $keys);
            $data = array();
            if($keys != "") {
                $data = $this->_db->fetchAll("select * from zpraba_taxonomy_term_data where parent=? and level=1", $keys);
            }
            // Zend_Debug::dump($keys);
            // Zend_Debug::dump($data);
            // die();

            foreach($data as $v) {
                $new[] = $v['tid'];
            }
            // Zend_Debug::dump($new);     
            // die();
            return $new;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    public function get_keys1_pmas($keys) {
        try {
            // Zend_Debug::dump("select * from zpraba_taxonomy_term_data where parent=$keys and level=1");die();
            // $data = $this->_db->fetchAll("select * from zpraba_taxonomy_term_data where attr4=?", $keys);
            $data = array();
            if($keys != "") {
                $data = $this->_db->fetchAll("select * from zpraba_predictive_term_data where parent=? and level=1", $keys);
            }
            // Zend_Debug::dump($keys);
            // Zend_Debug::dump($keys);
            // die();

            foreach($data as $v) {
                $new[] = $v['tid'];
            }
            // Zend_Debug::dump($new);     
            // die();
            return $new;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    public function get_tid_by_vid($vid = 13) {
        try {
            $data = $this->_db->fetchAll("select tid from zpraba_taxonomy_term_data where vid=?", $vid);

            foreach($data as $v) {
                $new[] = $v['tid'];
            }
            //Zend_Debug::dump($new);     
            //die();
            return $new;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    public function get_tags_pmas() {
        try {
            // $data = $this->_db->fetchAll("select * from zpraba_taxonomy_term_data where vid=13");
            $data = $this->_db->fetchAll("select * from zpraba_taxonomy_term_data where vid=14 and parent=0 
order by weight");
            // Zend_Debug::dump($data); 
            // foreach($data as $v) {
            // $new[$v['attr4']][] = $v['name'];
            // }

            foreach($data as $v) {
                // $new[$v['attr4']][] = $v['name'];
                $new[$v['name']] = $v['tid'];
            }
            // Zend_Debug::dump($new);     
            // die();
            return $new;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    public function get_trainingset_pmas() {
        try {
            // $data = $this->_db->fetchAll("select * from zpraba_taxonomy_term_data where vid=13");
            $data = $this->_db->fetchAll("select * from zpraba_predictive_term_data where vid=14 and parent=0 
order by weight");
            // foreach($data as $v) {
            // $new[$v['attr4']][] = $v['name'];
            // }

            foreach($data as $v) {
                // $new[$v['attr4']][] = $v['name'];
                $new[$v['name']] = $v['pid'];
            }
            // Zend_Debug::dump($new);     
            // die();
            return $new;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    public function get_tags1_pmas($id) {
        try {
            // $data = $this->_db->fetchAll("select * from zpraba_taxonomy_term_data where vid=13");
            // $dataid = $this->_db->fetchOne("select pid from zpraba_predictive_term_data where LOWER(name)='".strtolower($id)."' order by weight");
            $data = $this->_db->fetchOne("select * from zpraba_taxonomy_term_data where tid=" . 
                                         $id);
            // Zend_Debug::dump($data);
            $role = "";
            $data1 = $this->_db->fetchAll("select * from zpraba_taxonomy_term_data where level=1 and parent=" . 
                                          $data);
            // $new[$v['attr4']][] = $v['name'];

            foreach($data1 as $v) {
                $new[$v['name']] = $v['tid'];
                $data2 = $this->_db->fetchAll("select * from zpraba_taxonomy_term_data where level=2 and parent=" . 
                                              $v['tid']);

                foreach($data2 as $v1) {
                    $role .= $v1['role'] . ",";
                }
            }
            $role = rtrim($role, ",");
            // Zend_Debug::dump($role);
            // die();
            $data = array($new,
                         $role);
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    public function get_logo() {
        try {
            $data = $this->_db->fetchAll("select * from pmas_media");

            foreach($data as $v) {
                if(trim($v['logo'])== '/assets/pmas/images/media/'||empty($v['logo'])) {
                             $new[$v['slugname']] = '/assets/pmas/images/media/default.png';
                } else {
                                     $new[$v['slugname']] = $v['logo'];

                }
            }
            return $new;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    public function get_stopword($t = 0) {
        $parent = 0;
        if($t == "laporan") {
            $parent = 3;
        } else if($t == "digmed") {
            $parent = 4;
        } else if($t == "socmed") {
            $parent = 1;
        } else if($t == "who") {
            $parent = 164;
        }
        // Zend_Debug::dump($parent);//die();
        // Zend_Debug::dump($parent);die();
        $sql = "select * from zpraba_stopword_term_data where 1=1 and parent=?";
        try {
            $data = $this->_db->fetchAll($sql, $parent);
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }
    }

    function get_tema_by_tema_id($temaid) {
        // die($temaid);
        $sWhere = "";
        if($temaid != "") {
            $sWhere = "and tid=" . $temaid;
        }
        $sql = "select * from zpraba_taxonomy_term_data where 1=1 and level=0 and vid=14 " . $sWhere;
        try {
            $data = $this->_db->fetchAll($sql);
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }
    }

    function get_topik_by_tema($id, $topik = "") {
        try {
            if($topik == "") {
                // die("i");
                $sql = "select * from zpraba_taxonomy_term_data where 1=1 and level=1 and parent=? ";
                // Zend_Debug::dump($sql);die();
                $data = $this->_db->fetchAll($sql, (int) $id);
            } else {
                // die("e");
                $sql = "select * from zpraba_taxonomy_term_data where 1=1 and level=1 and parent=? and tid=? ";
                $data = $this->_db->fetchAll($sql, array((int) $id, (int) $topik));
            }
            // Zend_Debug::dump($data);die();
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }
    }

    function get_subtema_by_tema($id) {
        $sql = "select * from zpraba_taxonomy_term_data where 1=1 and level=2 and parent=? ";
        try {
            $data = $this->_db->fetchAll($sql, (int) $id);
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }
    }

    function get_name($id) {
        $sql = "SELECT * FROM `zpraba_taxonomy_term_data` where parent =? and level = 2 and is_crawl = 1";
        try {
            $data = $this->_db->fetchAll($sql, (int) $id);
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }
    }
}
