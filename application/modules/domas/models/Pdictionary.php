<?php


class Domas_Model_Pdictionary extends Zend_Db_Table_Abstract {

   function count_listdictionary($app, $GET,$type) {
		// Zend_Debug::dump($arr_searchkey); die('jjjjjj');
		$aColumns = array('no',
											'kata',
											'bahasa',
											'topik',
											'sentiment');			
			/*
			 * Ordering
			 */
	 $sOrder = "";
		if (isset ( $_GET ['iSortCol_0'] )) {
			$sOrder = "ORDER BY  ";
			for($i = 1; $i < intval ( $_GET ['iSortingCols'] ); $i ++) {
				if ($_GET ['bSortable_' . intval ( $_GET ['iSortCol_' . $i] )] == "true") {
					$sOrder .= "`" . $aColumns [intval ( $_GET ['iSortCol_' . $i] )] . "` " . ($_GET ['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
				}
			}
			
			$sOrder = substr_replace ( $sOrder, "", - 2 );
			if ($sOrder == "ORDER BY") {
				$sOrder = "";
			}
		}
			
		/*
		 * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
		 */
		// $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
		$whapp = "";
		$sWhere = " WHERE 1=1  ";
		// if($app != "") {
				// $whapp = "AND app = '$app'";
		// }
		if(isset($type)&&$type!=""){
						$sType .=  "AND `bahasa` LIKE '%" . strtoupper($type) . "%'";
		}
		
		if(isset($GET['sSearch'])&& $GET['sSearch'] != "") {
				$sWhere .= " AND (";
				for($i = 1;
				$i < count ($aColumns );
				$i ++ ) {
						$sWhere .=  "`" . $aColumns [$i] . "` LIKE '%" . strtoupper($GET['sSearch']) . "%' OR ";
				}
				// die($sWhere);
				// 
				$sWhere = substr_replace($sWhere, "", - 3);
				$sWhere .= ')';
		}
		
		/* Individual column filtering */
		for($i = 1;
		$i < count ($aColumns );
		$i ++ ) {
				if(isset($GET['bSearchable_' . $i])&& $GET['bSearchable_' . 
								 $i] == "true" && $GET['sSearch_' . 
								 $i] != '') {
						if($sWhere == "") {
								$sWhere = "WHERE ";
						} else {
								$sWhere .= " AND ";
						}
						$sWhere .= "`" . $aColumns [$i] . "` LIKE '%" . strtoupper($_GET ['sSearch_' . $i]) . "%' ";
				}
		}
			
		if(isset($app)&&$app!=""){
			$arr_searchkey = explode(",",$app);
			 $sName.= " AND (";
				for($i = 1;
				$i < count ($aColumns );
				$i ++ ) {
					foreach($arr_searchkey as $k=>$v){
						$sName .=  "`" . $aColumns [$i] . "` LIKE '%" . strtoupper($v) . "%' OR ";
					}
				}
				// die($sWhere);
				// 
				$sName = substr_replace($sName, "", - 3);
				$sName .= ')';
		}
			
	// Zend_Debug::dump($sName);die('cek');
			
			
		$qry = "SELECT 
			count(id) as rowsd 
		FROM 
		zpraba_dictionary 	 " . $sWhere . "  " . $sType . " " .$sName . " " . $whapp . " " . $sOrder  ;
		try {
			$qry = str_ireplace("`", "", $qry);
			// Zend_Debug::dump($qry);die();
				$data = $this->_db->fetchOne($qry);
				// Zend_Debug::dump($sWhere);die();
				return $data;
		}
		catch(Exception $e) {
				Zend_Debug::dump($e->getMessage());
				die($q);
		}
	}
	
 function get_listdictionary($app, $GET,$type) {
			$aColumns = array('no',
											'kata',
											'bahasa',
											'topik',
											'sentiment');		
			
			/*
			 * Paging
			 */
	 
			$sLimit = "";
			if(isset($GET['iDisplayStart'])&& $GET['iDisplayLength'] != '-1') {
					$sLimit = "LIMIT " . intval($GET['iDisplayStart']). ", " . intval($GET['iDisplayLength']);
			}
			
			/*
			 * Ordering
			 */
		 $sOrder = "";
	if (isset ( $_GET ['iSortCol_0'] ) && intval ( $_GET ['iSortCol_0'] ) > 0) {
		$sOrder = "ORDER BY  ";
		for($i = 1; $i < intval ( $_GET ['iSortingCols'] ); $i ++ )
		//for ( $i=1 ; $i<count($aColumns) ; $i++ )
		{ 

			if ($_GET ['bSortable_' . intval ( $_GET ['iSortCol_' . $i] )] == "true") {
				$sOrder .= "`" . $aColumns [intval ( $_GET ['iSortCol_' . $i] )] . "` " . ($_GET ['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
			}
		}
		
		$sOrder .= "`" . $aColumns [intval ( $_GET ['iSortCol_0'] )] . "` " . ($_GET ['sSortDir_0'] === 'asc' ? 'asc' : 'desc') . ", ";
		// echo $sOrder.'<br>';
		
		$sOrder = substr_replace ( $sOrder, "", - 2 );
		if ($sOrder == "ORDER BY") {
			$sOrder = "";
		}
	}else{
		$sOrder = "ORDER BY  id";
		
	}
			
			/*
			 * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
			 */
			// $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
			$sWhere = " WHERE 1=1  ";
			
			if(isset($type)&&$type!=""){
						$sType .=  "AND `bahasa` LIKE '%" . strtoupper($type) . "%'";
		}
			
			if(isset($GET['sSearch'])&& $GET['sSearch'] != "") {
					$sWhere .= "  AND (";
					for($i = 1;
					$i < count ($aColumns );
					$i ++ ) {
							$sWhere .= "`" . $aColumns [$i] . "` LIKE '%" . strtoupper($GET['sSearch']) . "%' OR ";
					}
					// die($sWhere);
					$sWhere = substr_replace($sWhere, "", - 3);
					$sWhere .= ')';
			}
			// if($app != "") {
					// $whapp = "AND app = '$app'";
			// }
			//die($sWhere);
			
			/* Individual column filtering */
			for($i = 1;
			$i < count ($aColumns );
			$i ++ ) {
					if(isset($GET['bSearchable_' . $i])&& $GET['bSearchable_' . 
									 $i] == "true" && $_GET['sSearch_' . 
									 $i] != '') {
							if($sWhere == "") {
									$sWhere = "WHERE ";
							} else {
									$sWhere .= " AND ";
							}
						 $sWhere .= "`" . $aColumns [$i] . "` LIKE '%" . strtoupper($_GET ['sSearch_' . $i]) . "%' ";
					}
			}
			
			if(isset($app)&&$app!=""){
				$arr_searchkey = explode(",",$app);
				 $sName.= " AND (";
					for($i = 1;
					$i < count ($aColumns );
					$i ++ ) {
						foreach($arr_searchkey as $k=>$v){
							$sName .=  "`" . $aColumns [$i] . "` LIKE '%" . strtoupper($v) . "%' OR ";
						}
					}
					// die($sWhere);
					// 
					$sName = substr_replace($sName, "", - 3);
					$sName .= ')';
			}
			// echo $sWhere.'<br>';
			// echo $sOrder.'<br>';
			// echo $sLimit.'<br>';
			// die();
			$qry = "SELECT *
			FROM zpraba_dictionary 
				 " . $sWhere . " " . $sType . " " . $sName . " " . $whapp . " " . $sOrder . " " . $sLimit;
			 // die($qry);
			try {
				$qry = str_ireplace("`", "", $qry);
					$data = $this->_db->fetchAll($qry);
					// Zend_Debug::dump($qry);die();
					return $data;
			}
			catch(Exception $e) {
					Zend_Debug::dump($e->getMessage());
					die($q);
			}
	}

	
 
    public function get_dictionary($bahasa="") {
		$sWhere ="";
		if($bahasa!=""){
			$sWhere ="and bahasa = '".$bahasa."'";
		}
		
        try {
            $sql = "select * from zpraba_dictionary where 1=1 ".$sWhere." order by id desc limit 10";

            $data = $this->_db->fetchAll($sql);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
        return $data;
    }
		
		
	
	public function get_edit_dict($id_data="") {
		$sWhere ="";
		if($id_data!=""){
			$sWhere ="and id = ".$id_data;
		}
		
        try {
            $sql = "select * from zpraba_dictionary where 1=1 ".$sWhere."";
            $data = $this->_db->fetchAll($sql);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
        return $data;
    }
	
	public function get_topik() {
		
        try {
            $sql = "select tid,name from zpraba_taxonomy_term_data where level=2";
            $data = $this->_db->fetchAll($sql);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
        return $data;
    }
	
	
	public function add_dictionary($data) {
        try {
			//Zend_Debug::dump($data); die();
            if($data['id'] == "") {
             $this->_db->query("insert into zpraba_dictionary (kata, bahasa,topik, sentiment) values (?, ?, ?, ?)", array($data['kata'],$data['bahasa'], $data['topik'], $data['sentiment']));  
                return $this->_db->lastInsertId();
            } else {
                $this->_db->query("update zpraba_dictionary set kata=?, bahasa=?, topik=?, sentiment=?  where id=?", array($data['kata'],$data['bahasa'], $data['topik'],$data['sentiment'],$data['id']));
                // return $this->_db->lastInsertId();
                return $data['id'];
            }
        }   
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
	}
		
	public function add_predictive($data) {
        try {
			// Zend_Debug::dump($data);
			// Zend_Debug::dump("insert into zpraba_predictive (id, text, tema, topik, prediksi, nilai) values (?, ?, ?, ?, ?, ?)", array(null,$data['text'], $data['tema'], $data['topik'], $data['prediksi'], $data['nilai'])); die('as');
            if($data['id'] == "") {
             $this->_db->query("insert into zpraba_predictive (id, id_content,text, tema, topik, prediksi, nilai) values (?, ?, ?, ?, ?, ?, ?)", array(null, $data['id_content'],$data['text'], $data['tema'], $data['topik'], $data['prediksi'], $data['nilai']));  
               //return $this->_db->lastInsertId();
            } else {
                $this->_db->query("update zpraba_predictive set text=?, tema=?, topik=?, nilai=?  where id_content=?", array($data['id_content'],$data['text'], $data['tema'],$data['topik'],$data['nilai']));
                // return $this->_db->lastInsertId();
               // return $data['id'];
            }
        }   
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
		
 }
	
}
