<?php


/**
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>,Jul 15, 2014
 *
 */

class Domas_Model_Prediksi extends Zend_Db_Table_Abstract {

    public function add_file() {
    }

    public function get_params_by_param($params) {
        try {
            $sql = "select * from zpraba_params where param='$params' ";
            $data = $this->_db->fetchAll($sql, array($ctr, $act));
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($sql);
        }
    }

    public function del_tax($pid) {
        try {
            $this->_db->query("delete from zpraba_predictive_term_data where pid=?", $pid);
            return array('result' => true);
        }
        catch(Exception $e) {
            return array('result' => false);
        }
    }

    public function del_voc($pid) {
        try {
            $this->_db->query("delete from zpraba_predictive_vocabulary where vid=?", $pid);
            $this->_db->query("delete from zpraba_predictive_term_data where vid=?", $pid);
            return array('result' => true);
        }
        catch(Exception $e) {
            return array('result' => false);
        }
    }
     public function add_update_voca($data) {
       //  Zend_Debug::dump($data); die();
        // echo substr($data['parent'], 4); die();
       try{
        //die();
        if($data['id'] != "") {
            $upd = array($data['t_name'],
                        $data['t_name'],
                        $data['t_desc'],
                        $data['weight'],
                        substr($data['parent'], 4),
                        $data['id']);
            $this->_db->query("update zpraba_predictive_vocabulary set name=?, machine_name=?, description=?,weight=?, hierarchy=? where vid=?", $upd);
			//Zend_Debug::dump($this); die();
        } else {
            
            $this->_db->query("insert into zpraba_predictive_vocabulary (name, machine_name, description, hierarchy, weight) values(?, ?, ?,?,?)", array($data['t_name'], $data['t_name'], $data['t_desc'], $data['parent'], $data['weight']));
        }
        return array('result' => true,
                     'message' => 'update succed');
                     
       } catch(Exception $e) {
            Zend_Debug::dump($e); die();
        }
    }

    public function add_update($data) {
		 // Zend_Debug::dump(substr($data['parent'], 4)); die();
        //  echo substr($data['parent'], 4); die();
        $level = 0;
        if(is_numeric($data['parent'])) {
            $vdat = $this->_db->fetchRow("select * from zpraba_predictive_term_data where pid= ?", $data['parent']);
            $level = $vdat['level'] + 1;
        } else {
            $vdat = $this->_db->fetchRow("select * from zpraba_predictive_vocabulary where  vid=?", substr($data['parent'], 4));
			$data['parent']=0;
        }
		
		
        // Zend_Debug::dump($vdat);die();
        $data['vid'] = $vdat['vid'];
        // Zend_Debug::dump(is_numeric('val_1'));
        // Zend_Debug::dump("update zpraba_predictive_term_data set vid=".$data['vid'].",name=".$data['t_name'].", description=".$data['description'].",weight=".$data['weight'].", parent=".$data['parent'].",level=".$data['level'].",attr1=".$data['attr1'].",attr2=".$data['attr2'].",attr3=".$data['attr3'].",attr4=".$data['attr4'].", attr5=".$data['attr5'].",	icon_class=".$data['class']." where pid=".$data['id']."");
        // die();
        if($data['id'] != 0) {
            $upd = array($data['vid'],
                        $data['t_name'],
                        $data['t_desc'],
                        $data['weight'],
                        $data['parent'],
                        $level,
                        $data['attr1'],
                        $data['attr2'],
                        $data['attr3'],
                        $data['attr4'],
                        $data['attr5'],
                        $data['role'],
                        $data['class'],
                        $data['id']);
           $this->_db->query("update zpraba_predictive_term_data set vid=?,name=?, description=?,weight=?, parent=?,level=?,attr1=?,attr2=?,attr3=?,attr4=?, attr5=?,role=?,icon_class=? where pid=?", $upd);
        } else {
            $upd = array($data['vid'],
                        $data['t_name'],
                        $data['t_desc'],
                        $data['weight'],
                        $data['parent'],
                        $level,
                        $data['attr1'],
                        $data['attr2'],
                        $data['attr3'],
                        $data['attr4'],
                        $data['attr5'],
                        $data['role'],
                        $data['class']);
            $this->_db->query("insert into zpraba_predictive_term_data (vid,name, description,weight, parent,level,attr1,attr2,attr3,attr4, attr5, role,icon_class) values(?,?,?,?,?,?,?,?,?,?,?,?,?)", $upd);
        }
		// Zend_Debug::dump($upd);
		// Zend_Debug::dump($hasil);
		// Zend_Debug::dump("update zpraba_predictive_term_data set vid=".$data['vid'].",name=".$data['t_name'].", description=".$data['t_desc'].",weight=".$data['weight'].", parent=".$data['parent'].",level=".$data['level'].",attr1=".$data['attr1'].",attr2=".$data['attr2'].",attr3=".$data['attr3'].",attr4=".$data['attr4'].", attr5=".$data['attr5'].",	icon_class=".$data['class']." where pid=".$data['id']."");
        // die();
        return array('result' => true,
                     'message' => 'update succed');
    }

    public function main_parent($var) {
        $qry = "select * from temp_metadata where ptype=?";
        try {
            $data = $this->_db->fetchAll($qry, $var);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            $data = array();
        }

        foreach($data as $v3) {
            $new[$v3['pname']] = $v3['id'];
        }
        return $new;
    }

    public function get_term_data($id) {
        $qry = "select a.*, b.hierarchy from zpraba_predictive_term_data a inner join zpraba_taxonomy_vocabulary b on a.vid=b.vid where a.pid=?";
        try {
            $data = $this->_db->fetchRow($qry, $id);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            $data = array();
        }
        return $data;
    }

    public function get_vocabulary_by_id($id) {
        $qry = "select * from zpraba_predictive_vocabulary where vid=?";
        try {
            $data = $this->_db->fetchRow($qry, $id);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            $data = array();
        }
        return $data;
    }

    public function get_taxonomy_vocabulary() {
        $qry = "select * from zpraba_taxonomy_vocabulary order by 1";
        try {
            $data = $this->_db->fetchAll($qry);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            $data = array();
        }
        return $data;
    }

    public function get_parent_hierarchy() {
        $sql = "select * from zpraba_taxonomy_vocabulary order by name asc";
        try {
            $item = $this->_db->fetchAll($sql);

            foreach($item as $k => $v) {
                $data = array();
                $new[$k] = $v;
                if($v['hierarchy'] == 1) {
                    $data = $this->_db->fetchAll("select * from  zpraba_predictive_term_data where vid=?", $v['vid']);

                    foreach($data as $k1 => $v1) {
                        $new2[$v1['level']][$v1['parent']][$v1['pid']] = $v1;
                    }
                    Zend_Debug::dump($new2);
                    die();
                    $new[$k]['children'] = $data;
                }
            }
            Zend_Debug::dump($new);
            die("xxx");
        }
        catch(Exception $e) {
        }
        return $new;
    }

    public function get_tags_hierarchy() {
        $sql = "select * from zpraba_taxonomy_vocabulary where hierarchy=0 order by name asc";
        try {
            $item = $this->_db->fetchAll($sql);

            foreach($item as $k => $v) {
                $data = array();
                $data = $this->_db->fetchAll("select * from  zpraba_predictive_term_data where vid=?", $v['vid']);
                // Zend_Debug::dump($data); die(); 
                $new[$k] = $v;
                $new[$k]['children'] = $data;
            }
        }
        catch(Exception $e) {
        }
        return $new;
    }

    public function get_a_taxonomy_vocabulary($id) {
        $qry = "select * from zpraba_taxonomy_vocabulary where vid=?";
        try {
            $data = $this->_db->fetchRow($qry, $id);
        }
        catch(Exception $e) {
            //Zend_Debug::dump($e->getMessage());
            $data = array();
        }
        return $data;
    }

    public function inserting_temp($data) {
        try {
            $content = array($data['contents'],
                            $data['pname'],
                            $data['parent'],
                            $data['taxonomy'],
                            $data['tags'],
                            $data['keychar'],
                            $data['ptype'],
                            $data['attrs1'],
                            $data['attrs2']);
            $this->_db->query("insert into temp_metadata (contents, pname, parent, taxonomy, tags, keychar, ptype, attrs1, attrs2) values (?, ?, ?, ?,?,?,?, ?, ? )", $content);
            return true;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }
    }

    public function taxonomy_strc2() {
        try {
            $sql = "select * from zpraba_taxonomy_vocabulary where hierarchy=0 order by hierarchy, vid ";
            $item = $this->_db->fetchAll($sql);

            foreach($item as $k => $v) {
                $z[$v['vid']] = $v;
                // $tags['key'][$v['vid']] = $nz;
            }
            //  Zend_Debug::dump($z); 
            // die();
            $tags['val'] = $z;
            $sql = "select * from zpraba_taxonomy_vocabulary where hierarchy=1 order by hierarchy, vid ";
            $item = $this->_db->fetchAll($sql);

            foreach($item as $k => $v) {
                $max = $this->_db->fetchOne("select max(level) from zpraba_predictive_term_data where vid=?", $v['vid']);
                for($i = 0;
                $i <= $max;
                $i ++ ) {
                    $data = array();
                    $newx = array();
                    $data = $this->_db->fetchAll("select * from zpraba_predictive_term_data where vid=? and level=?", array($v['vid'], $i));

                    foreach($data as $k2 => $v2) {
                        if($v2['parent'] == null) {
                            $v2['parent'] = 0;
                        }
                        $newx[$v2['parent']][$v2['pid']] = $v2;
                    }
                    $new['key'][$v['vid']][$i] = $newx;
                }
                $new['val'][$v['vid']] = $v;
            }
        }
        catch(Exception $e) {
            die($sql);
        }
        //Zend_Debug::dump($new); die();
        return array("TAGGING" =>$tags,
                     "TAXONOMY" =>$new);
    }

    public function taxonomy_strc() {
        try {
            $sql = "select * from zpraba_predictive_vocabulary where hierarchy=0 order by hierarchy, vid ";
            $item = $this->_db->fetchAll($sql);

            foreach($item as $k => $v) {
                $mx = array();
                $nx = $this->_db->fetchAll("select * from zpraba_predictive_term_data where vid=?", $v['vid']);
                $nz = array();

                foreach($nx as $ka => $va) {
                    $nz[$va['pid']] = $va;
                }
                $z[$v['vid']] = $v;
                $tags['key'][$v['vid']] = $nz;
            }
             //Zend_Debug::dump($z);die();	   
            // die();
            $tags['val'] = $z;
            $sql = "select * from zpraba_predictive_vocabulary where hierarchy=1 order by hierarchy, vid ";
            $item = $this->_db->fetchAll($sql);
            //Zend_Debug::dump($item); die();
            foreach($item as $k => $v) {
                $max = $this->_db->fetchOne("select max(level) from zpraba_predictive_term_data where vid=?", $v['vid']);
                for($i = 0;$i <= $max;$i++ )
     			{
                    $data = array();
                    $newx = array();
                    $data = $this->_db->fetchAll("select * from zpraba_predictive_term_data where vid=? and level=?", array($v['vid'], $i));

                    foreach($data as $k2 => $v2) {
                        if($v2['parent'] == null) {
                            $v2['parent'] = 0;
                        }
                        $newx[$v2['parent']][$v2['pid']] = $v2;
                    }
                    $new['key'][$v['vid']][$i] = $newx;
                }
                $new['val'][$v['vid']] = $v;
            }
        }
        catch(Exception $e) {
            die($sql);
        }
        //Zend_Debug::dump($new); die();
        return array("TAGGING" =>$tags, 
                     "PREDIKSI" =>$new);
    }
	
	public function prediksi_strc() {
        try {
            $sql = "select * from zpraba_predictive_vocabulary where hierarchy=0 order by hierarchy, vid ";
            $item = $this->_db->fetchAll($sql);

            foreach($item as $k => $v) {
                $mx = array();
                $nx = $this->_db->fetchAll("select * from zpraba_predictive_term_data where vid=?", $v['vid']);
                $nz = array();

                foreach($nx as $ka => $va) {
                    $nz[$va['pid']] = $va;
                }
                $z[$v['vid']] = $v;
                $tags['key'][$v['vid']] = $nz;
            }
             //Zend_Debug::dump($z);die();	   
            // die();
            $tags['val'] = $z;
            $sql = "select * from zpraba_predictive_vocabulary where hierarchy=1 order by hierarchy, vid ";
            $item = $this->_db->fetchAll($sql);
            //Zend_Debug::dump($item); die();
            foreach($item as $k => $v) {
                $max = $this->_db->fetchOne("select max(level) from zpraba_predictive_term_data where vid=?", $v['vid']);
                for($i = 0;$i <= $max;$i++ )
     			{
                    $data = array();
                    $newx = array();
                    $data = $this->_db->fetchAll("select * from zpraba_predictive_term_data where vid=? and level=?", array($v['vid'], $i));

                    foreach($data as $k2 => $v2) {
                        if($v2['parent'] == null) {
                            $v2['parent'] = 0;
                        }
                        $newx[$v2['parent']][$v2['pid']] = $v2;
                    }
                    $new['key'][$v['vid']][$i] = $newx;
                }
                $new['val'][$v['vid']] = $v;
            }
        }
        catch(Exception $e) {
            die($sql);
        }
        //Zend_Debug::dump($new); die();
        return array("TAGGING" =>$tags, 
                     "PREDIKSI" =>$new);
    }

		
		public function stopword_strc() {
			
        try {
		$max = $this->_db->fetchOne("select max(level) from zpraba_stopword_term_data");
		for($i = 0;$i <= $max;$i ++ ) 
		{
           $data = array();
           $newx = array();
           $data = $this->_db->fetchAll("select * from zpraba_stopword_term_data where level=?", array($i));
           // Zend_Debug::dump($data); die();
           foreach($data as $k2 => $v2) 
		       {
                  if($v2['parent'] == null)
				     {
                            $v2['parent'] = 0;
                     }
                            $newx[$v2['parent']][$v2['stop_id']] = $v2;
               }
                            $new['key'][$i] = $newx;
         }	
							
            // foreach($item as $k => $v) {
                // $mx = array();
                // $nx = $this->_db->fetchAll("select * from zpraba_predictive_term_data where vid=?", $v['vid']);
                // $nz = array();

                // foreach($nx as $ka => $va) {
                    // $nz[$va['pid']] = $va;
                // }
                // $z[$v['vid']] = $v;
                // $tags['key'][$v['vid']] = $nz;
            // }
            // //  Zend_Debug::dump($z); 
            // // die();
            // $tags['val'] = $z;
            // $sql = "select * from zpraba_taxonomy_vocabulary where hierarchy=1 order by hierarchy, vid ";
            // $item = $this->_db->fetchAll($sql);

            // foreach($item as $k => $v) {
                // $max = $this->_db->fetchOne("select max(level) from zpraba_predictive_term_data where vid=?", $v['vid']);
                // for($i = 0;
                // $i <= $max;
                // $i ++ ) {
                    // $data = array();
                    // $newx = array();
                    // $data = $this->_db->fetchAll("select * from zpraba_predictive_term_data where vid=? and level=?", array($v['vid'], $i));

                    // foreach($data as $k2 => $v2) {
                        // if($v2['parent'] == null) {
                            // $v2['parent'] = 0;
                        // }
                        // $newx[$v2['parent']][$v2['pid']] = $v2;
                    // }
                    // $new['key'][$v['vid']][$i] = $newx;
                // }
                // $new['val'][$v['vid']] = $v;
            // }
        }
        catch(Exception $e) {
            die($sql);
        }
        //Zend_Debug::dump($new); die();
        return array( "STOPWORD" =>$new);
		//return $new;
    }
	
	public function stopword_strc2() { 
        try {
                for($i = 0;$i <= 1;$i++ )
     			{
                    $data = array();
                    $newx = array();
                    $data = $this->_db->fetchAll("select * from zpraba_stopword_term_data where level=?", $i);
                    foreach($data as $k2 => $v2) {
                        if($v2['parent'] == null) {
                            $v2['parent'] = 0;
                        }
                        $newx[$v2['parent']][$v2['stop_id']] = $v2;
                    }   
                    $new[$i] = $newx;
                }                 
        }
        catch(Exception $e) {
            die($sql);
        }
        //Zend_Debug::dump($new); die();
        //return array("TAGGING" =>$tags, 
         //            "TAXONOMY" =>$new);
		 return $new;
					 
    }
		
		
    public function updating() {
        $sql = "select * from zpraba_conn order by 1";
        try {
            $item = $this->_db->fetchAll($sql);

            foreach($item as $k => $v) {
                $data[$k] = $v;
                $data[$k]['conn'] = unserialize($v['conn_params']);
            }
        }
        catch(Exception $e) {
            die($sql);
        }
        $new = array();

        foreach($data as $v) {
            if($v['conn']['host'] != "") {
                $new[$v['conn']['host']][$v['conn']['adapter']] = $v['conn']['adapter'];
            }
        }
        $var = $this->main_parent('machine');
        //Zend_Debug::dump($new); die();

        foreach($new as $k => $v) {

            foreach($v as $v2) {
                $content = array("contents" => "",
                                 "pname" =>$v2,
                                 "parent" =>$var[$k],
                                 "taxonomy" => "",
                                 "tags" => "",
                                 "keychar" => "",
                                 "ptype" => "adapter",
                                 "attrs1" => "",
                                 "attrs2" => "");
                $this->inserting_temp($content);
            }
        }
        die();
    }
}
