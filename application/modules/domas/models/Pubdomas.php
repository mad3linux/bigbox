<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3000);


class Domas_Model_Pubdomas extends Zend_Db_Table_Abstract {

    public function pub_insert_mysql_content($data) {
        try {

            foreach($data['content'] as $k => $v) {
                $this->_db->query("insert into pmas_content_crawl (link, content, updated_date, bundle, bundle_name, title, author) values (?,?, now(),?,?,?,?)", array($k, $v['content'], $v['bundle'], $v['bundle_name'], $v['title'], $v['author']));
            }
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }
        return true;
    }

    public function pub_call_api_nlp($data) {
       /* 
        $vr = file_get_contents("http://10.2.27.49/in.php?x=1");
        $v= json_decode($vr);
        $data['nlp_sentiment']=$v->sentiment;
        
         */ 
         return $data;
    }
}



