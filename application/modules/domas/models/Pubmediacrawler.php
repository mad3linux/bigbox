<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3000);

class Domas_Model_Pubmediacrawler extends Zend_Db_Table_Abstract {

	function connect_solr() {
		$config = new Zend_Config_Ini(APPLICATION_PATH . 
									  '/configs/application.ini', 'production');
		$config = $config->toArray();
		$options = array('hostname' =>$config['data']['pmas']['solr']['host'],
						 'login' =>$config['data']['pmas']['solr']['host'],
						 'user' =>$config['data']['pmas']['solr']['user'],
						 'password' =>$config['data']['pmas']['solr']['password'],
						 'port' =>$config['data']['pmas']['solr']['port'],
						 'path' =>$config['data']['pmas']['solr']['path']);
		try {
			return new SolrClient($options);
		}
		catch(Exception $e) {
			Zend_Debug::dump($e);
			die();
		}
	}

	function update_index_mysql($id) {
		try {
			$this->_db->query("update pmas_crawl_index set log_crawl=1 where md5_url=?", $id);
		}
		catch(Exception $e) {
		}
	}

	public function pub_insert_digital_media() {
		try {
			$client = $this->connect_solr();
			$query = new SolrQuery();
			$query->setQuery("*:*");
			$query->addFilterQuery("entity_id:2");
			$query->addFilterQuery("is_mediatype:1");
			$query->addFilterQuery("content:[* TO *]");
			$query->addFilterQuery("content_date:[2016-10-20T00:00:00Z TO 2016-10-26T23:59:59Z]");
			//$query->addFilterQuery('bundle_name:sentiment');
			$query->addSortField('content_date', SolrQuery::ORDER_DESC);
			$query->setStart(0);
			$query->setRows(9041);
			$query_response = $client->query($query);
			$response = $query_response->getResponse();
			// $data = $this->_db->fetchAll("select * from bin_poswil_input_web_sample");
			// Zend_Debug::dump($data);die();
			//Zend_Debug::dump($response ["response"]["docs"]);die();

			foreach($response["response"]["docs"] as $v) {
				$input['id'] = $v['id'];
				$input['title'] = trim($v['label']);
				$input['publisher'] = $v['bundle_name'];
				$input['author'] = trim($v['ss_author']);
				$input['source'] = 'news';
				$input['text'] = $v['content'];
				$input['lang'] = 'id';
				$input['date_created'] = $v['content_date'];
				//$this->pub_insert_eceos($input);
				//Zend_Debug::dump($input);
			}
			//die();
		}
		catch(Exception $e) {
		}
	}

	public function pub_push() {
	}

	public function process_under_score($content, $tcontent) {
		$tcontent[0] = substr($tcontent[0], 1);
		$content = str_replace($tcontent[0], str_replace("_", '', $tcontent[0]), $content);
		return $content;
	}

	public function pub_log_test() {
		$this->_db->query("insert into logtest (waktu) values (now())");
		return true;
	}
	
	/**
	 * nama input =rutin, nilai 1=10 menit; 2= 1 jam; 3 =3 jam; 4=6 jam; 5=> 12 jam; 6=>daily; 7=>weekly; 8=>monthly
	 *
	 *
	 */

	public function pub_get_active_media($input) {
		if($input['rutin'] == "") {
			$input['rutin'] = 1;
		}
		try {
			$sql = "select a.*, b.slugname as main_media, b.language, b.media_type from pmas_media_sub_crawl a inner join pmas_media b on a.media_id=b.id where status=1  and b.update_routine=?";
			$data = $this->_db->fetchAll($sql, $input['rutin']);
			//Zend_Debug::dump($data); die();
		}
		catch(Exception $e) {
			//Zend_Debug::dump($e);
		}
		return $data;
	}

	public function pub_get_index_past($data) {
		$c2 = new CMS_LIB_parse();
		$z = new Domas_Model_Pmedia();
		$data['fetch'] = $z->get_a_media_by_slug($data['medianame']);
		//$content = file_get_contents($data['indexurl']);
		$content = file_get_contents($data['indexurl'] . 
									 "&" . 
									 $data['fetch']['params']['parameterpage'] . 
									 "=" . 
									 $data['startpage']);
		$content = $c2->remove($content, "<!--", "-->");
		$content = $c2->remove($content, "<style", "</style>");
		$content = $c2->remove($content, "<script", "</script>");
		$content = preg_replace('/\s+/', ' ', $content);
		//Zend_Debug::dump($content); die();
		$dom_1 = new Zend_Dom_Query($content);
		try {
			if($data['fetch']['params']['tagpage'] != "") {
				$page = $dom_1->query(trim($data['fetch']['params']['tagpage']));
				$data['countpage'] = count($page);
			}
		}
		catch(Exception $e) {
			Zend_Debug::dump($e->getMessage());
			die();
		}
		try {
			$rc = $dom_1->query($data['fetch']['params']['tagindex']);
			//Zend_Debug::dump($rc); die();
			if(count($rc)> 0) {
				$k = 0;

				foreach($rc as $v) {
					$urin = $v->getAttribute('href');
					$cek = strpos($urin, 'http:');
					if($cek === false) {
						$urin = "http:" . $urin;
					} else {
						$urin = $urin;
					}
					$data['indexes'][] = $urin;
					$k ++;
				}
			}
		}
		catch(Exception $e) {
			Zend_Debug::dump($e->getMessage());
			die();
		}
		//return $data;
		return $this->get_others_page($data);
	}

	public function pub_get_index($data) {
		//  Zend_Debug::dump($this->processing_date("Rabu 14 Sep 2016, 14:28 WIB", 1)); die();
		//Zend_Debug::dump($data); die("xx");
		$c2 = new CMS_LIB_parse();
		$z = new Domas_Model_Pmedia();
		$data['fetch'] = $z->get_a_media_by_slug($data['medianame']);
		$params = unserialize(base64_decode($data['attrs']));
		$data['fetch'] = $data;
		$data['fetch']['params'] = $params;
		$data['indexurl'] = $data['fetch']['url_crawl'];
		//Zend_Debug::dump($data); die();
		switch($data['fetch']['type_crawl']) {
			default : case 1 : return $this->processing_index_html($data);
			break;
			case 2 : return $this->processing_index_rss($data);
			break;
		}
	}

	public function processing_index_rss($data) {
		try {
			$feed = new Zend_Feed_Rss($data['indexurl']);
			///Zend_Debug::dump($feed ); die();
			$i = 0;

			foreach($feed as $v) {
				$data['indexes'][$i] = $v->link();
				if($v->description()!= "") {
					$data['rss']['description'][$i] = $v->description();
				}
				if($v->link()!= "") {
					$data['rss']['link'][$i] = $v->link();
				}
				if($v->pubDate()!= "") {
					$data['rss']['pubDate'][$i] = $v->pubDate();
				}
				if($v->comments()!= "") {
					$data['rss']['comments'][$i] = $v->comments();
				}
				if($v->author()!= "") {
					$data['rss']['author'][$i] = $v->pubDate();
				}
				if($v->category()!= "") {
					$data['rss']['category'][$i] = $v->category();
				}
				if($v->source()!= "") {
					$data['rss']['source'][$i] = $v->source();
				}
				if($v->enclosure()!= "") {
					$data['rss']['enclosure'][$i] = $v->enclosure();
				}
				if($v->guid()!= "") {
					$data['rss']['guid'][$i] = $v->guid();
				}
				$i ++;
			}
			//Zend_Debug::dump($data);
			//die();
		}
		catch(Exception $e) {
			Zend_Debug::dump($e->getMessage());
			// die();
		}
		return $data;
	}

	public function processing_index_html($data) {
		//   Zend_Debug::dump($data); //die();
		$c2 = new CMS_LIB_parse();
		$content = file_get_contents($data['indexurl']);
		//$content = file_get_contents($data['indexurl']."&".$data['fetch']['params']['parameterpage']."=".$data['startpage']);
		$content = $c2->remove($content, "<!--", "-->");
		$content = $c2->remove($content, "<style", "</style>");
		$content = $c2->remove($content, "<script", "</script>");
		$content = preg_replace('/\s+/', ' ', $content);
		//Zend_Debug::dump($content); die();
		$dom_1 = new Zend_Dom_Query($content);
		$dns = parse_url($data['url_crawl']);
		if(empty($data['fetch']['params']['tag-index-type'])) {
			$data['fetch']['params']['tag-index-type'] = 'css';
		}
		try {
			if($data['fetch']['params']['tag-index-type'] == 'css') {
				$rc = $dom_1->query($data['fetch']['params']['tag-index']);
			} else {
				$rc = $dom_1->queryXpath($data['fetch']['params']['tag-index']);
			}
			if(count($rc)> 0) {
				$urin = "";
				$k = 0;

				foreach($rc as $v) {
					if($k < 20) {
						$urin = $v->getAttribute('href');
						$cek = strpos($urin, 'http');
						//Zend_Debug::dump($urin); die();
						if($cek === false) {
							$cek2 = parse_url($urin);
							if($cek2['host'] != "") {
								$urin = "http:" . $urin;
							} else {
								$urin = "http://" . $dns['host'] . "" . $urin;
							}
						} else {
							$urin = $urin;
						}
						$data['indexes'][] = $urin;
					}
					$k ++;
				}
			}
		}
		catch(Exception $e) {
			Zend_Debug::dump($e->getMessage());
			// die();
		}
		//  Zend_Debug::dump($data); die();
		return $data;
		//return $this->get_others_page ($data);
	}

	function get_others_page($data) {
		$c2 = new CMS_LIB_parse();
		try {
			for($page = $data['startpage'] + 1;
			$page < ($data['startpage'] + $data['countpage'] )- 1;
			$page ++ ) {
				$content = file_get_contents($data['indexurl'] . 
											 "&" . 
											 $data['fetch']['params']['parameterpage'] . 
											 "=" . 
											 $page);
				$content = $c2->remove($content, "<!--", "-->");
				$content = $c2->remove($content, "<style", "</style>");
				$content = $c2->remove($content, "<script", "</script>");
				$content = preg_replace('/\s+/', ' ', $content);
				try {
					$dom_1 = new Zend_Dom_Query($content);
					$rc = $dom_1->query($data['fetch']['params']['tagindex']);
				}
				catch(Exception $e) {
					Zend_Debug::dump($e->getMessage());
					die();
				}
				if($rc) {
					if(count($rc)> 0) {

						foreach($rc as $v) {
							if($cont = $v->getAttribute('href')) {
								$data['indexes'][] = $cont;
							}
						}
					}
				}
				//	Zend_Debug::dump($data); die();
			}
		}
		catch(Exception $e) {
			//Zend_Debug::dump($e->getMessage()); die();
		}
		//Zend_Debug::dump($data); die();
		return $data;
	}

	public function pub_get_item($data) {
		$k = 0;

		foreach($data['indexes'] as $z) {
			$data['content'][$z] = $this->processing_item($z, $data, $k);
			$k ++;
		}
		// Zend_Debug::dump($data);die();
		return $data;
	}

	public function processing_date($input, $case) {
		//echo ($case);die();
		//echo ($case);die();
		$month = array('Januari' => 'January',
					   'Februari' => 'February',
					   'Maret' => 'March',
					   'April' => 'April',
					   'Mei' => 'May',
					   'Juni' => 'June',
					   'Juli' => 'July',
					   'Agustus' => 'August',
					   'September' => 'September',
					   'Oktober' => 'October',
					   'November' => 'November',
					   'Desember' => 'December');
		$month2 = array('Jan' => 'January',
						'Feb' => 'February',
						'Mar' => 'March',
						'Apr' => 'April',
						'Mei' => 'May',
						'Jun' => 'June',
						'Jul' => 'July',
						'Agu' => 'August',
						'Sep' => 'September',
						'Okt' => 'October',
						'Nop' => 'November',
						'Des' => 'December');
		///	Zend_Debug::dump($input); die();
		//Zend_Debug::dump($case); die();
		switch($case) {
			case 1 : $var = explode(" ", $input);
			return gmdate("Y-m-d\TH:i:s\Z", strtotime($var[1] . " " . $var[2] . " " . (int) $var[3] . " " . $var[4]));
			break;
			case 2 : // die("x");
			$var = explode(" ", trim($input));
			//Zend_Debug::dump($var); die("xxx");
			return gmdate("Y-m-d\TH:i:s\Z", strtotime($var[1] . " " . $month[$var[2]] . " " . $var[3] . " " . $var[5]));
			break;
			case '3' : $var = explode(" ", trim($input));
			// Zend_Debug::dump($var); die();
			return gmdate("Y-m-d\TH:i:s\Z", strtotime($var[1] . " " . $month[$var[2]] . " " . $var[3] . " " . $var[5]));
			break;
			case '4' : $var = explode(" ", trim($input));
			return gmdate("Y-m-d\TH:i:s\Z", strtotime($var[1] . " " . $month[$var[2]] . " " . (int) $var[3] . " " . $var[4]));
			break;
			case '5' : $var = explode(" ", trim($input));
			// Zend_Debug::dump($var); die();
			return gmdate("Y-m-d\TH:i:s\Z", strtotime($var[1] . " " . $month[$var[2]] . " " . (int) $var[3] . " " . $var[7]));
			break;
			case '6' : $var = explode(" ", trim($input));
			// Zend_Debug::dump($var); die();
			return gmdate("Y-m-d\TH:i:s\Z", strtotime($var[0] . " " . $var[1] . " " . (int) $var[2] . " " . $var[3]));
			break;
			case '7' : $var = explode(" ", trim($input));
			// Zend_Debug::dump($var); die();
			return gmdate("Y-m-d\TH:i:s\Z", strtotime($var[0] . " " . $month2[$var[1]] . " " . (int) $var[2] . " " . $var[3]));
			break;
			case '8' : $var = explode(" ", trim($input));
			return gmdate("Y-m-d\TH:i:s\Z", strtotime($var[0] . " " . $month[$var[1]] . " " . (int) $var[2] . " " . $var[3]));
			break;
			case '9' : // echo $input; die();
			//$var = explode(" ", $input);
			return gmdate("Y-m-d\TH:i:s\Z", trim($input));
			break;
			case '10' : $var = explode("•", trim($input));
			$var2 = explode(" ", trim($var[1]));
			//   Zend_Debug::dump($var2); die();
			return gmdate("Y-m-d\TH:i:s\Z", strtotime($var2[2] . " " . $var2[3] . " " . $var2[4] . " " . $var2[5]));
			break;
			case '11' : $var = explode(" ", trim($input));
			$var1 = explode("/", trim($var[1]));
			// Zend_Debug::dump($input); die();
			return gmdate("Y-m-d\TH:i:s\Z", strtotime($var1[2] . "-" . $var1[1] . "-" . $var1[0] . " " . $var[2]));
			break;
			case '12' : return gmdate("Y-m-d\TH:i:s\Z", strtotime($input));
			break;
			case '13' : $cc = new CMS_General();
			$date = $cc->find_date($input);
			//  Zend_Debug::dump($date); die();
			return gmdate("Y-m-d\TH:i:s\Z", strtotime($date));
			break;
		}
	}

	function processing_item($link, $data, $k) {
		#  Zend_Debug::dump(count($data['fetch']['params']['fname'])); die();
		$result = array();
		$c2 = new CMS_LIB_parse();
		$content = file_get_contents($link);
		$content = $c2->remove($content, "<!--", "-->");
		$content = $c2->remove($content, "<style", "</style>");
		$content = $c2->remove($content, "<script", "</script>");
		$content = preg_replace('/\s+/', ' ', $content);
		//Zend_Debug::dump($content);die();
		if(empty($data['fetch']['params']['tag-content-type'])) {
			$data['fetch']['params']['tag-content-type'] = 'css';
		}
		if(empty($data['fetch']['params']['tag-author-type'])) {
			$data['fetch']['params']['tag-author-type'] = 'css';
		}
		if(empty($data['fetch']['params']['tag-category-type'])) {
			$data['fetch']['params']['tag-category-type'] = 'css';
		}
		if(empty($data['fetch']['params']['tag-date-type'])) {
			$data['fetch']['params']['tag-date-type'] = 'css';
		}
		if(empty($data['fetch']['params']['tag-date2-type'])) {
			$data['fetch']['params']['tag-date2-type'] = 'css';
		}
		if(empty($data['fetch']['params']['tag-date3-type'])) {
			$data['fetch']['params']['tag-date3-type'] = 'css';
		}
		if(empty($data['fetch']['params']['tag-topic-type'])) {
			$data['fetch']['params']['tag-topic-type'] = 'css';
		}
		if(empty($data['fetch']['params']['tag-title-type'])) {
			$data['fetch']['params']['tag-title-type'] = 'css';
		}
		if(empty($data['fetch']['params']['tag-icon-type'])) {
			$data['fetch']['params']['tag-icon-type'] = 'css';
		}
		if(isset($data['fetch']['params']['tag-content'])&& $data['fetch']['params']['tag-content'] != "") {
			$tcontent = explode('~', $data['fetch']['params']['tag-content']);
			if(count($tcontent)> 0) {
				$tcontent[1] = 'current';
			}
		}
		if(isset($data['fetch']['params']['tag-author'])&& $data['fetch']['params']['tag-author'] != "") {
			$tauth = explode('~', $data['fetch']['params']['tag-author']);
			if(count($tauth)> 0) {
				$tauth[1] = 'current';
			}
		}
		if(isset($data['fetch']['params']['tag-category'])&& $data['fetch']['params']['tag-category'] != "") {
			$tcategory = explode('~', $data['fetch']['params']['tag-category']);
			if(count($tcategory)> 0) {
				$tcategory[1] = 'current';
			}
		}
		if(isset($data['fetch']['params']['tag-date'])&& $data['fetch']['params']['tag-date'] != "") {
			$tdate = explode('~', $data['fetch']['params']['tag-date']);
			if(count($tdate)> 0) {
				$tdate[1] = 'current';
			}
		}
		if(isset($data['fetch']['params']['tag-date2'])&& $data['fetch']['params']['tag-date2'] != "") {
			$tdate2 = explode('~', $data['fetch']['params']['tag-date2']);
			if(count($tdate2)> 0) {
				$tdate2[1] = 'current';
			}
		}
		if(isset($data['fetch']['params']['tag-date3'])&& $data['fetch']['params']['tag-date3'] != "") {
			$tdate3 = explode('~', $data['fetch']['params']['tag-date3']);
			if(count($tdate3)> 0) {
				$tdate3[1] = 'current';
			}
		}
		if(isset($data['fetch']['params']['tag-topic'])&& $data['fetch']['params']['tag-topic'] != "") {
			$ttopic = explode('~', $data['fetch']['params']['tag-topic']);
			if(count($ttopic)> 0) {
				$ttopic[1] = 'current';
			}
		}
		if(isset($data['fetch']['params']['tag-title'])&& $data['fetch']['params']['tag-title'] != "") {
			$tjudul = explode('~', $data['fetch']['params']['tag-title']);
			if(count($tjudul)> 0) {
				$tjudul[1] = 'current';
			}
		}
		if(isset($data['fetch']['params']['tag-icon'])&& $data['fetch']['params']['tag-icon'] != "") {
			$ticon = explode('~', $data['fetch']['params']['tag-icon']);
			if(count($ticon)> 0) {
				$ticon[1] = 'current';
			}
		}
		//Zend_Debug::dump($data); die();
		if($content != "") {
			if($ticon[0][0] == "_") {
			}
			$dom_1 = new Zend_Dom_Query($content);
			try {
				if(count($ticon)> 0) {
					if($ticon[0][1] == "_") {
						$content = $this->process_under_score($content, $ticon);
						$ticon[0] = str_replace("_", "", $ticon[0]);
						$dom_1 = new Zend_Dom_Query($content);
						//Zend_Debug::dump($dom_1); 
					}
					$res_icon = $dom_1->query($ticon[0]);
					// Zend_Debug::dump($res_icon->current());die();
					if($res_icon && count($res_icon)> 0) {
						$result['icon'] = $res_icon-> {
							$ticon[1] }
						()->getAttribute('src');
					}
				}
			}
			catch(Exception $e) {
				$result['icon'] = "";
			}
			try {
				if(count($tcontent)> 0) {
					if($tcontent[0][1] == "_") {
						$content = $this->process_under_score($content, $tcontent);
						$tcontent[0] = str_replace("_", "", $tcontent[0]);
						$dom_1 = new Zend_Dom_Query($content);
						//Zend_Debug::dump($dom_1); 
					}
					if($data['fetch']['params']['tag-content-type'] == 'css') {
						$res_content = $dom_1->query($tcontent[0]);
					} elseif($data['fetch']['params']['tag-content-type'] == 'xpath') {
						$res_content = $dom_1->queryXpath($tcontent[0]);
					}
					// Zend_Debug::dump($res_content); die();
					if($res_content && count($res_content)> 0) {
						$result['content'] = $res_content-> {
							$tcontent[1] }
						()->textContent;
						
						/*
						 $result['content'] = $res_content->getDocument()->saveHTML($res_content->{$tcontent[1]}());
						 $pattern = '/<a[^>]*?href=(?:\'\'|"")[^>]*?>(.*?)<\/a>/i';
						 $replacement = '$1';
						 $result['rawcontent'] = $result['content'];
						 //  $result['content']= preg_replace($pattern, $replacement, $result['content']);
						 $result['content'] = preg_replace('/<a href="(.*?)">(.*?)<\/a>/', "\\2", $result['content']);
						 //  $result['content'] = Html2Text\Html2Text::convert($result['content']);
						 $result['content'] = str_replace(array("\r\n", "\n"), "\\n", $result['content']);
						 */
					}
				}
			}
			catch(Exception $e) {
				$result['content'] = "";
				if($data['fetch']['params']['tag-content-type'] == 'string') {
					$result['content'] = $data['fetch']['params']['tag-content'];
				}
			}
			try {
				if(count($tauth)> 0) {
					if($tauth[0][1] == "_") {
						$content = $this->process_under_score($content, $tauth);
						$tauth[0] = str_replace("_", "", $tauth[0]);
						$dom_1 = new Zend_Dom_Query($content);
						//Zend_Debug::dump($dom_1); 
					}
					if($data['fetch']['params']['tag-author-type'] == 'css') {
						$res_auth = $dom_1->query($tauth[0]);
					} elseif($data['fetch']['params']['tag-author-type'] == 'xpath') {
						$res_auth = $dom_1->queryXpath($tauth[0]);
					}
					if($res_auth && count($res_auth)> 0) {
						$result['author'] = $res_auth-> {
							$tauth[1] }
						()->textContent;
					}
				}
			}
			catch(Exception $e) {
				$result['author'] = "";
				if($data['fetch']['params']['tag-author-type'] == 'string') {
					$result['author'] = $data['fetch']['params']['tag-author'];
				}
			}
			try {
				if(count($tcategory)> 0) {
					if($tcategory[0][1] == "_") {
						$content = $this->process_under_score($content, $tcategory);
						$tcategory[0] = str_replace("_", "", $tcategory[0]);
						$dom_1 = new Zend_Dom_Query($content);
						//Zend_Debug::dump($dom_1); 
					}
					//Zend_Debug::dump($tcategory); die();
					$res_category = $dom_1->query($tcategory[0]);
					if($res_category && count($res_category)> 0) {
						$result['category'] = $res_category-> {
							$tcategory[1] }
						()->textContent;
					}
				}
			}
			catch(Exception $e) {
				$result['category'] = "";
			}
			try {
				$cdate = 0;
				if(isset($data['rss']['pubDate'][$k])&& $data['rss']['pubDate'][$k] != "") {
					$result['date'] = $this->processing_date($data['rss']['pubDate'][$k], 12);
					$cdate = 1;
				}
				if(count($tdate)> 0 && $cdate == 0) {
					if($tdate[0][1] == "_") {
						$content = $this->process_under_score($content, $tdate);
						$tdate[0] = str_replace("_", "", $tdate[0]);
						$dom_1 = new Zend_Dom_Query($content);
						//Zend_Debug::dump($dom_1); 
					}
					if($data['fetch']['params']['tag-date-type'] == 'css') {
						$res_date = $dom_1->query($tdate[0]);
					} elseif($data['fetch']['params']['tag-date-type'] == 'xpath') {
						$res_date = $dom_1->queryXpath($tdate[0]);
					}
					if($res_date && count($res_date)> 0) {
						$result['date'] = $this->processing_date($res_date-> {
							$tdate[1] }
						()->textContent, $data["fetch"]['params']['processing-date']);
						if($tdate[2] != "") {
							//    echo $res_date->$tdate[1]()->getAttribute($tdate[2]); die("xx");
							$result['date'] = $this->processing_date($res_date-> {
								$tdate[1] }
							()->getAttribute($tdate[2]), $data["fetch"]['params']['processing-date']);
						}
					}
				}
				// Zend_Debug::dump(count($tdate2)); //die();
				// Zend_Debug::dump($cdate); die();
				// Zend_Debug::dump($result); die();
				if(count($tdate2)> 0 && $cdate == 0 &&($result['date'] == '1970-01-01T00:00:00Z' || $result['date'] == '')) {
					// die("s");
					if($tdate2[0][1] == "_") {
						$content = $this->process_under_score($content, $tdate2);
						$tdate2[0] = str_replace("_", "", $tdate2[0]);
						$dom_1 = new Zend_Dom_Query($content);
						//Zend_Debug::dump($dom_1); 
					}
					if($data['fetch']['params']['tag-date2-type'] == 'css') {
						$res_date2 = $dom_1->query($tdate2[0]);
					} elseif($data['fetch']['params']['tag-date2-type'] == 'xpath') {
						$res_date2 = $dom_1->queryXpath($tdate2[0]);
					}
					if($res_date2 && count($res_date2)> 0) {
						$result['date'] = $this->processing_date($res_date2-> {
							$tdate2[1] }
						()->textContent, $data["fetch"]['params']['processing-date2']);
						if($tdate2[2] != "") {
							//    echo $res_date->$tdate[1]()->getAttribute($tdate[2]); die("xx");
							$result['date'] = $this->processing_date($res_date2-> {
								$tdate2[1] }
							()->getAttribute($tdate2[2]), $data["fetch"]['params']['processing-date2']);
						}
					}
				}
				if(count($tdate3)> 0 && $cdate == 0 &&($result['date'] == '1970-01-01T00:00:00Z' || $result['date'] == '')) {
					// die("s");
					if($tdate3[0][1] == "_") {
						$content = $this->process_under_score($content, $tdate3);
						$tdate3[0] = str_replace("_", "", $tdate3[0]);
						$dom_1 = new Zend_Dom_Query($content);
						//Zend_Debug::dump($dom_1); 
					}
					if($data['fetch']['params']['tag-date3-type'] == 'css') {
						$res_date3 = $dom_1->query($tdate3[0]);
					} elseif($data['fetch']['params']['tag-date3-type'] == 'xpath') {
						$res_date3 = $dom_1->queryXpath($tdate3[0]);
					}
					if($res_date3 && count($res_date3)> 0) {
						$result['date'] = $this->processing_date($res_date3-> {
							$tdate3[1] }
						()->textContent, $data["fetch"]['params']['processing-date3']);
						if($tdate3[2] != "") {
							//    echo $res_date->$tdate[1]()->getAttribute($tdate[2]); die("xx");
							$result['date'] = $this->processing_date($res_date3-> {
								$tdate3[1] }
							()->getAttribute($tdate3[2]), $data["fetch"]['params']['processing-date3']);
						}
					}
				}
			}
			catch(Exception $e) {
				$result['date'] = "";
				if($data['fetch']['params']['tag-date-type'] == 'string') {
					$result['date'] = $data['fetch']['params']['tag-date'];
				}
			}
			try {
				if(count($ttopic)> 0) {
					if($ttopic[0][1] == "_") {
						$content = $this->process_under_score($content, $ttopic);
						$ttopic[0] = str_replace("_", "", $ttopic[0]);
						$dom_1 = new Zend_Dom_Query($content);
						//Zend_Debug::dump($dom_1); 
					}
					if($data['fetch']['params']['tag-topic-type'] == 'css') {
						$res_topic = $dom_1->query($ttopic[0]);
					} elseif($data['fetch']['params']['tag-topic-type'] == 'xpath') {
						$res_topic = $dom_1->queryXpath($ttopic[0]);
					}
					if($res_topic && count($res_topic)> 0) {
						$result['topic'] = $res_topic-> {
							$ttopic[1] }
						()->textContent;
					}
				}
			}
			catch(Exception $e) {
				$result['topic'] = "";
				if($data['fetch']['params']['tag-topic-type'] == 'string') {
					$result['topic'] = $data['fetch']['params']['tag-topic'];
				}
			}
			try {
				if(count($tjudul)> 0) {
					if($tjudul[0][1] == "_") {
						$content = $this->process_under_score($content, $tjudul);
						$tjudul[0] = str_replace("_", "", $tjudul[0]);
						$dom_1 = new Zend_Dom_Query($content);
						//Zend_Debug::dump($dom_1); 
					}
					if($data['fetch']['params']['tag-title-type'] == 'css') {
						$res_title = $dom_1->query($tjudul[0]);
					} elseif($data['fetch']['params']['tag-title-type'] == 'xpath') {
						$res_title = $dom_1->queryXpath($tjudul[0]);
					}
					//Zend_Debug::dump($res_title);die();
					if(count($res_title)) {
						$result['title'] = $res_title-> {
							$tjudul[1] }
						()->textContent;
					}
				}
			}
			catch(Exception $e) {
				$result['title'] = "";
				if($data['fetch']['params']['tag-title-type'] == 'string') {
					$result['title'] = $data['fetch']['params']['tag-title'];
				}
			}
			//Zend_Debug::dump($data['items']['date'][$link]);die();
			//$result['domain'] =$this->get_domain($link);
			$result['bundle'] = $data['main_media'];
			$result['bundle_name'] = $this->get_domain($link);
			if(!isset($result['title'])&& $data['items']['title'][$link] != "") {
				$result['title'] = $data['items']['title'][$link];
			}
			if(!isset($result['date'])&& $data['items']['date'][$link] != "") {
				$result['date'] = date('Y-m-d\TH:i:s\Z', strtotime($data['items']['date'][$link]));
				//$result['date2'] =$data['items']['date'][$link];
			}
			if(!isset($result['icon'])&& $data['items']['icon'][$link] != "") {
				$result['icon'] = $data['items']['icon'][$link];
			}
			if(count($data['fetch']['params']['fname'])>= 1 && $data['fetch']['params']['fname'][0] != "") {

				foreach($data['fetch']['params']['fname'] as $xx => $vv) {
					//Zend_Debug::dump($data['fetch']['params']['fname']);
					//Zend_Debug::dump($vv);
					// die();
					$t_ = array();
					$res_ = array();
					if(isset($data['fetch']['params']['fvalue'][$xx])&& $data['fetch']['params']['fvalue'][$xx] != "") {
						$t_ = explode('~', $data['fetch']['params']['fvalue'][$xx]);
						//Zend_Debug::dump(  $t_ ); die();
						if(count($t_)> 0) {
							$t_[1] = 'current';
						}
					}
					try {
						if(count($t_)> 0) {
							if($t_[0][1] == "_") {
								$content = $this->process_under_score($content, $t_);
								$t_[0] = str_replace("_", "", $t_[0]);
								$dom_1 = new Zend_Dom_Query($content);
								// Zend_Debug::dump($dom_1); 
							}
							if($data['fetch']['params']['ftype'][$xx] == 'css') {
								$res_ = $dom_1->query($t_[0]);
							} elseif($data['fetch']['params']['ftype'][$xx] == 'xpath') {
								$res_ = $dom_1->queryXpath($t_[0]);
							}
							if($res_ && count($res_)> 0) {
								$result['dynamic'][$vv] = $res_->current()->textContent;
								//die();
							}
						}
					}
					catch(Exception $e) {
						//  Zend_Debug::dump($e->getMessage()); die();
						$result[$vv] = "";
						if($data['fetch']['params']['ftype'][$xx] == 'string') {
							$result[$vv] = $data['fetch']['params']['fvalue'][$xx];
						}
					}
				}
			}
		}
		//Zend_Debug::dump($result);
		//die();
		return $result;
	}

	function get_domain($url) {
		//parse_url($url);
		return parse_url($url, PHP_URL_HOST);
		$pieces = parse_url($url);
		$domain = isset($pieces['host'])? $pieces['host'] : '';
		if(preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
			return $regs['domain'];
		}
		return false;
	}

	public function pub_insert_data_item_with_noreplace($data) {
		$sent = new \PHPInsight\Sentiment();
		$senArr = array('pos' => 2,
						'neu' => 0,
						'neg' => 1);
		$senStatus = array(2 => "POSITIF",
						   0 => "NETRAL",
						   1 => "NEGATIF");
		$cc = new Domas_Model_Withsolrpmas();
		$ccc = new Domas_Model_Params();
		$p = $ccc->get_params_pmas(true);
		//Zend_Debug::dump($data); die();

		foreach($data['content'] as $k => $v) {
			$idata['id'] = md5($k);
			$idata['bundle'] = $v['bundle'];
			$idata['content'] = $v['content'];
			$idata['label'] = $v['title'];
			$idata['bundle_name'] = $v['bundle_name'];
			$idata['entity_type'] = 'data-html';
			$idata['entity_id'] = '2';
			$idata['content_date'] = $v['date'];
			$idata['is']['is_flag'] = 0;
			if($p['IsActiveSentimentPmas'] == 1) {
				$idata['is']['is_sentiment'] = $senArr[$sent->categorise($v['title'])];
			} else {
				$idata['is']['is_sentiment'] = 0;
			}
			$idata['is']['is_approve'] = 0;
			$idata['is']['is_mktime'] = time();
			$idata['ss']['ss_author'] = $v['author'];
			$idata['ss']['ss_category'] = $v['category'];
			$idata['ss']['ss_topic'] = $v['topic'];
			$idata['ss']['ss_icon'] = $v['icon'];
			//Zend_Debug::dump($idata); die();
			try {
				$u = $cc->insertdata($idata);
			}
			catch(Exception $e) {
				Zend_Debug::dump($e->getMessage());
				die();
			}
		}
		//die("s");
		return true;
	}

	public function pub_insert_data_item($data) {
		// Zend_Debug::dump($data);
		//die();
		
		/*
		 $sent = new \PHPInsight\Sentiment();
		 $senArr = array('pos' => 2,
		 'neu' => 0,
		 'neg' => 1);
		 $senStatus = array(2 => "POSITIF",
		 0 => "NETRAL",
		 1 => "NEGATIF");
		 //	Zend_Debug::dump($data);die();
		 */
		$cc = new Domas_Model_Withsolrpmas();

		foreach($data['content'] as $k => $v) {
			if($data['sources'] == 'mysql') {
				$this->update_index_mysql(md5($k));
			}
			$cek = $cc->if_id_exist($k);
			//Zend_Debug::dump($cek);// die();
			if($cek->response->numFound < 1 && $v['content'] != "") {
				$idata['id'] = $k;
				$idata['bundle'] = $v['bundle'];
				$idata['content'] = $v['content'];
				$idata['label'] = $v['title'];
				$idata['bundle_name'] = $v['bundle_name'];
				$idata['entity_type'] = 'data-html';
				$idata['entity_id'] = '2';
				$idata['content_date'] = $v['date'];
				$idata['is']['is_flag'] = 0;
				if($data['nlp_sentiment'] != "") {
					$idata['is']['is_sentiment'] = $data['nlp_sentiment'];
				} else {
					$idata['is']['is_sentiment'] = 0;
				}
				$idata['is']['is_approve'] = 0;
				$idata['is']['is_mktime'] = time();
				$idata['is']['is_mediatype'] = $data['fetch']['media_type'];
				$idata['ss']['ss_author'] = $v['author'];
				$idata['ss']['ss_category'] = $v['category'];
				$idata['ss']['ss_topic'] = $v['topic'];
				$idata['ss']['ss_icon'] = $v['icon'];
				$idata['ss']['ss_lang'] = $data['fetch']['language'];
				if(isset($v['dynamic'])&& count($v['dynamic'])> 0) {

					foreach($v['dynamic'] as $gkey => $ver) {
						$idata['ss']["ss_" . $gkey] = $ver;
					}
				}
				try {
					// Zend_Debug::dump($idata); die();
					$u = $cc->insertdata($idata);
				}
				catch(Exception $e) {
					//die("2");
					//Zend_Debug::dump($e->getMessage());
					// die();
				}
			}
		}
		//die("3");
		return array("transaction" => true);
	}
}
