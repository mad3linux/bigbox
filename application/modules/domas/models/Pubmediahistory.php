<?php

ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3000);

class Pmas_Model_Pubmediahistory extends Zend_Db_Table_Abstract {

    public function pub_insert_laporan() {
        try {
            $data = $this->_db->fetchAll("select * from msi.lap_pesan");
            // Zend_Debug::dump($data);die("s");

            foreach($data as $v) {
                $input['id'] = $v['no_pesan'];
                $input['title'] = $v['judul'];
                $input['publisher'] = 'DIT-41';
                $input['author'] = 'TU Direktur 41';
                $input['source'] = 'lapin';
                $input['text'] = Html2Text\Html2Text::convert($v['isi_pesan']);
                $input['lang'] = 'id';
                $input['date_created'] = date("Y-m-d\TH:i:s\Z", strtotime($v['created_at']));
                // Zend_Debug::dump($input);
                $out = $this->pub_insert_eceos($input);
                echo $out;
                // Zend_Debug::dump($input);
            }
            // die();
        }
        catch(Exception $e) {
        }
    }

    public function pub_insert_eceos($data) {
        // Zend_Debug::dump($data);die();
        $config = new Zend_Config_Ini(APPLICATION_PATH . 
                                      '/configs/application.ini', 'production');
        $config = $config->toArray();
        $server = $config['data']['factminer']['ip'];
        $token = $config['data']['factminer']['token'];
        if($server == "" or $token == "") {
            return false;
        }
        //Zend_Debug::dump($data); die("xxxxxxxxxxxxxxxxxx");

        foreach($data as $key => $value) {
            $post_items[] = $key . '=' . $value;
        }
        $post_string = implode('&', $post_items);
        $curl_connection = curl_init("http://$server/api/document/add?access_token=$token");
        $options = array(CURLOPT_RETURNTRANSFER => true,
                         CURLOPT_CONNECTTIMEOUT => 120,
                         CURLOPT_TIMEOUT => 120,
                         CURLOPT_SSL_VERIFYPEER => false,
                         CURLOPT_POSTFIELDS =>$post_string);
        curl_setopt_array($curl_connection, $options);
        $result = curl_exec($curl_connection);
        // echo curl_error($curl_connection);
        curl_close($curl_connection);
        $ret =(json_decode($result, true));
        Zend_Debug::dump($ret);
        //die();
        return $ret;
    }

    public function pub_push() {
        $cc = new Pmas_Model_Withsolrgeneral();
        $data = $cc->searching("*", 5000, 200, array("entity_id:2", "content:[* TO * ]"));
        //Zend_Debug::dump($data); die();

        foreach($data as $v) {
            $id = $v['id'];
            $raw['fetch']['language'] = $v['ss_lang'];
            $item['content'] = $v['content'];
            //$item['content']="Andi pergi ke pasar";
            $this->insertdata_eceos($id, $item, $raw);
        }
        die("ok");
    }

    public function process_under_score($content, $tcontent) {
        $tcontent[0] = substr($tcontent[0], 1);
        $content = str_replace($tcontent[0], str_replace("_", '', $tcontent[0]), $content);
        return $content;
    }

    public function pub_get_active_crawling($input) {
        if($input ['media']== "") {
            $input ['media']= 1;
        }
        try {
            $sql = "select c.last_date, c.end_date, a.*, b.slugname as main_media, b.language, b.media_type from pmas_media_sub_crawl a inner join pmas_media b on a.media_id=b.id left join pmas_hist_crawl c on a.media_id=c.media_id where status=1 and a.media_id in (".$input['media'].")";
           $data = $this->_db->fetchAll($sql);
         //   Zend_Debug::dump($data); die();
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
        return $data;
    }

    public function pub_get_index($data) {
        //  Zend_Debug::dump($this->processing_date("Rabu 14 Sep 2016, 14:28 WIB", 1)); die();
        //Zend_Debug::dump($data); die("xx");
        $c2 = new CMS_LIB_parse();
        $z = new Pmas_Model_Pmedia();
        $data['fetch'] = $z->get_a_media_by_slug($data['medianame']);
        $params = unserialize(base64_decode($data['attrs']));
        $data['fetch'] = $data;
        $data['fetch']['params'] = $params;
        $data['indexurl'] = $data['fetch']['url_crawl'];
         //Zend_Debug::dump($data);
        //die();
        switch($data['fetch']['type_crawl']) {
            default : case 1 : return $this->processing_index_html($data);
            break;
            //  case 2 : return $this->processing_index_rss($data);
            // break;
        }
    }

    public function processing_index_rss($data) {
        try {
            $feed = new Zend_Feed_Rss($data['indexurl']);
            ///Zend_Debug::dump($feed ); die();
            $i = 0;

            foreach($feed as $v) {
                $data['indexes'][$i] = $v->link();
                if($v->description()!= "") {
                    $data['rss']['description'][$i] = $v->description();
                }
                if($v->link()!= "") {
                    $data['rss']['link'][$i] = $v->link();
                }
                if($v->pubDate()!= "") {
                    $data['rss']['pubDate'][$i] = $v->pubDate();
                }
                if($v->comments()!= "") {
                    $data['rss']['comments'][$i] = $v->comments();
                }
                if($v->author()!= "") {
                    $data['rss']['author'][$i] = $v->pubDate();
                }
                if($v->category()!= "") {
                    $data['rss']['category'][$i] = $v->category();
                }
                if($v->source()!= "") {
                    $data['rss']['source'][$i] = $v->source();
                }
                if($v->enclosure()!= "") {
                    $data['rss']['enclosure'][$i] = $v->enclosure();
                }
                if($v->guid()!= "") {
                    $data['rss']['guid'][$i] = $v->guid();
                }
                $i ++;
            }
            //Zend_Debug::dump($data);
            //die();
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }
        return $data;
    }

    public function processing_index_html($data) {
        $c2 = new CMS_LIB_parse();
      //  $content = file_get_contents($data['indexurl']);
        $past = unserialize(base64_decode($data['attrs_past']));
        // Zend_Debug::dump($data); die();
        $datetime = new DateTime($data['last_date']);
        $datetime->modify('+1 day');
        $tdate = $datetime->format('Y-m-d');
        if($tdate == $data['end_date']) {
            return $data;
        }
        $date = explode("-", $tdate);
        // Zend_Debug::dump($past); die();
        switch($past['type']) {
            case 'get' : break;

            default : foreach($past['formname'] as $key => $value) {
                $past['formatname'][$key] = str_replace("dd", $date[2], $past['formatname'][$key]);
                $past['formatname'][$key] = str_replace("mm", $date[1], $past['formatname'][$key]);
                $past['formatname'][$key] = str_replace("yyyy", $date[0], $past['formatname'][$key]);
                $post_items[] = $value . '=' . $past['formatname'][$key];
            }
            $post_string = implode('&', $post_items);
            // Zend_Debug::dump($post_string); die();
            $curl_connection = curl_init($data['indexurl']);
            $options = array(CURLOPT_RETURNTRANSFER => true,
                             CURLOPT_CONNECTTIMEOUT => 120,
                             CURLOPT_TIMEOUT => 120,
                             CURLOPT_SSL_VERIFYPEER => false,
                             CURLOPT_POSTFIELDS =>$post_string);
            curl_setopt_array($curl_connection, $options);
            $content = curl_exec($curl_connection);
            curl_close($curl_connection);
            break;
        }
        $content = $c2->remove($content, "<!--", "-->");
        $content = $c2->remove($content, "<style", "</style>");
        $content = $c2->remove($content, "<script", "</script>");
        $content = preg_replace('/\s+/', ' ', $content);
        //Zend_Debug::dump($content); die();
        $dom_1 = new Zend_Dom_Query($content);
        $dns = parse_url($data['url_crawl']);
        try {
            $rc = $dom_1->query($data['fetch']['params']['tag-index']);
            if(count($rc)> 0) {
				//die("gg");
                $urin = "";
                $k = 0;

                foreach($rc as $v) {
                    //if($k < 20) {
                    $urin = $v->getAttribute('href');
                    $cek = strpos($urin, 'http');
                   // Zend_Debug::dump($urin); die("cccc");
                    if($cek === false) {
                        $cek2 = parse_url($urin);
                        if($cek2['host'] != "") {
                            $urin = "http:" . $urin;
                        } else {
                            $urin = "http://" . $dns['host'] . "" . $urin;
                        }
                    } else {
                        $urin = $urin;
                    }
                    $data['indexes'][] = $urin;
                    //}
                    $k ++;
                }
            }
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }
        //Zend_Debug::dump($data); die();
       $this->_db->query("update pmas_hist_crawl set last_date=? where media_id=?", array($tdate, $data['media_id']));
      // Zend_Debug::dump($data); die();
        return $data;
        //return $this->get_others_page ($data);
    }

    function update_media_past($id, $data) {
        $input = base64_encode(serialize($data));
        $this->_db->query("update pmas_media_sub_crawl set attrs_past=? where media_id=$id", $input);
    }

    public function pub_get_item($data) {
        //  Zend_Debug::dump($data); die();    
        //   $c = new Pmas_Model_Pmedia();
        // $data['fetch2'] = $c->get_submedia_by_id($data['fetch']['id']);
        $k = 0;

        foreach($data['indexes'] as $z) {
			//if($k<=2) {
					$data['content'][$z] = $this->processing_item($z, $data, $k);
				$k ++;
			//}
        }
       // Zend_Debug::dump($data);die();
        return true;
    }

    public function processing_date($input, $case) {
        //echo ($case);die();
        //echo ($case);die();
        $month = array('Januari' => 'January',
                       'Februari' => 'February',
                       'Maret' => 'March',
                       'April' => 'April',
                       'Mei' => 'May',
                       'Juni' => 'June',
                       'Juli' => 'July',
                       'Agustus' => 'August',
                       'September' => 'September',
                       'Oktober' => 'October',
                       'Nopember' => 'November',
                       'Desember' => 'December');
        $month2 = array('Jan' => 'January',
                        'Feb' => 'February',
                        'Mar' => 'March',
                        'Apr' => 'April',
                        'Mei' => 'May',
                        'Jun' => 'June',
                        'Jul' => 'July',
                        'Agu' => 'August',
                        'Sep' => 'September',
                        'Okt' => 'October',
                        'Nop' => 'November',
                        'Des' => 'December');
        switch($case) {
            case 1 : $var = explode(" ", $input);
            return gmdate("Y-m-d\TH:i:s\Z", strtotime($var[1] . " " . $var[2] . " " . (int) $var[3] . " " . $var[4]));
            break;
            case 2 : // die("x");
            $var = explode(" ", trim($input));
            //Zend_Debug::dump($var); die("xxx");
            return gmdate("Y-m-d\TH:i:s\Z", strtotime($var[1] . " " . $month[$var[2]] . " " . $var[3] . " " . $var[5]));
            break;
            case '3' : $var = explode(" ", trim($input));
            // Zend_Debug::dump($var); die();
            return gmdate("Y-m-d\TH:i:s\Z", strtotime($var[1] . " " . $month[$var[2]] . " " . $var[3] . " " . $var[5]));
            break;
            case '4' : $var = explode(" ", trim($input));
            return gmdate("Y-m-d\TH:i:s\Z", strtotime($var[1] . " " . $month[$var[2]] . " " . (int) $var[3] . " " . $var[4]));
            break;
            case '5' : $var = explode(" ", trim($input));
            // Zend_Debug::dump($var); die();
            return gmdate("Y-m-d\TH:i:s\Z", strtotime($var[1] . " " . $month[$var[2]] . " " . (int) $var[3] . " " . $var[7]));
            break;
            case '6' : $var = explode(" ", trim($input));
            // Zend_Debug::dump($var); die();
            return gmdate("Y-m-d\TH:i:s\Z", strtotime($var[0] . " " . $var[1] . " " . (int) $var[2] . " " . $var[3]));
            break;
            case '7' : $var = explode(" ", trim($input));
            // Zend_Debug::dump($var); die();
            return gmdate("Y-m-d\TH:i:s\Z", strtotime($var[0] . " " . $month2[$var[1]] . " " . (int) $var[2] . " " . $var[3]));
            break;
            case '8' : $var = explode(" ", trim($input));
            return gmdate("Y-m-d\TH:i:s\Z", strtotime($var[0] . " " . $month[$var[1]] . " " . (int) $var[2] . " " . $var[3]));
            break;
            case '9' : // echo $input; die();
            //$var = explode(" ", $input);
            return gmdate("Y-m-d\TH:i:s\Z", trim($input));
            break;
            case '10' : $var = explode("•", trim($input));
            $var2 = explode(" ", trim($var[1]));
            //   Zend_Debug::dump($var2); die();
            return gmdate("Y-m-d\TH:i:s\Z", strtotime($var2[2] . " " . $var2[3] . " " . $var2[4] . " " . $var2[5]));
            break;
            case '11' : $var = explode(" ", trim($input));
            $var1 = explode("/", trim($var[1]));
            // Zend_Debug::dump($input); die();
            return gmdate("Y-m-d\TH:i:s\Z", strtotime($var1[2] . "-" . $var1[1] . "-" . $var1[0] . " " . $var[2]));
            break;
            case '12' : return gmdate("Y-m-d\TH:i:s\Z", strtotime($input));
            break;
            case '13' : $cc = new CMS_General();
            $date = $cc->find_date($input);
            //  Zend_Debug::dump($date); die();
            return gmdate("Y-m-d\TH:i:s\Z", strtotime($date));
            break;
        }
    }

    function processing_item($link, $data, $k) {
        // Zend_Debug::dump($data); die();
        $result = array();
        $c2 = new CMS_LIB_parse();
        $content = file_get_contents($link);
        $content = $c2->remove($content, "<!--", "-->");
        $content = $c2->remove($content, "<style", "</style>");
        $content = $c2->remove($content, "<script", "</script>");
        $content = preg_replace('/\s+/', ' ', $content);
        //Zend_Debug::dump($content);die();
        if(isset($data['fetch']['params']['tag-content'])&& $data['fetch']['params']['tag-content'] != "") {
            $tcontent = explode('~', $data['fetch']['params']['tag-content']);
            if(count($tcontent)> 0) {
                $tcontent[1] = 'current';
            }
        }
        if(isset($data['fetch']['params']['tag-author'])&& $data['fetch']['params']['tag-author'] != "") {
            $tauth = explode('~', $data['fetch']['params']['tag-author']);
            if(count($tauth)> 0) {
                $tauth[1] = 'current';
            }
        }
        if(isset($data['fetch']['params']['tag-category'])&& $data['fetch']['params']['tag-category'] != "") {
            $tcategory = explode('~', $data['fetch']['params']['tag-category']);
            if(count($tcategory)> 0) {
                $tcategory[1] = 'current';
            }
        }
        if(isset($data['fetch']['params']['tag-date'])&& $data['fetch']['params']['tag-date'] != "") {
            $tdate = explode('~', $data['fetch']['params']['tag-date']);
            if(count($tdate)> 0) {
                $tdate[1] = 'current';
            }
        }
        if(isset($data['fetch']['params']['tag-topic'])&& $data['fetch']['params']['tag-topic'] != "") {
            $ttopic = explode('~', $data['fetch']['params']['tag-topic']);
            if(count($ttopic)> 0) {
                $ttopic[1] = 'current';
            }
        }
        if(isset($data['fetch']['params']['tag-title'])&& $data['fetch']['params']['tag-title'] != "") {
            $tjudul = explode('~', $data['fetch']['params']['tag-title']);
            if(count($tjudul)> 0) {
                $tjudul[1] = 'current';
            }
        }
        if(isset($data['fetch']['params']['tag-icon'])&& $data['fetch']['params']['tag-icon'] != "") {
            $ticon = explode('~', $data['fetch']['params']['tag-icon']);
            if(count($ticon)> 0) {
                $ticon[1] = 'current';
            }
        }
        // Zend_Debug::dump($tdate); die();
        if($content != "") {
            if($ticon[0][0] == "_") {
            }
            $dom_1 = new Zend_Dom_Query($content);
            try {
                if(count($ticon)> 0) {
                    if($ticon[0][1] == "_") {
                        $content = $this->process_under_score($content, $ticon);
                        $ticon[0] = str_replace("_", "", $ticon[0]);
                        $dom_1 = new Zend_Dom_Query($content);
                        //Zend_Debug::dump($dom_1); 
                    }
                    $res_icon = $dom_1->query($ticon[0]);
                    // Zend_Debug::dump($res_icon->current());die();
                    if($res_icon && count($res_icon)> 0) {
                        $result['icon'] = $res_icon->$ticon[1]()->getAttribute('src');
                    }
                }
            }
            catch(Exception $e) {
                $result['icon'] = "";
            }
            try {
                if(count($tcontent)> 0) {
                    if($tcontent[0][1] == "_") {
                        $content = $this->process_under_score($content, $tcontent);
                        $tcontent[0] = str_replace("_", "", $tcontent[0]);
                        $dom_1 = new Zend_Dom_Query($content);
                        //Zend_Debug::dump($dom_1); 
                    }
                    $res_content = $dom_1->query($tcontent[0]);
                    // Zend_Debug::dump($res_content); die();
                    if($res_content && count($res_content)> 0) {
                        //$result['content'] = $res_content->$tcontent[1]()->textContent;
                        $result['content'] = $res_content->getDocument()->saveHTML($res_content->$tcontent[1]());
                        $pattern = '/<a[^>]*?href=(?:\'\'|"")[^>]*?>(.*?)<\/a>/i';
                        $replacement = '$1';
                        $result['rawcontent'] = $result['content'];
                        //  $result['content']= preg_replace($pattern, $replacement, $result['content']);
                        $result['content'] = preg_replace('/<a href="(.*?)">(.*?)<\/a>/', "\\2", $result['content']);
                        $result['content'] = Html2Text\Html2Text::convert($result['content']);
                        $result['content'] = str_replace(array("\r\n", "\n"), "\\n", $result['content']);
                    }
                }
            }
            catch(Exception $e) {
                $result['content'] = "";
            }
            try {
                if(count($tauth)> 0) {
                    if($tauth[0][1] == "_") {
                        $content = $this->process_under_score($content, $tauth);
                        $tauth[0] = str_replace("_", "", $tauth[0]);
                        $dom_1 = new Zend_Dom_Query($content);
                        //Zend_Debug::dump($dom_1); 
                    }
                    $res_auth = $dom_1->query($tauth[0]);
                    if($res_auth && count($res_auth)> 0) {
                        $result['author'] = $res_auth->$tcontent[1]()->textContent;
                    }
                }
            }
            catch(Exception $e) {
                $result['author'] = "";
            }
            try {
                if(count($tcategory)> 0) {
                    if($tcategory[0][1] == "_") {
                        $content = $this->process_under_score($content, $tcategory);
                        $tcategory[0] = str_replace("_", "", $tcategory[0]);
                        $dom_1 = new Zend_Dom_Query($content);
                        //Zend_Debug::dump($dom_1); 
                    }
                    //Zend_Debug::dump($tcategory); die();
                    $res_category = $dom_1->query($tcategory[0]);
                    if($res_category && count($res_category)> 0) {
                        $result['category'] = $res_category->$tcategory[1]()->textContent;
                    }
                }
            }
            catch(Exception $e) {
                $result['category'] = "";
            }
            try {
                $cdate = 0;
                if(isset($data['rss']['pubDate'][$k])&& $data['rss']['pubDate'][$k] != "") {
                    $result['date'] = $this->processing_date($data['rss']['pubDate'][$k], 12);
                    $cdate = 1;
                }
                if(count($tdate)> 0 && $cdate == 0) {
                    if($tdate[0][1] == "_") {
                        $content = $this->process_under_score($content, $tdate);
                        $tcategory[0] = str_replace("_", "", $tdate[0]);
                        $dom_1 = new Zend_Dom_Query($content);
                        //Zend_Debug::dump($dom_1); 
                    }
                    $res_date = $dom_1->query($tdate[0]);
                    if($res_date && count($res_date)> 0) {
                        $result['date'] = $this->processing_date($res_date->$tdate[1]()->textContent, $data["fetch"]['params']['processing-date']);
                        if($tdate[2] != "") {
                            //    echo $res_date->$tdate[1]()->getAttribute($tdate[2]); die("xx");
                            $result['date'] = $this->processing_date($res_date->$tdate[1]()->getAttribute($tdate[2]), $data["fetch"]['params']['processing-date']);
                        }
                    }
                }
            }
            catch(Exception $e) {
                $result['date'] = "";
            }
            try {
                if(count($ttopic)> 0) {
                    if($ttopic[0][1] == "_") {
                        $content = $this->process_under_score($content, $ttopic);
                        $ttopic[0] = str_replace("_", "", $ttopic[0]);
                        $dom_1 = new Zend_Dom_Query($content);
                        //Zend_Debug::dump($dom_1); 
                    }
                    $res_topic = $dom_1->query($ttopic[0]);
                    if($res_topic && count($res_topic)> 0) {
                        $result['topic'] = $res_topic->$ttopic[1]()->textContent;
                    }
                }
            }
            catch(Exception $e) {
                $result['topic'] = "";
            }
            try {
                if(count($tjudul)> 0) {
                    if($tjudul[0][1] == "_") {
                        $content = $this->process_under_score($content, $tjudul);
                        $tjudul[0] = str_replace("_", "", $tjudul[0]);
                        $dom_1 = new Zend_Dom_Query($content);
                        //Zend_Debug::dump($dom_1); 
                    }
                    $res_title = $dom_1->query($tjudul[0]);
                    //Zend_Debug::dump($res_title);die();
                    if(count($res_title)) {
                        $result['title'] = $res_title->$tjudul[1]()->textContent;
                    }
                }
            }
            catch(Exception $e) {
                $result['title'] = "";
            }
            //Zend_Debug::dump($data['items']['date'][$link]);die();
            //$result['domain'] =$this->get_domain($link);
            $result['bundle'] = $data['main_media'];
            $result['bundle_name'] = $this->get_domain($link);
            if(!isset($result['title'])&& $data['items']['title'][$link] != "") {
                $result['title'] = $data['items']['title'][$link];
            }
            if(!isset($result['date'])&& $data['items']['date'][$link] != "") {
                $result['date'] = date('Y-m-d\TH:i:s\Z', strtotime($data['items']['date'][$link]));
                //$result['date2'] =$data['items']['date'][$link];
            }
            if(!isset($result['icon'])&& $data['items']['icon'][$link] != "") {
                $result['icon'] = $data['items']['icon'][$link];
            }
        }
        //        Zend_Debug::dump($result);
        //        die();
       
	   $data['content'][$link]=$result;
	   $this->pub_insert_data_item($data);
    }

    function get_domain($url) {
        //parse_url($url);
        return parse_url($url, PHP_URL_HOST);
        $pieces = parse_url($url);
        $domain = isset($pieces['host'])? $pieces['host'] : '';
        if(preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
            return $regs['domain'];
        }
        return false;
    }

    public function pub_insert_data_item_with_noreplace($data) {
        $sent = new \PHPInsight\Sentiment();
        $senArr = array('pos' => 2,
                        'neu' => 0,
                        'neg' => 1);
        $senStatus = array(2 => "POSITIF",
                           0 => "NETRAL",
                           1 => "NEGATIF");
        $cc = new Pmas_Model_Withsolr();

        foreach($data['content'] as $k => $v) {
            $idata['id'] = md5($k);
            $idata['bundle'] = $v['bundle'];
            $idata['content'] = $v['content'];
            $idata['label'] = $v['title'];
            $idata['bundle_name'] = $v['bundle_name'];
            $idata['entity_type'] = 'data-html';
            $idata['entity_id'] = '2';
            $idata['content_date'] = $v['date'];
            $idata['is']['is_flag'] = 0;
            $idata['is']['is_sentiment'] = $senArr[$sent->categorise($v['title'])];
            $idata['is']['is_approve'] = 0;
            $idata['is']['is_mktime'] = time();
            $idata['ss']['ss_author'] = $v['author'];
            $idata['ss']['ss_category'] = $v['category'];
            $idata['ss']['ss_topic'] = $v['topic'];
            $idata['ss']['ss_icon'] = $v['icon'];
            //Zend_Debug::dump($idata); die();
            try {
                $u = $cc->insertdata($idata);
            }
            catch(Exception $e) {
                Zend_Debug::dump($e->getMessage());
                die();
            }
        }
        //die("s");
        return true;
    }

    public function pub_insert_data_item($data) {
      //  Zend_Debug::dump($data); die("zzzzzzzzzzzzzz");
        $sent = new \PHPInsight\Sentiment();
        $senArr = array('pos' => 2,
                        'neu' => 0,
                        'neg' => 1);
        $senStatus = array(2 => "POSITIF",
                           0 => "NETRAL",
                           1 => "NEGATIF");
        //	Zend_Debug::dump($data);die();
        $cc = new Pmas_Model_Withsolr();

        foreach($data['content'] as $k => $v) {
            $cek = $cc->if_id_exist($k);
            //Zend_Debug::dump($cek);// die();
            if($cek->response->numFound < 1 && $v['content'] != "") {
                $idata['id'] = $k;
                $idata['bundle'] = $v['bundle'];
                $idata['content'] = $v['content'];
                $idata['label'] = $v['title'];
                $idata['bundle_name'] = $v['bundle_name'];
                $idata['entity_type'] = 'data-html';
                $idata['entity_id'] = '2';
                $idata['content_date'] = $v['date'];
                $idata['is']['is_flag'] = 0;
                //$idata['is']['is_sentiment'] = $senArr[$sent->categorise($v['title'])];
                $idata['is']['is_approve'] = 0;
                $idata['is']['is_mktime'] = time();
                $idata['is']['is_mediatype'] = $data['fetch']['media_type'];
                $idata['ss']['ss_author'] = $v['author'];
                $idata['ss']['ss_category'] = $v['category'];
                $idata['ss']['ss_topic'] = $v['topic'];
                $idata['ss']['ss_icon'] = $v['icon'];
                $idata['ss']['ss_lang'] = $data['fetch']['language'];
                try {
                    //die("1");
                    $u = $cc->insertdata($idata);
					//Zend_Debug::dump($u);
					$this->insertdata_eceos($k, $idata, $data);
                }
                catch(Exception $e) {
                    //die("2");
                    Zend_Debug::dump($e->getMessage());
                    die();
                }
            }
        }
     //   die("3");
        return true;
    }

    public function insertdata_eceos($id, $item, $raw) {
        //Zend_Debug::dump($item); die();
        $config = new Zend_Config_Ini(APPLICATION_PATH . 
                                      '/configs/application.ini', 'production');
        $config = $config->toArray();
        $server = $config['data']['factminer']['ip'];
        $token = $config['data']['factminer']['token'];
        //Zend_Debug::dump($config['data']['factminer']);
        if($server == "" or $token == "") {
            return false;
        }
        $data['id'] = base64_encode($id);
        $data['source'] = 'news';
        $data['text'] = $item['content'];
        $data['lang'] = $raw['fetch']['language'];
        $data['date_created'] = gmdate("Y-m-d\TH:i:s\Z", time());
        $data['title'] = trim($item['label']);
        $data['publisher'] = $item['bundle_name'];
        $data['author'] = trim($item['ss_author']);

        foreach($data as $key => $value) {
            $post_items[] = $key . '=' . $value;
        }
        $post_string = implode('&', $post_items);
        $curl_connection = curl_init("http://$server/api/document/add?access_token=$token");
        $options = array(CURLOPT_RETURNTRANSFER => true,
                         CURLOPT_CONNECTTIMEOUT => 120,
                         CURLOPT_TIMEOUT => 120,
                         CURLOPT_SSL_VERIFYPEER => false,
                         CURLOPT_POSTFIELDS =>$post_string);
        curl_setopt_array($curl_connection, $options);
        $result = curl_exec($curl_connection);
        echo curl_error($curl_connection);
        curl_close($curl_connection);
        $ret =(json_decode($result, true));
        //Zend_Debug::dump($result);//die();
        return $ret;
    }
}
