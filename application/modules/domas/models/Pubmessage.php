<?php

class Domas_Model_Pubmessage extends Zend_Db_Table_Abstract {

    public function pub_sending_telegram($data) {
        $authAdapter = Zend_Auth::getInstance();
        $identity = $authAdapter->getIdentity();
        $c = new Domas_Model_Params();
        $d = $c->get_params_pmas();

        foreach($d as $v) {
            $d2[$v['display_param']] = $v['value_param'];
        }
        if($identity->fullname != "") {
            $by = $identity->fullname;
        } else {
            $by = 'System';
        }
        //Zend_Debug::dump($d2); die();
        $msg = $data['inputcustom'][0];
        //$msg = $data['msg']; 
        $target = $d2['telegramGroupIdDefault'];
        if($data['inputcustom'][1] != null) {
            $target = $data['inputcustom'][1];
        }
        //return true;
        sleep(2);
        $time = date("Y-m-d H:i:s");
        $bot_token = $d2['telegramBotToken'];
        $telegram_api = "https://api.telegram.org/bot{$bot_token}/";
        $group_id = $target;
        $tel_msg = array('chat_id' =>$group_id);
        //$msg = "== LAPORAN STATUS ==\n";
        $tel_msg['text'] =  $msg."\n runend@" . $time . "\n by:" . $by . '';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "{$telegram_api}sendMessage");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $tel_msg);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);
        curl_close($ch);
        return true;
    }

    public function sending_alert($data) {
        try {
            $raws = $this->_db->fetchRow("select * from zprahu_groups_target a inner join  zprahu_service_attrs b  on b.id_service=a.service_id where a.id=?", $data['opt_nomor']);
        }
        catch(Exception $e) {
            //Zend_Debug::dump($e->getMessage()); die();
        }
        //Zend_Debug::dump($data); die();
        $bot_token = $raws['attr1'];
        $telegram_api = "https://api.telegram.org/bot{$bot_token}/";
        $group_id = $target;
        $tel_msg = array('chat_id' => "-" .$raws['group_code']);
        //$msg = "== LAPORAN STATUS ==\n";
        $tel_msg['text'] = $data['text'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "{$telegram_api}sendMessage");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $tel_msg);
        curl_exec($ch);
        curl_close($ch);
        return true;
    }
}
