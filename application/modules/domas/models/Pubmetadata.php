<?php

/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
ini_set("memory_limit", "-1");
//ini_set("display_errors", "on");

class Domas_Model_Pubmetadata extends Zend_Db_Table_Abstract {

    public function get_adapters($con) {
        try {
            $rows = $this->_db->fetchRow("select * from zpraba_conn where id=?", $con);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
        return $rows;
    }

    public function pub_get_adapters_active($con) {
        try {
            $rows = $this->_db->fetchRow("select * from zpraba_conn where id=?", $con);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
        return $rows;
    }

    public function pub_get_proc_funct($data) {
        $cc = new Mdata_Model_Zprabametadata($data['connid']);
        switch($data['adapters']['conn_params_array']['adapter']) {
            case 'Mysql' : case 'Mysqli' : return $cc->mysql_proc_funct($data);
            break;
            case 'Oracle' : case 'Pdo_oci' : return $cc->oracle_proc_funct($data);
            break;
        }
    }

    public function insert_solr_for_oracle($data) {
        #   Zend_Debug::dump($data);
        #  die();
        $cc = new Mdata_Model_Withsolr();
        //insert skema db...
        $idata['id'] = $data['adapters']['conn_params_array']['host'] . '~' . $data['adapters']['conn_params_array']['username'];
        $idata['bundle'] = 'schema';
        $idata['content'] = $data['adapters']['conn_params_array']['dbname'];
        $idata['bundle_name'] = $data['adapters']['conn_params_array']['username'];
        $idata['entity_type'] = 'data-sources';
        $idata['entity_id'] = '1';
        $idata['is']['is_port'] = (int) $data['adapters']['conn_params_array']['port'];
        $idata['ss']['ss_username'] = $data['adapters']['conn_params_array']['username'];
        $idata['ss']['ss_password'] = $data['adapters']['conn_params_array']['password'];
        $idata['ss']['ss_adapter'] = $data['adapters']['conn_params_array']['adapter'];
        $u = $cc->insertdata($idata);
        //Zend_Debug::dump( $idata ); die();
        //insert  table

        foreach($data['tables'] as $k => $v) {
            $idata = array();
            $idata['id'] = $data['adapters']['conn_params_array']['host'] . '~' . $data['adapters']['conn_params_array']['username'] . "~" . $k;
            $idata['bundle'] = 'table';
            $idata['content'] = $k;
            $idata['bundle_name'] = $k;
            $idata['entity_type'] = 'data-sources';
            $idata['entity_id'] = '1';

            foreach($v['attributes']['indexes'] as $k2 => $v2) {
                $idata['ss']['ss_' . $v2['INDEX_NAME']] = $v2['INDEX_NAME'];
                $idata['ss']['ss_' . $v2['INDEX_NAME'] . "_TYPE"] = $v2['INDEX_TYPE'];
                $idata['ss']['ss_' . $v2['INDEX_NAME'] . "_UNIQUENESS"] = $v2['UNIQUENESS'];
                $idata['ss']['ss_' . $v2['INDEX_NAME'] . "_COMPRESSION"] = $v2['COMPRESSION'];
                $idata['ss']['ss_' . $v2['INDEX_NAME'] . "_TABLESPACE_NAME"] = $v2['TABLESPACE_NAME'];
            }
            $u = $cc->insertdata($idata);

            foreach($v as $k3 => $v3) {
                if($k3 != "attributes") {
                    $idata = array();
                    $idata['id'] = $data['adapters']['conn_params_array']['host'] . '~' . $data['adapters']['conn_params_array']['username'] . "~" . $k . '~' . $k3;
                    $idata['bundle'] = 'column';
                    $idata['content'] = $k3;
                    $idata['bundle_name'] = $k3;
                    $idata['entity_type'] = 'data-sources';
                    $idata['entity_id'] = '1';

                    foreach($v3 as $k4 => $v4) {
                        $idata['ss']['ss_' . $k4] = $v4;
                    }
                    $u = $cc->insertdata($idata);
                }
                //Zend_Debug::dump( $v3); die();
                //Zend_Debug::dump( $x); 
            }
        }

        foreach($data['procs'] as $k => $v) {

            foreach($v as $k2 => $v2) {
                $idata = array();
                $idata['id'] = $data['adapters']['conn_params_array']['host'] . '~' . $data['adapters']['conn_params_array']['username'] . "~" . $k . '~' . $k2;
                $idata['bundle'] = strtolower($k);
                $idata['content'] = $v2;
                $idata['bundle_name'] = $k2;
                $idata['entity_type'] = 'data-sources';
                $idata['entity_id'] = '1';
                $u = $cc->insertdata($idata);
            }
        }
        return $data;
    }

    public function insert_solr_for_mysql($data) {
        $cc = new Mdata_Model_Withsolr();
        $idata['id'] = $data['adapters']['conn_params_array']['host'] . '~' . $data['adapters']['conn_params_array']['dbname'];
        $idata['bundle'] = 'schema';
        $idata['content'] = $data['adapters']['conn_params_array']['dbname'];
        $idata['bundle_name'] = $data['adapters']['conn_params_array']['dbname'];
        $idata['entity_type'] = 'data-sources';
        $idata['entity_id'] = '1';
        $idata['is']['is_port'] = (int) $data['adapters']['conn_params_array']['port'];
        $idata['ss']['ss_username'] = $data['adapters']['conn_params_array']['username'];
        $idata['ss']['ss_password'] = $data['adapters']['conn_params_array']['password'];
        $idata['ss']['ss_adapter'] = $data['adapters']['conn_params_array']['adapter'];
        $u = $cc->insertdata($idata);
        //Zend_Debug::dump( $idata ); die();
        //insert  table

        foreach($data['tables'] as $k => $v) {
            $idata = array();
            $idata['id'] = $data['adapters']['conn_params_array']['host'] . '~' . $data['adapters']['conn_params_array']['dbname'] . "~" . $k;
            $idata['bundle'] = 'table';
            $idata['content'] = $k;
            $idata['bundle_name'] = $k;
            $idata['entity_type'] = 'data-sources';
            $idata['entity_id'] = '1';

            foreach($v['attributes']['indexes'] as $k2 => $v2) {
                $idata['ss']['ss_' . $v2['Key_name']] = $v2['Column_name'];
                $idata['ss']['ss_' . $v2['Key_name'] . "_type"] = $v2['Index_type'];
            }
            $u = $cc->insertdata($idata);

            foreach($v as $k3 => $v3) {
                if($k3 != "attributes") {
                    $u = $cc->insertdata($idata);
                    $idata = array();
                    $idata['id'] = $data['adapters']['conn_params_array']['host'] . '~' . $data['adapters']['conn_params_array']['dbname'] . "~" . $k . '~' . $k3;
                    $idata['bundle'] = 'column';
                    $idata['content'] = $k3;
                    $idata['bundle_name'] = $k3;
                    $idata['entity_type'] = 'data-sources';
                    $idata['entity_id'] = '1';

                    foreach($v3 as $k4 => $v4) {
                        $idata['ss']['ss_' . $k4] = $v4;
                    }
                    //Zend_Debug::dump( $v3); die();
                    $u = $cc->insertdata($idata);
                    //Zend_Debug::dump( $x); 
                }
            }
        }

        foreach($data['procs'] as $k => $v) {

            foreach($v as $k2 => $v2) {
                $idata = array();
                $idata['id'] = $data['adapters']['conn_params_array']['host'] . '~' . $data['adapters']['conn_params_array']['dbname'] . "~" . $k . '~' . $k2;
                $idata['bundle'] = strtolower($k);
                $idata['content'] = $v2;
                $idata['bundle_name'] = $k2;
                $idata['entity_type'] = 'data-sources';
                $idata['entity_id'] = '1';
                $u = $cc->insertdata($idata);
            }
        }
        return $data;
    }
    
    /**
     * Debug helper function.  This is a wrapper for var_dump() that adds
     * the <pre /> tags, cleans up newlines and indents, and runs
     * htmlentities() before output.
     *
     * @param  mixed  $var   The variable to dump.
     * @param  string $label OPTIONAL Label to prepend to output.
     * @param  bool   $echo  OPTIONAL Echo output if true.
     * @return string
     */
    public function pub_mapping_insert_solr_table_col($data) {
        switch($data['adapters']['conn_params_array']['adapter']) {
            case 'Mysql' : case 'Mysqli' : return $this->insert_solr_for_mysql($data);
            break;
            case 'Oracle' : case 'Pdo_oci' : return $this->insert_solr_for_oracle($data);
            break;
        }
    }

    private function cache() {
        $front = array('lifetime' => null,
                       'automatic_serialization' => true);
        $back = array('cache_dir' => APPLICATION_PATH . '/../cache2/');
        return Zend_Cache::factory('Core', 'File', $front, $back);
    }
    
    /**
     * mendapatkan metadata untuk tables
     *
     * $connid array $connid adalah connid.
     * @return array('transaction' => true, 'data' => $data, 'message' => 'success').
     */
    function get_indexes_switch($data, $conn_id) {
        $c = new Mdata_Model_Zprabametadata($conn_id);
        switch($data['adapters']['conn_params_array']['adapter']) {

            case 'Mysql' : case 'Mysqli' : foreach($data['tables'] as $k => $v) {
                $data['tables'][$k]['attributes']['indexes'] = $c->get_indexes($data, $k, $v);
            }
            //      Zend_Debug::dump($data); die();
            break;
            case 'Oracle' : case 'Pdo_oci' : //die("ss");
            $vdata['tables'] = $c->get_indexes_oracle($data);
            $data = array_merge_recursive($data, $vdata);
            //Zend_Debug::dump($data); die();	
            break;
        }
        return $data;
    }

    public function get_tables_metadata($conn_id) {
        try {
            $adapters = $this->get_adapters($conn_id);
            $c = new Mdata_Model_Zprabametadata($conn_id);
            $data['adapters'] = $adapters;
            $data['adapters']['conn_params_array'] = unserialize($adapters['conn_params']);
            //return array('transaction' => true, 'data' => $data, 'message' => 'success');
            $data['tables'] = $c->get_tables();
            $data = $this->get_indexes_switch($data, $conn_id);
            //Zend_Debug::dump($data); die();
            return array('transaction' => true,
                         'data' =>$data,
                         'message' => 'success');
        }
        catch(Exception $e) {
            return array('transaction' => false,
                         'data' => "",
                         'message' =>$e->getMessage());
        }
    }
    
    /* mengecek perubahan DDL database dengan inputan conn_id
     * perubahan akan di log dalam redis.
     *
     * @params array $data['id'] adalah conn_id.
     * @return 1 untuk  adanya perubanan dan 0 utk tidak ada perubahan.
     */
    public function pub_check_changed_by_conn($data) {
        // Zend_Debug::dump($data); die();
        try {
            $var = $this->cache()->load("md_" . 
                                        $data['key']);
            $tab = $this->get_tables_metadata($data['connid']);
            $data['tables'] = $tab['data']['tables'];
            $data['adapters'] = $tab['data']['adapters'];
            $cek_key = md5(serialize($data['tables']));
            if($cek_key == $var) {
                //   $data ['checked'] = 1;
                // return array('transaction' => true, 'data' => $data, 'message' => 'changed');
            }
            $data['checked'] = 0;
            //  Zend_Debug::dump($data); die();
            return $data;
        }
        catch(Exception $e) {
            return $data;
        }
    }

    public function mapping_tables_columnn($data) {
        Zend_Debug::dump($data);
        die();
    }

    public function pub_parsing_indexes_link($data) {
        $c2 = new CMS_LIB_parse();
        $html = file_get_contents($data['url']);
        $html = $c2->remove($html, "<!--", "-->");
        $html = $c2->remove($html, "<style", "</style>");
        $html = $c2->remove($html, "<script", "</script>");
        $parse = parse_url($data['url']);
        $data['dns'] = $parse['host'];
        try {
            $dom = new Zend_Dom_Query($html);
            $results = $dom->query($data['tag_index']);
            $new = array();

            foreach($results as $v) {
                $new[] = $v->getAttribute('href');
            }
        }
        catch(Exception $e) {
            //Zend_Debug::dump($e);
            //die();
        }
        $data['indexes'] = $new;
        //Zend_Debug::dump($data); die();
        return $data;
    }

    public function pub_get_content($data) {
        $c2 = new CMS_LIB_parse();

        foreach($data['indexes'] as $k => $v) {
            $html = file_get_contents($v);
            $html = $c2->remove($html, "<!--", "-->");
            $html = $c2->remove($html, "<style", "</style>");
            $html = $c2->remove($html, "<script", "</script>");
            $dom = new Zend_Dom_Query($html);
            if(isset($data['tag_title'])) {
                $title = $dom->query($data['tag_title']);
                $data['result'][$k]['title'] = trim($title->current()->textContent);
            }
            if(isset($data['tag_content'])) {
                $content = $dom->query($data['tag_content']);
                if(count($content)> 0) {
                    $p = "";

                    foreach($content as $zz) {
                        $p .= trim($zz->textContent);
                    }
                    $data['result'][$k]['content'] = $p;
                }
            }
            if(isset($data['tag_date'])) {
                $date = $dom->query($data['tag_date']);
                if(count($date)> 0) {
                    $data['result'][$k]['date'] = trim($date->current()->textContent);
                }
            }
            if(isset($data['tag_author'])) {
                $author = $dom->query($data['tag_author']);
                if(count($author)> 0) {
                    $data['result'][$k]['author'] = trim($author->current()->textContent);
                }
            }
            if(isset($data['tag_content'])) {
                $img = $dom->query($data['tag_content'] . 
                                   " img");
                if(count($img)> 0) {
                    $data['result'][$k]['img'] = $img->current()->getAttribute('src');
                }
            }
        }
        return $data;
    }

    public function pub_insert_solr_html($data) {
        $cc = new Mdata_Model_Withsolr();

        foreach($data['result'] as $k => $v) {
            $idata = array();
            $idata['id'] = $data['indexes'][$k];
            $idata['bundle'] = 'url';
            $idata['content'] = $v['content'];
            $idata['bundle_name'] = $data['dns'];
            $idata['label'] = $v['title'];
            $idata['entity_type'] = 'data-html';
            $idata['entity_id'] = '2';
            if(isset($v['date'])) {
                $idata['ss']['ss_date'] = $v['date'];
            }
            if(isset($v['author'])) {
                $idata['ss']['ss_author'] = $v['author'];
            }
            //$idata['ss']['ss_date'] = $v['date'];
            //Zend_Debug::dump($idata); die();   
            $data['message'][] = $cc->insertdata($idata);
        }
        return $data;
    }
}
