<?php


/**
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>,Jul 15, 2014
 *
 */

class Pmas_Model_Pubmssql extends Zend_Db_Table_Abstract {


function connect_mssql() {


	$params = array(
		'dbname'=>'eBdesk',
		'username'=>'sigma',
		'password'=>'s1gm4DB4dm1N',
		'port'=>'1433',
		'host'=>'MsSQL',
		'pdoType'=>'dblib'
	);

	try {
		$this->_db2 = Zend_Db::factory('Pdo_Mssql', $params);
		$this->_db2->getConnection();
	} catch (Zend_Db_Adapter_Exception $e) {
		echo $e;
	} catch (Zend_Exception $e) {
		echo $e;
	}
		// $config = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
	
		// //Zend_Debug::dump( $config); die();
        
	// $this->_db = Zend_Db::factory($config['resources']['db']['adapter'], $config['resources']['db']['params']);

}

function get_lapin_1(){
	
	$sql = "select top 100 * FROM BIN_POSWIL_INPUT_WEB_2016 where lower(jenis_laporan) ='lapin' and send_flag = 1 ORDER BY TANGGAL_INPUT DESC";
	$sql = "select * FROM BIN_POSWIL_INPUT_WEB_2016 where lower(jenis_laporan) ='lapin' and send_flag = 1 ORDER BY TANGGAL_INPUT DESC";

	try{
		$this->connect_mssql();
		$data = $this->_db2->fetchAll($sql);
		// Zend_Debug::dump($data);die();

		return $data;

	} catch(Exception $e){
		Zend_Debug::dump($e);die();
	}

	
}

function set_lapin_0_by_id($data){
	
	
	try{	
		
		$this->connect_mssql();
		
		$res = array();
		if(count($data)>0){
			
			foreach($data as $key=>$id){
				
				$sql = "update BIN_POSWIL_INPUT_WEB_2016 set send_flag=0 where 1=1 and no_laporan='".$id."' and lower(jenis_laporan) ='lapin' and send_flag = 1";
				// die($sql);
				$upd = $this->_db2->query($sql);
				
				if($upd){
					$res = array("transaction"=>true,"id"=>$id);
				} else {
					$res = array("transaction"=>false,"id"=>$id);					
				}				
			}
		}

		return $res;

	} catch(Exception $e){
		Zend_Debug::dump($e);die();
	}	
}

function pub_get_lapin(){
	
	// die("s");
	// Zend_Debug::dump($data);die();

	// $sql = "SELECT count(*) cnt FROM [eBdesk].[dbo].[BIN_POSWIL_INPUT_WEB] 
   // where lower(jenis_laporan) = 'lapin' and DATALENGTH(ISI_LAPORAN) > 0 
    // and CONVERT(DATETIME, CONVERT(VARCHAR(10), TANGGAL_INPUT, 120)) =  CONVERT(DATETIME, CONVERT(VARCHAR(10), GETDATE()-2, 120))
				// ";
	$sql = "SELECT * FROM [eBdesk].[dbo].[BIN_POSWIL_INPUT_WEB] 
   where lower(jenis_laporan) = 'lapin' and DATALENGTH(ISI_LAPORAN) > 0 
    and CONVERT(DATETIME, CONVERT(VARCHAR(10), TANGGAL_INPUT, 120)) =  CONVERT(DATETIME, CONVERT(VARCHAR(10), GETDATE()-2, 120))";
		

		// Zend_Debug::dump($data);die();
		
// $sql = "SELECT * FROM [eBdesk].[dbo].[BIN_POSWIL_INPUT_WEB] where lower(jenis_laporan) = 'lapin' and DATALENGTH(ISI_LAPORAN) > 0 and CONVERT(DATETIME, CONVERT(VARCHAR(10), TANGGAL_INPUT, 120)) =  CONVERT(DATETIME, CONVERT(VARCHAR(10), GETDATE()-2, 120))";	
				
	// $sql = "SELECT * FROM [eBdesk].[dbo].[BIN_POSWIL_INPUT_WEB] where lower(jenis_laporan) = 'lapin' and DATALENGTH(ISI_LAPORAN) > 0 and CONVERT(DATETIME, CONVERT(VARCHAR(20), TANGGAL_INPUT, 120)) > DATEADD(HOUR, -4, GETDATE())";
	// die($sql);

	try{
		$this->connect_mssql();
		$data = $this->_db2->fetchAll($sql);
			
		// Zend_Debug::dump($data);die();
		
		$cc =  new Model_Appoperator();
		$p = new Pmas_Model_Pubmediacrawler();
		$c2 = new CMS_LIB_parse();

		foreach($data as $k=>$v){
			$prov = $v['LOKASI_WILAYAH'];
			$kota = $v['LOKASI_KOTA'];
			
			try{
				if ($kota <> 0) {
					$dataWilayah =$cc->call_api(13, array('query'=>'select a.NAMA_PROVINSI,b.NAMA_KOTA,b.LONGITUDE_KOTA,b.LATITUDE_KOTA from TBL_MASTER_PROVINSI a, TBL_MASTER_KOTA b
							where a.ID_PROVINSI = b.ID_PROVINSI
							  and a.ID_PROVINSI = '.$prov.' 
							  and b.ID_KOTA = '.$kota));
				}else{
					$dataWilayah =$cc->call_api(13, array('query'=>'select distinct a.NAMA_PROVINSI,a.NAMA_PROVINSI NAMA_KOTA,LONGITUDE LONGITUDE_KOTA,LATITUDE LATITUDE_KOTA from TBL_MASTER_PROVINSI a, TBL_MASTER_KOTA b
							where a.ID_PROVINSI = b.ID_PROVINSI
							  and a.ID_PROVINSI = '.$prov));
				}
				$xxx = preg_replace("/<[^>]+>/","",$c2->remove(Html2Text\Html2Text::convert($v['ISI_LAPORAN']), "<!--", "</style>"));
				// Zend_Debug::dump('Original : '.$v['ISI_LAPORAN']);
				// Zend_Debug::dump('Regex : '.$xxx);
				// Zend_Debug::dump('Hasil Akhir : '.$c2->remove(Html2Text\Html2Text::convert($v['ISI_LAPORAN']), "<!--", "</style>"));
				// die('stop bro!!!');
				
				
				$input = array();
				$input['id'] = $v['NO_LAPORAN'];
				$input['title'] = $v['JUDUL'];
				$input['publisher'] = $v['CREATOR'];
				$input['author'] = $v['PELAPOR'];
				$input['source'] = 'lapin';
				$input['text'] = preg_replace("/<[^>]+>/","",$c2->remove(Html2Text\Html2Text::convert($v['ISI_LAPORAN']), "<!--", "</style>"));			
				$input['lang'] = 'id';
				$input['date_created'] = date("Y-m-d\TH:i:s\Z", strtotime($v['TANGGAL_INPUT']));	
				$input['province_str'] = $dataWilayah['data'][0]['NAMA_PROVINSI'];
				$input['city_str'] = $dataWilayah['data'][0]['NAMA_KOTA'];
				$input['longitude_str'] = $dataWilayah['data'][0]['LONGITUDE_KOTA'];
				$input['latitude_str'] = $dataWilayah['data'][0]['LATITUDE_KOTA'];
				
				// Zend_Debug::dump($input);die();
				$out = $p->pub_insert_eceos($input);
				// die($v['NO_LAPORAN'].'\n Send!!');			 
			} catch(Exception $e){
				Zend_Debug::dump($e.' - '.$input);die();
			}				
		}
		return array("transaction"=>true);

	} catch(Exception $e){
		Zend_Debug::dump($e);die();
	}
}

function pub_get_siit(){

	$ch = curl_init();  
	$date=date("Y-m-d");
	$date2=date("Y-m-d",strtotime("-1 days"));
	// curl_setopt($ch, CURLOPT_URL,"http://10.1.54.36:11223/get/laporan/recent/list/first/sort/ASC");
	curl_setopt($ch, CURLOPT_URL,"http://10.1.54.36:11223/get/laporan/list/filter/0/100/".$date2."/".$date."/sort/DESC"); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");                                                                 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-SIIT-API-Key:SIITqnhsqjstgirvnw3rja7k'));  
	
	$output=curl_exec($ch);
 
	curl_close($ch);
	$result =  json_decode($output);
	$result1 =  json_decode($result);
	$i=0;
	foreach($result1->results as $k=>$v){		
		 $data[$i]["id"]=$v->id;
		 $data[$i]["tanggal"]=$v->tanggal;
		 $data[$i]["nomor"]=$v->nomor;
		 $data[$i]["judul"]=$v->judul;
		 $data[$i]["jenis"]=$v->jenis;
		 $data[$i]["aspek"]=$v->aspek;
		 $data[$i]["pengirim"]=$v->pengirim;
		 $data[$i]["tujuan"]=$v->tujuan;
		 $data[$i]["approve"]=$v->approve;
		 $data[$i]["pengirim_nama"]=$v->pengirim_nama;
		 $data[$i]["lampiran"]=$v->lampiran;	
// echo 'id = '.$v->id.' tanggal = '.$v->tanggal.' nomor = '.$v->nomor.'<br>';
		$i++;
	 }
	 // echo count($data);
	 Zend_Debug::dump("http://10.1.54.36:11223/get/laporan/list/filter/0/100/".$date2."/".$date."/sort/DESC");//die();
	 // Zend_Debug::dump($id);//die();
	 // Zend_Debug::dump($result1->results);//die();
	$ll =  new Pmas_Model_Solrbigdataloc();
	$c2 = new CMS_LIB_parse();
	$p = new Pmas_Model_Pubmediacrawler();
	$i=1;
	try{
		if (count($data)!=0){
			foreach($data as $k=>$v){
			
				
				$ch1 = curl_init();  
				curl_setopt($ch1, CURLOPT_URL,"http://10.1.54.36:11223/get/laporan/detail/".$v['id']); 
				curl_setopt($ch1, CURLOPT_CUSTOMREQUEST, "GET");                                                              
				curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);                                                                      
				curl_setopt($ch1, CURLOPT_HTTPHEADER, array('X-SIIT-API-Key:SIITqnhsqjstgirvnw3rja7k'));  

				$hasil=curl_exec($ch1);

				curl_close($ch1);
				$datasiit =  json_decode($hasil);
				$datasiit1 =  json_decode($datasiit);
				echo $i.'. nomor = '.$datasiit1->nomor.'  judul = '.$datasiit1->judul.'<br>';
				// Zend_Debug::dump($datasiit1);//die();
				
				$addres=$datasiit1->isi[0]->lokasi_nama;
				$dataWilayah =$ll->search_longlat($addres);
				// Zend_Debug::dump($datasiit1->jenis_nama);
				$source = str_replace(' ', '_', $datasiit1->jenis_nama);
				Zend_Debug::dump($source);
				// Zend_Debug::dump($datasiit1->judul);
				// Zend_Debug::dump($datasiit1->satuan_kerja);
				// Zend_Debug::dump($datasiit1->pelapor_nama);
				// Zend_Debug::dump($datasiit1->narasi);
				// Zend_Debug::dump($datasiit1->created_date);
					// die();
				$input = array();
				$input['id'] = $datasiit1->nomor;
				$input['title'] = $datasiit1->judul;
				$input['publisher'] = $datasiit1->satuan_kerja;
				$input['author'] = $datasiit1->pelapor_nama;
				$input['source'] = $source;
				$input['text'] = preg_replace("/<[^>]+>/","",$c2->remove(Html2Text\Html2Text::convert($datasiit1->narasi), "<!--", "</style>"));			
				$input['lang'] = 'id';
				$input['date_created'] = date("Y-m-d\TH:i:s\Z", strtotime($datasiit1->created_date));	
				$input['province_str'] ='' ;
				$input['city_str'] ='' ;
				$input['longitude_str'] = $dataWilayah[1];
				$input['latitude_str'] = $dataWilayah[0];
				$out = $p->pub_insert_eceos($input);
				// Zend_Debug::dump($input);die();
				// Zend_Debug::dump($out);die();
				$i++;
			}
			
		}
		// echo '<br>';
		// die();
		return array("transaction"=>true);
	} catch(Exception $e){
		Zend_Debug::dump($e.' - '.$input);die();
	}
}

function pub_get_siit2(){

	$ch = curl_init();  
	$date=date("Y-m-d");
	$date2=date("Y-m-d",strtotime("-1 days"));
	// curl_setopt($ch, CURLOPT_URL,"http://10.1.54.36:11223/get/laporan/recent/list/first/sort/ASC");
	curl_setopt($ch, CURLOPT_URL,"http://10.1.54.36:11223/get/laporan/list/filter/0/100/2017-04-10/2017-04-11/sort/DESC"); 
	// curl_setopt($ch, CURLOPT_URL,"http://10.1.54.36:11223/get/laporan/list/filter/0/100/".$date2."/".$date."/sort/DESC"); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");                                                                 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-SIIT-API-Key:SIITqnhsqjstgirvnw3rja7k'));  
	
	$output=curl_exec($ch);
 
	curl_close($ch);
	$result =  json_decode($output);
	$result1 =  json_decode($result);
	$i=0;
	foreach($result1->results as $k=>$v){		
		 $data[$i]["id"]=$v->id;
		 $data[$i]["tanggal"]=$v->tanggal;
		 $data[$i]["nomor"]=$v->nomor;
		 $data[$i]["judul"]=$v->judul;
		 $data[$i]["jenis"]=$v->jenis;
		 $data[$i]["aspek"]=$v->aspek;
		 $data[$i]["pengirim"]=$v->pengirim;
		 $data[$i]["tujuan"]=$v->tujuan;
		 $data[$i]["approve"]=$v->approve;
		 $data[$i]["pengirim_nama"]=$v->pengirim_nama;
		 $data[$i]["lampiran"]=$v->lampiran;	
// echo 'id = '.$v->id.' tanggal = '.$v->tanggal.' nomor = '.$v->nomor.'<br>';
// echo $v->jenis.'<br><br>';
		$i++;
	 }
	 // echo count($data);
	 Zend_Debug::dump("http://10.1.54.36:11223/get/laporan/list/filter/0/100/".$date2."/".$date."/sort/DESC");//die();
	 // Zend_Debug::dump($id);//die();
	 // Zend_Debug::dump($result1->results);die();
	$ll =  new Pmas_Model_Solrbigdataloc();
	$c2 = new CMS_LIB_parse();
	// $p = new Pmas_Model_Pubmediacrawler();
	$p = new Analis_Model_General();
	$i=1;
	try{
		if (count($data)!=0){
			foreach($data as $k=>$v){
			
				
				$ch1 = curl_init();  
				curl_setopt($ch1, CURLOPT_URL,"http://10.1.54.36:11223/get/laporan/detail/".$v['id']); 
				curl_setopt($ch1, CURLOPT_CUSTOMREQUEST, "GET");                                                              
				curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);                                                                      
				curl_setopt($ch1, CURLOPT_HTTPHEADER, array('X-SIIT-API-Key:SIITqnhsqjstgirvnw3rja7k'));  

				$hasil=curl_exec($ch1);

				curl_close($ch1);
				$datasiit =  json_decode($hasil);
				$datasiit1 =  json_decode($datasiit);
				// echo $i.'. nomor = '.$datasiit1->nomor.'  judul = '.$datasiit1->judul.'<br>';
				// Zend_Debug::dump($datasiit1);die();
				$source = str_replace(' ', '_', $datasiit1->jenis_nama);
				Zend_Debug::dump($source);
				
				$addres=$datasiit1->isi[0]->lokasi_nama;
				$dataWilayah =$ll->search_longlat($addres);
				// Zend_Debug::dump($datasiit1->nomor);
					// die();
				$input = array();
				$input['id'] = $datasiit1->id;
				$input['nomor'] = $datasiit1->nomor;
				$input['title'] = $datasiit1->judul;
				$input['publisher'] = $datasiit1->satuan_kerja;
				$input['author'] = $datasiit1->pelapor_nama;
				$input['source'] = $datasiit1->jenis_nama;
				// $input['text'] = preg_replace("/<[^>]+>/","",$c2->remove(Html2Text\Html2Text::convert($datasiit1->narasi), "<!--", "</style>"));			
				$input['text'] = $datasiit1->narasi;			
				$input['lang'] = 'id';
				$input['date_created'] = date("Y-m-d\TH:i:s\Z", strtotime($datasiit1->created_date));	
				$input['province_str'] ='' ;
				$input['city_str'] ='' ;
				$input['longitude_str'] = $dataWilayah[1];
				$input['latitude_str'] = $dataWilayah[0];
				
				$out = $p->inputlaporandumy($input);
				
				// $out = $p->pub_insert_eceos($input);
				// // $cek= $datasiit1->jenis_nama;
				// // if($cek=="Laporan Intelijen"){
				// // $id=$datasiit1->nomor;
				// // // $conten = preg_replace("/<[^>]+>/","",$c2->remove(Html2Text\Html2Text::convert($datasiit1->narasi), "<!--", "</style>"));			
				// // $conten = $datasiit1->narasi;			
				// // $html .="";
				// // $html .="<div style=\"text-align:center;\">".$datasiit1->nomor."</div><br />";
				// // $html .="<div style=\"text-align:center;\">".$datasiit1->judul."</div><br />";
				// // $html .="<div style=\"text-align:center;\">".$datasiit1->satuan_kerja."</div><br />";
				// // $html .="<div style=\"text-align:center;\">".$datasiit1->pelapor_nama."</div><br />";
				// // $html .="<div style=\"text-align:center;\">".$datasiit1->jenis_nama."</div><br />";
				// // $html .=$conten;
				// // $html .="<br><br>";
				// // // echo $html;die();
				// // }
				
				// die();
				// Zend_Debug::dump($input);die();
				Zend_Debug::dump($i);//die();
				$i++;
			}
			
		}
		// $this->downloadlap($id,$html);
		// echo '<br>';
		// die();
		return array("transaction"=>true);
	} catch(Exception $e){
		Zend_Debug::dump($e.' - '.$input);die();
	}
}

function pub_get_siit3(){

	$ch = curl_init();  
	$date=date("Y-m-d");
	$date2=date("Y-m-d",strtotime("-1 days"));
	// curl_setopt($ch, CURLOPT_URL,"http://10.1.54.36:11223/get/laporan/recent/list/first/sort/ASC");
	// curl_setopt($ch, CURLOPT_URL,"http://10.1.54.36:11223/get/laporan/list/filter/0/100/2017-04-07/2017-04-07/sort/DESC"); 
	curl_setopt($ch, CURLOPT_URL,"http://10.1.54.36:11223/get/laporan/detail/1097968"); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");                                                                 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-SIIT-API-Key:SIITqnhsqjstgirvnw3rja7k'));  
	
	$output=curl_exec($ch);
 
	curl_close($ch);
	$result =  json_decode($output);
	$result1 =  json_decode($result);
	$i=0;
	// Zend_Debug::dump($result);//die();
	// Zend_Debug::dump($result1);//die();
	// Zend_Debug::dump($result1->results);die();
	// foreach($result1->results as $k=>$v){		
		 // $data[$i]["id"]=$v->id;
		 // $data[$i]["tanggal"]=$v->tanggal;
		 // $data[$i]["nomor"]=$v->nomor;
		 // $data[$i]["judul"]=$v->judul;
		 // $data[$i]["jenis"]=$v->jenis;
		 // $data[$i]["aspek"]=$v->aspek;
		 // $data[$i]["pengirim"]=$v->pengirim;
		 // $data[$i]["tujuan"]=$v->tujuan;
		 // $data[$i]["approve"]=$v->approve;
		 // $data[$i]["pengirim_nama"]=$v->pengirim_nama;
		 // $data[$i]["lampiran"]=$v->lampiran;	
// echo 'id = '.$v->id.' tanggal = '.$v->tanggal.' nomor = '.$v->nomor.'<br>';
		// $i++;
	 // }
	 // // echo count($data);
	 // Zend_Debug::dump("http://10.1.54.36:11223/get/laporan/list/filter/0/100/2017-04-07/2017-04-8/sort/DESC");//die();
	 // Zend_Debug::dump($id);//die();
	 // Zend_Debug::dump($result1->results);//die();
	$ll =  new Pmas_Model_Solrbigdataloc();
	$c2 = new CMS_LIB_parse();
		$input = preg_replace("/<[^>]+>/","",$c2->remove(Html2Text\Html2Text::convert($result1->narasi), "<!--", "</style>"));	
		Zend_Debug::dump($input);//die();
	$p = new Pmas_Model_Pubmediacrawler();
	$i=1;
	try{
		if (count($data)!=0){
			foreach($data as $k=>$v){
			
				
				$ch1 = curl_init();  
				curl_setopt($ch1, CURLOPT_URL,"http://10.1.54.36:11223/get/laporan/detail/".$v['id']); 
				curl_setopt($ch1, CURLOPT_CUSTOMREQUEST, "GET");                                                              
				curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);                                                                      
				curl_setopt($ch1, CURLOPT_HTTPHEADER, array('X-SIIT-API-Key:SIITqnhsqjstgirvnw3rja7k'));  

				$hasil=curl_exec($ch1);

				curl_close($ch1);
				$datasiit =  json_decode($hasil);
				$datasiit1 =  json_decode($datasiit);
				echo $i.'. nomor = '.$datasiit1->nomor.'  judul = '.$datasiit1->judul.'<br>';
				// Zend_Debug::dump($datasiit1);//die();
				
				$addres=$datasiit1->isi[0]->lokasi_nama;
				$dataWilayah =$ll->search_longlat($addres);
				// Zend_Debug::dump($datasiit1->jenis_nama);
				$source = str_replace(' ', '_', $datasiit1->jenis_nama);
				Zend_Debug::dump($source);
				// Zend_Debug::dump($datasiit1->judul);
				// Zend_Debug::dump($datasiit1->satuan_kerja);
				// Zend_Debug::dump($datasiit1->pelapor_nama);
				// Zend_Debug::dump($datasiit1->narasi);
				// Zend_Debug::dump($datasiit1->created_date);
					// die();
				$input = array();
				$input['id'] = $datasiit1->nomor;
				$input['title'] = $datasiit1->judul;
				$input['publisher'] = $datasiit1->satuan_kerja;
				$input['author'] = $datasiit1->pelapor_nama;
				$input['source'] = $source;
				$input['text'] = preg_replace("/<[^>]+>/","",$c2->remove(Html2Text\Html2Text::convert($datasiit1->narasi), "<!--", "</style>"));			
				$input['lang'] = 'id';
				$input['date_created'] = date("Y-m-d\TH:i:s\Z", strtotime($datasiit1->created_date));	
				$input['province_str'] ='' ;
				$input['city_str'] ='' ;
				$input['longitude_str'] = $dataWilayah[1];
				$input['latitude_str'] = $dataWilayah[0];
				// $out = $p->pub_insert_eceos($input);
				// Zend_Debug::dump($input);die();
				// Zend_Debug::dump($out);die();
				$i++;
			}
			
		}
		// echo '<br>';
		// die();
		return array("transaction"=>true);
	} catch(Exception $e){
		Zend_Debug::dump($e.' - '.$input);die();
	}
}

function downloadlap($id,$data){
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=Laporan_Intelijen.doc");
	header("Pragma: no-cache");
	header("Expires: 0");
	
	echo "<html>";
	echo "<head>";
	echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">";
	echo "<style>";
	echo "
	body {
			font-family: Arial, Verdana, Helvetica, Courier, sans-serif;
			font-size:9pt;
	}
	.netral{
		font-weight: bold;
		color: blue;
	}
	.negatif{
		font-weight: bold;
		color: red;
	}
	.positif{
		font-weight: bold;
		color: green;
	}
	";
	echo "</style>";
	echo "</head>";
	echo "<body>";
	// echo "<span style=\"color:#000000;font-size:12px;font-family:Arial,'Trebuchet MS', Verdana;\">";
	echo $data;
	// echo "</span>";
	echo "</body>";
	echo "</html>";
	die();
}


function pub_upd_lapin($data){
	
	
	$p = new Pmas_Model_Pubmediacrawler();
	
	$this->connect_mssql();

	try{
		
		// Zend_Debug::dump($data);die("sx");
			$input = array();
			$input['id'] = $data['NO_LAPORAN'];
			$input['title'] = $data['JUDUL'];
			$input['publisher'] = $data['CREATOR'];
			$input['author'] = $data['PELAPOR'];
			$input['source'] = 'lapintel';
			$input['text'] = Html2Text\Html2Text::convert($data['ISI_LAPORAN']);
			$input['lang'] = 'id';
			$input['date_created'] = date("Y-m-d\TH:i:s\Z", strtotime($data['TANGGAL_INPUT']));		
			// Zend_Debug::dump($input);die("x");
			
			 $out = $p->pub_insert_eceos($input);			 
			 // Zend_Debug::dump($out);die("s");
	
			$sql = "update BIN_POSWIL_INPUT_WEB_2016 set send_flag=1 where 1=1 and no_laporan='".$data["NO_LAPORAN"]."' and lower(jenis_laporan) ='lapintel' and send_flag = 0";
			// die($sql);
			$this->_db2->query($sql);

		// return array("transaction"=>true,"params"=>$data,"sql"=>$sql,"message"=>"");
		return array("transaction"=>true);

	} catch(Exception $e){
		return array("transaction"=>false);
		// Zend_Debug::dump($e);die();
	}
	
}

public function bin_poswil_update_web($id){

	$sql = "update BIN_POSWIL_INPUT_WEB_2016 set  send_flag = 1 where id_input = ? and send_flag = 0 ";

	try{
		$this->connect_mssql();
		$data = $this->_db2->query($sql,$id);
		
		return $data;

	} catch(Exception $e){
		Zend_Debug::dump($e);die();
	}

}

}