<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3000);

class Domas_Model_Pubxpath extends Zend_Db_Table_Abstract {

    public function pub_query_xpath($data) {
        if($input['rutin'] == "") {
            $input['rutin'] = 1;
        }
        try {
            $sql = "select *  from zpraba_crawl_custom where cron=?";
            $data = $this->_db->fetchAll($sql, $input['rutin']);
            //   Zend_Debug::dump($data); die();
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
        return $data;
    }

    function get_content($type, $value, $content) {
        switch($type) {
            case 'xpath' : $dom_1 = new Zend_Dom_Query($content);
            $res = $dom_1->queryXpath($value);
            if($res && count($res)> 0) {
                $out = $res->current()->textContent;
            }
            break;
            case 'css' : $dom_1 = new Zend_Dom_Query($content);
            $res = $dom_1->query($value);
            if($res && count($res)> 0) {
                $out = $res->current()->textContent;
            }
            break;
            case 'string' : $out = $value;
            break;
        }
        return $out;
    }

    public function pub_get_content($data) {
        $vz = unserialize($data['attrs']);
        $result = array();
        $c2 = new CMS_LIB_parse();
        $content = file_get_contents($data['target_url']);
        $content = $c2->remove($content, "<!--", "-->");
        $content = $c2->remove($content, "<style", "</style>");
        $content = $c2->remove($content, "<script", "</script>");
        $content = preg_replace('/\s+/', ' ', $content);
        //get title
        $data['result']['title'] = $this->get_content($data['title_type'], $data['title'], $content);
        $data['result']['content'] = $this->get_content($data['content_type'], $data['content'], $content);

        foreach($vz['fname'] as $k => $v) {
            $data['result'][$v] = $this->get_content($vz['ftype'][$k], $vz['fvalue'][$k], $content);
        }
        // $data['result']['title'] = $this->get_content($data['content_type'], $data['content'], $content);
        //Zend_Debug::dump($data); die();
        return $data;
    }

    public function pub_insert_data_item($v) {
       // Zend_Debug::dump($v); die();
        $cc = new Domas_Model_Withsolrpmas();
        //$cek = $cc->if_id_exist($v['target_url']);
        //Zend_Debug::dump($cek);// die();
        //if($cek->response->numFound < 1 && $v['content'] != "") {
            $idata['id'] = $k;
            $idata['bundle'] = parse_url($v['target_url'], PHP_URL_HOST);
           
            $idata['content'] = $v['result']['content'];
            $idata['label'] = $v['result']['title'];
            $idata['bundle_name'] = $v['bundle_name'];
            $idata['entity_type'] = 'data-html';
            $idata['entity_id'] = '2';
            $idata['is']['is_flag'] = 0;
            $idata['is']['is_approve'] = 0;
            $idata['is']['is_mktime'] = time();
            unset($v['result']['content']);
            unset($v['result']['title']);

            foreach($v['result'] as $k => $z) {
                $idata['ss']['ss_' . $k] = $z;
            }
            try {
                //Zend_Debug::dump($idata); die();
                
                $u = $cc->insertdata($idata);
            }
            catch(Exception $e) {
                //die("2");
                Zend_Debug::dump($e->getMessage());
                die();
            }
      //  }
        //die("3");
        return array("transaction"=>true);
    }
}
