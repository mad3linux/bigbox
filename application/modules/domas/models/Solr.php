<?php

class Domas_Model_Solr extends Zend_Db_Table_Abstract {

    function connect_solr() {
        $config = new Zend_Config_Ini(APPLICATION_PATH . 
                                      '/configs/application.ini', 'production');
        $config = $config->toArray();
        $options = array('hostname' =>$config['data']['pmas']['solr']['host'],
                         'login' =>$config['data']['pmas']['solr']['host'],
                         'user' =>$config['data']['pmas']['solr']['user'],
                         'password' =>$config['data']['pmas']['solr']['password'],
                         'port' =>$config['data']['pmas']['solr']['port'],
                         'path' =>$config['data']['pmas']['solr']['path']);
        try {
          //  Zend_Debug::dump($options); die();
            return new SolrClient($options);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    function connect_solr1() {
        $config = new Zend_Config_Ini(APPLICATION_PATH . 
                                      '/configs/application.ini', 'production');
        $config = $config->toArray();
        $options = array('hostname' =>$config['data']['factminer']['solr']['host'],
                         'login' =>$config['data']['factminer']['solr']['host'],
                         'user' =>$config['data']['factminer']['solr']['user'],
                         'password' =>$config['data']['factminer']['solr']['password'],
                         'port' =>$config['data']['factminer']['solr']['port'],
                         'path' =>$config['data']['factminer']['solr']['path']);
        try {
            return new SolrClient($options);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    function get_entity_id(){

        $data = array(1,2,3,4,5,6);

        return $data;
    }

    function get_entity_desc(){

        $data = array(
            "1"=>"DATA",
            "2"=>"DIGITAL MEDIA NEWS",
            "3"=>"DOCUMENT",
            "4"=>"MESSAGING",
            "5"=>"SOCIAL MEDIA TWITTER",
            "6"=>"OTHERS",
        );

        return $data;
    }

    function get_count_by_entity($p=array()) {
        
        try {

            $entity_id = $this->get_entity_id();
            // Zend_Debug::dump($entity_id);die();

            $data = array();
            $i=0;
            foreach($entity_id as $k=>$v){
                $data[$v] = 0;
                $i++;
            }
            // Zend_Debug::dump($data);//die();
            
            $n = new Domas_Model_Withsolrpmas();

            $q = "";
            if($p["startdate"]!="" && $p["enddate"]!=""){
                $q .= "content_date:[".$p["startdate"]."T00:00:00Z%20TO%20".$p["enddate"]."T23:59:59Z]";
            }
            
            $query = "select?wt=json&indent=true&fq=entity_id:[*%20TO%20*]&fl=id&q=".$q."&group=true&group.field=entity_id";
            // Zend_Debug::dump($query);die();

            $tmp = $n->get_by_rawurl($query);
            // Zend_Debug::dump($tmp);//die();

            $i = 0;
            foreach ($tmp["grouped"]["entity_id"]["groups"] as $k => $v) {
                if(in_array($v["groupValue"], $entity_id)){
                    $data[$v["groupValue"]] = $v["doclist"]["numFound"];
                }
            }

            // Zend_Debug::dump($data);die();
            
            return $data;
            
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());die();
        }
    }

    function get_sentiment(){

        $data = array(
            "negatif"=>1,
            "netral"=>0,
            "positif"=>2,
        );

        return $data;
    }

    function get_count_ranges_sentiment($p=array()){
        try {

            $entity = $this->get_entity_id();
            $sentiment = $this->get_sentiment();

            $n = new Domas_Model_Withsolrpmas();

            $data = array();
            foreach($entity as $k0=>$entity_id){

                foreach($sentiment as $k1=>$sentiment_id){


                    $fq  = "";
                    $fq .= "&fq=entity_id:".$entity_id;
                    if(isset($p['startdate']) && $p['startdate']!='' && isset($p['enddate']) && $p['enddate']!=''){
                        $fq .= "&facet.range.start=".$p["startdate"]."T00:00:00Z&facet.range.end=".$p["enddate"]."T23:59:59Z";   
                    }

                    $query = "select?q=" . urlencode($str). "&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.gap=%2B1DAY&fq=is_sentiment:" . $sentiment_id . $fq . "";

                    // Zend_Debug::dump($query);die();

                    $tmp = $n->get_by_rawurl($query);
                    // Zend_Debug::dump($tmp["facet_counts"]["facet_ranges"]["content_date"]["counts"]);//die();

                    // $ranges = array();
                    $i = 0;
                    $ii = 0;
                    foreach($tmp["facet_counts"]["facet_ranges"]["content_date"]["counts"] as $k=>$v){
                        if($k%2==0){
                            $date = date("Y-m-d", strtotime($v));
                            $data[$entity_id][$i]["date"] = $date;
                            $i++;
                        }
                        if($k%2==1){
                            $data[$entity_id][$ii][$k1] = $v;
                            $ii++;
                        }
                    }
                }
            }

            // Zend_Debug::dump($data);die();

            return $data;

        } catch(Exception $e){
            Zend_Debug::dump($e->getMessage());die();
        }
    }

    function get_discovery_pie_outside($p=array()){
        try {

            // Zend_Debug::dump($p);//die();

            $tags = array();
            if($p["tags"]!=""){
                $tags = explode(",", $p["tags"]);
            }

            // Zend_Debug::dump($tags);//die();

            $n = new Domas_Model_Withsolrpmas();

            $data = array();
            foreach($tags as $k=>$tag){

                $fq ="";
                $fq .="&fq=content_date:[".$p["startdate"]."T00:00:00Z%20TO%20".$p["enddate"]."T23:59:59Z]";
                $fq .="&fq=content:".$tag;

                $query = "select?q=*%3A*&rows=0&wt=json&indent=true".$fq;
                // Zend_Debug::dump($query);//die();

                $tmp = $n->get_by_rawurl($query);
                // Zend_Debug::dump($tmp);die();

                $data[$tag] = $tmp["response"]["numFound"];
            }

            // Zend_Debug::dump($data);die();

            return $data;

        } catch(Exception $e){
            Zend_Debug::dump($e->getMessage());die();
        }
    }

    function get_discovery_pie_inside($p=array()){

        try {

            $sentiment = $this->get_sentiment();

            // Zend_Debug::dump($p);//die();

            $tags = array();
            if($p["tags"]!=""){
                $tags = explode(",", $p["tags"]);
            }

            // Zend_Debug::dump(implode(" OR ",$tags));die();

            $n = new Domas_Model_Withsolrpmas();

            $data = array();
            // foreach($tags as $k=>$tag){

                foreach($sentiment as $k1=>$v1){
                    $fq ="";
                    $fq .="&fq=is_sentiment:".$v1;
                    $fq .="&fq=content_date:[".$p["startdate"]."T00:00:00Z%20TO%20".$p["enddate"]."T23:59:59Z]";
                    $fq .="&fq=content:".implode("%20OR%20",$tags);

                    $query = "select?q=*%3A*&rows=0&wt=json&indent=true".$fq;
                    // Zend_Debug::dump($query);die();

                    $tmp = $n->get_by_rawurl($query);
                    // Zend_Debug::dump($tmp);die();

                    // $data[$tag][$k1] = $tmp["response"]["numFound"];
                    $data[$k1] = $tmp["response"]["numFound"];

                }
            // }

            // Zend_Debug::dump($data);die();

            return $data;

        } catch(Exception $e){
            Zend_Debug::dump($e->getMessage());die();
        }
    }


    function get_katadasar($p=array()){

        try {

            $sWhere = "";
            if($p["tipe_katadasar"]!=""){

                $tipe_katadasar = explode(",",$p["tipe_katadasar"]);

                $sWhere .=" and tipe_katadasar in ('".implode("','",$tipe_katadasar)."') ";
            }

            $sql = "select * from domas_katadasar where 1=1".$sWhere;
            // Zend_Debug::dump($sql);die();

            $tmp = $this->_db->fetchAll($sql);
            // Zend_Debug::dump($tmp);die();

            $data = array();
            foreach ($tmp as $key => $value) {
                $data[$key] = $value["katadasar"];
            }

            return $data;

        } catch(Exception $e){
            Zend_Debug::dump($e->getMessage());die();
        }
    }

    function get_words(){

        try {

            $sql = "select * from domas_words";

            $tmp = $this->_db->fetchAll($sql);

            $data = array();
            foreach ($tmp as $k => $v) {
                $data[] = strtolower($v["word"]);
            }

            return $data;

        } catch(Exception $e){
            Zend_Debug::dump($e->getMessage());die();
        }
    }


    function get_count_keysearch($key="",$p=array(),$start=0,$rows=0){

        try {

            $filter = array();
            if($p["startdate"]!="" && $p["enddate"]!=""){
                $filter[] = 'content_date:['.$p["startdate"].'T00:00:00Z TO '.$p["enddate"].'T23:59:59Z]';
            }
            if($p["andsearch"]!=""){
                $andsearch = explode(",",$p["andsearch"]);
                $filter[] = 'content:"'.implode('" AND "',$andsearch).'"';
            }
            if($p["exsearch"]!=""){
                $exsearch = explode(",",$p["exsearch"]);
                $filter[] = '-content:"'.implode('" AND "',$exsearch).'"';
            }
            // Zend_Debug::dump($filter);die();

            $client = $this->connect_solr();
            $client->setResponseWriter('php');

            $query = new SolrQuery();
            $query->addFilterQuery('content:"'.$key.'"');
            foreach ($filter as $k => $v) {
                $query->addFilterQuery($v);
            }
            $query->setStart(0);
            // $query->setRows(25);
            $query_response = $client->query($query);
            $response = $query_response->getResponse()->response;
            // Zend_Debug::dump($response);die();

            return $response;

        } catch(Exception $e){
            Zend_Debug::dump($e->getMessage());die();
        }
    }


    function solrquery($p=array()){

        try {

            // Zend_Debug::dump($p);//die();

            $start = (isset($p["start"]) && (int)$p["start"]>0 ? (int)$p["start"] : 0);
            $rows = (isset($p["rows"]) && $p["rows"]!="" ? (int)$p["rows"] : 10);
            // Zend_Debug::dump($rows);die();

            $query = new SolrQuery();
            $query->setQuery($query);

            $query->setStart($start);
            $query->setRows($rows);

            if(isset($p["q"]) && $p["q"]!=""){
                $query->setQuery('"'.$p["q"].'"');
            }

            if(isset($p["filterQuery"]) && count($p["filterQuery"])>0){
                foreach($p["filterQuery"] as $k=>$v){
                    // $query->addFacetField('price');   
                    $query->addFilterQuery($v);
                }
            }

            if(isset($p["field"]) && count($p["field"])>0){
                foreach($p["field"] as $k=>$v){
                    // $query->addFacetField('price');   
                    $query->addField($v);
                }
            }

            if(isset($p["sortField"]) && $p["sortField"]!=""){
                $query->addSortField($p["sortField"]);
            }

            if(isset($p["facet"]) && ($p["facet"]==true||$p["facet"]=="true")){
                $query->setFacet(true);
            }

            if(isset($p["facetField"]) && count($p["facetField"])>0){
                foreach($p["facetField"] as $k=>$v){
                    // $query->addFacetField('price');   
                    $query->addFacetField($v);
                }
            }

            if(isset($p["facetQuery"]) && count($p["facetQuery"])>0){
                foreach($p["facetQuery"] as $k=>$v){
                    // $query->addFacetQuery('price:[* TO 500]');   
                    $query->addFacetQuery($v);
                }
            }

            // Zend_Debug::dump($query);die();


            $client = $this->connect_solr();
            $client->setResponseWriter('json');
            $query_response = $client->query($query);
            // Zend_Debug::dump($query_response->getResponse());die();

            $response = json_decode(json_encode($query_response->getResponse()), true);
            // Zend_Debug::dump($response);die();

            if(isset($response["facet_counts"]["facet_fields"])){
                // die("xx");
                $facet_fields = array();
                foreach($response["facet_counts"]["facet_fields"] as $k=>$v){
                    // Zend_Debug::dump($v);die();
                    $key = "";
                    $value = 0;
                    foreach($v as $k1=>$v1){
                        if($k1%2==0){
                            $key = $v1;
                            // die($key);
                        }
                        if($k1%2==1 && $v1>0){
                            $value = $v1;

                            $facet_fields[$k][$key] = $value;
                        }
                    }
                }
                $response["facet_counts"]["facet_fields"] = array();
                $response["facet_counts"]["facet_fields"] = $facet_fields;

            }

            // Zend_Debug::dump($response);die("ss");

            $request = str_replace("localhost", "10.2.27.49", $query_response->getRequestUrl())."&".$query_response->getRawRequest();

            $data = array(
                "params"=>$p,
                "request"=>$request,
                "response"=>$response,
            );


            return $data;

        } catch(Exception $e){
            Zend_Debug::dump($e->getMessage());die();
        }
    }





    function solrquery_select($key="", $and=array(), $excep=array(), $start=0, $rows=10){

        try {

            $query = new SolrQuery();            
            $query->setStart($start);
            $query->setRows($rows);

            if($key!=""){
                $query->addFilterQuery('content:"'.$key.'"');
            }

            if(count($and)>0){
                foreach ($and  as $key => $value) {                    
                    $query->addFilterQuery($value);
                }
            }

            if(count($excep)>0){
                foreach ($excep  as $key => $value) {                    
                    $query->addFilterQuery($value);
                }
            }

            $client = $this->connect_solr();
            $client->setResponseWriter('json');
            $query_response = $client->query($query);

            $response = json_decode(json_encode($query_response->getResponse()->response), true);
            // Zend_Debug::dump($response);die();

            $request = str_replace("localhost", "10.2.27.49", $query_response->getRequestUrl())."&".$query_response->getRawRequest();

            $data = array(
                "request"=>$request,
                "response"=>$response,
            );

            Zend_Debug::dump($data);die();

            return $response;

        } catch(Exception $e){
            Zend_Debug::dump($e->getMessage());die();
        }
    }

    //==================localhost mysql
    function getTema() {
        try {
            $data = $this->_db->fetchAll("select * from zpraba_taxonomy_term_data where vid=14 and parent=0 order by weight");
            // Zend_Debug::dump($data); 

            foreach($data as $v) {
                $new[$v['name']] = $v['tid'];
            }

            return $new;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }
    function getTopik($tema=""){

        try {

            $sWhere ="and parent in (533,529,530,532,531,534)";
            if($tema!=""){
                $sWhere = "and parent = ".(int)$tema;
            }

            $sql = "select tid,name from zpraba_taxonomy_term_data where 1=1 and level=1 ".$sWhere;
            $data = $this->_db->fetchAll($sql);
            Zend_Debug::dump($data);die();

            return $data;
        } catch(Exception $e){
            Zend_Debug::dump($e->getMessage());die();
        }
    }

    function objToArray($obj, &$arr){

        if(!is_object($obj) && !is_array($obj)){
            $arr = $obj;
            return $arr;
        }

        foreach ($obj as $key => $value)
        {
            if (!empty($value))
            {
                $arr[$key] = array();
                $this->objToArray($value, $arr[$key]);
            }
            else
            {
            $arr[$key] = $value;
            }
        }
        return $arr;
    }

    function prosentaseDigitalMedia($p=array())
    {        

        // Zend_Debug::dump($p);die();
        try {

            $client = $this->connect_solr();
            $client->setResponseWriter('json');
            $query = new SolrQuery();
            $query->setQuery($query);
            $query->setRows(0);
            if(isset($p['entity_id']) && (int)$p['entity_id']>0){
                $query->addFilterQuery('entity_id:'.(int)$p['entity_id']);
            } else {
                $query->addFilterQuery('entity_id:2');
            }
            if(isset($p['startdate']) && $p['startdate']!='' && isset($p['enddate']) && $p['enddate']!=''){
                $query->addFilterQuery('content_date:['.$p['startdate'].'T00:00:00Z TO '.$p['enddate'].'T23:59:59Z]');
            }
            if(isset($p['searchkey']) && $p['searchkey']!=''){
                $query->addFilterQuery('content:'.$p['searchkey']);
            }
            if(isset($p['andsearch']) && count($p['andsearch'])>0){
                $query->addFilterQuery('content:('.implode(' AND ',$p['andsearch']).')');
            }
            if(isset($p['exsearch']) && count($p['exsearch'])>0){
                $query->addFilterQuery('-content:('.implode(' OR ',$p['exsearch']).')');
            }
            $query->setFacet(true);
            $query->addFacetField('bundle');
            
            $query_response = $client->query($query); //Zend_Debug::dump($response);die();
            $response = (array)$query_response->getResponse();
            // Zend_Debug::dump($response);die();

            $arr = array();
            $response = $this->objToArray($response,$arr);
            // Zend_Debug::dump($response);die();

            $facet_fields = array();
            foreach ($response['facet_counts']['facet_fields'] as $key => $value) {
                $odd = array();
                $even = array();
                foreach ($value as $k => $v) {
                    if ($k % 2 == 0) {
                        $even[] = $v;
                    }
                    else {
                        $odd[] = $v;
                    }
                }
                $response['facet_counts']['facet_fields'][$key] = array_combine($even, $odd);
            }
            // Zend_Debug::dump($response);die();

            return $response;
            
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());die('X');
        }
    }


}