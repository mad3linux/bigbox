<?php

class Domas_Model_Solrhierarchy extends Zend_Db_Table_Abstract {

    function connect_solr() {
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'production');
        $config = $config->toArray();
        $options = array(
            'hostname' => $config['data']['solr']['host'],
            'login' => $config['data']['solr']['host'],
            'user' => $config['data']['solr']['user'],
            'password' => $config['data']['solr']['password'],
            'port' => $config['data']['solr']['port'],
            'path' => $config['data']['solr']['path']
        );
        try {
            return new SolrClient($options);
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    public function get_bundle_group($bundle, $id = null, $key = '*:*', $start = 0, $limit = 500) {

        try {
            $client = $this->connect_solr();
            $query = new SolrQuery();
            $query->setQuery($key);
            $query->addField("bundle_name");
            $query->addField("id");
            $query->addField("bundle");
            $query->addFilterQuery('bundle:' . $bundle . '');
            if (!is_null($id)) {
                $id = str_replace("~", "\~", $id);
                $query->addFilterQuery('id:' . $id . '');
            }
            $query->addSortField('bundle_name', SolrQuery::ORDER_ASC);
            $query->setGroup(true);
            $query->addGroupField("bundle_name");
            $query->setStart($start);
            $query->setRows($limit);
            $query_response = $client->query($query);
            $response = $query_response->getResponse();
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }

        return $response;
    }

    public function get_by_bundle_name($bundle, $bundlename, $id = null, $key = '*:*', $start = 0, $limit = 500) {

        try {

            $client = $this->connect_solr();
            $query = new SolrQuery();
            $query->setQuery($key);
            $query->addField("bundle_name");
            $query->addField("id");
            $query->addField("bundle");
            $query->addField("label");



            $query->addSortField('bundle_name', SolrQuery::ORDER_ASC);
            if (!is_null($id)) {
                $id = str_replace("~", "\~", $id);
                $query->addFilterQuery('id:' . $id . '');
            }
            if (!is_null($bundlename)) {
                //$id = str_replace("~", "\~", $id);
              //  echo $bundlename;die();
                $query->addFilterQuery('bundle_name:' . $bundlename . '');
            }


            $query->setStart($start);
            $query->setRows($limit);
            $query_response = $client->query($query);
            $response = $query_response->getResponse();
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }

        return $response;
    }

    public function get_bundle($bundle, $id = null, $fq = array(), $key = '*:*', $start = 0, $limit = 500) {

        try {

            $client = $this->connect_solr();
            $query = new SolrQuery();
            $query->setQuery($key);

            $query->addFilterQuery('bundle:' . $bundle . '');
            if (!is_null($id)) {
                $id = str_replace("~", "\~", $id);
                $query->addFilterQuery('id:' . $id . '');
            }
            if (count($fq) > 0) {
                foreach ($fq as $k => $v) {
                    $query->addFilterQuery(''.$k.':' . $v . '');
                }
            }
            $query->addSortField('bundle_name', SolrQuery::ORDER_ASC);
            $query->setStart($start);
            $query->setRows($limit);
            $query_response = $client->query($query);
            $response = $query_response->getResponse();
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }

        return $response;
    }

    public function get_by_id($id) {

        try {
            $client = $this->connect_solr();
            $query = new SolrQuery();
            $query->setQuery('*:*');
            if (!is_null($id)) {
                $id = str_replace("~", "\~", $id);
                $id = str_replace(":", "\:", $id);
                $id = str_replace("/", "\/", $id);
                $query->addFilterQuery('id:' . $id . '');
            }
            $query_response = $client->query($query);
            $response = $query_response->getResponse();
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }

        return $response;
    }

}
