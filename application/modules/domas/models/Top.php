<?php

ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3000);
ini_set("display_errors", "On");
class Domas_Model_Top extends Zend_Db_Table_Abstract {
	
	
    public function get_model() {
		$vz = $this->_db->fetchAll("select tema, stype from zpraba_predictive group by tema, stype");
		
		return $vz;
	}		

    public function get_top() {
        
		$vz = $this->_db->fetchAll("select * from pmas_top_media a inner join pmas_media b on a.media_id=b.id");
		// Zend_Debug::dump($vz); die();
		foreach($vz as $k=>$v) {
				#if($k==2){
				$c2 = new CMS_LIB_parse();
				$content = file_get_contents($v['index_url']);
				//$content = file_get_contents($data['indexurl']."&".$data['fetch']['params']['parameterpage']."=".$data['startpage']);
				$content = $c2->remove($content, "<!--", "-->");
				$content = $c2->remove($content, "<style", "</style>");
				$content = $c2->remove($content, "<script", "</script>");
				$content = preg_replace('/\s+/', ' ', $content);
			
			if($content != "") {
           
				
					try {
							$dom_1 = new Zend_Dom_Query($content);
							
							$res[$v['media_name']] = $dom_1->query($v['tags']);
								
							// Zend_Debug::dump($v['tags']);die();
							//if(count($res[$v['media_name']])> 0) {
									
								  foreach($res[$v['media_name']] as $zz) {
									 
										$result="";
										$link="";
										
										if($v['attrs']!="") {
											
											$result = $zz->getAttribute($v['attrs']);
										}else {
											$result = $zz->textContent;
										}
										
										
										$link = $zz->getAttribute('href');
										$data[$v['media_name']]['content'][] = $result;
										$data[$v['media_name']]['url'][] = $link;
									}
							
							//}
						
					}
					catch(Exception $e) {
						Zend_Debug::dump($e->getMessage()); die();
					}
			}
			
				#}
		}
		
		// Zend_Debug::dump($data);die();
		return $data;
	
        //Zend_Debug::dump($content); die();
       
       
    }

    function processing_item($link, $data, $k) {
        // Zend_Debug::dump($data); die();
        $result = array();
        $c2 = new CMS_LIB_parse();
        $content = file_get_contents($link);
        $content = $c2->remove($content, "<!--", "-->");
        $content = $c2->remove($content, "<style", "</style>");
        $content = $c2->remove($content, "<script", "</script>");
        $content = preg_replace('/\s+/', ' ', $content);
        //Zend_Debug::dump($content);die();
        if(isset($data['fetch']['params']['tag-content'])&& $data['fetch']['params']['tag-content'] != "") {
            $tcontent = explode('~', $data['fetch']['params']['tag-content']);
            if(count($tcontent)> 0) {
                $tcontent[1] = 'current';
            }
        }
        if(isset($data['fetch']['params']['tag-author'])&& $data['fetch']['params']['tag-author'] != "") {
            $tauth = explode('~', $data['fetch']['params']['tag-author']);
            if(count($tauth)> 0) {
                $tauth[1] = 'current';
            }
        }
        if(isset($data['fetch']['params']['tag-category'])&& $data['fetch']['params']['tag-category'] != "") {
            $tcategory = explode('~', $data['fetch']['params']['tag-category']);
            if(count($tcategory)> 0) {
                $tcategory[1] = 'current';
            }
        }
        if(isset($data['fetch']['params']['tag-date'])&& $data['fetch']['params']['tag-date'] != "") {
            $tdate = explode('~', $data['fetch']['params']['tag-date']);
            if(count($tdate)> 0) {
                $tdate[1] = 'current';
            }
        }
		
		 if(isset($data['fetch']['params']['tag-date2'])&& $data['fetch']['params']['tag-date2'] != "") {
            $tdate2 = explode('~', $data['fetch']['params']['tag-date2']);
            if(count($tdate2)> 0) {
                $tdate2[1] = 'current';
            }
        }
		
		 if(isset($data['fetch']['params']['tag-date3'])&& $data['fetch']['params']['tag-date3'] != "") {
            $tdate3 = explode('~', $data['fetch']['params']['tag-date3']);
            if(count($tdate3)> 0) {
                $tdate3[1] = 'current';
            }
        }
		
        if(isset($data['fetch']['params']['tag-topic'])&& $data['fetch']['params']['tag-topic'] != "") {
            $ttopic = explode('~', $data['fetch']['params']['tag-topic']);
            if(count($ttopic)> 0) {
                $ttopic[1] = 'current';
            }
        }
        if(isset($data['fetch']['params']['tag-title'])&& $data['fetch']['params']['tag-title'] != "") {
            $tjudul = explode('~', $data['fetch']['params']['tag-title']);
            if(count($tjudul)> 0) {
                $tjudul[1] = 'current';
            }
        }
        if(isset($data['fetch']['params']['tag-icon'])&& $data['fetch']['params']['tag-icon'] != "") {
            $ticon = explode('~', $data['fetch']['params']['tag-icon']);
            if(count($ticon)> 0) {
                $ticon[1] = 'current';
            }
        }
        // Zend_Debug::dump($tdate); die();
        if($content != "") {
            if($ticon[0][0] == "_") {
            }
            $dom_1 = new Zend_Dom_Query($content);
            try {
                if(count($ticon)> 0) {
                    if($ticon[0][1] == "_") {
                        $content = $this->process_under_score($content, $ticon);
                        $ticon[0] = str_replace("_", "", $ticon[0]);
                        $dom_1 = new Zend_Dom_Query($content);
                        //Zend_Debug::dump($dom_1); 
                    }
                    $res_icon = $dom_1->query($ticon[0]);
                    // Zend_Debug::dump($res_icon->current());die();
                    if($res_icon && count($res_icon)> 0) {
                        $result['icon'] = $res_icon->$ticon[1]()->getAttribute('src');
                    }
                }
            }
            catch(Exception $e) {
                $result['icon'] = "";
            }
            try {
                if(count($tcontent)> 0) {
                    if($tcontent[0][1] == "_") {
                        $content = $this->process_under_score($content, $tcontent);
                        $tcontent[0] = str_replace("_", "", $tcontent[0]);
                        $dom_1 = new Zend_Dom_Query($content);
                        //Zend_Debug::dump($dom_1); 
                    }
                    $res_content = $dom_1->query($tcontent[0]);
                    // Zend_Debug::dump($res_content); die();
                    if($res_content && count($res_content)> 0) {
                                            //$result['content'] = $res_content->$tcontent[1]()->textContent;
                        $result['content'] = $res_content->getDocument()->saveHTML($res_content->$tcontent[1]());
                        $pattern = '/<a[^>]*?href=(?:\'\'|"")[^>]*?>(.*?)<\/a>/i';
                        $replacement = '$1';
                        $result['rawcontent'] = $result['content'];
                        //  $result['content']= preg_replace($pattern, $replacement, $result['content']);
                        $result['content'] = preg_replace('/<a href="(.*?)">(.*?)<\/a>/', "\\2", $result['content']);
                        $result['content'] = Html2Text\Html2Text::convert($result['content']);
                        $result['content'] = str_replace(array("\r\n", "\n"), "\\n", $result['content']);
                       
                    }
                }
            }
            catch(Exception $e) {
                $result['content'] = "";
            }
            try {
                if(count($tauth)> 0) {
                    if($tauth[0][1] == "_") {
                        $content = $this->process_under_score($content, $tauth);
                        $tauth[0] = str_replace("_", "", $tauth[0]);
                        $dom_1 = new Zend_Dom_Query($content);
                        //Zend_Debug::dump($dom_1); 
                    }
                    $res_auth = $dom_1->query($tauth[0]);
                    if($res_auth && count($res_auth)> 0) {
                        $result['author'] = $res_auth->$tcontent[1]()->textContent;
                    }
                }
            }
            catch(Exception $e) {
                $result['author'] = "";
            }
            try {
                if(count($tcategory)> 0) {
                    if($tcategory[0][1] == "_") {
                        $content = $this->process_under_score($content, $tcategory);
                        $tcategory[0] = str_replace("_", "", $tcategory[0]);
                        $dom_1 = new Zend_Dom_Query($content);
                        //Zend_Debug::dump($dom_1); 
                    }
                    //Zend_Debug::dump($tcategory); die();
                    $res_category = $dom_1->query($tcategory[0]);
                    if($res_category && count($res_category)> 0) {
                        $result['category'] = $res_category->$tcategory[1]()->textContent;
                    }
                }
            }
            catch(Exception $e) {
                $result['category'] = "";
            }
            try {
                $cdate = 0;
                if(isset($data['rss']['pubDate'][$k])&& $data['rss']['pubDate'][$k] != "") {
                    $result['date'] = $this->processing_date($data['rss']['pubDate'][$k], 12);
                    $cdate = 1;
                }
				
				
                if(count($tdate)> 0 && $cdate == 0) {
					
                    if($tdate[0][1] == "_") {
                        $content = $this->process_under_score($content, $tdate);
                        $tdate[0] = str_replace("_", "", $tdate[0]);
                        $dom_1 = new Zend_Dom_Query($content);
                        //Zend_Debug::dump($dom_1); 
                    }
                    $res_date = $dom_1->query($tdate[0]);
                    if($res_date && count($res_date)> 0) {
                        $result['date'] = $this->processing_date($res_date->$tdate[1]()->textContent, $data["fetch"]['params']['processing-date']);
                        if($tdate[2] != "") {
                            //    echo $res_date->$tdate[1]()->getAttribute($tdate[2]); die("xx");
                            $result['date'] = $this->processing_date($res_date->$tdate[1]()->getAttribute($tdate[2]), $data["fetch"]['params']['processing-date']);
                        }
                    }
                }
				// Zend_Debug::dump(count($tdate2)); //die();
				// Zend_Debug::dump($cdate); die();
				// Zend_Debug::dump($result); die();
				if(count($tdate2)> 0 && $cdate == 0&& ($result['date']=='1970-01-01T00:00:00Z'||$result['date']=='')) {
					// die("s");
                    if($tdate2[0][1] == "_") {
                        $content = $this->process_under_score($content, $tdate2);
                        $tdate2[0] = str_replace("_", "", $tdate2[0]);
                        $dom_1 = new Zend_Dom_Query($content);
                        //Zend_Debug::dump($dom_1); 
                    }
                    $res_date2 = $dom_1->query($tdate2[0]);
                    if($res_date2 && count($res_date2)> 0) {
							$result['date'] = $this->processing_date($res_date2->$tdate2[1]()->textContent, $data["fetch"]['params']['processing-date2']);
                        if($tdate2[2] != "") {
                            //    echo $res_date->$tdate[1]()->getAttribute($tdate[2]); die("xx");
                            $result['date'] = $this->processing_date($res_date2->$tdate2[1]()->getAttribute($tdate2[2]), $data["fetch"]['params']['processing-date2']);
                        }
                    }
                }
				
				if(count($tdate3)> 0 && $cdate == 0&& ($result['date']=='1970-01-01T00:00:00Z'||$result['date']=='')) {
					// die("s");
                    if($tdate3[0][1] == "_") {
                        $content = $this->process_under_score($content, $tdate3);
                        $tdate3[0] = str_replace("_", "", $tdate3[0]);
                        $dom_1 = new Zend_Dom_Query($content);
                        //Zend_Debug::dump($dom_1); 
                    }
                    $res_date3 = $dom_1->query($tdate3[0]);
                    if($res_date3 && count($res_date3)> 0) {
							$result['date'] = $this->processing_date($res_date3->$tdate3[1]()->textContent, $data["fetch"]['params']['processing-date3']);
                        if($tdate3[2] != "") {
                            //    echo $res_date->$tdate[1]()->getAttribute($tdate[2]); die("xx");
                            $result['date'] = $this->processing_date($res_date3->$tdate3[1]()->getAttribute($tdate3[2]), $data["fetch"]['params']['processing-date3']);
                        }
                    }
                }

            }
            catch(Exception $e) {
                $result['date'] = "";
            }
            try {
                if(count($ttopic)> 0) {
                    if($ttopic[0][1] == "_") {
                        $content = $this->process_under_score($content, $ttopic);
                        $ttopic[0] = str_replace("_", "", $ttopic[0]);
                        $dom_1 = new Zend_Dom_Query($content);
                        //Zend_Debug::dump($dom_1); 
                    }
                    $res_topic = $dom_1->query($ttopic[0]);
                    if($res_topic && count($res_topic)> 0) {
                        $result['topic'] = $res_topic->$ttopic[1]()->textContent;
                    }
                }
            }
            catch(Exception $e) {
                $result['topic'] = "";
            }
            try {
                if(count($tjudul)> 0) {
                    if($tjudul[0][1] == "_") {
                        $content = $this->process_under_score($content, $tjudul);
                        $tjudul[0] = str_replace("_", "", $tjudul[0]);
                        $dom_1 = new Zend_Dom_Query($content);
                        //Zend_Debug::dump($dom_1); 
                    }
                    $res_title = $dom_1->query($tjudul[0]);
                    //Zend_Debug::dump($res_title);die();
                    if(count($res_title)) {
                        $result['title'] = $res_title->$tjudul[1]()->textContent;
                    }
                }
            }
            catch(Exception $e) {
                $result['title'] = "";
            }
            //Zend_Debug::dump($data['items']['date'][$link]);die();
            //$result['domain'] =$this->get_domain($link);
            $result['bundle'] = $data['main_media'];
            $result['bundle_name'] = $this->get_domain($link);
            if(!isset($result['title'])&& $data['items']['title'][$link] != "") {
                $result['title'] = $data['items']['title'][$link];
            }
            if(!isset($result['date'])&& $data['items']['date'][$link] != "") {
                $result['date'] = date('Y-m-d\TH:i:s\Z', strtotime($data['items']['date'][$link]));
                //$result['date2'] =$data['items']['date'][$link];
            }
            if(!isset($result['icon'])&& $data['items']['icon'][$link] != "") {
                $result['icon'] = $data['items']['icon'][$link];
            }
        }
        //        Zend_Debug::dump($result);
        //        die();
        return $result;
    }

    function get_domain($url) {
        //parse_url($url);
        return parse_url($url, PHP_URL_HOST);
        $pieces = parse_url($url);
        $domain = isset($pieces['host'])? $pieces['host'] : '';
        if(preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
            return $regs['domain'];
        }
        return false;
    }

   

   
    
	
}
