<?php


/**
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>,Jul 15, 2014
 *
 */

class Domas_Model_Topics extends Zend_Db_Table_Abstract {

	
    public function get_tema($in) {
        try {
			
               $data = $this->_db->fetchAll("select * from  zpraba_taxonomy_term_data where tid in ($in)");

            foreach($data as $v) {
                $new[] = $v['name'];
            }
            // Zend_Debug::dump($new);
            // die();
            return implode("", $new);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }


    public function get_keys_topics($tid) {
        try {
               $data = $this->_db->fetchAll("select * from pmas_roles_keywords where topic_id=?", $tid);

            foreach($data as $v) {
                $new[] = $v['keyword'];
            }
            // Zend_Debug::dump($new);     
            // die();
            return $new;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    public function update_new($data) {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
            if($data['id'] == "") {
                $sql = "insert into from pmas_topics(topic_descr, update_date, created_by) values (?, now(), ?)";
                $this->_db->query($sql, array($data['topic_descr'], $identity->uid));
                $id = $this->_db->lastInsertId();
                $data['id'] = $id;
            } else {
                $sql = "update pmas_topics set topic_descr=? where id=?";
                $this->_db->query($sql, array($data['topic_descr'], $data['id']));
            }

            foreach($data['class'] as $v) {
                $this->_db->query("insert into pmas_classify(cl_text, topic_id) values (?,?)", array($v, $data['id']));
                $n[] = $this->_db->lastInsertId();
            }
            return array('result' => true,
                         'message' => "succed",
                         "data" =>$n);
        }
        catch(Exception $e) {
            return array('result' => false,
                         'message' =>$e->getMessage());
        }
    }

    public function get_list() {
        try {
            $sql = "select * from pmas_topics ";
            $data = $this->_db->fetchAll($sql);
            //Zend_Debug::dump($data); die();
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($sql);
        }
    }

    public function get_a_classify($id) {
        try {
            $sql = "select * from pmas_classify where topic_id=? ";
            $data = $this->_db->fetchAll($sql, $id);
            //  Zend_Debug::dump($data); die();
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($sql);
        }
    }

    public function get_key_topics($id) {
        try {
            $sql = "select * from pmas_classify where topic_id=? ";
            $data = $this->_db->fetchAll($sql, $id);
            //  Zend_Debug::dump($data); die();
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($sql);
        }
    }

    public function get_a_topics($id) {
        try {
            $sql = "select * from pmas_topics where id=? ";
            $data = $this->_db->fetchRow($sql, $id);
            //  Zend_Debug::dump($data); die();
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($sql);
        }
    }

    public function add_new($data) {
        $authAdapter = Zend_Auth::getInstance();
        $identity = $authAdapter->getIdentity();
        try {
            $sql = "insert into from pmas_topics(topic_descr, update_date, created_by) values (?, now(), ?)";
            $this->_db->query($sql, array($data['topic_descr'], $identity->uid));
            return array('result' => true,
                         'message' => "succed");
        }
        catch(Exception $e) {
            return array('result' => false,
                         'message' =>$e->getMessage());
        }
    }
}
