    <?php


class Domas_Model_Withsolrdash extends Zend_Db_Table_Abstract {

    function connect_solr() {
        $config = new Zend_Config_Ini(APPLICATION_PATH . 
                                      '/configs/application.ini', 'production');
        $config = $config->toArray();
        $options = array('hostname' =>$config['data']['pmas']['solr']['host'],
                         'login' =>$config['data']['pmas']['solr']['host'],
                         'user' =>$config['data']['pmas']['solr']['user'],
                         'password' =>$config['data']['pmas']['solr']['password'],
                         'port' =>$config['data']['pmas']['solr']['port'],
                         'path' =>$config['data']['pmas']['solr']['path']);
        try {
            return new SolrClient($options);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    function connect_solr1() {
        $config = new Zend_Config_Ini(APPLICATION_PATH . 
                                      '/configs/application.ini', 'production');
        $config = $config->toArray();
        $options = array('hostname' =>$config['data']['factminer']['solr']['host'],
                         'login' =>$config['data']['factminer']['solr']['host'],
                         'user' =>$config['data']['factminer']['solr']['user'],
                         'password' =>$config['data']['factminer']['solr']['password'],
                         'port' =>$config['data']['factminer']['solr']['port'],
                         'path' =>$config['data']['factminer']['solr']['path']);
        try {
            return new SolrClient($options);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }
	
	public function get_factminer(){
		
		$str = "*:*";
		$str = "type:doc";
        try {
            $client = $this->connect_solr1();
            $query = new SolrQuery();
            $query->setQuery($str);
            $query->setFacet(true);
			$query->addFilterQuery("source:report");			
            $query_response = $client->query($query);
            $response = $query_response->getResponse();
			// Zend_Debug::dump($response);die();
			
			return $response->response->docs;
			
		} catch(Exception $e){
			Zend_Debug::dump($e->getMessage());die();
		}
		
	}

		 public function get_news_bin_fact($str = "*:*", $start = 0, $limit = 10, $entity = 2, $tid = array(), $filter=array()) {
		// die("x");
		// Zend_Debug::dump($str);
		// Zend_Debug::dump($start);
		// Zend_Debug::dump($limit);
		// Zend_Debug::dump($entity);
		// Zend_Debug::dump($tid);
		// Zend_Debug::dump($filter);
		// die("2");
        // return true;
        if($str == "") {
            $str = "*:*";
        }
        try {
            $client = $this->connect_solr1();
            $query = new SolrQuery();
            $query->setQuery($str);
						$query->addFilterQuery('source:lapin');
						$query->addFilterQuery('type:doc');
						$query->addFilterQuery($filter[0]);
			// if($entity!='-1'){
				// $query->addFilterQuery("entity_id:$entity");
			// }            
            // if(count($tid)> 0) {
                // $filterx = implode(" OR  ", $tid);
                // Zend_Debug::dump($client); //die();
                // $query->addFilterQuery("tid:(" . 
                                             // $filterx . 
                                             // ")");
            // }
			// if(count($filter)> 0) {

                // foreach($filter as $v) {
                    // if($v != "") {
						// echo $v;
                        // $query->addFilterQuery($v);
                    // }
                // }
				
				// die();
            // }
            //$query->addFilterQuery('bundle_name:sentiment');
            $query->addSortField('create_date', SolrQuery::ORDER_DESC);
            $query->setStart($start);
            $query->setRows($limit);
            $query_response = $client->query($query);
            $response = $query_response->getResponse();
			// Zend_Debug::dump($response->response->docs);die("s");
            return $response->response->docs;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }
		
    public function get_news_bin($str = "*:*", $start = 0, $limit = 10, $entity = 2, $tid = array(), $filter=array()) {
		
		// Zend_Debug::dump($str);
		// Zend_Debug::dump($start);
		// Zend_Debug::dump($limit);
		// Zend_Debug::dump($entity);die();
		// Zend_Debug::dump($tid);
		# Zend_Debug::dump($filter);
		# die();
        // return true;
        if($str == "") {
            $str = "*:*";
        }
        try {
            $client = $this->connect_solr();
            $query = new SolrQuery();
            $query->setQuery($str);
			if($entity!='-1'){

                $xplode = explode(",", trim($entity));
                if(count($xplode)>0){
                    // Zend_Debug::dump($xplode);die();
                    $query->addFilterQuery('entity_id:("'.implode('" OR "',$xplode).'")');
                } else {
                    $query->addFilterQuery('entity_id:"'.$entity.'"');
                }


			}            
            if(count($tid)> 0) {
                $filterx = implode(" OR  ", $tid);
                // Zend_Debug::dump($client); die();
                $query->addFilterQuery("tid:(" . 
                                             $filterx . 
                                             ")");
            }
			if(count($filter)> 0) {

                foreach($filter as $v) {
                    if($v != "") {
						// echo $v;
                        $query->addFilterQuery($v);
                    }
                }
				
				// die();
            }
            //$query->addFilterQuery('bundle_name:sentiment');
            $query->addSortField('content_date', SolrQuery::ORDER_DESC);
            $query->setStart($start);
            $query->setRows($limit);
            $query_response = $client->query($query);
            $response = $query_response->getResponse();
			 //Zend_Debug::dump($response->response->docs);die("s");
            return $response->response->docs;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }
		
		 public function get_trainingset($str = "*", $start = 0, $limit = 10, $entity = 2, $tid = array(), $filter=array()) {
		
		// Zend_Debug::dump($str);
		// Zend_Debug::dump($start);
		// Zend_Debug::dump($limit);
		// Zend_Debug::dump($entity);
		// Zend_Debug::dump($tid);
		// Zend_Debug::dump($filter);
		// die();
        // return true;
        if($str == "") {
            $str = "*:*";
        }
        try {
            $client = $this->connect_solr();
            $query = new SolrQuery();
            $query->setQuery($str);
			// if($entity!='-1'){
				$query->addFilterQuery("entity_id:2");
			// }            
            // if(count($tid)> 0) {
                // $filterx = implode(" OR  ", $tid);
                // Zend_Debug::dump($client); die();
                // $query->addFilterQuery("tid:" . 
                                             // $filterx . 
                                             // "");
            // }
			if(count($filter)> 0) {

                foreach($filter as $v) {
                    if($v != "") {
						// echo $v;
                        $query->addFilterQuery($v);
                    }
                }
				
				// die();
            }
            //$query->addFilterQuery('bundle_name:sentiment');
            $query->addSortField('content_date', SolrQuery::ORDER_DESC);
            $query->setStart($start);
            $query->setRows($limit);
            $query_response = $client->query($query);
            $response = $query_response->getResponse();
			// Zend_Debug::dump($response->response->docs);die("s");
            return $response->response->docs;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    public function get_statement($str = "*", $start = 0, $limit = 10) {
        if($str == "") {
            $str = "*";
        }
        try {
            $client = $this->connect_solr();
            $query = new SolrQuery();
            $query->setQuery($str);
            $query->addFilterQuery('entity_id:6');
            $query->addFilterQuery('bundle_name:sentiment');
            $query->addSortField('content_date', SolrQuery::ORDER_DESC);
            $query->setStart($start);
            $query->setRows($limit);
            $query_response = $client->query($query);
            $response = $query_response->getResponse();
            return $response->response->docs;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    public function dash_grouping_total_type($key = null, $ent = 2, $filter = array(), $facet = array()) {
        try {
            if($key == "") {
                $key = "*";
            }
            $client = $this->connect_solr();
            $query = new SolrQuery($key);
            if(count($filter)> 0) {

                foreach($filter as $v) {
                    $query->addFilterQuery($v);
                }
            }
            if($ent) {
                $query->addFilterQuery('entity_id:' . 
                                       $ent . 
                                       '');
            }
            $query->setFacet(true);
            $query->setRows(0);
            $query->setFacetLimit(- 1);

            foreach($facet as $c) {
                $query->addFacetField($c);
            }
            $response = $client->query($query);
            $response_array = $response->getResponse();
            $facet_data = $response_array->facet_counts->facet_fields;
            return $facet_data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
    }

    public function dash_grouping_month_interesr($ent = 2, $key = null, $filter) {
        try {
            if(!$key) {
                $key = $this->get_tags_figure();
            }
            $client = $this->connect_solr();
            $query = new SolrQuery($key);
            $query->addFilterQuery($filter);
            if($ent) {
                $query->addFilterQuery('entity_id:' . 
                                       $ent . 
                                       '');
            }
            $query->setFacet(true);
            $query->setRows(0);
            $query->setFacetLimit(- 1);
            $query->addFacetField('entity_type');
            $query->addFacetField('bundle_name');
            $query->addFacetField('is_sentiment');
            $response = $client->query($query);
            $response_array = $response->getResponse();
            $facet_data = $response_array->facet_counts->facet_fields;
            return $facet_data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
    }

    function get_tags_figure($vid = 10) {
        $new = $this->_db->fetchAll("select name as tags, tid  from zpraba_taxonomy_term_data where vid=$vid and attr1=1");

        foreach($new as $v) {
            $data[] = $v['tags'];
        }
        return $data;
    }

    function get_tags_interest($vid = 10) {
        $new = $this->_db->fetchAll("select name as tags, tid  from zpraba_taxonomy_term_data where vid=$vid and attr1 is null ");

        foreach($new as $v) {
            $data[] = $v['tags'];
        }
        //return implode (" ", $data);
        return $data;
    }

    public function dash_grouping_today($ent = 2, $key = null) {
        try {
            if(!$key) {
                $key = $this->get_tags_figure();
            }
            $client = $this->connect_solr();
            $query = new SolrQuery($key);
            $query->addFilterQuery('timestamp:[NOW/DAY-0DAY TO NOW/DAY+1DAY]');
            if($ent) {
                $query->addFilterQuery('entity_id:' . 
                                       $ent . 
                                       '');
            }
            $query->setFacet(true);
            $query->setRows(0);
            $query->setFacetLimit(- 1);
            //$query->addFacetField('entity_type');
            $query->addFacetField('is_sentiment');
            $response = $client->query($query);
            $response_array = $response->getResponse();
            $facet_data = $response_array->facet_counts->facet_fields;
            return $facet_data->is_sentiment;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
    }



    
    
    public function dash_grouping_week($ent = 2, $key = null) {
        try {
            if(!$key) {
                $key = $this->get_tags_figure();
            }
            $client = $this->connect_solr();
            $query = new SolrQuery($key);
            $query->addFilterQuery('timestamp:[' . 
                                   gmdate("Y-m-d\TH:i:s\Z", strtotime('monday last week')). 
                                                                      ' TO NOW]');
            if($ent) {
                $query->addFilterQuery('entity_id:' . 
                                       $ent . 
                                       '');
            }
            $query->setFacet(true);
            $query->setRows(0);
            $query->setFacetLimit(- 1);
            $query->addFacetField('is_sentiment');
            $response = $client->query($query);
            $response_array = $response->getResponse();
            $facet_data = $response_array->facet_counts->facet_fields;
            return $facet_data->is_sentiment;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
    }

    public function dash_grouping_month($ent = 2, $key = null) {
        if(!$key) {
            $key = $this->get_tags_figure();
        }
        //Zend_Debug::dump($key);die();
        try {
            $client = $this->connect_solr();
            $query = new SolrQuery($key);
            $query->addFilterQuery('timestamp:[' . 
                                   gmdate("Y-m-d\TH:i:s\Z", mktime(0, 0, 0, date("1"), date("n"), date("Y"))). 
                                                                                                       ' TO NOW]');
            if($ent) {
                $query->addFilterQuery('entity_id:' . 
                                       $ent . 
                                       '');
            }
            $query->setFacet(true);
            $query->setRows(0);
            $query->setFacetLimit(- 1);
            $query->addFacetField('is_sentiment');
            $response = $client->query($query);
            $response_array = $response->getResponse();
            $facet_data = $response_array->facet_counts->facet_fields;
            return $facet_data->is_sentiment;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
        }
    }
}
