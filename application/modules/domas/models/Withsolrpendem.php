<?php

class Domas_Model_Withsolrpendem extends Zend_Db_Table_Abstract {

    function connect_solr() {
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'production');
        $config = $config->toArray();
        $options = array(
            'hostname' => $config['data']['solr']['host'],
            'login' => $config['data']['solr']['host'],
            'user' => $config['data']['solr']['user'],
            'password' => $config['data']['solr']['password'],
            'port' => $config['data']['solr']['port'],
            'path' => $config['data']['solr']['path']
        );
        try {
            return new SolrClient($options);
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    public function rawRequest($xml) {
        try {
            $client = $this->connect_solr();
            $update_response = $client->request($xml);

            $response = $update_response->getResponse();


            return ($response);
        } catch (SolrException $e) {

            Zend_Debug::dump($e);
            die();
        }
    }

    public function tax_grouping() {
        try {

            $client = $this->connect_solr();
            $query = new SolrQuery('*:*');

            //$query->setQuery($query);
            $query->setFacet(true);
            $query->setRows(0);
            $query->setFacetLimit(-1);
            $query->addFacetField('tid');
            $response = $client->query($query);
            $response_array = $response->getResponse();
            $facet_data = $response_array->facet_counts->facet_fields;

            return $facet_data->tid;
        } catch (Exception $e) {

            Zend_Debug::dump($e);
        }
    }

    public function mapping($params) {
        $client = $this->connect_solr();
        $query = new SolrQuery();
        $query->setQuery($key);
    }

    public function searchtree($key, $start, $limit) {
        $client = $this->connect_solr();
        $query = new SolrQuery();
        $query->setQuery($key);

        $query->setStart($start);
        $query->setRows($limit);
        $query_response = $client->query($query);
        $response = $query_response->getResponse();
        return $response;
    }

    public function searchingall($key, $start, $limit) {
        $client = $this->connect_solr();
        $query = new SolrQuery();
        $query->setQuery($key);

        $query->setHighlight(true);
        $query->addHighlightField();
        $query->setHighlightSnippets(5, 'content');
        $query->setHighlightSimplePre('<em style="color:black;background-color:#66ffff">');
        $query->setHighlightSimplePost('</em>');

        $query->setHighlightUsePhraseHighlighter(true);
        $query->setStart($start);
        $query->setRows($limit);
        $query_response = $client->query($query);
        $response = $query_response->getResponse();
        return $response;
    }

    public function insert_document($cont, $data, $vdata) {

        try {

//["id"] => int(2)
//["server_name"] => string(9) "server 55"
//["server_ip"] => string(10) "10.2.27.55"
//["server_user"] => string(3) "tma"
//["server_pwd"] => string(8) "admintma"
//["channel"] => int(22)
//["root_loc"] => string(8) "/var/www"


            $client = $this->connect_solr();
            $query = new SolrQuery();
            $query->setQuery("*:*");

            $query->addFilterQuery("entity_type:data-documents");
            $query->addFilterQuery("bundle_name:" . $vdata['server_ip'] . "");
            $query->addFilterQuery("bundle:machine");

            $query_response = $client->query($query);
            $response = $query_response->getResponse();
            $cnt = ($response->response->numFound);
            // if ($cnt == 0) {
            //insert mesin
            $idata['id'] = $vdata['server_ip'];
            $idata['bundle'] = 'machine';
            $idata['content'] = $vdata['server_ip'];
            $idata['bundle_name'] = $vdata['server_ip'];
            $idata['entity_type'] = 'data-documents';
            $idata['entity_id'] = '3';
            //Zend_Debug::dump($idata);
            $this->insertdata($idata);
            //}
            $idata = array();
            ///insert documents
            if ($cont[0] != "") {

                $idata['id'] = $vdata['server_ip'] . "~" . $data[1];
                $idata['bundle'] = 'document';
                $idata['content'] = $cont[0];
                $idata['bundle_name'] = $data[1];
                $idata['entity_type'] = 'data-documents';
                $idata['entity_id'] = '3';
                if (count($cont[1]) > 0)
                    foreach ($cont[1] as $k => $v) {
                        $idata['ss']['ss_' . $k] = $v;
                    }
                // Zend_Debug::dump($idata);die();

                $res = $this->insertdata($idata);
            }

            Zend_Debug::dump($res);
            die();
            return true;
        } catch (Exception $e) {

            Zend_Debug::dump($e);
            die();
        }
    }

    public function filter_tax($tid, $start = 0, $limit = 100, $src="") {
        try {
            foreach ($tid as $z) {
                if (is_numeric($z)) {
                    $new[] = $z;
                }
            }
            $client = $this->connect_solr();
            $query = new SolrQuery();
            if($src!=""){
                $query->setQuery($src);
            } else {
                   $query->setQuery("*:*"); 
            }
            $filter = implode(" OR  ", $new);
            #Zend_Debug::dump($filter); //die();
            $query->addFilterQuery("tid:(" . $filter . ")");

            $query->setStart($start);
            $query->setRows($limit);
            $query_response = $client->query($query);
            $response = $query_response->getResponse();

            return $response;
        } catch (Exception $e) {

            Zend_Debug::dump($e);
            die();
        }
    }

    public function insertdata($data, $overwrite = true) {

        //Zend_Debug::dump($data); die();  
        $client = $this->connect_solr();
        $doc = new SolrInputDocument();
        try {
            if (isset($data['id']) && $data['id'] != null) {
                $doc->addField('id', $data['id']);
            }
            if (isset($data['bundle']) && $data['bundle'] != null) {
                $doc->addField('bundle', $data['bundle']);
            }
            if (isset($data['bundle_name']) && $data['bundle_name'] != null) {
                $doc->addField('bundle_name', $data['bundle_name']);
            }
            if (isset($data['content']) && $data['content'] != null) {
                $doc->addField('content', $data['content']);
            }
            if (isset($data['content_date']) && $data['content_date'] != null) {
                $doc->addField('content_date', $data['content_date']);
            }
            if (isset($data['entity_id']) && $data['entity_id'] != null) {
                $doc->addField('entity_id', $data['entity_id']);
            }
            if (isset($data['entity_type']) && $data['entity_type'] != null) {
                $doc->addField('entity_type', $data['entity_type']);
            }
            if (isset($data['hash']) && $data['hash'] != null) {
                $doc->addField('hash', $data['hash']);
            }
            if (isset($data['index_id']) && $data['index_id'] != null) {
                $doc->addField('index_id', $data['index_id']);
            }
            if (isset($data['location']) && $data['location'] != null) {
                $doc->addField('location', $data['location']);
            }

            if (isset($data['item_id']) && $data['item_id'] != null) {
                $doc->addField('item_id', $data['item_id']);
            }
            if (isset($data['label']) && $data['label'] != null) {
                $doc->addField('label', $data['label']);
            }
            if (isset($data['path']) && $data['path'] != null) {
                $doc->addField('path', $data['path']);
            }
            if (isset($data['path_alias']) && $data['path_alias'] != null) {
                $doc->addField('path_alias', $data['path_alias']);
            }
            if (isset($data['site']) && $data['site'] != null) {
                $doc->addField('site', $data['site']);
            }
            if (isset($data['sort_label']) && $data['sort_label'] != null) {
                $doc->addField('sort_label', $data['sort_label']);
            }
            if (isset($data['sort_search_api_id']) && $data['sort_search_api_id'] != null) {
                $doc->addField('sort_search_api_id', $data['sort_search_api_id']);
            }
            if (isset($data['spell']) && $data['spell'] != null) {
                $doc->addField('spell', $data['spell']);
            }
            if (isset($data['taxonomy_names']) && $data['taxonomy_names'] != null && !is_array($data['taxonomy_names'])) {
                $doc->addField('taxonomy_names', $data['taxonomy_names']);
            } else if (is_array($data['taxonomy_names'])) {
                foreach ($data['taxonomy_names'] as $v) {
                    $doc->addField('taxonomy_names', $v);
                }
            }

            if (isset($data['tid']) && $data['tid'] != null && !is_array($data['tid'])) {
                $doc->addField('tid', $data['tid']);
            } else if (is_array($data['tid'])) {
                foreach ($data['tid'] as $v) {
                    $doc->addField('tid', $v);
                }
            }

            if (isset($data['teaser']) && $data['teaser'] != null) {
                $doc->addField('teaser', $data['teaser']);
            }

            if (isset($data['ds']) && count($data['ds']) > 0) {
                foreach ($data['ds'] as $k => $v) {
                    $doc->addField($k, $v);
                }
            }
            if (isset($data['dm']) && count($data['dm']) > 0) {
                foreach ($data['dm'] as $k => $v) {
                    foreach ($v as $v2) {
                        $doc->addField($k, $v2);
                    }
                }
            }
            if (isset($data['is']) && count($data['is']) > 0) {
                foreach ($data['is'] as $k => $v) {
                    $doc->addField($k, $v);
                }
            }
            if (isset($data['im']) && count($data['im']) > 0) {
                foreach ($data['im'] as $k => $v) {
                    foreach ($v as $v2) {
                        $doc->addField($k, $v2);
                    }
                }
            }
            if (isset($data['fs']) && count($data['fs']) > 0) {
                foreach ($data['fs'] as $k => $v) {
                    $doc->addField($k, $v);
                }
            }
            if (isset($data['fm']) && count($data['fm']) > 0) {
                foreach ($data['fm'] as $k => $v) {
                    foreach ($v as $v2) {
                        $doc->addField($k, $v2);
                    }
                }
            }
            if (isset($data['ps']) && count($data['ps']) > 0) {
                foreach ($data['ps'] as $k => $v) {
                    $doc->addField($k, $v);
                }
            }
            if (isset($data['pm']) && count($data['pm']) > 0) {
                foreach ($data['pm'] as $k => $v) {
                    foreach ($v as $v2) {
                        $doc->addField($k, $v2);
                    }
                }
            }
            if (isset($data['bs']) && count($data['bs']) > 0) {
                foreach ($data['bs'] as $k => $v) {
                    $doc->addField($k, $v);
                }
            }
            if (isset($data['bm']) && count($data['bm']) > 0) {
                foreach ($data['bm'] as $k => $v) {
                    foreach ($v as $v2) {
                        $doc->addField($k, $v2);
                    }
                }
            }
            if (isset($data['ss']) && count($data['ss']) > 0) {
                foreach ($data['ss'] as $k => $v) {
                    $doc->addField($k, $v);
                }
            }
            if (isset($data['sm']) && count($data['sm']) > 0) {
                foreach ($data['sm'] as $k => $v) {
                    foreach ($v as $v2) {
                        $doc->addField($k, $v2);
                    }
                }
            }

            if (isset($data['ts']) && count($data['ts']) > 0) {
                foreach ($data['ts'] as $k => $v) {
                    $doc->addField($k, $v);
                }
            }
            if (isset($data['tm']) && count($data['tm']) > 0) {
                foreach ($data['tm'] as $k => $v) {
                    foreach ($v as $v2) {
                        $doc->addField($k, $v2);
                    }
                }
            }
            //  Zend_Debug::dump($doc); die();
            $updateResponse = $client->addDocument($doc, $overwrite);
        } catch (Exception $e) {
            return array('transaction' => false, 'result' => false, 'message' => $e->getMessage());
        }
        return array('transaction' => true, 'result' => true, 'message' =>'transaction succed' );
    }

}
