<?php

class Domas_Model_Withsolrpmas extends Zend_Db_Table_Abstract {

	function connect_solr() {
		$config = new Zend_Config_Ini(APPLICATION_PATH . 
									  '/configs/application.ini', 'production');
		$config = $config->toArray();
		$options = array('hostname' =>$config['data']['pmas']['solr']['host'],
						 'login' =>$config['data']['pmas']['solr']['host'],
						 'user' =>$config['data']['pmas']['solr']['user'],
						 'password' =>$config['data']['pmas']['solr']['password'],
						 'port' =>$config['data']['pmas']['solr']['port'],
						 'path' =>$config['data']['pmas']['solr']['path']);
		try {
			return new SolrClient($options);
		}
		catch(Exception $e) {
			Zend_Debug::dump($e);
			die();
		}
	}

	public function get_mediatype_null() {
		try {
			$client = $this->connect_solr();
			$query = new SolrQuery();
			$query->setQuery("*");
			$query->addFilterQuery('-is_mediatype:[* TO *]');
			$query->addFilterQuery('entity_id:2');
			//$query->addFilterQuery('bundle:sms');
			$query->setStart(0);
			$query->setRows(500);
			$query_response = $client->query($query);
			$response = $query_response->getResponse();
			return $response;
		}
		catch(Exception $e) {
			Zend_Debug::dump($e);
			die();
		}
	}

	public function get_by_rawurl($url) {
		$config = new Zend_Config_Ini(APPLICATION_PATH . 
									  '/configs/application.ini', 'production');
		$config = $config->toArray();
		$url = "http://" . $config['data']['pmas']['solr']['host'] . ":" . $config['data']['pmas']['solr']['port'] . "" . $config['data']['pmas']['solr']['path'] . "/" . $url;
		//echo $url;//die();
		return json_decode(file_get_contents($url), true);
	}

	public function search_for_sending_tstamp($key, $second) {
		try {
			$client = $this->connect_solr();
			$query = new SolrQuery();
			$query->setQuery($key);
			$query->addFilterQuery('timestamp:[NOW-' . 
								   $second . 
								   'SECONDS/SECOND TO NOW]');
			//$query->addFilterQuery('is_flag:0');
			$query->addFilterQuery('entity_id:2');
			$query->setStart(0);
			$query->setRows(25);
			$query_response = $client->query($query);
			$response = $query_response->getResponse();
			$data = $response->response->docs;
			//Zend_Debug::dump( $response);
			return $data;
		}
		catch(Exception $e) {
			Zend_Debug::dump($e);
			die();
		}
	}

	public function rawRequest($xml) {
		try {
			$client = $this->connect_solr();
			$update_response = $client->request($xml);
			$response = $update_response->getResponse();
			return($response);
		}
		catch(SolrException $e) {
			Zend_Debug::dump($e);
			die();
		}
	}

	public function entity_grouping_today() {
		try {
			$client = $this->connect_solr();
			$query = new SolrQuery('*:*');
			$query->addFilterQuery('timestamp:[NOW/DAY-0DAY TO NOW/DAY+1DAY]');
			//$query->setQuery($query);
			$query->setFacet(true);
			$query->setRows(0);
			$query->setFacetLimit(- 1);
			$query->addFacetField('entity_type');
			$response = $client->query($query);
			$response_array = $response->getResponse();
			$facet_data = $response_array->facet_counts->facet_fields;
			return $facet_data->entity_type;
		}
		catch(Exception $e) {
			Zend_Debug::dump($e);
		}
	}

	public function entity_grouping() {
		try {
			$client = $this->connect_solr();
			$query = new SolrQuery('*:*');
			//$query->setQuery($query);
			$query->setFacet(true);
			$query->setRows(0);
			$query->setFacetLimit(- 1);
			$query->addFacetField('entity_type');
			$response = $client->query($query);
			$response_array = $response->getResponse();
			$facet_data = $response_array->facet_counts->facet_fields;
			return $facet_data->entity_type;
		}
		catch(Exception $e) {
			Zend_Debug::dump($e);
		}
	}

	public function tax_grouping() {
		try {
			$client = $this->connect_solr();
			$query = new SolrQuery('*:*');
			//$query->setQuery($query);
			$query->setFacet(true);
			$query->setRows(0);
			$query->setFacetLimit(- 1);
			$query->addFacetField('tid');
			$response = $client->query($query);
			$response_array = $response->getResponse();
			$facet_data = $response_array->facet_counts->facet_fields;
			return $facet_data->tid;
		}
		catch(Exception $e) {
			Zend_Debug::dump($e);
		}
	}

	public function mapping($params) {
		$client = $this->connect_solr();
		$query = new SolrQuery();
		$query->setQuery($key);
	}

	public function searchtree($key, $start, $limit) {
		$client = $this->connect_solr();
		$query = new SolrQuery();
		$query->setQuery($key);
		$query->setStart($start);
		$query->setRows($limit);
		$query_response = $client->query($query);
		$response = $query_response->getResponse();
		return $response;
	}

	public function init_map_grouping($params) {
		$params['src'] = 2;
		$data[2] = $this->map_grouping($params);
		$params['src'] = 4;
		$data[4] = $this->map_grouping($params);
		$params['src'] = 5;
		$data[5] = $this->map_grouping($params);
		return $data;
	}

	public function map_grouping($params) {
		if($params['keyw'] == "") {
			$key = "*";
		} else {
			$key = $params['keyw'];
		}
		$client = $this->connect_solr();
		$query = new SolrQuery();
		$query->setQuery($key);
		if($params['src'] != "") {
			$query->addFilterQuery('entity_id:' . 
								   $params['src'] . 
								   '');
		}
		if($params['areac'] != "") {
			$query->addFilterQuery('tid:' . 
								   $params['areac'] . 
								   '');
		}
		if($params['cond'] != "") {
			$query->addFilterQuery('is_sentiment:' . 
								   $params['cond'] . 
								   '');
		}
		$query->addFilterQuery('is_flag:1');
		$query->setFacet(true);
		$query->setRows(0);
		$query->setFacetLimit(- 1);
		$query->addFacetField('entity_id');
		$query->addFacetField('is_sentiment');
		$response = $client->query($query);
		$response_array = $response->getResponse();
		return $response_array;
		// $facet_data = $response_array->facet_counts->facet_fields;
		//Zend_Debug::dump($response_array);die();
	}

	public function searching_maps($params) {
		//Zend_Debug::dump($params); die();
		$client = $this->connect_solr();
		$query = new SolrQuery();
		if($params['keyw'] == "") {
			$key = "*";
		} else {
			$key = $params['keyw'];
		}
		$query->setQuery($key);
		$query->addFilterQuery('is_flag:1');
		if($params['cond'] != "") {
			$query->addFilterQuery('is_sentiment:' . 
								   $params['cond'] . 
								   '');
		}
		if($params['src'] != "") {
			$query->addFilterQuery('entity_id:' . 
								   $params['src'] . 
								   '');
		}
		if($params['areac'] != "") {
			$query->addFilterQuery('tid:' . 
								   $params['areac'] . 
								   '');
		}
		$query->addSortField('timestamp', SolrQuery::ORDER_DESC);
		$query->setHighlight(true);
		$query->addHighlightField();
		$query->setHighlightSnippets(5, 'content');
		$query->setHighlightSimplePre('<em style="color:black;background-color:#66ffff">');
		$query->setHighlightSimplePost('</em>');
		$query->setHighlightUsePhraseHighlighter(true);
		$query->setStart($params['start']);
		$query->setRows($params['limit']);
		$query_response = $client->query($query);
		$response = $query_response->getResponse();
		//  Zend_Debug::dump($response); die();
		return $response;
	}

	public function searchingall($key, $start, $limit) {
		$client = $this->connect_solr();
		$query = new SolrQuery();
		$query->setQuery($key);
		$query->setHighlight(true);
		$query->addHighlightField();
		$query->setHighlightSnippets(5, 'content');
		$query->setHighlightSimplePre('<em style="color:black;background-color:#66ffff">');
		$query->setHighlightSimplePost('</em>');
		$query->setHighlightUsePhraseHighlighter(true);
		$query->setStart($start);
		$query->setRows($limit);
		$query_response = $client->query($query);
		$response = $query_response->getResponse();
		return $response;
	}

	public function search_for_updated($key) {
		//    die($key);
		try {
			$client = $this->connect_solr();
			$query2 = new SolrQuery();
			$query2 = new SolrQuery();
			$query2->setQuery($key);
			$query2->addFilterQuery('timestamp:[2016-05-01T00:00:00Z TO 2016-05-03T00:00:00Z]');
			$query2->addFilterQuery('is_flag:1');
			$query2->addFilterQuery('entity_id:2');
			$query2->setStart(0);
			$query2->setRows(200);
			$query_response2 = $client->query($query2);
			$response2 = $query_response2->getResponse();
			$data2 = $response2->response->docs;
			if(!$response2->response->docs) {
				$data2 = array();
			}
			//Zend_Debug::dump( $response2);
			return $data2;
		}
		catch(Exception $e) {
			Zend_Debug::dump($e);
			die();
		}
	}

	public function search_for_sending_riil_en($key) {
		//    die($key);
		try {
			$query2 = new SolrQuery();
			$query2->setQuery($key);
			$query2->addFilterQuery('timestamp:[NOW-10MINUTES/MINUTE TO NOW]');
			$query2->addFilterQuery('is_flag:0');
			$query2->addFilterQuery('entity_id:(2 OR 4)');
			$query2->addFilterQuery('is_mediatype:3');
			$query2->setStart(0);
			$query2->setRows(25);
			$query_response2 = $client->query($query2);
			$response2 = $query_response2->getResponse();
			$data2 = $response2->response->docs;
			if(!$response->response->docs) {
				$data = array();
			}
			if(!$response2->response->docs) {
				$data2 = array();
			}
			//Zend_Debug::dump( $response2);
			// return $data2;
			return array_merge($data, $data2);
		}
		catch(Exception $e) {
			Zend_Debug::dump($e);
			die();
		}
	}

	public function search_for_sending_raw($key) {
		//    die($key);
		//$key='kasus';
		try {
			$client = $this->connect_solr();
			$query = new SolrQuery();
			$query->setQuery('*:*');
			$query->addFilterQuery('timestamp:[2016-11-20T17:00:00Z TO 2016-11-21T00:00:00Z]');
			$query->addFilterQuery('is_flag:0');
			$query->addFilterQuery('content:' . 
								   $key);
			$query->setStart(0);
			$query->setRows(100000);
			$query->addFilterQuery('entity_id:(5 OR 2 OR 4)');
			$query_response = $client->query($query);
			$response = $query_response->getResponse();
			$data = $response->response->docs;
			//Zend_Debug::dump( $response);
			
			/*
			 $query2 = new SolrQuery();
			 $query2->setQuery('*:*');
			 $query2->addFilterQuery('timestamp:[NOW-10MINUTES/MINUTE TO NOW]');
			 $query2->addFilterQuery('is_flag:0');
			 $query2->addFilterQuery('content:'.$key);
			 
			 $query2->addFilterQuery('entity_id:(2 OR 4)');
			 $query2->addFilterQuery('is_mediatype:(1 OR 2)');
			 $query2->setStart(0);
			 $query2->setRows(5);
			 $query_response2 = $client->query($query2);
			 $response2 = $query_response2->getResponse();
			 $data2 = $response2->response->docs;
			 */
			if(!$response->response->docs) {
				$data = array();
			}
			if(!$response2->response->docs) {
				$data2 = array();
			}
			//  Zend_Debug::dump( $response2);die();
			// return $data2;
			return array_merge($data, $data2);
		}
		catch(Exception $e) {
			Zend_Debug::dump($e);
			die();
		}
	}

	public function search_for_sending_riil($key) {
		//    die($key);
		//$key='kasus';
		try {
			$client = $this->connect_solr();
			$query = new SolrQuery();
			$query->setQuery('*:*');
			$query->addFilterQuery('timestamp:[NOW-3MINUTES/MINUTE TO NOW]');
			$query->addFilterQuery('is_flag:0');
			$query->addFilterQuery('content:' . 
								   $key);
			$query->setStart(0);
			$query->setRows(25);
			$query->addFilterQuery('entity_id:5');
			$query_response = $client->query($query);
			$response = $query_response->getResponse();
			$data = $response->response->docs;
			//Zend_Debug::dump( $response);
			$query2 = new SolrQuery();
			$query2->setQuery('*:*');
			$query2->addFilterQuery('timestamp:[NOW-10MINUTES/MINUTE TO NOW]');
			$query2->addFilterQuery('is_flag:0');
			$query2->addFilterQuery('content:' . 
									$key);
			$query2->addFilterQuery('entity_id:(2 OR 4)');
			$query2->addFilterQuery('is_mediatype:(1 OR 2)');
			$query2->setStart(0);
			$query2->setRows(5);
			$query_response2 = $client->query($query2);
			$response2 = $query_response2->getResponse();
			$data2 = $response2->response->docs;
			if(!$response->response->docs) {
				$data = array();
			}
			if(!$response2->response->docs) {
				$data2 = array();
			}
			//  Zend_Debug::dump( $response2);die();
			// return $data2;
			return array_merge($data, $data2);
		}
		catch(Exception $e) {
			Zend_Debug::dump($e);
			die();
		}
	}

	public function search_for_dashboard_sms() {
		//    die($key);
		try {
			$client = $this->connect_solr();
			$query = new SolrQuery();
			$query->setQuery("*");
			$query->addFilterQuery('timestamp:[NOW-120MINUTES/MINUTE TO NOW]');
			$query->addFilterQuery('is_flag:1');
			$query->addFilterQuery('bundle:sms');
			$query->setStart(0);
			$query->setRows(10);
			$query_response = $client->query($query);
			$response = $query_response->getResponse();
			return $response;
		}
		catch(Exception $e) {
			Zend_Debug::dump($e);
			die();
		}
	}

	public function search_for_dashboard_media() {
		//    die($key);
		try {
			$client = $this->connect_solr();
			$query = new SolrQuery();
			$query->setQuery("*");
			$query->addFilterQuery('timestamp:[NOW-120MINUTES/MINUTE TO NOW]');
			$query->addFilterQuery('is_flag:1');
			$query->addFilterQuery('entity_type:data-html');
			$query->setStart(0);
			$query->setRows(10);
			$query_response = $client->query($query);
			$response = $query_response->getResponse();
			return $response;
		}
		catch(Exception $e) {
			Zend_Debug::dump($e);
			die();
		}
	}

	public function search_by_id_multi($id) {
		//   die($id);
		try {
			// echo $id; die();
			$client = $this->connect_solr();
			$query = new SolrQuery();
			$query->setQuery("*");
			//  $query->addFilterQuery('timestamp:[NOW-3MINUTES/MINUTE TO NOW]');
			$query->addFilterQuery('id:' . 
								   $id . 
								   '');
			//$query->addFilterQuery('entity_type:data-socmed');
			//$query->setStart(0);
			//$query->setRows(10);
			$query_response = $client->query($query);
			$response = $query_response->getResponse();
			return $response;
		}
		catch(Exception $e) {
			Zend_Debug::dump($e);
			die();
		}
	}

	public function search_by_id($id) {
		//   die($id);
		try {
			// echo $id; die();
			$client = $this->connect_solr();
			$query = new SolrQuery();
			$query->setQuery("*");
			//  $query->addFilterQuery('timestamp:[NOW-3MINUTES/MINUTE TO NOW]');
			$query->addFilterQuery('id:"' . 
								   $id . 
								   '"');
			//$query->addFilterQuery('entity_type:data-socmed');
			//$query->setStart(0);
			//$query->setRows(10);
			$query_response = $client->query($query);
			$response = $query_response->getResponse();
			return $response;
		}
		catch(Exception $e) {
			Zend_Debug::dump($e);
			die();
		}
	}

	public function search_for_dashboard() {
		//    die($key);
		try {
			$client = $this->connect_solr();
			$query = new SolrQuery();
			$query->setQuery("*");
			$query->addFilterQuery('timestamp:[NOW-3MINUTES/MINUTE TO NOW]');
			$query->addFilterQuery('is_flag:1');
			$query->addFilterQuery('entity_type:data-socmed');
			$query->setStart(0);
			$query->setRows(10);
			$query_response = $client->query($query);
			$response = $query_response->getResponse();
			return $response;
		}
		catch(Exception $e) {
			Zend_Debug::dump($e);
			die();
		}
	}

	public function insert_document($cont, $data, $vdata) {
		try {
			$client = $this->connect_solr();
			$query = new SolrQuery();
			$query->setQuery("*:*");
			$query->addFilterQuery("entity_type:data-documents");
			$query->addFilterQuery("bundle_name:" . 
								   $vdata['server_ip'] . 
								   "");
			$query->addFilterQuery("bundle:machine");
			$query_response = $client->query($query);
			$response = $query_response->getResponse();
			$cnt =($response->response->numFound);
			// if ($cnt == 0) {
			//insert mesin
			$idata['id'] = $vdata['server_ip'];
			$idata['bundle'] = 'machine';
			$idata['content'] = $vdata['server_ip'];
			$idata['bundle_name'] = $vdata['server_ip'];
			$idata['entity_type'] = 'data-documents';
			$idata['entity_id'] = '3';
			//Zend_Debug::dump($idata);
			$this->insertdata($idata);
			//}
			$idata = array();
			///insert documents
			if($cont[0] != "") {
				$idata['id'] = $vdata['server_ip'] . "~" . $data[1];
				$idata['bundle'] = 'document';
				$idata['content'] = $cont[0];
				$idata['bundle_name'] = $data[1];
				$idata['entity_type'] = 'data-documents';
				$idata['entity_id'] = '3';
				if(count($cont[1])> 0)

					foreach($cont[1] as $k => $v) {
					$idata['ss']['ss_' . $k] = $v;
				}
				// Zend_Debug::dump($idata);die();
				$res = $this->insertdata($idata);
			}
			//Zend_Debug::dump($res);
			// die();
			return true;
		}
		catch(Exception $e) {
			// Zend_Debug::dump($e);
			//die();
		}
	}

	public function if_id_exist($id) {
		try {
			$client = $this->connect_solr();
			$query = new SolrQuery();
			$query->setQuery("*:*");
			$query->addFilterQuery('id:"' . 
								   $id . 
								   '"');
			$query_response = $client->query($query);
			$response = $query_response->getResponse();
			return $response;
		}
		catch(Exception $e) {
			// Zend_Debug::dump($e);
			//die();
		}
	}

	public function filter_tax($tid, $start = 0, $limit = 100) {
		try {

			foreach($tid as $z) {
				if(is_numeric($z)) {
					$new[] = $z;
				}
			}
			$client = $this->connect_solr();
			$query = new SolrQuery();
			$query->setQuery("*:*");
			$filter = implode(" OR  ", $new);
			#Zend_Debug::dump($filter); //die();
			$query->addFilterQuery("tid:(" . 
										 $filter . 
										 ")");
			$query->setStart($start);
			$query->setRows($limit);
			$query_response = $client->query($query);
			$response = $query_response->getResponse();
			return $response;
		}
		catch(Exception $e) {
			Zend_Debug::dump($e);
			die();
		}
	}

	public function insertdata($data, $overwrite = true) {
		//Zend_Debug::dump($data); die();  
		$client = $this->connect_solr();
		$doc = new SolrInputDocument();
		try {
			if(isset($data['id'])&& $data['id'] != null) {
				$doc->addField('id', $data['id']);
			}
			if(isset($data['bundle'])&& $data['bundle'] != null) {
				$doc->addField('bundle', $data['bundle']);
			}
			if(isset($data['bundle_name'])&& $data['bundle_name'] != null) {
				$doc->addField('bundle_name', $data['bundle_name']);
			}
			if(isset($data['content'])&& $data['content'] != null) {
				$doc->addField('content', $data['content']);
			}
			if(isset($data['content_date'])&& $data['content_date'] != null) {
				$doc->addField('content_date', $data['content_date']);
			}
			if(isset($data['entity_id'])&& $data['entity_id'] != null) {
				$doc->addField('entity_id', $data['entity_id']);
			}
			if(isset($data['entity_type'])&& $data['entity_type'] != null) {
				$doc->addField('entity_type', $data['entity_type']);
			}
			if(isset($data['hash'])&& $data['hash'] != null) {
				$doc->addField('hash', $data['hash']);
			}
			if(isset($data['index_id'])&& $data['index_id'] != null) {
				$doc->addField('index_id', $data['index_id']);
			}
			if(isset($data['location'])&& $data['location'] != null) {
				$doc->addField('location', $data['location']);
			}
			if(isset($data['item_id'])&& $data['item_id'] != null) {
				$doc->addField('item_id', $data['item_id']);
			}
			if(isset($data['label'])&& $data['label'] != null) {
				$doc->addField('label', $data['label']);
			}
			if(isset($data['path'])&& $data['path'] != null) {
				$doc->addField('path', $data['path']);
			}
			if(isset($data['path_alias'])&& $data['path_alias'] != null) {
				$doc->addField('path_alias', $data['path_alias']);
			}
			if(isset($data['site'])&& $data['site'] != null) {
				$doc->addField('site', $data['site']);
			}
			if(isset($data['sort_label'])&& $data['sort_label'] != null) {
				$doc->addField('sort_label', $data['sort_label']);
			}
			if(isset($data['sort_search_api_id'])&& $data['sort_search_api_id'] != null) {
				$doc->addField('sort_search_api_id', $data['sort_search_api_id']);
			}
			if(isset($data['spell'])&& $data['spell'] != null) {
				$doc->addField('spell', $data['spell']);
			}
			if(isset($data['taxonomy_names'])&& $data['taxonomy_names'] != null && !is_array($data['taxonomy_names'])) {
				$doc->addField('taxonomy_names', $data['taxonomy_names']);
			} else if(is_array($data['taxonomy_names'])) {

				foreach($data['taxonomy_names'] as $v) {
					$doc->addField('taxonomy_names', $v);
				}
			}
			if(isset($data['tid'])&& $data['tid'] != null && !is_array($data['tid'])) {
				$doc->addField('tid', $data['tid']);
			} else if(is_array($data['tid'])) {

				foreach($data['tid'] as $v) {
					$doc->addField('tid', $v);
				}
			}
			if(isset($data['teaser'])&& $data['teaser'] != null) {
				$doc->addField('teaser', $data['teaser']);
			}
			if(isset($data['ds'])&& count($data['ds'])> 0) {

				foreach($data['ds'] as $k => $v) {
					$doc->addField($k, $v);
				}
			}
			if(isset($data['dm'])&& count($data['dm'])> 0) {

				foreach($data['dm'] as $k => $v) {

					foreach($v as $v2) {
						$doc->addField($k, $v2);
					}
				}
			}
			if(isset($data['is'])&& count($data['is'])> 0) {

				foreach($data['is'] as $k => $v) {
					$doc->addField($k, $v);
				}
			}
			if(isset($data['im'])&& count($data['im'])> 0) {

				foreach($data['im'] as $k => $v) {

					foreach($v as $v2) {
						$doc->addField($k, $v2);
					}
				}
			}
			if(isset($data['fs'])&& count($data['fs'])> 0) {

				foreach($data['fs'] as $k => $v) {
					$doc->addField($k, $v);
				}
			}
			if(isset($data['fm'])&& count($data['fm'])> 0) {

				foreach($data['fm'] as $k => $v) {

					foreach($v as $v2) {
						$doc->addField($k, $v2);
					}
				}
			}
			if(isset($data['ps'])&& count($data['ps'])> 0) {

				foreach($data['ps'] as $k => $v) {
					$doc->addField($k, $v);
				}
			}
			if(isset($data['pm'])&& count($data['pm'])> 0) {

				foreach($data['pm'] as $k => $v) {

					foreach($v as $v2) {
						$doc->addField($k, $v2);
					}
				}
			}
			if(isset($data['bs'])&& count($data['bs'])> 0) {

				foreach($data['bs'] as $k => $v) {
					$doc->addField($k, $v);
				}
			}
			if(isset($data['bm'])&& count($data['bm'])> 0) {

				foreach($data['bm'] as $k => $v) {

					foreach($v as $v2) {
						$doc->addField($k, $v2);
					}
				}
			}
			if(isset($data['ss'])&& count($data['ss'])> 0) {

				foreach($data['ss'] as $k => $v) {
					$doc->addField($k, $v);
				}
			}
			if(isset($data['sm'])&& count($data['sm'])> 0) {

				foreach($data['sm'] as $k => $v) {

					foreach($v as $v2) {
						$doc->addField($k, $v2);
					}
				}
			}
			if(isset($data['ts'])&& count($data['ts'])> 0) {

				foreach($data['ts'] as $k => $v) {
					$doc->addField($k, $v);
				}
			}
			if(isset($data['tm'])&& count($data['tm'])> 0) {

				foreach($data['tm'] as $k => $v) {

					foreach($v as $v2) {
						$doc->addField($k, $v2);
					}
				}
			}
			//  Zend_Debug::dump($doc); die();
			$updateResponse = $client->addDocument($doc, $overwrite);
			#Zend_Debug::dump($updateResponse); die();
		}
		catch(Exception $e) {
			return array('transaction' => false,
						 'result' => false,
						 'message' =>$e->getMessage());
		}
		return array('transaction' => true,
					 'result' => true,
					 'message' => 'transaction succed');
	}
}
