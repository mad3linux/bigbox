<?php


/*
 * Copyright (C) Prabtech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class Domas_Model_Zprabaqueue extends Zend_Db_Table_Abstract {

    public function get_a_worker_by_name($name) {
        //die("select * from zprabaf_data_$bundle  where entity_id= $cid");
        try {
            $item = $this->_db->fetchRow("select * from zpraba_workers where machine_name= ?", $name);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
        return $item;
    }

    public function get_workers() {
        //die("select * from zprabaf_data_$bundle  where entity_id= $cid");
        try {
            $item = $this->_db->fetchAll("select * from zpraba_workers");
        }
        catch(Exception $e) {
            return array();
        }

        foreach($item as $v) {
            $new[$v['machine_name']] = $v;
        }
        return $new;
    }

    public function add_action($data) {
        $authAdapter = Zend_Auth::getInstance();
        $identity = $authAdapter->getIdentity();
        $folder = APPLICATION_PATH . '/../public/binprabaqueue/';
        $logs = APPLICATION_PATH . '/../public/logs/';
        try {
            $qname = strtolower($data['qname']);
            $qname = str_replace(" ", "", $qname);
            if($data['interval'] == "") {
                $interval = " ";
                $data['interval'] = 5;
            } else {
                $interval = " INTERVAL=" . $data['interval'] . " ";
            }
            if($data['qcount'] == "") {
                $qcount = " ";
                $data['qcount'] = 1;
            } else {
                $qcount = " COUNT=" . $data['qcount'] . " ";
            }
            $str = '#!/bin/bash
					export PATH=$PATH
					DATE=`date -d "now" +%Y%m%d`
					QUEUE=default VVERBOSE=1 ' . $interval . ' ' . $qcount . ' /usr/bin/php ' . APPLICATION_PATH . '/../bin/cli.php rescue run > ' . $logs . '' . $qname . '_$DATE.log 2>&1';
            $file = fopen($folder . 
                          '' . 
                          $qname . 
                          '.sh', "w")or die("Unable to open file!");
            #file_put_contents($file, $str);
            fwrite($file, $str);
            fclose($file);
            #nohup bash default.sh > rescue.log 2>&1 &
            shell_exec("chmod +x " . 
                       $folder . 
                       '' . 
                       $qname . 
                       '.sh');
            #die('nohup bash  '. $folder.''.$qname.'.sh > '.$logs.'run'.$qname.'.log 2>&1 &');
            #die('nohup bash  '. $folder.''.$qname.'.sh > '.$logs.'run'.$qname.'.log 2>&1 &');
            shell_exec('nohup bash  ' . 
                       $folder . 
                       '' . 
                       $qname . 
                       '.sh > ' . 
                       $logs . 
                       'run' . 
                       $qname . 
                       '.log 2>&1 &');
            #echo shell_exec('echo $!'); die("zzzz");
            sleep("10");
            $pidarray = $this->get_running_pid();
            $npid = array_slice($pidarray, 0, $data['qcount']);
            $pid = implode(",", $npid);
            $this->_db->query("insert into zpraba_workers (machine_name, que_interval,  sum_workers, array_pid, created_by, status, created_date) values (?, ?, ?, ?, ?, ?, now() )", array($qname, $data['interval'], $data['qcount'], $pid, $identity->uid, 1));
            $result = array('result' => true,
                            'id' =>$id,
                            'message' => 'Action has been created.');
        }
        catch(Exception $e) {
            $result = array('result' => false,
                            'message' =>$e->getMessage());
            Zend_Debug::dump($e->getMessage());
            die($sql);
        }
        // Zend_Debug::dump($result); die();
        return $result;
    }

    function stop_running_pid($id) {
        $cdata = $this->get_a_worker_by_name($id);
        $authAdapter = Zend_Auth::getInstance();
        $identity = $authAdapter->getIdentity();
        $folder = APPLICATION_PATH . '/../public/binprabaqueue/';
        $logs = APPLICATION_PATH . '/../public/logs/';
        try {
            if($cdata) {
                $qname = $id;
                $allpid = explode(",", $cdata['array_pid']);
                #Zend_Debug::dump($allpid); die();
                if(count($allpid)> 0) {

                    foreach($allpid as $zc) {
                        shell_exec('kill -9  ' . 
                                   $zc . 
                                   '');
                        sleep("5");
                    }
                }
                $this->_db->query("update zpraba_workers set status =0 where machine_name=?", array($qname));
                $result = array('result' => true,
                                'message' => 'Workers has been activated.');
            }
        }
        catch(Exception $e) {
            $result = array('result' => false,
                            'message' =>$e);
        }
        return $result;
    }

    function delete_running_pid($id) {
        $authAdapter = Zend_Auth::getInstance();
        $identity = $authAdapter->getIdentity();
        $folder = APPLICATION_PATH . '/../public/binprabaqueue/';
        $logs = APPLICATION_PATH . '/../public/logs/';
        try {
            $this->stop_running_pid($id);
            $qname = $id;
            unlink($folder . 
                   '' . 
                   $qname . 
                   '.sh');
            unlink($logs . 
                   'run' . 
                   $qname . 
                   '.log');
            $this->_db->query("delete from zpraba_workers  where machine_name=?", array($qname));
            #die("ok");
            $result = array('result' => true,
                            'message' => 'Workers has been activated.');
        }
        catch(Exception $e) {
            $result = array('result' => false,
                            'message' =>$e);
        }
        return $result;
    }

    function set_running_pid($id) {
        $cdata = $this->get_a_worker_by_name($id);
        //Zend_Debug::dump($cdata); die();
        $authAdapter = Zend_Auth::getInstance();
        $identity = $authAdapter->getIdentity();
        $folder = APPLICATION_PATH . '/../public/binprabaqueue/';
        $logs = APPLICATION_PATH . '/../public/logs/';
        try {
            if($cdata) {
                $qname = $id;
                //die($folder.''.$qname.'.sh');
                shell_exec('nohup bash  ' . 
                           $folder . 
                           '' . 
                           $qname . 
                           '.sh > ' . 
                           $logs . 
                           'run' . 
                           $qname . 
                           '.log 2>&1 &');
                //die('nohup bash  '. $folder.''.$qname.'.sh > '.$logs.'run'.$qname.'.log 2>&1 &');		
                sleep("2");
                $pidarray = $this->get_running_pid();
                //Zend_Debug::dump($pidarray);die();
                $npid = array_slice($pidarray, 0, $cdata['sum_workers']);
                #Zend_Debug::dump($npid);
                $pid = implode(",", $npid);
                $this->_db->query("update zpraba_workers set array_pid= ?,  status =1 where machine_name=?", array($pid, $qname));
                #die("ok");
                $result = array('result' => true,
                                'message' => 'Workers has been activated.');
            }
        }
        catch(Exception $e) {
            $result = array('result' => false,
                            'message' =>$e);
        }
        return $result;
    }

    function get_running_pid() {
        $content = shell_exec(" ps -ef | grep rescue | grep -v grep | awk '{print $2}'");
        #$content = str_replace("\n", "", $content); 
        #$content = str_replace("\r", "", $content);
        $data =(explode("\n", $content));
        arsort($data);

        foreach($data as $v) {
            if($v != "") {
                $new[] = $v;
            }
        }
        //Zend_Debug::dump($new); die();
        return $new;
    }

    function get_running_temp() {
        $files = glob(APPLICATION_PATH . 
                      '/../public/binprabaqueue/*.sh');
        #Zend_Debug::dump($files); die();

        foreach($files as $k => $v) {
            $sar = explode("/", $v);
            $sh[$k]['NAME'] = substr(end($sar), 0, - 3);
            $sh[$k]['LINK'] = $v;
            $content = file_get_contents($v);
            $zz = explode("INTERVAL=", $content);
            $yy = explode("COUNT=", $content);
            if(count($zz)> 1) {
                $sh[$k]['INTERVAL'] = current(explode(' ', $zz[1]));
            } else {
                $sh[$k]['INTERVAL'] = "5";
            }
            if(count($yy)> 1) {
                $sh[$k]['COUNT'] = current(explode(' ', $yy[1]));
            } else {
                $sh[$k]['COUNT'] = "1";
            }
        }
        return $sh;
        #Zend_Debug::dump($sh); die();
    }
}
