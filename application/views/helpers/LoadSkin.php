<?php
class Zend_View_Helper_LoadSkin extends Zend_View_Helper_Abstract
{
    public function loadSkin ($skin, $module=null)
    {
		$public =constant('APPLICATION_PATH') . '/../public';
		if($module) {
			$skinData = new Zend_Config_Xml($public.'/assets/'.$module.'/skins/' . $skin . '/skin.xml');
			 
		} else {
			$skinData = new Zend_Config_Xml($public.'/assets/core/skins/' . $skin . '/skin.xml');
			
		}
		$stylesheets = $skinData->stylesheets->stylesheet->toArray();
		//Zend_Debug::dump($stylesheets);
		if (is_array($stylesheets)) {
            foreach ($stylesheets as $stylesheet) {
        		if($module) {
					$this->view->headLink()->prependStylesheet('/assets/'.$module.'/skins/' . $skin . '/css/' . $stylesheet);
				} else {
					$this->view->headLink()->prependStylesheet('/assets/core/skins/' . $skin . '/css/' . $stylesheet);	
				}
            
            }
        }
    }
}

