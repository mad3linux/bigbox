<?php

/**
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>,Jul 15, 2014
 *
 */
require_once(APPLICATION_PATH). '/../library/CMS/Workflowmetadata/wftaskinterface.php';

class Domas_ActionController extends Zend_Controller_Action {

public function runningdescAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
		
		//Zend_Debug::dump($params); die();
		
		$c= new Model_Zprabawf();
		$data = $c->get_process_by_pid($params['id']);
		$var = unserialize($data['zparams_serialize']);

	//	Zend_Debug::dump($var); die();
		/*	
        $m = new Model_Prabasystem();
        $inx = $m->get_a_action($params['id']);
        $inar = array();
        if($inx['input_array'] != "") {
            $inar = unserialize($inx['input_array']);
        }*/
        //  Zend_Debug::dump($inar); die();
        $out = '
<div id="basic-modal-content" class="simplemodal-data" style="display: block;">
    <h3>Test Running Process</h3>
   
     
    <div id="load">'.$var['OperatorData']['desfunc'].'</div>
    <div id="debug"></div></div>';
        $records = array('out' => array('html' =>$out));
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }
public function getattrAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($params);die();    
        $m = new Model_Action();
        $data = $m->get_attrs_by_id($params['id']);
        $result = array('retCode' => '00',
                        'retMsg' => 'success',
                        'result' => true,
                        'data' =>$data);
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function runningbgAction() {
        $params = $this->getRequest()->getParams();
        $id = $params['id'];
        $path = APPLICATION_PATH . '/../public/bincron/';
        if($id != '') {
            try {
                
                /*
                 $params = array('input' =>$input,
                 'zzzid' =>$id);
                 $jobId = Resque::enqueue('default', 'Model_Executequeueaction', $params, true);
                 echo "Jobid  : " . $jobId;
                 die();
                 
                 */
                $id = strtolower($id);
                $id = str_replace(" ", "", $id);
                //$sh = fopen($path."log/".$id.".log", "w") or die("Unable to open file!");
                $sh = fopen($path . 
                            "log/" . 
                            $id . 
                            ".log", "w");
                fclose($sh);
                shell_exec("chmod 775 " . 
                           $path . 
                           "log/" . 
                           $id . 
                           ".log");
                //$sh = fopen($path."sh/".$id.".sh", "w") or die("Unable to open file");
                $sh = fopen($path . 
                            "sh/" . 
                            $id . 
                            ".sh", "w");
                $txt = "#!/bin/sh" . "\n" . "\n" . '/usr/bin/php  ' . APPLICATION_PATH . '/../bin/cli.php cron exec id=' . $id . '  > ' . $path . 'log/' . $id . '.log';
                fwrite($sh, $txt);
                fclose($sh);
                shell_exec("chmod +x " . 
                           $path . 
                           "sh/" . 
                           $id . 
                           ".sh");
                //$cron = fopen($path."prabacron","a")or die("Unable to open file!");
                //die("crontab ".$path."prabacron");
                $res = shell_exec("at -f " . 
                                  $path . 
                                  "sh/" . 
                                  $id . 
                                  ".sh now");
                echo "running background";
                die();
            }
            catch(Exception $e) {
                Zend_Debug::dump($e->getMessage());
                die();
            }
        }
    }

    public function runningallAction() {
        $params = $this->getRequest()->getParams();
        $i = 0;

        foreach($params['awal'] as $v) {
            $input[$v] = $params['akhir'][$i];
            $i ++;
        }
        // $input['indexurl']='http://news.detik.com/indeks';
        // $input['medianame']='detik';
        $new = new Model_Zprabawf();
        $out = $new->get_wf_process($params['id'], $input, true, $params['debug']);
        echo "<div style='font-size:10px;color:red;'>";
        echo "<div style='float:left;width:50%'>";
        echo "<span style='margin-left:5px;font-weight:bold'>INPUT</span>";
        Zend_Debug::dump($input);
        echo "</div>";
        echo "<div style='float:right;width:50%'>";
        echo "<span style='margin-left:5px;font-weight:bold'>OUTPUT</span>";
        Zend_Debug::dump($out);
        echo "</div>";
        echo "</div>";
        die();
    }

    public function editactionAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        // <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        //  Zend_Debug::dump($m->get_a_action($params['id']));die();
        $this->view->data = $m->get_a_action($params['id']);
    }

    public function addactionAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        // <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        // Zend_Debug::dump($m2->list_page());die();
        if(!isset($params['row'])) {
            $params['row'] = 1;
        }
        $this->view->lay = $m->get_layout();
    }

    public function saveintanceAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($params);die();    
        $m = new Prabadata_Model_General();
        $update = $m->save_instance($params);
        $records = array('out' => array('html' =>$update));
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }

    public function loadinstanceidAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        $m = new Prabadata_Model_General();
        $var = $m->get_a_action_instance($params['id']);
        $nvar = unserialize($var['input_array']);
        // Zend_Debug::dump($params);die();
        $html = '<thead>
        <tr><td>Array Key</td><td>Array Value</td></tr>
    </thead><tbody>';

        foreach($nvar as $k => $v) {
            $html .= '<tr><td><input class="form-control" value="' . $k . '" name="awal[]"></td><td><input class="form-control" name="akhir[]" value="' . $v . '"></td></tr>';
        }
        $html .= '</tbody>';
        echo $html;
        die();
    }

    public function loadinstanceAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        $m = new Prabadata_Model_General();
        $update = $m->load_instance($params);
        $html = "
    <script>
       function load_instance_id (vax) {

        $.ajax({
            url: '/domas/action/loadinstanceid/id/'+vax,
            type:'GET',
            dataType: 'html',   
            beforeSend: function() {

            },
            success: function(data) {
             $('#tutorial').html(data);

         },
         timeout:9000,
         error:function(){

         },
     });
 }
</script><br><br>
<div class='table-responsive'>
    <table class='table table-bordered table-hover table-advance'>
       <tr><th>Instance Name</th><th>Input</th><th>Action</th></tr>   
       ";
        if(count($update['result'])> 0) {

            foreach($update['result'] as $z) {
                $nar = unserialize($z['input_array']);
                $nnnar = "(";

                foreach($nar as $k => $v) {
                    $nnnar .= $k . "=>" . $v . ",";
                }
                $nnnar .= ")";
                $html .= "<tr><td>" . $z['instance_name'] . "</td>";
                $html .= "<td>" . $nnnar . "</td><td><input data-id='" . $z['id'] . "' id =  class='grey' type='button' onclick='load_instance_id(" . $z['id'] . ");' value='load' /></td></tr>";
            }
        }
        $html .= "</table></div>";
        echo $html;
        die();
    }

    public function runningAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($params);die();
        $m = new Model_Prabasystem();
        $inx = $m->get_a_action($params['id']);
        $inar = array();
        if($inx['input_array'] != "") {
            $inar = unserialize($inx['input_array']);
        }
        //  Zend_Debug::dump($inar); die();
        $out = '<script>

    function load_instance () {
                
        $("#loadins").hide();

        $.ajax({
            url: "/domas/action/loadinstance/id/' . $params['id'] . '",
            type:"GET",
            dataType: "html",   
            beforeSend: function() {

            },
            success: function(data) {
                            //$("#simplemodal-container").css("height", "500px");
                $("#load").html(data);

            },
            timeout:9000,
            error:function(){

            },
        });
    }
    function save_instance () {
        $("#ins_name").show();

        var $Ins =  $("#savins").val();   
        if($Ins == "Save New Instance") {
            $.ajax({
                url: "/domas/action/saveintance/id/' . $params['id'] . '",
                type:"POST",
                data: $("#running").serialize(),
                dataType: "html",   
                beforeSend: function() {

                },
                success: function(data) {
                            //$("#simplemodal-container").css("height", "500px");
                            //$("#debug").html(data);

                },
                timeout:9000,
                error:function(){

                },
            });

        } else {
            $("#savins").val("Save New Instance");

        }

    }

    function running_submit() {
        enable_ajax_indicator();
     $.ajax({
        url: "/domas/action/runningstep/id/' . $params['id'] . '/rid/' . mktime(). '",
        type:"POST",
        data: $("#running").serialize(),
        dataType: "html",	
        beforeSend: function() {

        },
        success: function(data) {
            $("#simplemodal-container").css("height", "500px");
            $("#debug").html(data);
             disable_ajax_indicator();    
        },
        timeout:9000,
        error:function(){

        },
    });


}
function running_submitall() {
      enable_ajax_indicator();
 $.ajax({
    url: "/domas/action/runningall/id/' . $params['id'] . '/rid/' . mktime(). '",
    type:"POST",
    data: $("#running").serialize(),
    dataType: "html",	
    beforeSend: function() {

    },
    success: function(data) {
        $("#simplemodal-container").css("height", "500px");

        $("#debug").html(data);
         disable_ajax_indicator();    
    },
    timeout:9000,
    error:function(){

    },
});


}


function running_bg() {
      enable_ajax_indicator();
 $.ajax({
    url: "/domas/action/runningbg/id/' . $params['id'] . '/rid/' . mktime(). '",
    type:"POST",
    data: $("#running").serialize(),
    dataType: "html",	
    beforeSend: function() {

    },
    success: function(data) {
        $("#simplemodal-container").css("height", "500px");

        $("#debug").html(data);
         disable_ajax_indicator();    
    },
    timeout:9000,
    error:function(){

    },
});

}
function running_debug() {
 $.ajax({
    url: "/domas/action/runningall/id/' . $params['id'] . '/rid/' . mktime(). '/debug/1",
    type:"POST",
    data: $("#running").serialize(),
    dataType: "html",	
    beforeSend: function() {

    },
    success: function(data) {
        $("#simplemodal-container").css("height", "500px");

        $("#debug").html(data);
    },
    timeout:9000,
    error:function(){

    },
});


}
';
        $out .= ' function comp0(me) {
  var val = $(me).val();
		//alert(val);
  if(val=="map") {
      $( "#mapping" ).show();
  } else {
      $( "#mapping" ).hide();
  }

}
function addPIC(me) {
  $("#tutorial tbody").append("<tr><td><input class=\'form-control\' name=awal[]></td><td><input class=\'form-control\' name=akhir[]></td></tr>");

}


</script>
<div id="basic-modal-content" class="simplemodal-data" style="display: block;">
    <h3>Test Running Process</h3>
    <form id="running" method="post" ">
        <input type="hidden" name="task_class" value="' . $task_class . '">
        <input type="hidden" name="template_data_id" value="' . $id . '">
        <table width="100%" class="table table-hover">
          <tbody>
           <tr id="mapping">
            <td style="vertical-align: top;">Input</td>
            <td style="width: 70%;">

                <table  id="tutorial" style="border: 1px solid green;" width= 100% >
                    <thead>
                        <tr><td>Array Key</td><td>Array Value</td></tr>
                    </thead>
                    <tbody>';
        for($i = 0;
        $i < count ($inar );
        $i ++ ) {
            $out .= '<tr><td><input class="form-control" value="' . trim($inar[$i]). '" name=awal[]></td><td><input class="form-control"  name=akhir[]></td></tr>';
        }
        $out .= '</tbody>
                    </table>
                    <button type="button" class="btn grey" onclick="addPIC(); return false;" data-act="addPIC" data-id="1" style="padding: 5px;">Add</button>
                </td>
            </tr>
        </tbody></table>';
        $out .= '<!--<input  class="btn grey" type="button" onclick="running_submit();" value="Debug per Step">&nbsp;&nbsp;--><input  id = "savins" class="btn grey" type="button" onclick="save_instance();" value="Save Instance">&nbsp;&nbsp;<input id = "loadins"   class="btn grey" type="button" onclick="load_instance();" value="Load Instance">&nbsp;&nbsp;<input class="btn blue" class="form-submit" type="button" onclick="running_submitall();" value="Running">&nbsp;&nbsp;<input class="btn green"  type="button" onclick="running_debug();"    value="Running w Debug"><br><br><input class="btn red"  type="button" onclick="running_bg();"    value="Running background">
        <div id="ins_name" style="display:none;" placeholder="Instance Name"><label>Instance Name :</label><input class="form-control"  name=ins_name /></div>
    </form>
    <div id="load"></div>
    <div id="debug"></div>';
        $records = array('out' => array('html' =>$out));
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }

    public function ajaxer4Action() {
        $zz = new CMS_General();
        $params = $this->getRequest()->getParams();
        $id = $params['type'] . "_getgroups_" . $params['id'];
        $front = array('lifetime' => NULL,
                       'automatic_serialization' => true);
        $back = array('cache_dir' => APPLICATION_PATH . '/../cache/');
        $ncache = Zend_Cache::factory('Core', 'File', $front, $back);
        try {
            $vdata = $ncache->load($id);
        }
        catch(Exception $e) {
        }
        $cc = new CMS_General();
        if(!is_array($vdata)) {
            $odata = $cc->objectToArray($vdata->result);
        } else {
            $odata = $vdata;
        }
        switch($params['type']) {

            case 'telegram' : foreach($odata as $v) {
                $data[$v] = $v;
            }
            break;

            case 'wa' : foreach($odata as $v) {
                $data[$v['id']] = $v['subject'];
            }
            break;
        }
        $group = $zz->__unserialize($params['group']);

        foreach($data as $k => $d) {
            $sel = "";
            echo "<input ";
            if(in_array($k, $group)) {
                $sel = "checked";
            }
            echo "type=checkbox " . $sel . " name=group[] value='" . $d . "' />" . $d . "<br>";
        }
        echo "<br>";
        die();
    }

    public function ajaxer3Action() {
        $id = $this->getRequest()->getParam('id');
        $acc = $this->getRequest()->getParam('acc');
        $cc = new Model_Zprahu();
        switch($id) {
            case 'tl' : case 'telegram' : $data = $cc->get_account('telegram');
            break;
            case 'wa' : $data = $cc->get_account('wa');
            break;
            case 'email' : $data = $cc->get_account('email');
            break;
            case 'gtalk' : $data = $cc->get_account('gtalk');
            break;
        }
        echo '<select onchange="comp1(this)" name="account" id="account">
    <option value="">--choose api--</option>';

        foreach($data as $k => $d) {
            $sel = "";
            if($acc == $k) {
                $sel = "selected";
            }
            echo '<option ' . $sel . ' value="' . $k . '">' . $d . '</option>';
        }
        echo '</select>';
        die();
    }

    public function getcustom2Action() {
        $params = $this->getRequest()->getParams();
        $this->_helper->layout->disableLayout();
        $mm = new Model_Prabasystem();
        $dat = $mm->get_method_class_model_operator($params['id']);
        $out = "<option></option>";

        foreach($dat as $k => $v) {
            $out .= "<option value='" . $v . "'>" . $v . "</option>";
        }
        echo $out;
        die();
    }

    public function getcustom3Action() {
        $params = $this->getRequest()->getParams();
        $this->_helper->layout->disableLayout();
        $zz2 = new Zend_Reflection_Method($params['xid2'], $params['xid']);
        $ket = "";
        try {
            $rdoce = $zz2->getDocblock();
            $ket = $rdoce->getShortDescription();
        }
        catch(Exception $e) {
            $ket = "";
            // Zend_Debug::dump($e->getMessage()); 
        }
        //Zend_Debug::dump($params);die();
        echo $ket;
        die();
    }

    public function getcustomAction() {
        $this->_helper->layout->disableLayout();
        $mm = new Model_Prabasystem();
        $dat = $mm->get_models_class_operator();
        $params = $this->getRequest()->getParams();
        $out = '<td style="vertical-align: top;">Class/Model</td><td>
    <select  class="form-control" onchange=changeModel(this)  name="model" id="model"  >
     <option></option>';

        foreach($dat as $k => $v) {
            $out .= "<optgroup label='" . $k . "'>";

            foreach($v as $vv) {
                $sel = "";
                if($params['xid'] == $vv) {
                    $sel = "selected";
                }
                $out .= "<option $sel value='" . $vv . "'>" . $vv . "</option>";
            }
            $out .= "</optgroup>";
        }
        $out .= '</select><br><select  class="form-control" onchange=changeMethod(this) name="method" id="method"  >';
        if(isset($params['xid'])&& $params['xid'] != "") {
            $mm = new Model_Prabasystem();
            $dat = $mm->get_method_class_model_operator($params['xid']);
            $out .= "<option></option>";

            foreach($dat as $k => $v) {
                $sel = "";
                if($params['xid2'] == $v) {
                    $sel = "selected";
                }
                $out .= "<option $sel value='" . $v . "'>" . $v . "</option>";
            }
        }
        $out .= '</select>
        </td>';
        echo $out;
        die();
    }

    public function ajaxer2Action() {
        $id = $this->getRequest()->getParam('id');
        $cc = new Model_Zprabawf();
        $dat = $cc->get_api_by_conn_id($id);
        $this->_helper->layout->disableLayout();
        echo '<select  class="form-control"  name="api" id="api">
    <option value="">--choose api--</option>';

        foreach($dat as $d) {
            echo '<option value="' . $d['id'] . '">' . $d['id'] . '-[' . substr($d['sql_text'], 0, 30). ']</option>';
        }
        echo '</select>';
        die();
    }

    public function ajaxerAction() {
        $this->_helper->layout->disableLayout();
        $ctype = $this->getRequest()->getParam('ctype');
        $taskid = $this->getRequest()->getParam('taskid');
        $tempid = $this->getRequest()->getParam('tempid');
        $action = $this->getRequest()->getParam('act');
        //die($ctype);
        //Zend_Debug::dump($this->getRequest()->getParam('ltype')); die();
        if($ctype == "PrabaOperator") {
            $ti = new $ctype($taskid, $tempid, $this->getRequest()->getParam('ltype'));
        } else {
            $ti = new $ctype($taskid, $tempid);
        }
        $out = $ti->$action();
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode(array("out" => $out));
        die();
    }

    public function showfunctionAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        $cc = new Prabadata_Model_General();
        $data = $cc->get_description($params['id']);
        if($data) {
            echo "<div>" . $data . "</div>";
        }
        die();
    }

    public function listAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $sys = new Model_Zprabapage();
        $mdl = new Model_Prabasystem();
        $params = $sys->list_action();
        // Zend_Debug::dump($params);die();
        //$apps = $mdl->get_apps ();
        $this->view->params = $data;
        //$this->view->apps = $apps;
    }

    public function displayAction() {
        $params = $this->getRequest()->getParams();
        $this->view->varams = $params;
        //$this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery.numeric.js' );
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/prabawf/css/metadata.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/prabawf/metadata/css/basic.css');
        $params = $this->getRequest()->getParams();
        //Zend_Debug::dump($params); die();
        $datatemp = null;
        $task = array();
        $template_id = $this->getRequest()->getParam('id');
        $cc = new Model_Prabasystem();
        $this->view->act = $cc->get_a_action($template_id);
        $c = new CMS_Workflowmetadata_wfinterface($template_id);
        $var = $c->displayPage();
        //Zend_Debug::dump($var); die();
        $this->view->temp = $datatemp;
        $this->view->cwf = $var;
        $c3 = new Model_Zprabawf();
        $res = $c3->get_process($template_id);

        foreach($res as $rec) {
            // Zend_Debug::dump($rec);
            $task_type = $rec['class_type'];
            $task_class = 'Praba' . $task_type;
            #echo $task_class; 
            if(class_exists($task_class)) {
                $ti = new $task_class($rec['pid']);
            } else {
                //unknown classe
            }
            $restask = $c3->get_process_by_pid($rec['pid']);
            $task[$rec['pid']]['api'] = array();
            if($rec['class_type'] == 'Task') {
                if($rec['p_type'] == 'action') {
                    $task[$rec['pid']]['api'] = $c3->get_action_by_id_with_conn($rec['ext_id']);
                } else {
                    $task[$rec['pid']]['api'] = $c3->get_api_by_id_with_conn($rec['ext_id']);
                }
            }
            $task_class = 'Praba' . $restask['class_type'];
            $task[$rec['pid']]['res'] = $restask;
            $task[$rec['pid']]['ti'] = $ti;
            $task[$rec['pid']]['task_class'] = $task_class;
        }
        // die("ss");
        $this->view->task = $task;
        $this->view->temp = $template_id;
        $this->view->menu = $c->getContextMenu();
    }
}
