<?php

/**
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>,Jul 15, 2014
 *
 */
// ini_set("display_errors", "On");

class Domas_AjaxhomeController extends Zend_Controller_Action {

    public function init() {
        
        /* Initialize action controller here */
    }

    public function highlight($text, $words) {
        // Zend_Debug::dump($words);die();
        preg_match_all('~\w+~', $words, $m);
        if(!$m)
            return $text;
        // Zend_Debug::dump($m[0]);
        $re = '~\\b(' . implode('|', $m[0]). ')\\b~i';
        return preg_replace($re, '<span class="label label-info">$0</span>', $text);
        
        /*
         $wordsArray = array();
         $markedWords = array();
         // explode the phrase in words
         $wordsArray = explode(' ', $words);
         
         foreach ($wordsArray as $k => $word) {
         $markedWords[$k]='<mark>'.$word.'</mark>';
         }
         
         $text = str_ireplace($wordsArray, $markedWords, $text);
         
         //right trows results
         return $text;*/
    }

    public function testAction() {
        // $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);//die();
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2-bootstrap.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.full.min.js');
        $this->view->headScript()->appendFile('/assets/core/pages/scripts/components-select2.min.js');
    }

    public function wordcloudAction() {
    }

    public function trendlineAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);//die();
        $g = new Pmas_Model_Pdash();
        $ff = new Pmas_Model_Withsolr();
        $str = "*:*";
        //-============= start fq content
        $skey = null;
        $arr_pencarian = array();
        $pencarian = "";
        if($params["skey"] != "") {
            $arr_pencarian = explode(",", $params["skey"]);
        }
        if($params["cate"] != "") {
            $params["cat"] = $params["cate"];
        }
        $tmp_tp = $g->get_topik_by_tema($params["cat"], $params["topik"]);
        // Zend_Debug::dump($tp);die();
        $tp = array();

        foreach($tmp_tp as $k => $v) {
            $st = $g->get_subtema_by_tema($v["tid"]);

            foreach($st as $k1 => $v1) {
                array_push($tp, $v1);
            }
        }
        // Zend_Debug::dump($tp);die();
        $arr_topik = array();

        foreach($tp as $k => $v) {
            array_push($arr_topik, trim(strtolower($v["name"])));
        }
        // Zend_Debug::dump($arr_topik);die();
        if(count(array_unique($arr_topik))> 500) {
            array_splice($arr_topik, 320);
        }
        $skey = null;
        if(count($arr_pencarian)> 0 && count(array_unique($arr_topik))> 0) {
            $skey = '&fq=content:("' . urlencode(implode('" AND "', $arr_pencarian)). '")' . urlencode(' AND '). '("' . urlencode(implode('" OR "', array_unique($arr_topik))). '")';
        } else if(count($arr_pencarian)> 0 && count(array_unique($arr_topik))== 0) {
            $skey = '&fq=content:("' . urlencode(implode('" AND "', $arr_pencarian)). '")';
        } else if(count($arr_pencarian)== 0 && count(array_unique($arr_topik))> 0) {
            $skey = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_topik))). '")';
        }
        //-============= end fq content
        //-============= start fq content_date range
        $d1 = date("Y-m-d\T00:00:00\Z", strtotime('monday last week'));
        $d2 = "NOW";
        if(isset($params["d1"])&& $params["d1"] != "" && isset($params["d2"])&& $params["d2"] != "") {
            $d1 = gmdate("Y-m-d\TH:i:s\Z", strtotime($params["d1"]));
            $d2 = gmdate("Y-m-d\TH:i:s\Z", strtotime($params["d2"]));
            $d1 = date("Y-m-d\T00:00:00\Z", strtotime($params['d1'] . " -1 days"));
            $d2 = date("Y-m-d\T23:59:59\Z", strtotime($params['d2'] . " +1 days"));
            // Zend_Debug::dump($d2);die();
        }
        // $filter .="&content_date:[".$d1.$d2."]";
        $cdd = "&facet.range.start=" . urlencode($d1). "&facet.range.end=" . urlencode($d2);
        //-============= end fq content_date range
        //-============= start fq sentiment
        $sen = null;
        if(isset($params["is_sentiment"])&& $params["is_sentiment"] != "") {
            $sen = "&fq=is_sentiment:0";
            if($params["is_sentiment"] == "negatif") {
                $sen = "&fq=is_sentiment:1";
            } else if($params["is_sentiment"] == "positif") {
                $sen = "&fq=is_sentiment:2";
            }
        }
        //-============= end fq sentiment
        //-============= start fq media_type
        $mt = null;
        if(isset($params["is_mediatype"])&& $params["is_mediatype"] != "") {
            if($params["is_mediatype"] == "nasional") {
                $mt = "&fq=is_mediatype:(" . urlencode("1 OR 2"). ")";
            } else {
                $mt = "&fq=is_mediatype:3";
            }
        }
        //-============= end fq media_type
        $fq1 = $skey . $cdd . $sen;
        $fq = $skey . $cdd . $sen . $mt;
        $news =($ff->get_by_rawurl("select?q=" . urlencode($str). "&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.gap=%2B1DAY&fq=entity_id%3A+2" . $fq));
        // Zend_Debug::dump($news);die();
        $socmed =($ff->get_by_rawurl("select?q=" . urlencode($str). "&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.gap=%2B1DAY&fq=entity_id%3A+5" . $fq1 . ""));
        // Zend_Debug::dump($socmed);die();
        $laporan =($ff->get_by_rawurl("select?q=" . urlencode($str). "&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.gap=%2B1DAY&fq=entity_id%3A+3" . $fq1));
        // Zend_Debug::dump($laporan);die();
        $cat = array();
        $tmpnews = array();
        $tmpnews["name"] = "News";
        $i = 0;
        $ii = 0;

        foreach($news["facet_counts"]["facet_ranges"]["content_date"]["counts"] as $k => $v) {
            if($k % 2 == 0) {
                $xx = strtotime(date("Y-m-d H:i:s", strtotime($v)));
            }
            if($k % 2 == 1) {
                // $tmpnews["data"][] = $v;
                $tmpnews["data"][$ii] = array((int)$xx* 1000,
                                              (int)$v);
                $ii ++;
            }
        }
        $tmplaporan = array();
        $tmplaporan["name"] = "Laporan";
        $ii = 0;

        foreach($laporan["facet_counts"]["facet_ranges"]["content_date"]["counts"] as $k => $v) {
            if($k % 2 == 0) {
                $xx = strtotime(date("Y-m-d H:i:s", strtotime($v)));
            }
            if($k % 2 == 1) {
                // $tmplaporan["data"][] = $v;
                $tmplaporan["data"][$ii] = array((int)$xx* 1000,
                                                 (int)$v);
                $ii ++;
            }
        }
        $tmpsocmed = array();
        $tmpsocmed["name"] = "Social Media";
        $ii = 0;

        foreach($socmed["facet_counts"]["facet_ranges"]["content_date"]["counts"] as $k => $v) {
            if($k % 2 == 0) {
                $yyyy = date("Y", strtotime($v));
                $mm = date("m", strtotime($v));
                $dd = date("d", strtotime($v));
                $xx = strtotime(date("Y-m-d H:i:s", strtotime($v)));
                // echo $xx;die();
            }
            if($k % 2 == 1) {
                // $tmpsocmed["data"][] = $v;
                // $tmplaporan["data"][$ii] = array("Date.UTC(".(int)$yyyy.",".$mm.",".$dd.")" , (int)$v);
                $tmpsocmed["data"][$ii] = array((int)$xx* 1000,
                                                (int)$v);
                $ii ++;
            }
        }
        // echo json_encode($tmplaporan);die();
        // Zend_Debug::dump($tmplaporan);die();
        $series = array();
        $series[0] = $tmplaporan;
        $series[1] = $tmpnews;
        $series[2] = $tmpsocmed;
        // Zend_Debug::dump($series);die();
        $this->view->cat = $cat;
        $this->view->series = $series;
    }

    public function latestAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        $n = new Pmas_Model_Solrfactminer();
        $g = new Pmas_Model_Pdash();
        $ff = new Pmas_Model_Withsolr();
        $arr = array("lap" => 3,
                     "news" => 2,
                     "socmed" => 5);
        $x = $arr[$params["l"]];
        $params["x"] = $x;
        //-============= start get stopword
        $tmp_sw = $g->get_stopword($params["t"]);
        // Zend_Debug::dump($tmp_sw);die("s");
        $stopword = array();

        foreach($tmp_sw as $k => $v) {
            // $stopword[$k] = (strlen($v["name"])>1 && substr($v["name"],0,1)=="n"?strtolower(substr($v["name"],1)):strtolower($v["name"]));
            $stopword[$k] = strtolower($v["name"]);
        }
        //-============= end get stopword
        //-============= start fq sentence_tx
        $skey = null;
        $arr_pencarian = array();
        $pencarian = "";
        if($params["skey"] != "") {
            $arr_pencarian = explode(",", $params["skey"]);
        }
        if($params["cate"] != "") {
            $params["cat"] = $params["cate"];
        }
        $tmp_tp = $g->get_topik_by_tema($params["cat"], $params["topik"]);
        // Zend_Debug::dump($tp);die();
        $tp = array();

        foreach($tmp_tp as $k => $v) {
            $st = $g->get_subtema_by_tema($v["tid"]);

            foreach($st as $k1 => $v1) {
                array_push($tp, $v1);
            }
        }
        // Zend_Debug::dump($tp);die();
        $arr_topik = array();

        foreach($tp as $k => $v) {
            array_push($arr_topik, trim(strtolower($v["name"])));
        }
        if(count(array_unique($arr_topik))> 500) {
            array_splice($arr_topik, 320);
        }
        // array_unique($arr_topik);
        // Zend_Debug::dump(array_unique($arr_topik));die();
        $skey = "&fq=content:[*+TO+*]";
        $skey1 = "&fq=doc_tx:[*+TO+*]";
        if(count($arr_pencarian)> 0 && count($arr_topik)> 0) {
            $skey = '&fq=content:("' . urlencode(implode('" AND "', $arr_pencarian)). '")' . urlencode(' AND '). '("' . urlencode(implode('" OR "', array_unique($arr_topik))). '")';
            $skey1 = '&fq=doc_tx:("' . urlencode(implode('" AND "', $arr_pencarian)). '")' . urlencode(' AND '). '("' . urlencode(implode('" OR "', array_unique($arr_topik))). '")';
        } else if(count($arr_pencarian)> 0 && count(array_unique($arr_topik))== 0) {
            $skey = '&fq=content:("' . urlencode(implode('" AND "', $arr_pencarian)). '")';
            $skey1 = '&fq=doc_tx:("' . urlencode(implode('" AND "', $arr_pencarian)). '")';
        } else if(count($arr_pencarian)== 0 && count(array_unique($arr_topik))> 0) {
            $skey = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_topik))). '")';
            $skey1 = '&fq=doc_tx:("' . urlencode(implode('" OR "', array_unique($arr_topik))). '")';
        }
        //-============= end fq sentence_tx
        //-============= start fq date range
        $cdd = null;
        $d1 = date("Y-m-d", strtotime('monday last week'));
        $d2 = date("Y-m-d");
        if(isset($params['d1'])&& $params["d1"] != "" && isset($params['d2'])&& $params["d2"] != "") {
            $d1 = $params['d1'];
            $d2 = $params['d2'];
        }
        $cdd = "&fq=content_date:[" . $d1 . "T00:00:00Z+TO+" . $d2 . "T23:59:59Z]";
        $cdd1 = "&fq=create_date:[" . $d1 . "T00:00:00Z+TO+" . $d2 . "T23:59:59Z]";
        //-============= end fq date range
        //-============= start fq sentiment	$sen=null;
        $sen = null;
        if(isset($params["is_sentiment"])&& $params["is_sentiment"] != "") {
            $sen = "&fq=is_sentiment:0";
            if($params["is_sentiment"] == "negatif") {
                $sen = "&fq=is_sentiment:1";
            } else if($params["is_sentiment"] == "positif") {
                $sen = "&fq=is_sentiment:2";
            }
        }
        //-============= end fq sentiment
        //-============= start fq mediatype
        $mt = null;
        if(isset($params["is_mediatype"])&& $params["is_mediatype"] != "") {
            if($params["l"] == "news") {
                if($params["is_mediatype"] == "nasional") {
                    $mt = "&fq=is_mediatype:(1+OR+2)";
                } else {
                    $mt = "&fq=is_mediatype:3";
                }
            }
        }
        //-============= end fq mediatype
        $fq = $skey . $cdd . $sen . $mt;
        $fq1 = $skey1 . $cdd1;
        // die($fq1);
        if($params["l"] == "lap") {
            $data = $n->get_by_rawurl("select?q=*%3A*&sort=create_date+desc&wt=json&indent=true&fq=type:doc&fq=source:lapin" . 
                                      $fq1);
        } else {
            // die($fqSS);
            $data = $ff->get_by_rawurl("select?q=*%3A*&sort=content_date+desc&wt=json&indent=true&fq=entity_id:" . 
                                       $x . 
                                       $fq);
        }
        // Zend_Debug::dump($data);die();
        $this->view->params = $params;
        $this->view->data = $data["response"]["docs"];
        $this->view->logo = $g->get_logo();
    }

    public function toptwitterAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);//die();
        if(isset($params["skey"])&& $params["skey"] != "") {
            $params["skey"] = explode(',', $params["skey"]);
            // Zend_Debug::dump($params["skey"]);//die();
            $params["skey"] = implode(" AND ", $params["skey"]);
            // Zend_Debug::dump($params["skey"]);//die();
        }
        $str = "*:*";
        if(isset($params["skey"])&& $params["skey"] != "") {
            $str = $params["skey"];
        }
        // Zend_Debug::dump($params);die();
        $ff = new Pmas_Model_Withsolr();
        // $params["cat"] = "Politik";
        $filter = "";
        if(isset($params["cat"])&& $params["cat"] != "") {
            $cc = new Pmas_Model_Pdash();
            $gettid = $cc->get_keys_pmas($params['cat']);
            $filter = implode(" OR  ", $gettid);
            $filter = "&fq=tid:(" . urlencode($filter). ")";
        }
        $d1 = date("Y-m-d\T00:00:00\Z", strtotime('monday last week'));
        $d2 = "NOW";
        if(isset($params["d1"])&& $params["d1"] != "" && isset($params["d2"])&& $params["d2"] != "") {
            $d1 = date("Y-m-d\T00:00:00\Z", strtotime($params['d1']));
            $d2 = date("Y-m-d\T23:59:59\Z", strtotime($params['d2']));
            // Zend_Debug::dump($d2);die();
        }
        $mt1 = null;
        $mt2 = null;
        if(isset($params["is_mediatype"])&& $params["is_mediatype"] != "" && $params["t"] != "Social Media" && $params["t"] != "Laporan") {
            // $mt2 = "fq=entity_id:2";
            if($params["is_mediatype"] == "nasional") {
                $x = "1 OR 2";
                $mt1 = "&fq=is_mediatype:(" . urlencode($x). ")";
            } else {
                $mt1 = "&fq=is_mediatype:3";
            }
        }
        $arr = array("akun" => "ss_userScreenName",
                     "retweet" => "ss_retweeted_user_screenname",
                     "mention" => "ss_entities_mention_0_screenname");
        $x = $arr[$params["l"]];
        $series = array();
        $topakun =($ff->get_by_rawurl("select?q=" . urlencode($str). "&wt=json&rows=0&facet.field=" . $x . "&facet=true&facet.range.start=" . urlencode($d1). "&facet.range.end=" . urlencode($d2). "&facet.range.gap=%2B1DAY" . $filter . "&fq=entity_id:5"));
        // Zend_Debug::dump($topmention);
        // die();
        $tmpakun = array();
        $ii = 0;

        foreach($topakun["facet_counts"]["facet_fields"][$x] as $k => $v) {
            if($k % 2 == 0) {
                $xx = $v;
            }
            if($k % 2 == 1) {
                // Zend_Debug::dump($v);
                $tmpakun[$ii] = array($xx,
                                      (int)$v);
                $ii ++;
            }
        }
        $this->view->params = $params;
        $this->view->data = $tmpakun;
    }

    public function timelineAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        if($params["skey"] != "") {
            // $params["skey"] = str_replace(',','","',$params["skey"]);
            $params["skey"] = explode(',', $params["skey"]);
            // Zend_Debug::dump($params["skey"]);//die();

            foreach($params["skey"] as $k => $v) {
                $skey .= '"' . $v . '" AND ';
            }
            // Zend_Debug::dump($params["skeys"]);//die();
            $skey = rtrim($skey, " AND ");
            $params["skey"] = urlencode($skey);
            // Zend_Debug::dump($params);die();
        }
        // echo $params["tgl"];
        $this->view->params = $params;
        // die();	
    }

    public function sentimentAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        $g = new Pmas_Model_Pdash();
        // $n = new Pmas_Model_Solrfactminer();
        $ff = new Pmas_Model_Withsolr();
        $str = "*:*";
        //-============= start fq content
        $skey = null;
        $arr_pencarian = array();
        $pencarian = "";
        if($params["skey"] != "") {
            $arr_pencarian = explode(",", $params["skey"]);
        }
        if($params["cate"] != "") {
            $params["cat"] = $params["cate"];
        }
        $tmp_tp = $g->get_topik_by_tema($params["cat"], $params["topik"]);
        // Zend_Debug::dump($tp);die();
        $tp = array();

        foreach($tmp_tp as $k => $v) {
            $st = $g->get_subtema_by_tema($v["tid"]);

            foreach($st as $k1 => $v1) {
                array_push($tp, $v1);
            }
        }
        // Zend_Debug::dump($tp);die();
        $arr_topik = array();

        foreach($tp as $k => $v) {
            array_push($arr_topik, trim(strtolower($v["name"])));
        }
        // Zend_Debug::dump($arr_topik);die();
        if(count(array_unique($arr_topik))> 500) {
            array_splice($arr_topik, 320);
        }
        $skey = null;
        if(count($arr_pencarian)> 0 && count(array_unique($arr_topik))> 0) {
            $skey = '&fq=content:("' . urlencode(implode('" AND "', $arr_pencarian)). '")' . urlencode(' AND '). '("' . urlencode(implode('" OR "', array_unique($arr_topik))). '")';
        } else if(count($arr_pencarian)> 0 && count(array_unique($arr_topik))== 0) {
            $skey = '&fq=content:("' . urlencode(implode('" AND "', $arr_pencarian)). '")';
        } else if(count($arr_pencarian)== 0 && count(array_unique($arr_topik))> 0) {
            $skey = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_topik))). '")';
        }
        //-============= end fq content
        //-============= start facet content_date range
        $d1 = date("Y-m-d\T00:00:00\Z", strtotime('monday last week'));
        $d2 = "NOW";
        if(isset($params["d1"])&& $params["d1"] != "" && isset($params["d2"])&& $params["d2"] != "") {
            $d1 = date("Y-m-d\T00:00:00\Z", strtotime($params['d1']));
            $d2 = date("Y-m-d\T23:59:59\Z", strtotime($params['d2']));
        }
        $facet_ranges = "&facet.range.start=" . urlencode($d1). "&facet.range.end=" . urlencode($d2);
        //-============= end facet content_date range
        //-============= start fq media_type
        $mt = null;
        if(isset($params["is_mediatype"])&& $params["is_mediatype"] != "" && $params["t"] != "Social Media" && $params["t"] != "Laporan") {
            if($params["is_mediatype"] == "nasional") {
                $mt = "&fq=is_mediatype:(" . urlencode("1 OR 2"). ")";
            } else {
                $mt = "&fq=is_mediatype:3";
            }
        }
        //-============= end fq media_type
        //-============= start fq entity_id
        $ent = null;
        if(isset($params["t"])&& $params["t"] != "") {
            $e_id = array("News" => 2,
                          "Social Media" => 5,
                          "Laporan" => 3,
                         );
            $ent = "&fq=entity_id:" . $e_id[$params["t"]];
        }
        //-============= end fq entity_id
        $fq1 = $skey . $facet_ranges . $ent;
        $fq = $skey . $facet_ranges . $mt . $ent;
        // die($fq);
        $series = array();
        // die("select?q=".urlencode($str)."&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.gap=%2B1DAY&fq=is_sentiment%3A+1".$fq."&fq=entity_id:(2+OR+5)");
        // NEGATIF
        $neg_digmed =($ff->get_by_rawurl("select?q=" . urlencode($str). "&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.gap=%2B1DAY&fq=is_sentiment%3A+1" . $fq . "&fq=entity_id:(2+OR+5)"));
        // Zend_Debug::dump($neg_digmed);die();
        $negatif_digmed = $neg_digmed["facet_counts"]["facet_ranges"]["content_date"]["counts"];
        // $neg_ndigmed =($n->get_by_rawurl("select?q=".urlencode($str)."&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.gap=%2B1DAY&fq=is_sentiment%3A+1".$fq1."&fq=entity_id:3"));
        // // Zend_Debug::dump($neg_ndigmed);die();
        // $negatif_ndigmed = $neg_ndigmed["facet_counts"]["facet_ranges"]["content_date"]["counts"];
        $tmp_negatif = array();
        $data = array();
        $i = 0;
        $series[0]["type"] = "column";
        $series[0]["name"] = "negatif";
        $series[0]["color"] = "#E43A45";

        foreach($negatif_digmed as $k => $v) {
            if($k % 2 == 0) {
                $xx = strtotime(date("Y-m-d H:i:s", strtotime($v)));
            }
            if($k % 2 == 1) {
                $data[$i] = array((int)$xx* 1000,
                                  (int)$v+$negatif_ndigmed[$k]);
                $data[$i] = array((int)$xx* 1000,
                                  (int)$v);
                $i ++;
            }
        }
        // Zend_Debug::dump($tmp_negatif);
        $series[0]["data"] = $data;
        // NETRAL
        $net_digmed =($ff->get_by_rawurl("select?q=" . urlencode($str). "&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.gap=%2B1DAY&fq=is_sentiment%3A+0" . $fq . "&fq=entity_id:(2+OR+5)"));
        $netral_digmed = $net_digmed["facet_counts"]["facet_ranges"]["content_date"]["counts"];
        // $net_ndigmed =($n->get_by_rawurl("select?q=".urlencode($str)."&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.gap=%2B1DAY&fq=is_sentiment%3A+0".$fq1."&fq=entity_id:3"));
        // $netral_ndigmed = $net_ndigmed["facet_counts"]["facet_ranges"]["content_date"]["counts"];
        $tmp_netral = array();
        $data = array();
        $i = 0;
        $series[1]["type"] = "column";
        $series[1]["name"] = "netral";
        $series[1]["color"] = "#3598DC";

        foreach($netral_digmed as $k => $v) {
            if($k % 2 == 0) {
                $xx = strtotime(date("Y-m-d H:i:s", strtotime($v)));
            }
            if($k % 2 == 1) {
                $data[$i] = array((int)$xx* 1000,
                                  (int)$v+$netral_ndigmed[$k]);
                $data[$i] = array((int)$xx* 1000,
                                  (int)$v);
                $i ++;
            }
        }
        // Zend_Debug::dump($data);
        $series[1]["data"] = $data;
        // POSITIF
        $pos_digmed =($ff->get_by_rawurl("select?q=" . urlencode($str). "&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.gap=%2B1DAY&fq=is_sentiment%3A+2" . $fq . "&fq=entity_id:(2+OR+5)"));
        $positif_digmed = $pos_digmed["facet_counts"]["facet_ranges"]["content_date"]["counts"];
        // $pos_ndigmed =($n->get_by_rawurl("select?q=".urlencode($str)."&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.gap=%2B1DAY&fq=is_sentiment%3A+2".$fq1."&fq=entity_id:3"));
        // $positif_ndigmed = $pos_ndigmed["facet_counts"]["facet_ranges"]["content_date"]["counts"];
        $tmp_positif = array();
        $data = array();
        $i = 0;
        $series[2]["type"] = "column";
        $series[2]["name"] = "positif";
        $series[2]["color"] = "#26C281";

        foreach($positif_digmed as $k => $v) {
            if($k % 2 == 0) {
                $xx = strtotime(date("Y-m-d H:i:s", strtotime($v)));
            }
            if($k % 2 == 1) {
                // $data[$i] = array((int)$xx*1000 , (int)$v+$positif_ndigmed[$k]);
                $data[$i] = array((int)$xx* 1000,
                                  (int)$v);
                $i ++;
            }
        }
        // Zend_Debug::dump($tmp_negatif);
        $series[2]["data"] = $data;
        // Zend_Debug::dump($series);
        $this->view->params = $params;
        $this->view->vpos = $series;
        // die();
    }

    public function extractAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        $n = new Domas_Model_Withsolrdash();
        $id = 'id:"' . $params['id'] . '"';
        $filter = array($id);
        $tid = array();
        // Zend_Debug::dump($filter);die();
        if($params["entity"] == "lap") {
            // $data = $n->get_news_bin_fact("", 0, 1,3, $tid,$filter);
            // $label = $data[0]["title"];
            // // $content = str_replace('\n','<br/>',$data[0]["doc_tx"]);
            // $content = $data[0]["doc_tx"];
            $data = $n->get_news_bin("", 0, 1, 3, $tid, $filter);
           // Zend_Debug::dump($data);die();
             $label =($data[0]["label"] != "" ? $data[0]["label"] : $data[0]["id"]);
            $content = str_replace('\n', '<br/>', $data[0]["content"]);
        } else {
            $data = $n->get_news_bin("", 0, 1, 2, $tid, $filter);
            // Zend_Debug::dump($data);die();
            $label =($data[0]["label"] != "" ? $data[0]["label"] : $data[0]["id"]);
            $content = str_replace('\n', '<br/>', $data[0]["content"]);
        }
       // Zend_Debug::dump($data); die();
        $content .= "<br><span style='color:#17C4BB;'>Tags : ".implode(" | ", $data[0]["taxonomy_names"] )."</span>";
        $content_date = $data[0]["content_date"];
        
        
        //Zend_Debug::dump($data);die();
        // echo $content;die();
        // echo "<pre>".$content."</pre>";
        // Zend_Debug::dump($content);die();
        // Zend_Debug::dump($label);die();
        if($label == null) {
            $label = " ";
        }
        // Zend_Debug::dump($label);die();
        $this->view->label = $label;
        $this->view->data = $content;
        $this->view->content_date = $content_date;
        $this->view->varams = $params;
    }

    public function trainingsetAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        $this->_helper->layout->disableLayout();
        // $this->_helper->viewRenderer->setNoRender(true);
        $mdl_admin = new Analis_Model_Trainingset();
        $countgraph = $mdl_admin->count_train_unigram($params);
        $listgraph = $mdl_admin->get_train_unigram($params);
        // Zend_Debug::dump($listgraph);die();
        $iTotalRecords = intval($countgraph);
        $iDisplayLength = intval($_GET['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_GET['iDisplayStart']);
        $sEcho = intval($_GET['sEcho']);
        $records = array();
        $records["aaData"] = array();
        $end = $iDisplayStart + $iDisplayLength;

        foreach($listgraph as $k => $v) {
            $arr_sentiment = array("netral",
                                   "negatif",
                                   "positif");
            $sentiment = '';
            $sentiment .= '<select class="form-control" id="s' . $k . '" name="s' . $k . '">';
            $sentiment .= '<option value=""></option>';

            foreach($arr_sentiment as $k1 => $v1) {
                $sentiment .= '<option value="' . $k1 . '" ' .($k1 == $v["sentiment"] ? "selected" : ""). '>' . $v1 . '</option>';
            }
            $sentiment .= '</select>';
            if(strtolower($params["t"])== "unigram") {
                $records["aaData"][] = array($iDisplayStart+$k+ 1,
                                            $v['id'],
                                            $v['pre'],
                                            $v['root'],
                                            $v['word'],
                                            $sentiment,
                                            );
            } else if(strtolower($params["t"])== "ngram") {
                $records["aaData"][] = array($iDisplayStart+$k+ 1,
                                            $v['id'],
                                            $v['words'],
                                            $sentiment,
                                            $v['attrs'],
                                            );
            }
        }
        // Zend_Debug::dump($records);die();
        $records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;
        
        /*
         * $gg = new Escort_Model_Kpu(); $res = $gg->get_datakpu($data2['witel']); //var_dump($res);die();
         */
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }

    public function trainingsetpredictiveAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        $this->_helper->layout->disableLayout();
        // $this->_helper->viewRenderer->setNoRender(true);
        $mdl_admin = new Analis_Model_Trainingsetpredictive();
        $countgraph = $mdl_admin->count_train_unigram($params);
        $listgraph = $mdl_admin->get_train_unigram($params);
        // Zend_Debug::dump($listgraph);die();
        $iTotalRecords = intval($countgraph);
        $iDisplayLength = intval($_GET['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_GET['iDisplayStart']);
        $sEcho = intval($_GET['sEcho']);
        $records = array();
        $records["aaData"] = array();
        $end = $iDisplayStart + $iDisplayLength;

        foreach($listgraph as $k => $v) {
            $arr_sentiment = array("netral",
                                   "negatif",
                                   "positif");
            $sentiment = '';
            $sentiment .= '<select class="form-control" id="s' . $k . '" name="s' . $k . '">';
            $sentiment .= '<option value=""></option>';

            foreach($arr_sentiment as $k1 => $v1) {
                $sentiment .= '<option value="' . $k1 . '" ' .($k1 == $v["sentiment"] ? "selected" : ""). '>' . $v1 . '</option>';
            }
            $sentiment .= '</select>';
            if(strtolower($params["t"])== "unigram") {
                $records["aaData"][] = array($iDisplayStart+$k+ 1,
                                            $v['id'],
                                            $v['pre'],
                                            $v['root'],
                                            $v['word'],
                                            $sentiment,
                                            );
            } else if(strtolower($params["t"])== "ngram") {
                $records["aaData"][] = array($iDisplayStart+$k+ 1,
                                            $v['id'],
                                            $v['words'],
                                            $sentiment,
                                            $v['attrs'],
                                            );
            }
        }
        // Zend_Debug::dump($records);die();
        $records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;
        
        /*
         * $gg = new Escort_Model_Kpu(); $res = $gg->get_datakpu($data2['witel']); //var_dump($res);die();
         */
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }

    public function generatesolrAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);//die();
        $filter = array();
        $skey = null;
        $start = 0;
        if($params["topic"] != "") {
            // $params["skey"] = str_replace(',','","',$params["skey"]);
            $params["topic"] = explode(',', $params["topic"]);
            // Zend_Debug::dump($params["skey"]);die();
            if(count($params["topic"])> 1) {

                foreach($params["topic"] as $k => $v) {
                    $topic .= '"' . $v . '" OR ';
                }
            } else {
                $topic = $params["topic"][0];
            }
            $topic = rtrim($topic, " OR ");
            $params["topic"] = $topic;
            $filter = "content:" . $topic;
            // Zend_Debug::dump($params);die();
        }
        // Zend_Debug::dump($params["topic"]);die();
        $limit = 10;
        if(isset($params["quantity"])&& $params["quantity"]) {
            $limit = $params["quantity"];
        }
        $topic = array();
        if(isset($params["topic"])&& $params["topic"] != "") {
            $cc = new Pmas_Model_Pdash();
            $topic = $cc->get_keys_pmas(533);
        }
        // Zend_Debug::dump($topic);die();
        $entity = - 1;
        if(isset($params["type"])&& $params["type"] != "") {
            $entity = $params["type"];
        }
        //Zend_Debug::dump($skey);
        //Zend_Debug::dump($start);
        //Zend_Debug::dump($limit);
        //Zend_Debug::dump($entity);
        //Zend_Debug::dump($filter);
        // die();
        $n = new Pmas_Model_Withsolrdash();
        $data = $n->get_news_bin($skey, $start, $limit, $entity, $topic, $filter);
        //Zend_Debug::dump($data);die();
        $this->view->varams = $params;
        $this->view->data = $data;
    }

    public function generatetablepredictiveAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        $filter = array();
        $skey = null;
        $start = 0;
        $limit = 10;
        if(isset($params["quantity"])&& $params["quantity"]) {
            $limit = $params["quantity"];
        }
        $topic = array();
        if(isset($params["topic"])&& $params["topic"] != "") {
            $cc = new Pmas_Model_Pdash();
            $topic = $cc->get_keys_pmas($params['topic']);
        }
        // Zend_Debug::dump($topic);die();
        $entity = - 1;
        if(isset($params["type"])&& $params["type"] != "") {
            $entity = $params["type"];
        }
        // Zend_Debug::dump($skey);
        // Zend_Debug::dump($start);
        // Zend_Debug::dump($limit);
        // Zend_Debug::dump($entity);
        // Zend_Debug::dump($filter);
        // die();
        $n = new Analis_Model_General();
        $data = $n->get_predictive($params["topic"]);
        // Zend_Debug::dump($data);die();
        $this->view->varams = $params;
        $this->view->data = $data;
    }

    public function wwwwwhAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        $n = new Pmas_Model_Solrfactminer();
        $g = new Pmas_Model_Pdash();
        //-============= start get  is_mediatype
        $arr_mt = array("nasional" => 1,
                        "lokal" => 2,
                        "internasional" => 3,
                        "blog" => 4,
                       );
        $mt = null;
        $mt2 = null;
        if($params["is_mediatype"] == "nasional") {
            $mt = urlencode("&fq=is_mediatype:1 OR is_mediatype:2");
        } else {
            $mt = urlencode("&fq=is_mediatype:" . 
                            $arr_mt[$params["is_mediatype"]]);
        }
        //-============= end get is_mediatype
        //-============= start get stopword
        $tmp_sw = $g->get_stopword("who");
        // Zend_Debug::dump($tmp_sw);die("s");
        $stopword = array();

        foreach($tmp_sw as $k => $v) {
            // $stopword[$k] = (strlen($v["name"])>1 && substr($v["name"],0,1)=="n"?strtolower(substr($v["name"],1)):strtolower($v["name"]));
            $stopword[$k] = strtolower($v["name"]);
        }
        // Zend_Debug::dump($stopword);die();
        //-============= end get stopword
        //-============= start fq doc_tx
        $skey = null;
        $arr_pencarian = array();
        $pencarian = "";
        if($params["skey"] != "") {
            $arr_pencarian = explode(",", $params["skey"]);
        }
        $tmp_tp = $g->get_topik_by_tema($params["cat"], $params["topik"]);
        // Zend_Debug::dump($tp);die();
        $tp = array();

        foreach($tmp_tp as $k => $v) {
            $st = $g->get_subtema_by_tema($v["tid"]);

            foreach($st as $k1 => $v1) {
                array_push($tp, $v1);
            }
        }
        // Zend_Debug::dump($tp);die();
        $arr_topik = array();

        foreach($tp as $k => $v) {
            array_push($arr_topik, trim(strtolower($v["name"])));
        }
        if(count(array_unique($arr_topik))> 500) {
            array_splice($arr_topik, 320);
        }
        // array_unique($arr_topik);
        // Zend_Debug::dump(array_unique($arr_topik));die();
        $skey = null;
        if(count($arr_pencarian)> 0 && count($arr_topik)> 0) {
            $skey = '&fq=content:("' . urlencode(implode('" AND "', $arr_pencarian)). '")' . urlencode(' AND '). '("' . urlencode(implode('" OR "', array_unique($arr_topik))). '")';
        } else if(count($arr_pencarian)> 0 && count(array_unique($arr_topik))== 0) {
            $skey = '&fq=content:("' . urlencode(implode('" AND "', $arr_pencarian)). '")';
        } else if(count($arr_pencarian)== 0 && count(array_unique($arr_topik))> 0) {
            $skey = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_topik))). '")';
        }
        //-============= end fq content
        //-============= start fq source
        $source = null;
        $ent = null;
        if(isset($params["t"])&& $params["t"] != "") {
            $e_id = array("News" => '2',
                          "Social Media" => '5',
                          "Laporan" => '6',
                         );
            $source = "&fq=entity_id:" . $e_id[$params["t"]];
        }
        //-============= end fq source
        //-============= start fq create_date range
        $cdr = null;
        if(isset($params["d1"])&& $params["d1"] != "" && isset($params["d2"])&& $params["d2"] != "") {
            $cdr = "&fq=content_date:[" . $params["d1"] . "T00:00:00Z+TO+" . $params["d2"] . "T23:59:59Z]";
        }
        //-============= start fq create_date range
        $fq = $skey . $source . $cdr . $mt . "&fq=sm_who:[*+TO+*]&fq=sm_where:[*+TO+*]";
        // die($fq);
        $x = $n->get_by_rawurl("select?q=*%3A*&sort=content_date+desc&wt=json&indent=true" . 
                               $fq);
        // Zend_Debug::dump($x);die();
        $tmp = $x["response"]["docs"];
        $data = array();

        foreach($tmp as $k0 => $v0) {
            $data[$k0]["id"] = $v0["id"];
            //	$data[$k0]["ext_id"] = $v0["ext_id"];
            $data[$k0]["content_date"] = $v0["content_date"];
            $data[$k0]["title"] = $v0["title"];
            $data[$k0]["content"] = $v0["content"];
            $data[$k0]["ss_author"][0] = $v0["ss_author"][0];
            $data[$k0]["bundle_name"][0] = $v0["bundle_name"][0];

            foreach($v0["sm_who"] as $k1 => $v1) {
                if(!in_array(strtolower($v1), $stopword)) {
                    $data[$k0]["sm_who"][] = $v1;
                }
            }
            $data[$k0]["sm_what"] = $v0["sm_what"];
            $data[$k0]["sm_when"] = $v0["sm_when"];
            $data[$k0]["sm_where"] = $v0["sm_where"];
            $data[$k0]["sm_how"] = $v0["sm_how"];
            $data[$k0]["sm_why"] = $v0["sm_why"];
        }
        //Zend_Debug::dump($params);die();
        if(isset($params["ajax"])&& $params["ajax"] == 1) {
            // Zend_Debug::dump($tmp);die("s");
            //1227
            header("Content-type: application/octet-stream");
            header("Content-Disposition: attachment; filename=SUMMARY__.doc");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo "<html>";
            echo "<head>";
            echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">";
            echo "<style>";
            echo "body {
		font-family: Arial, Verdana, Helvetica, Courier, sans-serif;
		font-size:12pt;
	}";
            echo "</style>";
            echo "</head>";
            echo "<body>";
            echo "<span style=\"color:#000000;font-size:12px;font-family:Arial,'Trebuchet MS', Verdana;\">";

            foreach($tmp as $k => $x) {
                $label = $x["title"];
                $text = str_replace('\n', '<br />', $x["doc_tx"]);
                // $data = $this->highlight($text, str_replace(" (ORG)","",str_replace(" (PER)","",implode("|",$x["who_str_mv"]))));
                $data = $text;
                $author =($x["author"][0] != "" ? $x["author"][0] : "-");
                $publisher =($x["publisher"][0] != "" ? $x["publisher"][0] : "-");
                $id = $x["id"];
                $create_date = date("l, d F Y H:i T", strtotime($x["create_date"]));
                $url = base64_decode($x["ext_id"]);
                echo '<h3><a href="' . $url . '">' . $label . '</a></h3>';
                echo '<div style="text-align:justify;text-justify:inter-word">';
                echo "Pada ";
                echo " " . date(" d F Y ", strtotime($create_date)). " ";
                echo " di ";
                echo implode(",", $x["where_str_mv"]). ",";
                echo implode(",", $x["who_str_mv"]). ",";
                if($x["what"] != "") {
                    echo ' melakukan ';
                    echo implode(",", $x["what"]). ",";
                }
                if($x["how"] != "") {
                    echo ' dengan ';
                    echo implode(",", $x["how"]). ",";
                }
                if($x["why"] != "") {
                    echo ' karena ';
                    echo implode(",", $x["why"]). "<br />";
                }
                echo '</div>';
                // echo "<b>SIAPA</b><br />";
                // echo implode("<br/>",$x["who_str_mv"])."<br /><br />";
                // echo "<b>APA</b><br />";
                // echo implode("<br/>",$x["what"])."<br /><br />";
                // echo "<b>BILAMANA</b><br />";
                // echo implode("<br/>",$x["when_str_mv"])."<br /><br />";
                // echo "<b>DIMANA</b><br />";
                // echo implode("<br/>",$x["where_str_mv"])."<br /><br />";
                // echo "<b>BAGAIMANA</b><br />";
                // echo implode("<br/>",$x["how"])."<br /><br />";
                // echo "<b>MENGAPA</b><br />";
                // echo implode("<br/>",$x["why"])."<br /><br />";
                if($params["t"] != "Social Media") {
                    // echo str_replace('\n','<br />',$x["doc_tx"])."<br /><br />";	
                }
                echo "<hr>";
            }
            echo "</span>";
            echo "</body>";
            echo "</html>";
            die();
            // $data1 = array();
            // foreach($tmp as $k=>$v){
            // $data1[$k] = array(
            // ($k+1),
            // implode("<br />",$v["who_str_mv"]),
            // implode(",",$v["what"]),
            // implode(",",$v["when_str_mv"]),
            // implode(",",$v["where_str_mv"]),
            // implode(",",$v["how"]),
            // implode(",",$v["why"]),
            // gmdate("l, d F Y H:i T", strtotime($v["create_date"])),
            // );
            // }
            // Zend_Debug::dump($data1);die();
            header("Access-Control-Allow-Origin: *");
            header('Content-Type: application/json');
            echo json_encode($data1);
            die();
        }
        //Zend_Debug::dump($params);die();
        // die("s");
        $this->view->varams = $params;
        $this->view->data = $data;
    }

    public function morewwwwwhAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        // // // $n = new Pmas_Model_Solrfactminer();
        // // // $g = new Pmas_Model_Pdash();
        // // // //-============= start get stopword
        // // // $tmp_sw = $g->get_stopword("who");
        // // // // Zend_Debug::dump($tmp_sw);die("s");
        // // // $stopword = array();
        // // // foreach($tmp_sw as $k=>$v){
        // // // // $stopword[$k] = (strlen($v["name"])>1 && substr($v["name"],0,1)=="n"?strtolower(substr($v["name"],1)):strtolower($v["name"]));
        // // // $stopword[$k] = strtolower($v["name"]);
        // // // }
        // // // // Zend_Debug::dump($stopword);die();
        // // // //-============= end get stopword
        // // // //-============= start fq doc_tx
        // // // $skey = null;
        // // // $arr_pencarian = array();
        // // // $arr_searchkey = array();
        // // // $pencarian = "";
        // // // if($params["skey"]!=""){
        // // // $arr_pencarian = explode(",",$params["skey"]);
        // // // }
        // // // if($params["searchkey"]!=""){
        // // // $arr_searchkey = explode(",",$params["searchkey"]);
        // // // }
        // // // $tmp_tp = $g->get_topik_by_tema($params["cat"],$params["topik"]);
        // // // // Zend_Debug::dump($tp);die();
        // // // $tp = array();
        // // // foreach($tmp_tp as $k=>$v){
        // // // $st = $g->get_subtema_by_tema($v["tid"]);
        // // // foreach($st as $k1=>$v1){
        // // // array_push($tp,$v1);
        // // // }
        // // // }
        // // // // Zend_Debug::dump($tp);die();
        // // // $arr_topik = array();
        // // // foreach($tp as $k=>$v){
        // // // array_push($arr_topik,trim(strtolower($v["name"])));
        // // // }
        // // // if(count(array_unique($arr_topik))>500){	
        // // // array_splice($arr_topik, 320);
        // // // }
        // // // // array_unique($arr_topik);
        // // // // Zend_Debug::dump(array_unique($arr_topik));die();
        // // // $skey = null;
        // // // if(count($arr_pencarian) > 0 && count($arr_topik) > 0){
        // // // $skey = '&fq=content:("'.urlencode(implode('" AND "',$arr_pencarian)).'")'.urlencode(' AND ').'("'.urlencode(implode('" OR "',array_unique($arr_topik))).'")';
        // // // } else if(count($arr_pencarian) > 0 && count(array_unique($arr_topik)) == 0){
        // // // $skey = '&fq=content:("'.urlencode(implode('" AND "',$arr_pencarian)).'")';
        // // // } else if(count($arr_pencarian) == 0 && count(array_unique($arr_topik)) > 0){
        // // // $skey = '&fq=content:("'.urlencode(implode('" OR "',array_unique($arr_topik))).'")';
        // // // }
        // // // $content3 = null;
        // // // if(count($arr_searchkey) > 0){
        // // // $content3 = 'content:("'.(implode('" OR "',array_unique($arr_searchkey))).'")';
        // // // }
        // // // //-============= end fq doc_tx
        // // // //-============= start fq source
        // // // $source = null;
        // // // $ent=null;
        // // // if(isset($params["t"]) && $params["t"]!=""){
        // // // $e_id = array(
        // // // "News"=>2,
        // // // "news"=>2,
        // // // "Social Media"=>5,
        // // // "Media Sosial"=>5,
        // // // "Laporan"=>6,
        // // // );
        // // // $source ="&fq=entity_id:".$e_id[$params["t"]];
        // // // }
        // // // //-============= end fq source
        // // // //-============= start fq create_date range
        // // // $cdr = null;
        // // // if(isset($params["d1"]) && $params["d1"]!="" && isset($params["d2"]) && $params["d2"]!=""){
        // // // $cdr = "&fq=content_date:[".$params["d1"]."T00:00:00Z+TO+".$params["d2"]."T23:59:59Z]";
        // // // }
        // // // //-============= start fq create_date range
        // // // //-============= start get  is_mediatype
        // // // $arr_mt = array(
        // // // "nasional"=>1,
        // // // "lokal"=>2,
        // // // "internasional"=>3,
        // // // "blog"=>4,
        // // // );
        // // // $mt=null;
        // // // $mt2=null;
        // // // if($params["t"]=="News"){			
        // // // if($params["is_mediatype"]=="nasional"){
        // // // $mt = ("&fq=".urlencode("is_mediatype:1 OR is_mediatype:2"));
        // // // }else{
        // // // $mt = ("&fq=is_mediatype:".urlencode($arr_mt[$params["is_mediatype"]]));
        // // // }
        // // // $mt2 = "&fq=".urlencode("sm_who:[* TO *]");
        // // // }
        // // // //-============= end get is_mediatype
        // // // $fq = $skey.$source.$content3.$cdr.$mt.$mt2;
        // // // // die($fq);
        // // // $start = (int)$params['page'] * 10;
        // // // $next = (int) $params['page'] + 1;
        // // // // die("select?q=*%3A*&sort=content_date+asc&wt=json&indent=true&start=".$start."&rows=10".$fq);
        // // // $x = $n->get_by_rawurl("select?q=*%3A*&sort=content_date+asc&wt=json&indent=true&start=".urlencode($start)."&rows=10".$fq);
        // // // // Zend_Debug::dump($x);die();
        // // // $tmp = $x["response"]["docs"];
        $parameter = array();

        foreach($params as $k => $v) {
            if($k != "module" && $k != "controller" && $k != "action" && $k != "anomali") {
                $parameter[$k] = $v;
            }
        }
        $parameter["iDisplayStart"] = (int) $params['page'] * 10;
        $start = (int) $params['page'] * 10;
        $next = (int) $params['page'] + 1;
        $g = new Pmas_Model_Pdash();
        $s = new Analis_Model_Solr();
        $z = new Pmas_Model_Cache();
        $cache = $z->cachefunc(1000);
        $id = $z->get_id("Analis_Model_Solr_get_5w1h", $parameter);
        $tmp = $z->get_cache($cache, $id);
        // Zend_Debug::dump($tmp);die("s");    
        if(!$tmp) {
            $tmp = $s->get_5w1h($parameter);
            $cache->save($tmp, $id, array('systembin'));
        }
        // Zend_Debug::dump($tmp);die("s");  

        foreach($tmp as $k => $v) {
            // Zend_Debug::dump($v);
            if($params["t"] != "Social Media" && $params["t"] != "Media Sosial") {
                $whotext = '';
                if(count($v["sm_where"])> 0 AND $v["sm_where"][0] != "") {
                    // echo$v["sm_where"];
                    $textanalis .= implode(", ", $v["sm_where"]). ', ';
                }
                $textanalis = date(" d F Y ", strtotime($v["content_date"]));
                if(count($v["sm_what"])> 0 && $v["sm_what"][0] != "") {
                    $textanalis .= '<br><span style="color:#d5ff5b;">' . implode(",", $v["sm_what"]). '</span>';
                }
                if(count($v["sm_who"])> 0 && $v["sm_who"][0] != "") {

                    foreach($v["sm_who"] as $k1 => $v1) {
                        $who =(substr($v1, 0, 1)== "n" ? substr($v1, 1): $v1);
                        $whotext .= '<a class="font-default" href="/profilingmodul?search=' . urlencode(trim(str_replace("(ORG)", "", str_replace("(PER)", "", $who)))). '" target="_blank">' . $who . '</a>, ';
                    }
                    $textanalis .= "<br>WHO :" . $whotext;
                }
                if(count($v["sm_how"])> 0 && $v["sm_how"][0] != "") {
                    $textanalis .= '<br>HOW : ' . implode(",", $v["sm_how"]);
                }
                if(count($v["sm_why"])> 0 && $v["sm_why"][0] != "") {
                    $textanalis .= '<br>WHY : ' . implode(",", $v["sm_why"]);
                }
                if($params["t"] == "Laporan") {
                    $textanalis = "";
                    if(count($v["sm_what"])> 0 && $v["sm_what"][0] != "") {
                        $textanalis .= '<span style="color:#d5ff5b;">' . implode(",", $v["sm_what"]). '</span><br>';
                    }
                    $textanalis .= $v["ss_summary"];
                }
                echo '<tr>';
                echo '	<td>' .($start + $k + 1). '</td>';
                echo '	<td>' . $textanalis . '</td>';
                $id5w = base64_encode($v["id"]);
                echo '	<td>
					<button class="btn blue w5h1detail" data-id="' . $v["id"] . '" data-dl="0" data-t="' . $this->varams["t"] . '" onclick="getVS(this);">DETAIL</button>
					<a class="btn yellow" href="/trendingtopik/index/edit5w1h/id/' . $id5w . '" target="_blank">EDIT</a>
				</td>';
                echo '</tr>';
            } else {
                echo '<tr class="w5h1detail" data-id="' . $v["id"] . '" data-dl="0" data-t="' . $this->varams["t"] . '" onclick="getVS(this);">';
                echo '	<td>' .($start + $k + 1). '</td>';
                echo '	<td>TWITTER</td>';
                echo '	<td><a href="https://twitter.com/' . $v["ss_user_screenname"] . '/status/' . $v["id"] . '" target="_blank">' . $v["label"] . ' @' . $v["ss_user_screenname"] . '</a></td>';
                echo '	<td>' . $v["content"] . '</td>';
                echo '	<td>' . date("l, d F Y H:i T", strtotime($v["content_date"])). '</td>';
                echo '</tr>';
            }
        }
        if(count($tmp)== 10) {
            echo '<tr class="load">';
            echo '<td colspan="5">';
            echo '<div id="error"></div>';
            echo '<span class="ajaxload" style="display:none">';
            echo '<img src="/assets/core/img/ajax-loading.gif"></span>';
            echo '<button class="btn btn-circle blue" ><span>Lebih Lanjut</span></button>';
            echo '</td>';
            echo '</tr>';
        }
        echo '<script>';
        echo '$("#5w1h' . $params["anomali"] . ' .load").on("click", function(){
		$("#5w1h' . $params["anomali"] . ' .ajaxload").show();
		
		var sdate = edate = "";
		
		// console.log("ok");
		App.blockUI({target:"#5w1h"+anomali,boxed:!0});
		
		var anomali = "' . $params["anomali"] . '";
		
		var s = "who";
		// if($(me).data("s")!=undefined){
			// s = $(me).data("s");
		// }

		// console.log(s);

		var formData = {}
		var param = "";
		param +="anomali=' . $params["anomali"] . '";
		param +="&t=' . $params["t"] . '";
		param +="&s="+s;
		param +="&skey=' . $params["skey"] . '";
		param +="&searchkey=' . $params["searchkey"] . '";
		param +="&skeyexception=' . $params["skeyexception"] . '";
		param += "&cat=' . $params["cat"] . '";
		param += "&topik=' . $params["topik"] . '";
		param += "&is_mediatype="+$("#media_type").val();

		if(sdate!="" && edate!=""){
			param += "&d1="+sdate+"&d2="+edate;	
			param +="&is_sentiment=' . $params["is_sentiment"] . '";
		} else {	
			$("#is_sentiment").val("");
			param +="&d1=' . $params["d1"] . '&d2=' . $params["d2"] . '";
			param +="&is_sentiment=' . $params["is_sentiment"] . '";
		}

		// console.log(param);
		
		

	$.ajax({  
		type: "GET",
		url: "/analis/ajaxhome/morewwwwwh/page/' . $next . '?"+param,
		data: formData, //last_id kita berarti 15
		dataType: "html",  //sesuai keinginan, di sini saya pengen ngambil langsung data dalam bentuk html langsung dari file data_per_load.php
		success: function(data){
			 // console.log(data);
			// $("#5w1h").html(data);
			$("#5w1h' . $params["anomali"] . ' .load").hide();
			jQuery("#tbd5w1h"+anomali).append(data);
			App.unblockUI("#5w1h"+anomali);
		},
		error: function() {
            $("#error").html("error");
            }
	});
	});';
        echo '</script>';
        die();
    }

    public function vsAction() {
        // die("s");
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);die();
        // die("http://172.16.3.20:8080/solr/factminer/select?q=*%3A*&fq=type:doc&fq=id:".$params["id"]."&wt=json&indent=true");
        $n = new Pmas_Model_Solrfactminer();
        $x = $n->get_by_rawurl('select?q=*%3A*&fq=id:"' . 
                               $params["id"] . 
                               '"&wt=json&indent=true');
        // Zend_Debug::dump($x);die();
        // $label = $x["response"]["docs"][0]["title"];
        $label = $x["response"]["docs"][0]["sm_what"][0];
        $text = str_replace('\n', '<br />', $x["response"]["docs"][0]["content"]);
        // $data = $this->highlight($text, str_replace(" (ORG)","",str_replace(" (PER)","",implode("|",$x["response"]["docs"][0]["who_str_mv"]))));
        $data = $text;
        $author =($x["response"]["docs"][0]["ss_author"] != "" ? $x["response"]["docs"][0]["ss_author"] : "-");
        $publisher =($x["response"]["docs"][0]["bundle"] != "" ? $x["response"]["docs"][0]["bundle"] : "-");
        $id = $x["response"]["docs"][0]["id"];
        $sm_who = $x["response"]["docs"][0]["sm_who"];
        date_default_timezone_set('Europe/London');
        $create_date = date("l, d F Y H:i T", strtotime($x["response"]["docs"][0]["content_date"]));
        $url = base64_decode($x["response"]["docs"][0]["ext_id"]);
        $sent = array(0 => '<span class="badge badge-success" style="background-color:rgb(53, 152, 220);">&nbsp</span  Netral',
                      1 => '<span class="badge badge-success" style="background-color:rgb(228, 58, 69);">&nbsp</span> Negatif',
                      2 => '<span class="badge badge-success" style="background-color:rgba(63,219,154,1);">&nbsp</span> Positif');
        $params["sentiment"] = $sent[$x["response"]["docs"][0]["is_sentiment"]];
        // Zend_Debug::dump($params);die();		
        if(isset($params["dl"])&& $params["dl"] == "1") {
            // header("Content-type: application/octet-stream");
            // header("Content-Disposition: attachment; filename=SUMMARY__.doc");
            // header("Pragma: no-cache");
            // header("Expires: 0");
            // echo "<html>";
            // echo "<head>";
            // echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">";
            // echo "<style>";
            // echo "body {
            // font-family: Arial, Verdana, Helvetica, Courier, sans-serif;
            // font-size:12pt;
            // }";
            // echo "</style>";
            // echo "</head>";
            // echo "<body>";
            // echo "<span style=\"color:#000000;font-size:12px;font-family:Arial,'Trebuchet MS', Verdana;\">";
            // echo '<h3><a href="'.$url.'">'.$label.'</a></h3><br />';
            // echo "<h5>".$author." / ".$publisher."</h5><br />";
            // echo "<h5>".date("l, d F Y H:i T", strtotime($create_date))."</h5><br />";
            // echo str_replace('\n','<br />',$x["response"]["docs"][0]["doc_tx"])."<br /><br />";
            // echo "<b>SIAPA</b><br />";
            // echo implode("<br/>",$x["response"]["docs"][0]["who_str_mv"])."<br /><br />";
            // echo "<b>APA</b><br />";
            // echo implode("<br/>",$x["response"]["docs"][0]["what"])."<br /><br />";
            // echo "<b>BILAMANA</b><br />";
            // echo implode("<br/>",$x["response"]["docs"][0]["when_str_mv"])."<br /><br />";
            // echo "<b>DIMANA</b><br />";
            // echo implode("<br/>",$x["response"]["docs"][0]["where_str_mv"])."<br /><br />";
            // echo "<b>BAGAIMANA</b><br />";
            // echo implode("<br/>",$x["response"]["docs"][0]["how"])."<br /><br />";
            // echo "<b>MENGAPA</b><br />";
            // echo implode("<br/>",$x["response"]["docs"][0]["why"])."<br /><br />";
            // echo "</span>";
            // echo "</body>";
            // echo "</html>"; 
            header("Content-type: application/octet-stream");
            header("Content-Disposition: attachment; filename=SUMMARY__.doc");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo "<html>";
            echo "<head>";
            echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">";
            echo "<style>";
            echo "body {
    font-family: Arial, Verdana, Helvetica, Courier, sans-serif;
    font-size:12pt;
}";
            echo "</style>";
            echo "</head>";
            echo "<body>";
            echo "<span style=\"color:#000000;font-size:12px;font-family:Arial,'Trebuchet MS', Verdana;\">";
            echo '<h3><a href="' . $url . '">' . $label . '</a></h3><br />';
            //echo "<h5>".$author." / ".$publisher."</h5><br />";
            echo '<div style="text-align:justify;text-justify:inter-word">';
            echo "Pada ";
            echo " " . date(" d F Y ", strtotime($create_date)). " ";
            echo " di ";
            echo implode(",", $x["response"]["docs"][0]["where_str_mv"]). ",";
            echo implode(",", $x["response"]["docs"][0]["who_str_mv"]). ",";
            if($x["response"]["docs"][0]["what"] != "") {
                echo ' melakukan ';
                echo implode(",", $x["response"]["docs"][0]["what"]). ",";
            }
            if($x["response"]["docs"][0]["how"] != "") {
                echo ' dengan ';
                echo implode(",", $x["response"]["docs"][0]["how"]). ",";
            }
            if($x["response"]["docs"][0]["why"] != "") {
                echo ' karena ';
                echo implode(",", $x["response"]["docs"][0]["why"]). "<br />";
            }
            echo '</div>';
            echo "<h5>" . date("l, d F Y H:i T", strtotime($create_date)). "</h5><br />";
            echo str_replace('\n', '<br />', $x["response"]["docs"][0]["doc_tx"]). "<br /><br />";
            echo "</span>";
            echo "</body>";
            echo "</html>";
            die();
        }
        // Zend_Debug::dump(str_replace("\n","<br />",$x["response"]["docs"][0]["doc_tx"]));die("s");
        $this->view->label = str_replace('"', '\"', $label);
        $this->view->data = $data;
        $this->view->author = $author;
        $this->view->publisher = $publisher;
        $this->view->sm_who = $sm_who;
        $this->view->url = $url;
        $this->view->id = $id;
        $this->view->create_date = $create_date;
        $this->view->varams = $params;
        $this->view->xx = $x["response"]["docs"][0];
    }

    public function linkAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params);//die();
        $g = new Pmas_Model_Pdash();
        $ff = new Pmas_Model_Withsolr();
        $str = "*:*";
        //-============= start fq content
        $skey = null;
        $arr_pencarian = array();
        $pencarian = "";
        if($params["skey"] != "") {
            $arr_pencarian = explode(",", $params["skey"]);
        }
        $tmp_tp = $g->get_topik_by_tema($params["cat"], $params["topik"]);
        // Zend_Debug::dump($tp);die();
        $tp = array();

        foreach($tmp_tp as $k => $v) {
            $st = $g->get_subtema_by_tema($v["tid"]);

            foreach($st as $k1 => $v1) {
                array_push($tp, $v1);
            }
        }
        // Zend_Debug::dump($tp);die();
        $arr_topik = array();

        foreach($tp as $k => $v) {
            array_push($arr_topik, trim(strtolower($v["name"])));
        }
        if(count(array_unique($arr_topik))> 500) {
            array_splice($arr_topik, 320);
        }
        // Zend_Debug::dump($arr_topik);die();
        $skey = null;
        if(count($arr_pencarian)> 0 && count(array_unique($arr_topik))> 0) {
            $skey = '&fq=content:("' . urlencode(implode('" AND "', $arr_pencarian)). '")' . urlencode(' AND '). '("' . urlencode(implode('" OR "', array_unique($arr_topik))). '")';
        } else if(count($arr_pencarian)> 0 && count(array_unique($arr_topik))== 0) {
            $skey = '&fq=content:("' . urlencode(implode('" AND "', $arr_pencarian)). '")';
        } else if(count($arr_pencarian)== 0 && count(array_unique($arr_topik))> 0) {
            $skey = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_topik))). '")';
        }
        //-============= end fq content
        //-============= start fq content_date range
        $d1 = date("Y-m-d", strtotime('monday last week'));
        $d2 = date("Y-m-d");
        if(isset($params["d1"])&& $params["d1"] != "" && isset($params["d2"])&& $params["d2"] != "") {
            $d1 = date("Y-m-d", strtotime($params['d1']));
            $d2 = date("Y-m-d", strtotime($params['d2']));
            // Zend_Debug::dump($d2);die();
        }
        // $filter .="&content_date:[".$d1.$d2."]";
        $cdd = "&fq=content_date:[" . $d1 . "T00:00:00Z+TO+" . $d2 . "T23:59:59Z]";
        //-============= end fq content_date range
        //-============= start fq sentiment
        $sen = null;
        if(isset($params["is_sentiment"])&& $params["is_sentiment"] != "") {
            $sen = "&fq=is_sentiment:0";
            if($params["is_sentiment"] == "negatif") {
                $sen = "&fq=is_sentiment:1";
            } else if($params["is_sentiment"] == "positif") {
                $sen = "&fq=is_sentiment:2";
            }
        }
        //-============= end fq sentiment
        //-============= start fq media_type
        $mt = null;
        if(isset($params["is_mediatype"])&& $params["is_mediatype"] != "") {
            if($params["is_mediatype"] == "nasional") {
                $mt = "&fq=is_mediatype:(" . urlencode("1 OR 2"). ")";
            } else {
                $mt = "&fq=is_mediatype:3";
            }
        }
        //-============= end fq media_type
        $fq1 = $skey . $cdd . $sen;
        $fq = $skey . $cdd . $sen . $mt;
        // die("select?q=*%3A*&sort=content_date+desc&wt=json&indent=true&entity_id:5&fl=ss_user_screenname,ss_retweeted_user_screenname&rows=1000&fq=ss_isretweet:true".$fq);
        //echo $fq1; die();
        $tmp = $ff->get_by_rawurl("select?q=*%3A*&sort=content_date+desc&wt=json&indent=true&entity_id:5&fl=is_sentiment,id,ss_user_screenname,ss_retweeted_user_screenname&rows=1000&fq=ss_isretweet:true" . 
                                  $fq1);
        // Zend_Debug::dump($tmp);die();
        $x = $tmp["response"]["docs"];
        //Zend_Debug::dump(($x));die();
        $data = array();
        $sent = array(0 => 'netral',
                      1 => 'negatif',
                      2 => 'positif');

        foreach($x as $k => $v) {
            $data[$k]["xid"] = $v["id"];
            $data[$k]["sent"] = $sent[$v["is_sentiment"]];
            $data[$k]["source"] = $v["ss_user_screenname"];
            $data[$k]["target"] = $v["ss_retweeted_user_screenname"];
            $data[$k]["type"] = "licensing";
        }
        //Zend_Debug::dump($data); die();
        $this->view->data = json_encode($data);
        $this->view->cat = $dash;
        // $this->view->markers = json_encode($markers);
        $this->view->varams = $params;
        // Zend_Debug::dump($data);die();
    }

    public function listalertAction() {
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        // $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params["id"]);die();
        // $as = new Analis_Model_General();
        // $data = $as->listalertfr();
        // Zend_Debug::dump('dd'.$data);
        // $this->view->data = $data;
        $this->view->varams = $params;
    }

    public function tablealertAction() {
        //die("lalala");
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $params = $this->getRequest()->getParams();
        $this->_helper->layout->disableLayout();
        // $this->_helper->viewRenderer->setNoRender(true);
        $mdl_admin = new Analis_Model_General();
        $countgraph = $mdl_admin->count_listalert($params['search'], $_GET);
        $listgraph = $mdl_admin->get_listalert($params['search'], $_GET);
        // die();
        $iTotalRecords = intval($countgraph);
        $iDisplayLength = intval($_GET['iDisplayLength']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_GET['iDisplayStart']);
        $sEcho = intval($_GET['sEcho']);
        $records = array();
        $records["aaData"] = array();
        $end = $iDisplayStart + $iDisplayLength;

        foreach($listgraph as $k => $v) {
            // Zend_Debug::dump($v);die();
            $records["aaData"][] = array($iDisplayStart+$k+ 1,
                                        $v['IDPROFILE'],
                                         '<a href="../../profilingmodul/home/ceknik?nik=' .$v["NAMA"]. '&nama=' .$v["IDPROFILE"]. '" target="_blank" >' .$v['NAMA']. '</a>',
                                        $v['CAMERANAME'],
                                        $v['IPCAMERA'],
                                         '<img src="/' .$v['PHOTO']. '" height="100">',
                                        $v['SIMILARITY'],
                                        $v['LAST_UPDATE'],
                                        );
        }
        // Zend_Debug::dump($records);die();
        $records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;
        
        /*
         * $gg = new Escort_Model_Kpu(); $res = $gg->get_datakpu($data2['witel']); //var_dump($res);die();
         */
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($records);
        die();
    }

    public function alertfrnotifAction() {
        $this->_helper->layout->disableLayout();
        $as = new Analis_Model_General();
        $data = $as->alertfr();
        // Zend_Debug::dump($data);
        echo '
			<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
				<i class="glyphicon glyphicon-warning-sign"></i>
				<span class="badge badge-default"> ' . count($data). ' </span>
				
			</a>
			<ul class="dropdown-menu">
				<li class="external">
				<h3>ALERT BARU
				<span class="bold">' . count($data). '</span></h3>
					<a href="/analis/ajaxhome/listalert">view all</a>
				</li>';
        // <a href="app_inbox.html">view all</a>
        if(count($data)> 0) {

            foreach($data as $v) {
                echo '<li>
							<ul class="dropdown-menu-list scroller" style="overflow: hidden; width: auto;" data-handle-color="#637283">
								<li>
										<a  href="#alertfrnotif" data-toggle="modal" onclick="getalert(' . $v['ID_ALERT'] . ');">
											<div class="row">
												<div class="col-md-2">
															<img src="../../' . $v['PHOTO'] . '" class="img-circle" height="45" alt=""> </span>
												</div>
												<div class="row col-md-10">																								
													<div class="col-md-6">
														' . $v['NAMA'] . '
													</div>
													<div class="col-md-6">
														' . $v['LAST_UPDATE'] . '
													</div>
												</div>
												<div class="col-md-10">	
													' . $v['CAMERANAME'] . '
												</div>
											</div>
										</a>
								</li>
							</ul>
						</li>';
            }
        }
        echo '</ul>';
        die();
    }

    public function alertfgetAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params["id"]);die();
        $as = new Analis_Model_General();
        $data = $as->selectalertfr($params["id"]);
        // Zend_Debug::dump('dd'.$data);
        $this->view->data = $data;
        $this->view->varams = $params;
    }

    public function alertfupdateAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        // Zend_Debug::dump($params["id"]);die();
        $as = new Analis_Model_General();
        $data = $as->alertfrupdate($params["id"]);
        // Zend_Debug::dump($params["id"]);die();
        die();
    }
}
