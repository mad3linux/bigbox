<?php

/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
// ini_set("display_errors", "On"); 

class Domas_DiscoveryController extends Zend_Controller_Action {

    public function init() {
        
        /* Initialize action controller here */
    }

	public function listAction(){
		
    	$params = $this->getRequest()->getParams();
		// Zend_Debug::dump($params);die();
		
		if(isset($params["ajax"]) && $params["ajax"]==1){
			$this->_helper->layout->disableLayout();
		}		
		
		$getsentimentid = array("netral"=>0,"negatif"=>1,"positif"=>2);
		$getsentimentname = array(0=>"netral",1=>"negatif",2=>"positif");
		
		$params["display"] = ($params["series"]=="is_sentiment"?$getsentimentid[$params["display"]]:$params["display"]);
		
		// Zend_Debug::dump($params);die();
		// Zend_Debug::dump($_POST);//die();		
		
		$andsearch = ($_POST["andsearch"]!="" ? explode(",",$_POST["andsearch"]) : array());
		$exsearch = ($_POST["exsearch"]!="" ? explode(",",$_POST["exsearch"]) : array());
		
		$q = "";
		$filterQuery = array();
		$filterQuery[] = "entity_id:2";
		$filterQuery[] = "content_date:[".$_POST["startdate"]."T00:00:00Z TO ".$_POST["enddate"]."T23:59:59Z]";
		$filterQuery[] = "is_sentiment:".$params["is_sentiment"];		
		
		$filterQuery[] = 'content:"'.$params["name"].'"';
		if($params["name"]!=$params["display"]){			
			$filterQuery[] = $params["series"].':"'.$params["display"].'"';			
		}
		
		if(count($andsearch)>0){
			foreach ($andsearch as $k => $v) {
				$filterQuery[] = 'content:"'.$v.'"';
			}
		}
		if(count($exsearch)>0){
			foreach ($exsearch as $k => $v) {
				$filterQuery[] = '-content:"'.$v.'"';
			}
		}
		
		$rows = 10;
		$start = $params["page"]*$rows;
		

		$p = array(
			"q" => $q,
			"filterQuery" => $filterQuery,
			"rows" => $rows,
			"start" => $start,
		);

		// Zend_Debug::dump($p);die("xxxx");

		$mdlSolr = new Domas_Model_Solr();

		$data = $mdlSolr->solrquery($p);
		// Zend_Debug::dump($data["response"]["response"]["numFound"]);die();		
		
		$params["page"] = $params["page"]+1;
		$params["next"] = ((int)$data["response"]["response"]["numFound"]>10 && ((int)$data["response"]["response"]["numFound"]-(int)$data["response"]["response"]["start"])>10?true:false);
		
		$this->view->params;
		$this->view->data = $data;
	}
	
    public function v1Action(){

		// date_default_timezone_set('Asia/Jakarta');		

    	$params = $this->getRequest()->getParams();
		// Zend_Debug::dump($_POST);//die();


		if($_POST && isset($_POST["keysearch"]) && $_POST["keysearch"]!=""){

			// Zend_Debug::dump($_POST);die();
			
			$data = array();

			$keysearch = ($_POST["keysearch"]!="" ? explode(",",$_POST["keysearch"]) : array());
			$andsearch = ($_POST["andsearch"]!="" ? explode(",",$_POST["andsearch"]) : array());
			$exsearch = ($_POST["exsearch"]!="" ? explode(",",$_POST["exsearch"]) : array());
			// Zend_Debug::dump($andsearch);die();

			foreach ($keysearch as $key => $value) {

				$q = $value;

				$filterQuery = array();
				$filterQuery[] = "entity_id:2";
				$filterQuery[] = "content_date:[".$_POST["startdate"]."T00:00:00Z TO ".$_POST["enddate"]."T23:59:59Z]";
				if(count($andsearch)>0){
					foreach ($andsearch as $k => $v) {
						$filterQuery[] = 'content:"'.$v.'"';
					}
				}
				if(count($exsearch)>0){
					foreach ($exsearch as $k => $v) {
						$filterQuery[] = '-content:"'.$v.'"';
					}
				}

				$facet = true;
				$rows = "0";

				$p = array(
					"q" => $q,
					"filterQuery" => $filterQuery,
					"facet" => true,
					"rows" => $rows,
					"facetField" => array("bundle","is_sentiment"),
				);

				// Zend_Debug::dump($p);die("xxxx");

				$mdlSolr = new Domas_Model_Solr();

				$data[$value] = $mdlSolr->solrquery($p);
				// Zend_Debug::dump($data);die();
			}

			// Zend_Debug::dump($data);die();
			
			$jsdata = array();
			$jsdata["legend"] = array();
			$listlegend = array();
			$tmpfacetfields = array();
			$i = 0;
			foreach($data as $k=>$v){
				$jsdata["key"][$i]["name"] = $k;
				$jsdata["key"][$i]["display"] = $k;
				$jsdata["key"][$i]["value"] = $v["response"]["response"]["numFound"];
				foreach($v["response"]["facet_counts"]["facet_fields"] as $k1=>$v1){
					foreach($v1 as $name=>$value){
						$tmpfacetfields[$k][$k1][$name] = $value;
					}
				}
				if(!in_array($k, $listlegend)){
					
					array_push($listlegend, $k);

					$push = array(
						"name"=>$k,
						// "textStyle"=> array("fontSize"=>15,"icon"=>"rect"),
					);
					array_push($jsdata["legend"], $push);
				}
				$i++;
			}

			// Zend_Debug::dump($jsdata);die();
			
			// Zend_Debug::dump($tmpfacetfields);die();
			foreach($tmpfacetfields as $k=>$v){
				// $i = 0;
				foreach($v as $k1=>$v1){
					if(!isset($jsdata[$k1])){
						$jsdata[$k1] = array();	
					}
					foreach($v1 as $name=>$value){
						
						if($k1=="is_sentiment"){
							$sentiment = array("netral","negatif","positif");
							$name = $sentiment[$name];
						}

						$push = array(
							"name"=>$k,
							"display"=>$name,
							"value"=>$value,
						);

						array_push($jsdata[$k1], $push);
						
						// $jsdata[$k1][$i]["name"] = $k;
						// $jsdata[$k1][$i]["display"] = $name;
						// $jsdata[$k1][$i]["value"] = $value;
						// $i++;
					}
				}
			}
			
			// Zend_Debug::dump($jsdata);die();
		}
        
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        //-== JS
        $this->view->headScript()->appendFile('/assets/core/global/plugins/echarts/echarts.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        // $this->view->headScript()->appendFile('http://maps.google.com/maps/api/js?language=id');
        // $this->view->headScript()->appendFile('/assets/core/plugins/gmaps/gmaps.js');
        // $this->view->headScript()->appendFile('/assets/core/js/d3.v3.min.js');


        $g = new Pmas_Model_Pdash();
/*        $n = new Pmas_Model_Withsolr();

        $datasource = array(
        	"2"=>"DIGITAL MEDIA",
        	"3"=>"DOCUMENT",
        	"4"=>"MESSAGING",
        	"5"=>"SOCIAL MEDIA TWITTER",
        	"6"=>"OTHERS",
    	);
        $sentiment = array(
        	"netral",
        	"negatif",
        	"positif",
    	);

    	$andsearch = array();
    	if($params["andsearch"]!="" && $params["andsearch"]!=null){
    		$andsearch = explode(",", $params["andsearch"]);
    	}
        	
    	if($params["exsearch"]!="" && $params["exsearch"]!=null){
        	$excsearch = explode(",", $params["exsearch"]);
        	// Zend_Debug::dump($keysearch);die();
        	$exception = array();
        	foreach($excsearch as $k=>$v){
        		$vv = explode("<>",$v);
        		// Zend_Debug::dump($vv);die();
        		$exception[$vv[0]][] = $vv[1];
        	}
        }
        	// Zend_Debug::dump($exception);die();

        if($params["keysearch"]!="" &&$params["keysearch"]!=null){
        	$keysearch = explode(",", $params["keysearch"]);
			$andsearch = array();
			if($params["andsearch"]!=""){				
				$andsearch = explode(",", $params["andsearch"]);
			}
			$exsearch = array();
			if($params["exsearch"]!=""){				
				$exsearch = explode(",", $params["exsearch"]);
			}

        	$data = array();        	
        	$jsdata = array();
        	foreach($datasource as $entity_id=>$entity_name){   	
        		// $jsdata[$entity_id] = array();
        		$i = 0;
        		foreach ($keysearch as $k => $key) {

        			$and = "";
        			$and .= "&fq=content:%5B*+TO+*%5D";
        			$and .= "&fq=content_date%3A%5B".$params["startdate"]."T00%3A00%3A00Z+TO+".$params["enddate"]."T23%3A59%3A59Z%5D";
        			$and .= "&fq=entity_id%3A".$entity_id;
        			$and .= '&fq=content%3A%22'.urlencode($key).'%22';
        			$and .= '&fq=is_mediatype%3A1+OR+2';
					if(count($andsearch)>0){
						foreach($andsearch as $x=>$y){
							$and .='&fq=content%3A"'.urlencode($y).'"';
						}
					}
					if(count($exsearch)>0){
						foreach($exsearch as $x=>$y){
							$and .='&fq=-content%3A%22'.urlencode($y).'%22';
						}
					}

        			$query = "select?q=*%3A*".$and."&rows=0&wt=json&indent=true&facet=true";
        			$query2 = "select?q=*%3A*".$and."&rows=10&wt=json&indent=true";
        			// echo $query;
					// echo "<br>";
					// echo urldecode($query);
					// die();

        			$tmp = $n->get_by_rawurl($query);
        			$tmp2 = $n->get_by_rawurl($query2);
        			// Zend_Debug::dump($tmp2);die();
        			if($tmp["response"]["numFound"]>0){
        				$data[$entity_id]["entity"][$key]["count"] = $tmp2["response"]["numFound"];
        				$data[$entity_id]["entity"][$key]["data"] = $tmp2["response"]["docs"];
        				$jsdata[$entity_id]["key"][$i]["value"] = (int)$tmp["response"]["numFound"];
        				$jsdata[$entity_id]["key"][$i]["name"] = $key;
        				$i++;
        			}
        		}

        		// Zend_Debug::dump($entity_id);//die();
        		// Zend_Debug::dump($data);die();

        		// $jsdata[$entity_id]["sentiment"] = array();
    			$i = 0;
        		foreach ($sentiment as $sentiment_id => $sentiment_name) {

	    			$and = "";
        			$and .= "&fq=content%3A%5B*+TO+*%5D";
	    			$and .= "&fq=content_date%3A%5B".$params["startdate"]."T00%3A00%3A00Z+TO+".$params["enddate"]."T23%3A59%3A59Z%5D";
	    			$and .= "&fq=entity_id%3A".$entity_id;
					$and .= "&fq=content%3A%22".urlencode(implode('" OR "',$keysearch))."%22";
	    			$and .= "&fq=is_sentiment%3A".$sentiment_id;
        			$and .= '&fq=is_mediatype%3A1+OR+2';
					if(count($andsearch)>0){
						foreach($andsearch as $x=>$y){
							$and .='&fq=content%3A%22'.urlencode($y).'%22';
						}
					}
					if(count($exsearch)>0){
						foreach($exsearch as $x=>$y){
							$and .='&fq=-content%3A%22'.urlencode($y).'%22';
						}
					}

	    			$query = "select?q=*%3A*".$and."&rows=0&wt=json&indent=true&facet=true";
	    			$query2 = "select?q=*%3A*".$and."&rows=10&wt=json&indent=true";

	    			$tmp = $n->get_by_rawurl($query);
	    			$tmp2 = $n->get_by_rawurl($query2);
	    			// Zend_Debug::dump($tmp);die();

	    			if($tmp["response"]["numFound"]>0){
        				$data[$entity_id]["sentiment"][$sentiment_name]["count"] = $tmp2["response"]["numFound"];
        				$data[$entity_id]["sentiment"][$sentiment_name]["data"] = $tmp2["response"]["docs"];
	    				$jsdata[$entity_id]["sentiment"][$i]["value"] = (int)$tmp["response"]["numFound"];
	    				$jsdata[$entity_id]["sentiment"][$i]["name"] = $sentiment_name;
	    				$jsdata[$entity_id]["sentiment"][$i]["color"] = "#000000";
	    				$i++;
	    			}
        		}
    			// $i = 0;
        		// foreach ($sentiment as $sentiment_id => $sentiment_name) {

    			$and = "";
				$and .= "&fq=content%3A%5B*+TO+*%5D";
    			$and .= "&fq=content_date%3A%5B".$params["startdate"]."T00%3A00%3A00Z+TO+".$params["enddate"]."T23%3A59%3A59Z%5D";
    			$and .= "&fq=entity_id%3A".$entity_id;
    			$and .= "&fq=content%3A%22".urlencode(implode('" OR "',$keysearch))."%22";
				$and .= '&fq=is_mediatype%3A1+OR+2';
				if(count($andsearch)>0){
					foreach($andsearch as $x=>$y){
						$and .='&fq=content%3A%22'.urlencode($y).'%22';
					}
				}
				if(count($exsearch)>0){
					foreach($exsearch as $x=>$y){
						$and .='&fq=-content%3A%22'.urlencode($y).'%22';
					}
				}

    			$query = "select?q=*%3A*".$and."&rows=0&wt=json&indent=true&facet=true&facet.field=bundle";
				// Zend_Debug::dump(urldecode($query));die();

    			$tmp = $n->get_by_rawurl($query);
    			// Zend_Debug::dump($tmp);die();

    			$tmp1 = array();
        		// $jsdata[$entity_id]["bundle"] = array();
    			$i = 0;
    			foreach($tmp["facet_counts"]["facet_fields"]["bundle"] as $k=>$v){
    				if($k%2==1 && $v>0){
    					$jsdata[$entity_id]["bundle"][$i]["value"] = $v;
    					$jsdata[$entity_id]["bundle"][$i]["name"] = $tmp["facet_counts"]["facet_fields"]["bundle"][$k-1];
    					$data[$entity_id]["bundle"][$tmp["facet_counts"]["facet_fields"]["bundle"][$k-1]] = $v;
    					$i++;
    				}

    			}

        	}

        	// Zend_Debug::dump($jsdata[2]["sentiment"]);die();
        }
*/
        if(!isset($params["startdate"])) {
            $params["startdate"] = date("Y-m-d", strtotime('yesterday'));
        }
        if(!isset($params["enddate"])) {
            $params["enddate"] = date("Y-m-d");
        }
    	
    	// Zend_Debug::dump($jsdata);die();
        
        $this->view->params = $params;
        $this->view->datasource = $datasource;
        $this->view->data = $data;
        $this->view->jsdata = $jsdata;
        $this->view->logo = $g->get_logo();
	}

    public function v2Action(){
    	$params = $this->getRequest()->getParams();
		// Zend_Debug::dump($params);die();
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        //-== JS
        $this->view->headScript()->appendFile('/assets/core/global/plugins/echarts/echarts.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript()->appendFile('http://maps.google.com/maps/api/js?language=id');
        $this->view->headScript()->appendFile('/assets/core/plugins/gmaps/gmaps.js');
        $this->view->headScript()->appendFile('/assets/core/js/d3.v3.min.js');
		
        $n = new Pmas_Model_Withsolr();

		$data = array();
		$jsdata = array();
		if($params['keysearch']!=null & $params['keysearch']!=""){
			$tmpa = explode(",",$params['keysearch']);
			$params['key'] = $tmpa;
			foreach($tmpa as $k=>$v){
				//entity
				// Zend_Debug::dump($data);//die();
				$data[$v]['tag'] = "key".$k;
				$data[$v]['entity'] = array();
				$jsdata[$v]['entity'] = array();
				$start = 0;
				$offset = 100;
				$tmp = array();
				while(true){
					$and = "&fq=content%3A".$v;
					if($params['andsearch']!=null & $params['andsearch']!=""){
						$tmpb = explode(",",$params['andsearch']);
						foreach($tmpb as $vb){
							$and .= "&fq=content%3A".$vb;
						}
					}
					if($params['exsearch']!=null & $params['exsearch']!=""){
						$tmpb = explode(",",$params['exsearch']);
						foreach($tmpb as $vb){
							$and .= "&fq=-content%3A".$vb;
						}
					}
					$and .= "&fq=content_date:[".$params['startdate']."T00:00:00.000Z%20TO%20".$params['enddate']."T23:59:59.999Z]";
					$query = "select?q=*%3A*".$and."&sort=entity_type+asc&start=".$start."&rows=".($start+$offset)."&fl=entity_type&wt=json";
					$query .= "&indent=true&group=true&group.field=entity_type";
					$ent = $n->get_by_rawurl($query);
					// Zend_Debug::dump($ent);die($query);
					if(isset($ent['grouped']['entity_type']['groups']) & count($ent['grouped']['entity_type']['groups'])>0){
						foreach($ent['grouped']['entity_type']['groups'] as $v2){
							if($v2['groupValue']!=null){
								$tmp[$v2['groupValue']] = array(
									'name'=> strtoupper($v2['groupValue']),
									'val' => $v2['doclist']["numFound"]
								);
								$jsdata[$v]['entity'][] = array(
									'value'=>$v2['doclist']["numFound"],
									'name'=>$v2['groupValue'],
									'itemStyle'=>array(
										'normal'=>array(
											'label'=>array(
												'textStyle'=>array(
													'color'=>'black'
												)
											)
										)
									)
								);
							}
						}
					}
					if(count($ent['grouped']['entity_type']['groups'])==$offset){
						$start += $offset;
					}else{
						break;
					}
				}
				// Zend_Debug::dump($tmp);//die();
				$data[$v]['entity'] = $tmp;
				// Zend_Debug::dump($data);
				
				//bundle
				// Zend_Debug::dump($data);//die();
				$data[$v]['bundle'] = array();
				$jsdata[$v]['bundle'] = array();
				$start = 0;
				$offset = 100;
				$tmp = array();
				while(true){
					$and = "&fq=content%3A".$v;
					if($params['andsearch']!=null & $params['andsearch']!=""){
						$tmpb = explode(",",$params['andsearch']);
						foreach($tmpb as $vb){
							$and .= "&fq=content%3A".$vb;
						}
					}
					if($params['exsearch']!=null & $params['exsearch']!=""){
						$tmpb = explode(",",$params['exsearch']);
						foreach($tmpb as $vb){
							$and .= "&fq=-content%3A".$vb;
						}
					}
					$and .= "&fq=content_date:[".$params['startdate']."T00:00:00.000Z%20TO%20".$params['enddate']."T23:59:59.999Z]";
					$query = "select?q=*%3A*".$and."&sort=bundle+asc&start=".$start."&rows=".($start+$offset)."&fl=bundle&wt=json";
					$query .= "&indent=true&group=true&group.field=bundle";
					$ent = $n->get_by_rawurl($query);
					// Zend_Debug::dump($ent);die($query);
					if(isset($ent['grouped']['bundle']['groups']) & count($ent['grouped']['bundle']['groups'])>0){
						foreach($ent['grouped']['bundle']['groups'] as $v2){
							if($v2['groupValue']!=null){
								$tmp[$v2['groupValue']] = array(
									'name'=> strtoupper($v2['groupValue']),
									'val' => $v2['doclist']["numFound"]
								);
								$jsdata[$v]['bundle'][] = array(
									'value'=>$v2['doclist']["numFound"],
									'name'=>$v2['groupValue'],
									'itemStyle'=>array(
										'normal'=>array(
											'label'=>array(
												'textStyle'=>array(
													'color'=>'black'
												)
											),
											'labelLine'=>array(
												'length'=>3
											)
										)
									)
								);
							}
						}
					}
					if(count($ent['grouped']['bundle']['groups'])==$offset){
						$start += $offset;
					}else{
						break;
					}
				}
				// Zend_Debug::dump($tmp);//die();
				$data[$v]['bundle'] = $tmp;
				// Zend_Debug::dump($data);
				
				//sentiment
				// Zend_Debug::dump($data);//die();
				$data[$v]['sentiment'] = array();
				$jsdata[$v]['sentiment'] = array();
				$start = 0;
				$offset = 100;
				$tmp = array();
				while(true){
					$and = "&fq=content%3A".$v;
					if($params['andsearch']!=null & $params['andsearch']!=""){
						$tmpb = explode(",",$params['andsearch']);
						foreach($tmpb as $vb){
							$and .= "&fq=content%3A".$vb;
						}
					}
					if($params['exsearch']!=null & $params['exsearch']!=""){
						$tmpb = explode(",",$params['exsearch']);
						foreach($tmpb as $vb){
							$and .= "&fq=-content%3A".$vb;
						}
					}
					$and .= "&fq=content_date:[".$params['startdate']."T00:00:00.000Z%20TO%20".$params['enddate']."T23:59:59.999Z]";
					$query = "select?q=*%3A*".$and."&sort=is_sentiment+asc&start=".$start."&rows=".($start+$offset)."&fl=is_sentiment&wt=json";
					$query .= "&indent=true&group=true&group.field=is_sentiment";
					$ent = $n->get_by_rawurl($query);
					// Zend_Debug::dump($ent);die($query);
					if(isset($ent['grouped']['is_sentiment']['groups']) & count($ent['grouped']['is_sentiment']['groups'])>0){
						foreach($ent['grouped']['is_sentiment']['groups'] as $v2){
							if($v2['groupValue']!=null){
								$tmp[$v2['groupValue']] = array(
									'name'=> strtoupper($v2['groupValue']),
									'val' => $v2['doclist']["numFound"]
								);
								$jsdata[$v]['sentiment'][] = array(
									'value'=>$v2['doclist']["numFound"],
									'name'=>($v2['groupValue']===1)?"negative":(($v2['groupValue']===2)?"positive":"netral"),
									// 'name'=>$v2['groupValue'],
									'itemStyle'=>array(
										'normal'=>array(
											'label'=>array(
												'textStyle'=>array(
													'color'=>'black'
												)
											),
											'labelLine'=>array(
												'length'=>3
											)
										)
									)
								);
							}
						}
					}
					if(count($ent['grouped']['is_sentiment']['groups'])==$offset){
						$start += $offset;
					}else{
						break;
					}
				}
				// Zend_Debug::dump($tmp);//die();
				$data[$v]['sentiment'] = $tmp;
				// Zend_Debug::dump($data);
			}
			// Zend_Debug::dump($data);die();
			// Zend_Debug::dump($jsdata);die();
		}
        if(!isset($params["startdate"])) {
            $params["startdate"] = date("Y-m-d", strtotime('yesterday'));
        }
        if(!isset($params["enddate"])) {
            $params["enddate"] = date("Y-m-d");
        }

        $this->view->params = $params;
        $this->view->data = $data;
        $this->view->jsdata = $jsdata;
	}


}