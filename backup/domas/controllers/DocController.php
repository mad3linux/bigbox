<?php

/**
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>,Jul 15, 2014
 *
 */

class Domas_DocController extends Zend_Controller_Action {

    public function init() {
        
        /* Initialize action controller here */
    }

    public function sendingAction() {
        $params = $this->getRequest()->getParams();
        $c2 = new CMS_LIB_parse();
        $content = $params['ht'];
        $content = $c2->remove($content, "<!--", "-->");
        $content = $c2->remove($content, "<style", "</style>");
        $content = $c2->remove($content, "<script", "</script>");
        $content = preg_replace('/\s+/', ' ', $content);
        //Zend_Debug::dump($content); 
        $dom_1 = new Zend_Dom_Query($content);
        $rc = $dom_1->query('li .active');

        foreach($rc as $v) {
            $tcont[] = $v->getAttribute("data-id");
        }
        $cd = new Domas_Model_Generalpendem();
        $ic = $cd->get_icons();
        //     Zend_Debug::dump($params); die();
        if(count($tcont)> 0) {
            $cc = new Pendem_Model_Withsolr();
            if(!isset($params['page'])) {
                $cnt = 0;
            } else {
                $cnt = (int) $params['page'] * 100;
            }
            //$tid, $start = 0, $limit = 100, $src=""
            $data = $cc->filter_tax($tcont, $cnt, 100, $params['src']);
            $var = $data->response->docs;
        }
        //  Zend_Debug::dump($var);
        //  die();
        if(!isset($params['page'])) {
            echo '	<!-- BEGIN PORTLET-->
            
        <div class="row">
        <div class="col-md-12">

            <div id="ajax-loader" style="z-index: 9999; position: absolute; left: 0px; width: 100%; opacity: 0.5; overflow: hidden; display: none;">
                <img style="margin: 15% auto; opacity: 1; display: block;" src="/assets/core/img/gears.gif">
            </div>

            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h4 class="page-title">
                Business Metadata  <small></small>
            </h4>
            
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
         <div class="row">
        <div class="col-md-12" id="filtered">
        
        <div class="portlet">
            <div class="portlet-title line">
            
                <div class="caption">
                    <i style="font-size:30px" class="fa fa-filter"></i>Filtered Results
                </div>
                <div class="tools">
                    <a href="" class="collapse"></a>
                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                    <a href="" class="reload"></a>
                    <a href="" class="remove"></a>
                </div>
                <div class="col-md-6">
                        <div class="input-group">

                            <span class="input-group-addon">
                                <i class="fa fa-search"></i>
                            </span>
                            <input name="keyw" id="searching" class="form-control" placeholder="SearchContents">

                        </div>


                    </div>
            </div>
            
            <div class="portlet-body" id="chats">
                <div class="scrollerxx" style="height:600px; overflow-x:scroll" data-always-visible="1" data-rail-visible1="1">
            <ul class="chats forscroll">';
        }
        $i = 1;

        foreach($var as $z1) {
            $string = preg_replace('/\s+?(\S+)?$/', '', substr($z1['content'], 0, 300));
            $label =($z1['label'] != "")? $z1['label'] : $z1['bundle_name'];
            echo ' <li class="in">
                            
                          
                            
                        <i style="font-size:20px" class="fa ' . $ic[$z1['bundle']] . '"></i>';
            echo '<div class="message">
<div class="bootstrap-tagsinput">';

            foreach($z1['taxonomy_names'] as $kz => $z) {
                echo '<span class="tag label label-info">' . $z . '<span class="rem" data-role="remove" data-id="' . $z1['id'] . '" data-tid="' . $z1['tid'][$kz] . '"></span></span>';
            }
            echo '</div><br>';
            echo($i + (int) $cnt). '. 

                            <a href="#">' . $label . '</a>
                            <span class="datetime"><!--
                                        <a href="javascript:;" data-original-title="asdasd" class="btn btn-circle btn-icon-only green pull-right">
                                                                <i class="fa fa-save"></i>
                                                            </a>-->
                                                            <a href="javascript:;" id="' . $z1['id'] . '" class="btn btn-circle btn-icon-only green pull-right tname">
                                                                <i class="fa fa-eye"></i>
                                                            </a>
                            </span>
                            <span class="body">' . $string . '

                            </span>
                        </div>
                    </li>';
            $i ++;
        }
        // if(!isset($params['page'])){$params['page']=1;}
        $page = (int) $params['page'] + 1;
        if(count($var)> 0) {
            echo "<li class=\"in\"><a class='lastSelect' href='/domas/doc/filtering/page/" . $page . "?" . http_build_query(array('treecek' => $params['treecek'])). "'></li>";
        }
        if(!isset($params['page'])) {
            echo ' </ul>
                </div>

            </div>
        </div>
        <!--END PORTLET-->
        </div>
        <div id="rightcol" style="display:none" class="col-md-5"></div>
        </div>
        ';
        }
        echo '<script>
    jQuery(document).ready(function() {
        
        $("#searching").keypress(function(e) {
if (e.which == 13) {


    var ht = ($("#tags").html());

$.ajax({
    url: "/domas/doc/sending/src/"+$("#searching").val(),
    content: "html",
     type: "post",
    data: {ht:ht},
    beforeSend: function() {
        $("#ajax-loader").show();
    },
    complete: function(result) {


    },
    success: function(data) {
        $("#ajax-loader").hide();
        $(".page-content").html(data);
  
    },
    error: function() {

    }
})
    }
});
        
        $(".rem").click(function(){
        $(this).parent().hide();
        
        
        });
        var cont = $(\'#chats\');
        var list = $(\'.chats\', cont);
        $(\'.scroller\', cont).slimScroll({
            scrollTo: list.height(),
            height: \'200px\'
        });
        ';
        if(!isset($params['page'])) {
            echo '$(\'.forscroll\').jscroll({
                loadingHtml: \'<img src="/assets/core/img/ajax-loading.gif" alt="Loading" /> Loading...\',
                nextSelector: \'.lastSelect\',
                contentSelector: \'li\'

            });';
        }
        echo '$(".tname").click(function(e){

           e.preventDefault();
           var u =$(this).attr("id");


           var datay = {
            id: u
        };

        $.ajax({
            url: "/domas/doc/getid",
            content: "html",
            type: "get",
            data: datay,
            beforeSend: function() {
            },
            complete: function(result) {


            },
            success: function(data) {
                jQuery("#filtered").attr("class", "col-md-7");
                jQuery(\'#rightcol\').show();
                jQuery(\'#rightcol\').html(data);
            },
            error: function() {

            }
        });
    });

});
</script>
<style>

</style>

';
        die();
    }

    public function sendingxAction() {
        $params = $this->getRequest()->getParams();
        $c2 = new CMS_LIB_parse();
        $content = $params['ht'];
        $content = $c2->remove($content, "<!--", "-->");
        $content = $c2->remove($content, "<style", "</style>");
        $content = $c2->remove($content, "<script", "</script>");
        $content = preg_replace('/\s+/', ' ', $content);
        //Zend_Debug::dump($content); 
        $dom_1 = new Zend_Dom_Query($content);
        $rc = $dom_1->query('li .active');
        //   Zend_Debug::dump($rc);die();

        foreach($rc as $v) {
            $tcont[] = $v->getAttribute("data-id");
        }
        $cd = new Domas_Model_Generalpendem();
        $ic = $cd->get_icons();
        //     Zend_Debug::dump($params); die();
        if(count($tcont)> 0) {
            $cc = new Pendem_Model_Withsolr();
            if(!isset($params['page'])) {
                $cnt = 0;
            } else {
                $cnt = (int) $params['page'] * 100;
            }
            $data = $cc->filter_tax($tcont, $cnt);
            $var = $data->response->docs;
        }
        //  Zend_Debug::dump($var);
        //  die();
        if(!isset($params['page'])) {
            echo '	<!-- BEGIN PORTLET-->
            
        <div class="row">
        <div class="col-md-12">

            <div id="ajax-loader" style="z-index: 9999; position: absolute; left: 0px; width: 100%; opacity: 0.5; overflow: hidden; display: none;">
                <img style="margin: 15% auto; opacity: 1; display: block;" src="/assets/core/img/gears.gif">
            </div>

            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h4 class="page-title">
                Business Metadata  <small></small>
            </h4>
            
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
         <div class="row">
        <div class="col-md-12">
        <div class="portlet">
            <div class="portlet-title line">
                <div class="caption">
                    <i style="font-size:30px" class="fa fa-filter"></i>Filtered Results
                </div>
                <div class="tools">
                    <a href="" class="collapse"></a>
                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                    <a href="" class="reload"></a>
                    <a href="" class="remove"></a>
                </div>
            </div>
            <div class="portlet-body" id="chats">
                <div class="scrollerxx" style="height:600px; overflow-x:scroll" data-always-visible="1" data-rail-visible1="1">
            <ul class="chats forscroll">';
        }
        $i = 1;

        foreach($var as $z1) {
            //            $line = $body;
            //            if (preg_match('/^.{1,260}\b/s', $body, $match)) {
            //                $line = $match[0];
            //            }
            $string = preg_replace('/\s+?(\S+)?$/', '', substr($z1['content'], 0, 300));
            $label =($z1['label'] != "")? $z1['label'] : $z1['bundle_name'];
            echo ' <li class="in">
                            
                          
                            
                        <i style="font-size:20px" class="fa ' . $ic[$z1['bundle']] . '"></i>';
            echo '<div class="message">
<div class="bootstrap-tagsinput">';

            foreach($z1['taxonomy_names'] as $kz => $z) {
                echo '<span class="tag label label-info">' . $z . '<span class="rem" data-role="remove" data-id="' . $z1['id'] . '" data-tid="' . $z1['tid'][$kz] . '"></span></span>';
            }
            echo '</div><br>';
            echo($i + (int) $cnt). '. 

                            <a href="#" >' . $label . '</a>
                            <span class="datetime">
                                        <a href="javascript:;" data-original-title="asdasd" class="btn btn-circle btn-icon-only green pull-right">
                                                                <i class="fa fa-save"></i>
                                                            </a>
                                                            <a href="javascript:;" id="' . $z1['id'] . '" class="tname btn btn-circle btn-icon-only green pull-right">
                                                                <i class="fa fa-eye"></i>
                                                            </a>
                            </span>
                            <span class="body">' . $string . '

                            </span>
                        </div>
                    </li>';
            $i ++;
        }
        // if(!isset($params['page'])){$params['page']=1;}
        $page = (int) $params['page'] + 1;
        if(count($var)> 0) {
            echo "<li class=\"in\"><a class='lastSelect' href='/domas/doc/filtering/page/" . $page . "?" . http_build_query(array('treecek' => $params['treecek'])). "'></li>";
        }
        if(!isset($params['page'])) {
            echo ' </ul>
                </div>

            </div>
        </div>
        <!--END PORTLET-->
        </div>
        </div>
        ';
        }
        echo '<script>
    jQuery(document).ready(function() {
        $(".rem").click(function(){
        $(this).parent().hide();
        
        
        });
        var cont = $(\'#chats\');
        var list = $(\'.chats\', cont);
        $(\'.scroller\', cont).slimScroll({
            scrollTo: list.height(),
            height: \'200px\'
        });
        ';
        if(!isset($params['page'])) {
            echo '$(\'.forscroll\').jscroll({
                loadingHtml: \'<img src="/assets/core/img/ajax-loading.gif" alt="Loading" /> Loading...\',
                nextSelector: \'.lastSelect\',
                contentSelector: \'li\'

            });';
        }
        echo '$(".tname").click(function(e){

           e.preventDefault();
           var u =$(this).attr("id");


           var datay = {
            id: u
        };

        $.ajax({
            url: "/domas/doc/getid",
            content: "html",
            type: "get",
            data: datay,
            beforeSend: function() {
            },
            complete: function(result) {


            },
            success: function(data) {
                
                jQuery(\'#rightcol\').show();
                jQuery(\'#rightcol\').html(data);
            },
            error: function() {

            }
        });
    });

});
</script>
<style>

</style>

';
        die();
    }

    public function createnodeAction() {
        $params = $this->getRequest()->getParams();
        $params['old'] = str_replace(array('[', ']'), "", $params['old']);
        $params['old'] = preg_replace('/[0-9]+/', '', $params['old']);
        $params['text'] = str_replace(array('[', ']'), "", $params['text']);
        $params['text'] = preg_replace('/[0-9]+/', '', $params['text']);
        // Zend_Debug::dump($params); die();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if(isset($params['text'])) {
            $mdl = new Domas_Model_Generalpendem();
            $update = $mdl->add_node($params);
            if($update['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$update['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function filteringAction() {
        $params = $this->getRequest()->getParams();
        $cd = new Domas_Model_Generalpendem();
        $ic = $cd->get_icons();
        //     Zend_Debug::dump($params); die();
        if(count($params['treecek'])> 0) {
            $cc = new Pendem_Model_Withsolr();
            if(!isset($params['page'])) {
                $cnt = 0;
            } else {
                $cnt = (int) $params['page'] * 100;
            }
            $data = $cc->filter_tax($params['treecek'], $cnt);
            $var = $data->response->docs;
        }
        if(!isset($params['page'])) {
            echo '	<!-- BEGIN PORTLET-->
        <div class="portlet">
            <div class="portlet-title line">
                <div class="caption">
                    <i style="font-size:30px" class="fa fa-filter"></i>Filtered Results
                </div>
                <div class="tools">
                    <a href="" class="collapse"></a>
                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                    <a href="" class="reload"></a>
                    <a href="" class="remove"></a>
                </div>
            </div>
            <div class="portlet-body" id="chats">

                <div class="scrollerxx" style="height:600px; overflow:scroll" data-always-visible="1" data-rail-visible1="1">
                    <ul class="chats forscroll">';
        }
        $i = 1;

        foreach($var as $z1) {
            //            $line = $body;
            //            if (preg_match('/^.{1,260}\b/s', $body, $match)) {
            //                $line = $match[0];
            //            }
            $string = preg_replace('/\s+?(\S+)?$/', '', substr($z1['content'], 0, 300));
            $label =($z1['label'] != "")? $z1['label'] : $z1['bundle_name'];
            echo ' <li class="in">
                        <i style="font-size:20px" class="fa ' . $ic[$z1['bundle']] . '"></i>
                        <div class="message">' .($i + (int) $cnt). '. 

                            <a href="#" id="' . $z1['id'] . '" class="tname">' . $label . '</a>
                            <span class="datetime">

                            </span>
                            <span class="body">' . $string . '

                            </span>
                        </div>
                    </li>';
            $i ++;
        }
        // if(!isset($params['page'])){$params['page']=1;}
        $page = (int) $params['page'] + 1;
        if(count($var)> 0) {
            echo "<li class=\"in\"><a class='lastSelect' href='/domas/doc/filtering/page/" . $page . "?" . http_build_query(array('treecek' => $params['treecek'])). "'></li>";
        }
        if(!isset($params['page'])) {
            echo ' </ul>
                </div>

            </div>
        </div>
        <!--END PORTLET-->';
        }
        echo '<script>
    jQuery(document).ready(function() {
        var cont = $(\'#chats\');
        var list = $(\'.chats\', cont);
        $(\'.scroller\', cont).slimScroll({
            scrollTo: list.height(),
            height: \'200px\'
        });
        ';
        if(!isset($params['page'])) {
            echo '$(\'.forscroll\').jscroll({
                loadingHtml: \'<img src="/assets/core/img/ajax-loading.gif" alt="Loading" /> Loading...\',
                nextSelector: \'.lastSelect\',
                contentSelector: \'li\'

            });';
        }
        echo '$(".tname").click(function(e){

           e.preventDefault();
           var u =$(this).attr("id");


           var datay = {
            id: u
        };

        $.ajax({
            url: "/domas/doc/getid",
            content: "html",
            type: "get",
            data: datay,
            beforeSend: function() {
            },
            complete: function(result) {


            },
            success: function(data) {
                jQuery(\'#rightcol\').html(data);
            },
            error: function() {

            }
        });
    });

});
</script>

';
        die();
    }

    public function gethtmldataAction() {
        $cc = new Pendem_Model_Admin();
        $data = $cc->taxonomy_strc();
        $dd = new Pendem_Model_Withsolr();
        $gr_tax = $dd->tax_grouping();
        # Zend_Debug::dump($data);die();
        $this->_helper->layout->disableLayout();
        $colors = array("icon-state-success",
                        "icon-state-danger",
                        "icon-state-warning");
        $icons = array("fa fa-file",
                       "fa fa-folder",
                       "fa fa-desktop",
                       "fa fa-globe",
                       "fa fa-gear",
                       "fa-quote-left",
                       "fa fa-sitemap",
                       "fa fa-download");
        $colors[array_rand($colors)];
        //  echo '<ul>';
        //$icons[array_rand($icons)] . " " . $colors[array_rand($colors)] 

        foreach($data as $k => $v) {
            if($k == "TAGGING") {
                echo '<li class="nav-item" id="tags"><a href="javascript:;" class="nav-link nav-toggle">
											<i class="fa fa-home"></i>
											<span class="title">' . $k . '</span>
											<span class="arrow "></span>
										</a>';
                echo ' <ul class="sub-menu">';
                echo '<li class="sidebar-search-wrapper">
                                <form class="sidebar-search  "  method="POST">
                                    <a href="javascript:;" class="remove">
                                        <i class="icon-close"></i>
                                    </a>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search...">
                                        <span class="input-group-btn">
                                            <a href="javascript:;" class="btn">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </span>
                                    </div>
                                </form>
                             
                            </li>';

                foreach($v['key'] as $k2 => $v2) {
                    echo "<li class='nav-item' id='vid_" . $k2 . "' > 
                                   <a href='javascript:;' class='nav-link nav-toggle'>
											<i class='" . $icons[array_rand($icons)] . "'></i>
											<span class='title'>" . $data['TAGGING']['val'][$k2]['name'] . "</span>
											<span class='arrow'></span>
										</a>";
                    echo '<ul class="sub-menu">';

                    foreach($data['TAGGING']['key'][$k2] as $k3 => $v3) {
                        $cnt = 0;
                        if($gr_tax[$v3['tid']] != "") {
                            $cnt = $gr_tax[$v3['tid']];
                        }
                        if($v3['class_icon'] == "") {
                            $icon = $icons[array_rand($icons)];
                        } else {
                            $icon = $v3['class_icon'];
                        }
                        echo ' <a href="javascript:;" data-id="' . $v3['tid'] . '" class="tagclick icon-btn btn-circle">
                            <i class="' . $icon . '"></i>
                            <div> ' . $v3['name'] . '</div>
                            <span class="badge  badge-success"> ' . $cnt . '</span>
                        </a>';
                        //                        echo "<li class='nav-item active' data-vid='" . $k2 . "' id='" . $v3['tid'] . "' >
                        //                          <a href='#' class='nav-link '>
                        //                                        <i class='" . $v2['class'] . "'></i> " . $v3['name'] . "[" . $cnt . "]
                        //                                        
                        //                                    </a></li>";
                        //                        
                        //                        
                    }
                    echo '</ul>';
                    echo '</li>';
                }
                echo '</ul>';
                echo "</li>";
            } else if($k == "TAXONOMY") {
                //                echo "<li class='tlevel1' id='TAXONOMY'>" . $k;
                //                if(count($v['key'])> 0) {
                //                    echo ' <ul>';
                //
                //                    foreach($v['key'] as $k2 => $v2) {
                //                        echo "<li class='tlevel2' id='vid_" . $k2 . "'> <a href='javascript:;' class='nav-link nav-toggle'>
                //                            <i class='" . $icons[array_rand($icons)] . "'></i> " . $data['TAXONOMY']['val'][$k2]['name'] . "<span class='arrow'></span></a>";
                //                         if (count($data['TAXONOMY']['key'][$k2][0][0]) > 0) {
                //                        echo '<ul class="sub-menu" >';
                //
                //                        foreach($data['TAXONOMY']['key'][$k2][0][0] as $z3 => $v3) {
                //                            
                //                            echo "<li data-vid='" . $k2 . "' id='" . $v3['tid'] . "'>" . $v3['name'] . '[' . $gr_tax[$v3['tid']] . ']';
                //                            if(count($data['TAXONOMY']['key'][$k2][1][$v3['tid']])> 0) {
                //                                echo "<ul>";
                //
                //                                foreach($data['TAXONOMY']['key'][$k2][1][$v3['tid']] as $z4 => $v4) {
                //                                    //if ($z4 == $z3) {
                //                                    echo "<li data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}' data-vid='" . $k2 . "' id='" . $v4['tid'] . "'>" . $v4['name'] . '[' . $gr_tax[$v4['tid']] . ']';
                //                                    if(count($data['TAXONOMY']['key'][$k2][2][$v4['tid']])> 0) {
                //                                        echo "<ul>";
                //
                //                                        foreach($data['TAXONOMY']['key'][$k2][2][$v4['tid']] as $z5 => $v5) {
                //                                            echo "<li data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}'  data-vid='" . $k2 . "' id='" . $v5['tid'] . "'>" . $v5['name'] . '[' . $gr_tax[$v5['tid']] . ']';
                //                                            if(count($data['TAXONOMY']['key'][$k2][3][$v5['tid']])> 0) {
                //                                                echo "<ul>";
                //
                //                                                foreach($data['TAXONOMY']['key'][$k2][3][$v5['tid']] as $z6 => $v6) {
                //                                                    echo "<li data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}' data-vid='" . $k2 . "' id='" . $v6['tid'] . "'>" . $v6['name'] . '[' . $gr_tax[$v6['tid']] . ']';
                //                                                    if(count($data['TAXONOMY']['key'][$k2][4][$v6['tid']])> 0) {
                //                                                        echo "<ul>";
                //
                //                                                        foreach($data['TAXONOMY']['key'][$k2][3][$v6['tid']] as $z7 => $v7) {
                //                                                            echo "<li data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}'  data-vid='" . $k2 . "' id='" . $v7['tid'] . "'>" . $v7['name'] . '[' . $gr_tax[$v7['tid']] . ']';
                //                                                            if(count($data['TAXONOMY']['key'][$k2][4][$v7['tid']])> 0) {
                //                                                                echo "<ul>";
                //
                //                                                                foreach($data['TAXONOMY']['key'][$k2][3][$v7['tid']] as $z8 => $v8) {
                //                                                                    echo "<li data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}' data-vid='" . $k2 . "' id='" . $v8['tid'] . "'>" . $v8['name'] . '[' . $gr_tax[$v8['tid']] . ']';
                //                                                                    echo '</li>';
                //                                                                }
                //                                                                echo "</ul>";
                //                                                            }
                //                                                            echo '</li>';
                //                                                        }
                //                                                        echo "</ul>";
                //                                                    }
                //                                                    echo '</li>';
                //                                                }
                //                                                echo "</ul>";
                //                                            }
                //                                        }
                //                                        echo "</ul>";
                //                                    }
                //                                    echo '</li>';
                //                                }
                //                                echo "</ul>";
                //                            }
                //                            echo '</li>';
                //                    }
                //               echo '</ul>';
                //            }
                //          echo '</li>';
                //         }
                //       echo ' <ul>';
                //     echo "</li>";
            }
        }
        //    echo '</ul>';
        echo '<script>
jQuery(document).ready(function() {

$(".tagclick ").click(function(){
if(!$(this).hasClass("active")){
        $(this).addClass("active")
}else {
    $(this).removeClass("active");
}  

var ht = ($("#tags").html());

$.ajax({
    url: "/domas/doc/sending",
    content: "html",
     type: "post",
    data: {ht:ht},
    beforeSend: function() {
        $("#ajax-loader").show();
    },
    complete: function(result) {


    },
    success: function(data) {
        $("#ajax-loader").hide();
      $(".page-content").html(data);
  
    },
    error: function() {

    }
});

});
   
    

});
</script>';
        die();
    }

    public function gethtmldata2Action() {
        $cc = new Pendem_Model_Admin();
        $data = $cc->taxonomy_strc();
        $dd = new Pendem_Model_Withsolr();
        $gr_tax = $dd->tax_grouping();
        # Zend_Debug::dump($data);die();
        $this->_helper->layout->disableLayout();
        $colors = array("icon-state-success",
                        "icon-state-danger",
                        "icon-state-warning");
        $icons = array("fa fa-file",
                       "fa fa-folder",
                       "fa fa-desktop",
                       "fa fa-globe",
                       "fa fa-gear",
                       "fa-quote-left",
                       "fa fa-sitemap",
                       "fa fa-download");
        $colors[array_rand($colors)];
        //  echo '<ul>';
        //$icons[array_rand($icons)] . " " . $colors[array_rand($colors)] 

        foreach($data as $k => $v) {
            if($k == "TAGGING") {
                echo '<li class="nav-item" id="tags"><a href="javascript:;" class="nav-link nav-toggle">
											<i class="fa fa-home"></i>
											<span class="title">' . $k . '</span>
											<span class="arrow "></span>
										</a>';
                echo ' <ul class="sub-menu">';

                foreach($v['key'] as $k2 => $v2) {
                    echo "<li class='nav-item' id='vid_" . $k2 . "' > 
                                    <a href='javascript:;' class='nav-link nav-toggle'>
											<i class='" . $icons[array_rand($icons)] . "'></i>
											<span class='title'>" . $data['TAGGING']['val'][$k2]['name'] . "</span>
											<span class='arrow'></span>
										</a>";
                    echo '<ul class="sub-menu">';

                    foreach($data['TAGGING']['key'][$k2] as $k3 => $v3) {
                        $cnt = 0;
                        if($gr_tax[$v3['tid']] != "") {
                            $cnt = $gr_tax[$v3['tid']];
                        }
                        echo "<li class='nav-item active' data-vid='" . $k2 . "' id='" . $v3['tid'] . "' >
                          <a href='#' class='nav-link '>
                                        <i class='" . $v2['class'] . "'></i> " . $v3['name'] . "[" . $cnt . "]
                                        
                                    </a></li>";
                        
                        /*
                         echo "<li class='flat' data-vid='" . $k2 . "' id='" . $v3['tid'] . "'  data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}'><div class='ftext'>as</div></li>";
                         */
                    }
                    echo '</ul>';
                    echo '</li>';
                }
                echo '</ul>';
                echo "</li>";
            } else if($k == "TAXONOMY") {
                //                echo "<li class='tlevel1' id='TAXONOMY'>" . $k;
                //                if(count($v['key'])> 0) {
                //                    echo ' <ul>';
                //
                //                    foreach($v['key'] as $k2 => $v2) {
                //                        echo "<li class='tlevel2' id='vid_" . $k2 . "'> <a href='javascript:;' class='nav-link nav-toggle'>
                //                            <i class='" . $icons[array_rand($icons)] . "'></i> " . $data['TAXONOMY']['val'][$k2]['name'] . "<span class='arrow'></span></a>";
                //                         if (count($data['TAXONOMY']['key'][$k2][0][0]) > 0) {
                //                        echo '<ul class="sub-menu" >';
                //
                //                        foreach($data['TAXONOMY']['key'][$k2][0][0] as $z3 => $v3) {
                //                            
                //                            echo "<li data-vid='" . $k2 . "' id='" . $v3['tid'] . "'>" . $v3['name'] . '[' . $gr_tax[$v3['tid']] . ']';
                //                            if(count($data['TAXONOMY']['key'][$k2][1][$v3['tid']])> 0) {
                //                                echo "<ul>";
                //
                //                                foreach($data['TAXONOMY']['key'][$k2][1][$v3['tid']] as $z4 => $v4) {
                //                                    //if ($z4 == $z3) {
                //                                    echo "<li data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}' data-vid='" . $k2 . "' id='" . $v4['tid'] . "'>" . $v4['name'] . '[' . $gr_tax[$v4['tid']] . ']';
                //                                    if(count($data['TAXONOMY']['key'][$k2][2][$v4['tid']])> 0) {
                //                                        echo "<ul>";
                //
                //                                        foreach($data['TAXONOMY']['key'][$k2][2][$v4['tid']] as $z5 => $v5) {
                //                                            echo "<li data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}'  data-vid='" . $k2 . "' id='" . $v5['tid'] . "'>" . $v5['name'] . '[' . $gr_tax[$v5['tid']] . ']';
                //                                            if(count($data['TAXONOMY']['key'][$k2][3][$v5['tid']])> 0) {
                //                                                echo "<ul>";
                //
                //                                                foreach($data['TAXONOMY']['key'][$k2][3][$v5['tid']] as $z6 => $v6) {
                //                                                    echo "<li data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}' data-vid='" . $k2 . "' id='" . $v6['tid'] . "'>" . $v6['name'] . '[' . $gr_tax[$v6['tid']] . ']';
                //                                                    if(count($data['TAXONOMY']['key'][$k2][4][$v6['tid']])> 0) {
                //                                                        echo "<ul>";
                //
                //                                                        foreach($data['TAXONOMY']['key'][$k2][3][$v6['tid']] as $z7 => $v7) {
                //                                                            echo "<li data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}'  data-vid='" . $k2 . "' id='" . $v7['tid'] . "'>" . $v7['name'] . '[' . $gr_tax[$v7['tid']] . ']';
                //                                                            if(count($data['TAXONOMY']['key'][$k2][4][$v7['tid']])> 0) {
                //                                                                echo "<ul>";
                //
                //                                                                foreach($data['TAXONOMY']['key'][$k2][3][$v7['tid']] as $z8 => $v8) {
                //                                                                    echo "<li data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}' data-vid='" . $k2 . "' id='" . $v8['tid'] . "'>" . $v8['name'] . '[' . $gr_tax[$v8['tid']] . ']';
                //                                                                    echo '</li>';
                //                                                                }
                //                                                                echo "</ul>";
                //                                                            }
                //                                                            echo '</li>';
                //                                                        }
                //                                                        echo "</ul>";
                //                                                    }
                //                                                    echo '</li>';
                //                                                }
                //                                                echo "</ul>";
                //                                            }
                //                                        }
                //                                        echo "</ul>";
                //                                    }
                //                                    echo '</li>';
                //                                }
                //                                echo "</ul>";
                //                            }
                //                            echo '</li>';
                //                    }
                //               echo '</ul>';
                //            }
                //          echo '</li>';
                //         }
                //       echo ' <ul>';
                //     echo "</li>";
            }
        }
        //    echo '</ul>';
        die();
    }

    public function gethtmlAction() {
        $cc = new Domas_Model_Admin();
        $data = $cc->taxonomy_strc();
        //$dd = new Pendem_Model_Withsolr();
        //$gr_tax = $dd->tax_grouping();
        # Zend_Debug::dump($data);die();
        $this->_helper->layout->disableLayout();
        $colors = array("icon-state-success",
                        "icon-state-danger",
                        "icon-state-warning");
        $icons = array("fa fa-file",
                       "fa fa-folder",
                       "fa fa-desktop",
                       "fa fa-globe",
                       "fa fa-gear",
                       "fa-quote-left",
                       "fa fa-sitemap",
                       "fa fa-download");
        $colors[array_rand($colors)];
        echo '<ul>';

        foreach($data as $k => $v) {
            if($k == "TAGGING") {
                echo "<li class='level1' id='tags' data-jstree='{
                \"icon\":\"fa fa-tags " . $colors[array_rand($colors)] . "\"}'>  " . $k;
                echo ' <ul>';

                foreach($v['key'] as $k2 => $v2) {
                    echo "<li class='level2' id='vid_" . $k2 . "' data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}'>     " . $data['TAGGING']['val'][$k2]['name'];
                    echo '<ul>';

                    foreach($data['TAGGING']['key'][$k2] as $k3 => $v3) {
                        $cnt = 0;
                        if($gr_tax[$v3['tid']] != "") {
                            //  $cnt = $gr_tax[$v3['tid']];
                        }
                        echo "<li data-vid='" . $k2 . "' id='" . $v3['tid'] . "'  data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " \"}'>" . $v3['name'] . '</li>';
                        
                        /*
                         echo "<li class='flat' data-vid='" . $k2 . "' id='" . $v3['tid'] . "'  data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}'><div class='ftext'>as</div></li>";
                         */
                    }
                    echo '</ul>';
                    echo '</li>';
                }
                echo '</ul>';
                echo "</li>";
            } else if($k == "TAXONOMY") {
                echo "<li class='tlevel1' id='TAXONOMY' data-jstree='{\"icon\":\"fa fa-sitemap " . $colors[array_rand($colors)] . "\"}'>" . $k;
                if(count($v['key'])> 0) {
                    echo ' <ul>';

                    foreach($v['key'] as $k2 => $v2) {
                        echo "<li class='tlevel2' id='vid_" . $k2 . "' data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}'>" . $data['TAXONOMY']['val'][$k2]['name'];
                        // if (count($data['TAXONOMY']['key'][$k2][0][0]) > 0) {
                        echo '<ul>';

                        foreach($data['TAXONOMY']['key'][$k2][0][0] as $z3 => $v3) {
                            echo "<li data-vid='" . $k2 . "' id='" . $v3['tid'] . "'  data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}'>" . $v3['name'] . '';
                            if(count($data['TAXONOMY']['key'][$k2][1][$v3['tid']])> 0) {
                                echo "<ul>";

                                foreach($data['TAXONOMY']['key'][$k2][1][$v3['tid']] as $z4 => $v4) {
                                    //if ($z4 == $z3) {
                                    echo "<li data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}' data-vid='" . $k2 . "' id='" . $v4['tid'] . "'>" . $v4['name'] . '';
                                    if(count($data['TAXONOMY']['key'][$k2][2][$v4['tid']])> 0) {
                                        echo "<ul>";

                                        foreach($data['TAXONOMY']['key'][$k2][2][$v4['tid']] as $z5 => $v5) {
                                            echo "<li data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}'  data-vid='" . $k2 . "' id='" . $v5['tid'] . "'>" . $v5['name'] . '';
                                            if(count($data['TAXONOMY']['key'][$k2][3][$v5['tid']])> 0) {
                                                echo "<ul>";

                                                foreach($data['TAXONOMY']['key'][$k2][3][$v5['tid']] as $z6 => $v6) {
                                                    echo "<li data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}' data-vid='" . $k2 . "' id='" . $v6['tid'] . "'>" . $v6['name'] . '';
                                                    if(count($data['TAXONOMY']['key'][$k2][4][$v6['tid']])> 0) {
                                                        echo "<ul>";

                                                        foreach($data['TAXONOMY']['key'][$k2][3][$v6['tid']] as $z7 => $v7) {
                                                            echo "<li data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}'  data-vid='" . $k2 . "' id='" . $v7['tid'] . "'>" . $v7['name'] . '[' . $gr_tax[$v7['tid']] . ']';
                                                            if(count($data['TAXONOMY']['key'][$k2][4][$v7['tid']])> 0) {
                                                                echo "<ul>";

                                                                foreach($data['TAXONOMY']['key'][$k2][3][$v7['tid']] as $z8 => $v8) {
                                                                    echo "<li data-jstree='{\"icon\":\"" . $icons[array_rand($icons)] . " " . $colors[array_rand($colors)] . "\"}' data-vid='" . $k2 . "' id='" . $v8['tid'] . "'>" . $v8['name'] . '[' . $gr_tax[$v8['tid']] . ']';
                                                                    echo '</li>';
                                                                }
                                                                echo "</ul>";
                                                            }
                                                            echo '</li>';
                                                        }
                                                        echo "</ul>";
                                                    }
                                                    echo '</li>';
                                                }
                                                echo "</ul>";
                                            }
                                        }
                                        echo "</ul>";
                                    }
                                    echo '</li>';
                                }
                                echo "</ul>";
                            }
                            echo '</li>';
                        }
                        echo '</ul>';
                    }
                    echo '</li>';
                }
                echo ' <ul>';
                echo "</li>";
            }
        }
        echo '</ul>';
        die();
    }

    public function mappingAction() {
        $params = $this->getRequest()->getParams();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if(isset($params['text'])) {
            $mdl = new Pendem_Model_Withsolr();
            $update = $mdl->mapping_tags($params);
            if($update['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$update['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function getidAction() {
        $params = $this->getRequest()->getParams();
        $this->_helper->layout->disableLayout();
        $dd = new Domas_Model_Generalpendem();
        $icons = $dd->get_icons();
        $params['id'] = urldecode($params['id']);
        $cc = new Pendem_Model_Solrhierarchy();
        $data2 = $cc->get_by_id($params['id']);
        $get = $data2->response->docs[0];
        $e = new Pendem_Model_Admin();
        $data = $e->taxonomy_strc();
       ($get['label'] != "")? $label = $get['label'] : $label = strtoupper($get['bundle_name']);
        echo ' <form id="mapping"><div class="portlet box grey">
        <div class="portlet-title">
            <div class="caption">
                <i style="font-size:30px" class="fa ' . $icons[$get['bundle']] . '"></i>' .($label). '
            </div>
            <div class="actions">
                <a href="#" class="btn green submitmapping btn-sm"><i class="fa fa-pencil"></i> Update</a>
                <!--<a href="#" class="btn red btn-sm"><i class="fa fa-plus"></i> Add</a>-->

            </div>
        </div>
        <div class="portlet-body">

            <div class="row">
                <div class="col-md-4">
                    <ul class="ver-inline-menu tabbable margin-bottom-10">
                        <li class="active">
                            <a data-toggle="tab" href="#tab_1">
                                <i class="fa ' . $icons[$get['bundle']] . '"></i>ATTRS [' . strtoupper($get['bundle']). ']</a>
                                <span class="after">
                                </span>
                            </li>
                            <!--
                            <li class="">
                                <a data-toggle="tab" href="#tab_2"><i class="fa fa-tags"></i>TAGS</a>
                            </li>
                        -->
                        <li class="">
                            <a data-toggle="tab" href="#tab_3"><i class="fa fa-tags"></i>TAG/TAXONOMY</a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#tab_4"><i class="fa fa-group"></i>+ATTRIBUTES</a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#tab_5"><i class="fa fa-group"></i>STRUCTURES</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-8">
                    <div class="tab-content">
                        <div id="tab_1" class="tab-pane active">
                            <div id="accordion1" class="panel-group">';
        //  foreach ($data )	
        if(isset($get['entity_type'])&& $get['entity_type'] != "") {
            echo "<div class='panel panel-danger'>
                                    <div class='panel-heading'>
                                        <h4 class='panel-title'><b>Kategori :</b> " . $get['entity_type'] . "

                                        </h4>
                                    </div>
                                </div>";
        }
        if(isset($get['bundle'])&& $get['bundle'] != "") {
            echo "<div class='panel panel-warning'>
                                <div class='panel-heading'>
                                    <h4 class='panel-title'><b>Tipe :</b> " . $get['bundle'] . "

                                    </h4>
                                </div>
                            </div>";
        }
        if(isset($get['label'])&& $get['label'] != "") {
            echo "<div class='panel panel-success'>
                            <div class='panel-heading'>
                                <h4 class='panel-title'><b>Title :</b> " . $get['label'] . "

                                </h4>
                            </div>
                        </div>";
        }
        if(isset($get['bundle_name'])&& $get['bundle_name'] != "") {
            echo "<div class='panel panel-success'>
                        <div class='panel-heading'>
                            <h4 class='panel-title'><b>Name:</b>  " . $get['bundle_name'] . "

                            </h4>
                        </div>
                    </div>";
        }

        foreach($get as $k => $v) {
            $key = substr($k, 3);
            $cek = strpos($k, 'ss_');
            if($cek !== false) {
                echo "<div class='panel panel-success'>
                        <div class='panel-heading'>
                            <h4 class='panel-title'><b>" . $key . ":</b>  " . $get[$k] . "

                            </h4>
                        </div>
                    </div>";
            }
        }
        echo '<div class="panel panel-warning">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#content">
                        <b> Content </b></a>
                    </h4>
                </div>
                <div id="content" class="panel-collapse in" style="height: auto;">
                    <div class="panel-body"><a href="#" id="comments" data-type="textarea" data-pk="1" data-placeholder="Your comments here..." data-original-title="Update Contents">
                        <p>' . $get['content'] . '</p>
                    </a>   
                </div>
            </div>
        </div>';
        echo '</div>
    </div>


    <div id="tab_4" class="tab-pane">
        <div id="accordion2" class="panel-group">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a>
                            <b>Add Custom Atributes</b></a>
                        </h4>
                    </div>
                    <div id="content" class="panel-collapse in" style="height: auto;">
                        <div class="panel-body">
                            <div class="form-group row breadcrump" id="breadcrump1" data-id="1">

                                <div class="col-md-10">
                                    <div class="col-md-4">
                                        <input class="form-control input-sm" name="atr_key[]" placeholder="FieldName">
                                    </div>
                                    <div class="col-md-4">
                                        <input class="form-control input-sm" name="atr_val[]" placeholder="Value">

                                    </div>
                                    <div class="col-md-4">
                                        <label class="checkbox-inline">
                                            <input type="checkbox"  value=1 name="cek_val[]" > Indexed </label>
                                        </div>

                                    </div>

                                    <div class="col-md-2">
                                        <button type="button" class="btn grey addGC" onclick="addGC(this); return false;" data-act="addGC" data-id="1" style="padding: 5px;">Add</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <div id="tab_3" class="tab-pane">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a >
                                <b>Mapping Tags and Taxonomies</b></a>
                            </h4>
                        </div>
                        <div id="content" class="panel-collapse in" style="height: auto;">
                            <div class="panel-body">
                                <div id="tax4submitx">';
        echo '<ul>';

        foreach($data as $k => $v) {
            if($k == "TAGS") {
                echo "<li id='tags' data-jstree='{id :\"TAGS\"}'>" . $k;
                echo ' <ul>';

                foreach($v['key'] as $k2 => $v2) {
                    echo "<li id='vid_" . $k2 . "' data-jstree='{id :\"vid_" . $k2 . "\"}'>" . $data['TAGS']['val'][$k2]['name'];
                    echo '<ul>';

                    foreach($data['TAGS']['key'][$k2] as $k3 => $v3) {
                        echo "<li data-checkstate='checked'  data-vid='" . $k2 . "' id='" . $v3['tid'] . "'  data-jstree='{id :\"" . $v3['tid'] . "\"}'>" . $v3['name'] . '</li>';
                    }
                    echo '</ul>';
                    echo '</li>';
                }
                echo '</ul>';
                echo "</li>";
            } else if($k == "TAXONOMY") {
                echo "<li id='TAXONOMY' data-jstree='{id :\"TAXONOMY\"}'>" . $k;
                if(count($v['key'])> 0) {
                    echo ' <ul>';

                    foreach($v['key'] as $k2 => $v2) {
                        echo "<li id='vid_" . $k2 . "' data-jstree='{id :\"vid_" . $k2 . "\"}'>" . $data['TAXONOMY']['val'][$k2]['name'];
                        // if (count($data['TAXONOMY']['key'][$k2][0][0]) > 0) {
                        echo '<ul>';

                        foreach($data['TAXONOMY']['key'][$k2][0][0] as $z3 => $v3) {
                            echo "<li class='jstree-checked'  data-vid='" . $k2 . "' id='" . $v3['tid'] . "'  data-jstree='{id :\"" . $v3['tid'] . "\"}'>" . $v3['name'];
                            if(count($data['TAXONOMY']['key'][$k2][1][$v3['tid']])> 0) {
                                echo "<ul>";

                                foreach($data['TAXONOMY']['key'][$k2][1][$v3['tid']] as $z4 => $v4) {
                                    //if ($z4 == $z3) {
                                    echo "<li data-vid='" . $k2 . "' id='" . $v4['tid'] . "'>" . $v4['name'];
                                    if(count($data['TAXONOMY']['key'][$k2][2][$v4['tid']])> 0) {
                                        echo "<ul>";

                                        foreach($data['TAXONOMY']['key'][$k2][2][$v4['tid']] as $z5 => $v5) {
                                            echo "<li  data-vid='" . $k2 . "' id='" . $v5['tid'] . "'>" . $v5['name'];
                                            if(count($data['TAXONOMY']['key'][$k2][3][$v5['tid']])> 0) {
                                                echo "<ul>";

                                                foreach($data['TAXONOMY']['key'][$k2][3][$v5['tid']] as $z6 => $v6) {
                                                    echo "<li data-vid='" . $k2 . "' id='" . $v6['tid'] . "'>" . $v6['name'];
                                                    if(count($data['TAXONOMY']['key'][$k2][4][$v6['tid']])> 0) {
                                                        echo "<ul>";

                                                        foreach($data['TAXONOMY']['key'][$k2][3][$v6['tid']] as $z7 => $v7) {
                                                            echo "<li data-vid='" . $k2 . "' id='" . $v7['tid'] . "'>" . $v7['name'];
                                                            if(count($data['TAXONOMY']['key'][$k2][4][$v7['tid']])> 0) {
                                                                echo "<ul>";

                                                                foreach($data['TAXONOMY']['key'][$k2][3][$v7['tid']] as $z8 => $v8) {
                                                                    echo "<li data-vid='" . $k2 . "' id='" . $v8['tid'] . "'>" . $v8['name'];
                                                                    echo '</li>';
                                                                }
                                                                echo "</ul>";
                                                            }
                                                            echo '</li>';
                                                        }
                                                        echo "</ul>";
                                                    }
                                                    echo '</li>';
                                                }
                                                echo "</ul>";
                                            }
                                        }
                                        echo "</ul>";
                                    }
                                    echo '</li>';
                                }
                                echo "</ul>";
                            }
                            echo '</li>';
                        }
                        echo '</ul>';
                    }
                    echo '</li>';
                }
                echo ' <ul>';
                echo "</li>";
            }
        }
        echo '</ul>';
        echo '</div>
                                </div>
                            </div>
                        </div>

                    </div><!--end tab-->


                </div>
            </div>
        </div>
    </div></form>
    <script>';
        $js_array = json_encode($get['tid']);
        echo '

        var sbbc = ' . $js_array . ';
        console.log();
        var gamasidx2 = 1;
        var elem = 1;
        function addGC(me){
            var id = $(me).data(\'id\');
            var currow = $(\'body\').find(\'.breadcrump:last\');';
        echo 'var str = \'<div class="form-group row breadcrump" id="breadcrump\'+(parseInt(gamasidx2)+1)+\'" data-id="\'+(parseInt(gamasidx2)+1)+\'" >\'
            + \'<div class="col-md-10">\'
            + \'<div class="col-md-4">\'
            + \'<input   class="form-control input-sm"  name="atr_key[\'+parseInt(gamasidx2)+\']"  placeholder="FieldName" />\'
            + \'</div>\'
            + \'<div class="col-md-4">\'
            + \'<input   class="form-control input-sm"  name="atr_val[\'+parseInt(gamasidx2)+\']" placeholder="Value" />\'
            + \'</div>\'
            + \'<div class="col-md-4">\'
            + \'<label class="checkbox-inline">\'
            + \'<input type="checkbox"  value=1 name="cek_val[\'+parseInt(gamasidx2)+\']" > Indexed </label>\'
            + \'</div>\'

            + \'</div>\'
            + \'<div class="col-md-2">\'
            + \'<button type="button" class="btn red removeGC" onclick="removeGC(this); return false;" data-act="removeGC" data-id="\'+(parseInt(gamasidx2)+1)+\'" style="padding: 5px;">Remove</button>\'
            + \'</div>\'
            + \'</div>\';';
        echo 'gamasidx2++;
                                                                                    //console.log(str);
            currow.after(str);
        }

        function removeGC(me){
            var id = $(me).data(\'id\');
                                                                                    //console.log(id);
            $(\'body\').find(\'#breadcrump\'+id+\':first\').remove();
        }
        jQuery(document).ready(function() {
//            if (App.getURLParameter(\'mode\') == \'inline\') {
//                $.fn.editable.defaults.mode = \'inline\';
//                $(\'#inline\').attr("checked", true);
//                jQuery.uniform.update(\'#inline\');
//            } else {
//                $(\'#inline\').attr("checked", false);
//                jQuery.uniform.update(\'#inline\');
//            }

                                                                                                            //global settings 
//            $.fn.editable.defaults.inputclass = \'form-control\';
//            $.fn.editable.defaults.url = \'/post\';
//
//                                                                                                                    //ed
//            $(\'#comments\').editable({
//                showbuttons: \'bottom\'
//            });
            $("#fortags").select2({
                                                                                                            //tags: true
            })


            $(\'#tax4submitx\').jstree({
                "core": {

                    "themes": {
                        "responsive": false
                    },
                                                                                                                                    // so that create works
                    "check_callback": true
                },
                "types": {
                    "default": {
                        "icon": "fa fa-folder icon-state-warning icon-lg"
                    },
                    "file": {
                        "icon": "fa fa-file icon-state-warning icon-lg"
                    }
                },
         //  "state": {"key2": "4submit"},
                "plugins": ["checkbox", "types"]

            });
            $(".submitmapping").click(function(){

                var checked_ids = [];
                var raws = ($("#tax4submitx").jstree("get_checked", null, true));
                var mapp = {
                    formvar: $(\'form#mapping\').serialize(),
                    treecek:raws,
                    id: "' . $params['id'] . '"
                };
                $.ajax({
                    url: "/domas/doc/postmap",
                    content: \'html\',
                    type:\'post\',
                    data: mapp,
                    beforeSend: function() {
                    },
                    complete: function(result) {


                    },
                    success: function(data) {
                     //jQuery(\'#rightcol\').html(data);
                    },
                    error: function() {

                    }
                });
            });
            $("#tax4submitx").jstree("open_all");
            if (sbbc)
                $.each(sbbc, function(key, value) {
                    //alert( key + ": " + value );
                    if (value) {
                        $(\'#tax4submitx\').jstree(\'check_node\', $("#"+value));
                    }
                });
                                                                                                                                    //alert("ss");
            });


        </script>';
        die();
    }

    function searching($params) {
        if(isset($params['xx'])&& $params['xx'] != "") {
            $dd = new Pendem_Model_Withsolr();
            $data = $dd->searchtree($params['xx'], $start = 0, $limit = 500);
            $i = 0;
            $cd = new Domas_Model_Generalpendem();
            $icon = $cd->get_icons();

            foreach($data->response->docs as $v) {
                if($v['label'] != "") {
                    $label = $v['label'];
                } else {
                    $label = $v['bundle_name'];
                }
                $newx[$i]['id'] = $v['id'];
                $newx[$i]['text'] = $label;
                $newx[$i]['icon'] = "fa " . $icon[$v['bundle']];
                $i ++;
            }
            return $newx;
            $new = array();
            $table = array();
            $machine = array();
            $col = array();
            $schema = array();

            foreach($data->response->docs as $v) {
                switch($v['bundle']) {
                    case 'table' : case 'column' : case 'schema' : case 'machine' : $dd = $this->proc_col($v);
                    //                        $table = array_merge($table, $dd['table']);
                    //                        $machine = array_merge($machine, $dd['machine']);
                    //                        $schema = array_merge($schema, $dd['schema']);
                    //                        $col = array_merge($col, $dd['column']);
                    $table[] = $dd['table'];
                    $machine[] = $dd['machine'];
                    $schema[] = $dd['schema'];
                    $col[] = $dd['column'];
                    break;
                }
            }
        }
        $table =(array_map("unserialize", array_unique(array_map("serialize", $table))));
        $schema =(array_map("unserialize", array_unique(array_map("serialize", $schema))));
        $machine =(array_map("unserialize", array_unique(array_map("serialize", $machine))));
        $col =(array_map("unserialize", array_unique(array_map("serialize", $col))));
        $ncol = array();

        foreach($col as $v) {

            foreach($v as $k2 => $v2) {
                $i = 0;

                foreach($v2 as $k3 => $v3) {
                    $ncol[$k2][$i] = $v3;
                    //  $ncol[$k2][$k3]['children']=$ncol[$k3];
                    $i ++;
                }
            }
        }
        //Zend_Debug::dump($ncol);
        $ntab = array();
        $i = 0;

        foreach($table as $v) {

            foreach($v as $k2 => $v2) {
                //Zend_Debug::dump($v2);die();

                foreach($v2 as $k3 => $v3) {
                    $ntab[$k2][$k3] = $v3;
                    $ntab[$k2][$k3]['children'] = $ncol[$k3];
                }
            }
            $i ++;
        }
        $result = array(array("xid" => "data-application",
                        'bundle' => "data-application",
                        "text" => "Applications",
                        "parent" => "#",
                        "children" => true),
                        array("xid" => "data-sources",
                        "text" => "Data Sources",
                        'bundle' => 'data-sources',
                        "parent" => "#",
                        "children" =>$ntab),
                        array("xid" => "data-url",
                        "text" => "Html Document",
                        'bundle' => 'data-url',
                        "parent" => "#",
                        "children" => true));
        return($ntab);
    }

    public function jsontreeAction() {
        $result = array();
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        if(isset($params['xx'])&& $params['xx'] != "undefined") {
            header("Access-Control-Allow-Origin: *");
            header('Content-Type: application/json');
            $result = $this->searching($params);
            if(!is_array($result)) {
                $result = array();
            }
            echo json_encode($result);
            die();
        }
        $nc = new Pendem_Model_Solrhierarchy();
        if($params['nested'] != 1) {
            $result = array(array("id" => "data-application",
                            "icon" => "fa fa-check icon-state-success",
                            'bundle' => "data-application",
                            "text" => "Applications",
                            "parent" => "#",
                            "children" => true),
                            array("id" => "data-sources",
                            "icon" => "fa fa-warning icon-state-danger",
                            "text" => "Data Sources",
                            'bundle' => 'data-sources',
                            "parent" => "#",
                            "children" => true),
                            array("id" => "data-url",
                            "text" => "Html Document",
                            'bundle' => 'data-url',
                            "parent" => "#",
                            "children" => true),
                            array("id" => "data-documents",
                            "text" => "Documents",
                            'bundle' => 'data-documents',
                            "parent" => "#",
                            "children" => true));
        } else {
            $tran = array('data-sources' => 'machine',
                          'machine' => 'schema',
                          'schema' => 'table',
                          'table' => 'column');
            switch($params['bundle']) {
                case 'schema' : $id = $params['xid'] . "~*";
               ($id == 'data-sources~*')? $id = null : $id = $id;
                # echo $id; die();
                $list[0]['id'] = $params['xid'] . "~fq:table";
                $list[0]['bundle'] = 'fq:table';
                $list[0]['text'] = 'table';
                $list[0]['children'] = true;
                $list[1]['id'] = $params['xid'] . "~fq:function";
                $list[1]['bundle'] = 'fq:function';
                $list[1]['text'] = 'function';
                $list[1]['children'] = true;
                $list[2]['id'] = $params['xid'] . "~fq:procedure";
                $list[2]['bundle'] = 'fq:procedure';
                $list[2]['text'] = 'procedure';
                $list[2]['children'] = true;
                break;
                case 'fq:table' : case 'fq:function' : case 'fq:procedure' : $params['xid'] = str_replace("~" . 
                                                                                                          $params['bundle'], "", $params['xid']);
                $fqraw = explode(":", $params['bundle']);
                $id = $params['xid'] . "~*";
               ($id == 'data-sources~*')? $id = null : $id = $id;
                $data = $nc->get_bundle($fqraw[1], $id);
                $i = 0;

                foreach($data->response->docs as $v) {
                    $list[$i]['id'] = $v['id'];
                    $list[$i]['bundle'] = $v['bundle'];
                    $list[$i]['text'] = $v['bundle_name'];
                    $list[$i]['children'] = true;
                    $i ++;
                }
                break;
                case 'machine' : case 'table' : case 'data-sources' : $id = $params['xid'] . "~*";
               ($id == 'data-sources~*')? $id = null : $id = $id;
                # echo $id; die();
                $data = $nc->get_bundle($tran[$params['bundle']], $id);
                //  Zend_Debug::dump($data); die();
                $i = 0;

                foreach($data->response->docs as $v) {
                    $list[$i]['id'] = $v['id'];
                    $list[$i]['bundle'] = $v['bundle'];
                    $list[$i]['text'] = $v['bundle_name'];
                    $list[$i]['children'] = true;
                    $i ++;
                }
                break;
                case 'data-url' : $id = null;
                $data = $nc->get_bundle_group('url', $id);
                $i = 0;

                foreach($data->grouped->bundle_name->groups as $v) {
                    $list[$i]['id'] = urlencode($v['groupValue']);
                    $list[$i]['bundle'] = 'domain-name';
                    $list[$i]['text'] = urlencode($v['groupValue']);
                    $list[$i]['children'] = true;
                    $i ++;
                }
                break;
                case 'domain-name' : $data = $nc->get_by_bundle_name('url', urldecode($params['xid']));
                $i = 0;

                foreach($data->response->docs as $v) {
                    $list[$i]['id'] = urlencode($v['id']);
                    $list[$i]['bundle'] = 'domain-name';
                    $list[$i]['text'] = $v['label'];
                    $list[$i]['children'] = true;
                    $i ++;
                }
                break;
                case 'document' : $data = $nc->get_by_bundle_name('url', urldecode($params['xid']));
                Zend_Debug::dump($data);
                die();
                $i = 0;

                foreach($data->response->docs as $v) {
                    $list[$i]['id'] = urlencode($v['id']);
                    $list[$i]['bundle'] = 'domain-name';
                    $list[$i]['text'] = $v['label'];
                    $list[$i]['children'] = true;
                    $i ++;
                }
                break;
            }
            $result = $list;
        }
        if(!is_array($result)) {
            $result = array();
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function deltaxAction() {
        $params = $this->getRequest()->getParams();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if(isset($params['id'])) {
            $mdl = new Domas_Model_Generalpendem();
            $update = $mdl->del_node($params['id']);
            if($update['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$update['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function homeAction() {
    }

    public function indexrAction() {
        $this->view->headLink()->appendStylesheet('/assets/metronic454/global/plugins/simple-line-icons/simple-line-icons.min.css');
        $this->view->headLink()->appendStylesheet('/assets/metronic454/global/plugins/socicon/socicon.css');
        $this->view->headLink()->appendStylesheet('/assets/metronic454/global/plugins/bootstrap/css/bootstrap.min.css');
        $this->view->headLink()->appendStylesheet('/assets/metronic454/global/css/components.min.css');
        $this->view->headLink()->appendStylesheet('/assets/metronic454/global/css/plugins.min.css');
        $this->view->headLink()->appendStylesheet('https://swisnl.github.io/jQuery-contextMenu/dist/jquery.contextMenu.css');
        $this->view->headLink()->appendStylesheet('https://swisnl.github.io/jQuery-contextMenu/css/screen.css');
        $this->view->headLink()->appendStylesheet('/assets/core/css/components.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/infinitescroll/jquery.jscroll.js');
        $this->view->headScript()->appendFile('https://swisnl.github.io/jQuery-contextMenu/dist/jquery.contextMenu.js');
        $params = $this->getRequest()->getParams();
        $cz = new Domas_Model_Generalpendem();
        $this->view->sources = $cz->get_params_sources();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
    }

    public function mapgroupAction() {
        $params = $this->getRequest()->getParams();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if(isset($params['raw0'])&& count($params['raw0'])> 0) {
            $dd = new Domas_Model_Generalpendem();
            $tags = $dd->get_tags_for_insert($params['raw0']);
            $mdl = new Pendem_Model_Withsolarium();
            $update = $mdl->mapping_tags_groups($params, $tags);
            if($update['result'] === true) {
                $result = array('retCode' => '00',
                                'retMsg' =>$update['message'],
                                'result' => true,
                                'data' =>$_POST);
            } else {
                $result = array('retCode' => '11',
                                'retMsg' =>$update['message'],
                                'result' => false,
                                'data' =>$_POST);
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function mapAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-toastr/toastr.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-toastr/toastr.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/css/pendem.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.full.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-toastr/toastr.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2-bootstrap.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/infinitescroll/jquery.jscroll.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jstree/dist/themes/default/style.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jstree/dist/jstree_c.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootbox/bootbox.min.js');
        $params = $this->getRequest()->getParams();
        //    die("s");
        $cz = new Domas_Model_Generalpendem();
        $this->view->sources = $cz->get_params_sources();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
    }

    public function rawmapAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/css/pendem.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.full.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-toastr/toastr.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2-bootstrap.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/infinitescroll/jquery.jscroll.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jstree/dist/themes/default/style.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jstree/dist/jstree_c.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-toastr/toastr.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2-bootstrap.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/css/custom.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jquery-nestable/jquery.nestable.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/uniform/css/uniform.default.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.min.js');
        $params = $this->getRequest()->getParams();
        //    die("s");
        $cz = new Domas_Model_Generalpendem();
        $this->view->sources = $cz->get_params_sources();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
    }

    public function indexAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/morris/morris.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/fullcalendar/fullcalendar.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jqvmap/jqvmap/jqvmap.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/moment.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/morris/morris.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/morris/raphael-min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/counterup/jquery.waypoints.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/counterup/jquery.counterup.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/amcharts/amcharts/amcharts.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/amcharts/amcharts/serial.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/amcharts/amcharts/pie.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/amcharts/amcharts/radar.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/amcharts/themes/light.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/amcharts/themes/patterns.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/amcharts/themes/chalk.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/amcharts/ammap/ammap.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/amcharts/ammap/maps/js/worldLow.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/amcharts/amstockcharts/amstock.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/fullcalendar/fullcalendar.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/horizontal-timeline/horozontal-timeline.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/flot/jquery.flot.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/flot/jquery.flot.resize.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/flot/jquery.flot.categories.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery.sparkline.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jqvmap/jqvmap/jquery.vmap.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css');
        $this->view->headLink()->appendStylesheet('/assets/core/css/pendem.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/typeahead/typeahead.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.full.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2-bootstrap.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/infinitescroll/jquery.jscroll.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jstree/dist/themes/default/style.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jstree/dist/jstree_c.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins//bootstrap-tagsinput/bootstrap-tagsinput.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/typeahead/handlebars.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/typeahead/typeahead.bundle.min.js');
        $this->view->headScript()->appendFile('/assets/core/pages/scripts/components-bootstrap-tagsinput.min.js');
        $params = $this->getRequest()->getParams();
        //    die("s");
        $cz = new Domas_Model_Generalpendem();
        $this->view->sources = $cz->get_params_sources();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
    }

    public function serverAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/css/pendem.css');
        $this->view->headLink()->appendStylesheet('/assets/core/css/components.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/data-tables/DT_bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jstree/dist/themes/default/style.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jstree/dist/jstree_c.js');
        $params = $this->getRequest()->getParams();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
    }
}
