<?php


/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class Domas_PrabaeditorController extends Zend_Controller_Action {

    public function init() {
    }

    public function formbuilderAction() {
        //$this->_helper->layout->disableLayout ();
    }

    public function newAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($_POST); die();
        $this->_helper->layout->disableLayout();
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        $result = array('retCode' => '10',
                        'retMsg' => 'Invalid Parameter',
                        'result' => false,
                        'data' => null);
        if($_POST) {
            $str = '<?php 
			/* Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by PrabaC <info@prabatech.com>, ' . date('d.m.Y'). ' 
			*/';
            $path = str_replace('~', '/', $_POST['ppath']);
            $config = new Zend_Config_Ini(APPLICATION_PATH . 
                                          '/configs/application.ini', 'production');
            $config = $config->toArray();
            $connection = ssh2_connect($config['data']['server']['ip'], 22);
            if($connection) {
                if(ssh2_auth_password($connection, $config['data']['server']['user'], $config['data']['server']['pwd'])) {
                    $stream = ssh2_exec($connection, 'echo  "' . 
                                        $str . 
                                        '" >  ' . 
                                        $path . 
                                        "/" . 
                                        $_POST['title']);
                }
            }
            $result = array('retCode' => '00',
                            'retMsg' => 'succed',
                            'result' => true,
                            'data' =>$_POST);
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function sourceAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/codemirror/lib/codemirror.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/codemirror/theme/material.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/codemirror/theme/ambiance.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/codemirror/lib/codemirror.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/codemirror/lib/codemirror.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/codemirror/mode/php/php.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/codemirror/mode/htmlmixed/htmlmixed.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/codemirror/mode/javascript/javascript.js');
        // die("end");
    }

    public function sourcecodeAction() {
        $nc = new Prabacontent_Model_Params();
        $excl = $nc->exclude_file();
        if($_POST) {
            $this->_helper->layout->disableLayout();
            //die();
            parse_str($_POST['form'], $data);
            // Zend_Debug::dump($data);die();
            $str = str_replace('"', '\"', $_POST['source']);
            $str = str_replace('$', '\$', $str);
            $path = str_replace('~', '/', $data['path']);
            //Zend_Debug::dump($path);die();
            $config = new Zend_Config_Ini(APPLICATION_PATH . 
                                          '/configs/application.ini', 'production');
            $config = $config->toArray();
            $connection = ssh2_connect($config['data']['server']['ip'], 22);
            if($connection) {
                if(ssh2_auth_password($connection, $config['data']['server']['user'], $config['data']['server']['pwd'])) {
                    $stream = ssh2_exec($connection, 'echo  "' . 
                                        $str . 
                                        '" >  ' . 
                                        $path);
                }
            }
            $result = array('retCode' => '00',
                            'retMsg' => 'Code saved at ' .$path,
                            'result' => true,
                            'data' =>$_POST);
            header("Access-Control-Allow-Origin: *");
            header('Content-Type: application/json');
            echo json_encode($result);
            die();
            //$this->_redirect('/prabacontent/prabaeditor/sourcecode');
        }
        $params = $this->getRequest()->getParams();
        $key = "";
        $listapp = array(md5(implode('~',
                         array('server',
                         '127.0.0.1',
                         '',
                         '',
                         '')))=> array(APPLICATION_PATH,
                         '127.0.0.1',
                         '',
                         '',
                         ''),
                        );
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        if(isset($params['ajax'])) {
            $this->_helper->layout->disableLayout();
            // Zend_Debug::dump($listapp);//die();
            // Zend_Debug::dump($params);//die();
            $list = array();
            $counter = 1;
            if($params['ajax'] == '0') {

                foreach($listapp as $k => $v) {
                    $list[$counter - 1]['title'] = $v[0];
                    $list[$counter - 1]['key'] = $counter;
                    $list[$counter - 1]['folder'] = true;
                    $list[$counter - 1]['hideCheckbox'] = true;
                    $list[$counter - 1]['iconclass'] = "fa fa-home";
                    $list[$counter - 1]['tooltip'] = $v[0];
                    $line = scandir($v[0]);

                    foreach($line as $kk => $vv) {
                        if($vv == 'views' || $vv == 'modules' || $vv == 'models') {
                            if($kk > 1) {
                                if(is_dir($v[0] . "/" . $vv)) {
                                    $folder = true;
                                    $icon = "fa fa-folder";
                                } else {
                                    $folder = false;
                                    $icon = "fa fa-file";
                                }
                                $list[$counter - 1]['children'][] = array('title' =>$vv,
                                                                          'key' =>$counter.$idx,
                                                                          'folder' =>$folder,
                                                                          'hideCheckbox' => true,
                                                                          'iconclass' =>$icon,
                                                                          'tooltip' =>$str,
                                                                          'lazy' =>$folder,
                                                                          'md5' =>$k,
                                                                          'nd' => str_replace("/",
                                                                          "~",
                                                                         $v[0]. "/" .$vv));
                            }
                        }
                    }
                    $counter ++;
                }
            } else {
                if(isset($listapp[$params['ajax']])&& isset($params['node'])) {
                    // Zend_Debug::dump($params);//die();
                    $v = $listapp[$params['ajax']];
                    $line = scandir(str_replace("~", "/", $params['node']). 
                                                "");
                    //Zend_Debug::dump($line);die();
                    $idx = 0;

                    foreach($line as $kk => $vv) {
                        if($kk > 1&& !in_array($vv, $excl)) {
                            if(is_dir(str_replace("~", "/", $params['node']). "/" . $vv)) {
                                $folder = true;
                                $icon = "fa fa-folder";
                            } else {
                                $folder = false;
                                $icon = "fa fa-file";
                            }
                            $list[] = array('title' =>$vv,
                                            'key' =>$counter.$idx,
                                            'folder' =>$folder,
                                            'hideCheckbox' => true,
                                            'iconclass' =>$icon,
                                            'tooltip' =>$str,
                                            'lazy' =>$folder,
                                            'md5' =>$params['ajax'],
                                            'nd' => str_replace("/",
                                            "~",
                                           $params['node']). "~" .$vv);
                        }
                    }
                    // Zend_Debug::dump($list);die();
                    fclose($errorStream);
                    fclose($stream);
                    $counter ++;
                }
            }
            header("Access-Control-Allow-Origin: *");
            header('Content-Type: application/json');
            echo json_encode($list);
            die();
        } else if(isset($params['open'])) {
            #die("ss");
            $this->_helper->layout->disableLayout();
            $txt = "";
            if(isset($params['open'])&& isset($params['node'])) {
                //$v = $listapp[$params['open']];
                $txt = file_get_contents(str_replace("~", "/", $params['node']));
                //Zend_Debug::dump($txt);die();
            }
            die(htmlentities($txt));
        }
        $this->view->headLink()->appendStylesheet('/assets/core/global/phplib/fancytree/src/skin-lion/ui.fancytree.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/phplib/contextmenu/css/jquery.contextMenu.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/phplib/jquery-linedtextarea.css');
        $this->view->headScript()->appendFile('/assets/core/global/phplib/contextmenu/js/jquery.contextMenu-1.6.5.js');
        $this->view->headScript()->appendFile('/assets/core/global/phplib/fancytree/src/jquery.fancytree.js');
        $this->view->headScript()->appendFile('/assets/core/global/phplib/fancytree/src/jquery.fancytree.childcounter.js');
        $this->view->headScript()->appendFile('/assets/core/global/phplib/fancytree/src/jquery.fancytree.edit.js');
        $this->view->headScript()->appendFile('/assets/core/global/phplib/contextmenu/jquery.fancytree.contextMenu.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/codemirror/lib/codemirror.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/codemirror/theme/material.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/codemirror/theme/ambiance.css');
        $this->view->headScript()->appendFile('/assets/core/global/phplib/jquery-linedtextarea.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/codemirror/lib/codemirror.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/codemirror/lib/codemirror.css');
        //   $this->view->headScript()->appendFile('/assets/core/global/plugins/codemirror/mode/php/php.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/codemirror/mode/htmlmixed/htmlmixed.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/codemirror/mode/javascript/javascript.js');
        // die("end");
    }

    public function tableauAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        $dd = new Model_Prabasystemaddon();
        if(isset($params['act'])) {
            $data = $m2->get_portlet($params['id']);
            // Zend_Debug::dump($data); die();
        }
        $apis = $dd->get_api_grouping();
        if(!isset($params['row'])) {
            $params['row'] = 1;
        }
        // Zend_Debug::dump($apis); die();
        $this->view->lay = $m->get_layout();
        $this->view->apis = $apis;
        $this->view->data = $data;
        $this->view->apps = $m->get_apps();
        $this->view->ws = $m->get_workspace();
        $this->view->varams = $params;
        $this->view->classicon = $m->get_params('classicon');
        $this->view->area = $m->get_params('formarea');
    }

    public function builderhtmlAction() {
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        $dd = new Model_Prabasystemaddon();
        if(isset($params['act'])) {
            $data = $m2->get_portlet($params['id']);
            // Zend_Debug::dump($data); die();
        }
        $apis = $dd->get_api_grouping();
        if(!isset($params['row'])) {
            $params['row'] = 1;
        }
        // Zend_Debug::dump($apis); die();
        $this->view->lay = $m->get_layout();
        $this->view->apis = $apis;
        $this->view->data = $data;
        $this->view->apps = $m->get_apps();
        $this->view->ws = $m->get_workspace();
        $this->view->varams = $params;
        $this->view->classicon = $m->get_params('classicon');
        $this->view->area = $m->get_params('formarea');
    }

    public function builderformAction() {
        
        /*
         $this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery.numeric.js' );
         $this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css' );
         $this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css' );
         $this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-datepicker/css/datepicker.css' );
         $this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css' );
         $this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/select2/select2_metro.css' );
         $this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css' );
         //
         $this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js' );
         
         $this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js' );
         $this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js' );
         
         $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );
         $this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js' );
         $this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js' );
         $this->view->headScript ()->appendFile ( '/assets/core/plugins/select2/select2.min.js' );
         */
        $this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/css/plugins.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        $dd = new Model_Prabasystemaddon();
        $apis = $dd->get_api_grouping();
        if(!isset($params['row'])) {
            $params['row'] = 1;
        }
        $data = array();
        if(isset($params['act'])) {
            $data = $m2->get_portlet($params['id']);
        }
        $this->view->lay = $m->get_layout();
        $this->view->data = $data;
        $this->view->apis = $apis;
        $this->view->apps = $m->get_apps();
        $this->view->ws = $m->get_workspace();
        $this->view->varams = $params;
        $this->view->classicon = $m->get_params('classicon');
        $this->view->area = $m->get_params('formarea');
    }

    public function buildergraphicAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        $dd = new Model_Prabasystemaddon();
        $apis = $dd->get_api_grouping();
        if(!isset($params['row'])) {
            $params['row'] = 1;
        }
        if(isset($params['act'])) {
            $data = $m2->get_portlet($params['id']);
            // Zend_Debug::dump($data); die();
        }
        // Zend_Debug::dump($apis); die();
        $this->view->lay = $m->get_layout();
        $this->view->data = $data;
        $this->view->apis = $apis;
        $this->view->apps = $m->get_apps();
        $this->view->ws = $m->get_workspace();
        $this->view->varams = $params;
        // $this->view->classicon = $m->get_params('classicon');
        // $this->view->area = $m->get_params('formarea');
    }

    public function buildergraphicplusAction() {
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery.numeric.js');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-datepicker/css/datepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/select2/select2_metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-colorpicker/css/colorpicker.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        // $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        // $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js');
        // $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/fuelux/js/spinner.min.js');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        $dd = new Model_Prabasystemaddon();
        $apis = $dd->get_api_grouping();
        $par = new Model_Zparams();
        if(!isset($params['row'])) {
            $params['row'] = 1;
        }
        if(isset($params['act'])) {
            $data = $m2->get_portlet($params['id']);
            // Zend_Debug::dump($data); die();
        }
        // Zend_Debug::dump($apis); die();
        $this->view->lay = $m->get_layout();
        $this->view->data = $data;
        $this->view->apis = $apis;
        $this->view->apps = $m->get_apps();
        $this->view->ws = $m->get_workspace();
        $this->view->varams = $params;
        $this->view->chartType = $par->get_chart_type();
        $this->view->chartTheme = $par->get_chart_theme();
        // $this->view->classicon = $m->get_params('classicon');
        // $this->view->area = $m->get_params('formarea');
    }

    public function buildertableAction() {
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        $dd = new Model_Prabasystemaddon();
        $apis = $dd->get_api_grouping();
        $this->view->apis = $apis;
        $data = array();
        if(isset($params['act'])) {
            $data = $m2->get_portlet($params['id']);
        }
        // Zend_Debug::dump($data); die();
        $apis = $m2->get_all_api();
        $this->view->data = $data;
        $this->view->apps = $m->get_apps();
        $this->view->ws = $m->get_workspace();
        // Zend_Debug::dump($m->get_apps());die();
        $this->view->varams = $params;
    }

    public function editportletAction() {
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
        $dd = new Model_Prabasystemaddon();
        $apis = $dd->get_api_grouping();
        $params = $this->getRequest()->getParams();
        $cd = new Model_Zprabapage();
        $data = $cd->get_portlet($params['id']);
        $this->view->data = $data;
        $dd = new Model_Prabasystemaddon();
        $apis = $dd->get_api_grouping();
        $this->view->apis = $apis;
        // Zend_Debug::dump($data);
        // die();
    }

    public function editpageAction() {
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/select2.0/select2.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2.0/select2_metro.css');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        $data = $m2->get_page($params['id']);
        $data['id'] = $params['id'];
        //Zend_Debug::dump($data);die("ss");
        $arel = unserialize($data['element']);
        //Zend_Debug::dump($arel);die("ss");
        if(!isset($params['row'])&& count($arel)> 1) {
            $params['row'] = count($arel);
        } elseif(!isset($params['row'])) {
            $params['row'] = 1;
        }
        $this->view->apps = $m->get_apps();
        $this->view->ws = $m->get_workspace();
        $this->view->lay = $m->get_layout();
        // Zend_Debug::dump($m->get_layout());die();
        $this->view->varams = $params;
        $this->view->data = $data;
    }

    public function editapiAction() {
        $params = $this->getRequest()->getParams();
        if(!isset($params['id'])|| $params['id'] == '') {
            $this->_redirect('/prabaeditor/listapi');
        }
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $mdl_sys = new Model_Prabasystem();
        $data = $mdl_sys->get_a_api($params['id']);
        $page = new Model_Zprabapage();
        $userapp = $page->list_users_api();
        // Zend_Debug::dump($data); die();
        $modes = $mdl_sys->get_params('api_mode');
        $params = $mdl_sys->get_params('api_type');
        $conns = $mdl_sys->get_conn();
        $this->view->modes = $modes;
        $this->view->params = $params;
        $this->view->conns = $conns;
        $this->view->data = $data;
        $this->view->userapp = $userapp;
    }

    public function addportletAction() {
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/select2/js/select2.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2/css/select2.min.css');
        $params = $this->getRequest()->getParams();
        $dd = new Model_Prabasystemaddon();
        $apis = $dd->get_api_grouping();
        $this->view->apis = $apis;
    }

    public function listportletAction() {
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/css/plugins.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $sys = new Model_Zprabapage();
        $mdl = new Model_Prabasystem();
        $params = $sys->list_page();
        // Zend_Debug::dump($params);die();
        $apps = $mdl->get_apps();
        $this->view->params = $data;
        $this->view->apps = $apps;
    }

    public function addpageAction() {
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/select2.0/select2.min.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/select2.0/select2_metro.css');
        $params = $this->getRequest()->getParams();
        $m = new Model_Prabasystem();
        $m2 = new Model_Zprabapage();
        // Zend_Debug::dump($m2->list_page());die();
        if(!isset($params['row'])) {
            $params['row'] = 1;
        }
        $this->view->lay = $m->get_layout();
        $this->view->apps = $m->get_apps();
        $this->view->ws = $m->get_workspace();
        // Zend_Debug::dump($m->get_apps());die();
        $this->view->varams = $params;
    }

    public function frontAction() {
        //$this->view->headScript ()->appendFile ( '/assets/core/js/d3.v3.min.js' );
    }

    public function frontajaxAction() {
        $this->_helper->layout->disableLayout();
        $params = $this->getRequest()->getParams();
        $this->view->varams = $params;
    }

    public function loginappAction() {
        $params = $this->getRequest()->getParams();
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css');
        $this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-components.js');
        $this->view->headScript()->appendFile('/assets/core/scripts/form-validation.js');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $cc = new Model_System();
        $dd = new Model_Zparams();
        $themes = $dd->get_login_theme();
        $lays = $dd->get_login_position();
        $mdl_sys = new Model_Prabasystem();
        $data = $mdl_sys->get_a_app($params['id']);
        //Zend_Debug::dump($data); die();
        $this->view->params = $params;
        $this->view->lays = $lays;
        $this->view->data = $data;
        $this->view->themes = $themes;
    }

    public function listpageAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $sys = new Model_Zprabapage();
        $mdl = new Model_Prabasystem();
        $params = $sys->list_page();
        // Zend_Debug::dump($params);die();
        $apps = $mdl->get_apps();
        $this->view->params = $data;
        $this->view->apps = $apps;
    }

    public function editconnAction() {
        $params = $this->getRequest()->getParams();
        if(!isset($params['id'])|| $params['id'] == '') {
            $this->_redirect('/prabaeditor/listapi');
        }
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        
        /*
         $this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css' );
         $this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/jquery-multi-select/css/multi-select.css' );
         
         $this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );
         $this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js' );
         $this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js' );
         $this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery-multi-select/js/jquery.quicksearch.js' );
         $this->view->headScript ()->appendFile ( '/assets/core/scripts/form-components.js' );
         $this->view->headScript ()->appendFile ( '/assets/core/scripts/form-validation.js' );
         */
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $mdl_sys = new Model_Prabasystem();
        $data = $mdl_sys->get_a_conn($params['id']);
        $page = new Model_Zprabapage();
        $conns = $mdl_sys->get_conn();
        $pa = new Model_Zparams();
        $this->view->adapts = $pa->get_adapters();
        $this->view->modes = $modes;
        $this->view->params = $params;
        $this->view->conns = $conns;
        $this->view->data = $data;
    }

    public function listconnAction() {
        $mm = new Model_Zprabapage();
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $sys = new Model_Prabasystem();
        $pa = new Model_Zparams();
        $this->view->adapts = $pa->get_adapters();
        //Zend_Debug::dump($pa->get_adapters()); 
        $datacon = $sys->get_conns_serialize();
        // Zend_Debug::dump($datacon); die();
        $this->view->conns = $datacon;
    }

    public function listapiAction() {
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/jquery-multi-select/css/multi-select.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/datatables.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/datatables/datatables.min.css');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        // Zend_Debug::dump($identity);die();
        $sys = new Model_Prabasystem();
        $page = new Model_Zprabapage();
        $userapp = $page->list_users_api();
        // Zend_Debug::dump($userapp); die();
        $params = $sys->get_params('api_type');
        $modes = $sys->get_params('api_mode');
        // Zend_Debug::dump($modes); die();
        $conns = $sys->get_conn();
        $this->view->modes = $modes;
        $this->view->params = $params;
        $this->view->conns = $conns;
        $this->view->userapp = $userapp;
    }

    public function editappAction() {
        $params = $this->getRequest()->getParams();
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        $cc = new Model_System();
        $dd = new Model_Zparams();
        $themes = $cc->get_themes();
        $lays = $dd->get_layout();
        $skins = $dd->get_skins();
        $mdl_sys = new Model_Prabasystem();
        $data = $mdl_sys->get_a_app($params['id']);
        // Zend_Debug::dump($data); die();
        $this->view->params = $params;
        $this->view->lays = $lays;
        $this->view->data = $data;
        $this->view->skins = $skins;
        $this->view->themes = $themes;
    }

    public function listappAction() {
        $c1 = new Prabacontent_Model_Content();
        $datacon = $c1->get_applications_with_check();
        $mm = new Model_Zprabapage();
        $cc = new Model_System();
        $apps = $cc->get_app_modules();
        $this->view->headScript()->appendFile('/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
        $this->view->headScript()->appendFile('/assets/core/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
        $this->view->headLink()->appendStylesheet('/assets/core/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }

        foreach($datacon as $k => $v) {
            if(in_array($v['s_app'], $apps)) {
                $datacon2[$k] = $v;
            }
        }
        $cc = new Model_System();
        $dd = new Model_Zparams();
        $themes = $cc->get_themes();
        $lays = $dd->get_layout();
        $skins = $dd->get_skins();
        $this->view->lays = $lays;
        $this->view->themes = $themes;
        $this->view->skins = $skins;
        $this->view->conns = $datacon;
    }
}
