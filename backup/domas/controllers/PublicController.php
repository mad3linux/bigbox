<?php

/*
 * Copyright (C) Prabtech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class Domas_PublicController extends Zend_Controller_Action {

    public function init() {
        
        /*
         * $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger'); $this->_cache = Zend_Registry::get('cache'); $this->initView();
         */
    }

    public function index() {
    }

    public function telegramAction() {
        $params = $this->getRequest()->getParams();
        $this->_helper->layout->disableLayout();
        switch($params['act']) {
            case 'sendmsg' : $target = $params['target'];
            $msg = $params['msg'];
            $id =(isset($params['id']))? : time();
            $target = str_replace(' ', '_', $target);
            $cmd = 'tg_send ' . $id . ' ' . $target . ' "' . $msg . '" &';
            $tmp = array();
            if(isset($target)&& $target != "" && isset($msg)&& $msg != "") {
                $output = shell_exec($cmd);
                $output = str_replace('', '', $output);
                $tmp = explode("\n", $output);
                $tmp = explode("~", $tmp[1]);
            }
            header("Access-Control-Allow-Origin: *");
            header('Content-type: application/json');
            echo json_encode($tmp);
            die();
            break;
            case 'sendingimg' : $target = $params['target'];
            $target = $params['target'];
            $target = str_replace(' ', '_', $target);
            $msg = $params['msg'];
            $id =(isset($params['id']))? : time();
            $path = $params['path'];
            if($_FILES) {
                $dest_dir = constant("APPLICATION_PATH"). "/../public/tmp/telegram/images";
                $upload = new Zend_File_Transfer_Adapter_Http();
                $upload->setDestination($dest_dir);
                $files = $upload->getFileInfo();
                try {
                    $upload->receive();
                    $path = $dest_dir . '/' . $files['File']['name'];
                }
                catch(Zend_File_Transfer_Exception $e) {
                }
            }
            $tmp = array();
            if(isset($params['target'])&& $params['target'] != "" && $path != "") {
                $cmd = 'tg_sendphoto ' . $id . ' ' . $target . ' "' . $path . '" &';
                $output = shell_exec($cmd);
                $output = str_replace('', '', $output);
                //my_dump($output);
                $tmp = explode("\n", $output);
                //my_dump($tmp);
                $tmp = explode("~", $tmp[0]);
            }
            //my_dump_exit($tmp);
            header("Access-Control-Allow-Origin: *");
            header('Content-type: application/json');
            echo json_encode($tmp);
            die();
            break;
            case 'sendmsgtxt' : $target = $params['target'];
            $target = $params['target'];
            $target = str_replace(' ', '_', $target);
            $msg = $params['msg'];
            $id =(isset($params['id']))? : time();
            $dir = constant("APPLICATION_PATH"). "/../public/tmp/telegram/";
            if(isset($params['target'])&& $params['target'] != "" && isset($params['msg'])&& $params['msg'] != "") {
                $length = str_split($msg, 3900);
                $loc = $dir . time(). '_' . rand(). '_';
                //$output = shell_exec("whoami");
                //die($output

                foreach($length as $k => $v) {
                    $host = $loc . $k . '.txt';
                    //die($host);
                    $handle = fopen($host, 'w')or die('Cannot open file:  ' . 
                                                      $host);
                    fwrite($handle, $v);
                    fclose($handle);
                    //die();
                    $target = str_replace(' ', '_', $target);
                    $cmd = 'tg_sendtext ' . $id . ' ' . $target . ' "' . $host . '" &';
                    //die($cmd);
                    $output = shell_exec($cmd);
                    $output = str_replace('', '', $output);
                    //die($output);
                    //my_dump($output);
                    $tmp = explode("\n", $output);
                    //my_dump($tmp);
                    $tmp = explode("~", $tmp[1]);
                    //my_dump_exit($tmp);
                }
            }
            header("Access-Control-Allow-Origin: *");
            header('Content-type: application/json');
            echo json_encode($tmp);
            die();
            break;
            case 'sendingvideo' : $target = $params['target'];
            $target = str_replace(' ', '_', $target);
            $id =(isset($params['id']))? : time();
            $path = $params['path'];
            if($_FILES) {
                $dest_dir = constant("APPLICATION_PATH"). "/../public/tmp/telegram/video";
                $upload = new Zend_File_Transfer_Adapter_Http();
                $upload->setDestination($dest_dir);
                $files = $upload->getFileInfo();
                try {
                    $upload->receive();
                    $path = $dest_dir . '/' . $files['File']['name'];
                }
                catch(Zend_File_Transfer_Exception $e) {
                }
            }
            $tmp = array();
            if(isset($params['target'])&& $params['target'] != "" && $path != "") {
                $cmd = 'tg_sendvideo ' . $id . ' ' . $target . ' "' . $path . '" &';
                //die();
                $output = shell_exec($cmd);
                $output = str_replace('', '', $output);
                //my_dump($output);
                $tmp = explode("\n", $output);
                //my_dump($tmp);
                $tmp = explode("~", $tmp[0]);
            }
            //my_dump_exit($tmp);
            header("Access-Control-Allow-Origin: *");
            header('Content-type: application/json');
            echo json_encode($tmp);
            die();
            break;
            case 'creategroup' : $subject = $params['subject'];
            $member = $params['member'];
            if(isset($params['subject'])&& $params['subject'] != "" && isset($params['member'])&& $params['member'] != "") {
                $member = explode(',', $member);
                //my_dump($member);
                $cmd = 'tg_creategroup ' . str_replace(' ', '_', $subject). ' ' . str_replace(' ', '_', $member[0]);
                //die($cmd);
                $output = shell_exec($cmd);
                $output = str_replace('', '', $output);
                //my_dump_exit($output);
                $tmp = explode("\n", $output);
                //my_dump($tmp);
                $tmp = explode(" ", $tmp[0]);
                //my_dump($tmp);
                $group = array();
                $group['name'] = $tmp[3];
                $group['status'] = $tmp[4];
                $group['member'] = array();
                $tmp = array();
                if(isset($params['member'])&& $params['member'] != "" && isset($params['subject'])&& $params['subject'] != "") {
                    if($tmp[4] == "Berhasil") {
                        $group['member'][] = array('name' =>$tmp[2],
                                                   'status' =>$tmp[4]);

                        foreach($member as $k => $v) {
                            if($k > 0) {
                                $cmd = 'tg_addmember ' . str_replace(' ', '_', $subject). ' ' . str_replace(' ', '_', $v);
                                //die($cmd);
                                $output = shell_exec($cmd);
                                $output = str_replace('', '', $output);
                                //my_dump_exit($output);
                                $tmp = explode("\n", $output);
                                //my_dump($tmp);
                                $tmp = explode(" ", $tmp[0]);
                                //my_dump_exit($tmp);
                                $group['member'][] = array('name' =>$tmp[3],
                                                           'status' =>$tmp[4]);
                            }
                        }
                    }
                }
            }
            //my_dump_exit($group);
            header("Access-Control-Allow-Origin: *");
            header('Content-type: application/json');
            echo json_encode($group);
            die();
            break;
            case 'delmember' : $group = $params['group'];
            $member = $params['member'];
            if(isset($params['group'])&& $params['group'] != "" && isset($params['member'])&& $params['member'] != "") {
                $member = explode(',', $member);
                //my_dump_exit($member);
                $remove = array();

                foreach($member as $k => $v) {
                    $cmd = 'tg_delmember ' . str_replace(' ', '_', $group). ' ' . str_replace(' ', '_', $v);
                    //die($cmd);
                    $output = shell_exec($cmd);
                    $output = str_replace('', '', $output);
                    //my_dump_exit($output);
                    $tmp = explode("\n", $output);
                    //my_dump($tmp);
                    $tmp = explode(" ", $tmp[0]);
                    //my_dump_exit($tmp);
                    $remove[] = array('name' =>$tmp[3],
                                      'status' =>$tmp[4],
                                     );
                }
            }
            header("Access-Control-Allow-Origin: *");
            header('Content-type: application/json');
            echo json_encode($remove);
            die();
            break;
            case 'addmember' : $params['group'];
            $member = $params['member'];
            $member = explode(',', $member);
            //my_dump_exit($member);
            $add = array();

            foreach($member as $k => $v) {
                $cmd = 'tg_addmember ' . str_replace(' ', '_', $group). ' ' . str_replace(' ', '_', $v);
                //die($cmd);
                $output = shell_exec($cmd);
                $output = str_replace('', '', $output);
                //my_dump_exit($output);
                $tmp = explode("\n", $output);
                //my_dump($tmp);
                $tmp = explode(" ", $tmp[0]);
                //my_dump_exit($tmp);
                $add[] = array('name' =>$tmp[3],
                               'status' =>$tmp[4],
                              );
            }
            header("Access-Control-Allow-Origin: *");
            header('Content-type: application/json');
            echo json_encode($add);
            die();
            break;
            case 'addcontact' : $phone = $params['phone'];
            $firstname = $params['firstname'];
            $lastname = $params['lastname'];
            $phone = explode(',', $phone);
            $firstname = explode(',', $firstname);
            $lastname = explode(',', $lastname);
            //my_dump($phone);
            //my_dump($firstname);
            //my_dump_exit($lastname);
            $contacts = array();

            foreach($phone as $k => $v) {
                $contacts[] = array('phone' =>$v,
                                    'firstname' =>((isset($firstname[$k])&&$firstname[$k]!= '')?$firstname[$k]: time()),
                                    'lastname' =>((isset($lastname[$k])&&$lastname[$k]!= '')?$lastname[$k]: time()));
            }
            //my_dump_exit($contacts);
            $add = array();
            if(isset($params['phone'])&& $params['phone'] != "" && isset($params['firstname'])&& $params['firstname'] != "" && isset($params['lastname'])&& $params['lastname'] != "") {

                foreach($contacts as $k => $v) {
                    $cmd = 'tg_addcontact +' . $v['phone'] . ' ' . str_replace(' ', '_', $v['firstname']). ' ' . str_replace(' ', '_', $v['lastname']);
                    //die($cmd);
                    $output = shell_exec($cmd);
                    $output = str_replace('', '', $output);
                    //my_dump_exit($output);
                    $tmp = explode("\n", $output);
                    //my_dump($tmp);
                    $tmp = explode(" ", $tmp[0]);
                    //my_dump_exit($tmp);
                    $add[] = array('phone' =>$tmp[2],
                                   'name' =>$tmp[3],
                                   'status' =>$tmp[4]);
                }
            }
            header("Access-Control-Allow-Origin: *");
            header('Content-type: application/json');
            echo json_encode($add);
            die();
            break;
            case 'getgroups' : $output = shell_exec('tg_getgroups');
            $output = str_replace('', '', $output);
            $patterns = array();
            $patterns[0] = '/\[35;1m/';
            $patterns[1] = '/\[33;1m:/';
            $replacements = array();
            $replacements[1] = '#';
            $replacements[0] = '#';
            $output = preg_replace($patterns, $replacements, $output);
            preg_match_all("/#(.*)#/", $output, $tmp);
            //$tmp = explode('#',$output);
            //my_dump_exit($tmp);
            $group = $tmp[1];
            header("Access-Control-Allow-Origin: *");
            header('Content-type: application/json');
            echo json_encode($group);
            die();
            break;
            case 'getcontacts' : $output = shell_exec('tg_getcontacts');
            $output = str_replace('', '', $output);
            $patterns = array();
            $patterns[0] = '/33;1m/';
            $replacements = array();
            $replacements[0] = '';
            $output = preg_replace($patterns, $replacements, $output);
            //my_dump_exit($output);
            $tmp = explode("\n", $output);
            //my_dump($tmp);
            $contact = array();
            //my_dump($output_array);

            foreach($tmp as $k => $v) {
                preg_match_all("/\[0;31m\[1;31m(.*?)\[0;31m\[0m/", $v, $tmpx);
                //my_dump($tmpx);
                //my_dump($tmpx);
                if(isset($tmpx[1][0])) {
                    $contact[] = $tmpx[1][0];
                }
            }
            header("Access-Control-Allow-Origin: *");
            header('Content-type: application/json');
            echo json_encode($contact);
            die();
            break;
            case 'getmembers' : $group = $params['group'];
            if($group != "") {
                $output = shell_exec('tg_getmembers ' . 
                                     str_replace(' ', '_', $group));
                //my_dump('tg_getmembers '.str_replace(' ','_',$group));
                $output = str_replace('', '', $output);
                //my_dump($output);//die();
                //my_dump(str_replace('0;31m','',$output));die();
                $patterns = array();
                $patterns[0] = '/0;31m1;31m/';
                //$patterns[1] = '/0;31m33;1m./';
                $patterns[1] = '/(0;31m)/';
                $patterns[2] = '/(33;1m.)/';
                //$patterns[2] = '/33;1m/';
                //$patterns[3] = '/invited.by/';
                //$patterns[2] = '/at./';
                $replacements = array();
                //$replacements[4] = '#';
                //$replacements[3] = '#';
                //$replacements[2] = '#';
                $replacements[0] = '#';
                $replacements[1] = '#';
                $replacements[2] = '#';
                $output = preg_replace($patterns, $replacements, $output);
                $output = str_replace('at ', 'at #', $output);
                //my_dump_exit($output);
                $tmp = explode("\n", $output);
                //my_dump_exit($tmp);
                $member = array();

                foreach($tmp as $k => $v) {
                    if($v != '') {
                        $tmp2 = explode('#', trim($v));
                        //my_dump($tmp2);
                        if(isset($tmp2[0])&& $tmp2[0] != '') {
                            $tmp3 = explode(' ', $tmp2[4]);
                            $member[] = array('name' =>$tmp2[0],
                                              'invited_by' =>$tmp2[2],
                                              'invited_at' =>$tmp3[0]. ' ' .$tmp3[1],
                                              'admin' =>(isset($tmp3[2]))? true : false);
                        } else if(isset($tmp2[0])&& $tmp2[0] == '' && trim($tmp2[4])== 'at') {
                            $tmp3 = explode(' ', $tmp2[5]);
                            $member[] = array('name' =>$tmp2[1],
                                              'invited_by' =>$tmp2[3],
                                              'invited_at' =>$tmp3[0]. ' ' .$tmp3[1],
                                              'admin' =>(isset($tmp3[2]))? true : false);
                        } else if(isset($tmp2[0])&& $tmp2[0] == '' && $tmp2[5] == '') {
                            $tmp3 = explode(' ', $tmp2[7]);
                            $member[] = array('name' =>$tmp2[1],
                                              'invited_by' =>$tmp2[4],
                                              'invited_at' =>$tmp3[0]. ' ' .$tmp3[1],
                                              'admin' =>(isset($tmp3[2]))? true : false);
                        } else if(isset($tmp2[0])&& $tmp2[0] == '' && $tmp2[4] == '') {
                            $tmp3 = explode(' ', $tmp2[6]);
                            $member[] = array('name' =>$tmp2[1],
                                              'invited_by' =>$tmp2[3],
                                              'invited_at' =>$tmp3[0]. ' ' .$tmp3[1],
                                              'admin' =>(isset($tmp3[2]))? true : false);
                        } else if(isset($tmp2[0])&& $tmp2[0] == '') {
                            $tmp3 = explode(' ', $tmp2[6]);
                            $member[] = array('name' =>$tmp2[1],
                                              'invited_by' =>$tmp2[4],
                                              'invited_at' =>$tmp3[0]. ' ' .$tmp3[1],
                                              'admin' =>(isset($tmp3[2]))? true : false);
                        }
                    }
                }
            }
            //die();
            header("Access-Control-Allow-Origin: *");
            header('Content-type: application/json');
            echo json_encode($member);
            die();
            break;
        }
    }

    public function basicapiAction() {
        $this->_helper->layout->disableLayout();
        $realm = "BasicAPI";
        $result = array('ret' => false,
                        'msg' => 'You Can\'t Access This API');
        // Zend_Debug::dump($_SERVER['REQUEST_METHOD']); //die();
        //Zend_Debug::dump($_SERVER['PHP_AUTH_USER']); die();
        if($_SERVER['REQUEST_METHOD'] != "POST" and !isset($_SERVER['PHP_AUTH_USER'])) {
            ///die("x");
            header('WWW-Authenticate: Basic realm="' . 
                   $realm . 
                   '"');
            header('HTTP/1.0 401 Unauthorized');
            // die();
        } else {
            //  die("2");
            //Zend_Debug::dump($_SERVER['REQUEST_METHOD']); die();
            //$_SERVER['PHP_AUTH_USER'] $_SERVER['PHP_AUTH_PW']
            $mdl_zuser = new Model_Zusers();
            $usr = $mdl_zuser->getuser($_SERVER['PHP_AUTH_USER']);
            if($usr == false) {
                header('WWW-Authenticate: Basic realm="' . 
                       $realm . 
                       '"');
                header('HTTP/1.0 401 Unauthorized');
            } else {
                // Zend_Debug::dump($usr); die();
                if($usr["upassword"] == md5($_SERVER['PHP_AUTH_PW'])) {
                    $result = array('ret' => true,
                                    'msg' => 'Hello ' .$_SERVER['PHP_AUTH_USER']);
                } else {
                    header('WWW-Authenticate: Basic realm="' . 
                           $realm . 
                           '"');
                    header('HTTP/1.0 401 Unauthorized');
                }
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    public function digestapiAction() {
        $this->_helper->layout->disableLayout();
        $realm = "DigestAPI";
        $result = array('ret' => false,
                        'msg' => 'You Can\'t Access This API');
        if($_SERVER['REQUEST_METHOD'] != "POST" && empty($_SERVER['PHP_AUTH_DIGEST'])) {
            Zend_Debug::dump($_SERVER['PHP_AUTH_DIGEST']);
            die();
            header('HTTP/1.1 401 Unauthorized');
            header('WWW-Authenticate: Digest realm="' . 
                   $realm . 
                   '",qop="auth",nonce="' . 
                   uniqid(). 
                          '",opaque="' . 
                          md5($realm). 
                              '"');
        } else {
            Zend_Debug::dump($_SERVER['PHP_AUTH_DIGEST']);
            die();
            $data = $this->http_digest_parse($_SERVER['PHP_AUTH_DIGEST']);
            // Zend_Debug::dump($_SERVER);
            // Zend_Debug::dump($data); //die();
            // analyze the PHP_AUTH_DIGEST variable
            if($data['realm'] != $realm) {
                header('HTTP/1.1 401 Unauthorized');
                header('WWW-Authenticate: Digest realm="' . 
                       $realm . 
                       '",qop="auth",nonce="' . 
                       uniqid(). 
                              '",opaque="' . 
                              md5($realm). 
                                  '"');
            } else {
                $mdl_zuser = new Model_Zusers();
                $usr = $mdl_zuser->getuser($data['username']);
                if($usr == false) {
                    header('HTTP/1.0 401 Unauthorized');
                    header('WWW-Authenticate: Digest realm="' . 
                           $realm . 
                           '",qop="auth",nonce="' . 
                           uniqid(). 
                                  '",opaque="' . 
                                  md5($realm). 
                                      '"');
                } else {
                    // Zend_Debug::dump($usr); die();
                    // if($usr["upassword"]==md5($_SERVER['PHP_AUTH_PW'])){
                    $result = array('ret' => true,
                                    'msg' => 'Hello ' .$data['username']);
                    // }else{
                    // header('HTTP/1.0 401 Unauthorized');
                    // }
                }
            }
        }
        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($result);
        die();
    }

    function http_digest_parse($txt) {
        // Zend_Debug::dump($txt); die();
        // protect against missing data
        $needed_parts = array('nonce' => 1,
                              'realm' => 1,
                              'nc' => 0,
                              'cnonce' => 0,
                              'qop' => 0,
                              'username' => 1,
                              'uri' => "/public/digestapi",
                              'opaque' => 1,
                              'response' => 1);
        $data = array();
        $keys = implode('|', array_keys($needed_parts));
        preg_match_all('@(' . 
                          $keys . 
                          ')=(?:([\'"])([^\2]+?)\2|([^\s,]+))@', $txt, $matches, PREG_SET_ORDER);
        // Zend_Debug::dump($matches); //die();

        foreach($matches as $m) {
            $data[$m[1]] = $m[3] ? $m[3] : $m[4];
            // Zend_Debug::dump($data); die();
            if($needed_parts[$m[1]] == 1 ||($needed_parts[$m[1]] == $data[$m[1]])) {
                unset($needed_parts[$m[1]]);
            }
        }

        foreach($needed_parts as $k => $v) {
            if($v == 0) {
                unset($needed_parts[$k]);
            }
        }
        // Zend_Debug::dump($needed_parts); die();
        return $needed_parts ? false : $data;
    }

    public function loginAction() {
        $theme = Zend_Registry::get('theme');
        $this->view->headLink()->appendStylesheet("/assets/$theme/pages/css/login.min.css");
        $params = $this->getRequest()->getParams();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $usr = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        if(isset($usr->uid)&& isset($usr->uname)) {
            $this->redirect('/');
        }
        $captcha = new Zend_Form_Element_Captcha('captcha', // This is the name of the input field
        array('label' => 'Write the chars to the field', 'captcha' => array(// Here comes the magic...
        // First the type...
        'captcha' => 'Image', // Length of the word...
        'wordLen' => 3, //'height'=>30,
        // Captcha timeout, 5 mins
        'timeout' => 300, 'LineNoiseLevel' => 0, 'DotNoiseLevel' => 5, // What font to use...
        'font' => APPLICATION_PATH . '/../public/assets/core/fonts/elephant.ttf', // Where to put the image
        'imgDir' => APPLICATION_PATH . '/../public/tmp/captcha/', // URL to the images
        // This was bogus, here's how it should be... Sorry again :S
        'imgUrl' => '/tmp/captcha/',)));
        $form = new Form_Login();
        $form->setAction('/public/login');
        $form->addElement($captcha);
        $this->view->form = $form;
        $this->view->captcha = $captcha;
        if($this->_request->isPost()&& isset($_POST)) {
            $cms_enc = new CMS_Enc();
            $data = $_POST;
            $raws = $_POST;
            $captcha = $data['captcha'];
            $captchaId = $captcha['id'];
            $captchaInput = $captcha['input'];
            $captchaSession = new Zend_Session_Namespace('Zend_Form_Captcha_' . 
                                                         $captchaId);
            $captchaIterator = $captchaSession->getIterator();
            if(!isset($captchaIterator['word'])) {
                $this->redirect('/');
            }
            $captchaWord = $captchaIterator['word'];
            if($captchaInput == $captchaWord) {
                $u = $data['uname'];
                $p = $data['passw'];
                $mdl_zusr = new Model_Zusers();
                $mdl_sys = new Model_System();
                $data = $mdl_zusr->getuser($u);
                $arr1 = array();
                if(isset($data['id'])) {
                    $rols = $mdl_sys->get_roles_by_uid($data['id']);

                    foreach($rols as $v) {
                        $arr[] = $v['gid'];
                        if($data['main_role'] != '' && $data['main_role'] != null) {
                            if($v['landing_page'] != "" && $v['landing_page'] != null && ( int ) $v['gid'] == ( int ) $data['main_role']) {
                                $redirect_role = $v['landing_page'];
                            }
                        }
                        if($v['exec'] != "") {
                            $exec[] = $v['exec'];
                        }
                    }
                }
                $auth = false;
                if(!isset($data['id'])|| $data['id'] == "") {
                    $cms_ldp = new CMS_LDAP();
                    $auth = $cms_ldp->auth($u, $p);
                    if($auth) {
                        $data['id'] = "999999999";
                        $arr[0] = "108";
                        $uid = "999999999";
                        $redirect_role = '/';
                    } else {
                        $uid = $u;
                    }
                } else {
                    $uid = $data['id'];
                }
                $row = array();
                $msg = "Username dan Password anda tidak dikenal\nSilahkan gunakan user LDAP anda.";
                $ret = array('success' => 0,
                             'msg' =>$msg);
                if($u == '') {
                    $ret['success'] = 0;
                    $ret['msg'] = " Username is required";
                }
                if($p == '') {
                    $ret['success'] = 0;
                    $ret['msg'] = "Password is required";
                }
                $cms_ldp = new CMS_LDAP();
                if($data['isldap'] == 1) {
                    $auth = $cms_ldp->auth($u, $p);
                }
                if($auth && $data['id'] != "") {
                    $info = $cms_ldp->bind($u);
                    $ret['success'] = $auth;
                    $authsession = Zend_Auth::getInstance();
                    $storage = $authsession->getStorage();
                    $obj2 = new stdClass();
                    $obj2->uid = $uid;
                    $obj2->uname = $u;
                    $obj2->fullname = $info[0]['cn'][0];
                    $obj2->isldap = 1;
                    $obj2->mainrole = 1;
                    $obj2->mainrole = $data['main_role'];
                    $obj2->auth = 1;
                    $obj2->currentevent = $event;
                    $obj2->roles = $arr;
                    $obj2->ubis = $data['ubis_id'];
                    $obj2->sububis = $data['sub_ubis_id'];
                    $obj2->sububisname = $data['sub_ubis'];
                    $storage->write($obj2);
                    $ret['msg'] = "";
                } elseif(($data['isldap'] != 1 or $data['isldap'] == "" or !isset($data['isldap']))&& $uid != "") {
                    $db = Zend_Db_Table::getDefaultAdapter();
                    if(count($data)> 0) {
                        $authAdapter = new CMS_TmaAuth($db, 'z_users', 'uname', 'upassword', 'MD5(?)', 0);
                        $authAdapter->setIdentity($u);
                        $authAdapter->setCredential($p);
                        $result = $authAdapter->authenticate();
                        if($result->isValid()) {
                            $cekz = false;
                            if($cekz) {
                                $ret['success'] = 0;
                                $ret['msg'] = "Username has been used in other place.";
                                // var_dump($ret);die();
                            } else {
                                try {
                                    $authsession = Zend_Auth::getInstance();
                                    $storage = $authsession->getStorage();
                                }
                                catch(Exception $e) {
                                }
                                $ret['success'] = 1;
                                $obj2 = new stdClass();
                                $obj2->uid = $data['id'];
                                $obj2->uname = $data['uname'];
                                $obj2->roles = $arr;
                                $obj2->fullname = $data['fullname'];
                                $obj2->mainrole = $data['main_role'];
                                $obj2->isldap = 0;
                                $obj2->currentevent = $event;
                                $obj2->auth = 1;
                                $obj2->ubis = $data['ubis_id'];
                                $obj2->sububis =(isset($data['sub_ubis_id']))? $data['sub_ubis_id'] : null;
                                $obj2->sububisname =(isset($data['sub_ubis']))? $data['sub_ubis'] : null;
                                $storage->write($obj2);
                                $ret['msg'] = "";
                            }
                        } else {
                            $ret['msg'] = "Wrong User Name/Password";
                        }
                    } else {
                        $ret['success'] = 0;
                        $ret['msg'] = "User tersebut tidak terdaftar.";
                    }
                } else {
                    $ret['success'] = 0;
                    $ret['msg'] = "LDAP Error";
                }
                if($ret['success'] == 1) {
                    try {
                        $auth = Zend_Auth::getInstance();
                        $usr = $auth->getIdentity();
                    }
                    catch(Zend_Session_Exception $e) {
                    }
                    if(isset($usr->uid)&& isset($usr->uname)) {
                        $mdl_sys->update_log($usr->uname);
                        $res2 = $mdl_zusr->update_c_act($usr->uname);
                        if(isset($_GET['redirect'])&&($_GET['redirect'] != '/')&& $_GET['redirect'] != '') {
                            $this->_redirect($_GET['redirect']);
                        } else if(isset($_POST['redirect'])&& $_POST['redirect'] != '' && $_POST['redirect'] != '/') {
                            $this->_redirect($_POST['redirect']);
                        } else if(isset($params['redirect'])&& $params['redirect'] != '' && $params['redirect'] != '/') {
                            $this->_redirect($params['redirect']);
                        } else {
                            $module = Zend_Controller_Front::getInstance()->getRequest()->getModuleName();
                            if(isset($redirect_role)&& $redirect_role != "") {
                                $this->_redirect($redirect_role);
                            } else if($module != 'default') {
                                $this->_redirect("/" . 
                                                 $control);
                            } else {
                                $this->_redirect("/");
                            }
                        }
                    }
                } else {
                }
            } else {
                $ret['success'] = 0;
                $ret['msg'] = "Please enter valid captcha.";
            }
        }
        if(isset($params['redirect'])&& $params['redirect'] != '') {
            $this->view->redirect = $params['redirect'];
        }
        if(isset($ret)) {
            $this->_redirect("http://" . 
                             $_SERVER['HTTP_HOST'] . 
                             '' . 
                             $_SERVER['REQUEST_URI'] . 
                             '?' . 
                             http_build_query($ret));
        }
    }

    public function logoutAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
            // /clear
            $cc = new Model_Zusers();
            $cc->clear_logout($identity->uname);
            // / end
            $authAdapter->clearIdentity();
            Zend_Session::destroy();
        }
        catch(Exception $e) {
        }
        $this->_redirect('/');
    }
}
