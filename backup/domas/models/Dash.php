<?php


/*
 * Copyright (C) Prabtech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class Domas_Model_Dash extends Zend_Db_Table_Abstract {

    public function get_a_dash () {
        //die("select * from zprabaf_data_$bundle  where entity_id= $cid");
        try {
            
            $n = new Pmas_Model_Withsolr();
			
			$enddate = date("Y-m-d");
			$startdate = date("Y-m-d", strtotime($enddate." -6 days"));
			// Zend_Debug::dump($startdate);die();
			
			$q = "content_date:[".$startdate."T00:00:00Z%20TO%20".$enddate."T23:59:59Z]";
			// $q = "";
			
			$query = "select?wt=json&indent=true&fl=id&q=".$q."&group=true&group.field=entity_id";
			
			// Zend_Debug::dump($n->get_by_rawurl($query));die($query);
            
            return $n->get_by_rawurl($query);
            
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
        //return $item;
    }

   
}
