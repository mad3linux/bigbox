<?php

class Domas_Model_General extends Zend_Db_Table_Abstract {

    public function get_prediksi($data) {
        // Zend_Debug::dump($data);die();
        //Zend_Debug::dump($t);//die();
        //Zend_Debug::dump($ti);die();
        $ideo = $data['cat'];
        $sql0 = "SELECT parent FROM `zpraba_predictive_term_data` where 1=1 AND attr2=" . $ideo . "";
        $cek = $this->_db->fetchOne($sql0);
        if($cek == 0) {
            $sql1 = "SELECT name FROM `zpraba_taxonomy_term_data` where tid=" . $data['topik'] . "";
            $cek1 = $this->_db->fetchOne($sql1);
            $sql = "SELECT * FROM `zpraba_predictive_term_data` where 1=1 AND parent=(SELECT pid FROM `zpraba_predictive_term_data` where 1=1 AND name like '" . $cek1 . "')";
            // Zend_Debug::dump($sql);die('dd');
        } else {
            $sql = "SELECT * FROM `zpraba_predictive_term_data` where 1=1 AND parent=(SELECT pid FROM `zpraba_predictive_term_data` where 1=1 AND attr2=" . $ideo . ")";
        }
        // die($sql); 
        try {
            $data = $this->_db->fetchAll($sql);
            // Zend_Debug::dump($data);die();
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }
    }

    public function get_isi_prediksi() {
        $sql2 = "SELECT a.*,case when nilai=0 then 'Meningkat' when nilai=1 then 'Normal' else 'Menurun' end as nilai2 FROM `zpraba_predictive` a where 1=1 limit 10";
        // die($sql2); 
        // Zend_Debug::dump($sql2);die();
        try {
            // Zend_Debug::dump($sql);die();
            $data = $this->_db->fetchAll($sql2);
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }
    }

    public function insert_contetnt($param) {
        // Zend_Debug::dump($param);die();
        $sql1 = "SELECT count(*) AS cnt FROM `zpraba_predictive` where id_content='" . $param['id_content'] . "'";
        $data1 = $this->_db->fetchAll($sql1);
        // Zend_Debug::dump($data1);die();
        if(isset($param["stype"])&& $param["stype"] != "") {
            $e_id = array(2 => "News",
                          5 => "Socmed",
                          3 => "Lapin",
                         );
            $ent = $e_id[$param["stype"]];
            // Zend_Debug::dump($ent); die('sssssssss');
        }
        // Zend_Debug::dump($param["stype"]); die('');
        if($data1[0]['cnt'] == 0) {
            // Zend_Debug::dump($data1);die();
            $sql = "INSERT INTO zpraba_predictive (
				id,
				id_content,
				text,
				tema,
				topik,
				Prediksi,
				nilai,
				stype)
				VALUES ( null,
				'" . $param['id_content'] . "',
				'" . str_replace("'", "\"", $param['text']). "',
				'" . $param['tema'] . "',
				'" . $param['topik'] . "',
				'" . $param['prediksi'] . "',
				'" . $param['nilai'] . "',
				'" . $ent . "'
				)";
        }
        // Zend_Debug::dump($sql);die();
        // die($sql); 
        try {
            // Zend_Debug::dump($e_id);die();
            $data = $this->_db->fetchAll($sql);
            // Zend_Debug::dump($sql);die();
            return $data;
        }
        catch(Exception $e) {
            // Zend_Debug::dump($e->getMessage());die();
        }
    }

    function count_listalert($app, $GET) {
        // Zend_Debug::dump($arr_searchkey); die('jjjjjj');
        $aColumns = array('no',
                          'IDPROFILE',
                          'NAMA',
                          'CAMERANAME');
        
        /*
         * Ordering
         */
        $sOrder = "";
        if(isset($_GET['iSortCol_0'])) {
            $sOrder = "ORDER BY  ";
            for($i = 1;
            $i < intval ($_GET['iSortingCols'] );
            $i ++ ) {
                if($_GET['bSortable_' . 
                   intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= "`" . $aColumns[intval($_GET['iSortCol_' . 
                                                      $i])] . "` " .($_GET['sSortDir_' . 
                                                                     $i] === 'asc' ? 'asc' : 'desc'). ", ";
                }
            }
            $sOrder = substr_replace($sOrder, "", - 2);
            if($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }
        
        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $whapp = "";
        $sWhere = " WHERE 1=1  ";
        // if($app != "") {
        // $whapp = "AND app = '$app'";
        // }
        if(isset($GET['sSearch'])&& $GET['sSearch'] != "") {
            $sWhere .= " AND (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . strtoupper($GET['sSearch']). "%' OR ";
            }
            // die($sWhere);
            // 
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }
        
        /* Individual column filtering */
        for($i = 1;
        $i < count ($aColumns );
        $i ++ ) {
            if(isset($GET['bSearchable_' . $i])&& $GET['bSearchable_' . 
                     $i] == "true" && $GET['sSearch_' . 
                     $i] != '') {
                if($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . strtoupper($_GET['sSearch_' . 
                                                                          $i]). "%' ";
            }
        }
        if(isset($app)&& $app != "") {
            $arr_searchkey = explode(",", $app);
            $sName .= " AND (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {

                foreach($arr_searchkey as $k => $v) {
                    $sName .= "`" . $aColumns[$i] . "` LIKE '%" . strtoupper($v). "%' OR ";
                }
            }
            // die($sWhere);
            // 
            $sName = substr_replace($sName, "", - 3);
            $sName .= ')';
        }
        // Zend_Debug::dump($sName);die('cek');
        $qry = "SELECT 
			count(ID_ALERT) as rowsd 
		FROM 
		TBL_ALERT_FR 	 " . $sWhere . " " . $sName . " " . $whapp . " " . $sOrder;
        try {
            $qry = str_ireplace("`", "", $qry);
            // Zend_Debug::dump($qry);die();
            $data = $this->_db1->fetchOne($qry);
            // Zend_Debug::dump($sWhere);die();
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }

    function get_listalert($app, $GET) {
        $aColumns = array('no',
                          'IDPROFILE',
                          'NAMA',
                          'CAMERANAME');
        
        /*
         * Paging
         */
        $sLimit = "";
        if(isset($GET['iDisplayStart'])&& $GET['iDisplayLength'] != '-1') {
            $sLimit = "OFFSET " . intval($GET['iDisplayStart']). "ROWS FETCH NEXT " . intval($GET['iDisplayLength']). " ROWS ONLY";
        }
        
        /*
         * Ordering
         */
        $sOrder = "";
        if(isset($_GET['iSortCol_0'])&& intval($_GET['iSortCol_0'])> 0) {
            $sOrder = "ORDER BY  ";
            for($i = 1;
            $i < intval ($_GET['iSortingCols'] );
            $i ++ )//for ( $i=1 ; $i<count($aColumns) ; $i++ )
            {
                if($_GET['bSortable_' . 
                   intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= "`" . $aColumns[intval($_GET['iSortCol_' . 
                                                      $i])] . "` " .($_GET['sSortDir_' . 
                                                                     $i] === 'asc' ? 'asc' : 'desc'). ", ";
                }
            }
            $sOrder .= "`" . $aColumns[intval($_GET['iSortCol_0'])] . "` " .($_GET['sSortDir_0'] === 'asc' ? 'asc' : 'desc'). ", ";
            // echo $sOrder.'<br>';
            $sOrder = substr_replace($sOrder, "", - 2);
            if($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        } else {
            $sOrder = "ORDER BY  LAST_UPDATE";
        }
        
        /*
         * Filtering NOTE this does not match the built-in DataTables filtering which does it word by word on any field. It's possible to do here, but concerned about efficiency on very large tables, and MySQL's regex functionality is very limited
         */
        // $sWhere = "WHERE LOWER(ubis) LIKE 'c4%' ";
        $sWhere = " WHERE 1=1  ";
        if(isset($GET['sSearch'])&& $GET['sSearch'] != "") {
            $sWhere .= "  AND (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . strtoupper($GET['sSearch']). "%' OR ";
            }
            // die($sWhere);
            $sWhere = substr_replace($sWhere, "", - 3);
            $sWhere .= ')';
        }
        // if($app != "") {
        // $whapp = "AND app = '$app'";
        // }
        //die($sWhere);
        
        /* Individual column filtering */
        for($i = 1;
        $i < count ($aColumns );
        $i ++ ) {
            if(isset($GET['bSearchable_' . $i])&& $GET['bSearchable_' . 
                     $i] == "true" && $_GET['sSearch_' . 
                     $i] != '') {
                if($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                $sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . strtoupper($_GET['sSearch_' . 
                                                                          $i]). "%' ";
            }
        }
        if(isset($app)&& $app != "") {
            $arr_searchkey = explode(",", $app);
            $sName .= " AND (";
            for($i = 1;
            $i < count ($aColumns );
            $i ++ ) {

                foreach($arr_searchkey as $k => $v) {
                    $sName .= "`" . $aColumns[$i] . "` LIKE '%" . strtoupper($v). "%' OR ";
                }
            }
            // die($sWhere);
            // 
            $sName = substr_replace($sName, "", - 3);
            $sName .= ')';
        }
        // echo $sWhere.'<br>';
        // echo $sOrder.'<br>';
        // echo $sLimit.'<br>';
        // die();
        $qry = "SELECT *
			FROM TBL_ALERT_FR 
				 " . $sWhere . " " . $sName . " " . $whapp . " " . $sOrder . " " . $sLimit;
        // die($qry);
        try {
            $qry = str_ireplace("`", "", $qry);
            $data = $this->_db1->fetchAll($qry);
            // Zend_Debug::dump($qry);die();
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die($q);
        }
    }
}
