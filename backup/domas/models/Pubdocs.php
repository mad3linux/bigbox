<?php

/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
ini_set("memory_limit", "-1");
//ini_set("display_errors", "on");

class Domas_Model_Pubdocs extends Zend_Db_Table_Abstract {

    public function pub_get_list_server() {
    }

    public function pub_detect_change() {
    }

    public function pub_get_list_docs() {
    }

    public function pub_get_content() {
    }

    public function pub_index_doc() {
    }
}
