<?php

/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
ini_set("memory_limit", "-1");
//ini_set("display_errors", "on");

class Domas_Model_Pubgeneric extends Zend_Db_Table_Abstract {

    function connect_solr() {
        $config = new Zend_Config_Ini(APPLICATION_PATH . 
                                      '/configs/application.ini', 'production');
        $config = $config->toArray();
        $options = array('hostname' =>$config['data']['solr']['host'],
                         'login' =>$config['data']['solr']['host'],
                         'user' =>$config['data']['solr']['user'],
                         'password' =>$config['data']['solr']['password'],
                         'port' =>$config['data']['solr']['port'],
                         'path' =>$config['data']['solr']['path']);
        try {
            return new SolrClient($options);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    function connect_solr2() {
        $options = array('hostname' => '10.62.29.94',
                         'login' => '10.62.29.94',
                         'user' =>$config['data']['solr']['user'],
                         'password' => '',
                         'port' => '8983',
                         'path' => '/solr/pmasdev');
        try {
            return new SolrClient($options);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    public function search_for_updated() {
        //    die($key);
        try {
            $client = $this->connect_solr();
            $query2 = new SolrQuery();
            $query2 = new SolrQuery();
            $query2->setQuery('*:*');
            $query2->setStart(3501);
            $query2->setRows(1000);
            $query_response2 = $client->query($query2);
            $response2 = $query_response2->getResponse();
            $data2 = $response2->response->docs;
            if(!$response2->response->docs) {
                $data2 = array();
            }
            //Zend_Debug::dump( $response2);
            return $data2;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    public function insertdata($data, $overwrite = true) {
        //Zend_Debug::dump($data); die();  
        $client = $this->connect_solr2();
        $doc = new SolrInputDocument();
        try {
            if(isset($data['id'])&& $data['id'] != null) {
                $doc->addField('id', $data['id']);
            }
            if(isset($data['bundle'])&& $data['bundle'] != null) {
                $doc->addField('bundle', $data['bundle']);
            }
            if(isset($data['bundle_name'])&& $data['bundle_name'] != null) {
                $doc->addField('bundle_name', $data['bundle_name']);
            }
            if(isset($data['content'])&& $data['content'] != null) {
                $doc->addField('content', $data['content']);
            }
            if(isset($data['content_date'])&& $data['content_date'] != null) {
                $doc->addField('content_date', $data['content_date']);
            }
            if(isset($data['entity_id'])&& $data['entity_id'] != null) {
                $doc->addField('entity_id', $data['entity_id']);
            }
            if(isset($data['entity_type'])&& $data['entity_type'] != null) {
                $doc->addField('entity_type', $data['entity_type']);
            }
            if(isset($data['hash'])&& $data['hash'] != null) {
                $doc->addField('hash', $data['hash']);
            }
            if(isset($data['index_id'])&& $data['index_id'] != null) {
                $doc->addField('index_id', $data['index_id']);
            }
            if(isset($data['location'])&& $data['location'] != null) {
                $doc->addField('location', $data['location']);
            }
            if(isset($data['item_id'])&& $data['item_id'] != null) {
                $doc->addField('item_id', $data['item_id']);
            }
            if(isset($data['label'])&& $data['label'] != null) {
                $doc->addField('label', $data['label']);
            }
            if(isset($data['path'])&& $data['path'] != null) {
                $doc->addField('path', $data['path']);
            }
            if(isset($data['path_alias'])&& $data['path_alias'] != null) {
                $doc->addField('path_alias', $data['path_alias']);
            }
            if(isset($data['site'])&& $data['site'] != null) {
                $doc->addField('site', $data['site']);
            }
            if(isset($data['sort_label'])&& $data['sort_label'] != null) {
                $doc->addField('sort_label', $data['sort_label']);
            }
            if(isset($data['sort_search_api_id'])&& $data['sort_search_api_id'] != null) {
                $doc->addField('sort_search_api_id', $data['sort_search_api_id']);
            }
            if(isset($data['spell'])&& $data['spell'] != null) {
                $doc->addField('spell', $data['spell']);
            }
            if(isset($data['taxonomy_names'])&& $data['taxonomy_names'] != null && !is_array($data['taxonomy_names'])) {
                $doc->addField('taxonomy_names', $data['taxonomy_names']);
            } else if(is_array($data['taxonomy_names'])) {

                foreach($data['taxonomy_names'] as $v) {
                    $doc->addField('taxonomy_names', $v);
                }
            }
            if(isset($data['tid'])&& $data['tid'] != null && !is_array($data['tid'])) {
                $doc->addField('tid', $data['tid']);
            } else if(is_array($data['tid'])) {

                foreach($data['tid'] as $v) {
                    $doc->addField('tid', $v);
                }
            }
            if(isset($data['teaser'])&& $data['teaser'] != null) {
                $doc->addField('teaser', $data['teaser']);
            }
            if(isset($data['ds'])&& count($data['ds'])> 0) {

                foreach($data['ds'] as $k => $v) {
                    $doc->addField($k, $v);
                }
            }
            if(isset($data['dm'])&& count($data['dm'])> 0) {

                foreach($data['dm'] as $k => $v) {

                    foreach($v as $v2) {
                        $doc->addField($k, $v2);
                    }
                }
            }
            if(isset($data['is'])&& count($data['is'])> 0) {

                foreach($data['is'] as $k => $v) {
                    $doc->addField($k, $v);
                }
            }
            if(isset($data['im'])&& count($data['im'])> 0) {

                foreach($data['im'] as $k => $v) {

                    foreach($v as $v2) {
                        $doc->addField($k, $v2);
                    }
                }
            }
            if(isset($data['fs'])&& count($data['fs'])> 0) {

                foreach($data['fs'] as $k => $v) {
                    $doc->addField($k, $v);
                }
            }
            if(isset($data['fm'])&& count($data['fm'])> 0) {

                foreach($data['fm'] as $k => $v) {

                    foreach($v as $v2) {
                        $doc->addField($k, $v2);
                    }
                }
            }
            if(isset($data['ps'])&& count($data['ps'])> 0) {

                foreach($data['ps'] as $k => $v) {
                    $doc->addField($k, $v);
                }
            }
            if(isset($data['pm'])&& count($data['pm'])> 0) {

                foreach($data['pm'] as $k => $v) {

                    foreach($v as $v2) {
                        $doc->addField($k, $v2);
                    }
                }
            }
            if(isset($data['bs'])&& count($data['bs'])> 0) {

                foreach($data['bs'] as $k => $v) {
                    $doc->addField($k, $v);
                }
            }
            if(isset($data['bm'])&& count($data['bm'])> 0) {

                foreach($data['bm'] as $k => $v) {

                    foreach($v as $v2) {
                        $doc->addField($k, $v2);
                    }
                }
            }
            if(isset($data['ss'])&& count($data['ss'])> 0) {

                foreach($data['ss'] as $k => $v) {
                    $doc->addField($k, $v);
                }
            }
            if(isset($data['sm'])&& count($data['sm'])> 0) {

                foreach($data['sm'] as $k => $v) {

                    foreach($v as $v2) {
                        $doc->addField($k, $v2);
                    }
                }
            }
            if(isset($data['ts'])&& count($data['ts'])> 0) {

                foreach($data['ts'] as $k => $v) {
                    $doc->addField($k, $v);
                }
            }
            if(isset($data['tm'])&& count($data['tm'])> 0) {

                foreach($data['tm'] as $k => $v) {

                    foreach($v as $v2) {
                        $doc->addField($k, $v2);
                    }
                }
            }
            //  Zend_Debug::dump($doc); die();
            $updateResponse = $client->addDocument($doc, $overwrite);
        }
        catch(Exception $e) {
            return array('transaction' => false,
                         'result' => false,
                         'message' =>$e->getMessage());
        }
        return array('transaction' => true,
                     'result' => true,
                     'message' => 'transaction succed');
    }
}
