<?php

/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
ini_set("memory_limit", "-1");
//ini_set("display_errors", "on");

class Domas_Model_Puboperator extends Zend_Db_Table_Abstract {

    function insert_lucene($data) {
        // Zend_Debug::dump($data); die();
        $path =(APPLICATION_PATH . 
                '/indexes');
        //echo $path; die();
        //  die("xxxx");
        try {
            $index = Zend_Search_Lucene::open($path);
            $doc = new Zend_Search_Lucene_Document();
            $doc->addField(Zend_Search_Lucene_Field::UnIndexed('id', $data['id']));
            $doc->addField(Zend_Search_Lucene_Field::Text('bundle', $data['bundle']));
            $doc->addField(Zend_Search_Lucene_Field::Text('bundle_name', $data['bundle_name']));
            $doc->addField(Zend_Search_Lucene_Field::Text('content', $data['content']));
            $doc->addField(Zend_Search_Lucene_Field::Text('label', $data['label']));
            $doc->addField(Zend_Search_Lucene_Field::Text('entity_type', $data['entity_type']));
            $doc->addField(Zend_Search_Lucene_Field::Text('entity_id', $data['entity_id']));
            $doc->addField(Zend_Search_Lucene_Field::Text('content_date', $data['content_date']));
            $doc->addField(Zend_Search_Lucene_Field::Text('entity_id', $data['entity_id']));
            $doc->addField(Zend_Search_Lucene_Field::Text('is_flag', $data['is']['is_flag']));
            $doc->addField(Zend_Search_Lucene_Field::Text('is_approve', $data['is']['is_approve']));
            $doc->addField(Zend_Search_Lucene_Field::Text('is_sentiment', $data['is']['is_sentiment']));
            $doc->addField(Zend_Search_Lucene_Field::Text('is_mktime', $data['is']['is_mktime']));
            $doc->addField(Zend_Search_Lucene_Field::Text('ss_author', $data['ss']['ss_author']));
            $doc->addField(Zend_Search_Lucene_Field::Text('ss_category', $data['ss']['ss_category']));
            $doc->addField(Zend_Search_Lucene_Field::Text('ss_topic', $data['ss']['ss_topic']));
            $doc->addField(Zend_Search_Lucene_Field::Text('ss_icon', $data['ss']['ss_icon']));
            $doc->addField(Zend_Search_Lucene_Field::Text('ss_lang', $data['ss']['ss_lang']));
            $index->addDocument($doc);
            #Zend_Debug::dump($updateResponse); die();
        }
        catch(Exception $e) {
            //die("xxx");
            $index = Zend_Search_Lucene::create($path);
            $doc = new Zend_Search_Lucene_Document();
            $doc->addField(Zend_Search_Lucene_Field::UnIndexed('id', $data['id']));
            $doc->addField(Zend_Search_Lucene_Field::Text('bundle', $data['bundle']));
            $doc->addField(Zend_Search_Lucene_Field::Text('bundle_name', $data['bundle_name']));
            $doc->addField(Zend_Search_Lucene_Field::Text('content', $data['content']));
            $doc->addField(Zend_Search_Lucene_Field::Text('label', $data['label']));
            $doc->addField(Zend_Search_Lucene_Field::Text('entity_type', $data['entity_type']));
            $doc->addField(Zend_Search_Lucene_Field::Text('entity_id', $data['entity_id']));
            $doc->addField(Zend_Search_Lucene_Field::Text('content_date', $data['content_date']));
            $doc->addField(Zend_Search_Lucene_Field::Text('entity_id', $data['entity_id']));
            $doc->addField(Zend_Search_Lucene_Field::Text('is_flag', $data['is']['is_flag']));
            $doc->addField(Zend_Search_Lucene_Field::Text('is_approve', $data['is']['is_approve']));
            $doc->addField(Zend_Search_Lucene_Field::Text('is_sentiment', $data['is']['is_sentiment']));
            $doc->addField(Zend_Search_Lucene_Field::Text('is_mktime', $data['is']['is_mktime']));
            $doc->addField(Zend_Search_Lucene_Field::Text('ss_author', $data['ss']['ss_author']));
            $doc->addField(Zend_Search_Lucene_Field::Text('ss_category', $data['ss']['ss_category']));
            $doc->addField(Zend_Search_Lucene_Field::Text('ss_topic', $data['ss']['ss_topic']));
            $doc->addField(Zend_Search_Lucene_Field::Text('ss_icon', $data['ss']['ss_icon']));
            $doc->addField(Zend_Search_Lucene_Field::Text('ss_lang', $data['ss']['ss_lang']));
            $index->addDocument($doc);
            return array('transaction' => true,
                         'result' => false,
                         'message' =>$e->getMessage());
        }
        return array('transaction' => true,
                     'result' => true,
                     'message' => 'transaction succed');
    }

    public function pub_insert_index_mysql($data) {
        //Zend_Debug::dump($data); die();
        $params = $data;
        unset($params['indexes']);
        $attrs = base64_encode(serialize($params));

        foreach($data['indexes'] as $k => $v) {
            $this->_db->query("insert into pmas_crawl_index (index_url, attrs, media_id, sub_domain, md5_url) values (?, ?, ?, ?, ?) ", array($v, $attrs, $data['media_id'], $data['subdomain'], md5($v)));
        }
        return true;
    }

    public function pub_get_lucene($data) {
        $data['xrules'] = 'detik';
        $path =(APPLICATION_PATH . 
                '/indexes');
        if($data['rules'] == null and $data['xrules'] != null) {
            $strung = $data['xrules'];
            $query = Zend_Search_Lucene_Search_QueryParser::parse("content:{$strung}");
            $index = Zend_Search_Lucene::open($path);
            $hits = $index->find($query);
        } else if($data['rules'] != null and $data['xrules'] != null) {
            $strung = $data['xrules'];
            $query = Zend_Search_Lucene_Search_QueryParser::parse($strung);
            $index = Zend_Search_Lucene::open($path);
            $hits = $index->find($query);
        }

        foreach($hits as $key => $hit) {
            $new[$key]["bundle"] = $hit->bundle;
            $new[$key]["bundle_name"] = $hit->bundle_name;
            $new[$key]["content"] = $hit->content;
            $new[$key]["label"] = $hit->label;
            $new[$key]['entity_type'] = $hit->entity_type;
            $new[$key]['entity_id'] = $hit->entity_id;
            $new[$key]['content_date'] = $hit->content_date;
            $new[$key]['entity_id'] = $hit->entity_id;
            $new[$key]['is_flag'] = $hit->is_flag;
            $new[$key]['is_approve'] = $hit->is_approve;
            $new[$key]['is_sentiment'] = $hit->is_sentiment;
            $new[$key]['is_mktime'] = $hit->is_mktime;
            $new[$key]['ss_author'] = $hit->ss_author;
            $new[$key]['ss_category'] = $hit->ss_category;
            $new[$key]['ss_topic'] = $hit->ss_topic;
            $new[$key]['ss_icon'] = $hit->ss_icon;
            $new[$key]['ss_lang'] = $hit->ss_lang;
        }
        return $new;
    }

    public function pub_insert_into_lucene($data) {
        //Zend_Debug::dump($data);
        //die();
        $sent = new \PHPInsight\Sentiment();
        $senArr = array('pos' => 2,
                        'neu' => 0,
                        'neg' => 1);
        $senStatus = array(2 => "POSITIF",
                           0 => "NETRAL",
                           1 => "NEGATIF");

        foreach($data['content'] as $k => $v) {
            $idata['id'] = $k;
            $idata['bundle'] = $v['bundle'];
            $idata['content'] = $v['content'];
            $idata['label'] = $v['title'];
            $idata['bundle_name'] = $v['bundle_name'];
            $idata['entity_type'] = 'data-html';
            $idata['entity_id'] = '2';
            $idata['content_date'] = $v['date'];
            $idata['is']['is_flag'] = 0;
            $idata['is']['is_sentiment'] = $senArr[$sent->categorise($v['title'])];
            $idata['is']['is_approve'] = 0;
            $idata['is']['is_mktime'] = time();
            $idata['is']['is_mediatype'] = $data['fetch']['media_type'];
            $idata['ss']['ss_author'] = $v['author'];
            $idata['ss']['ss_category'] = $v['category'];
            $idata['ss']['ss_topic'] = $v['topic'];
            $idata['ss']['ss_icon'] = $v['icon'];
            $idata['ss']['ss_lang'] = $data['fetch']['language'];
            try {
                $u = $this->insert_lucene($idata);
            }
            catch(Exception $e) {
                //die("2");
                Zend_Debug::dump($e->getMessage());
                die();
                //    }
            }
        }
        //die("3");
        return array("transaction" => true);
    }

    public function pub_get_url_frommysql() {
        $data = $this->_db->fetchAll("select * from pmas_crawl_index where log_crawl=0  order by media_id, sub_domain limit 0, 50");
        $k = 0;
        //Zend_Debug::dump($data);
        //Zend_Debug::dump(unserialize(base64_decode($data[0]['attrs'])));
        // die();

        foreach($data as $v) {
            // Zend_Debug::dump($v);
            //$new[$v['media_id'] . "_" . $v['sub_domain']]['indexes'][] = "asd";
            $new[$v['media_id'] . "_" . $v['sub_domain']] = unserialize(base64_decode($v['attrs']));
            $var[] = $v['index_url'];
            $new[$v['media_id'] . "_" . $v['sub_domain']]['indexes'] = $var;
            $new[$v['media_id'] . "_" . $v['sub_domain']]['sources'] = 'mysql';
            $k ++;
        }
        //Zend_Debug::dump($new); die();
        return $new;
    }

    public function xxxpub_get_tax_lucene() {
        #die("x");
        $sent = new \PHPInsight\Sentiment();
        $senArr = array('pos' => 2,
                        'neu' => 0,
                        'neg' => 1);
        $clas_pda = new Domas_Model_Withsolarium();
        $new = $this->_db->fetchAll("select name as tags, tid  from zpraba_taxonomy_term_data where vid=$vid and is_crawl=1");
        $cc = new Domas_Model_Withsolrpmas();
        $sending = new Domas_Model_Pmessaging();
        //Zend_Debug::dump($sending); die();
        $gg = new Domas_Model_Predictive();
        $files = glob(APPLICATION_PATH . 
                      '/../public/predictive/*/*');

        foreach($files as $k => $f) {
            $varx[$k] = explode('/', $f);
            $vvv[] = strtolower(end($varx[$k]));
        }
        //Zend_Debug::dump($vvv); die();

        foreach($new as $v) {
            $cnt = array();
            $cnt = $cc->search_for_sending_riil(trim($v['tags']));
            //Zend_Debug::dump($cnt);// die();
            if(count($cnt)> 0) {

                foreach($cnt as $doc) {
                    if($doc['ss_lang'] == 'en') {
                        $sent->setDataFolder(APPLICATION_PATH . 
                                             '/../public/pmas/sentiment/data/' . 
                                             $doc['ss_lang'] . 
                                             '/general/');
                    }
                    $var[$doc['id']]['content'] = $doc;
                    $var[$doc['id']]['tags'][] = $v['tags'];
                    $var[$doc['id']]['id'][] = $v['tid'];
                    $var[$doc['id']]['entity_id'][] = $doc['entity_id'];
                    
                    /*
                     if($doc['entity_id'] == 2) {
                     $var[$doc['id']]['sentiment'][] = $senArr[$sent->categorise($doc['label'])];
                     foreach($vvv as $x) {
                     $paramsx = array('tid' =>$x,
                     'stype' => 'news');
                     if(file_exists(APPLICATION_PATH . '/../public/predictive/news/' . $x . '')) {
                     $var[$doc['id']]['cat'][$x] = $gg->category(substr($doc['content'], 0, 2000), $paramsx);
                     }
                     }
                     } elseif($doc['entity_id'] == 5) {
                     $var[$doc['id']]['sentiment'][] = $senArr[$sent->categorise($doc['content'])];
                     foreach($vvv as $x) {
                     $paramsx = array('tid' =>$x,
                     'stype' => 'socmed');
                     if(file_exists(APPLICATION_PATH . '/../public/predictive/socmed/' . $x . '')) {
                     $var[$doc['id']]['cat'][$x] = $gg->category(substr($doc['content'], 0, 2000), $paramsx);
                     }
                     }
                     } elseif($doc['entity_id'] == 6) {
                     $var[$doc['id']]['sentiment'][] = $senArr[$sent->categorise($doc['content'])];
                     foreach($vvv as $x) {
                     $paramsx = array('tid' =>$x,
                     'stype' => 'lapin');
                     if(file_exists(APPLICATION_PATH . '/../public/predictive/lapin/' . $x . '')) {
                     $var[$doc['id']]['tags'][] = '';
                     $var[$doc['id']]['id'][] = '';
                     $var[$doc['id']]['cat'][$x] = $gg->category(substr($doc['content'], 0, 2000), $paramsx);
                     }
                     }
                     }
                     
                     
                     */
                }
            }
        }
        return $var;
        // Zend_Debug::dump($var); die();
    }

    public function pub_get_taxonomy() {
        $data = $this->_db->fetchAll("select * from zpraba_taxonomy_vocabulary where hierarchy=1");
        $i = 0;

        foreach($data as $v) {
            //$d2[$v['vid']]['detail'] = 
            // $d2[$v['vid']]['data'] = $v;
            $new[$v['vid']] = $this->_db->fetchAll("select * from zpraba_taxonomy_term_data where vid=? and is_crawl=1", $v['vid']);
            // Zend_Debug::dump($new[$v['vid']]); 

            foreach($new[$v['vid']] as $k2 => $v2) {
                if($v2['rules'] == "") {
                    $d2[$i] = $v2;
                    $d2[$i]['xrules'] = $v2['name'];
                    $d2[$i]['language'] = $v['language'];
                    $i ++;
                } else {
                    $vAr = array();
                    $vAr = unserialize(base64_decode($v2['rules']));

                    foreach($vAr as $k3 => $v3) {
                        $d2[$i] = $v2;
                        $d2[$i]['xrules'] = $v3;
                        $d2[$i]['language'] = $v['language'];
                        $i ++;
                    }
                }
            }
        }
        return $d2;
    }

    public function pub_get_tax_lucene($data) {
        $ccc = new Domas_Model_Params();
        $p = $ccc->get_params_pmas(true);
        //Zend_Debug::dump($data); die();
        if($p['IsActiveSentimentPmas'] == 1) {
        }
        $senArr = array('pos' => 2,
                        'neu' => 0,
                        'neg' => 1);
        $cc = new Domas_Model_Withsolrpmas();

        foreach($data as $v) {
            // $cnt = array();
            $cnt[] = $this->pub_get_lucene($v);
            
            /*
             if(count($cnt)> 0) {
             foreach($cnt as $doc) {
             if($doc['ss_lang'] == 'en') {
             $sent->setDataFolder(APPLICATION_PATH .
             '/../public/pmas/sentiment/data/' .
             $doc['ss_lang'] .
             '/general/');
             }
             $var[$doc['id']]['content'] = $doc;
             $var[$doc['id']]['tags'][] = $v['tags'];
             $var[$doc['id']]['id'][] = $v['tid'];
             $var[$doc['id']]['entity_id'][] = $doc['entity_id'];
             
             /*
             if($doc['entity_id'] == 2) {
             $var[$doc['id']]['sentiment'][] =           $senArr[$sent->categorise($doc['label'])];
             foreach($vvv as $x) {
             $paramsx = array('tid' =>$x,
             'stype' => 'news');
             if(file_exists(APPLICATION_PATH . '/../public/predictive/news/' . $x . '')) {
             $var[$doc['id']]['cat'][$x] = $gg->category(substr($doc['content'], 0, 2000), $paramsx);
             }
             }
             } elseif($doc['entity_id'] == 5) {
             $var[$doc['id']]['sentiment'][] = $senArr[$sent->categorise($doc['content'])];
             foreach($vvv as $x) {
             $paramsx = array('tid' =>$x,
             'stype' => 'socmed');
             if(file_exists(APPLICATION_PATH . '/../public/predictive/socmed/' . $x . '')) {
             $var[$doc['id']]['cat'][$x] = $gg->category(substr($doc['content'], 0, 2000), $paramsx);
             }
             }
             } elseif($doc['entity_id'] == 6) {
             $var[$doc['id']]['sentiment'][] = $senArr[$sent->categorise($doc['content'])];
             foreach($vvv as $x) {
             $paramsx = array('tid' =>$x,
             'stype' => 'lapin');
             if(file_exists(APPLICATION_PATH . '/../public/predictive/lapin/' . $x . '')) {
             $var[$doc['id']]['tags'][] = '';
             $var[$doc['id']]['id'][] = '';
             $var[$doc['id']]['cat'][$x] = $gg->category(substr($doc['content'], 0, 2000), $paramsx);
             }
             }
             }
             
             
             */
            //}
            //}
        }
        Zend_Debug::dump($cnt);
        die();
        return $var;
        // Zend_Debug::dump($var); die();
    }

    public function pub_get_tax_solr() {
        $sent = new \PHPInsight\Sentiment();
        $senArr = array('pos' => 2,
                        'neu' => 0,
                        'neg' => 1);
        $vid = 14;
        $clas_pda = new Domas_Model_Withsolarium();
        $new = $this->_db->fetchAll("select name as tags, tid  from zpraba_taxonomy_term_data where vid=$vid and is_crawl=1");
        Zend_Debug::dump($new);
        die();
        $cc = new Domas_Model_Withsolrpmas();
        $sending = new Domas_Model_Pmessaging();
        //Zend_Debug::dump($sending); die();
        $gg = new Domas_Model_Predictive();
        $files = glob(APPLICATION_PATH . 
                      '/../public/predictive/*/*');

        foreach($files as $k => $f) {
            $varx[$k] = explode('/', $f);
            $vvv[] = strtolower(end($varx[$k]));
        }
        //Zend_Debug::dump($vvv); die();

        foreach($new as $v) {
            $cnt = array();
            $cnt = $cc->search_for_sending_riil(trim($v['tags']));
            //Zend_Debug::dump($cnt);// die();
            if(count($cnt)> 0) {

                foreach($cnt as $doc) {
                    if($doc['ss_lang'] == 'en') {
                        $sent->setDataFolder(APPLICATION_PATH . 
                                             '/../public/pmas/sentiment/data/' . 
                                             $doc['ss_lang'] . 
                                             '/general/');
                    }
                    $var[$doc['id']]['content'] = $doc;
                    $var[$doc['id']]['tags'][] = $v['tags'];
                    $var[$doc['id']]['id'][] = $v['tid'];
                    $var[$doc['id']]['entity_id'][] = $doc['entity_id'];
                    
                    /*
                     if($doc['entity_id'] == 2) {
                     $var[$doc['id']]['sentiment'][] = $senArr[$sent->categorise($doc['label'])];
                     foreach($vvv as $x) {
                     $paramsx = array('tid' =>$x,
                     'stype' => 'news');
                     if(file_exists(APPLICATION_PATH . '/../public/predictive/news/' . $x . '')) {
                     $var[$doc['id']]['cat'][$x] = $gg->category(substr($doc['content'], 0, 2000), $paramsx);
                     }
                     }
                     } elseif($doc['entity_id'] == 5) {
                     $var[$doc['id']]['sentiment'][] = $senArr[$sent->categorise($doc['content'])];
                     foreach($vvv as $x) {
                     $paramsx = array('tid' =>$x,
                     'stype' => 'socmed');
                     if(file_exists(APPLICATION_PATH . '/../public/predictive/socmed/' . $x . '')) {
                     $var[$doc['id']]['cat'][$x] = $gg->category(substr($doc['content'], 0, 2000), $paramsx);
                     }
                     }
                     } elseif($doc['entity_id'] == 6) {
                     $var[$doc['id']]['sentiment'][] = $senArr[$sent->categorise($doc['content'])];
                     foreach($vvv as $x) {
                     $paramsx = array('tid' =>$x,
                     'stype' => 'lapin');
                     if(file_exists(APPLICATION_PATH . '/../public/predictive/lapin/' . $x . '')) {
                     $var[$doc['id']]['tags'][] = '';
                     $var[$doc['id']]['id'][] = '';
                     $var[$doc['id']]['cat'][$x] = $gg->category(substr($doc['content'], 0, 2000), $paramsx);
                     }
                     }
                     }
                     
                     
                     */
                }
            }
        }
        return $var;
        // Zend_Debug::dump($var); die();
    }

    public function pub_sentiment_nlp($var) {
        Zend_Debug::dump($var);
        die();
        $sent = new \PHPInsight\Sentiment();
        $senArr = array('pos' => 2,
                        'neu' => 0,
                        'neg' => 1);
        $clas_pda = new Domas_Model_Withsolarium();
        $cnt = array();
        //  $cnt = $cc->search_for_sending_lapin();        
        // Zend_Debug::dump($var); die();
        $senStatus = array(2 => "POSITIF",
                           0 => "NETRAL",
                           1 => "NEGATIF");

        foreach($var as $k => $v) {
            $clas_pda->update_flags(1, $k, $var[$k]['id'], $var[$k]['sentiment'][0], $var[$k]['cat']);
        }
        return true;
    }

    public function pub_update_kategorisasi() {
    }

    public function pub_api_sigma($cat, $time) {
        $vid = 13;
        $new = $this->_db->fetchAll("select name as tags, tid  from zpraba_taxonomy_term_data where vid=$vid and attr4='$cat'");
        //Zend_Debug::dump($new); die();
        $cc = new Domas_Model_Withsolrpmas();
        $delta = mktime()- $time;

        foreach($new as $v) {
            $cnt = array();
            $cnt = $cc->search_for_sending_tstamp(trim($v['tags']), $delta);
            #Zend_Debug::dump($cnt);die(); 
            if(count($cnt)> 0) {

                foreach($cnt as $doc) {
                    $var[base64_encode($doc['id'])]['id'] = base64_encode($doc['id']);
                    $var[base64_encode($doc['id'])]['content'] = $doc;
                    $var[base64_encode($doc['id'])]['tags'][] = $v['tags'];
                    $var[base64_encode($doc['id'])]['tid'][] = $v['tid'];
                    $var[base64_encode($doc['id'])]['entity_id'][] = $doc['entity_id'];
                    $var[base64_encode($doc['id'])]['cat'] = $cat;
                }
            }
        }
        return $var;
    }

    public function pub_updated_data() {
        $sent = new \PHPInsight\Sentiment();
        $senArr = array('pos' => 2,
                        'neu' => 0,
                        'neg' => 1);
        $vid = 10;
        $clas_pda = new Domas_Model_Withsolarium();
        $new = $this->_db->fetchAll("select name as tags, tid  from zpraba_taxonomy_term_data where vid=$vid");
        //Zend_Debug::dump($new); die();
        $cc = new Domas_Model_Withsolrpmas();
        $sending = new Domas_Model_Pmessaging();

        foreach($new as $v) {
            $cnt = array();
            $cnt = $cc->search_for_updated(trim($v['tags']));
            //Zend_Debug::dump($cnt);die(); 
            if(count($cnt)> 0) {

                foreach($cnt as $doc) {
                    $var[$doc['id']]['content'] = $doc;
                    $var[$doc['id']]['tags'][] = $v['tags'];
                    $var[$doc['id']]['tags'][] = $v['tags'];
                    $var[$doc['id']]['id'][] = $v['tid'];
                    $var[$doc['id']]['entity_id'][] = $doc['entity_id'];
                    if($doc['entity_id'] == 2) {
                        $var[$doc['id']]['sentiment'][] = $senArr[$sent->categorise($doc['label'])];
                    } elseif($doc['entity_id'] == 5) {
                        $var[$doc['id']]['sentiment'][] = $senArr[$sent->categorise($doc['content'])];
                    }
                }
            }
        }
        $senStatus = array(2 => "POSITIF",
                           0 => "NETRAL",
                           1 => "NEGATIF");
        // Zend_Debug::dump($var);die(); 

        foreach($var as $k => $v) {
            $clas_pda->update_flags(1, $k, $var[$k]['id'], $var[$k]['sentiment'][0]);
        }
        die("ccc");
        return 1;
    }

    public function pub_sending_riil_en() {
        $sent = new \PHPInsight\Sentiment();
        $senArr = array('pos' => 2,
                        'neu' => 0,
                        'neg' => 1);
        $vid = 8;
        $clas_pda = new Domas_Model_Withsolarium();
        $new = $this->_db->fetchAll("select name as tags, tid  from zpraba_taxonomy_term_data where vid=$vid and is_crawl=1");
        //Zend_Debug::dump($new); die();
        $cc = new Domas_Model_Withsolrpmas();
        $sending = new Domas_Model_Pmessaging();

        foreach($new as $v) {
            $cnt = array();
            $cnt = $cc->search_for_sending_riil_en(trim($v['tags']));
            //Zend_Debug::dump($cnt); //die();
            if(count($cnt)> 0) {

                foreach($cnt as $doc) {
                    if($doc['ss_lang'] == 'id') {
                        $sent->setDataFolder(APPLICATION_PATH . 
                                             '/../public/pmas/sentiment/data/');
                    }
                    $var[$doc['id']]['content'] = $doc;
                    $var[$doc['id']]['tags'][] = $v['tags'];
                    $var[$doc['id']]['id'][] = $v['tid'];
                    $var[$doc['id']]['entity_id'][] = $doc['entity_id'];
                    if($doc['entity_id'] == 2) {
                        $var[$doc['id']]['sentiment'][] = $senArr[$sent->categorise($doc['label'])];
                    } elseif($doc['entity_id'] == 5) {
                        $var[$doc['id']]['sentiment'][] = $senArr[$sent->categorise($doc['content'])];
                    } elseif($doc['entity_id'] == 4) {
                        $var[$doc['id']]['sentiment'][] = $senArr[$sent->categorise($doc['content'])];
                    }
                }
            }
        }
        $senStatus = array(2 => "POSITIF",
                           0 => "NETRAL",
                           1 => "NEGATIF");

        foreach($var as $k => $v) {
            $mess = "";
            $mess .= "ID : " . time(). " \n";
            $mess .= "BERITA SENTIMEN- " . $senStatus[$var[$k]['sentiment'][0]] . "\n";
            $mess .= "KEYWORDS : (" . implode(",", $var[$k]['tags']). ")\n";
            $mess .= "NAMA-MEDIA  : " . $var[$k]['content']['bundle_name'] . "\n";
            $mess .= "URL  : " . $var[$k]['content']['id'] . ")\n";
            $date = array();
            $date = explode("T", $var[$k]['content']['content_date']);
            $mess .= "WAKTU  : " . $date[0] . " " . $date[1] . "\n";
            $mess .= "ISI  : " . $var[$k]['content']['content'] . "\n";
            if($var[$k]['content']['entity_id'] == 5) {
                //	$sending->sending_message($mess, '-15036680');
                //Zend_Debug::dump($var[$k]['sentiment']); 
            } elseif($var[$k]['content']['entity_id'] == 2) {
                $sending->sending_message($mess);
            }
            //Zend_Debug::dump($mess); 
            $clas_pda->update_flags(1, $k, $var[$k]['id'], $var[$k]['sentiment'][0]);
        }
        die("ccc");
        return 1;
    }

    public function pub_sending_lapin() {
        #die("x");
        $sent = new \PHPInsight\Sentiment();
        $senArr = array('pos' => 2,
                        'neu' => 0,
                        'neg' => 1);
        $gg = new Domas_Model_Predictive();
        $files = glob(APPLICATION_PATH . 
                      '/../public/predictive/*/*');

        foreach($files as $k => $f) {
            $varx[$k] = explode('/', $f);
            $vvv[] = strtolower(end($varx[$k]));
        }
        $cc = new Domas_Model_Withsolrpmas();
        $cnt = array();
        $cnt = $cc->search_for_sending_lapin();
        //	Zend_Debug::dump($cnt);die();
        if(count($cnt)> 0) {

            foreach($cnt as $doc) {
                if($doc['ss_lang'] == 'en') {
                    $sent->setDataFolder(APPLICATION_PATH . 
                                         '/../public/pmas/sentiment/data/' . 
                                         $doc['ss_lang'] . 
                                         '/general/');
                }
                $var[$doc['id']]['content'] = $doc;
                $var[$doc['id']]['tags'][] = $v['tags'];
                $var[$doc['id']]['id'][] = $v['tid'];
                $var[$doc['id']]['entity_id'][] = $doc['entity_id'];
                if($doc['entity_id'] == 6) {
                    $var[$doc['id']]['sentiment'][] = $senArr[$sent->categorise($doc['content'])];
                    if(file_exists(APPLICATION_PATH . '/../public/predictive/lapin/' . $x . '')) {
                        $var[$doc['id']]['tags'][] = '';
                        $var[$doc['id']]['id'][] = '';
                        $var[$doc['id']]['cat'][$x] = $gg->category(substr($doc['content'], 0, 2000), $paramsx);
                    }
                }
            }
        }
        Zend_Debug::dump($var);
        die();
        $senStatus = array(2 => "POSITIF",
                           0 => "NETRAL",
                           1 => "NEGATIF");

        foreach($var as $k => $v) {
            $mess = "";
            $mess .= "ID : " . time(). " \n";
            $mess .= "BERITA SENTIMEN- " . $senStatus[$var[$k]['sentiment'][0]] . "\n";
            $mess .= "KEYWORDS : (" . implode(",", $var[$k]['tags']). ")\n";
            $mess .= "NAMA-MEDIA  : " . $var[$k]['content']['bundle_name'] . "\n";
            $mess .= "URL  : " . $var[$k]['content']['id'] . ")\n";
            $date = array();
            $date = explode("T", $var[$k]['content']['content_date']);
            $mess .= "WAKTU  : " . $date[0] . " " . $date[1] . "\n";
            $mess .= "ISI  : " . $var[$k]['content']['content'] . "\n";
            $clas_pda->update_flags(1, $k, $var[$k]['id'], $var[$k]['sentiment'][0], $var[$k]['cat']);
        }
        die("ccc");
        return 1;
    }

    public function pub_sending_riil() {
        #die("x");
        $sent = new \PHPInsight\Sentiment();
        $senArr = array('pos' => 2,
                        'neu' => 0,
                        'neg' => 1);
        $vid = 14;
        $clas_pda = new Domas_Model_Withsolarium();
        $new = $this->_db->fetchAll("select name as tags, tid  from zpraba_taxonomy_term_data where vid=$vid and is_crawl=1");
        $cc = new Domas_Model_Withsolrpmas();
        $sending = new Domas_Model_Pmessaging();
        //Zend_Debug::dump($sending); die();
        $gg = new Domas_Model_Predictive();
        $files = glob(APPLICATION_PATH . 
                      '/../public/predictive/*/*');

        foreach($files as $k => $f) {
            $varx[$k] = explode('/', $f);
            $vvv[] = strtolower(end($varx[$k]));
        }
        //Zend_Debug::dump($vvv); die();

        foreach($new as $v) {
            $cnt = array();
            $cnt = $cc->search_for_sending_riil(trim($v['tags']));
            //Zend_Debug::dump($cnt);// die();
            if(count($cnt)> 0) {

                foreach($cnt as $doc) {
                    if($doc['ss_lang'] == 'en') {
                        $sent->setDataFolder(APPLICATION_PATH . 
                                             '/../public/pmas/sentiment/data/' . 
                                             $doc['ss_lang'] . 
                                             '/general/');
                    }
                    $var[$doc['id']]['content'] = $doc;
                    $var[$doc['id']]['tags'][] = $v['tags'];
                    $var[$doc['id']]['id'][] = $v['tid'];
                    $var[$doc['id']]['entity_id'][] = $doc['entity_id'];
                    if($doc['entity_id'] == 2) {
                        $var[$doc['id']]['sentiment'][] = $senArr[$sent->categorise($doc['label'])];

                        foreach($vvv as $x) {
                            $paramsx = array('tid' =>$x,
                                             'stype' => 'news');
                            if(file_exists(APPLICATION_PATH . '/../public/predictive/news/' . $x . '')) {
                                $var[$doc['id']]['cat'][$x] = $gg->category(substr($doc['content'], 0, 2000), $paramsx);
                            }
                        }
                    } elseif($doc['entity_id'] == 5) {
                        $var[$doc['id']]['sentiment'][] = $senArr[$sent->categorise($doc['content'])];

                        foreach($vvv as $x) {
                            $paramsx = array('tid' =>$x,
                                             'stype' => 'socmed');
                            if(file_exists(APPLICATION_PATH . '/../public/predictive/socmed/' . $x . '')) {
                                $var[$doc['id']]['cat'][$x] = $gg->category(substr($doc['content'], 0, 2000), $paramsx);
                            }
                        }
                    } elseif($doc['entity_id'] == 6) {
                        $var[$doc['id']]['sentiment'][] = $senArr[$sent->categorise($doc['content'])];

                        foreach($vvv as $x) {
                            $paramsx = array('tid' =>$x,
                                             'stype' => 'lapin');
                            if(file_exists(APPLICATION_PATH . '/../public/predictive/lapin/' . $x . '')) {
                                $var[$doc['id']]['tags'][] = '';
                                $var[$doc['id']]['id'][] = '';
                                $var[$doc['id']]['cat'][$x] = $gg->category(substr($doc['content'], 0, 2000), $paramsx);
                            }
                        }
                    }
                }
            }
        }
        $cnt = array();
        //  $cnt = $cc->search_for_sending_lapin();        
        // Zend_Debug::dump($var); die();
        $senStatus = array(2 => "POSITIF",
                           0 => "NETRAL",
                           1 => "NEGATIF");

        foreach($var as $k => $v) {
            $mess = "";
            $mess .= "ID : " . time(). " \n";
            $mess .= "BERITA SENTIMEN- " . $senStatus[$var[$k]['sentiment'][0]] . "\n";
            $mess .= "KEYWORDS : (" . implode(",", $var[$k]['tags']). ")\n";
            $mess .= "NAMA-MEDIA  : " . $var[$k]['content']['bundle_name'] . "\n";
            $mess .= "URL  : " . $var[$k]['content']['id'] . ")\n";
            $date = array();
            $date = explode("T", $var[$k]['content']['content_date']);
            $mess .= "WAKTU  : " . $date[0] . " " . $date[1] . "\n";
            $mess .= "ISI  : " . $var[$k]['content']['content'] . "\n";
            if($var[$k]['content']['entity_id'] == 5) {
                //	$sending->sending_message($mess);
                //Zend_Debug::dump($var[$k]['sentiment']); 
            } elseif($var[$k]['content']['entity_id'] == 2) {
                $sending->sending_message($mess);
            }
            //Zend_Debug::dump($mess); 
            $clas_pda->update_flags(1, $k, $var[$k]['id'], $var[$k]['sentiment'][0], $var[$k]['cat']);
        }
        die("ccc");
        return 1;
    }

    public function pub_raw_last() {
        $sent = new \PHPInsight\Sentiment();
        $senArr = array('pos' => 2,
                        'neu' => 0,
                        'neg' => 1);
        $vid = 14;
        $clas_pda = new Domas_Model_Withsolarium();
        $new = $this->_db->fetchAll("select name as tags, tid  from zpraba_taxonomy_term_data where vid=$vid and is_crawl=1");
        $cc = new Domas_Model_Withsolrpmas();
        $sending = new Domas_Model_Pmessaging();

        foreach($new as $v) {
            $cnt = array();
            $cnt = $cc->search_for_sending_raw(trim($v['tags']));
            //Zend_Debug::dump($cnt);// die();
            if(count($cnt)> 0) {

                foreach($cnt as $doc) {
                    if($doc['ss_lang'] == 'en') {
                        $sent->setDataFolder(APPLICATION_PATH . 
                                             '/../public/pmas/sentiment/data/' . 
                                             $doc['ss_lang'] . 
                                             '/general/');
                    }
                    $var[$doc['id']]['content'] = $doc;
                    $var[$doc['id']]['tags'][] = $v['tags'];
                    $var[$doc['id']]['id'][] = $v['tid'];
                    $var[$doc['id']]['entity_id'][] = $doc['entity_id'];
                    if($doc['entity_id'] == 2) {
                        $var[$doc['id']]['sentiment'][] = $senArr[$sent->categorise($doc['label'])];
                    } elseif($doc['entity_id'] == 5) {
                        $var[$doc['id']]['sentiment'][] = $senArr[$sent->categorise($doc['content'])];
                    } elseif($doc['entity_id'] == 4) {
                        $var[$doc['id']]['sentiment'][] = $senArr[$sent->categorise($doc['content'])];
                    }
                }
            }
        }
        //Zend_Debug::dump($var); die();
        $senStatus = array(2 => "POSITIF",
                           0 => "NETRAL",
                           1 => "NEGATIF");

        foreach($var as $k => $v) {
            $mess = "";
            $mess .= "ID : " . time(). " \n";
            $mess .= "BERITA SENTIMEN- " . $senStatus[$var[$k]['sentiment'][0]] . "\n";
            $mess .= "KEYWORDS : (" . implode(",", $var[$k]['tags']). ")\n";
            $mess .= "NAMA-MEDIA  : " . $var[$k]['content']['bundle_name'] . "\n";
            $mess .= "URL  : " . $var[$k]['content']['id'] . ")\n";
            $date = array();
            $date = explode("T", $var[$k]['content']['content_date']);
            $mess .= "WAKTU  : " . $date[0] . " " . $date[1] . "\n";
            $mess .= "ISI  : " . $var[$k]['content']['content'] . "\n";
            if($var[$k]['content']['entity_id'] == 5) {
                //	$sending->sending_message($mess);
                //Zend_Debug::dump($var[$k]['sentiment']); 
            } elseif($var[$k]['content']['entity_id'] == 2) {
                // $sending->sending_message($mess);
            }
            //Zend_Debug::dump($mess); 
            $clas_pda->update_flags(1, $k, $var[$k]['id'], $var[$k]['sentiment'][0]);
        }
        die("ccc");
        return 1;
    }
}
