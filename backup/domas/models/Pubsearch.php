<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3000);

class Domas_Model_Pubsearch extends Zend_Db_Table_Abstract {

    public function pub_get_suggestion_solr($data) {
        if(strlen($data['key'])<= 3) {
            return array('transaction' => false,
                         'message' => '4 characters minimum',
                         'data' => array());
        }
        $cc = new Domas_Model_Withsolr();
        $data = $cc->searching_suggestion($data['key']);
        return array('transaction' => true,
                     'message' => 'success',
                     'data' =>$data);
    }

    public function pub_get_suggestion($data) {
        try {
            $data = $this->_db->fetchAll("select keyw  from z_keyw_logs where keyw like '%" . 
                                         $data['key'] . 
                                         "%' limit 20");
        }
        catch(Exception $e) {
        }
        return array('transaction' => true,
                     'message' => 'success',
                     'data' =>$data);
    }

    public function pub_get_taxonomy() {
        try {
            $data = $this->_db->fetchAll("select * from zpraba_taxonomy_vocabulary where hierarchy=1");

            foreach($data as $v) {
                $datax = array();
                $ne[$v['vid']]['name'] = $v['name'];
                $datax = $this->_db->fetchAll("select * from zpraba_taxonomy_term_data where vid=" . 
                                              $v['vid'] . 
                                              " and parent=0");
                if(count($datax)> 0) {

                    foreach($datax as $vx) {
                        $ne[$v['vid']]['detail'][$vx['tid']]['name'] = $vx['name'];
                        $datay = array();
                        $datay = $this->_db->fetchAll("select * from zpraba_taxonomy_term_data where vid=" . 
                                                      $v['vid'] . 
                                                      " and parent=" . 
                                                      $vx['tid']);
                        if(count($datay)> 0) {

                            foreach($datay as $vy) {
                                $ne[$v['vid']]['detail'][$vx['tid']]['detail'][$vy['tid']]['name'] = $vy['name'];
                                $dataz = array();
                                $dataz = $this->_db->fetchAll("select * from zpraba_taxonomy_term_data where vid=" . 
                                                              $v['vid'] . 
                                                              " and parent=" . 
                                                              $vy['tid']);
                                if(count($dataz)> 0) {

                                    foreach($dataz as $vz) {
                                        $ne[$v['vid']]['detail'][$vx['tid']]['detail'][$vy['tid']]['detail'][$vz['tid']] = $vz['name'];
                                    }
                                }
                            }
                        }
                    }
                }
                //$ne[$v['vid']]['detail']=
            }
            $ne[$v['vid']]['name']['detil'] = $v['name'];
        }
        catch(Exception $e) {
        }
        return array('transaction' => true,
                     'message' => 'success',
                     'data' =>$ne);
    }

    public function pub_general_search($params = array()) {
        try {
            $params["start"] = 0;
            // $params["next"] = (int) $params['page'] + 1;
            $arr = array("doc" => 3,
                         "news" => 2,
                         "socmed" => 5);

            foreach($params['fk'] as $vz) {
                $nmed[] = $arr[$vz];
            }
            $fil = implode(" OR ", $nmed);
            $params["x"] = $fil;
            $s = new Domas_Model_Solranalis();
            $g = new Domas_Model_Pdash();
            $z = new Model_Cache();
            $cache = $z->cachefunc(4000);
            $id = $z->get_id("Domas_Model_Solranalis_get_latest", $params);
            $data = $z->get_cache($cache, $id);
            $data = false;
            if(!$data) {
                $data = $s->get_latest_all($params);
                $cache->save($data, $id, array('systemdomas'));
            }
        }
        catch(Exception $e) {
        }
        return array('transaction' => true,
                     'message' => 'success',
                     'data' =>$data);
    }

    public function pub_general_by_topic($params = array()) {
        try {
            $params["start"] = 0;
            // $params["next"] = (int) $params['page'] + 1;
            $arr = array("doc" => 3,
                         "news" => 2,
                         "socmed" => 5);

            foreach($params['fk'] as $vz) {
                $nmed[] = $arr[$vz];
            }
            $fil = implode(" OR ", $nmed);
            $params["x"] = $fil;
            $s = new Domas_Model_Solranalis();
            $g = new Domas_Model_Pdash();
            $z = new Model_Cache();
            $cache = $z->cachefunc(4000);
            $id = $z->get_id("Domas_Model_Solranalis_get_latest", $params);
            $data = $z->get_cache($cache, $id);
            $data = false;
            if(!$data) {
                $data = $s->get_latest_all($params);
                $cache->save($data, $id, array('systemdomas'));
            }
        }
        catch(Exception $e) {
        }
        return array('transaction' => true,
                     'message' => 'success',
                     'data' =>$data);
    }
}
