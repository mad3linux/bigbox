<?php

/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

use Enzim\Lib\TikaWrapper\TikaWrapper;

class Domas_Model_Server extends Zend_Db_Table_Abstract {

    public function add_server($data) {
        
        $var =array(
            'sftp'=>22,
            'ftp'=>21,
            'web'=>80);
        $idata = array(
            $data['servername'],
            $data['ipaddress'],
            $data['username'],
            $data['upassword'],
            $var[$data['conntype']],
            serialize($data['docroot'])
        );
        //Zend_Debug::dump()
        try {
            $sql = "insert into zpraba_server (server_name, server_ip, server_user, server_pwd, channel, root_loc) values (?, ?, ?, ?, ?, ?)";
            $data = $this->_db->query($sql, $idata);
        } catch (Exception $e) {
            return array("transaction" => false, "result" => false, "message" => $e->getMessage());
        }
        return array("transaction" => true, "result" => true, "message" => "transaction succed");
    }

    public function get_server() {
        try {
            $sql = "select * from zpraba_server order by id";
            $data = $this->_db->fetchAll($sql);
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
        return $data;
    }

    public function get_file_extract($data) {
        try {
            $vdata = $this->get_a_server($data[0]);
            //Zend_Debug::dump($vdata); die();
            $cot = array();
            switch ($vdata['channel']) {

                case '22':
                    $cot = $this->get_extract_sftp($data, $vdata);

                    break;
            }

            //  Zend_Debug::dump($cot); die();
            $cc = new Mdata_Model_Withsolr();
            $cc->insert_document($cot, $data, $vdata);

            //Zend_Debug::dump($vdata); die();
        } catch (Exception $e) {

            Zend_Debug::dump($e);
            die();
        }
    }

    function get_extract_sftp($data, $vdata) {
        $host = $vdata['server_ip'];
        $port = $vdata['channel'];
        $username = $vdata['server_user'];
        $password = $vdata['server_pwd'];
        $remoteDir = $data[1];
        $localDir = constant('APPLICATION_PATH') . '/../public/tmp/extract';
        $fil = explode("/", $data[1]);
        $file = end($fil);
        $connection = ssh2_connect($host, $port);
        ssh2_auth_password($connection, $username, $password);
        ssh2_scp_recv($connection, $remoteDir, $localDir . "/" . $file);
        $content = TikaWrapper::getText($localDir . "/" . $file);
        $meta = TikaWrapper::getMetadata($localDir . "/" . $file);

        $v_meta = explode("\n", $meta);
        $list = array();
        foreach ($v_meta as $c) {
            $vz = array();
            if ($vz != "") {
                $vz = explode(": ", $c);
                if ($vz[0] != "") {
                    $list[$vz[0]] = $vz[1];
                }
            }
        }
        return array($content, $list);
    }

    function get_a_server($id) {
        try {
            $sql = "select * from zpraba_server where id=?";
            $data = $this->_db->fetchRow($sql, $id);
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
        return $data;
    }

    function get_file_server($data) {
        $vdata = $this->get_a_server($data[0]);
        #Zend_Debug::dump($data); die();
        $path = $data[1];
        switch ($vdata['channel']) {
            case '22' :
                return $this->get_sftp($data, $vdata, $path);
                break;
            case '21':


                break;
        }
    }

    function get_sftp($data, $vdata, $path) {
        // Zend_Debug::dump($vdata); die();
        $connection = ssh2_connect($vdata['server_ip'], $vdata['channel']);
        if ($connection) {
            if (ssh2_auth_password($connection, $vdata['server_user'], $vdata['server_pwd'])) {
                $stream = ssh2_exec($connection, 'ls -l  ' . $path);
                $errorStream = ssh2_fetch_stream($stream, SSH2_STREAM_STDERR);
                stream_set_blocking($errorStream, true);
                stream_set_blocking($stream, true);

                $line = (stream_get_contents($stream));
                $vline = explode("\n", $line);

                foreach ($vline as $k => $v) {
                    if ($v != "" && $k != 0) {
                        $tmp = array();
                        $line = "";
                        $line = str_replace(array("\r\n", "\n", "\r"), "", $v);
                        $line = preg_replace('!\s+!', ' ', $line);
                        $tmp = explode(" ", $line);
                        # Zend_Debug::dump($tmp);//die();
                        if (substr($tmp[0], 0, 1) == "d") {
                            $folder = true;
                            $icon = "fa fa-folder icon-state-success";
                        } else {
                            $folder = false;
                            $icon = "fa fa-file icon-state-danger";
                        }

                        /// $str = $tmp[8] . "<br>" . $tmp[0] . "<br>" . $tmp[2] . "<br>" . $tmp[3] . "<br>" . $tmp[4] . "<br>" . $tmp[6] . ' ' . $tmp[5] . ' ' . $tmp[7] . "<br>";
                        if (count($tmp) > 8) {
                            $item = $tmp[8] . " " . $tmp[9];
                        } else {
                            $item = $tmp[8];
                        }

                        $list[$k - 1] = array(
                            'id' => base64_encode(serialize(array($data[0], trim($path) . "/" . trim($item)))),
                            'text' => $item,
                            "icon" => $icon,
                            'bundle' => "schema",
                            'children' => $folder,
                        );

                        // Zend_Debug::dump($tmp); die();
                    }
                }




                fclose($errorStream);
                fclose($stream);
            }
        }

        return $list;
    }

}
