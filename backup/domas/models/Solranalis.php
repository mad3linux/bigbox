<?php

class Domas_Model_Solranalis extends Zend_Db_Table_Abstract {

    function connect_solr() {
        $config = new Zend_Config_Ini(APPLICATION_PATH . 
                                      '/configs/application.ini', 'production');
        $config = $config->toArray();
        $options = array('hostname' =>$config['data']['pmas']['solr']['host'],
                         'login' =>$config['data']['pmas']['solr']['host'],
                         'user' =>$config['data']['pmas']['solr']['user'],
                         'password' =>$config['data']['pmas']['solr']['password'],
                         'port' =>$config['data']['pmas']['solr']['port'],
                         'path' =>$config['data']['pmas']['solr']['path']);
        try {
            return new SolrClient($options);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    function connect_solr1() {
        $config = new Zend_Config_Ini(APPLICATION_PATH . 
                                      '/configs/application.ini', 'production');
        $config = $config->toArray();
        $options = array('hostname' =>$config['data']['factminer']['solr']['host'],
                         'login' =>$config['data']['factminer']['solr']['host'],
                         'user' =>$config['data']['factminer']['solr']['user'],
                         'password' =>$config['data']['factminer']['solr']['password'],
                         'port' =>$config['data']['factminer']['solr']['port'],
                         'path' =>$config['data']['factminer']['solr']['path']);
        try {
            return new SolrClient($options);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    function get_sentiment($params) {
        // if($params["cat"]!=""){
        // Zend_Debug::dump($params);die();			
        // }
        $g = new Domas_Model_Pdash();
        // $n = new Domas_Model_Solrfactminer();
        $ff = new Domas_Model_Withsolrpmas();
        $str = "*:*";
        //-============= start fq content
        $skey = null;
        $arr_pencarian = array();
        $arr_searchkey = array();
        $pencarian = "";
        if($params["skey"] != "") {
            $arr_pencarian = explode(",", $params["skey"]);
        }
        if($params["searchkey"] != "") {
            $arr_searchkey = explode(",", $params["searchkey"]);
        }
        $tmp_tp = $g->get_topik_by_tema($params["cat"], $params["topik"]);
        // Zend_Debug::dump($tp);die();
        $tp = array();

        foreach($tmp_tp as $k => $v) {
            $st = $g->get_subtema_by_tema($v["tid"]);

            foreach($st as $k1 => $v1) {
                array_push($tp, $v1);
            }
        }
        // Zend_Debug::dump($tp);die();
        $arr_topik = array();

        foreach($tp as $k => $v) {
            array_push($arr_topik, trim(strtolower($v["name"])));
        }
        // Zend_Debug::dump($arr_topik);die();
        if(count(array_unique($arr_topik))> 500) {
            array_splice($arr_topik, 320);
        }
        $content1 = null;
        if(count($arr_pencarian)> 0) {
            $content1 = '&fq=content:("' . urlencode(implode('" AND "', $arr_pencarian)). '")+OR+sm_what:("' . urlencode(implode('" OR "', $arr_pencarian)). '")+OR+label:("' . urlencode(implode('" OR "', $arr_pencarian)). '")';
        }
        $content2 = null;
        if(count($arr_topik)> 0) {
            $content2 = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_topik))). '")';
        }
        $content3 = null;
        if(count($arr_searchkey)> 0) {
            $content3 = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")+OR+sm_what:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")+OR+label:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")';
        }
        //-============= end fq content
        //-============= start fq date range
        $cdd = null;
        $d1 = date("Y-m-d", strtotime('monday last week'));
        $d2 = date("Y-m-d");
        if(isset($params['d1'])&& $params["d1"] != "" && isset($params['d2'])&& $params["d2"] != "") {
            // Zend_Debug::dump($params['d1']);
            // Zend_Debug::dump($params['d2']);
            $d1 = date("Y-m-d\TH:i:s\Z", strtotime($params['d1'] . " 00:00:00"));
            $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " 23:59:59"));
            if($params["x"] == "2") {
                $d1 = date("Y-m-d\TH:i:s\Z", strtotime($params['d1'] . " 00:00:00 -7 hours"));
                $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " 23:59:59 -7 hours"));
            }
            if(isset($params["last"])&& $params["last"] != "") {
                $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " " . date("H:i:s"). " -" . $params["last"] . " hour"));
            }
        }
        $facet_ranges = "&facet.range.start=" .($d1). "&facet.range.end=" .($d2);
        // //-============= start facet content_date range
        // $d1 = date("Y-m-d", strtotime('monday last week'));
        // $d2 = date("Y-m-d");
        // if(isset($params["d1"]) && $params["d1"]!="" && isset($params["d2"]) && $params["d2"]!=""){
        // $d1 = date("Y-m-d\T00:00:00\Z", strtotime($params['d1']));
        // $d2 = date("Y-m-d\T23:59:59\Z", strtotime($params['d2']));
        // $d1 = $params["d1"];
        // $d2 = $params["d2"];
        // }
        // $facet_ranges = "&facet.range.start=".($d1."T00:00:00Z")."&facet.range.end=".($d2."T23:59:59Z");
        // //-============= end facet content_date range
        //-============= start fq media_type
        $mt = null;
        if(isset($params["is_mediatype"])&& $params["is_mediatype"] != "" && $params["t"] != "Social Media" && $params["t"] != "Laporan") {
            if($params["is_mediatype"] == "nasional") {
                $mt = "&fq=is_mediatype:(" . urlencode("1 OR 2"). ")";
            } else {
                $mt = "&fq=is_mediatype:3";
            }
        }
        //-============= end fq media_type
        //-============= start fq entity_id
        $ent = null;
        if(isset($params["t"])&& $params["t"] != "") {
            $e_id = array("News" => 2,
                          "Social Media" => 5,
                          "Laporan" => 6,
                          "BDI" => 21,
                         );
            $ent = "&fq=entity_id:" . $e_id[$params["t"]];
            if($params["t"] == "Laporan") {
                $ent = "&fq=entity_id:(6 OR 21)";
            }
        }
        //-============= end fq entity_id
        $fq1 = $content1 . $content2 . $content3 . $facet_ranges . $ent . "&fq=is_flag:1";
        $fq = $content1 . $content2 . $content3 . $facet_ranges . $mt . $ent . "&fq=is_flag:1";
        // die($fq);
        // die($fq);
        // NETRAL
        $net_digmed =($ff->get_by_rawurl("select?q=" . urlencode($str). "&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.gap=%2B1DAY&fq=is_sentiment%3A+0" . $fq . "&fq=entity_id:(2+OR+5)"));
        $dnetral = $net_digmed["facet_counts"]["facet_ranges"]["content_date"]["counts"];
        // Zend_Debug::dump($dnetral);die();	
        // NEGATIF
        $neg_digmed =($ff->get_by_rawurl("select?q=" . urlencode($str). "&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.gap=%2B1DAY&fq=is_sentiment%3A+1" . $fq . "&fq=entity_id:(2+OR+5)"));
        // Zend_Debug::dump($neg_digmed);die();
        $dnegatif = $neg_digmed["facet_counts"]["facet_ranges"]["content_date"]["counts"];
        // POSITIF
        $pos_digmed =($ff->get_by_rawurl("select?q=" . urlencode($str). "&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.gap=%2B1DAY&fq=is_sentiment%3A+2" . $fq . "&fq=entity_id:(2+OR+5)"));
        $dpositif = $pos_digmed["facet_counts"]["facet_ranges"]["content_date"]["counts"];
        // die("s");
        // if($params["t"]=="News"){
        // Zend_Debug::dump($dnetral);//die();
        // Zend_Debug::dump($dnegatif);//die();
        // Zend_Debug::dump($dpositif);//die("s");
        // }
        // die("s");
        $netral = 0;
        $positif = 0;
        $negatif = 0;

        foreach($dnetral as $k => $v) {
            // Zend_Debug::dump($k);die();
            if(($k % 2)== 1) {
                $netral += (int) $v;
                $positif += (int) $dpositif[$k];
                $negatif += (int) $dnegatif[$k];
            }
        }
        // die();
        $data[$params["t"]][$params["skey"]] = array("Netral" =>$netral,
                                                     "Positif" =>$positif,
                                                     "Negatif" =>$negatif,
                                                    );
        // if($params["t"]=="News"){
        // Zend_Debug::dump($data);die();
        // }
        return $data;
    }

    function get_sentimentx($params) {
        // Zend_Debug::dump($params);die();
        $g = new Domas_Model_Pdash();
        // $n = new Domas_Model_Solrfactminer();
        $ff = new Domas_Model_Withsolrpmas();
        $str = "*:*";
        //-============= start fq content
        $skey = null;
        $arr_pencarian = array();
        $arr_searchkey = array();
        $arr_skeyexception = array();
        $pencarian = "";
        if($params["skey"] != "") {
            $arr_pencarian = explode(",", $params["skey"]);
        }
        if($params["searchkey"] != "") {
            $arr_searchkey = explode(",", $params["searchkey"]);
        }
        if($params["skeyexception"] != "") {
            $arr_skeyexception = explode(",", $params["skeyexception"]);
        }
        $tmp_tp = $g->get_topik_by_tema($params["cat"], $params["topik"]);
        // Zend_Debug::dump($tp);die();
        $tp = array();

        foreach($tmp_tp as $k => $v) {
            $st = $g->get_subtema_by_tema($v["tid"]);

            foreach($st as $k1 => $v1) {
                array_push($tp, $v1);
            }
        }
        // Zend_Debug::dump($tp);die();
        $arr_topik = array();

        foreach($tp as $k => $v) {
            array_push($arr_topik, trim(strtolower($v["name"])));
        }
        // Zend_Debug::dump($arr_topik);die();
        if(count(array_unique($arr_topik))> 500) {
            array_splice($arr_topik, 320);
        }
        $content1 = null;
        if(count($arr_pencarian)> 0) {
            $content1 = '&fq=content:("' . urlencode(implode('" AND "', $arr_pencarian)). '")+OR+sm_what:("' . urlencode(implode('" OR "', $arr_pencarian)). '")+OR+label:("' . urlencode(implode('" OR "', $arr_pencarian)). '")';
        }
        if(count($arr_pencarian)== 1) {
            $content1 = '&fq=content:"' . urlencode(implode('" AND "', $arr_pencarian)). '"+OR+sm_what:"' . urlencode(implode('" OR "', $arr_pencarian)). '"+OR+label:"' . urlencode(implode('" OR "', $arr_pencarian)). '"';
        }
        $content2 = null;
        if(count($arr_topik)> 0) {
            $content2 = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_topik))). '")';
        }
        $content3 = null;
        if(count($arr_searchkey)> 0) {
            $content3 = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")+OR+sm_what:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")+OR+label:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")';
        }
        $content4 = null;
        $sm_what_or = null;
        if(count($arr_skeyexception)> 0) {
            $content3 = '&fq=-content:("' . urlencode(implode('" OR "', array_unique($arr_skeyexception))). '")+OR+-sm_what:("' . urlencode(implode('" OR "', array_unique($arr_skeyexception))). '")+OR+-label:("' .(implode('" OR "', array_unique($arr_skeyexception))). '")';
            // $sm_what_or = 'sm_what:("'.(implode('" OR "',array_unique($arr_searchkey))).'")';
        }
        //-============= end fq content
        //-============= start fq date range
        $cdd = null;
        $d1 = date("Y-m-d", strtotime('monday last week'));
        $d2 = date("Y-m-d");
        if(isset($params['d1'])&& $params["d1"] != "" && isset($params['d2'])&& $params["d2"] != "") {
            // Zend_Debug::dump($params['d1']);
            // Zend_Debug::dump($params['d2']);
            $d1 = date("Y-m-d\TH:i:s\Z", strtotime($params['d1'] . " 00:00:00"));
            $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " 23:59:59"));
            if($params["x"] == "2") {
                $d1 = date("Y-m-d\TH:i:s\Z", strtotime($params['d1'] . " 00:00:00 -7 hours"));
                $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " 23:59:59 -7 hours"));
            }
            if(isset($params["last"])&& $params["last"] != "") {
                $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " " . date("H:i:s"). " -" . $params["last"] . " hour"));
            }
        }
        $facet_ranges = "&facet.range.start=" . urlencode($d1). "&facet.range.end=" . urlencode($d2);
        // echo $facet_ranges;die();
        //-============= start facet content_date range
        // $d1 = date("Y-m-d", strtotime('monday last week'));
        // $d2 = date("Y-m-d");
        // if(isset($params["d1"]) && $params["d1"]!="" && isset($params["d2"]) && $params["d2"]!=""){
        // $d1 = date("Y-m-d\T00:00:00\Z", strtotime($params['d1']));
        // $d2 = date("Y-m-d\T23:59:59\Z", strtotime($params['d2']));
        // $d1 = $params["d1"];
        // $d2 = $params["d2"];
        // }
        // $facet_ranges = "&facet.range.start=".($d1."T00:00:00Z")."&facet.range.end=".($d2."T23:59:59Z");
        //-============= end facet content_date range
        //-============= start fq media_type
        $mt = null;
        if(isset($params["is_mediatype"])&& $params["is_mediatype"] != "" && $params["t"] != "Social Media" && $params["t"] != "Laporan" && $params["t"] != "Media Sosial") {
            if($params["is_mediatype"] == "nasional") {
                $mt = "&fq=is_mediatype:(" . urlencode("1 OR 2"). ")";
            } else {
                $mt = "&fq=is_mediatype:3";
            }
        }
        //-============= end fq media_type
        //-============= start fq entity_id
        $ent = null;
        if(isset($params["t"])&& $params["t"] != "") {
            $e_id = array("News" => 2,
                          "news" => 2,
                          "Social Media" => 5,
                          "Media Sosial" => 5,
                          "Laporan" => 3);
            $ent = "&fq=entity_id:" . $e_id[$params["t"]];
            if($params["t"] == "Laporan") {
                $ent = "&fq=entity_id:3";
            }
        }
        //-============= end fq entity_id
        $fq1 = $content1 . $content2 . $sm_what . $content3 . $content4 . $facet_ranges . $ent;
        $fq = $content1 . $content2 . $sm_what . $content3 . $content4 . $facet_ranges . $mt . $ent;
        // Zend_Debug::dump(explode("&",$fq));die();
        // Zend_Debug::dump($fq);die();
        // die($fq);
        try {
            $arr_sent = array("netral",
                              "negatif",
                              "positif",
                             );
            $data = array();

            foreach($arr_sent as $k => $v) {
                // die("select?q=".urlencode($str)."&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.gap=%2B1DAY&fq=is_sentiment:".$k.$fq."");				
                $sentiment =($ff->get_by_rawurl("select?q=" . urlencode($str). "&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.gap=%2B1DAY&fq=is_sentiment:" . $k . $fq . ""));
                // Zend_Debug::dump($sentiment);die();
                $data[strtoupper($v)] = $sentiment["facet_counts"]["facet_ranges"]["content_date"]["counts"];
            }
            // Zend_Debug::dump($data);die();
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }
    }

    function get_stopword($params) {
    }

    function get_wordcloud($params) {
        // Zend_Debug::dump($params);die(); 
        $g = new Domas_Model_Pdash();
        $t = new Analis_Model_General();
        //-============= start get stopword
        $tmp_sw = $g->get_stopword($params["t"]);
        // Zend_Debug::dump($tmp_sw);die();
        $stopword = array();

        foreach($tmp_sw as $k => $v) {
            // $stopword[$k] = (strlen($v["name"])>1 && substr($v["name"],0,1)=="n"?strtolower(substr($v["name"],1)):strtolower($v["name"]));
            $stopword[$k] = strtolower($v["name"]);
        }
        //-============= end get stopword
        //-============= start get tokoh
        $tmp_tk = $t->data_tokoh();
        // Zend_Debug::dump($tmp_tk);die();
        // $i=0;
        $tokoh = array();

        foreach($tmp_tk as $k => $v) {
            if(count($v['key'])> 0) {

                foreach($v['key'] as $k2 => $v2) {
                    // Zend_Debug::dump($tmp_tk['TOKOH']['key'][0][$k2]);

                    foreach($tmp_tk['TOKOH']['key'][0][$k2] as $z3 => $v3) {
                        // Zend_Debug::dump($tmp_tk['TOKOH']['key'][1][$v3['ID_TOKOH']]);						
                        if(count($tmp_tk['TOKOH']['key'][1][$v3['ID_TOKOH']])> 0) {
                            $i = 0;

                            foreach($tmp_tk['TOKOH']['key'][1][$v3['ID_TOKOH']] as $z4 => $v4) {
                                $tokoh[$v3['F_NAMA']][$i] = strtoupper($v4['F_NAMA']);
                                $i ++;
                            }
                        }
                        $tokoh[$v3['F_NAMA']][$i] = strtoupper($v3['F_NAMA']);
                    }
                }
            }
        }
        //-============= end get tokoh
        // Zend_Debug::dump($stopword);die();
        //-============= start fq content
        $skey = null;
        $arr_pencarian = array();
        $arr_searchkey = array();
        $arr_skeyexception = array();
        $pencarian = "";
        if($params["skey"] != "") {
            $arr_pencarian = explode(",", $params["skey"]);
        }
        if($params["searchkey"] != "") {
            $arr_searchkey = explode(",", $params["searchkey"]);
        }
        if($params["skeyexception"] != "") {
            $arr_skeyexception = explode(",", $params["skeyexception"]);
        }
        $tmp_tp = $g->get_topik_by_tema($params["cat"], $params["topik"]);
        // Zend_Debug::dump($tmp_tp);die();
        $tp = array();

        foreach($tmp_tp as $k => $v) {
            $st = $g->get_subtema_by_tema($v["tid"]);

            foreach($st as $k1 => $v1) {
                array_push($tp, $v1);
            }
        }
        // Zend_Debug::dump($tp);die();
        $arr_topik = array();

        foreach($tp as $k => $v) {
            array_push($arr_topik, trim(strtolower($v["name"])));
        }
        // asort($arr_topik);
        // Zend_Debug::dump($arr_topik);die();
        if(count(array_unique($arr_topik))> 500) {
            array_splice($arr_topik, 320);
        }
        $content1 = null;
        $sm_what_and = null;
        if(count($arr_pencarian)> 0) {
            $content1 = 'content:("' .(implode('" AND "', $arr_pencarian)). '") OR sm_what:("' .(implode('" OR "', $arr_pencarian)). '") OR label:("' .(implode('" OR "', $arr_pencarian)). '")';
            // $sm_what_and = 'sm_what:("'.(implode('" AND "',array_unique($arr_pencarian))).'")';
        }
        if(count($arr_pencarian)== 1) {
            $content1 = 'content:"' .(implode('" AND "', $arr_pencarian)). '" OR sm_what:"' .(implode('" OR "', $arr_pencarian)). '" OR label:"' .(implode('" OR "', $arr_pencarian)). '"';
        }
        $content2 = null;
        if(count($arr_topik)> 0) {
            $content2 = 'content:("' .(implode('" OR "', array_unique($arr_topik))). '")';
        }
        $content3 = null;
        $sm_what_or = null;
        if(count($arr_searchkey)> 0) {
            $content3 = 'content:("' .(implode('" OR "', array_unique($arr_searchkey))). '") OR sm_what:("' .(implode('" OR "', array_unique($arr_searchkey))). '") OR label:("' .(implode('" OR "', array_unique($arr_searchkey))). '")';
            // $sm_what_or = 'sm_what:("'.(implode('" OR "',array_unique($arr_searchkey))).'")';
        }
        $content4 = null;
        $sm_what_or = null;
        if(count($arr_skeyexception)> 0) {
            $content3 = '-content:("' .(implode('" OR "', array_unique($arr_skeyexception))). '") OR -sm_what:("' .(implode('" OR "', array_unique($arr_skeyexception))). '") OR label:("' .(implode('" OR "', array_unique($arr_skeyexception))). '")';
            // $sm_what_or = 'sm_what:("'.(implode('" OR "',array_unique($arr_searchkey))).'")';
        }
        //-============= end fq content
        //-============= start fq date range
        $cdd = null;
        $d1 = date("Y-m-d", strtotime('monday last week'));
        $d2 = date("Y-m-d");
        $dr = null;
        if(isset($params['d1'])&& $params["d1"] != "" && isset($params['d2'])&& $params["d2"] != "") {
            // Zend_Debug::dump($params['d1']);
            // Zend_Debug::dump($params['d2']);
            $d1 = date("Y-m-d\TH:i:s\Z", strtotime($params['d1'] . " 00:00:00"));
            $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " 23:59:59"));
            if($params["x"] == "2") {
                $d1 = date("Y-m-d\TH:i:s\Z", strtotime($params['d1'] . " 00:00:00 -7 hours"));
                $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " 23:59:59 -7 hours"));
            }
            if(isset($params["last"])&& $params["last"] != "") {
                $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " " . date("H:i:s"). " -" . $params["last"] . " hour"));
            }
            $dr = "[" . $d1 . " TO " . $d2 . "]";
            $dr = "content_date:" .($dr);
        }
        //-============= start fq content_date range
        // $dr = null;
        // if(isset($params["d1"]) && $params["d1"]!="" && isset($params["d2"]) && $params["d2"]!=""){
        // $dr = "[".$params["d1"]."T00:00:00Z TO ".$params["d2"]."T23:59:59Z]";
        // $dr = "content_date:".($dr);
        // }
        //-============= end fq content_date range
        //-============= start fq provinsi kota
        $provinsi = null;
        $kota = null;
        if($params["provinsi"] != "") {
            $provinsi = "ss_wilayah:'" . $params["provinsi"] . "'";
        }
        if($params["kota"] != "") {
            $kota = "ss_kota:'" . $params["kota"] . "'";
        }
        //-============= end fq provinsi kota
        //-============= start fq source
        $entity = array("laporan" => "entity_id:6 OR entity_id:21",
                        "digmed" => "entity_id:2",
                        "socmed" => "entity_id:5",
                        "BDI" => "entity_id:21",
                       );
        //-============= end fq source
        //-============= start fq is_sentiment
        $sent = null;
        if($params["is_sentiment"] != "") {
            $arr_sent = array("netral" => 0,
                              "negatif" => 1,
                              "positif" => 2,
                             );
            $sent = "is_sentiment:" . $arr_sent[strtolower($params["is_sentiment"])];
        }
        //-============= end fq is_sentiment
        //-============= start fq is_mediatype
        $mt = null;
        if($params["is_mediatype"] != "" &&($params["t"] == "digmed" || $params["t"] == "laporan")) {
            if($params["is_mediatype"] == "nasional") {
                $mt = "is_mediatype:(1 OR 2)";
            } else {
                $mt = "is_mediatype:3";
            }
        }
        //-============= end fq is_mediatype
        $fq = array($content1,
                   $content2,
                   $content3,
                   $content4,
                   $dr,
                   $entity[$params["t"]],
                   $mt,
                   $sm_what_and,
                   $sm_what_or,
                   $provinsi,
                   $kota);
        // Zend_Debug::dump($fq);die();
        //-============= start facet_fields 
        $facetfield = "sm_term";
        $cek = "";
        if($params["tc"] != "") {
            if($params["tc"] == 'personal') {
                $facetfield = "sm_who";
                $cek = '(PER)';
            } else if($params["tc"] == 'organisasi') {
                $facetfield = "sm_who";
                $cek = '(ORG)';
            } else if($params["tc"] == 'tempat') {
                $facetfield = "sm_where";
            } else if($params["tc"] == 'prase') {
                $facetfield = "sm_term";
            }
        }
        // Zend_Debug::dump($params["tc"]);die();
        //-============= end facet_fields
        // Zend_Debug::dump($facetfield);die();
        try {
            ///!--- asli
            $client = $this->connect_solr();
            $query = new SolrQuery('*:*');

            foreach($fq as $k => $v) {
                if($v != "" && $v != null) {
                    $query->addFilterQuery($v);
                }
            }
            $query->addSortField('content_date', SolrQuery::ORDER_DESC);
            $query->setFacet(true);
            $query->setRows(0);
            $query->setFacetLimit(100);
            $query->addFacetField($facetfield);
            $response = $client->query($query);
            $response_array = $response->getResponse();
            // Zend_Debug::dump($response_array);die();
            // $x = $response_array->facet_counts->facet_fields;
            // Zend_Debug::dump($facet_data);die();
            $lap2 = (array) $response_array->facet_counts->facet_fields->$facetfield;
            // Zend_Debug::dump($lap2);die();
            ///!--- end asli
            //edit
            // // // $ff = new Domas_Model_Withsolr();
            // // // $filter='';
            // // // foreach($fq as $k=>$v){
            // // // if($v!="" && $v!=null){
            // // // $filter.='&fq='.urlencode($v);
            // // // }
            // // // }
            // // // // Zend_Debug::dump('select?q=*%3A*&wt=json&indent=true&rows=0'.$filter.'&facet=true&facet.field='.$facetfield);die();
            // // // $tmp = $ff->get_by_rawurl('select?q=*%3A*&wt=json&indent=true&rows=0'.$filter.'&facet=true&facet.field='.$facetfield);
            // // // $lap2 = $tmp['response_array']['facet_counts']['facet_fields'][$facetfield];
            // Zend_Debug::dump($lap2);die();
            $lap1 = array();
            $i = 0;

            foreach($lap2 as $k => $v) {
                // Zend_Debug::dump($k);
                if($k != "") {
                    if(strpos($k, $cek)!== false) {
                        $k = trim(str_replace($cek, "", $k));
                        if(!in_array(strtolower($k), $stopword)&& $i < 100) {
                            // Zend_Debug::dump($k);die("s");
                            $lap1[trim(strtoupper(str_replace($cek, "", $k)))] += (int) $v;
                            $i ++;
                        }
                    } else if($cek == "") {
                        if(!in_array(strtolower($k), $stopword)&& $i < 100) {
                            // Zend_Debug::dump($k);die("s");
                            $lap1[trim(strtoupper(str_replace($cek, "", $k)))] += (int) $v;
                            $i ++;
                        }
                    }
                }
            }
            $i = 0;
            $coun = 0;
            // Zend_Debug::dump($stopword);
            // Zend_Debug::dump($tokoh);
            $i = 0;

            foreach($lap1 as $k1 => $v1) {
                // Zend_Debug::dump($k1);
                // Zend_Debug::dump($v1);
                $coun = 0;

                foreach($tokoh as $k => $v) {
                    // Zend_Debug::dump($v);//die();
                    if(in_array($k1, $v)) {
                        $lap3[$k] += (int) $v1;
                        $i ++;
                    } else {
                        $lap31[$k1] = (int) $v1;
                    }
                }
            }
            $j = 0;

            foreach($tokoh as $k => $v) {

                foreach($v as $k1 => $v1) {
                    $temp_tokoh[$j] = trim($v1);
                    $j ++;
                }
            }
            // Zend_Debug::dump($lap1);
            // Zend_Debug::dump(COUNT($lap3));
            // Zend_Debug::dump($temp_tokoh);
            // die();
            $i = 0;

            foreach($lap1 as $k1 => $v1) {
                if(!in_array($k1, $temp_tokoh)&& $i < 10) {
                    // Zend_Debug::dump($k1);//die("s");
                    $lap3[$k1] += (int) $v1;
                    $i ++;
                }
            }
            $i = - 0;

            foreach($lap3 as $k1 => $v1) {
                if($i < 10) {
                    // Zend_Debug::dump($k1);//die("s");
                    $lap4[$k1] += (int) $v1;
                    $i ++;
                }
            }
            arsort($lap4);
            // Zend_Debug::dump($lap3);die("s");
            return $lap4;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage);
            die();
        }
    }

    function get_markers($params) {
        $g = new Domas_Model_Pdash();
        //-============= start fq doc_tx
        $skey = null;
        $arr_pencarian = array();
        $arr_searchkey = array();
        $arr_skeyexception = array();
        $pencarian = "";
        if($params["skey"] != "") {
            $arr_pencarian = explode(",", $params["skey"]);
        }
        if($params["searchkey"] != "") {
            $arr_searchkey = explode(",", $params["searchkey"]);
        }
        if($params["skeyexception"] != "") {
            $arr_skeyexception = explode(",", $params["skeyexception"]);
        }
        $tmp_tp = $g->get_topik_by_tema($params["cat"], $params["topik"]);
        // Zend_Debug::dump($tp);die();
        $tp = array();

        foreach($tmp_tp as $k => $v) {
            $st = $g->get_subtema_by_tema($v["tid"]);

            foreach($st as $k1 => $v1) {
                array_push($tp, $v1);
            }
        }
        // Zend_Debug::dump($tp);die();
        $arr_topik = array();

        foreach($tp as $k => $v) {
            array_push($arr_topik, $v["name"]);
        }
        if(count(array_unique($arr_topik))> 500) {
            array_splice($arr_topik, 320);
        }
        // Zend_Debug::dump($arr_topik);die();
        $content1 = null;
        $sm_what_and = null;
        if(count($arr_pencarian)> 0) {
            $content1 = 'content:("' .(implode('" AND "', $arr_pencarian)). '") OR sm_what:("' .(implode('" OR "', array_unique($arr_pencarian))). '") OR label:("' .(implode('" OR "', array_unique($arr_pencarian))). '")';
        }
        $content2 = null;
        if(count($arr_topik)> 0) {
            $content2 = 'content:("' .(implode('" OR "', array_unique($arr_topik))). '")';
        }
        $content3 = null;
        $sm_what_or = null;
        if(count($arr_searchkey)> 0) {
            $content3 = 'content:("' .(implode('" OR "', array_unique($arr_searchkey))). '") OR sm_what:("' .(implode('" OR "', array_unique($arr_searchkey))). '") OR label:("' .(implode('" OR "', array_unique($arr_searchkey))). '")';
        }
        $content4 = null;
        $sm_what_or = null;
        if(count($arr_skeyexception)> 0) {
            $content3 = '-content:("' .(implode('" OR "', array_unique($arr_skeyexception))). '") OR -sm_what:("' .(implode('" OR "', array_unique($arr_skeyexception))). '") OR label:("' .(implode('" OR "', array_unique($arr_skeyexception))). '")';
            // $sm_what_or = 'sm_what:("'.(implode('" OR "',array_unique($arr_searchkey))).'")';
        }
        //-============= end fq doc_tx
        //-============= start fq date range
        $cdd = null;
        $d1 = date("Y-m-d", strtotime('monday last week'));
        $d2 = date("Y-m-d");
        if(isset($params['d1'])&& $params["d1"] != "" && isset($params['d2'])&& $params["d2"] != "") {
            // Zend_Debug::dump($params['d1']);
            // Zend_Debug::dump($params['d2']);
            $d1 = date("Y-m-d\TH:i:s\Z", strtotime($params['d1'] . " 00:00:00"));
            $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " 23:59:59"));
            if($params["x"] == "2") {
                $d1 = date("Y-m-d\TH:i:s\Z", strtotime($params['d1'] . " 00:00:00 -7 hours"));
                $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " 23:59:59 -7 hours"));
            }
            if(isset($params["last"])&& $params["last"] != "") {
                $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " " . date("H:i:s"). " -" . $params["last"] . " hour"));
            }
        }
        $cdr = "content_date:" .("[" . 
                                 $d1 . 
                                 " TO " . 
                                 $d2 . 
                                 "]");
        // //-============= start fq content_date range
        // if(!isset($params["d1"])){
        // $params["d1"] = date("Y-m-d", strtotime('monday last week'));
        // }
        // if(!isset($params["d2"])){
        // $params["d2"] = date("Y-m-d");
        // }
        // $cdr = "content_date:".("[".$params["d1"]."T00:00:00Z TO ".$params["d2"]."T23:59:59Z]");
        //-============= end fq content_date range
        //-============= start fq doc_tx
        $source = null;
        if(isset($params["t"])&& $params["t"] != "") {
            if($params["t"] == "Social Media") {
                $source = "entity_id:5";
            } else if($params["t"] == "News") {
                $source = "entity_id:2";
            } else if($params["t"] == "Laporan") {
                $source = "entity_id:6";
            }
        }
        //-============= end fq doc_tx
        //-============= start fq is_mediatype
        $mt = null;
        if($params["is_mediatype"] != "" &&($params["t"] == "News" || $params["t"] == "Laporan")) {
            if($params["is_mediatype"] == "nasional") {
                $mt = "is_mediatype:(1 OR 2)";
            } else {
                $mt = "is_mediatype:3";
            }
        }
        //-============= end fq is_mediatype
        //-============= start fq sentiment
        $sen = null;
        if(isset($params["is_sentiment"])&& $params["is_sentiment"] != "") {
            $sen = "is_sentiment:0";
            if(strtolower($params["is_sentiment"])== "negatif") {
                $sen = "is_sentiment:1";
            } else if(strtolower($params["is_sentiment"])== "positif") {
                $sen = "is_sentiment:2";
            }
        }
        //-============= end fq sentiment
        //-============= start fq where_str_mv
        $ex1 = "ss_latitude:[* TO *]";
        $ex2 = "ss_longitude:[* TO *]";
        if($params["t"] != "Social Media" && $params["t"] != "Laporan") {
            $ex1 = "sm_where:[* TO *]";
            $ex2 = "sm_who:[* TO *]";
        }
        //-============= end fq where_str_mv
        $fq = array($content1,
                   $content2,
                   $content3,
                   $content4,
                   $cdr,
                   $source,
                   $ex1,
                   $ex2,
                   $mt,
                   $sen,
                   $sm_what_and,
                   $sm_what_or);
        // Zend_Debug::dump($fq);die();
        try {
            $client = $this->connect_solr();
            $query = new SolrQuery('*:*');

            foreach($fq as $k => $v) {
                if($v != "" && $v != null) {
                    $query->addFilterQuery($v);
                }
            }
            $query->addSortField('content_date', SolrQuery::ORDER_DESC);
            // $query->setFacet(true);
            $query->setRows(250);
            // $query->setFacetLimit(10);		
            // $query->addFacetField('sm_term');
            $response = $client->query($query);
            $response_array = $response->getResponse();
            // Zend_Debug::dump($response_array);die();
            // Zend_Debug::dump($response_array->response->docs);die();
            $data = array();

            foreach($response_array->response->docs as $k => $v) {
                $data[$k]["source"] =($params["t"] == "Social Media" ? "Twitter" :($params["t"] == "News" ? "Media Digital" : "Laporan"));
                $data[$k]["loc"] =($params["t"] == "Social Media" ? $v["ss_place_name"] :($params["t"] == "Laporan" ? $v["ss_kota"] : $v["sm_where"]));
                $data[$k]["lat"] =($params["t"] == "Social Media" || $params["t"] == "Laporan" ? (float) $v["ss_latitude"] : null);
                $data[$k]["lng"] =($params["t"] == "Social Media" || $params["t"] == "Laporan" ? (float) $v["ss_longitude"] : null);
                $data[$k]["text"] =($params["t"] == "Social Media" ? $v["content"] :($params["t"] == "News" ?($v["label"] != "" ? $v["label"] : $v["id"]): $v["sm_what"][0]));
                $data[$k]["author"] =($params["t"] == "Social Media" ? $v["ss_user_name"] . 
                                      " @" . 
                                      $v["ss_user_screenname"] : $v["bundle_name"]);
                $data[$k]["link"] =($params["t"] == "Social Media" ? "https://twitter.com/" . 
                                    $v["ss_user_screenname"] . 
                                    "/status/" . 
                                    $v["id"] : $v["id"]);
                $data[$k]["sentiment"] = $v["is_sentiment"];
                $data[$k]["sm_who"] = $v["sm_who"];
            }
            // Zend_Debug::dump($data);die();
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }
    }

    function get_trendline($params) {
        // Zend_Debug::dump($params);die('asd');
        $g = new Domas_Model_Pdash();
        $ff = new Domas_Model_Withsolrpmas();
        $str = "*:*";
        //-============= start fq content
        $skey = null;
        $arr_pencarian = array();
        $arr_searchkey = array();
        $arr_skeyexception = array();
        $pencarian = "";
        if($params["skey"] != "") {
            $arr_pencarian = explode(",", $params["skey"]);
        }
        if($params["searchkey"] != "") {
            $arr_searchkey = explode(",", $params["searchkey"]);
        }
        if($params["skeyexception"] != "") {
            $arr_skeyexception = explode(",", $params["skeyexception"]);
        }
        $tmp_tp = $g->get_topik_by_tema($params["cat"], $params["topik"]);
        // Zend_Debug::dump($tp);die();
        $tp = array();

        foreach($tmp_tp as $k => $v) {
            $st = $g->get_subtema_by_tema($v["tid"]);

            foreach($st as $k1 => $v1) {
                array_push($tp, $v1);
            }
        }
        // Zend_Debug::dump($tp);die();
        $arr_topik = array();

        foreach($tp as $k => $v) {
            array_push($arr_topik, trim(strtolower($v["name"])));
        }
        // Zend_Debug::dump($arr_topik);die();
        if(count(array_unique($arr_topik))> 500) {
            array_splice($arr_topik, 320);
        }
        $content1 = null;
        $sm_what_and = null;
        if(count($arr_pencarian)> 0) {
            $content1 = '&fq=content:("' . urlencode(implode('" AND "', $arr_pencarian)). '")+OR+label:("' . urlencode(implode('" AND "', $arr_pencarian)). '")';
        }
        $content2 = null;
        if(count($arr_topik)> 0) {
            $content2 = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_topik))). '")';
        }
        $content3 = null;
        $sm_what_or = null;
        if(count($arr_searchkey)> 0) {
            $content3 = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")+OR+label:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")';
        }
        $content4 = null;
        $sm_what_or = null;
        if(count($arr_skeyexception)> 0) {
            $content3 = '&fq=-content:("' . urlencode(implode('" OR "', array_unique($arr_skeyexception))). '") +OR+-label:("' .(implode('" OR "', array_unique($arr_skeyexception))). '")';
            // $sm_what_or = 'sm_what:("'.(implode('" OR "',array_unique($arr_searchkey))).'")';
        }
        //-============= end fq content
        // Zend_Debug::dump($params);
        //-============= start fq date range
        $cdd = null;
        $d1 = date("Y-m-d", strtotime('monday last week'));
        $d2 = date("Y-m-d");
        if(isset($params['d1'])&& $params["d1"] != "" && isset($params['d2'])&& $params["d2"] != "") {
            $d1 = date("Y-m-d\TH:i:s\Z", strtotime($params['d1'] . " 00:00:00"));
            $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " 23:59:59"));
            if($params["x"] == "2") {
                $d1 = date("Y-m-d\TH:i:s\Z", strtotime($params['d1'] . " 00:00:00 -7 hours"));
                $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " 23:59:59 -7 hours"));
            }
            // Zend_Debug::dump($d1);
            // Zend_Debug::dump($d2);
            // $d1 = date("Y-m-d\TH:i:s\Z",strtotime($d1." -1 days"));
            // $d2 = date("Y-m-d\TH:i:s\Z",strtotime($d2." +1 days"));
            if(isset($params["last"])&& $params["last"] != "") {
                $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " " . date("H:i:s"). " -" . $params["last"] . " hour"));
            }
        }
        $cdd = "&facet.range.start=" . urlencode($d1). "&facet.range.end=" . urlencode($d2);
        // echo $cdd;die("s");
        // //-============= start fq content_date range
        // $d1 = date("Y-m-d\T00:00:00\Z", strtotime('monday last week'));
        // $d2 = "NOW";
        // if(isset($params["d1"]) && $params["d1"]!="" && isset($params["d2"]) && $params["d2"]!=""){
        // $d1 =  gmdate("Y-m-d\TH:i:s\Z", strtotime($params["d1"]));
        // $d2 =  gmdate("Y-m-d\TH:i:s\Z", strtotime($params["d2"]));
        // $d1 = date("Y-m-d\T00:00:00\Z", strtotime($params['d1']." -1 days"));
        // $d2 = date("Y-m-d\T23:59:59\Z", strtotime($params['d2']." +1 days"));
        // if(isset($params["gap"]) && $params["gap"]=="+1HOUR"){
        // $d1 = date("Y-m-d\T00:00:00\Z", strtotime($params['d1']));
        // $d2 = date("Y-m-d\T23:59:59\Z", strtotime($params['d2']));
        // }
        // // Zend_Debug::dump($d2);die();
        // }
        // // $filter .="&content_date:[".$d1.$d2."]";
        // $cdd = "&facet.range.start=".urlencode($d1)."&facet.range.end=".urlencode($d2);
        //-============= end fq content_date range
        //-============= start fq provinsi kota
        $provinsi = null;
        $kota = null;
        if($params["provinsi"] != "") {
            $provinsi = "ss_wilayah:'" . $params["provinsi"] . "'";
        }
        if($params["kota"] != "") {
            $kota = "ss_kota:'" . $params["kota"] . "'";
        }
        //-============= end fq provinsi kota
        //-============= start fq sentiment
        $sen = null;
        if(isset($params["is_sentiment"])&& $params["is_sentiment"] != "") {
            $sen = "&fq=is_sentiment:0";
            if(strtolower($params["is_sentiment"])== "negatif") {
                $sen = "&fq=is_sentiment:1";
            } else if(strtolower($params["is_sentiment"])== "positif") {
                $sen = "&fq=is_sentiment:2";
            }
        }
        //-============= end fq sentiment
        //-============= start fq media_type
        $mt = null;
        if(isset($params["is_mediatype"])&& $params["is_mediatype"] != "") {
            if($params["is_mediatype"] == "nasional") {
                $mt = "&fq=is_mediatype:(" . urlencode("1 OR 2"). ")";
            } else {
                $mt = "&fq=is_mediatype:3";
            }
        }
        //-============= end fq media_type
        $frg = urlencode("+1DAY");
        if(isset($params["gap"])&& $params["gap"] != "") {
            $frg = urlencode($params["gap"]);
        }
        $fq1 = $content1 . $content2 . $content3 . $cdd . $sen . $sm_what_and . $sm_what_or . $provinsi . $kota;
        $fq = $content1 . $content2 . $content3 . $cdd . $sen . $mt . $sm_what_and . $sm_what_or;
        // Zend_Debug::dump(explode("&",$fq));die();
        // die("select?q=".urlencode($str)."&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.gap=%2B1DAY&fq=entity_id%3A+2".$fq);
        try {
            // $arr_entity
            $news =($ff->get_by_rawurl("select?q=" . urlencode($str). "&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.gap=" . $frg . "&fq=entity_id%3A+2" . $fq));
            // Zend_Debug::dump($news);die();
            $socmed =($ff->get_by_rawurl("select?q=" . urlencode($str). "&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.gap=" . $frg . "&fq=entity_id%3A+5" . $fq1 . ""));
            // Zend_Debug::dump($socmed);//die();
            $laporan =($ff->get_by_rawurl("select?q=" . urlencode($str). "&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.gap=" . $frg . "&fq=entity_id%3A+3" . $fq1));
         //   Zend_Debug::dump($laporan);die("xxx");
            $data = array("Document" =>$laporan["facet_counts"]["facet_ranges"]["content_date"]["counts"],
                          "news" =>$news["facet_counts"]["facet_ranges"]["content_date"]["counts"],
                          "Media Sosial" =>$socmed["facet_counts"]["facet_ranges"]["content_date"]["counts"],
                         );
            // Zend_Debug::dump($data);die();
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }
    }

    function get_5w1hbdi($params) {
        // Zend_Debug::dump($params);die();
        // $n = new Domas_Model_Solrfactminer();
        $g = new Domas_Model_Pdash();
        $ff = new Domas_Model_Withsolrpmas();
        // Zend_Debug::dump($stopword);die();
        //-============= end get stopword
        //-============= start fq doc_tx
        $skey = null;
        $arr_pencarian = array();
        $arr_searchkey = array();
        $arr_skeyexception = array();
        $pencarian = "";
        if($params["skey"] != "") {
            $arr_pencarian = explode(",", $params["skey"]);
        }
        if($params["searchkey"] != "") {
            $arr_searchkey = explode(",", $params["searchkey"]);
        }
        if($params["skeyexception"] != "") {
            $arr_skeyexception = explode(",", $params["skeyexception"]);
        }
        $tmp_tp = $g->get_topik_by_tema($params["cat"], $params["topik"]);
        // Zend_Debug::dump($tp);die();
        $tp = array();

        foreach($tmp_tp as $k => $v) {
            $st = $g->get_subtema_by_tema($v["tid"]);

            foreach($st as $k1 => $v1) {
                array_push($tp, $v1);
            }
        }
        // Zend_Debug::dump($tp);die();
        $arr_topik = array();

        foreach($tp as $k => $v) {
            array_push($arr_topik, trim(strtolower($v["name"])));
        }
        if(count(array_unique($arr_topik))> 500) {
            array_splice($arr_topik, 320);
        }
        // array_unique($arr_topik);
        // Zend_Debug::dump(array_unique($arr_topik));die();
        $content1 = null;
        $sm_what_and = null;
        if(count($arr_pencarian)> 0) {
            $content1 = 'content:("' .(implode('" AND "', $arr_pencarian)). '") OR sm_what:("' .(implode('" OR "', array_unique($arr_pencarian))). '") OR label:("' .(implode('" OR "', array_unique($arr_pencarian))). '")';
        }
        $content2 = null;
        if(count($arr_topik)> 0) {
            $content2 = 'content:("' .(implode('" OR "', array_unique($arr_topik))). '")';
        }
        $content3 = null;
        $sm_what_or = null;
        if(count($arr_searchkey)> 0) {
            $content3 = 'content:("' .(implode('" OR "', array_unique($arr_searchkey))). '") OR sm_what:("' .(implode('" OR "', array_unique($arr_searchkey))). '") OR label:("' .(implode('" OR "', array_unique($arr_searchkey))). '")';
        }
        $content4 = null;
        $sm_what_or = null;
        if(count($arr_skeyexception)> 0) {
            $content4 = '-content:("' .(implode('" OR "', array_unique($arr_skeyexception))). '") OR -sm_what:("' .(implode('" OR "', array_unique($arr_skeyexception))). '") OR label:("' .(implode('" OR "', array_unique($arr_skeyexception))). '")';
            // $sm_what_or = 'sm_what:("'.(implode('" OR "',array_unique($arr_searchkey))).'")';
        }
        $content1 =($content1 == "" && $content2 == "" && $content3 == "" && $content4 == "" ? NULL : $content1);
        //-============= end fq content
        //-============= start fq source
        $source = "entity_id:21";
        //-============= end fq source
        //-============= start fq sentiment
        $sen = null;
        if(isset($params["is_sentiment"])&& $params["is_sentiment"] != "") {
            $sen = "is_sentiment:0";
            if(strtolower($params["is_sentiment"])== "negatif") {
                $sen = "is_sentiment:1";
            } else if(strtolower($params["is_sentiment"])== "positif") {
                $sen = "is_sentiment:2";
            }
        }
        //-============= end fq sentiment
        // $fq = $skey.$source.$cdr.$mt;
        // die($fq);
        $fq = array($content1,
                   $content2,
                   $content3,
                   $content4,
                   $source,
                   $cdr,
                   $mt,
                   $mt2,
                   $sen,
                   $sm_what_and,
                   $sm_what_or);
        // Zend_Debug::dump($fq);die();
        // Zend_Debug::dump($tmp);die();
        // Zend_Debug::dump('select?q=*%3A*&wt=json&indent=true'.$filter);die();
        try {
            // $client = $this->connect_solr();
            // $query = new SolrQuery('*:*');
            // foreach($fq as $k=>$v){
            // if($v!="" && $v!=null){
            // $query->addFilterQuery($v);
            // }
            // }
            // $query->addSortField('content_date', SolrQuery::ORDER_ASC);
            // // $query->setFacet(true);
            // // $query->setRows(0);
            // if($params["iDisplayStart"]!=''){
            // $query->setStart($params["iDisplayStart"]);
            // }
            // //$query->setFacetLimit();		
            // // $query->addFacetField('sm_term');
            // $response = $client->query($query);
            // $response_array = $response->getResponse();
            // // Zend_Debug::dump($response_array);die();
            // // $x = $response_array->facet_counts->facet_fields;
            // // Zend_Debug::dump($facet_data);die();
            // return $response_array->response->docs;
            $ff = new Domas_Model_Withsolrpmas();
            $filter = '';

            foreach($fq as $k => $v) {
                if($v != "" && $v != null) {
                    $filter .= '&fq=' . urlencode($v);
                }
            }
            if($params["iDisplayStart"] != '') {
                $filter .= '&start=' . $params["iDisplayStart"];
            }
            // Zend_Debug::dump($filter);die();
            $tmp = $ff->get_by_rawurl('select?q=*%3A*&wt=json&indent=true&sort=content_date+desc' . 
                                      $filter);
            return $tmp["response"]["docs"];
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }
    }

    function get_5w1h($params) {
        // Zend_Debug::dump($params);die();
        // $n = new Domas_Model_Solrfactminer();
        $g = new Domas_Model_Pdash();
        $ff = new Domas_Model_Withsolrpmas();
        //-============= start get  is_mediatype
        $arr_mt = array("nasional" => 1,
                        "lokal" => 2,
                        "internasional" => 3,
                        "blog" => 4,
                       );
        $mt = null;
        $mt2 = null;
        // if($params["sSearch_2"]!=""){			
        // $params["t"]=$params["sSearch_2"];
        // }
        if($params["t"] == "Laporan") {
            if($params["is_mediatype"] == "nasional") {
                $mt =("is_mediatype:1 OR is_mediatype:2");
            } else {
                $mt =("is_mediatype:" . 
                      $arr_mt[$params["is_mediatype"]]);
            }
            $mt2 = "sm_who:[* TO *] AND sm_where:[* TO *]";
        }
        //-============= end get is_mediatype
        // Zend_Debug::dump($stopword);die();
        //-============= end get stopword
        //-============= start fq doc_tx
        $skey = null;
        $arr_pencarian = array();
        $arr_searchkey = array();
        $arr_skeyexception = array();
        $pencarian = "";
        if($params["skey"] != "") {
            $arr_pencarian = explode(",", $params["skey"]);
        }
        if($params["searchkey"] != "") {
            $arr_searchkey = explode(",", $params["searchkey"]);
        }
        if($params["skeyexception"] != "") {
            $arr_skeyexception = explode(",", $params["skeyexception"]);
        }
        $tmp_tp = $g->get_topik_by_tema($params["cat"], $params["topik"]);
        // Zend_Debug::dump($tp);die();
        $tp = array();

        foreach($tmp_tp as $k => $v) {
            $st = $g->get_subtema_by_tema($v["tid"]);

            foreach($st as $k1 => $v1) {
                array_push($tp, $v1);
            }
        }
        // Zend_Debug::dump($tp);die();
        $arr_topik = array();

        foreach($tp as $k => $v) {
            array_push($arr_topik, trim(strtolower($v["name"])));
        }
        if(count(array_unique($arr_topik))> 500) {
            array_splice($arr_topik, 320);
        }
        // array_unique($arr_topik);
        // Zend_Debug::dump(array_unique($arr_topik));die();
        $content1 = null;
        $sm_what_and = null;
        if(count($arr_pencarian)> 0) {
            $content1 = 'content:("' .(implode('" AND "', $arr_pencarian)). '") OR sm_what:("' .(implode('" OR "', array_unique($arr_pencarian))). '") OR label:("' .(implode('" OR "', array_unique($arr_pencarian))). '")';
        }
        $content2 = null;
        if(count($arr_topik)> 0) {
            $content2 = 'content:("' .(implode('" OR "', array_unique($arr_topik))). '")';
        }
        $content3 = null;
        $sm_what_or = null;
        if(count($arr_searchkey)> 0) {
            $content3 = 'content:("' .(implode('" OR "', array_unique($arr_searchkey))). '") OR sm_what:("' .(implode('" OR "', array_unique($arr_searchkey))). '") OR label:("' .(implode('" OR "', array_unique($arr_searchkey))). '")';
        }
        $content4 = null;
        $sm_what_or = null;
        if(count($arr_skeyexception)> 0) {
            $content4 = '-content:("' .(implode('" OR "', array_unique($arr_skeyexception))). '") OR -sm_what:("' .(implode('" OR "', array_unique($arr_skeyexception))). '") OR label:("' .(implode('" OR "', array_unique($arr_skeyexception))). '")';
            // $sm_what_or = 'sm_what:("'.(implode('" OR "',array_unique($arr_searchkey))).'")';
        }
        $content1 =($content1 == "" && $content2 == "" && $content3 == "" && $content4 == "" ? NULL : $content1);
        //-============= end fq content
        //-============= start fq source
        $source = null;
        $ent = null;
        if(isset($params["t"])&& $params["t"] != "") {
            $e_id = array("News" => 2,
                          "news" => 2,
                          "Social Media" => 5,
                          "Media Sosial" => 5,
                          "Laporan" => 6,
                          "BDI" => 21,
                         );
            $source = "entity_id:" . $e_id[$params["t"]];
            if($params["t"] == "Laporan") {
                $source = "entity_id:(6 OR 21)";
            }
        }
        //-============= end fq source
        //-============= start fq date range
        $cdd = null;
        $d1 = date("Y-m-d", strtotime('monday last week'));
        $d2 = date("Y-m-d");
        if(isset($params['d1'])&& $params["d1"] != "" && isset($params['d2'])&& $params["d2"] != "") {
            // Zend_Debug::dump($params['d1']);
            // Zend_Debug::dump($params['d2']);
            $d1 = date("Y-m-d\TH:i:s\Z", strtotime($params['d1'] . " 00:00:00"));
            $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " 23:59:59"));
            if($params["x"] == "2") {
                $d1 = date("Y-m-d\TH:i:s\Z", strtotime($params['d1'] . " 00:00:00 -7 hours"));
                $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " 23:59:59 -7 hours"));
            }
            if(isset($params["last"])&& $params["last"] != "") {
                $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " " . date("H:i:s"). " -" . $params["last"] . " hour"));
            }
        }
        $cdr = "content_date:[" . $d1 . " TO " . $d2 . "]";
        //-============= start fq content_date range
        // $cdr = null;
        // if(isset($params["d1"]) && $params["d1"]!="" && isset($params["d2"]) && $params["d2"]!=""){
        // $cdr = "content_date:[".$params["d1"]."T00:00:00Z TO ".$params["d2"]."T23:59:59Z]";
        // }
        //-============= start fq content_date range
        //-============= start fq sentiment
        $sen = null;
        if(isset($params["is_sentiment"])&& $params["is_sentiment"] != "") {
            $sen = "is_sentiment:0";
            if(strtolower($params["is_sentiment"])== "negatif") {
                $sen = "is_sentiment:1";
            } else if(strtolower($params["is_sentiment"])== "positif") {
                $sen = "is_sentiment:2";
            }
        }
        //-============= end fq sentiment
        // $fq = $skey.$source.$cdr.$mt;
        // die($fq);
        $fq = array($content1,
                   $content2,
                   $content3,
                   $content4,
                   $source,
                   $cdr,
                   $mt,
                   $mt2,
                   $sen,
                   $sm_what_and,
                   $sm_what_or);
        // Zend_Debug::dump($fq);die();
        // Zend_Debug::dump($tmp);die();
        // Zend_Debug::dump('select?q=*%3A*&wt=json&indent=true'.$filter);die();
        try {
            // $client = $this->connect_solr();
            // $query = new SolrQuery('*:*');
            // foreach($fq as $k=>$v){
            // if($v!="" && $v!=null){
            // $query->addFilterQuery($v);
            // }
            // }
            // $query->addSortField('content_date', SolrQuery::ORDER_ASC);
            // // $query->setFacet(true);
            // // $query->setRows(0);
            // if($params["iDisplayStart"]!=''){
            // $query->setStart($params["iDisplayStart"]);
            // }
            // //$query->setFacetLimit();		
            // // $query->addFacetField('sm_term');
            // $response = $client->query($query);
            // $response_array = $response->getResponse();
            // // Zend_Debug::dump($response_array);die();
            // // $x = $response_array->facet_counts->facet_fields;
            // // Zend_Debug::dump($facet_data);die();
            // return $response_array->response->docs;
            $ff = new DOmas_Model_Withsolrpmas();
            $filter = '';

            foreach($fq as $k => $v) {
                if($v != "" && $v != null) {
                    $filter .= '&fq=' . urlencode($v);
                }
            }
            if($params["iDisplayStart"] != '') {
                $filter .= '&start=' . $params["iDisplayStart"];
            }
            // Zend_Debug::dump($filter);die();
            $tmp = $ff->get_by_rawurl('select?q=*%3A*&wt=json&indent=true&sort=content_date+desc' . 
                                      $filter);
            return $tmp["response"]["docs"];
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }
    }

    function get_count5w1h($params) {
    }

    function get_link($params) {
        $g = new Domas_Model_Pdash();
        $ff = new Domas_Model_Withsolrpmas();
        $str = "*:*";
        //-============= start fq content
        $skey = null;
        $arr_pencarian = array();
        $arr_searchkey = array();
        $pencarian = "";
        if($params["skey"] != "") {
            $arr_pencarian = explode(",", $params["skey"]);
        }
        if($params["searchkey"] != "") {
            $arr_searchkey = explode(",", $params["searchkey"]);
        }
        $tmp_tp = $g->get_topik_by_tema($params["cat"], $params["topik"]);
        // Zend_Debug::dump($tp);die();
        $tp = array();

        foreach($tmp_tp as $k => $v) {
            $st = $g->get_subtema_by_tema($v["tid"]);

            foreach($st as $k1 => $v1) {
                array_push($tp, $v1);
            }
        }
        // Zend_Debug::dump($tp);die();
        $arr_topik = array();

        foreach($tp as $k => $v) {
            array_push($arr_topik, trim(strtolower($v["name"])));
        }
        if(count(array_unique($arr_topik))> 500) {
            array_splice($arr_topik, 320);
        }
        // Zend_Debug::dump($arr_topik);die();
        $content1 = null;
        $sm_what_and = null;
        if(count($arr_pencarian)> 0) {
            $content1 = 'content:("' .(implode('" AND "', $arr_pencarian)). '") OR sm_what:("' .(implode('" OR "', array_unique($arr_pencarian))). '") OR label:("' .(implode('" OR "', array_unique($arr_pencarian))). '")';
        }
        $content2 = null;
        if(count($arr_topik)> 0) {
            $content2 = 'content:("' .(implode('" OR "', array_unique($arr_topik))). '")';
        }
        $content3 = null;
        $sm_what_or = null;
        if(count($arr_searchkey)> 0) {
            $content3 = 'content:("' .(implode('" OR "', array_unique($arr_searchkey))). '") OR sm_what:("' .(implode('" OR "', array_unique($arr_searchkey))). '") OR label:("' .(implode('" OR "', array_unique($arr_searchkey))). '")';
        }
        //-============= end fq content
        //-============= start fq date range
        $cdd = null;
        $d1 = date("Y-m-d", strtotime('monday last week'));
        $d2 = date("Y-m-d");
        if(isset($params['d1'])&& $params["d1"] != "" && isset($params['d2'])&& $params["d2"] != "") {
            // Zend_Debug::dump($params['d1']);
            // Zend_Debug::dump($params['d2']);
            $d1 = date("Y-m-d\TH:i:s\Z", strtotime($params['d1'] . " 00:00:00"));
            $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " 23:59:59"));
            if($params["x"] == "2") {
                $d1 = date("Y-m-d\TH:i:s\Z", strtotime($params['d1'] . " 00:00:00 -7 hours"));
                $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " 23:59:59 -7 hours"));
            }
            if(isset($params["last"])&& $params["last"] != "") {
                $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " " . date("H:i:s"). " -" . $params["last"] . " hour"));
            }
        }
        $cdd = "content_date:[" . $d1 . " TO " . $d2 . "]";
        //-============= start fq content_date range
        // $d1 = date("Y-m-d", strtotime('monday last week'));
        // $d2 = date("Y-m-d");
        // if(isset($params["d1"]) && $params["d1"]!="" && isset($params["d2"]) && $params["d2"]!=""){
        // $d1 = date("Y-m-d", strtotime($params['d1']));
        // $d2 = date("Y-m-d", strtotime($params['d2']));
        // // Zend_Debug::dump($d2);die();
        // }
        // // $filter .="&content_date:[".$d1.$d2."]";
        // $cdd = "content_date:[".$d1."T00:00:00Z TO ".$d2."T23:59:59Z]";
        //-============= end fq content_date range
        //-============= start fq sentiment
        $sen = null;
        if(isset($params["is_sentiment"])&& $params["is_sentiment"] != "") {
            $sen = "is_sentiment:0";
            if(strtolower($params["is_sentiment"])== "negatif") {
                $sen = "is_sentiment:1";
            } else if(strtolower($params["is_sentiment"])== "positif") {
                $sen = "is_sentiment:2";
            }
        }
        //-============= end fq sentiment
        //-============= start fq media_type
        $mt = null;
        if(isset($params["is_mediatype"])&& $params["is_mediatype"] != "") {
            if($params["is_mediatype"] == "nasional") {
                $mt = "&fq=is_mediatype:(" . urlencode("1 OR 2"). ")";
            } else {
                $mt = "&fq=is_mediatype:3";
            }
        }
        //-============= end fq media_type
        // die("s");
        $fq1 = $skey . $cdd . $sen;
        $fq = $skey . $cdd . $sen . $mt;
        $fq = array($content1,
                   $content2,
                   $content3,
                   $cdd,
                   $sen,
                   $sm_what_and,
                   $sm_what_or,
                    "entity_id:5 AND ss_isretweet:true");
        try {
            $client = $this->connect_solr();
            $query = new SolrQuery('*:*');

            foreach($fq as $k => $v) {
                if($v != "" && $v != null) {
                    $query->addFilterQuery($v);
                }
            }
            $query->addSortField('content_date', SolrQuery::ORDER_DESC);
            $query->addField("is_sentiment");
            $query->addField("id");
            $query->addField("content");
            $query->addField("ss_user_screenname");
            $query->addField("ss_retweeted_user_screenname");
            $query->setRows(1000);
            // $query->setFacetLimit(10);		
            // $query->addFacetField('sm_term');
            $response = $client->query($query);
            $response_array = $response->getResponse();
            // Zend_Debug::dump($response_array);die();
            // $x = $response_array->facet_counts->facet_fields;
            // Zend_Debug::dump($facet_data);die();
            return $response_array->response->docs;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }
    }

    function get_latest_all($params) {
        // Zend_Debug::dump($params);die();
        // $n = new Domas_Model_Solrfactminer();
        $g = new Domas_Model_Pdash();
        // $ff = new Domas_Model_Withsolrpmas();
        //-============= start fq content
        $skey = null;
        $arr_pencarian = array();
        $arr_searchkey = array();
        $arr_skeyexception = array();
        $pencarian = "";
        if($params["skey"] != "") {
            $arr_pencarian = explode(",", $params["skey"]);
        }
        if($params["searchkey"] != "") {
            $arr_searchkey = explode(",", $params["searchkey"]);
        }
        if($params["skeyexception"] != "") {
            $arr_skeyexception = explode(",", $params["skeyexception"]);
        }
        $tmp_tp = $g->get_topik_by_tema($params["cat"], $params["topik"]);
        // Zend_Debug::dump($tp);die();
        $tp = array();

        foreach($tmp_tp as $k => $v) {
            $st = $g->get_subtema_by_tema($v["tid"]);

            foreach($st as $k1 => $v1) {
                array_push($tp, $v1);
            }
        }
        // Zend_Debug::dump($tp);die();
        $arr_topik = array();

        foreach($tp as $k => $v) {
            array_push($arr_topik, trim(strtolower($v["name"])));
        }
        if(count(array_unique($arr_topik))> 500) {
            array_splice($arr_topik, 320);
        }
        $content1 = null;
        $sm_what_and = null;
        if(count($arr_pencarian)> 0) {
            $content1 = 'content:("' .(implode('" AND "', $arr_pencarian)). '") OR sm_what:("' .(implode('" AND "', $arr_pencarian)). '") OR label:("' .(implode('" AND "', $arr_pencarian)). '")';
        }
        $content2 = null;
        if(count($arr_topik)> 0) {
            $content2 = 'content:("' .(implode('" OR "', array_unique($arr_topik))). '")';
        }
        $content3 = null;
        $sm_what_or = null;
        if(count($arr_searchkey)> 0) {
            $content3 = 'content:("' .(implode('" OR "', array_unique($arr_searchkey))). '") OR sm_what:("' .(implode('" OR "', array_unique($arr_searchkey))). '") OR label:("' .(implode('" OR "', array_unique($arr_searchkey))). '")';
        }
        $content4 = null;
        $sm_what_or = null;
        if(count($arr_skeyexception)> 0) {
            $content3 = '-content:("' .(implode('" OR "', array_unique($arr_skeyexception))). '") OR -sm_what:("' .(implode('" OR "', array_unique($arr_skeyexception))). '") OR -label:("' .(implode('" OR "', array_unique($arr_skeyexception))). '")';
            // $sm_what_or = 'sm_what:("'.(implode('" OR "',array_unique($arr_searchkey))).'")';
        }
        $content1 =($content1 == "" && $content2 == "" && $content3 == "" && $content4 == "" ? NULL : $content1);
        //-============= end fq content
        //-============= start fq date range
        $cdd = null;
        $d1 = date("Y-m-d", strtotime('monday last week'));
        $d2 = date("Y-m-d");
        if(isset($params['d1'])&& $params["d1"] != "" && isset($params['d2'])&& $params["d2"] != "") {
            // Zend_Debug::dump($params['d1']);
            // Zend_Debug::dump($params['d2']);
            $d1 = date("Y-m-d\TH:i:s\Z", strtotime($params['d1'] . " 00:00:00"));
            $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " 23:59:59"));
            if($params["x"] == "2") {
                $d1 = date("Y-m-d\TH:i:s\Z", strtotime($params['d1'] . " 00:00:00 -7 hours"));
                $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " 23:59:59 -7 hours"));
            }
            if(isset($params["last"])&& $params["last"] != "") {
                $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " " . date("H:i:s"). " -" . $params["last"] . " hour"));
            }
            $dr = "[" . $d1 . " TO " . $d2 . "]";
            $cdd = "content_date:[" . $d1 . " TO " . $d2 . "]";
        }
        $sen = null;
        if(isset($params["is_sentiment"])&& $params["is_sentiment"] != "") {
            $sen = "is_sentiment:0";
            if(strtolower($params["is_sentiment"])== "negatif") {
                $sen = "is_sentiment:1";
            } else if(strtolower($params["is_sentiment"])== "positif") {
                $sen = "is_sentiment:2";
            }
        }
        //-============= end fq sentiment
        //-============= start fq provinsi kota
        $provinsi = null;
        $kota = null;
        if($params["provinsi"] != "") {
            $provinsi = "ss_wilayah:'" . $params["provinsi"] . "'";
        }
        if($params["kota"] != "") {
            $kota = "ss_kota:'" . $params["kota"] . "'";
        }
        //-============= end fq provinsi kota
        //-============= start fq mediatype
        $mt = null;
        if(isset($params["is_mediatype"])&& $params["is_mediatype"] != "") {
            if($params["l"] == "news" || $params["type"] == 2) {
                if($params["is_mediatype"] == "nasional") {
                    $mt = "is_mediatype:(1 OR 2)";
                } else {
                    $mt = "is_mediatype:3";
                }
            }
        }
        //-============= end fq mediatype
        $fq = array($content1,
                   $content2,
                   $content3,
                   $content4,
                   $cdd,
                   $sen,
                   $mt,
                   $sm_what_and,
                   $sm_what_or,
                   $provinsi,
                   $kota);
        if(isset($params["x"])) {
            $fq = array($content1,
                       $content2,
                       $content3,
                       $content4,
                       $cdd,
                       $sen,
                       $mt,
                       $sm_what_and,
                       $sm_what_or,
                       $provinsi,
                       $kota,
                        "entity_id:(" .$params["x"].")");
        }
        try {
            $client = $this->connect_solr();
            $query = new SolrQuery('*:*');

            foreach($fq as $k => $v) {
                if($v != "" && $v != null) {
                    $query->addFilterQuery($v);
                }
            }
            //-============= start fiter 
            $tf = 1;
            if(isset($params["tf"])&& $params["tf"] != "") {
                $tf = (int) $params["tf"];
            }
            //-============= end fiter
            $query->addSortField('content_date', $tf);
            // $query->addField("ss_user_screenname");
            // $query->addField("ss_retweeted_user_screenname");
            $query->setStart($params["start"]);
            $query->setRows(10);
            // $query->setFacetLimit(10);		
            // $query->addFacetField('sm_term');
            $response = $client->query($query);
            $response_array = $response->getResponse();
            // Zend_Debug::dump($response_array);die();
            // $x = $response_array->facet_counts->facet_fields;
          // Zend_Debug::dump($response_array->response->docs);die();
            return $response_array->response->docs;
            //edit
            // // // $ff = new Domas_Model_Withsolrpmas();
            // // // $filter='';
            // // // foreach($fq as $k=>$v){
            // // // if($v!="" && $v!=null){
            // // // $filter.='&fq='.urlencode($v);
            // // // }
            // // // }
            // // // if($params["tf"]!=1){
            // // // $filter.= '&sort=content_date+asc';
            // // // }else{
            // // // $filter.='&sort=content_date+desc';
            // // // }
            // // // // Zend_Debug::dump($filter);die();
            // // // $tmp = $ff->get_by_rawurl('select?q=*%3A*&wt=json&indent=true'.$filter);
            // // // return $tmp["response"]["docs"] ;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }
    }

    function get_latest($params) {
        // Zend_Debug::dump($params);die();
        // $n = new Domas_Model_Solrfactminer();
        $g = new Domas_Model_Pdash();
        // $ff = new Domas_Model_Withsolrpmas();
        //-============= start fq content
        $skey = null;
        $arr_pencarian = array();
        $arr_searchkey = array();
        $arr_skeyexception = array();
        $pencarian = "";
        if($params["skey"] != "") {
            $arr_pencarian = explode(",", $params["skey"]);
        }
        if($params["searchkey"] != "") {
            $arr_searchkey = explode(",", $params["searchkey"]);
        }
        if($params["skeyexception"] != "") {
            $arr_skeyexception = explode(",", $params["skeyexception"]);
        }
        $tmp_tp = $g->get_topik_by_tema($params["cat"], $params["topik"]);
        // Zend_Debug::dump($tp);die();
        $tp = array();

        foreach($tmp_tp as $k => $v) {
            $st = $g->get_subtema_by_tema($v["tid"]);

            foreach($st as $k1 => $v1) {
                array_push($tp, $v1);
            }
        }
        // Zend_Debug::dump($tp);die();
        $arr_topik = array();

        foreach($tp as $k => $v) {
            array_push($arr_topik, trim(strtolower($v["name"])));
        }
        if(count(array_unique($arr_topik))> 500) {
            array_splice($arr_topik, 320);
        }
        $content1 = null;
        $sm_what_and = null;
        if(count($arr_pencarian)> 0) {
            $content1 = 'content:("' .(implode('" AND "', $arr_pencarian)). '") OR sm_what:("' .(implode('" AND "', $arr_pencarian)). '") OR label:("' .(implode('" AND "', $arr_pencarian)). '")';
        }
        $content2 = null;
        if(count($arr_topik)> 0) {
            $content2 = 'content:("' .(implode('" OR "', array_unique($arr_topik))). '")';
        }
        $content3 = null;
        $sm_what_or = null;
        if(count($arr_searchkey)> 0) {
            $content3 = 'content:("' .(implode('" OR "', array_unique($arr_searchkey))). '") OR sm_what:("' .(implode('" OR "', array_unique($arr_searchkey))). '") OR label:("' .(implode('" OR "', array_unique($arr_searchkey))). '")';
        }
        $content4 = null;
        $sm_what_or = null;
        if(count($arr_skeyexception)> 0) {
            $content3 = '-content:("' .(implode('" OR "', array_unique($arr_skeyexception))). '") OR -sm_what:("' .(implode('" OR "', array_unique($arr_skeyexception))). '") OR -label:("' .(implode('" OR "', array_unique($arr_skeyexception))). '")';
            // $sm_what_or = 'sm_what:("'.(implode('" OR "',array_unique($arr_searchkey))).'")';
        }
        $content1 =($content1 == "" && $content2 == "" && $content3 == "" && $content4 == "" ? NULL : $content1);
        //-============= end fq content
        //-============= start fq date range
        $cdd = null;
        $d1 = date("Y-m-d", strtotime('monday last week'));
        $d2 = date("Y-m-d");
        if(isset($params['d1'])&& $params["d1"] != "" && isset($params['d2'])&& $params["d2"] != "") {
            // Zend_Debug::dump($params['d1']);
            // Zend_Debug::dump($params['d2']);
            $d1 = date("Y-m-d\TH:i:s\Z", strtotime($params['d1'] . " 00:00:00"));
            $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " 23:59:59"));
            if($params["x"] == "2") {
                $d1 = date("Y-m-d\TH:i:s\Z", strtotime($params['d1'] . " 00:00:00 -7 hours"));
                $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " 23:59:59 -7 hours"));
            }
            if(isset($params["last"])&& $params["last"] != "") {
                $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " " . date("H:i:s"). " -" . $params["last"] . " hour"));
            }
            $dr = "[" . $d1 . " TO " . $d2 . "]";
            $cdd = "content_date:[" . $d1 . " TO " . $d2 . "]";
        }
        $sen = null;
        if(isset($params["is_sentiment"])&& $params["is_sentiment"] != "") {
            $sen = "is_sentiment:0";
            if(strtolower($params["is_sentiment"])== "negatif") {
                $sen = "is_sentiment:1";
            } else if(strtolower($params["is_sentiment"])== "positif") {
                $sen = "is_sentiment:2";
            }
        }
        //-============= end fq sentiment
        //-============= start fq provinsi kota
        $provinsi = null;
        $kota = null;
        if($params["provinsi"] != "") {
            $provinsi = "ss_wilayah:'" . $params["provinsi"] . "'";
        }
        if($params["kota"] != "") {
            $kota = "ss_kota:'" . $params["kota"] . "'";
        }
        //-============= end fq provinsi kota
        //-============= start fq mediatype
        $mt = null;
        if(isset($params["is_mediatype"])&& $params["is_mediatype"] != "") {
            if($params["l"] == "news" || $params["type"] == 2) {
                if($params["is_mediatype"] == "nasional") {
                    $mt = "is_mediatype:(1 OR 2)";
                } else {
                    $mt = "is_mediatype:3";
                }
            }
        }
        //-============= end fq mediatype
        $fq = array($content1,
                   $content2,
                   $content3,
                   $content4,
                    // $cdd,$sen,$mt,
                   $sm_what_and,
                   $sm_what_or,
                   $provinsi,
                   $kota);
        if(isset($params["x"])) {
            $fq = array($content1,
                       $content2,
                       $content3,
                       $content4,
                        // $cdd,$sen,$mt,
                       $sm_what_and,
                       $sm_what_or,
                       $provinsi,
                       $kota,
                        "entity_id:" .$params["x"]);
        }
        try {
            $client = $this->connect_solr();
            $query = new SolrQuery('*:*');

            foreach($fq as $k => $v) {
                if($v != "" && $v != null) {
                    $query->addFilterQuery($v);
                }
            }
            //-============= start fiter 
            $tf = 1;
            if(isset($params["tf"])&& $params["tf"] != "") {
                $tf = (int) $params["tf"];
            }
            //-============= end fiter
            $query->addSortField('content_date', $tf);
            // $query->addField("ss_user_screenname");
            // $query->addField("ss_retweeted_user_screenname");
            $query->setStart($params["start"]);
            $query->setRows(10);
            // $query->setFacetLimit(10);		
            // $query->addFacetField('sm_term');
            $response = $client->query($query);
            $response_array = $response->getResponse();
            // Zend_Debug::dump($response_array);die();
            // $x = $response_array->facet_counts->facet_fields;
            // Zend_Debug::dump($facet_data);die();
            return $response_array->response->docs;
            //edit
            // // // $ff = new Domas_Model_Withsolrpmas();
            // // // $filter='';
            // // // foreach($fq as $k=>$v){
            // // // if($v!="" && $v!=null){
            // // // $filter.='&fq='.urlencode($v);
            // // // }
            // // // }
            // // // if($params["tf"]!=1){
            // // // $filter.= '&sort=content_date+asc';
            // // // }else{
            // // // $filter.='&sort=content_date+desc';
            // // // }
            // // // // Zend_Debug::dump($filter);die();
            // // // $tmp = $ff->get_by_rawurl('select?q=*%3A*&wt=json&indent=true'.$filter);
            // // // return $tmp["response"]["docs"] ;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }
    }

    function get_toptren_twitter($params) {
        // Zend_Debug::dump($params);die();
        $g = new Domas_Model_Pdash();
        $ff = new Domas_Model_Withsolrpmas();
        $str = "*:*";
        //-============= start fq content
        $skey = null;
        $arr_pencarian = array();
        $arr_searchkey = array();
        $arr_skeyexception = array();
        $pencarian = "";
        if($params["skey"] != "") {
            $arr_pencarian = explode(",", $params["skey"]);
        }
        if($params["searchkey"] != "") {
            $arr_searchkey = explode(",", $params["searchkey"]);
        }
        if($params["skeyexception"] != "") {
            $arr_skeyexception = explode(",", $params["skeyexception"]);
        }
        $tmp_tp = $g->get_topik_by_tema($params["cat"], $params["topik"]);
        // Zend_Debug::dump($tp);die();
        $tp = array();

        foreach($tmp_tp as $k => $v) {
            $st = $g->get_subtema_by_tema($v["tid"]);

            foreach($st as $k1 => $v1) {
                array_push($tp, $v1);
            }
        }
        // Zend_Debug::dump($tp);die();
        $arr_topik = array();

        foreach($tp as $k => $v) {
            array_push($arr_topik, trim(strtolower($v["name"])));
        }
        // Zend_Debug::dump($arr_topik);die();
        if(count(array_unique($arr_topik))> 500) {
            array_splice($arr_topik, 320);
        }
        $content1 = null;
        $sm_what_and = null;
        if(count($arr_pencarian)> 0) {
            $content1 = '&fq=content:("' . urlencode(implode('" AND "', $arr_pencarian)). '")+OR+sm_what:("' . urlencode(implode('" OR "', $arr_pencarian)). '")+OR+label:("' . urlencode(implode('" OR "', $arr_pencarian)). '")';
        }
        $content2 = null;
        if(count($arr_topik)> 0) {
            $content2 = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_topik))). '")';
        }
        $content3 = null;
        $sm_what_or = null;
        if(count($arr_searchkey)> 0) {
            $content3 = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")+OR+sm_what:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")+OR+label:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")';
        }
        $content4 = null;
        $sm_what_or = null;
        if(count($arr_skeyexception)> 0) {
            $content3 = '&fq=-content:("' . urlencode(implode('" OR "', array_unique($arr_skeyexception))). '")+OR+-sm_what:("' . urlencode(implode('" OR "', array_unique($arr_skeyexception))). '")+OR+-label:("' .(implode('" OR "', array_unique($arr_skeyexception))). '")';
            // $sm_what_or = 'sm_what:("'.(implode('" OR "',array_unique($arr_searchkey))).'")';
        }
        //-============= end fq content
        //-============= start fq date range
        $cdd = null;
        $d1 = date("Y-m-d", strtotime('monday last week'));
        $d2 = date("Y-m-d");
        if(isset($params['d1'])&& $params["d1"] != "" && isset($params['d2'])&& $params["d2"] != "") {
            // Zend_Debug::dump($params['d1']);
            // Zend_Debug::dump($params['d2']);
            $d1 = date("Y-m-d\TH:i:s\Z", strtotime($params['d1'] . " 00:00:00"));
            $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " 23:59:59"));
            if($params["x"] == "2") {
                $d1 = date("Y-m-d\TH:i:s\Z", strtotime($params['d1'] . " 00:00:00 -7 hours"));
                $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " 23:59:59 -7 hours"));
                $d1 = date("Y-m-d\T00:00:00\Z", strtotime($d1 . " -1 days"));
                $d2 = date("Y-m-d\T23:59:59\Z", strtotime($d2 . " +1 days"));
            }
            if(isset($params["last"])&& $params["last"] != "") {
                $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " " . date("H:i:s"). " -" . $params["last"] . " hour"));
            }
        }
        $cdd = "&fq=content_date:[" . urlencode($d1). "+TO+" . urlencode($d2). "]";
        //-============= start fq content_date range
        // $d1 = date("Y-m-d\T00:00:00\Z", strtotime('monday last week'));
        // $d2 = "NOW";
        // if(isset($params["d1"]) && $params["d1"]!="" && isset($params["d2"]) && $params["d2"]!=""){
        // $d1 =  gmdate("Y-m-d\TH:i:s\Z", strtotime($params["d1"]));
        // $d2 =  gmdate("Y-m-d\TH:i:s\Z", strtotime($params["d2"]));
        // $d1 = date("Y-m-d\T00:00:00\Z", strtotime($params['d1']." -1 days"));
        // $d2 = date("Y-m-d\T23:59:59\Z", strtotime($params['d2']." +1 days"));
        // if(isset($params["gap"]) && $params["gap"]=="+1HOUR"){
        // $d1 = date("Y-m-d\T00:00:00\Z", strtotime($params['d1']));
        // $d2 = date("Y-m-d\T23:59:59\Z", strtotime($params['d2']));
        // }
        // // Zend_Debug::dump($d2);die();
        // }
        // // $filter .="&content_date:[".$d1.$d2."]";
        // $cdd = "&fq=content_date:[".urlencode($d1)."+TO+".urlencode($d2)."]";
        //-============= end fq content_date range
        //-============= start fq sentiment
        $sen = null;
        if(isset($params["is_sentiment"])&& $params["is_sentiment"] != "") {
            $sen = "&fq=is_sentiment:0";
            if(strtolower($params["is_sentiment"])== "negatif") {
                $sen = "&fq=is_sentiment:1";
            } else if(strtolower($params["is_sentiment"])== "positif") {
                $sen = "&fq=is_sentiment:2";
            }
        }
        //-============= end fq sentiment
        //-============= start fq media_type
        $mt = null;
        // if(isset($params["is_mediatype"]) && $params["is_mediatype"]!=""){
        // if($params["is_mediatype"]=="nasional"){
        // $mt = "&fq=is_mediatype:(".urlencode("1 OR 2").")";
        // } else {
        // $mt = "&fq=is_mediatype:3";	
        // }
        // }
        //-============= end fq media_type
        $frg = urlencode("+1DAY");
        if(isset($params["gap"])&& $params["gap"] != "") {
            $frg = urlencode($params["gap"]);
        }
        $fq1 = $content1 . $content2 . $content3 . $content4 . $cdd . $sen . $sm_what_and . $sm_what_or;
        $fq = $content1 . $content2 . $content3 . $content4 . $cdd . $sen . $mt . $sm_what_and . $sm_what_or;
        // die("select?q=".urlencode($str)."&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.gap=%2B1DAY&fq=entity_id%3A+2".$fq);
        // $fq = null;
        if($params["buzz"] == "buzzer") {
            $pivot = "&facet.pivot={!key=data}ss_retweeted_user_screenname,ss_retweeted_id";
        } else {
            $pivot = "&facet.pivot={!key=data}ss_retweeted_text,ss_retweeted_user_screenname,ss_retweeted_id";
        }
        // die("select?q=*%3A*&fq=entity_id%3A5&sort=content_date+desc&rows=0&wt=json&indent=true&facet=true".$pivot.$fq);
        $news =($ff->get_by_rawurl("select?q=*%3A*&fq=entity_id%3A5&sort=content_date+desc&rows=0&wt=json&indent=true&facet=true" . $pivot . $fq));
        // Zend_Debug::dump($news["facet_counts"]["facet_pivot"]["data"]);die();
        $tmp = $news["facet_counts"]["facet_pivot"]["data"];
        // Zend_Debug::dump($tmp);die();
        $data = array();
        $i = 0;

        foreach($tmp as $k0 => $v0) {
            // Zend_Debug::dump($v0);die();
            // foreach($v0["pivot"] as $k1=>$v1){				
            // $data[$k0]["ss_retweeted_user_screenname"] = $v1["value"];
            // // $data[$k0]["ss_retweeted_id"] = $v2["value"];
            // // $data[$k0]["count"] = $v2["count"];	
            // // foreach($v1["pivot"] as $k2=>$v2){
            // // $data[$k0]["ss_retweeted_id"] = $v2["value"];
            // // $data[$k0]["count"] = $v2["count"];		
            // // }
            // }
            if($params["buzz"] == "buzzer") {
                if($i <= 29) {
                    $data[$k0]["ss_retweeted_user_screenname"] = $v0["value"];
                    $data[$k0]["ss_retweeted_id"] = $v0["pivot"][0]["value"];
                    $data[$k0]["count"] = $v0["count"];
                    $i ++;
                }
            } else {
                $data[$k0]["ss_retweeted_user_screenname"] = $v0["pivot"][0]["value"];
                $data[$k0]["ss_retweeted_id"] = $v0["pivot"][0]["pivot"][0]["value"];
                $data[$k0]["ss_retweeted_text"] = $v0["value"];
                $data[$k0]["count"] = $v0["pivot"][0]["pivot"][0]["count"];
            }
        }
        // Zend_Debug::dump($data);die();
        return $data;
    }

    function get_toptren_news($params) {
        // Zend_Debug::dump($params);die();
        $g = new Domas_Model_Pdash();
        $ff = new Domas_Model_Withsolrpmas();
        $str = "*:*";
        //-============= start fq content
        $skey = null;
        $arr_pencarian = array();
        $arr_searchkey = array();
        $arr_skeyexception = array();
        $pencarian = "";
        if($params["skey"] != "") {
            $arr_pencarian = explode(",", $params["skey"]);
        }
        if($params["searchkey"] != "") {
            $arr_searchkey = explode(",", $params["searchkey"]);
        }
        if($params["skeyexception"] != "") {
            $arr_skeyexception = explode(",", $params["skeyexception"]);
        }
        $tmp_tp = $g->get_topik_by_tema($params["cat"], $params["topik"]);
        // Zend_Debug::dump($tp);die();
        $tp = array();

        foreach($tmp_tp as $k => $v) {
            $st = $g->get_subtema_by_tema($v["tid"]);

            foreach($st as $k1 => $v1) {
                array_push($tp, $v1);
            }
        }
        // Zend_Debug::dump($tp);die();
        $arr_topik = array();

        foreach($tp as $k => $v) {
            array_push($arr_topik, trim(strtolower($v["name"])));
        }
        // Zend_Debug::dump($arr_topik);die();
        if(count(array_unique($arr_topik))> 500) {
            array_splice($arr_topik, 320);
        }
        $content1 = null;
        $sm_what_and = null;
        if(count($arr_pencarian)> 0) {
            $content1 = '&fq=content:("' . urlencode(implode('" AND "', $arr_pencarian)). '")+OR+sm_what:("' . urlencode(implode('" OR "', $arr_pencarian)). '")+OR+label:("' . urlencode(implode('" OR "', $arr_pencarian)). '")';
        }
        $content2 = null;
        if(count($arr_topik)> 0) {
            $content2 = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_topik))). '")';
        }
        $content3 = null;
        $sm_what_or = null;
        if(count($arr_searchkey)> 0) {
            $content3 = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")+OR+sm_what:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")+OR+label:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")';
        }
        $content4 = null;
        $sm_what_or = null;
        if(count($arr_skeyexception)> 0) {
            $content3 = '&fq=-content:("' . urlencode(implode('" OR "', array_unique($arr_skeyexception))). '")+OR+-sm_what:("' . urlencode(implode('" OR "', array_unique($arr_skeyexception))). '")+OR+-label:("' .(implode('" OR "', array_unique($arr_skeyexception))). '")';
            // $sm_what_or = 'sm_what:("'.(implode('" OR "',array_unique($arr_searchkey))).'")';
        }
        //-============= end fq content
        //-============= start fq date range
        $cdd = null;
        $d1 = date("Y-m-d", strtotime('monday last week'));
        $d2 = date("Y-m-d");
        if(isset($params['d1'])&& $params["d1"] != "" && isset($params['d2'])&& $params["d2"] != "") {
            // Zend_Debug::dump($params['d1']);
            // Zend_Debug::dump($params['d2']);
            $d1 = date("Y-m-d\TH:i:s\Z", strtotime($params['d1'] . " 00:00:00"));
            $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " 23:59:59"));
            if($params["x"] == "2") {
                $d1 = date("Y-m-d\TH:i:s\Z", strtotime($params['d1'] . " 00:00:00 -7 hours"));
                $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " 23:59:59 -7 hours"));
                $d1 = date("Y-m-d\T00:00:00\Z", strtotime($d1 . " -1 days"));
                $d2 = date("Y-m-d\T23:59:59\Z", strtotime($d2 . " +1 days"));
            }
            if(isset($params["last"])&& $params["last"] != "") {
                $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " " . date("H:i:s"). " -" . $params["last"] . " hour"));
            }
        }
        $cdd = "&fq=content_date:[" . urlencode($d1). "+TO+" . urlencode($d2). "]";
        //-============= start fq content_date range
        // $d1 = date("Y-m-d\T00:00:00\Z", strtotime('monday last week'));
        // $d2 = "NOW";
        // if(isset($params["d1"]) && $params["d1"]!="" && isset($params["d2"]) && $params["d2"]!=""){
        // $d1 =  gmdate("Y-m-d\TH:i:s\Z", strtotime($params["d1"]));
        // $d2 =  gmdate("Y-m-d\TH:i:s\Z", strtotime($params["d2"]));
        // $d1 = date("Y-m-d\T00:00:00\Z", strtotime($params['d1']." -1 days"));
        // $d2 = date("Y-m-d\T23:59:59\Z", strtotime($params['d2']." +1 days"));
        // if(isset($params["gap"]) && $params["gap"]=="+1HOUR"){
        // $d1 = date("Y-m-d\T00:00:00\Z", strtotime($params['d1']));
        // $d2 = date("Y-m-d\T23:59:59\Z", strtotime($params['d2']));
        // }
        // // Zend_Debug::dump($d2);die();
        // }
        // // $filter .="&content_date:[".$d1.$d2."]";
        // $cdd = "&fq=content_date:[".urlencode($d1)."+TO+".urlencode($d2)."]";
        //-============= end fq content_date range
        //-============= start fq sentiment
        $sen = null;
        if(isset($params["is_sentiment"])&& $params["is_sentiment"] != "") {
            $sen = "&fq=is_sentiment:0";
            if(strtolower($params["is_sentiment"])== "negatif") {
                $sen = "&fq=is_sentiment:1";
            } else if(strtolower($params["is_sentiment"])== "positif") {
                $sen = "&fq=is_sentiment:2";
            }
        }
        //-============= end fq sentiment
        //-============= start fq media_type
        $mt = null;
        // if(isset($params["is_mediatype"]) && $params["is_mediatype"]!=""){
        // if($params["is_mediatype"]=="nasional"){
        // $mt = "&fq=is_mediatype:(".urlencode("1 OR 2").")";
        // } else {
        // $mt = "&fq=is_mediatype:3";	
        // }
        // }
        //-============= end fq media_type
        $frg = urlencode("+1DAY");
        if(isset($params["gap"])&& $params["gap"] != "") {
            $frg = urlencode($params["gap"]);
        }
        $fq1 = $content1 . $content2 . $content3 . $content4 . $cdd . $sen . $sm_what_and . $sm_what_or;
        $fq = $content1 . $content2 . $content3 . $content4 . $cdd . $sen . $mt . $sm_what_and . $sm_what_or;
        // die("select?q=".urlencode($str)."&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.gap=%2B1DAY&fq=entity_id%3A+2".$fq);
        // $fq = null;
        // die("select?q=*%3A*&fq=entity_id%3A5&sort=content_date+desc&rows=0&wt=json&indent=true&facet=true&facet.pivot={!key=data}ss_retweeted_text,ss_retweeted_user_screenname,ss_retweeted_id".$fq);
        $news =($ff->get_by_rawurl("select?q=*%3A*&fq=entity_id%3A2&rows=0&wt=json&indent=true&facet=true&facet.field=bundle_name" . $fq));
        // Zend_Debug::dump($news);die();
        $result = array();
        $i = 0;

        foreach($news["facet_counts"]["facet_fields"]["bundle_name"] as $k => $v) {
            if($k % 2 == 0 && $news["facet_counts"]["facet_fields"]["bundle_name"][$k + 1] > 0 && $i < 10) {
                // $result[$v] = $news["facet_counts"]["facet_fields"]["bundle_name"][$k+1];
                $result[$i]["content"] = $v;
                $result[$i]["url"] = "http://" . $v;
                $result[$i]["count"] = $news["facet_counts"]["facet_fields"]["bundle_name"][$k + 1];
                $i ++;
            }
        }
        // Zend_Debug::dump($result);die();
        return $result;
    }

    function get_trentop10_1($attr, $value, $xml) {
        $attr = preg_quote($attr);
        $value = preg_quote($value);
        $tag_regex = '/<ul[^>]*' . $attr . '="' . $value . '">(.*?)<\\/ul>/si';
        preg_match($tag_regex, $xml, $matches);
        return $matches[1];
    }

    function get_trentop10_2($attr, $value, $xml) {
        $attr = preg_quote($attr);
        $value = preg_quote($value);
        $tag_regex = '/<div[^>]*' . $attr . '="' . $value . '">(.*?)<\\/div>/si';
        preg_match($tag_regex, $xml, $matches);
        return $matches[1];
    }

    function get_top_news_by_div($attr, $value, $xml) {
        $attr = preg_quote($attr);
        $value = preg_quote($value);
        $tag_regex = '/<div[^>]*' . $attr . '="' . $value . '">(.*?)<\\/div>/si';
        preg_match($tag_regex, $xml, $matches);
        return $matches[1];
    }

    function highlight_regex_as_html($regex, $raw_html) {
        $regex = array('Jafar M Sidik',
                       'Frans Lebu Raya');
        $text = $raw_html;

        foreach($regex as $k => $v) {
            preg_match_all('/[A-Z][^\\.;]*(' . 
                                           $v . 
                                           ')[^\\.;]*/', $v, $m);
            if(!$m)
                $text;
            $re = preg_match_all('/[A-Z][^\\.;]*(' . 
                                                 $v . 
                                                 ')[^\\.;]*/', $v, $m);
            $x = preg_replace($re, '<span class="label label-info">$0</span>', $text);
        }
    }

    function get_trendline_sii($params) {
        // Zend_Debug::dump($params);die('d');
        $g = new Domas_Model_Pdash();
        $ff = new Domas_Model_Withsolrpmas();
        $str = "*:*";
        //-============= start fq content
        $skey = null;
        $arr_pencarian = array();
        $arr_searchkey = array();
        $pencarian = "";
        if($params["skey"] != "") {
            $arr_pencarian = explode(",", $params["skey"]);
        }
        if($params["searchkey"] != "") {
            $arr_searchkey = explode(",", $params["searchkey"]);
        }
        $tmp_tp = $g->get_topik_by_tema($params["cat"], $params["topik"]);
        // Zend_Debug::dump($tp);die();
        $tp = array();

        foreach($tmp_tp as $k => $v) {
            $st = $g->get_subtema_by_tema($v["tid"]);

            foreach($st as $k1 => $v1) {
                array_push($tp, $v1);
            }
        }
        // Zend_Debug::dump($tp);die();
        $arr_topik = array();

        foreach($tp as $k => $v) {
            array_push($arr_topik, trim(strtolower($v["name"])));
        }
        // Zend_Debug::dump($arr_topik);die();
        if(count(array_unique($arr_topik))> 500) {
            array_splice($arr_topik, 320);
        }
        $content1 = null;
        $sm_what_and = null;
        if(count($arr_pencarian)> 0) {
            $content1 = '&fq=content:("' . urlencode(implode('" AND "', $arr_pencarian)). '")+OR+sm_what:("' . urlencode(implode('" OR "', $arr_pencarian)). '")+OR+label:("' . urlencode(implode('" OR "', $arr_pencarian)). '")';
        }
        $content2 = null;
        if(count($arr_topik)> 0) {
            $content2 = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_topik))). '")';
        }
        $content3 = null;
        $sm_what_or = null;
        if(count($arr_searchkey)> 0) {
            $content3 = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")+OR+sm_what:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")+OR+label:("' . urlencode(implode('" OR "', array_unique($arr_searchkey))). '")';
        }
        //-============= end fq content
        //-============= start fq date range
        $cdd = null;
        $d1 = date("Y-m-d", strtotime('monday last week'));
        $d2 = date("Y-m-d");
        if(isset($params['d1'])&& $params["d1"] != "" && isset($params['d2'])&& $params["d2"] != "") {
            // Zend_Debug::dump($params['d1']);
            // Zend_Debug::dump($params['d2']);
            $d1 = date("Y-m-d\TH:i:s\Z", strtotime($params['d1'] . " 00:00:00"));
            $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " 23:59:59"));
            if($params["x"] == "2") {
                $d1 = date("Y-m-d\TH:i:s\Z", strtotime($params['d1'] . " 00:00:00 -7 hours"));
                $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " 23:59:59 -7 hours"));
            }
            if(isset($params["last"])&& $params["last"] != "") {
                $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " " . date("H:i:s"). " -" . $params["last"] . " hour"));
            }
            $d1 = date("Y-m-d\TH:i:s\Z", strtotime($d1 . " -1 days"));
            $d2 = date("Y-m-d\TH:i:s\Z", strtotime($d2 . " +1 days"));
        }
        $cdd = "&facet.range.start=" . urlencode($d1). "&facet.range.end=" . urlencode($d2);
        //-============= start fq content_date range
        // $d1 = date("Y-m-d\T00:00:00\Z", strtotime('monday last week'));
        // $d2 = "NOW";
        // if(isset($params["d1"]) && $params["d1"]!="" && isset($params["d2"]) && $params["d2"]!=""){
        // $d1 =  gmdate("Y-m-d\TH:i:s\Z", strtotime($params["d1"]));
        // $d2 =  gmdate("Y-m-d\TH:i:s\Z", strtotime($params["d2"]));
        // $d1 = date("Y-m-d\T00:00:00\Z", strtotime($params['d1']." -1 days"));
        // $d2 = date("Y-m-d\T23:59:59\Z", strtotime($params['d2']." +1 days"));
        // // Zend_Debug::dump($d2);die();
        // }
        // // $filter .="&content_date:[".$d1.$d2."]";
        // $cdd = "&facet.range.start=".urlencode($d1)."&facet.range.end=".urlencode($d2);
        //-============= end fq content_date range
        //-============= start fq sentiment
        $sen = null;
        if(isset($params["is_sentiment"])&& $params["is_sentiment"] != "") {
            $sen = "&fq=is_sentiment:0";
            if(strtolower($params["is_sentiment"])== "negatif") {
                $sen = "&fq=is_sentiment:1";
            } else if(strtolower($params["is_sentiment"])== "positif") {
                $sen = "&fq=is_sentiment:2";
            }
        }
        //-============= end fq sentiment
        //-============= start fq media_type
        $mt = null;
        if(isset($params["is_mediatype"])&& $params["is_mediatype"] != "") {
            if($params["is_mediatype"] == "nasional") {
                $mt = "&fq=is_mediatype:(" . urlencode("1 OR 2"). ")";
            } else {
                $mt = "&fq=is_mediatype:3";
            }
        }
        //-============= end fq media_type
        $fq1 = $content1 . $content2 . $content3 . $cdd . $sen . $sm_what_and . $sm_what_or;
        $fq = $content1 . $content2 . $content3 . $cdd . $sen . $mt . $sm_what_and . $sm_what_or;
        // die("select?q=".urlencode($str)."&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.gap=%2B1DAY&fq=entity_id%3A+2".$fq);
        try {
            $laporan =($ff->get_by_rawurl("select?q=" . urlencode($str). "&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.gap=%2B1HOUR&fq=entity_id%3A+6" . $fq1));
            // Zend_Debug::dump($laporan);die();
            $data = array("Laporan" =>$laporan["facet_counts"]["facet_ranges"]["content_date"]["counts"],
                         );
            // Zend_Debug::dump($data);die();
            return $data;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }
    }

    function listFolderFiles($dir) {
        $ffs = preg_grep('/^([^.])/', scandir($dir, 1));
        natcasesort($ffs);
        // Zend_Debug::dump($ffs);die();	
        $data = array();
        $i = 0;

        foreach($ffs as $k => $ff) {
            if($ff != '.' && $ff != '..') {
                $ext = pathinfo($dir . 
                                "/" . 
                                $ff, PATHINFO_EXTENSION);
                $arr_icon = array("word" => "fa-file-word-o icon-state-info",
                                  "excel" => "fa-file-excel-o icon-state-success",
                                  "pdf" => "fa-file-pdf-o icon-state-danger",
                                  "mp4" => "fa-file-video-o icon-state-danger",
                                 );
                $icon =($ext != "" ? $arr_icon[$ext] : "fa-file-o");
                $file = "file";
                $children = false;
                if(is_dir($dir . '/' . $ff)) {
                    $icon = "fa-folder icon-state-warning";
                    $file = "root";
                    $children = true;
                }
                $data[$i]["id"] = $dir . "/" . $ff;
                $data[$i]["text"] = $ff;
                $data[$i]["icon"] = "fa " . $icon . " icon-lg";
                $data[$i]["children"] = $children;
                $data[$i]["type"] = $file;
                $data[$i]["ext"] = $ext;
                $i ++;
            }
        }
        return $data;
    }

    function highlight($text, $words) {
        preg_match_all('~\w+~', $words, $m);
        // Zend_Debug::dump($words);//die();
        if(!$m)
            return $text;
        $re = '~\\b(' . implode('|', $m[0]). ')\\b~';
        // Zend_Debug::dump($text);
        // Zend_Debug::dump($re);die();
        return preg_replace("/\w*?" . 
                            preg_quote($words). 
                                       "\w*/i", "<a href=\"/profilingmodul?search=$0\" target=\"_blank\" style=\"color:#d5ff5b;\">$0</a>", $text);
        return preg_replace($re, '<span style="color:#d5ff5b;">$0</span>', $text);
        // $replace=array_flip(array_flip($words)); // remove duplicates
        // $pattern=array();
        // foreach ($replace as $k=>$fword) {
        // $pattern[]='/\b(' . $fword . ')(?!>)\b/i';
        // $replace[$k]='<b>$0</b>';
        // }
        // return preg_replace($pattern, $replace, $text);
    }

    function highlight2($text, $words) {
        preg_match_all('~\w+~', $words, $m);
        // Zend_Debug::dump($words);die();
        if(!$m)
            return $text;
        $re = '~\\b(' . implode('|', $m[0]). ')\\b~';
        return preg_replace("/\w*?" . 
                            preg_quote($words). 
                                       "\w*/i", "$0", $text);
        return preg_replace($re, '<span style="color:#d5ff5b;">$0</span>', $text);
        // $replace=array_flip(array_flip($words)); // remove duplicates
        // $pattern=array();
        // foreach ($replace as $k=>$fword) {
        // $pattern[]='/\b(' . $fword . ')(?!>)\b/i';
        // $replace[$k]='<b>$0</b>';
        // }
        // return preg_replace($pattern, $replace, $text);
    }

    function highlight3($text, $words) {
        preg_match_all('~\w+~', $words, $m);
        // Zend_Debug::dump($m);//die();
        $aa = $words;
        if(!$m)
            return $text;
        $re = '~\\b(' . implode('|', $m[0]). ')\\b~';
        // Zend_Debug::dump($words);//die();
        // Zend_Debug::dump("/\w*?".preg_quote($words)."\w*/i");//die();
        // Zend_Debug::dump(preg_replace($re, '<span style="color:#d5ff5b;">$0</span>',$text));//die();
        return preg_replace("/\w*?" . 
                            preg_quote($words). 
                                       "\w*/i", "<a href=\"/profilingmodul?search=" . 
                                       utf8_encode($aa). 
                                                   "\" target=\"_blank\" style=\"color:#d5ff5b;\">$0</a>", $text);
        // return preg_replace($re, '<a href="/profilingmodul?search='.preg_quote($words).'" target=\"_blank\"><span style="color:#d5ff5b;">$0</span></a>',$text);
        // $replace=array_flip(array_flip($words)); // remove duplicates
        // $pattern=array();
        // foreach ($replace as $k=>$fword) {
        // $pattern[]='/\b(' . $fword . ')(?!>)\b/i';
        // $replace[$k]='<b>$0</b>';
        // }
        // return preg_replace($pattern, $replace, $text);
    }

    function cekanomali($params) {
        // Zend_Debug::dump($params);die('ok');
        if(isset($params["skey"])&& $params["skey"] != "") {
            $params["skey"] = explode(',', $params["skey"]);
            $params["skey"] = implode(" OR ", $params["skey"]);
            $params["skey"] = str_replace("  ", " ", $params["skey"]);
            // Zend_Debug::dump($params["skey"]);
        }
        // die();
        $str = "*:*";
        $search = null;
        if(isset($params["skey"])&& $params["skey"] != "") {
            $search = "&fq=content:(" . urlencode($params["skey"]). ")";
        }
        $g = new Domas_Model_Pdash();
        $ff = new Domas_Model_Withsolrpmas();
        // $params["cat"] = "Politik";
        // if(isset($params["cat"])&&$params["cat"]!=""){
        // $cc = new Domas_Model_Pdash();
        // $gettid = $cc->get_keys_pmas($params['cat']);
        // $filter = implode(" OR  ", $gettid);
        // $filter .= "&fq=tid:(" . urlencode($filter). ")";
        // }
        $tmp_tp = $g->get_topik_by_tema($params["cat"], $params["topik"]);
        // Zend_Debug::dump($tp);die();
        $tp = array();

        foreach($tmp_tp as $k => $v) {
            $st = $g->get_subtema_by_tema($v["tid"]);

            foreach($st as $k1 => $v1) {
                array_push($tp, $v1);
            }
        }
        // Zend_Debug::dump($tp);die();
        $arr_topik = array();

        foreach($tp as $k => $v) {
            array_push($arr_topik, trim(strtolower($v["name"])));
        }
        // Zend_Debug::dump($arr_topik);die();
        if(count(array_unique($arr_topik))> 500) {
            array_splice($arr_topik, 320);
        }
        $topik = null;
        if(count($arr_topik)> 0) {
            $topik = '&fq=content:("' . urlencode(implode('" OR "', array_unique($arr_topik))). '")';
        }
        $selisih =((abs(strtotime($params['d1'])- strtotime($params['d2'])))/(60 * 60 * 24));
        if(($selisih % 2)== 0) {
            $da = $selisih / 2;
            $i = $da - 1;
            $dx1 = date("Y-m-d\T00:00:00\Z", strtotime($params['d1']));
            $dx2 = date("Y-m-d\T23:59:59\Z", strtotime($params['d1'] . " " . $i . " days"));
            $dy1 = date("Y-m-d\T00:00:00\Z", strtotime($params['d2'] . " -" . $da . " days"));
            $dy2 = date("Y-m-d\T23:59:59\Z", strtotime($params['d2']));
        } else {
            $da =($selisih + 1)/ 2;
            $i = $da - 1;
            $dx1 = date("Y-m-d\T00:00:00\Z", strtotime($params['d1']));
            $dx2 = date("Y-m-d\T23:59:59\Z", strtotime($params['d1'] . " " . $i . " days"));
            $dy1 = date("Y-m-d\T00:00:00\Z", strtotime($params['d2'] . " -" . $i . " days"));
            $dy2 = date("Y-m-d\T23:59:59\Z", strtotime($params['d2']));
        }
        // Zend_Debug::dump($dx1);Zend_Debug::dump($dx2);die();
        // Zend_Debug::dump($dy1);Zend_Debug::dump($dy2);
        // if(isset($params["d1"]) && $params["d1"]!="" && isset($params["d2"]) && $params["d2"]!=""){
        // $d1 = date("Y-m-d\T00:00:00\Z", strtotime($params['d1']." -1 days"));
        // $d2 = date("Y-m-d\T23:59:59\Z", strtotime($params['d2']));
        // Zend_Debug::dump($d2);die();
        // }
        // die();
        $sen = null;
        if(isset($params["is_sentiment"])&& $params["is_sentiment"] != "") {
            $sen = "&fq=is_sentiment:0";
            if($params["is_sentiment"] == "negatif") {
                $sen = "&fq=is_sentiment:1";
            } else if($params["is_sentiment"] == "positif") {
                $sen = "&fq=is_sentiment:2";
            }
        }
        $cont = "content:[* TO *]";
        //content is not null
        $cont = "&fq=" . urlencode($cont);
        $filter = $cont . $search . $sen . $topik;
        // $mt1=null;
        // if(isset($params["is_mediatype"]) && $params["is_mediatype"]!=""){
        // if($params["is_mediatype"]=="nasional"){
        // $x = "1 OR 2";
        // $mt1 = "is_mediatype:(".urlencode($x).")";
        // } else {
        // $mt1 = "is_mediatype:3";	
        // }
        // $filter .="&fq=".$mt1;
        // }
        // Zend_Debug::dump("select?q=".urlencode($str)."&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.start=" . urlencode($dx1). "&facet.range.end=" . urlencode($dx2). "&facet.range.gap=%2B1DAY&fq=entity_id%3A+2".$filter);die();
        // $ff = new Domas_Model_Withsolrpmas();
        $x =($ff->get_by_rawurl("select?q=" . urlencode($str). "&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.start=" . urlencode($dx1). "&facet.range.end=" . urlencode($dx2). "&facet.range.gap=%2B1DAY&fq=entity_id:(2+OR+5)" . $filter));
        $y =($ff->get_by_rawurl("select?q=" . urlencode($str). "&wt=json&rows=0&facet.range=content_date&facet=true&facet.range.start=" . urlencode($dy1). "&facet.range.end=" . urlencode($dy2). "&facet.range.gap=%2B1DAY&fq=entity_id:(2+OR+5)" . $filter));
        Zend_Debug::dump($x["facet_counts"]["facet_ranges"]["content_date"]["counts"]);
        Zend_Debug::dump($y["facet_counts"]["facet_ranges"]["content_date"]["counts"]);
        die();
        try {
            $xr = 0;

            foreach($x["facet_counts"]["facet_ranges"]["content_date"]["counts"] as $k => $v) {
                if($k % 2 == 1) {
                    $countx = $countx + $v;
                    $xr ++;
                }
            }
            $yr = 0;

            foreach($y["facet_counts"]["facet_ranges"]["content_date"]["counts"] as $k => $v) {
                if($k % 2 == 1) {
                    $county = $county + $v;
                    $yr ++;
                }
            }
            $ratax = $countx / $xr;
            $ratay = $county / $yr;
            if(isset($countx)&& $countx != "" && isset($county)&& $county != "") {
                if($ratay > $ratax) {
                    $anomali["data"] = array($xr,
                                            $yr,
                                            $dx1,
                                            $dx2,
                                            $dy1,
                                            $dy2);
                }
            }
            // Zend_Debug::dump($anomali);die('ss');
            // Zend_Debug::dump($anomali);die('ss');
            // Zend_Debug::dump($anomali);die('ss');
            return $anomali;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }
    }

    function get_linksna($params) {
        // Zend_Debug::dump($params);die();
        // $n = new Domas_Model_Solrfactminer();
        $g = new Domas_Model_Pdash();
        //-============= start get  is_mediatype
        $arr_mt = array("nasional" => 1,
                        "lokal" => 2,
                        "internasional" => 3,
                        "blog" => 4,
                       );
        $mt = null;
        $mt2 = null;
        // if($params["sSearch_2"]!=""){			
        // $params["t"]=$params["sSearch_2"];
        // }
        if($params["t"] == "Laporan") {
            if($params["is_mediatype"] == "nasional") {
                $mt =("is_mediatype:1 OR is_mediatype:2");
            } else {
                $mt =("is_mediatype:" . 
                      $arr_mt[$params["is_mediatype"]]);
            }
            $mt2 = "sm_who:[* TO *] AND sm_where:[* TO *]";
        }
        //-============= end get is_mediatype
        // Zend_Debug::dump($stopword);die();
        //-============= end get stopword
        //-============= start fq doc_tx
        $skey = null;
        $arr_pencarian = array();
        $arr_searchkey = array();
        $arr_skeyexception = array();
        $pencarian = "";
        if($params["skey"] != "") {
            $arr_pencarian = explode(",", $params["skey"]);
        }
        if($params["searchkey"] != "") {
            $arr_searchkey = explode(",", $params["searchkey"]);
        }
        if($params["skeyexception"] != "") {
            $arr_skeyexception = explode(",", $params["skeyexception"]);
        }
        $tmp_tp = $g->get_topik_by_tema($params["cat"], $params["topik"]);
        // Zend_Debug::dump($tp);die();
        $tp = array();

        foreach($tmp_tp as $k => $v) {
            $st = $g->get_subtema_by_tema($v["tid"]);

            foreach($st as $k1 => $v1) {
                array_push($tp, $v1);
            }
        }
        // Zend_Debug::dump($tp);die();
        $arr_topik = array();

        foreach($tp as $k => $v) {
            array_push($arr_topik, trim(strtolower($v["name"])));
        }
        if(count(array_unique($arr_topik))> 500) {
            array_splice($arr_topik, 320);
        }
        // array_unique($arr_topik);
        // Zend_Debug::dump(array_unique($arr_topik));die();
        $content1 = null;
        $sm_what_and = null;
        if(count($arr_pencarian)> 0) {
            $content1 = 'content:("' .(implode('" AND "', $arr_pencarian)). '") OR sm_what:("' .(implode('" OR "', array_unique($arr_pencarian))). '") OR label:("' .(implode('" OR "', array_unique($arr_pencarian))). '")';
        }
        $content2 = null;
        if(count($arr_topik)> 0) {
            $content2 = 'content:("' .(implode('" OR "', array_unique($arr_topik))). '")';
        }
        $content3 = null;
        $sm_what_or = null;
        if(count($arr_searchkey)> 0) {
            $content3 = 'content:("' .(implode('" OR "', array_unique($arr_searchkey))). '") OR sm_what:("' .(implode('" OR "', array_unique($arr_searchkey))). '") OR label:("' .(implode('" OR "', array_unique($arr_searchkey))). '")';
        }
        $content4 = null;
        $sm_what_or = null;
        if(count($arr_skeyexception)> 0) {
            $content3 = '-content:("' .(implode('" OR "', array_unique($arr_skeyexception))). '") OR -sm_what:("' .(implode('" OR "', array_unique($arr_skeyexception))). '") OR label:("' .(implode('" OR "', array_unique($arr_skeyexception))). '")';
            // $sm_what_or = 'sm_what:("'.(implode('" OR "',array_unique($arr_searchkey))).'")';
        }
        $content1 =($content1 == "" && $content2 == "" && $content3 == "" && $content4 == "" ? NULL : $content1);
        //-============= end fq content
        //-============= start fq source
        $source = null;
        $ent = null;
        if(isset($params["t"])&& $params["t"] != "") {
            $e_id = array("News" => 2,
                          "news" => 2,
                          "Social Media" => 5,
                          "Media Sosial" => 5,
                          "Laporan" => 6,
                          "BDI" => 21,
                         );
            $source .= "entity_id:" . $e_id[$params["t"]];
            if($params["t"] == "Laporan") {
                $source = "entity_id:(6 OR 21)";
            }
        }
        //-============= end fq source
        //-============= start fq date range
        $cdd = null;
        $d1 = date("Y-m-d", strtotime('monday last week'));
        $d2 = date("Y-m-d");
        if(isset($params['d1'])&& $params["d1"] != "" && isset($params['d2'])&& $params["d2"] != "") {
            // Zend_Debug::dump($params['d1']);
            // Zend_Debug::dump($params['d2']);
            $d1 = date("Y-m-d\TH:i:s\Z", strtotime($params['d1'] . " 00:00:00"));
            $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " 23:59:59"));
            if($params["x"] == "2") {
                $d1 = date("Y-m-d\TH:i:s\Z", strtotime($params['d1'] . " 00:00:00 -7 hours"));
                $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " 23:59:59 -7 hours"));
            }
            if(isset($params["last"])&& $params["last"] != "") {
                $d2 = date("Y-m-d\TH:i:s\Z", strtotime($params['d2'] . " " . date("H:i:s"). " -" . $params["last"] . " hour"));
            }
        } else {
            $d1 = date("Y-m-d\TH:i:s\Z", strtotime($d1 . " 00:00:00"));
            $d2 = date("Y-m-d\TH:i:s\Z", strtotime($d2 . " 23:59:59"));
        }
        $cdr = "content_date:[" . $d1 . " TO " . $d2 . "]";
        //-============= start fq content_date range
        // $cdr = null;
        // if(isset($params["d1"]) && $params["d1"]!="" && isset($params["d2"]) && $params["d2"]!=""){
        // $cdr = "content_date:[".$params["d1"]."T00:00:00Z TO ".$params["d2"]."T23:59:59Z]";
        // }
        //-============= start fq content_date range
        //-============= start fq sentiment
        $sen = null;
        if(isset($params["is_sentiment"])&& $params["is_sentiment"] != "") {
            $sen = "is_sentiment:0";
            if(strtolower($params["is_sentiment"])== "negatif") {
                $sen = "is_sentiment:1";
            } else if(strtolower($params["is_sentiment"])== "positif") {
                $sen = "is_sentiment:2";
            }
        }
        //-============= end fq sentiment
        // $fq = $skey.$source.$cdr.$mt;
        // die($fq);
        $fq = array($content1,
                   $content2,
                   $content3,
                   $content4,
                   $source,
                   $cdr,
                   $mt,
                   $mt2,
                   $sen,
                   $sm_what_and,
                   $sm_what_or);
        // Zend_Debug::dump($fq);die();
        try {
            $client = $this->connect_solr();
            $query = new SolrQuery('*:*');

            foreach($fq as $k => $v) {
                if($v != "" && $v != null) {
                    $query->addFilterQuery($v);
                }
            }
            $query->addSortField('content_date', SolrQuery::ORDER_ASC);
            // $query->setFacet(true);
            $query->setRows(5);
            if($params["iDisplayStart"] != '') {
                $query->setStart($params["iDisplayStart"]);
            }
            // $query->setFields('id','content','sm_who');		
            $query->addFacetField('sm_term', 'content');
            $response = $client->query($query);
            $response_array = $response->getResponse();
            // Zend_Debug::dump($response_array);die();
            // $x = $response_array->facet_counts->facet_fields;
            Zend_Debug::dump($response_array);
            die();
            return $response_array->response->docs;
        }
        catch(Exception $e) {
            Zend_Debug::dump($e->getMessage());
            die();
        }
    }
}
