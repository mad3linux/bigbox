<?php

class Domas_Model_Withsolarium extends Zend_Db_Table_Abstract {

    function connect_solr() {
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'production');
        $config = $config->toArray();
        $configsolarium = array(
            'endpoint' => array(
                'localhost' => array(
                    'host' => $config['data']['solr']['host'],
                    'port' => $config['data']['solr']['port'],
                    'path' => $config['data']['solr']['path'],
                )
            )
        );
        try {
            return new Solarium\Client($configsolarium);
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    public function test() {
        try {

            $client = $this->connect_solr();
            $query = $client->createQuery($client::QUERY_SELECT);

            $resultset = $client->execute($query);
            echo 'NumFound: ' . $resultset->getNumFound();

            die();
            return ($response);
        } catch (Exception $e) {

            Zend_Debug::dump($e);
            die();
        }
    }

    public function update_tags($data, $id) {
        try {
           
            //die();
           // Zend_Debug::dump($data); die();    
            $client = $this->connect_solr();
            $update = $client->createUpdate();
            $doc2 = $update->createDocument();
            $doc2->setKey('id', urldecode($id));
            foreach ($data as $k => $v) {
                foreach ($v as $k2 => $v2) {
                    $doc2->addField("tm_vid_" . $k . "_names", $v2);
                    $doc2->addField("im_vid_" . $k, $k2);
                    $doc2->addField("tid", $k2);
                }
                $doc2->setFieldModifier("tm_vid_" . $k . "_names", 'set');
            }

            $update->addDocument($doc2);
            $update->addCommit();
            $result = $client->update($update);
           // Zend_Debug::dump($result ); die();
            return $result;
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            // die();
        }
    }

    public function mapping_tags_groups($params, $tags) {
        try {


            foreach ($params['raw1'] as $z) {

                $this->update_tags($tags, $z);
            }
        } catch (Exception $e) {
            
        }
    }

    public function update_attrs($data, $id) {
        try {

            $client = $this->connect_solr();
            $update = $client->createUpdate();
            $doc2 = $update->createDocument();
            // $id = str_replace("~", "\~", $id);
            $doc2->setKey('id', $id);
            foreach ($data['atr_key']as $k => $v) {
                if ($data['cek_val'][$k] == 1) {
                    $doc2->addField("ts_" . $data['atr_key'][$k], $data['atr_val'][$k]);
                    $doc2->setFieldModifier("ts_" . $data['atr_key'][$k], 'set');
                } else {
                    $doc2->addField("ss_" . $data['atr_key'][$k], $data['atr_val'][$k]);
                    $doc2->setFieldModifier("ss_" . $data['atr_key'][$k], 'set');
                }
            }



            $update->addDocument($doc2);
            $update->addCommit();
            $result = $client->update($update);
            return $result;
        } catch (Exception $e) {
            Zend_Debug::dump($e);
            // die();
        }
    }

}
