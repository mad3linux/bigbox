<?php

class Domas_Model_Withsolariumtopic extends Zend_Db_Table_Abstract {

    function connect_solr() {
        $config = new Zend_Config_Ini(APPLICATION_PATH . 
                                      '/configs/application.ini', 'production');
        $config = $config->toArray();
        $configsolarium = array('endpoint' => array('localhost' => array('host' =>$config['data']['pmas']['solr']['host'],
                                'port' =>$config['data']['pmas']['solr']['port'],
                                'path' =>$config['data']['pmas']['solr']['path'],
                               )));
        try {
            return new Solarium\Client($configsolarium);
        }
        catch(Exception $e) {
            Zend_Debug::dump($e);
            die();
        }
    }

    public function update5w1h($param) {
        try {
			// Zend_Debug::dump($param["who"]);
			// Zend_Debug::dump($topic); 
			// Zend_Debug::dump($predict); die('asd');
			// $who=trim(preg_replace('/\s\s+/', ', ', $param["who"]));
			$who=explode(', ', preg_replace('/\s\s+/', ', ',$param["who"]));
			unset($who[count($who)-1]);
			
			// explode("\n", $str);
			// $who .= (substr($param["who"],0,1)=="n"?substr($$param["who"],1):$$param["who"]);
			// Zend_Debug::dump($param["id"]);
	
			 // die('as');
            $client = $this->connect_solr();
						
            $update = $client->createUpdate();
            $doc2 = $update->createDocument();
			// Zend_Debug::dump($update); die();
            $doc2->setKey('id', base64_decode($param["id"]));
			
            $doc2->addField("sm_who", $who);
            $doc2->setFieldModifier("sm_who", 'set');
						
            $doc2->addField("sm_what", $param["what"]);
            $doc2->setFieldModifier("sm_what", 'set');
						
			$doc2->addField("sm_when", $param["when"]);
            $doc2->setFieldModifier("sm_when", 'set');
						
			$doc2->addField("sm_where", $param["where"]);
            $doc2->setFieldModifier("sm_where", 'set');
						
			$doc2->addField("sm_why", $param["why"]);
            $doc2->setFieldModifier("sm_why", 'set');
						
			$doc2->addField("sm_how", $param["how"]);
            $doc2->setFieldModifier("sm_how", 'set');						

            $update->addDocument($doc2);
            $update->addCommit();
            $result = $client->update($update);
            //Zend_Debug::dump($result ); die();
            return $result;
        }
        catch(Exception $e) {
            //Zend_Debug::dump($e);
           // die();
        }
    }

    public function update_topic($id, $topic, $is_sentiment) {
        try {
			 // Zend_Debug::dump($stype);
			
            $dd = new Pmas_Model_General();
            $tags = $dd->get_tags_for_insert($tid);
			// Zend_Debug::dump($tags);die();
			// Zend_Debug::dump($topic); 
			// Zend_Debug::dump($is_sentiment); die('asd');
			// Zend_Debug::dump($tags); die();
            $client = $this->connect_solr();
            $update = $client->createUpdate();
            $doc2 = $update->createDocument();
			// Zend_Debug::dump($update); die();
            // $doc2->setKey('id', $id);
            $doc2->setKey('id', base64_decode($param["id"]));
           
			$doc2->addField("topic", $topic);
			$doc2->setFieldModifier("topic", 'set');
			
			// $doc2->addField("is_sentiment",  $is_sentiment);
			$doc2->addField("is_sentiment", $param["is_sentiment"]);
			$doc2->setFieldModifier("is_sentiment", 'set');
			
			// $doc2->addField("stype", $param["stype"]);
			// $doc2->setFieldModifier("stype", 'set');
           
            $update->addDocument($doc2);
            $update->addCommit();
            $result = $client->update($update);
            // Zend_Debug::dump($result ); die();
            return $result;
        }
        catch(Exception $e) {
            // Zend_Debug::dump($e);
            // die();
        }
    }
		
}