<?php

/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class CMS_Application_Resource_Cache extends Zend_Application_Resource_ResourceAbstract {

    public function init() {
        $options = $this->getOptions();
        // Get a Zend_Cache_Core object
        $cache = Zend_Cache::factory($options['frontEnd'], $options['backEnd'], $options['frontEndOptions'], $options['backEndOptions']);
        Zend_Registry::set('cache', $cache);
        $options['backEndOptions']['lifetime'] = null;
        
        $cachef = Zend_Cache::factory('core', $options['backEnd'], $options['frontEndOptions'], $options['backEndOptions']);
        
        Zend_Registry::set('cacheff', $cachef);
        return $cache;
    }
}
