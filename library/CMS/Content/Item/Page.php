<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class CMS_Content_Item_Page extends CMS_Content_Item_Abstract
{
    public $id;
    public $name;
    public $headline;
    public $image;
    public $description;
    public $content;
}
?>
