<?php
/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class CMS_Controller_Plugin_Nl extends Zend_Controller_Plugin_Abstract
{
    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
    {

        $module = $request->getModuleName();
        //die($module);
        $front = Zend_Controller_Front::getInstance();
        $layout = $front->getParam('bootstrap')->getResource('layout');

        $layoutDir = $front->getModuleDirectory() . '/views/layouts';
        $layout->setLayoutPath($layoutDir);     


    }
}












