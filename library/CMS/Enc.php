<?php
class CMS_Enc
{
	
	function __unserialize($text) {
		$text = unserialize(base64_decode($text)); 
		$text = preg_replace(array("@&#58;@", "@&#37;@", "@&###;@", "@&#60;@", "@&#62;@"), array(':', '%', '/', '<', '>'), $text);
		return $text;
		}

		function __serialize($text) {
		$text = preg_replace(array("@:@", "@%@", "@\/@", "@\<@", "@\>@"), array('&#58;', '&#37;', '&###;', '&#60;', '&#62;'), $text);
		return base64_encode(serialize($text));
		}
   function encrypt($data)
   {
       return base64_encode(serialize($this->_encryption_key() .'-'. $data));
   }
   function decrypt($data)
   {
	   //var_dump($data); die();
       $sig = unserialize(base64_decode( $this->_encryption_key() .'-'. $data));
       return explode('-',$sig);        
   }
   
   function _encryption_key()
   {
       return 'rahasia';
   }
   
   function encode($data)
   {
       return base64_encode(serialize($data));
   }
   
   function decode($data)
   {
       return unserialize(base64_decode($data));
   }

}
