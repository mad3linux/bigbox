<?php
	/*
		By Afiat Darmawan
	*/
	class CMS_Form {
			function openForm($obj) {
				if (isset($obj["method"])==false)
					$obj["method"]="post";
				
				return "<form 
						".($obj["name"]?"name=\"".$obj["name"]."\"":"")."
						".($obj["id"]?"id=\"".$obj["id"]."\"":"")."
						".($obj["method"]?"method=\"".$obj["method"]."\"":"")."
						".($obj["enctype"]?"enctype=\"".$obj["enctype"]."\"":"")."
						".($obj["target"]?"target=\"".$obj["target"]."\"":"")."
						".($obj["action"]?"action=\"".$obj["action"]."\"":"")."
						".($obj["class"]?"class=\"".$obj["class"]."\"":"")."
						".($obj["style"]?"style=\"".$obj["style"]."\"":"")."
						".($obj["onSubmit"]?"onSubmit=\"".$obj["onSubmit"]."\"":"")."
					>";
			}
			
			function text($obj) {
				return "<input type='textbox' 
						".($obj["name"]?"name=\"".$obj["name"]."\"":"")."
						".($obj["id"]?"id=\"".$obj["id"]."\"":"")."
						".($obj["class"]?"class=\"".$obj["class"]."\"":"")."
						".(isset($obj["value"])?"value=\"".$obj["value"]."\"":"")."
						".($obj["size"]?"size=\"".$obj["size"]."\"":"")."
						".($obj["maxlength"]?"maxlength=\"".$obj["maxlength"]."\"":"")."
						".($obj["style"]?"style=\"".$obj["style"]."\"":"")."
						".($obj["readonly"]?"readonly=1":"")."
						".($obj["onKeyUp"]?"onKeyUp=\"".$obj["onKeyUp"]."\"":"")."
						".($obj["onKeyPress"]?"onKeyPress=\"".$obj["onKeyPress"]."\"":"")."
						>
				";
			}
			
			function pass($obj) {
				return "<input type='password' 
						".($obj["name"]?"name=\"".$obj["name"]."\"":"")."
						".($obj["id"]?"id=\"".$obj["id"]."\"":"")."
						".($obj["class"]?"class=\"".$obj["class"]."\"":"")."
						".(isset($obj["value"])?"value=\"".$obj["value"]."\"":"")."
						".($obj["size"]?"size=\"".$obj["size"]."\"":"")."
						".($obj["maxlength"]?"maxlength=\"".$obj["maxlength"]."\"":"")."
						".($obj["onKeyUp"]?"onKeyUp=\"".$obj["onKeyUp"]."\"":"")."
						".($obj["style"]?"style=\"".$obj["style"]."\"":"")."
						".($obj["readonly"]?"readonly=1":"")."
						".($obj["onKeyPress"]?"onKeyPress=\"".$obj["onKeyPress"]."\"":"")."
					>";
			}
			
			function select($obj) {
				
				$ret = "<select
						".($obj["name"]?"name=\"".$obj["name"]."\"":"")."
						".($obj["id"]?"id=\"".$obj["id"]."\"":"")."
						".($obj["class"]?"class=\"".$obj["class"]."\"":"")."
						".($obj["onClick"]?"onClick=\"".$obj["onClick"]."\"":"")."
						".($obj["onChange"]?"onChange=\"".$obj["onChange"]."\"":"")."
						".($obj["size"]?"size=\"".$obj["size"]."\"":"")."
						".($obj["style"]?"style=\"".$obj["style"]."\"":"")."
						".($obj["multiple"]?"multiple":"")."
						".($obj["readonly"]?"readonly=1":"")."
						".($obj["disabled"]?"disabled":"")."
						>";
				
				if ($obj["multiple"]) {
					for ($i=0;$i<count($obj["data"]);$i++) {
						reset($obj["value"]);
						$ketemu=false;
						while (list($k,$v)=each($obj["value"])) {
							if ($data[$i][0]==$v && $data[$i][0]!="")
								$ketemu=true;
						}
						if ($ketemu)
							$ret .="<option selected value='".$obj["data"][$i][0]."'>".$obj["data"][$i][1]."</option>";
						else
							$ret .="<option value='".$obj["data"][$i][0]."'>".$obj["data"][$i][1]."</option>";
					}		
				} else {
					if ($obj["data"][0][1])
						$ret .="<option value='".$obj["data"][0][0]."'>".$obj["data"][0][1]."</option>";
					for ($i=1;$i<count($obj["data"]);$i++) {
						if ($obj["value"]==$obj["data"][$i][0])
							$ret .="<option selected value='".$obj["data"][$i][0]."'>".$obj["data"][$i][1]."</option>";
						else
							$ret .="<option value='".$obj["data"][$i][0]."'>".$obj["data"][$i][1]."</option>";
					}
				}
				$ret .="</select>";
				return $ret;
			}
			
			function textarea($obj) {
				return "<textarea 
							".($obj["name"]?"name=\"".$obj["name"]."\"":"")."
							".($obj["id"]?"id=\"".$obj["id"]."\"":"")."
							".($obj["rows"]?"rows=\"".$obj["rows"]."\"":"")."
							".($obj["cols"]?"cols=\"".$obj["cols"]."\"":"")."
							".($obj["class"]?"class=\"".$obj["class"]."\"":"")."
							".($obj["style"]?"style=\"".$obj["style"]."\"":"")."
							".($obj["readonly"]?"readonly=1":"")."
						>".(isset($obj["value"])?$obj["value"]:"")."</textarea>";
			}
			
			function hidden($obj) {
				return "<input
							type='hidden'
							".($obj["name"]?"name=\"".$obj["name"]."\"":"")."
							".($obj["id"]?"id=\"".$obj["id"]."\"":"")."
							".(isset($obj["value"])?"value=\"".$obj["value"]."\"":"")."
						>";
			}
			
			function  checkbox($obj) {
				return "<input 
						type='checkbox'
						".($obj["name"]?"name=\"".$obj["name"]."\"":"")."
						".($obj["id"]?"id=\"".$obj["id"]."\"":"")."
						".(isset($obj["value"])?"value=\"".$obj["value"]."\"":"")."
						".($obj["class"]?"class=\"".$obj["class"]."\"":"")."
						".($obj["onClick"]?"onClick=\"".$obj["onClick"]."\"":"")."
						".(isset($obj["checked"])?"checked=\"".$obj["checked"]."\"":"")."
						".($obj["disabled"]?"disabled=1":"")."
					  >".($obj["display"]?$obj["display"]:"");
			}
			
			function  radio($obj) {
				return "<input 
						type='radio'
						".($obj["name"]?"name=\"".$obj["name"]."\"":"")."
						".($obj["id"]?"id=\"".$obj["id"]."\"":"")."
						".(isset($obj["value"])?"value=\"".$obj["value"]."\"":"")."
						".($obj["class"]?"class=\"".$obj["class"]."\"":"")."
						".($obj["onClick"]?"onClick=\"".$obj["onClick"]."\"":"")."
						".(isset($obj["checked"])?"checked=\"".$obj["checked"]."\"":"")."
						".($obj["disabled"]?"disabled=1":"")."
					  >".($obj["display"]?$obj["display"]:"");
			}
			
			function file($obj) {
				return "<input type='file' 
						".($obj["name"]?"name=\"".$obj["name"]."\"":"")."
						".($obj["id"]?"id=\"".$obj["id"]."\"":"")."
						".($obj["class"]?"class=\"".$obj["class"]."\"":"")."
						".(isset($obj["value"])?"value=\"".$obj["value"]."\"":"")."
						".($obj["size"]?"size=\"".$obj["size"]."\"":"")."
						".($obj["maxlength"]?"maxlength=\"".$obj["maxlength"]."\"":"")."
						".($obj["style"]?"style=\"".$obj["style"]."\"":"")."
						".($obj["readonly"]?"readonly=1":"")."
						".($obj["onKeyUp"]?"onKeyUp=\"".$obj["onKeyUp"]."\"":"")."
						".($obj["onKeyPress"]?"onKeyPress=\"".$obj["onKeyPress"]."\"":"")."
						>
				";
			}
			
			function submit($obj) {
				return "<input 
							type='submit'
							".($obj["name"]?"name=\"".$obj["name"]."\"":"")."
							".($obj["id"]?"id=\"".$obj["id"]."\"":"")."
							".($obj["class"]?"class=\"".$obj["class"]."\"":"")."
							".($obj["value"]?"value=\"".$obj["value"]."\"":"")."
							".($obj["onClick"]?"onClick=\"".$obj["onClick"]."\"":"")."
							".($obj["readonly"]?"readonly=1":"")."
							".($obj["disabled"]?"disabled=1":"")."
							>";
			}
			
			function button($obj) {
				return "<input 
							type='button'
							".($obj["name"]?"name=\"".$obj["name"]."\"":"")."
							".($obj["id"]?"id=\"".$obj["id"]."\"":"")."
							".($obj["class"]?"class=\"".$obj["class"]."\"":"")."
							".(isset($obj["value"])?"value=\"".$obj["value"]."\"":"")."
							".($obj["onClick"]?"onClick=\"".$obj["onClick"]."\"":"")."
							".($obj["href"]?"href=\"".$obj["href"]."\"":"")."
							".($obj["readonly"]?"readonly=1":"")."
							".($obj["disabled"]?"disabled=1":"")."
						>";
			}
			
			function tanggal($obj) {
				global $bit_app;
				$obj["readonly"]=1;
				$ret =$this->text($obj);
				$ret .="<img
							src='".$bit_app["images_url"]."/calendar.gif'
							onClick=\"return showCalendar('".$obj["id"]."', 'y-mm-dd');\" 
						>";
				return $ret;
			}
			
			
			function closeForm() {
				return "</form>";
			}
	}
?>
