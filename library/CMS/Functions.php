<?php
class CMS_Functions
{

function print_out($array = array(), $continue = TRUE)
{
    echo '<pre style="text-align: left; color: #000; border: 4px dashed #ddd; padding: 20px; width: max-width: 800px; overflow: auto; margin: 20px auto; background-color: #f0f0f0;">'.print_r($array,TRUE).'</pre>';
    if (!$continue) {
      die;
      exit;
    }
}
	
function __unserialize($text) {
    $text = unserialize(base64_decode($text)); 
    $text = preg_replace(array("@&#58;@", "@&#37;@", "@&###;@", "@&#60;@", "@&#62;@"), array(':', '%', '/', '<', '>'), $text);
    return $text;
}

function __serialize($text) {
    $text = preg_replace(array("@:@", "@%@", "@\/@", "@\<@", "@\>@"), array('&#58;', '&#37;', '&###;', '&#60;', '&#62;'), $text);
    return base64_encode(serialize($text));
}	
function convertToHoursMins($time, $format = '%d:%d') {
settype($time, 'integer');
if ($time < 1) {
return;
}
$hours = floor($time/60);
$minutes = $time%60;
return sprintf($format, $hours, $minutes);
}





function transpose($array) {
	$transposed_array = array();
	if($array) {
		foreach ($array as $row_key => $row) {
		if (is_array($row) && !empty($row)) { //check to see if there is a second dimension
		foreach ($row as $column_key => $element) {
	$transposed_array[$column_key][$row_key] = $element;
	}
	}
	else {
	$transposed_array[0][$row_key] = $row;
	}
	}
	return $transposed_array;
	}
	}
function index_warna($nilai)
{
    $nilai = $nilai * 100;    
    $idx = -1;
    if ($nilai < 90)
        $idx = 1;
    elseif ($nilai >= 90 and $nilai <= 98)
        $idx = 3;
    elseif ($nilai > 98 and $nilai < 100)
        $idx = 2;
    elseif ($nilai >= 100)
        $idx = 0;
    
    return $idx;
}

 function dd_html($name,$options=array(), $label='', $terpilih = -1,$class='',$add_blank_opt = false)
 {
    $html="";
    $html = '<label for="'.$name.'">'.$label.'</label>';     
    $html .= '<select class="'.$class.'" style="font-family: TitilliumWeb-Regular;" data-mini="true" data-theme="c" name="'.$name.'" id="'.$name.'">'."\n";
    #$selected = $terpilih == -1 ? ' selected="selected" ' : ' ';
    if ($add_blank_opt)
    $html .= "<option ".$selected."  value='-'>-- ALL --</option>\n";
    foreach($options as $val => $label)
    {
        $selected = $terpilih == $val ? ' selected="selected" ' : ' ';
        $html .= "<option ".$selected."  value='" . $val . "'>" . $label . "</option>\n";
    }
    $html .= '</select>';
    return $html;
}

 function dd_html_II($name,$options=array(), $terpilih=-1,$class='dd_s1',$style='width: 150px;',$add_blank_opt = false)
 {
    $html="";
    $html = '<select class="'.$class.'" style="'.$style.'" name="'.$name.'" id="'.$name.'">'."\n";
    #$selected = $terpilih == -1 ? ' selected="selected" ' : ' ';
    #$html .= "<option ".$selected."  value='-1'>-- Silahkan Pilih --</option>\n";
    if ($add_blank_opt)
    $html .= "<option value='-'>-- ALL --</option>\n";
    foreach($options as $val => $label)
    {
        $selected = $terpilih == $val ? ' selected="selected" ' : ' ';
        $html .= "<option ".$selected."  value='" . $val . "'>" . $label . "</option>\n";
    }
    $html .= '</select>';
    return $html;
}   

function dd_html_III($name,$options=array(), $label='', $terpilih = -1,$class='',$add_blank_opt = false)
 {
    $html="";
    $html = '<label for="'.$name.'">'.$label.'</label>';     
    $html .= '<select class="'.$class.'" style="font-family: TitilliumWeb-Regular;" data-mini="true" data-theme="c" name="'.$name.'" id="'.$name.'">'."\n";
    #$selected = $terpilih == -1 ? ' selected="selected" ' : ' ';
    if ($add_blank_opt)
    $html .= "<option ".$selected."  value='-'>-- ALL --</option>\n";
    foreach($options as $val => $label)
    {
        $selected = $terpilih == $val ? ' selected="selected" ' : ' ';
        $html .= "<option ".$selected."  value='" . $val . "'>" . $label . "</option>\n";
    }
    $html .= '</select>';
    return $html;
}

function get_ap_in_location()
{
    $qap = "select * from ".$table_prefix."p_map_event_nms where event_id = ".$max_eventid." and loc_id = ".$loc_id;
    $res_ap = mysql_query($qap);
    
}

function get_ont_status($conn,$db,$table_prefix,$loc_id = -1)
{
    $q = "select a.loc_id,a.loc_area,b.id_perangkat,d.current_state as status,b.olt_node,b.olt_port,b.nama_pelanggan,b.ont_nama,c.group_loc_name 
from ".$table_prefix."list_location a left join ".$table_prefix."ont_devices b on a.loc_id = b.loc_id 
left join ".$table_prefix."list_group_location c on  b.group_loc_id=c.group_loc_id left join v_status d on b.ont_ip_address = d.address"; 

    if ($loc_id != -1)
    {
        $q .= " where a.loc_id = ".$loc_id;
    }
    $q .= " order by a.loc_id, c.group_loc_name ";
    
    #print_out($q);
    
    $res = mysql_query($q,$conn);
    $rows = array();
    while ($row = mysql_fetch_assoc($res))
    {
        $rows[] = $row;
    }    
    return $rows;
}

function summary_table_all($title,$data_switch,$data_ap,$data_ont,$table_prefix,$eventid,$db,$conn,$complete_table = true)
{
    if ($complete_table)
    {
            $html = '<table class="tlist_dashboard fl" width="100%">
            <tr>
                <td colspan="8" class="tc kolomjudul">'.ucwords($title).'</td>
            </tr>                      
            <tr>
                <td class="kolomjudul tc">No.</td>
                <td class="kolomjudul">Location</td>
                <td class="tc"><img title="Access Point UP" src="/assets/images/wifi-up.png" style="height: 16px;" class="img_wifi" /></td>
                <td class="tc"><img title="Access Point Down" src="/assets/images/wifi-down.png" style="height: 16px;" class="img_wifi" /></td>
                <td class="tc">Tot</td>';
            if ($title == 'Reguler')
            {
             $html.='
                <td class="tc"><img title="Switch UP" src="/assets/images/switch-up.png" style="height: 16px;" class="img_wifi" /><br />O | A</td>
                <td class="tc"><img title="Switch Down" src="/assets/images/switch-down.png" style="height: 16px;" class="img_wifi" /><br />O | A</td>';
            }
            else
            {
             $html.='
                <td class="tc"><img title="Switch UP" src="/assets/images/switch-up.png" style="height: 16px;" class="img_wifi" /><br />O | C | A</td>
                <td class="tc"><img title="Switch Down" src="/assets/images/switch-down.png" style="height: 16px;" class="img_wifi" /><br />O | C | A</td>';
            }
            $html.='
                <td class="tc">Tot</td>
            </tr>';
    } else { $html = ''; }
            
            $ttotal = $tt_up = $tt_down = $ttotal_switch_cr = $ttotal_switch_acc = 0;
            $tt_up_switch_cr = $tt_up_switch_acc = $tt_down_switch_cr = $tt_down_switch_acc = 0;
            $ttotal_ce = $ttotal_up_ce = $ttotal_down_ce = 0;
            $ttotal_olt = $ttotal_up_olt = $ttotal_down_olt = 0;
            $ttotal_ont = $ttotal_up_ont = $ttotal_down_ont = 0;
            $no = 1;

            foreach($data_switch as $hotel => $lokasi) {
                $alias = "";
                $t_down_switch_cr = $t_down_switch_acc = $t_up_switch_cr = $t_up_switch_acc = $t_up = $t_down = 0;
                $t_down_olt = $t_up_olt = 0;
                $t_down_ce = $t_up_ce = 0;
                $t_down_ont = $t_up_ont = 0;
                
                $loc_id = $lokasi[0]['loc_id'];
                $t_up = array_key_exists($hotel,$data_ap) ? @$data_ap[$hotel]['UP'] : 0;
                $t_down = array_key_exists($hotel,$data_ap) ? @$data_ap[$hotel]['DOWN'] : 0;
                
                $t_up_ont = array_key_exists($hotel,$data_ont) ? @$data_ont[$hotel]['UP'] : 0;
                $t_down_ont = array_key_exists($hotel,$data_ont) ? @$data_ont[$hotel]['DOWN'] : 0;

                foreach($lokasi as $row)
                {
                    $alias = $row['alias'];
                    $switchs[] = $row['ip_address'];
                    if ($row['TIPE_SW'] == 'COR')
                    {
                        if ($row['current_state'] == '')
                            $t_down_switch_cr++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_switch_cr++;
                        elseif ($row['current_state'] == 1)
                            $t_down_switch_cr++;
                        
                    }
                    elseif ($row['TIPE_SW'] == 'ACC' || $row['TIPE_SW'] == '8AC')
                    {
                        if ($row['current_state'] == '')
                            $t_down_switch_acc++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_switch_acc++;
                        elseif ($row['current_state'] == 1)
                            $t_down_switch_acc++;
                        
                    }
                    elseif ($row['TIPE_SW'] == 'OLT')
                    {
                        if ($row['current_state'] == '')
                            $t_down_olt++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_olt++;
                        elseif ($row['current_state'] == 1)
                            $t_down_olt++;
                        
                    }
                    elseif ($row['TIPE_SW'] == 'CE-')
                    {
                        if ($row['current_state'] == '')
                            $t_down_ce++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_ce++;
                        elseif ($row['current_state'] == 1)
                            $t_down_ce++;
                        
                    }
                }
                
                $total = $t_up + $t_down;
                $link_t_down = number_format($t_down,0);
                $link_param = $BASEURL.'index.php?event_id='.$eventid.'&loc_name='.$hotel.'&loc_id='.$loc_id;
                if ( ($t_down > 0)) 
                {
                    $warna = ' merah';
                    $fclass = ' fmerah'; 
                    $trclass = 'highlight';
                    $link_t_down = '<span style="color:red;">'.number_format($t_down,0).'</span>';
                    
                }
                elseif ( ($t_up == 0) and ($t_down == 0))
                { 
                    $warna = ' abu';
                    $fclass = ' fputih'; 
                    $trclass = '';
                }
                elseif ( ($t_up > 0) and ($t_down == 0))
                { 
                    $warna = ' hijau';
                    $fclass = ' fhijau'; 
                    $trclass = '';
                }
                $fclass_up = $fclass_down = ' style="color: #fff;" ';
                
                if ($t_up > 0)
                    $fclass_up = ' style="color: #82D711;" ';
                if ($t_down > 0)
                    $fclass_down = ' style="color: #FF0000;" ';
                

                $total_switch_cr = $t_up_switch_cr + $t_down_switch_cr;
                $total_switch_acc = $t_up_switch_acc + $t_down_switch_acc;
                $link_t_down_switch_cr = number_format($t_down_switch_cr,0);
                $link_t_down_switch_acc = number_format($t_down_switch_acc,0);
                
                /** STYLING SWITCH COUNT STATISTIC */
                /** SWITCH CORE ========================= */
                if ( ($t_down_switch_cr > 0)) 
                {
                    $warna_switch_cr = ' merah';
                    $trclass = 'highlight';
                    #$link_t_down_switch_cr = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-1-switch" style="color:red;" class="link_switch">'.number_format($t_down_switch_cr,0).'</a>';
                    $link_t_down_switch_cr = '<span style="color:red;">'.number_format($t_down_switch_cr,0).'</span>';
                    
                }
                elseif ( ($t_up_switch_cr == 0) and ($t_down_switch_cr == 0))
                { 
                    $warna_switch_cr = ' abu';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_cr > 0) and ($t_down_switch_cr == 0))
                { 
                    $warna_switch_cr = ' hijau';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_cr > 0))
                {
                    //$trclass = '';
                }
                
                $fclass_switch_up_cr = $fclass_switch_down_cr = ' style="color: #fff;" ';
                $link_t_up_switch_cr = number_format($t_up_switch_cr,0);
                
                if ($t_up_switch_cr > 0) {
                    $fclass_switch_up_cr = ' style="color: #82D711;" ';
                    $link_t_up_switch_cr = '<span style="color:#82D711;" class="link_switch">'.number_format($t_up_switch_cr,0).'</span>';
                    }
                if ($t_down_switch_cr > 0)
                    $fclass_switch_down_cr = ' style="color: #FF0000;" ';
                
                $link_total_switch_cr = '<span style="color:#FFF;" class="link_switch">'.number_format($total_switch_cr,0).'</span>';              
            

                /** SWITCH ACCESS ========================= */
                if ( ($t_down_switch_acc > 0)) 
                {
                    $warna_switch_acc = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_switch_acc = '<span style="color:red;" class="link_switch">'.number_format($t_down_switch_acc,0).'</span>';
                    
                }
                elseif ( ($t_up_switch_acc == 0) and ($t_down_switch_acc == 0))
                { 
                    $warna_switch_acc = ' abu';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_acc > 0) and ($t_down_switch_acc == 0))
                { 
                    $warna_switch_acc = ' hijau';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_acc > 0))
                {
                    //$trclass = '';
                }
                
                $fclass_switch_up_acc = $fclass_switch_down_acc = ' style="color: #fff;" ';
                $link_t_up_switch_acc = number_format($t_up_switch_acc,0);
                
                if ($t_up_switch_acc > 0) {
                    $fclass_switch_up_acc = ' style="color: #82D711;" ';
                    $link_t_up_switch_acc = '<span style="color:#82D711;" class="link_switch">'.number_format($t_up_switch_acc,0).'</span>';
                    }
                if ($t_down_switch_acc > 0)
                    $fclass_switch_down_acc = ' style="color: #FF0000;" ';
                
                $link_total_switch_acc = '<span style="color:#FFF;" class="link_switch">'.number_format($total_switch_acc,0).'</span>';              

                
                /** OLT ========================= */
                $total_olt = $t_up_olt + $t_down_olt;
                $link_t_down_olt = number_format($t_down_olt,0);
                if ( ($t_down_olt > 0)) 
                {
                    $warna_olt = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_olt = '<span style="color:red;" class="link_olt">'.number_format($t_down_olt,0).'</span>';
                    
                }
                elseif ( ($t_up_olt == 0) and ($t_down_olt == 0))
                { 
                    $warna_olt = ' abu';
                    #$trclass = '';
                }
                elseif ( ($t_up_olt > 0) and ($t_down_olt == 0))
                { 
                    $warna_olt = ' hijau';
                    #$trclass = '';
                }
                elseif ( ($t_up_olt > 0))
                {
                    #$trclass = '';
                }
                
                $fclass_olt_up = $fclass_olt_down = ' style="color: #fff;" ';
                $link_t_up_olt = number_format($t_up_olt,0);
                
                if ($t_up_olt > 0) {
                    $fclass_olt_up = ' style="color: #82D711;" ';
                    $link_t_up_olt = '<span style="color:#82D711;" class="link_olt">'.number_format($t_up_olt,0).'</span>';
                    }
                if ($t_down_olt > 0)
                    $fclass_olt_down = ' style="color: #FF0000;" ';
                $link_total_olt = '<span style="color:#FFF;" class="link_olt">'.number_format($total_olt,0).'</span>';              
                

                /** CE ========================= */
                $total_ce = $t_up_ce + $t_down_ce;
                $link_t_down_ce = number_format($t_down_ce,0);
                if ( ($t_down_ce > 0)) 
                {
                    $warna_ce = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_ce = '<span style="color:red;" class="link_ce">'.number_format($t_down_ce,0).'</span>';
                    
                }
                elseif ( ($t_up_ce == 0) and ($t_down_ce == 0))
                { 
                    $warna_ce = ' abu';
                    #$trclass = '';
                }
                elseif ( ($t_up_ce > 0) and ($t_down_ce == 0))
                { 
                    $warna_ce = ' hijau';
                    #$trclass = '';
                }
                elseif ( ($t_up_ce > 0))
                {
                    #$trclass = '';
                }
                
                $fclass_ce_up = $fclass_ce_down = ' style="color: #fff;" ';
                $link_t_up_ce = number_format($t_up_ce,0);
                
                if ($t_up_ce > 0) {
                    $fclass_ce_up = ' style="color: #82D711;" ';
                    $link_t_up_ce = '<span style="color:#82D711;" class="link_ce">'.number_format($t_up_ce,0).'</span>';
                    }
                if ($t_down_ce > 0)
                    $fclass_ce_down = ' style="color: #FF0000;" ';
                $link_total_ce = '<span style="color:#FFF;" class="link_ce">'.number_format($total_ce,0).'</span>';              
                /** ================================= */

                /** ONT ========================= */
                $total_ont = $t_up_ont + $t_down_ont;
                $link_t_down_ont = number_format($t_down_ont,0);
                if ( ($t_down_ont > 0)) 
                {
                    $trclass = 'highlight';
                    $link_t_down_ont = '<span style="color:red;" class="link_ont">'.number_format($t_down_ont,0).'</span>';
                    
                }
                
                $fclass_ont_up = $fclass_ont_down = ' style="color: #fff;" ';
                $link_t_up_ont = number_format($t_up_ont,0);
                
                if ($t_up_ont > 0) {
                    $fclass_ont_up = ' style="color: #82D711;" ';
                    $link_t_up_ont = '<span style="color:#82D711;" class="link_ce">'.number_format($t_up_ont,0).'</span>';
                    }
                if ($t_down_ont > 0)
                    $fclass_ont_down = ' style="color: #FF0000;" ';
                $link_total_ont = '<span style="color:#FFF;" class="link_ce">'.number_format($total_ont,0).'</span>';              
                /** ================================= */


                //$link_t_down = '<a href="'.$link_param.'" target="_blank" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-ap" style="color:red;" class="link_ap">'.number_format($t_down,0).'</a>';

                $html .= '<tr class="link_ap '.$trclass.'" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'">';                
                $html .= '<td class="tc">'.$no.'</td>';                
                $html .= '<td class="tl">'.strtoupper($alias).'</td>';                
                
                if (strtoupper($title) == 'TELKOM OFFICE')
                {
                    $html .= '<td class="tr"'.$fclass_ce_up.'>'.$link_t_up_ce.'</td>';                
                    $html .= '<td class="tr"'.$fclass_ce_down.'>'.$link_t_down_ce.'</td>';                
                    $html .= '<td class="tr">'.$link_total_ce.'</td>';                                    
                    $html .= '<td class="tr"'.$fclass_olt_up.'>'.$link_t_up_olt.'</td>';                
                    $html .= '<td class="tr"'.$fclass_olt_down.'>'.$link_t_down_olt.'</td>';                
                    $html .= '<td class="tr">'.$link_total_olt.'</td>';                                    
                }
                else
                {
                    $html .= '<td class="tr"'.$fclass_up.'>'.number_format($t_up,0).'</td>';                
                    $html .= '<td class="tr"'.$fclass_down.'>'.$link_t_down.'</td>';                
                    $html .= '<td class="tr">'.number_format($total,0).'</td>';       
                    if (ucwords($title) == 'Reguler')
                    {
                    $html .= '<td class="tr"'.$fclass_switch_up.'>'.$link_t_up_ont.' | '.$link_t_up_switch_acc.'</td>';                
                    $html .= '<td class="tr"'.$fclass_switch_down.'>'.$link_t_down_ont.' | '.$link_t_down_switch_acc.'</td>';                
                    $html .= '<td class="tr">'.$link_total_ont.' | '.$link_total_switch_acc.'</td>';                
                    } else {
                    $html .= '<td class="tr"'.$fclass_switch_up.'>'.$link_t_up_ont.' | '.$link_t_up_switch_cr.' | '.$link_t_up_switch_acc.'</td>';                
                    $html .= '<td class="tr"'.$fclass_switch_down.'>'.$link_t_down_ont.' | '.$link_t_down_switch_cr.' | '.$link_t_down_switch_acc.'</td>';                
                    $html .= '<td class="tr">'.$link_total_ont.' | '.$link_total_switch_cr.' | '.$link_total_switch_acc.'</td>';                
                    }
                    
                    
                }
                $html .= '</tr>';   
                $no++;
                $tt_up += $t_up; 
                $tt_down += $t_down; 
                $ttotal += $total; 
                $tt_up_switch_cr += $t_up_switch_cr; 
                $tt_down_switch_cr += $t_down_switch_cr; 
                $ttotal_switch_cr += $total_switch_cr; 
                $tt_up_switch_acc += $t_up_switch_acc; 
                $tt_down_switch_acc += $t_down_switch_acc; 
                $ttotal_switch_acc += $total_switch_acc; 
                $tt_up_olt += $t_up_olt; 
                $tt_down_olt += $t_down_olt; 
                $ttotal_olt += $total_olt; 
                $tt_up_ce += $t_up_ce; 
                $tt_down_ce += $t_down_ce; 
                $ttotal_ce += $total_ce; 
                $tt_up_ont += $t_up_ont; 
                $tt_down_ont += $t_down_ont; 
                $ttotal_ont += $total_ont; 
                
            } 
            
                $html .= '<tr class="no-border">';                
                $html .= '<td colspan="2" class="no-border">&nbsp;</td>';                

                if (strtoupper($title) == 'TELKOM OFFICE')
                {
                                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.$tt_up_ce.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.$tt_down_ce.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.$ttotal_ce.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.$tt_up_olt.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.$tt_down_olt.'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.$ttotal_olt.'</td>';                
                }
                else
                {
                                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.number_format($tt_up,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.number_format($tt_down,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.number_format($ttotal,0).'</td>';
                    if (ucwords($title) == 'Reguler')
                    {
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_up_ont,0).' | '.number_format($tt_up_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_down_ont,0).' | '.number_format($tt_down_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.number_format($ttotal_ont,0).' | '.number_format($ttotal_switch_acc,0).'</td>';                
                    } else {
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_up_ont,0).' | '.number_format($tt_up_switch_cr,0).' | '.number_format($tt_up_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_down_ont,0).' | '.number_format($tt_down_switch_cr,0).' | '.number_format($tt_down_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.number_format($ttotal_ont,0).' | '.number_format($ttotal_switch_cr,0).' | '.number_format($ttotal_switch_acc,0).'</td>';                
                    }
                    
                    #$html .= '<td class="tr '.$fclass.' alternate_black">'.number_format($ttotal_ont,0).' | '.number_format($ttotal_switch_cr,0).' | '.number_format($ttotal_switch_acc,0).'</td>';                
                    
                }
                    $html .= '</tr>';
                   
    if ($complete_table)
    {
            $html .= '</table>';
    }   
                $res = array();
                $res['total_sw_up_cr'] = $tt_up_switch_cr;
                $res['total_sw_down_cr'] = $tt_down_switch_cr;
                $res['total_sw_up_acc'] = $tt_up_switch_acc;
                $res['total_sw_down_acc'] = $tt_down_switch_acc;
                $res['total_ap_up'] = @$tt_up_ap;
                $res['total_ap_down'] = @$tt_down_ap;
                $res['total_olt_up'] = @$tt_up_olt;
                $res['total_olt_down'] = @$tt_down_olt;
                $res['total_ont_up'] = @$tt_up_ont;
                $res['total_ont_down'] = @$tt_down_ont;
                $res['html'] = $html;
                return $res;
    
}

function summary_table_sto($title,$conn,$complete_table = true)
{
        $rows_sto = $jenis = array();
        $arr_jenis = array(
        'TERRA ROUTER' => 'T',
        'PROVIDER EDGE' => 'PE',
        'METRO' => 'ME',
        'CUSTOMER EDGE' => 'CE',
        'WIRELESS ACCESS CONTROL' => 'WAC',
        );
        $q_sto = "SELECT c.alias,b.monitor,a.device_type,count(*) jml FROM
x_infra_devices a LEFT OUTER JOIN infra_host b ON (a.hostname = b.hostname)
INNER JOIN x_list_location c ON (a.loc_id = c.loc_id)
GROUP BY c.alias,b.monitor,a.device_type
order by alias, device_type
";
        $res_sto = mysql_query($q_sto,$conn);
        $i = 0;
        while($row = mysql_fetch_assoc($res_sto))
        {          
            $tipe = $arr_jenis[$row['device_type']];  
            $status = $row['monitor'] == 'on' ? 'up' : 'down'; 
            $jenis[$tipe] = $tipe;
            $rows_sto[$row['alias']][$tipe][$status] = $row['jml'];
            $i++;
        }
        #print_out($jenis);
        #print_out($rows_sto);
        $table_sto = "";
        $no = 1;
        foreach($rows_sto as $lokasi => $row)
        {
        $total = 0;
        $t_up = $t_down = 0;    
        $table_sto .= '<tr><td>'.$no.'</td><td>'.$lokasi.'</td>';
            foreach($arr_jenis as $long => $short)
            {
                $up = array_key_exists('up',$rows_sto[$lokasi][$short]) ? $rows_sto[$lokasi][$short]['up'] : 0;
                $down = array_key_exists('down',$rows_sto[$lokasi][$short]) ? $rows_sto[$lokasi][$short]['down'] : 0;
                #print_out($rows_sto[$lokasi][$short]);
                $table_sto .= '<td class="tr"><span style="color: green;">'.$up.'</span> | <span style="color: red;">'.$down.'</span></td>';
                $t_up += $up;
                $t_down += $down;
            }
                $table_sto .= '<td class="tr">'.$t_up." | ".$t_down.'</td>';
        $table_sto .= '</tr>';
        $no++;
        }    
}

function summary_table_reguler($title,$arr_lokasi = array(),$data_switch,$data_ap,$data_ont,$table_prefix,$eventid,$db,$conn, $complete_table = true)
{
    if ($complete_table)
    {
            $html = '<table class="tlist_dashboard fl" width="100%">
            <tr>
                <td colspan="8" class="tc kolomjudul">'.ucwords($title).'</td>
            </tr>                      
            <tr>
                <td class="kolomjudul tc">No.</td>
                <td class="kolomjudul">Location</td>
                <td class="tc"><img title="Access Point UP" src="/assets/images/wifi-up.png" style="height: 16px;" class="img_wifi" /></td>
                <td class="tc"><img title="Access Point Down" src="/assets/images/wifi-down.png" style="height: 16px;" class="img_wifi" /></td>
                <td class="tc">Tot</td>';
            if ($title == 'Reguler')
            {
             $html.='
                <td class="tc"><img title="Switch UP" src="/assets/images/switch-up.png" style="height: 16px;" class="img_wifi" /><br />O | A</td>
                <td class="tc"><img title="Switch Down" src="/assets/images/switch-down.png" style="height: 16px;" class="img_wifi" /><br />O | A</td>';
            }
            else
            {
             $html.='
                <td class="tc"><img title="Switch UP" src="/assets/images/switch-up.png" style="height: 16px;" class="img_wifi" /><br />O | C | A</td>
                <td class="tc"><img title="Switch Down" src="/assets/images/switch-down.png" style="height: 16px;" class="img_wifi" /><br />O | C | A</td>';
            }
            $html.='
                <td class="tc">Tot</td>
            </tr>';
    } else { $html = ''; }
            
            $ttotal = $tt_up = $tt_down = $ttotal_switch_cr = $ttotal_switch_acc = 0;
            $tt_up_switch_cr = $tt_up_switch_acc = $tt_down_switch_cr = $tt_down_switch_acc = 0;
            $ttotal_ce = $ttotal_up_ce = $ttotal_down_ce = 0;
            $ttotal_olt = $ttotal_up_olt = $ttotal_down_olt = 0;
            $ttotal_ont = $ttotal_up_ont = $ttotal_down_ont = 0;
            $no = 1;

            foreach($arr_lokasi as $hotel => $rows) {
                $alias = "";
                $t_down_switch_cr = $t_down_switch_acc = $t_up_switch_cr = $t_up_switch_acc = $t_up = $t_down = 0;
                $t_down_olt = $t_up_olt = 0;
                $t_down_ce = $t_up_ce = 0;
                $t_down_ont = $t_up_ont = 0;
                
                $loc_id = $rows[0]['loc_id'];
                $t_up = array_key_exists($hotel,$data_ap) ? @$data_ap[$hotel]['UP'] : 0;
                $t_down = array_key_exists($hotel,$data_ap) ? @$data_ap[$hotel]['DOWN'] : 0;
                
                $t_up_ont = array_key_exists($hotel,$data_ont) ? @$data_ont[$hotel]['UP'] : 0;
                $t_down_ont = array_key_exists($hotel,$data_ont) ? @$data_ont[$hotel]['DOWN'] : 0;

                foreach($rows as $row)
                {
                    $alias = $row['alias'];
                    $switchs[] = $row['ip_address'];
                    if ($row['TIPE_SW'] == 'COR')
                    {
                        if ($row['current_state'] == '')
                            $t_down_switch_cr++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_switch_cr++;
                        elseif ($row['current_state'] == 1)
                            $t_down_switch_cr++;
                        
                    }
                    elseif ($row['TIPE_SW'] == 'ACC' || $row['TIPE_SW'] == '8AC')
                    {
                        if ($row['current_state'] == '')
                            $t_down_switch_acc++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_switch_acc++;
                        elseif ($row['current_state'] == 1)
                            $t_down_switch_acc++;
                        
                    }
                    elseif ($row['TIPE_SW'] == 'OLT')
                    {
                        if ($row['current_state'] == '')
                            $t_down_olt++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_olt++;
                        elseif ($row['current_state'] == 1)
                            $t_down_olt++;
                        
                    }
                    elseif ($row['TIPE_SW'] == 'CE-')
                    {
                        if ($row['current_state'] == '')
                            $t_down_ce++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_ce++;
                        elseif ($row['current_state'] == 1)
                            $t_down_ce++;
                        
                    }
                }
                
                $total = $t_up + $t_down;
                $link_t_down = number_format($t_down,0);
                $link_param = $BASEURL.'index.php?event_id='.$eventid.'&loc_name='.$hotel.'&loc_id='.$loc_id;
                if ( ($t_down > 0)) 
                {
                    $warna = ' merah';
                    $fclass = ' fmerah'; 
                    $trclass = 'highlight';
                    $link_t_down = '<span style="color:red;">'.number_format($t_down,0).'</span>';
                    
                }
                elseif ( ($t_up == 0) and ($t_down == 0))
                { 
                    $warna = ' abu';
                    $fclass = ' fputih'; 
                    $trclass = '';
                }
                elseif ( ($t_up > 0) and ($t_down == 0))
                { 
                    $warna = ' hijau';
                    $fclass = ' fhijau'; 
                    $trclass = '';
                }
                $fclass_up = $fclass_down = ' style="color: #fff;" ';
                
                if ($t_up > 0)
                    $fclass_up = ' style="color: #82D711;" ';
                if ($t_down > 0)
                    $fclass_down = ' style="color: #FF0000;" ';
                

                $total_switch_cr = $t_up_switch_cr + $t_down_switch_cr;
                $total_switch_acc = $t_up_switch_acc + $t_down_switch_acc;
                $link_t_down_switch_cr = number_format($t_down_switch_cr,0);
                $link_t_down_switch_acc = number_format($t_down_switch_acc,0);
                
                /** STYLING SWITCH COUNT STATISTIC */
                /** SWITCH CORE ========================= */
                if ( ($t_down_switch_cr > 0)) 
                {
                    $warna_switch_cr = ' merah';
                    $trclass = 'highlight';
                    #$link_t_down_switch_cr = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-1-switch" style="color:red;" class="link_switch">'.number_format($t_down_switch_cr,0).'</a>';
                    $link_t_down_switch_cr = '<span style="color:red;">'.number_format($t_down_switch_cr,0).'</span>';
                    
                }
                elseif ( ($t_up_switch_cr == 0) and ($t_down_switch_cr == 0))
                { 
                    $warna_switch_cr = ' abu';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_cr > 0) and ($t_down_switch_cr == 0))
                { 
                    $warna_switch_cr = ' hijau';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_cr > 0))
                {
                    //$trclass = '';
                }
                
                $fclass_switch_up_cr = $fclass_switch_down_cr = ' style="color: #fff;" ';
                $link_t_up_switch_cr = number_format($t_up_switch_cr,0);
                
                if ($t_up_switch_cr > 0) {
                    $fclass_switch_up_cr = ' style="color: #82D711;" ';
                    $link_t_up_switch_cr = '<span style="color:#82D711;" class="link_switch">'.number_format($t_up_switch_cr,0).'</span>';
                    }
                if ($t_down_switch_cr > 0)
                    $fclass_switch_down_cr = ' style="color: #FF0000;" ';
                
                $link_total_switch_cr = '<span style="color:#FFF;" class="link_switch">'.number_format($total_switch_cr,0).'</span>';              
            

                /** SWITCH ACCESS ========================= */
                if ( ($t_down_switch_acc > 0)) 
                {
                    $warna_switch_acc = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_switch_acc = '<span style="color:red;" class="link_switch">'.number_format($t_down_switch_acc,0).'</span>';
                    
                }
                elseif ( ($t_up_switch_acc == 0) and ($t_down_switch_acc == 0))
                { 
                    $warna_switch_acc = ' abu';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_acc > 0) and ($t_down_switch_acc == 0))
                { 
                    $warna_switch_acc = ' hijau';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_acc > 0))
                {
                    //$trclass = '';
                }
                
                $fclass_switch_up_acc = $fclass_switch_down_acc = ' style="color: #fff;" ';
                $link_t_up_switch_acc = number_format($t_up_switch_acc,0);
                
                if ($t_up_switch_acc > 0) {
                    $fclass_switch_up_acc = ' style="color: #82D711;" ';
                    $link_t_up_switch_acc = '<span style="color:#82D711;" class="link_switch">'.number_format($t_up_switch_acc,0).'</span>';
                    }
                if ($t_down_switch_acc > 0)
                    $fclass_switch_down_acc = ' style="color: #FF0000;" ';
                
                $link_total_switch_acc = '<span style="color:#FFF;" class="link_switch">'.number_format($total_switch_acc,0).'</span>';              

                
                /** OLT ========================= */
                $total_olt = $t_up_olt + $t_down_olt;
                $link_t_down_olt = number_format($t_down_olt,0);
                if ( ($t_down_olt > 0)) 
                {
                    $warna_olt = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_olt = '<span style="color:red;" class="link_olt">'.number_format($t_down_olt,0).'</span>';
                    
                }
                elseif ( ($t_up_olt == 0) and ($t_down_olt == 0))
                { 
                    $warna_olt = ' abu';
                    #$trclass = '';
                }
                elseif ( ($t_up_olt > 0) and ($t_down_olt == 0))
                { 
                    $warna_olt = ' hijau';
                    #$trclass = '';
                }
                elseif ( ($t_up_olt > 0))
                {
                    #$trclass = '';
                }
                
                $fclass_olt_up = $fclass_olt_down = ' style="color: #fff;" ';
                $link_t_up_olt = number_format($t_up_olt,0);
                
                if ($t_up_olt > 0) {
                    $fclass_olt_up = ' style="color: #82D711;" ';
                    $link_t_up_olt = '<span style="color:#82D711;" class="link_olt">'.number_format($t_up_olt,0).'</span>';
                    }
                if ($t_down_olt > 0)
                    $fclass_olt_down = ' style="color: #FF0000;" ';
                $link_total_olt = '<span style="color:#FFF;" class="link_olt">'.number_format($total_olt,0).'</span>';              
                

                /** CE ========================= */
                $total_ce = $t_up_ce + $t_down_ce;
                $link_t_down_ce = number_format($t_down_ce,0);
                if ( ($t_down_ce > 0)) 
                {
                    $warna_ce = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_ce = '<span style="color:red;" class="link_ce">'.number_format($t_down_ce,0).'</span>';
                    
                }
                elseif ( ($t_up_ce == 0) and ($t_down_ce == 0))
                { 
                    $warna_ce = ' abu';
                    #$trclass = '';
                }
                elseif ( ($t_up_ce > 0) and ($t_down_ce == 0))
                { 
                    $warna_ce = ' hijau';
                    #$trclass = '';
                }
                elseif ( ($t_up_ce > 0))
                {
                    #$trclass = '';
                }
                
                $fclass_ce_up = $fclass_ce_down = ' style="color: #fff;" ';
                $link_t_up_ce = number_format($t_up_ce,0);
                
                if ($t_up_ce > 0) {
                    $fclass_ce_up = ' style="color: #82D711;" ';
                    $link_t_up_ce = '<span style="color:#82D711;" class="link_ce">'.number_format($t_up_ce,0).'</span>';
                    }
                if ($t_down_ce > 0)
                    $fclass_ce_down = ' style="color: #FF0000;" ';
                $link_total_ce = '<span style="color:#FFF;" class="link_ce">'.number_format($total_ce,0).'</span>';              
                /** ================================= */

                /** ONT ========================= */
                $total_ont = $t_up_ont + $t_down_ont;
                $link_t_down_ont = number_format($t_down_ont,0);
                if ( ($t_down_ont > 0)) 
                {
                    $trclass = 'highlight';
                    $link_t_down_ont = '<span style="color:red;" class="link_ont">'.number_format($t_down_ont,0).'</span>';
                    
                }
                
                $fclass_ont_up = $fclass_ont_down = ' style="color: #fff;" ';
                $link_t_up_ont = number_format($t_up_ont,0);
                
                if ($t_up_ont > 0) {
                    $fclass_ont_up = ' style="color: #82D711;" ';
                    $link_t_up_ont = '<span style="color:#82D711;" class="link_ce">'.number_format($t_up_ont,0).'</span>';
                    }
                if ($t_down_ont > 0)
                    $fclass_ont_down = ' style="color: #FF0000;" ';
                $link_total_ont = '<span style="color:#FFF;" class="link_ce">'.number_format($total_ont,0).'</span>';              
                /** ================================= */


                //$link_t_down = '<a href="'.$link_param.'" target="_blank" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-ap" style="color:red;" class="link_ap">'.number_format($t_down,0).'</a>';

                $html .= '<tr class="link_ap '.$trclass.'" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'">';                
                $html .= '<td class="tc">'.$no.'</td>';                
                $html .= '<td class="tl">'.strtoupper($alias).'</td>';                
                
                if (strtoupper($title) == 'TELKOM OFFICE')
                {
                    $html .= '<td class="tr"'.$fclass_ce_up.'>'.$link_t_up_ce.'</td>';                
                    $html .= '<td class="tr"'.$fclass_ce_down.'>'.$link_t_down_ce.'</td>';                
                    $html .= '<td class="tr">'.$link_total_ce.'</td>';                                    
                    $html .= '<td class="tr"'.$fclass_olt_up.'>'.$link_t_up_olt.'</td>';                
                    $html .= '<td class="tr"'.$fclass_olt_down.'>'.$link_t_down_olt.'</td>';                
                    $html .= '<td class="tr">'.$link_total_olt.'</td>';                                    
                }
                else
                {
                    $html .= '<td class="tr"'.$fclass_up.'>'.number_format($t_up,0).'</td>';                
                    $html .= '<td class="tr"'.$fclass_down.'>'.$link_t_down.'</td>';                
                    $html .= '<td class="tr">'.number_format($total,0).'</td>';       
                    if (ucwords($title) == 'Reguler')
                    {
                    $html .= '<td class="tr"'.$fclass_switch_up.'>'.$link_t_up_ont.' | '.$link_t_up_switch_acc.'</td>';                
                    $html .= '<td class="tr"'.$fclass_switch_down.'>'.$link_t_down_ont.' | '.$link_t_down_switch_acc.'</td>';                
                    $html .= '<td class="tr">'.$link_total_ont.' | '.$link_total_switch_acc.'</td>';                
                    } else {
                    $html .= '<td class="tr"'.$fclass_switch_up.'>'.$link_t_up_ont.' | '.$link_t_up_switch_cr.' | '.$link_t_up_switch_acc.'</td>';                
                    $html .= '<td class="tr"'.$fclass_switch_down.'>'.$link_t_down_ont.' | '.$link_t_down_switch_cr.' | '.$link_t_down_switch_acc.'</td>';                
                    $html .= '<td class="tr">'.$link_total_ont.' | '.$link_total_switch_cr.' | '.$link_total_switch_acc.'</td>';                
                    }
                    
                    
                }
                $html .= '</tr>';   
                $no++;
                $tt_up += $t_up; 
                $tt_down += $t_down; 
                $ttotal += $total; 
                $tt_up_switch_cr += $t_up_switch_cr; 
                $tt_down_switch_cr += $t_down_switch_cr; 
                $ttotal_switch_cr += $total_switch_cr; 
                $tt_up_switch_acc += $t_up_switch_acc; 
                $tt_down_switch_acc += $t_down_switch_acc; 
                $ttotal_switch_acc += $total_switch_acc; 
                $tt_up_olt += $t_up_olt; 
                $tt_down_olt += $t_down_olt; 
                $ttotal_olt += $total_olt; 
                $tt_up_ce += $t_up_ce; 
                $tt_down_ce += $t_down_ce; 
                $ttotal_ce += $total_ce; 
                $tt_up_ont += $t_up_ont; 
                $tt_down_ont += $t_down_ont; 
                $ttotal_ont += $total_ont; 
                
            } 
            
                $html .= '<tr class="no-border">';                
                $html .= '<td colspan="2" class="no-border">&nbsp;</td>';                

                if (strtoupper($title) == 'TELKOM OFFICE')
                {
                                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.$tt_up_ce.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.$tt_down_ce.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.$ttotal_ce.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.$tt_up_olt.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.$tt_down_olt.'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.$ttotal_olt.'</td>';                
                }
                else
                {
                                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.number_format($tt_up,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.number_format($tt_down,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.number_format($ttotal,0).'</td>';
                    if (ucwords($title) == 'Reguler')
                    {
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_up_ont,0).' | '.number_format($tt_up_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_down_ont,0).' | '.number_format($tt_down_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.number_format($ttotal_ont,0).' | '.number_format($ttotal_switch_acc,0).'</td>';                
                    } else {
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_up_ont,0).' | '.number_format($tt_up_switch_cr,0).' | '.number_format($tt_up_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_down_ont,0).' | '.number_format($tt_down_switch_cr,0).' | '.number_format($tt_down_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.number_format($ttotal_ont,0).' | '.number_format($ttotal_switch_cr,0).' | '.number_format($ttotal_switch_acc,0).'</td>';                
                    }
                    
                    #$html .= '<td class="tr '.$fclass.' alternate_black">'.number_format($ttotal_ont,0).' | '.number_format($ttotal_switch_cr,0).' | '.number_format($ttotal_switch_acc,0).'</td>';                
                    
                }
                    $html .= '</tr>';
                   
    if ($complete_table)
    {
            $html .= '</table>';
    }   
                $res = array();
                $res['total_sw_up_cr'] = $tt_up_switch_cr;
                $res['total_sw_down_cr'] = $tt_down_switch_cr;
                $res['total_sw_up_acc'] = $tt_up_switch_acc;
                $res['total_sw_down_acc'] = $tt_down_switch_acc;
                $res['total_ap_up'] = @$tt_up_ap;
                $res['total_ap_down'] = @$tt_down_ap;
                $res['total_olt_up'] = @$tt_up_olt;
                $res['total_olt_down'] = @$tt_down_olt;
                $res['total_ont_up'] = @$tt_up_ont;
                $res['total_ont_down'] = @$tt_down_ont;
                $res['html'] = $html;
                return $res;
    
}


function summary_table_witel($title = '',$data,$eventid,$loc_id = -1)
{

            $sum_total_nodeb = $sum_total_me = 0;
            $total_nodeb_up = $total_nodeb_down = $total_nodeb_unk = $total_me_up = $total_me_down = $total_me_unk = 0;            
            $no = 1;

            #foreach($data as $rnc => $devtype_rows)
            #{
                $html_sisip = ''; 
                $trclass = '';
                
                $arr = explode('*',$title);
                $trid = $eventid.'-'.$arr[0];
                $rnc = $arr[1];
                
                #echo '<pre>'.print_r($arr,1).'</pre>'; die;
                #echo 'alert("'.$rnc.'");';
                
                $html = '<table class="tlist_dashboard" width="99.5%"><tr><td class="tc" colspan="4">'.$rnc.'</td></tr><tr><td class="tc"><img title="Node B TSel" src="/assets/dws/images/bts-plain.png" style="height: 24px;" /><br />U | D | Un | Tot.</td><td class="tc"><img title="ME" src="/assets/dws/images/me-plain.png"  style="height: 20px;"  /><br />U | D | Un | Tot.</td><td class="tc"><img title="Lambda" src="/assets/dws/images/lambda-plain.png"  style="height: 28px;"  /><br />U | D | Un | Tot.</td><td class="tc"><img title="Traffic" src="/assets/dws/images/traffic-plain.png" style="height: 24px;" /><br />U | D | Un | Tot.</td></tr>';
            
                foreach($data as $tipe => $status)
                {
                    $total = $status['UP'] + $status['DOWN'] + $status['UNKNOWN']; 
                    $t_down = $status['DOWN'];
                    $t_up =  $status['UP']; 
                    $t_unk =  $status['UNKNOWN'];
                    
                    if ($tipe=='NODE B')
                    {
                        $sum_total_nodeb += $total; 
                        $total_nodeb_up += $t_up; 
                        $total_nodeb_down += $t_down; 
                        $total_nodeb_unk += $t_unk;                         
                    }
                    if ($tipe=='METRO')
                    {
                        $sum_total_me += $total; 
                        $total_me_up += $t_up; 
                        $total_me_down += $t_down; 
                        $total_me_unk += $t_unk;                         
                    }
                     
                    $fclass_up = $fclass_down = $fclass_unk = ' fputih ';
                    $trclass = '';
                    if ($t_up > 0)
                        $fclass_up = ' fhijau ';
                    if ($t_down > 0)
                        $fclass_down = ' fmerah ';
                    if ($t_unk > 0)
                        $fclass_unk = ' fabu ';
                    
                    if (($t_down > 0) or ($t_unk > 0))
                    $trclass = ' highlight ';
                    $html_sisip .= '<td class="tr"><span class="'.$fclass_up.'">'.number_format($status['UP'],0).'</span> | <span class="'.$fclass_down.'">'.number_format($status['DOWN'],0).'</span> | <span class="'.$fclass_unk.'">'.number_format($status['UNKNOWN'],0).'</span> | <span class="fputih">'.number_format($total,0).'</span></td>';
                       
                }
                
                $html .= '<tr class="link_ap">'.$html_sisip.'<td class="tr">0 | 0 | 0 | 0</td><td class="tr">0 | 0 | 0 | 0</td></tr>';   
                $no++;
            #}
            
            $html .= '</table>';   
            
            $res = array();
            $res['html'] = $html;
            $res['total_nodeb_up'] = $total_nodeb_up;
            $res['total_nodeb_down'] = $total_nodeb_down;
            $res['total_nodeb_unk'] = $total_nodeb_unk;
            $res['total_me_up'] = $total_me_up;
            $res['total_me_down'] = $total_me_down;
            $res['total_me_unk'] = $total_me_unk;
            return $res;

                
    
}

//add by him
function summary_table_witel2($title = '',$data,$eventid,$loc_id = -1){
$sum_total_nodeb = $sum_total_me = 0;
$total_nodeb_up = $total_nodeb_down = $total_nodeb_unk = $total_me_up = $total_me_down = $total_me_unk = 0;            
$no = 1;
$html_sisip = ''; 
$trclass = '';
$arr = explode('*',$title);
$trid = $eventid.'-'.$arr[0];
$rnc = $arr[1];

$html = '<div class="table-responsive">
		<table class="table table-bordered summary" >
		<thead>
			<tr><th colspan="4">'.$rnc.'</th></tr>
			<tr>
				<th><img src="/images/ICON/NODE-B/node_bwhite_fgrey.png" style="width:20px"/><br>U | D | Un | Tot.</th>
				<th><img src="/images/ICON/METRO/metro_bwhite_fgrey.png" style="width:20px"/><br>U | D | Un | Tot.</th>
				<th><img src="/images/ICON/LAMBDA/lambda_bwhite_fgrey.png" style="width:20px"/><br>U | D | Un | Tot.</th>
				<th><img src="/images/ICON/TRAFFIC/traffic_bwhite_fgrey.png" style="width:20px"/><br>U | D | Un | Tot.</th>
			</tr>
		</thead>
		<tbody>';

if(!isset($data) || !is_array($data)){
	$data = array();
}

foreach($data as $tipe => $status){
$total = $status['UP'] + $status['DOWN'] + $status['UNKNOWN']; 
$t_down = $status['DOWN'];
$t_up =  $status['UP']; 
$t_unk =  $status['UNKNOWN'];

if ($tipe=='NODE B'){
	$sum_total_nodeb += $total; 
	$total_nodeb_up += $t_up; 
	$total_nodeb_down += $t_down; 
	$total_nodeb_unk += $t_unk;                         
}

if ($tipe=='METRO'){
	$sum_total_me += $total; 
	$total_me_up += $t_up; 
	$total_me_down += $t_down; 
	$total_me_unk += $t_unk;                         
}

$fclass_up = $fclass_down = $fclass_unk = ' fputih ';
$trclass = '';

if ($t_up > 0)$fclass_up = ' fhijau ';
if ($t_down > 0)$fclass_down = ' fmerah ';
if ($t_unk > 0)$fclass_unk = ' fabu ';

if (($t_down > 0) or ($t_unk > 0))$trclass = ' highlight ';
$html_sisip .= '<td class="tr"><span class="'.$fclass_up.'">'.number_format($status['UP'],0).'</span> | <span class="'.$fclass_down.'">'.number_format($status['DOWN'],0).'</span> | <span class="'.$fclass_unk.'">'.number_format($status['UNKNOWN'],0).'</span> | <span class="fputih">'.number_format($total,0).'</span></td>';
}

$html .= '<tr class="link_aps">'.$html_sisip.'<td class="tr">0 | 0 | 0 | 0</td><td class="tr">0 | 0 | 0 | 0</td></tr>';   
$no++;
$html .= '	</tbody>
			</table>
		</div>';

$res = array();
$res['html'] = $html;
$res['total_nodeb_up'] = $total_nodeb_up;
$res['total_nodeb_down'] = $total_nodeb_down;
$res['total_nodeb_unk'] = $total_nodeb_unk;
$res['total_me_up'] = $total_me_up;
$res['total_me_down'] = $total_me_down;
$res['total_me_unk'] = $total_me_unk;
return $res;
}

function summary_table_regional($title = '',$data,$eventid,$loc_id = -1)
{
            $html = '<table class="tlist_dashboard" width="99.5%">
            <tr>
                <td class="kolomjudul tc" style="background-color:#333;">No.</td>
                <td class="kolomjudul" style="background-color:#333;">REGIONAL</td>
                <td class="tc" style="background-color:#333;"><img title="Node B TSel" src="/assets/dws/images/bts-plain.png" style="height: 24px;" /><br />U | D | Un | Tot.</td>
                <td class="tc" style="background-color:#333;"><img title="ME" src="/assets/dws/images/me-plain.png"  style="height: 20px;"  /><br />U | D | Un | Tot.</td>
                <td class="tc" style="background-color:#333;"><img title="Lambda" src="/assets/dws/images/lambda-plain.png"  style="height: 28px;"  /><br />U | D | Un | Tot.</td>
                <td class="tc" style="background-color:#333;"><img title="Traffic" src="/assets/dws/images/traffic-plain.png" style="height: 24px;" /><br />U | D | Un | Tot.</td>
            </tr>';

            $sum_total_nodeb = $sum_total_me = 0;
            $total_nodeb_up = $total_nodeb_down = $total_nodeb_unk = $total_me_up = $total_me_down = $total_me_unk = 0;            
            $no = 1;

            foreach($data as $rnc => $devtype_rows)
            {
                #echo '<pre>'.print_r($rnc,1).'</pre>'; die;
                $html_sisip = ''; 
                $trclass = '';
                
                $arr = explode('*',$rnc);
                $trid = $eventid.'-'.$arr[0];
                $rnc = $arr[1];

                foreach($devtype_rows as $tipe => $status)
                {
                    $total = $status['UP'] + $status['DOWN'] + $status['UNKNOWN']; 
                    $t_down = $status['DOWN'];
                    $t_up =  $status['UP']; 
                    $t_unk =  $status['UNKNOWN'];
                    
                    if ($tipe=='NODE B')
                    {
                        $sum_total_nodeb += $total; 
                        $total_nodeb_up += $t_up; 
                        $total_nodeb_down += $t_down; 
                        $total_nodeb_unk += $t_unk;                         
                    }
                    if ($tipe=='METRO')
                    {
                        $sum_total_me += $total; 
                        $total_me_up += $t_up; 
                        $total_me_down += $t_down; 
                        $total_me_unk += $t_unk;                         
                    }
                     
                    $fclass_up = $fclass_down = $fclass_unk = ' fputih ';
                    $trclass = '';
                    if ($t_up > 0)
                        $fclass_up = ' fhijau ';
                    if ($t_down > 0)
                        $fclass_down = ' fmerah ';
                    if ($t_unk > 0)
                        $fclass_unk = ' fabu ';
                    
                    if (($t_down > 0) or ($t_unk > 0))
                    $trclass = ' highlight ';
                    $html_sisip .= '<td class="tr"><span class="'.$fclass_up.'">'.number_format($status['UP'],0).'</span> | <span class="'.$fclass_down.'">'.number_format($status['DOWN'],0).'</span> | <span class="'.$fclass_unk.'">'.number_format($status['UNKNOWN'],0).'</span> | <span class="fputih">'.number_format($total,0).'</span></td>';
                       
                }
                $html .= '<tr id="'.$trid.'" class="link_ap">';   
                $html .= '<td>'.$no.'</td>';   
                $html .= '<td class="tl">'.$rnc.'</td>';  
                $html .= $html_sisip;
                $html .= '<td class="tr">0 | 0 | 0 | 0</td>';   
                $html .= '<td class="tr">0 | 0 | 0 | 0</td>';  
                $html .= '</tr>';   
                $no++;
            }
            
            $html .= '<tr class="no-border">';   
            $html .= '<td colspan="2" class="no-border">&nbsp;</td>';   
            $html .= '<td class="bgoren2 tr" style="background-color:#333; font-weight: bold;"><span class="fhijau">'.number_format($total_nodeb_up,0).'</span> | <span class="fmerah">'.number_format($total_nodeb_down,0).'</span> | <span class="fabu">'.number_format($total_nodeb_unk,0).'</span> | '.number_format($sum_total_nodeb,0).'</td>';  
            $html .= '<td class="bgoren2 tr" style="background-color:#333; font-weight: bold;"><span class="fhijau">'.number_format($total_me_up,0).'</span> | <span class="fmerah">'.number_format($total_me_down,0).'</span> | <span class="fabu">'.number_format($total_me_unk,0).'</span> | '.number_format($sum_total_me,0).'</td>';  
            #$html .= '<td class="bgoren2 tr" style="color: #000;background-color:#A9A9A9;">'.$total_nodeb_up.' | '.$total_nodeb_down.' | '.$total_nodeb_unk.' | '.$sum_total_nodeb.'</td>';  
            #$html .= '<td class="bgoren2 tr" style="color: #000;background-color:#A9A9A9;">'.$total_me_up.' | '.$total_me_down.' | '.$total_me_unk.' | '.$sum_total_me.'</td>';  
            $html .= '<td class="bgoren2 tr" style="color: #FFF;background-color:#333; font-weight: bold;">0 | 0 | 0 | 0</td>';  
            $html .= '<td class="bgoren2 tr" style="color: #FFF;background-color:#333; font-weight: bold;">0 | 0 | 0 | 0</td>';  
            $html .= '</tr>';   
            $html .= '</table>';   
            #echo $html;
            $res = array();
            $res['html'] = $html;
            $res['total_nodeb_up'] = $total_nodeb_up;
            $res['total_nodeb_down'] = $total_nodeb_down;
            $res['total_nodeb_unk'] = $total_nodeb_unk;
            $res['total_me_up'] = $total_me_up;
            $res['total_me_down'] = $total_me_down;
            $res['total_me_unk'] = $total_me_unk;
            return $res;

                
    
}

function summary_table($title,$data,$eventid,$loc_id)
{
            $html = '<table class="tlist_dashboard" width="99.5%">
            <tr>
                <td colspan="6" class="tc kolomjudul" style="background-color:#EF3125;">'.ucwords($title).'</td>
            </tr>                      
            <tr>
                <td class="kolomjudul tc" style="color: #FFF;background-color:#333; font-weight: bold;">No.</td>
                <td class="kolomjudul" style="color: #FFF;background-color:#333; font-weight: bold;">RNC</td>
                <td class="tc" style="color: #FFF;background-color:#333;"><img title="Node B TSel" src="/assets/dws/images/bts-plain.png" style="height: 24px;" /><br />U | D | Un | Tot.</td>
                <td class="tc" style="color: #FFF;background-color:#333;"><img title="ME" src="/assets/dws/images/me-plain.png"  style="height: 20px;"  /><br />U | D | Un | Tot.</td>
                <td class="tc" style="color: #FFF;background-color:#333;"><img title="Lambda" src="/assets/dws/images/lambda-plain.png"  style="height: 28px;"  /><br />U | D | Un | Tot.</td>
                <td class="tc" style="color: #FFF;background-color:#333;"><img title="Traffic" src="/assets/dws/images/traffic-plain.png" style="height: 24px;" /><br />U | D | Un | Tot.</td>
            </tr>';

            $sum_total_nodeb = $sum_total_me = 0;
            $total_nodeb_up = $total_nodeb_down = $total_nodeb_unk = $total_me_up = $total_me_down = $total_me_unk = 0;            
            $no = 1;

            foreach($data as $rnc => $devtype_rows)
            {
                #echo '<pre>'.print_r($rnc,1).'</pre>'; die;
                $html_sisip = ''; 
                $trclass = '';
                $arr = explode('*',$rnc);
                $trid = $eventid.'-'.$loc_id.'-'.$arr[0];
                $rnc = $arr[1];

                foreach($devtype_rows as $tipe => $status)
                {
                    $total = $status['UP'] + $status['DOWN'] + $status['UNKNOWN']; 
                    $t_down = $status['DOWN'];
                    $t_up =  $status['UP']; 
                    $t_unk =  $status['UNKNOWN'];
                    
                    if ($tipe=='NODE B')
                    {
                        $sum_total_nodeb += $total; 
                        $total_nodeb_up += $t_up; 
                        $total_nodeb_down += $t_down; 
                        $total_nodeb_unk += $t_unk;                         
                    }
                    if ($tipe=='METRO')
                    {
                        $sum_total_me += $total; 
                        $total_me_up += $t_up; 
                        $total_me_down += $t_down; 
                        $total_me_unk += $t_unk;                         
                    }
                     
                    $fclass_up = $fclass_down = $fclass_unk = ' fputih ';
                    $trclass = '';
                    if ($t_up > 0)
                        $fclass_up = ' fhijau ';
                    if ($t_down > 0)
                        $fclass_down = ' fmerah ';
                    if ($t_unk > 0)
                        $fclass_unk = ' fabu ';
                    
                    if (($t_down > 0) or ($t_unk > 0))
                    $trclass = ' highlight ';
                    $html_sisip .= '<td class="tr"><span class="'.$fclass_up.'">'.$status['UP'].'</span> | <span class="'.$fclass_down.'">'.$status['DOWN'].'</span> | <span class="'.$fclass_unk.'">'.$status['UNKNOWN'].'</span> | <span class="fputih">'.$total.'</span></td>';
                       
                }
                $html .= '<tr id="'.$trid.'" class="link_ap'.$trclass.'">';   
                $html .= '<td>'.$no.'</td>';   
                $html .= '<td>'.$rnc.'</td>';  
                $html .= $html_sisip;
                $html .= '<td class="tr">0 | 0 | 0 | 0</td>';   
                $html .= '<td class="tr">0 | 0 | 0 | 0</td>';  
                $html .= '</tr>';   
                $no++;
            }
            
            $html .= '<tr class="no-border">';   
            $html .= '<td colspan="2" class="no-border">&nbsp;</td>';   
            $html .= '<td class="bgoren2 tr" style="background-color:#333; font-weight: bold;"><span class="fhijau">'.$total_nodeb_up.'</span> | <span class="fmerah">'.$total_nodeb_down.'</span> | <span class="fabu">'.$total_nodeb_unk.'</span> | <span class="fputih">'.$sum_total_nodeb.'</span></td>';  
            $html .= '<td class="bgoren2 tr" style="background-color:#333; font-weight: bold;"><span class="fhijau">'.$total_me_up.'</span> | <span class="fmerah">'.$total_me_down.'</span> | <span class="fabu">'.$total_me_unk.'</span> | <span class="fputih">'.$sum_total_me.'</span></td>';  
            $html .= '<td class="bgoren2 tr" style="background-color:#333; font-weight: bold;">0 | 0 | 0 | 0</td>';  
            $html .= '<td class="bgoren2 tr" style="background-color:#333; font-weight: bold;">0 | 0 | 0 | 0</td>';  
            $html .= '</tr>';   
            $html .= '</table>';   
            #echo $html;
            $res = array();
            $res['html'] = $html;
            $res['total_nodeb_up'] = $total_nodeb_up;
            $res['total_nodeb_down'] = $total_nodeb_down;
            $res['total_nodeb_unk'] = $total_nodeb_unk;
            $res['total_me_up'] = $total_me_up;
            $res['total_me_down'] = $total_me_down;
            $res['total_me_unk'] = $total_me_unk;
            return $res;
            /*
            foreach($data_switch as $hotel => $lokasi) {
                $alias = "";
                $t_down_switch_cr = $t_down_switch_acc = $t_up_switch_cr = $t_up_switch_acc = $t_up = $t_down = 0;
                $t_down_olt = $t_up_olt = 0;
                $t_down_ce = $t_up_ce = 0;
                $t_down_ont = $t_up_ont = 0;
                
                $loc_id = $lokasi[0]['loc_id'];
                $t_up = array_key_exists($hotel,$data_ap) ? @$data_ap[$hotel]['UP'] : 0;
                $t_down = array_key_exists($hotel,$data_ap) ? @$data_ap[$hotel]['DOWN'] : 0;
                
                $t_up_ont = array_key_exists($hotel,$data_ont) ? @$data_ont[$hotel]['UP'] : 0;
                $t_down_ont = array_key_exists($hotel,$data_ont) ? @$data_ont[$hotel]['DOWN'] : 0;

                $total = $t_up + $t_down;
                $link_t_down = number_format($t_down,0);
                $link_param = $BASEURL.'index.php?event_id='.$eventid.'&loc_name='.$hotel.'&loc_id='.$loc_id;
                if ( ($t_down > 0)) 
                {
                    $warna = ' merah';
                    $fclass = ' fmerah'; 
                    $trclass = 'highlight';
                    $link_t_down = '<span style="color:red;">'.number_format($t_down,0).'</span>';
                    
                }
                elseif ( ($t_up == 0) and ($t_down == 0))
                { 
                    $warna = ' abu';
                    $fclass = ' fputih'; 
                    $trclass = '';
                }
                elseif ( ($t_up > 0) and ($t_down == 0))
                { 
                    $warna = ' hijau';
                    $fclass = ' fhijau'; 
                    $trclass = '';
                }
                $fclass_up = $fclass_down = ' style="color: #fff;" ';
                
                if ($t_up > 0)
                    $fclass_up = ' style="color: #82D711;" ';
                if ($t_down > 0)
                    $fclass_down = ' style="color: #FF0000;" ';
                

                $total_switch_cr = $t_up_switch_cr + $t_down_switch_cr;
                $total_switch_acc = $t_up_switch_acc + $t_down_switch_acc;
                $link_t_down_switch_cr = number_format($t_down_switch_cr,0);
                $link_t_down_switch_acc = number_format($t_down_switch_acc,0);
                
                if ( ($t_down_switch_cr > 0)) 
                {
                    $warna_switch_cr = ' merah';
                    $trclass = 'highlight';
                    #$link_t_down_switch_cr = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-1-switch" style="color:red;" class="link_switch">'.number_format($t_down_switch_cr,0).'</a>';
                    $link_t_down_switch_cr = '<span style="color:red;">'.number_format($t_down_switch_cr,0).'</span>';
                    
                }
                elseif ( ($t_up_switch_cr == 0) and ($t_down_switch_cr == 0))
                { 
                    $warna_switch_cr = ' abu';
                }
                elseif ( ($t_up_switch_cr > 0) and ($t_down_switch_cr == 0))
                { 
                    $warna_switch_cr = ' hijau';
                }
                elseif ( ($t_up_switch_cr > 0))
                {
                }
                
                $fclass_switch_up_cr = $fclass_switch_down_cr = ' style="color: #fff;" ';
                $link_t_up_switch_cr = number_format($t_up_switch_cr,0);
                
                if ($t_up_switch_cr > 0) {
                    $fclass_switch_up_cr = ' style="color: #82D711;" ';
                    $link_t_up_switch_cr = '<span style="color:#82D711;" class="link_switch">'.number_format($t_up_switch_cr,0).'</span>';
                    }
                if ($t_down_switch_cr > 0)
                    $fclass_switch_down_cr = ' style="color: #FF0000;" ';
                
                $link_total_switch_cr = '<span style="color:#FFF;" class="link_switch">'.number_format($total_switch_cr,0).'</span>';              
            

                if ( ($t_down_switch_acc > 0)) 
                {
                    $warna_switch_acc = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_switch_acc = '<span style="color:red;" class="link_switch">'.number_format($t_down_switch_acc,0).'</span>';
                    
                }
                elseif ( ($t_up_switch_acc == 0) and ($t_down_switch_acc == 0))
                { 
                    $warna_switch_acc = ' abu';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_acc > 0) and ($t_down_switch_acc == 0))
                { 
                    $warna_switch_acc = ' hijau';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_acc > 0))
                {
                    //$trclass = '';
                }
                
                $fclass_switch_up_acc = $fclass_switch_down_acc = ' style="color: #fff;" ';
                $link_t_up_switch_acc = number_format($t_up_switch_acc,0);
                
                if ($t_up_switch_acc > 0) {
                    $fclass_switch_up_acc = ' style="color: #82D711;" ';
                    $link_t_up_switch_acc = '<span style="color:#82D711;" class="link_switch">'.number_format($t_up_switch_acc,0).'</span>';
                    }
                if ($t_down_switch_acc > 0)
                    $fclass_switch_down_acc = ' style="color: #FF0000;" ';
                
                $link_total_switch_acc = '<span style="color:#FFF;" class="link_switch">'.number_format($total_switch_acc,0).'</span>';              

                
                $total_olt = $t_up_olt + $t_down_olt;
                $link_t_down_olt = number_format($t_down_olt,0);
                if ( ($t_down_olt > 0)) 
                {
                    $warna_olt = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_olt = '<span style="color:red;" class="link_olt">'.number_format($t_down_olt,0).'</span>';
                    
                }
                elseif ( ($t_up_olt == 0) and ($t_down_olt == 0))
                { 
                    $warna_olt = ' abu';
                    #$trclass = '';
                }
                elseif ( ($t_up_olt > 0) and ($t_down_olt == 0))
                { 
                    $warna_olt = ' hijau';
                    #$trclass = '';
                }
                elseif ( ($t_up_olt > 0))
                {
                    #$trclass = '';
                }
                
                $fclass_olt_up = $fclass_olt_down = ' style="color: #fff;" ';
                $link_t_up_olt = number_format($t_up_olt,0);
                
                if ($t_up_olt > 0) {
                    $fclass_olt_up = ' style="color: #82D711;" ';
                    $link_t_up_olt = '<span style="color:#82D711;" class="link_olt">'.number_format($t_up_olt,0).'</span>';
                    }
                if ($t_down_olt > 0)
                    $fclass_olt_down = ' style="color: #FF0000;" ';
                $link_total_olt = '<span style="color:#FFF;" class="link_olt">'.number_format($total_olt,0).'</span>';              
                

                $total_ce = $t_up_ce + $t_down_ce;
                $link_t_down_ce = number_format($t_down_ce,0);
                if ( ($t_down_ce > 0)) 
                {
                    $warna_ce = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_ce = '<span style="color:red;" class="link_ce">'.number_format($t_down_ce,0).'</span>';
                    
                }
                elseif ( ($t_up_ce == 0) and ($t_down_ce == 0))
                { 
                    $warna_ce = ' abu';
                    #$trclass = '';
                }
                elseif ( ($t_up_ce > 0) and ($t_down_ce == 0))
                { 
                    $warna_ce = ' hijau';
                    #$trclass = '';
                }
                elseif ( ($t_up_ce > 0))
                {
                    #$trclass = '';
                }
                
                $fclass_ce_up = $fclass_ce_down = ' style="color: #fff;" ';
                $link_t_up_ce = number_format($t_up_ce,0);
                
                if ($t_up_ce > 0) {
                    $fclass_ce_up = ' style="color: #82D711;" ';
                    $link_t_up_ce = '<span style="color:#82D711;" class="link_ce">'.number_format($t_up_ce,0).'</span>';
                    }
                if ($t_down_ce > 0)
                    $fclass_ce_down = ' style="color: #FF0000;" ';
                $link_total_ce = '<span style="color:#FFF;" class="link_ce">'.number_format($total_ce,0).'</span>';              

                $total_ont = $t_up_ont + $t_down_ont;
                $link_t_down_ont = number_format($t_down_ont,0);
                if ( ($t_down_ont > 0)) 
                {
                    $trclass = 'highlight';
                    $link_t_down_ont = '<span style="color:red;" class="link_ont">'.number_format($t_down_ont,0).'</span>';
                    
                }
                
                $fclass_ont_up = $fclass_ont_down = ' style="color: #fff;" ';
                $link_t_up_ont = number_format($t_up_ont,0);
                
                if ($t_up_ont > 0) {
                    $fclass_ont_up = ' style="color: #82D711;" ';
                    $link_t_up_ont = '<span style="color:#82D711;" class="link_ce">'.number_format($t_up_ont,0).'</span>';
                    }
                if ($t_down_ont > 0)
                    $fclass_ont_down = ' style="color: #FF0000;" ';
                $link_total_ont = '<span style="color:#FFF;" class="link_ce">'.number_format($total_ont,0).'</span>';              


                //$link_t_down = '<a href="'.$link_param.'" target="_blank" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-ap" style="color:red;" class="link_ap">'.number_format($t_down,0).'</a>';

                $html .= '<tr class="link_ap '.$trclass.'" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'">';                
                $html .= '<td class="tc">'.$no.'</td>';                
                $html .= '<td class="tl">'.strtoupper($alias).'</td>';                
                
                if (strtoupper($title) == 'TELKOM OFFICE')
                {
                    $html .= '<td class="tr"'.$fclass_ce_up.'>'.$link_t_up_ce.'</td>';                
                    $html .= '<td class="tr"'.$fclass_ce_down.'>'.$link_t_down_ce.'</td>';                
                    $html .= '<td class="tr">'.$link_total_ce.'</td>';                                    
                    $html .= '<td class="tr"'.$fclass_olt_up.'>'.$link_t_up_olt.'</td>';                
                    $html .= '<td class="tr"'.$fclass_olt_down.'>'.$link_t_down_olt.'</td>';                
                    $html .= '<td class="tr">'.$link_total_olt.'</td>';                                    
                }
                else
                {
                    $html .= '<td class="tr"'.$fclass_up.'>'.number_format($t_up,0).'</td>';                
                    $html .= '<td class="tr"'.$fclass_down.'>'.$link_t_down.'</td>';                
                    $html .= '<td class="tr">'.number_format($total,0).'</td>';       
                    if (ucwords($title) == 'Reguler')
                    {
                    $html .= '<td class="tr"'.$fclass_switch_up.'>'.$link_t_up_ont.' | '.$link_t_up_switch_acc.'</td>';                
                    $html .= '<td class="tr"'.$fclass_switch_down.'>'.$link_t_down_ont.' | '.$link_t_down_switch_acc.'</td>';                
                    $html .= '<td class="tr">'.$link_total_ont.' | '.$link_total_switch_acc.'</td>';                
                    } else {
                    $html .= '<td class="tr"'.$fclass_switch_up.'>'.$link_t_up_ont.' | '.$link_t_up_switch_cr.' | '.$link_t_up_switch_acc.'</td>';                
                    $html .= '<td class="tr"'.$fclass_switch_down.'>'.$link_t_down_ont.' | '.$link_t_down_switch_cr.' | '.$link_t_down_switch_acc.'</td>';                
                    $html .= '<td class="tr">'.$link_total_ont.' | '.$link_total_switch_cr.' | '.$link_total_switch_acc.'</td>';                
                    }
                    
                    
                }
                $html .= '</tr>';   
                $no++;
                $tt_up += $t_up; 
                $tt_down += $t_down; 
                $ttotal += $total; 
                $tt_up_switch_cr += $t_up_switch_cr; 
                $tt_down_switch_cr += $t_down_switch_cr; 
                $ttotal_switch_cr += $total_switch_cr; 
                $tt_up_switch_acc += $t_up_switch_acc; 
                $tt_down_switch_acc += $t_down_switch_acc; 
                $ttotal_switch_acc += $total_switch_acc; 
                $tt_up_olt += $t_up_olt; 
                $tt_down_olt += $t_down_olt; 
                $ttotal_olt += $total_olt; 
                $tt_up_ce += $t_up_ce; 
                $tt_down_ce += $t_down_ce; 
                $ttotal_ce += $total_ce; 
                $tt_up_ont += $t_up_ont; 
                $tt_down_ont += $t_down_ont; 
                $ttotal_ont += $total_ont; 
                
            } 
            
                $html .= '<tr class="no-border">';                
                $html .= '<td colspan="2" class="no-border">&nbsp;</td>';                

                if (strtoupper($title) == 'TELKOM OFFICE')
                {
                                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.$tt_up_ce.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.$tt_down_ce.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.$ttotal_ce.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.$tt_up_olt.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.$tt_down_olt.'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.$ttotal_olt.'</td>';                
                }
                else
                {
                                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.number_format($tt_up,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.number_format($tt_down,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.number_format($ttotal,0).'</td>';
                    if (ucwords($title) == 'Reguler')
                    {
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_up_ont,0).' | '.number_format($tt_up_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_down_ont,0).' | '.number_format($tt_down_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.number_format($ttotal_ont,0).' | '.number_format($ttotal_switch_acc,0).'</td>';                
                    } else {
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_up_ont,0).' | '.number_format($tt_up_switch_cr,0).' | '.number_format($tt_up_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_down_ont,0).' | '.number_format($tt_down_switch_cr,0).' | '.number_format($tt_down_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.number_format($ttotal_ont,0).' | '.number_format($ttotal_switch_cr,0).' | '.number_format($ttotal_switch_acc,0).'</td>';                
                    }
                    
                    #$html .= '<td class="tr '.$fclass.' alternate_black">'.number_format($ttotal_ont,0).' | '.number_format($ttotal_switch_cr,0).' | '.number_format($ttotal_switch_acc,0).'</td>';                
                    
                }
                    $html .= '</tr>';
                   
            $html .= '</table>';
            
                $res = array();
                $res['total_nodeb_up'] = $total_nodeb;
                $res['total_nodeb_down'] = ;
                $res['total_ce_up'] = $tt_up_switch_cr;
                $res['total_ce_down'] = $tt_down_switch_cr;
                $res['html'] = $html;
                return $res;
                */
                
    
}

function summary_top20($title,$data,$params)
{
            $html = '<table class="tlist_dashboard" width="99.5%">
            <tr>
                <td colspan="4" class="tc kolomjudul" style="background-color:#EF3125;">'.ucwords($title).'</td>
            </tr>                      
            <tr>
                <td class="kolomjudul tc" style="color: #FFF;background-color:#333; font-weight: bold;">No.</td>
                <td class="kolomjudul" style="color: #FFF;background-color:#333; font-weight: bold;">Cust. Name</td>
                <td class="tc" style="color: #FFF;background-color:#333;"><img title="RTGS" src="/assets/dws/images/rtgs.png" style="height: 24px;" /><br />U | D | Un | Tot.</td>
                <td class="tc" style="color: #FFF;background-color:#333;"><img title="LINK" src="/assets/dws/images/link.png"  style="height: 20px;"  /><br />U | D | Un | Tot.</td>
            </tr>';

            $sum_total_r = $sum_total_l = 0;
            $total_r_up = $total_r_down = $total_r_unk = 0; 
            $total_l_up = $total_l_down = $total_l_unk = 0;            
            $no = 1;
            $arr_tipe_devices = array('RTGS','LINK');
            foreach($data as $cname => $devtype_rows)
            {
                $html_sisip = ''; 
                $trclass = '';
                #echo '<pre>'.print_r($data,1).'</pre>'; die;
                #foreach($devtype_rows as $tipe => $status)                
                foreach($arr_tipe_devices as $tipe)
                {
                    #echo '<pre>'.print_r($tipe,1).'</pre>'; die;
                    #echo '<pre>'.print_r($data[$cname],1).'</pre>'; die;
                    if (!in_array($tipe,array_keys($data[$cname])))
                    {
                    #echo '<pre>'.print_r(array_keys($data[$cname]),1).'</pre>'; die;
                    $total = 0; 
                    $t_down = 0;
                    $t_up =  0; 
                    $t_unk =  0;                        
                    }
                    else
                    {
                    $status = $data[$cname][$tipe];
                    $total = $status['UP'] + $status['DOWN'] + $status['UNKNOWN']; 
                    $t_down = $status['DOWN'];
                    $t_up =  $status['UP']; 
                    $t_unk =  $status['UNKNOWN'];
                    }
                                        
                    if ($tipe=='RTGS')
                    {
                        $sum_total_r += $total; 
                        $total_r_up += $t_up; 
                        $total_r_down += $t_down; 
                        $total_r_unk += $t_unk;                         
                    }
                    if ($tipe=='LINK')
                    {
                        $sum_total_l += $total; 
                        $total_l_up += $t_up; 
                        $total_l_down += $t_down; 
                        $total_l_unk += $t_unk;                         
                    }
                    
                     
                    $fclass_up = $fclass_down = $fclass_unk = ' fputih ';
                    $trclass = '';
                    if ($t_up > 0)
                        $fclass_up = ' fhijau ';
                    if ($t_down > 0)
                        $fclass_down = ' fmerah ';
                    if ($t_unk > 0)
                        $fclass_unk = ' fabu ';
                    
                    #if (($t_down > 0) or ($t_unk > 0)) $trclass = ' highlight ';
                    $html_sisip .= '<td class="tr"><span class="'.$fclass_up.'">'.number_format($t_up,0).'</span> | <span class="'.$fclass_down.'">'.number_format($t_down,0).'</span> | <span class="'.$fclass_unk.'">'.number_format($t_unk,0).'</span> | <span class="fputih">'.$total.'</span></td>';
                       
                }
                $trid = $params['cat'].'-'.$params['ctype'].'-'.$cname; 
                $html .= '<tr id="'.$trid.'" class="link_ap'.$trclass.'">';   
                $html .= '<td>'.$no.'</td>';   
                $html .= '<td>'.$cname.'</td>';  
                $html .= $html_sisip;
                $html .= '</tr>';   
                $no++;
            }
            
            $html .= '<tr class="no-border">';   
            $html .= '<td colspan="2" class="no-border">&nbsp;</td>';   
            $html .= '<td class="bgoren2 tr" style="background-color:#333; font-weight: bold;"><span class="fhijau">'.number_format($total_r_up,0).'</span> | <span class="fmerah">'.number_format($total_r_down,0).'</span> | <span class="fabu">'.number_format($total_r_unk,0).'</span> | '.number_format($sum_total_r,0).'</td>';  
            $html .= '<td class="bgoren2 tr" style="background-color:#333; font-weight: bold;"><span class="fhijau">'.number_format($total_l_up,0).'</span> | <span class="fmerah">'.number_format($total_l_down,0).'</span> | <span class="fabu">'.number_format($total_l_unk,0).'</span> | '.number_format($sum_total_l,0).'</td>';  
            $html .= '</tr>';   
            $html .= '</table><br>';   
            #echo $html;
            $res = array();
            $res['html'] = $html;
            $res['total_r_up'] = $total_r_up;
            $res['total_r_down'] = $total_r_down;
            $res['total_r_unk'] = $total_r_unk;
            $res['total_l_up'] = $total_l_up;
            $res['total_l_down'] = $total_l_down;
            $res['total_l_unk'] = $total_l_unk;
            return $res;

                
    
}

function statistic_table($title,$data,$db_cacti,$conn_cacti,$table_prefix,$max_eventid,$db_icinga,$conn_icinga,$complete_table = true)
{
    if ($complete_table)
    {
            $html = '
        <table class="tlist fl" width="100%">
            <tr>
                <td colspan="8" class="tc kolomjudul">'.ucwords($title).'</td>
            </tr>
            <tr>
                <td class="kolomjudul alternate_black tc">No.</td>
                <td class="kolomjudul alternate_black">Location</td>
                <td class="tc alternate_black"><img title="Access Point UP" src="/assets/images/wifi-up.png" class="img_wifi" /></td>
                <td class="tc alternate_black"><img title="Access Point Down" src="/assets/images/wifi-down.png" class="img_wifi" /></td>
                <td class="alternate_black">Tot</td>                
                <td class="tc alternate_black"><img title="Switch UP" src="/assets/images/switch-up.png" class="img_wifi" /> O/C/A</td>
                <td class="tc alternate_black"><img title="Switch UP" src="/assets/images/switch-down.png" class="img_wifi" /> O/C/A</td>
                
                <td class="alternate_black">Tot</td>
            </tr>';
    } else { $html = ''; }
    
            $ttotal = $tt_up = $tt_down = $ttotal_switch = $tt_up_switch = $tt_down_switch = 0;
            $no = 1;
            foreach($data as $id => $lokasi) {
                $loc_id = $lokasi['loc_id'];
                $api_addr = $lokasi['ip_address'];
                $loc_name = $lokasi['loc_area'];
                $aps = array();
                mysql_select_db($db_cacti,$conn_cacti);
                $qap = "select * from ".$table_prefix."p_map_event_nms where event_id = ".$max_eventid." and loc_id = ".$loc_id;
                #print_out($qap);
                $res_ap = mysql_query($qap);
               
                while($row = mysql_fetch_assoc($res_ap))
                {
                    $aps[] = $row['nms_name'];
                }
                
                $total = sizeof($aps);
                $t_up = $t_down = 0;
                if (sizeof($aps))
                {
                    $nmsData = $Array_details = array();
                    /** GET AP STATUS */
                    /*
                    $str_apname = '%22'.implode('%22,%22',$aps).'%22';
                    $status_url = 'https://'.$api_addr.'/webacs/api/v1/data/Radios?.full=true&apName=in('.$str_apname.')';
                    $datar = file_get_contents($status_url, false, $context);
                    $domr = new DOMDocument;
                    $domr->loadXML($datar);
                    if (!$domr) {
                        echo 'Error while parsing the document';
                        exit;
                    }
                    $xr = simplexml_import_dom($domr);
                    #print_out($xr->entity);
                    
                    foreach ($xr->entity as $radio)
                    {
                        $nms_id = (string)$radio->radiosDTO->apName;
                        $operstatus = (string)$radio->radiosDTO->operStatus;
                        
                        if (array_key_exists($nms_id,$nmsData) && $nmsData[(string)$nms_id]['OPER_STATUS'] == 'UP')
                        {
                            $operstatus = 'UP';
                        }
                        $nmsData[(string)$nms_id]['OPER_STATUS'] = $operstatus;
                    }
#print_out($nmsData);
                    */

                    foreach($aps as $namaap)
                    {
                        if (array_key_exists($namaap,$nmsData))
                        {
                            $Array_details[$namaap]['OPER_STATUS'] = $nmsData[(string)$namaap]['OPER_STATUS'];
                        } else {
                            $Array_details[$namaap]['OPER_STATUS'] = 'DOWN';
                        }
                    }
                    
#print_out($Array_details);
                    foreach($Array_details as $namaAP)
                    {
                        if (strtoupper($namaAP['OPER_STATUS']) == 'UP') $t_up++;
                        elseif (strtoupper($namaAP['OPER_STATUS']) == 'DOWN') $t_down++;
                        else $t_unk++;
                        #$APSTATUS = @$Array_details[$apname]['OPER_STATUS'];
                        
                    }
                }
                
                /** GET SWITCH STATUS */
                $t_up_switch = $t_down_switch = 0;
                mysql_select_db($db_icinga,$conn_icinga);
                $q = "select b.hotel_name,b.ip_address,c.current_state ". 
                    "from hotel_ip_switchs b, icinga_hosts a, icinga_hoststatus c ". 
                    "where b.ip_address = a.address and a.host_object_id = c.host_object_id ".
                    "and upper(b.hotel_name) = upper('".$loc_name."') ";
                    #print_out($q);

                /*                    
                $q = "select substr(output,1,15) status,current_state,count(*) jml from icinga_hosts a, icinga_hoststatus b,hotel_ip_switch c  where a.host_object_id = b.host_object_id 
and a.address = c.ip_address and upper(c.hotel_name) = upper('".$loc_name."')  group by substr(output,1,15) ";
*/

                $res = mysql_query($q,$conn_icinga);
                $switchs = array();
                $rows = array();
                
                while($row = mysql_fetch_assoc($res))
                {
                    /*
                    $status = explode('-',$row['status']);
                    if (trim($status[0]) == 'PING OK')
                       $t_up_switch++;
                    else
                        $t_down_switch++;     
                    */
                    $switchs[] = $row['ip_address'];
                    
                    
                    if ($row['current_state'] == '')
                        $t_down_switch++;                    
                    elseif ($row['current_state'] == 0)
                        $t_up_switch++;
                    elseif ($row['current_state'] == 1)
                        $t_down_switch++;
                        
                    $rows[] = $row;
                    
                    
                }
                #echo mysql_error();
                #print_out($res);
                #print_out($q);
                #print_out($rows);
                //die;
                $total_switch = $t_up_switch + $t_down_switch;
                $link_t_down = $t_down;
                $link_t_down_switch = $t_down_switch;
                
                if ( ($t_down > 0)) 
                {
                    $warna = ' merah';
                    $fclass = ' fmerah'; 
                    $trclass = 'highlight';
                    $link_t_down = '<a href="#" id="'.$max_eventid.'-'.$lokasi['loc_id'].'-ap" style="color:red;" class="link_ap">'.$t_down.'</a>';
                    
                }
                elseif ( ($t_up == 0) and ($t_down == 0))
                { 
                    $warna = ' abu';
                    $fclass = ' fputih'; 
                    $trclass = '';
                }
                elseif ( ($t_up > 0) and ($t_down == 0))
                { 
                    $warna = ' hijau';
                    $fclass = ' fputih'; 
                    $trclass = '';
                }
                /** STYLING SWITCH COUNT STATISTIC */
                if ( ($t_down_switch > 0)) 
                {
                    $warna_switch = ' merah';
                    $fclass_switch = ' fmerah'; 
                    $trclass = 'highlight';
                    $link_t_down_switch = '<a href="#" id="'.$max_eventid.'-'.$lokasi['loc_id'].'-switch" style="color:red;" class="link_switch">'.$t_down_switch.'</a>';
                    
                }
                elseif ( ($t_up_switch == 0) and ($t_down_switch == 0))
                { 
                    $warna_switch = ' abu';
                    $fclass_switch = ' fputih'; 
                    $trclass = '';
                }
                elseif ( ($t_up_switch > 0) and ($t_down_switch == 0))
                { 
                    $warna_switch = ' hijau';
                    $fclass_switch = ' fputih'; 
                    $trclass = '';
                }
                
                $html .= '<tr class="'.$trclass.'">';                
                $html .= '<td class="tc">'.$no.'</td>';                
                $html .= '<td class="tl">'.strtoupper($lokasi['loc_area']).'</td>';                
                $html .= '<td class="tr">'.$t_up.'</td>';                
                $html .= '<td class="tr '.$fclass.'">'.$link_t_down.'</td>';                
                $html .= '<td class="tr">'.$total.'</td>';                
                $html .= '<td class="tr">'.$t_up_switch.'</td>';                
                $html .= '<td class="tr '.$fclass_switch.'">'.$link_t_down_switch.'</td>';                
                $html .= '<td class="tr">'.$total_switch.'</td>';                
                $html .= '</tr>';   
                $no++;
                $tt_up += $t_up; 
                $tt_down += $t_down; 
                $ttotal += $total; 
                $tt_up_switch += $t_up_switch; 
                $tt_down_switch += $t_down_switch; 
                $ttotal_switch += $total_switch; 
                
            } 
            
                $html .= '<tr>';                
                $html .= '<td colspan="2" class="kolomjudul alternate_black">TOTAL</td>';                
                $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_up.'</td>';                
                $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_down.'</td>';                
                $html .= '<td class="tr '.$fclass.' alternate_black">'.$ttotal.'</td>';                
                $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_up_switch.'</td>';                
                $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_down_switch.'</td>';                
                $html .= '<td class="tr '.$fclass.' alternate_black">'.$ttotal_switch.'</td>';                
                $html .= '</tr>';
                   
    if ($complete_table)
    {
            $html .= '</table>';
    }   
                $res = array();
                $res['total_sw_up'] = $tt_up_switch;
                $res['total_sw_down'] = $tt_down_switch;
                $res['total_ap_up'] = @$tt_up_ap;
                $res['total_ap_down'] = @$tt_down_ap;
                $res['html'] = $html;
                return $res;
    
}

function statistic_table_new($title,$data,$db_cacti,$conn_cacti,$table_prefix,$max_eventid,$db_icinga,$conn_icinga,$complete_table = true)
{

    if ($complete_table)
    {
            $html = '
        <table class="tlist fl" width="100%">
            <tr>
                <td colspan="8" class="tc kolomjudul">'.ucwords($title).'</td>
            </tr>
            <tr>
                <td class="kolomjudul alternate_black tc">No.</td>
                <td class="kolomjudul alternate_black">Location</td>
                <td class="tc alternate_black"><img title="Access Point UP" src="/assets/images/wifi-up.png" class="img_wifi" /></td>
                <td class="tc alternate_black"><img title="Access Point Down" src="/assets/images/wifi-down.png" class="img_wifi" /></td>
                <td class="alternate_black">Tot</td>
                <td class="tc alternate_black"><img title="Switch UP" src="/assets/images/switch-up.png" class="img_wifi" /> O/C/A</td>
                <td class="tc alternate_black"><img title="Switch UP" src="/assets/images/switch-down.png" class="img_wifi" /> O/C/A</td>
                <td class="alternate_black">Tot</td>
            </tr>';
    } else { $html = ''; }
            
            /** SQL STATUS AP */
            $sql_ap = "SELECT   a.loc_area, b.status, count(*) jml
            FROM     x_list_location a, x_p_map_event_nms b 
            where    a.loc_id = b.loc_id and a.event_id = ".$max_eventid." group by a.loc_area, b.status order by a.loc_area, b.status ";
            $res_ap = mysql_query($sql_ap,$conn_cacti);
            $rows_ap = array();
            while($row = mysql_fetch_assoc($res_ap))
            {
                $rows_ap[$row['loc_area']][$row['status']] = $row['jml'];
            }
    
            $ttotal = $tt_up = $tt_down = $ttotal_switch = $tt_up_switch = $tt_down_switch = 0;
            $no = 1;

            foreach($data as $hotel => $lokasi) {
                
                $t_down_switch = $t_up_switch = $t_up = $t_down = 0;
                $loc_id = $lokasi[0]['location_id'];
                $t_up = array_key_exists($hotel,$rows_ap) ? @$rows_ap[$hotel]['UP'] : 0;
                $t_down = array_key_exists($hotel,$rows_ap) ? @$rows_ap[$hotel]['DOWN'] : 0;
                foreach($lokasi as $row)
                {
                    $switchs[] = $row['ip_address'];
                    if ($row['current_state'] == '')
                        $t_down_switch++;                    
                    elseif ($row['current_state'] == 0)
                        $t_up_switch++;
                    elseif ($row['current_state'] == 1)
                        $t_down_switch++;
                        
                    $rows[] = $row;
                }
                
                #echo mysql_error();
                #print_out($res);
                #print_out($q);
                #print_out($rows);
                //die;
                $total = $t_up + $t_down;
                $total_switch = $t_up_switch + $t_down_switch;
                $link_t_down = $t_down;
                $link_t_down_switch = $t_down_switch;
                
                if ( ($t_down > 0)) 
                {
                    $warna = ' merah';
                    $fclass = ' fmerah'; 
                    $trclass = 'highlight';
                    #$link_t_down = '<a href="#" id="'.$max_eventid.'-'.$loc_id.'-'.$hotel.'-ap" style="color:red;" class="link_ap">'.$t_down.'</a>';
                    $link_t_down = '<a href="#" id="'.$max_eventid.'-'.$loc_id.'-'.$hotel.'-ap" style="color:red;" class="">'.$t_down.'</a>';
                    
                }
                elseif ( ($t_up == 0) and ($t_down == 0))
                { 
                    $warna = ' abu';
                    $fclass = ' fputih'; 
                    $trclass = '';
                }
                elseif ( ($t_up > 0) and ($t_down == 0))
                { 
                    $warna = ' hijau';
                    $fclass = ' fhijau'; 
                    $trclass = '';
                }
                $fclass_up = $fclass_down = ' style="color: #fff;" ';
                
                if ($t_up > 0)
                    $fclass_up = ' style="color: #82D711;" ';
                if ($t_down > 0)
                    $fclass_down = ' style="color: #FF0000;" ';
                
                /** STYLING SWITCH COUNT STATISTIC */
                if ( ($t_down_switch > 0)) 
                {
                    $warna_switch = ' merah';
                    /*
                    $fclass_switch_down = ' style="color: #FF0000;" ';
                    if ($t_up_switch > 0)
                        $fclass_switch_up = ' style="color: #fff;" '; 
                    */    
                    $trclass = 'highlight';
                    $link_t_down_switch = '<a href="#" id="'.$max_eventid.'-'.$loc_id.'-'.$hotel.'-1-switch" style="color:red;" class="link_switch">'.$t_down_switch.'</a>';
                    
                }
                elseif ( ($t_up_switch == 0) and ($t_down_switch == 0))
                { 
                    $warna_switch = ' abu';
                    $trclass = '';
                    /*                    
                    $fclass_switch_up = ' style="color: #fff;" ';
                    $fclass_switch_down = ' style="color: #fff;" ';
                    */ 
                }
                elseif ( ($t_up_switch > 0) and ($t_down_switch == 0))
                { 
                    $warna_switch = ' hijau';
                    $trclass = '';
                    /*
                    $fclass_switch_up = ' style="color: #82D711;" ';
                    $fclass_switch_down = ' style="color: #fff;" ';
                    */
                }
                elseif ( ($t_up_switch > 0))
                {
                    /*
                    $fclass_switch_up = ' style="color: #82D711;" ';
                    if ($t_down_switch > 0) 
                    $fclass_switch_down = ' style="color: #FF0000;" ';
                    */ 
                    $trclass = '';
                }
                
                $fclass_switch_up = $fclass_switch_down = ' style="color: #fff;" ';
                
                $link_t_up_switch = $t_up_switch;
                
                if ($t_up_switch > 0) {
                    $fclass_switch_up = ' style="color: #82D711;" ';
                    $link_t_up_switch = '<a href="#" id="'.$max_eventid.'-'.$loc_id.'-'.$hotel.'-0-switch" style="color:#82D711;" class="link_switch">'.$t_up_switch.'</a>';
                    }
                if ($t_down_switch > 0)
                    $fclass_switch_down = ' style="color: #FF0000;" ';
                
                $link_total_switch = '<a href="#" id="'.$max_eventid.'-'.$loc_id.'-'.$hotel.'-ALL-switch" style="color:#FFF;" class="link_switch">'.$total_switch.'</a>';              
            
                
                $html .= '<tr class="'.$trclass.'">';                
                $html .= '<td class="tc">'.$no.'</td>';                
                $html .= '<td class="tl">'.strtoupper($hotel).'</td>';                
                $html .= '<td class="tr"'.$fclass_up.'>'.$t_up.'</td>';                
                $html .= '<td class="tr"'.$fclass_down.'>'.$link_t_down.'</td>';                
                $html .= '<td class="tr">'.$total.'</td>';                
                $html .= '<td class="tr"'.$fclass_switch_up.'>'.$link_t_up_switch.'</td>';                
                $html .= '<td class="tr"'.$fclass_switch_down.'>'.$link_t_down_switch.'</td>';                
                $html .= '<td class="tr">'.$link_total_switch.'</td>';                
                $html .= '</tr>';   
                $no++;
                $tt_up += $t_up; 
                $tt_down += $t_down; 
                $ttotal += $total; 
                $tt_up_switch += $t_up_switch; 
                $tt_down_switch += $t_down_switch; 
                $ttotal_switch += $total_switch; 
                
            } 
            
                $html .= '<tr>';                
                $html .= '<td colspan="2" class="kolomjudul alternate_black">TOTAL</td>';                
                $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_up.'</td>';                
                $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_down.'</td>';                
                $html .= '<td class="tr '.$fclass.' alternate_black">'.$ttotal.'</td>';                
                $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_up_switch.'</td>';                
                $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_down_switch.'</td>';                
                $html .= '<td class="tr '.$fclass.' alternate_black">'.$ttotal_switch.'</td>';                
                $html .= '</tr>';
                   
    if ($complete_table)
    {
            $html .= '</table>';
    }   
                $res = array();
                $res['total_sw_up'] = $tt_up_switch;
                $res['total_sw_down'] = $tt_down_switch;
                $res['total_ap_up'] = @$tt_up_ap;
                $res['total_ap_down'] = @$tt_down_ap;
                $res['html'] = $html;
                return $res;
    
}
 
 
function format_traffic($traffic)
{
    $str_traffic = "";
    $traffic = is_numeric($traffic) ? ceil(($traffic / 1048576)) : $traffic;
    if ($traffic >= 1000)
    { $str_traffic = number_format(($traffic / 1000),2,',','.'); $satuan = 'GB'; }
    else             
    { $str_traffic = number_format($traffic,2,',','.'); $satuan = 'MB'; }
    
    return $str_traffic.' '.$satuan;
    
}

function format_bandwidth($bandwidth)
{
    $str_traffic = "";
    $bandwidth = is_numeric($bandwidth) ? ceil(($bandwidth / 1000000)) : $bandwidth;
    if ($bandwidth >= 1000)
    { $str_traffic = number_format(($bandwidth / 1000),2,',','.'); $satuan = 'Gbps'; }
    else             
    { $str_traffic = number_format($bandwidth,2,',','.'); $satuan = 'Mbps'; }
    
    return $str_traffic.' '.$satuan;
    
}

function format_uptime($uptime,$inmSec = true)
{
    
    $multiplier = 1; // Default dalam detik
    if ($inmSec)    
        $multiplier = $multiplier * 1000;
    
    $oneD = 86400 * $multiplier; 
    $oneH = 3600 * $multiplier;
    $oneMin = 60 * $multiplier;
    $oneSec = 1 * $multiplier;
    
    $day = floor($uptime / $oneD);
    $h = floor(($uptime % $oneD) / $oneH); 
    $m = floor( (($uptime % $oneD) % $oneH) / $oneMin );
    $s = floor( ((($uptime % $oneD) % $oneH) % $oneMin ) / $oneSec);

    #$UPTIME = $day . 'd ' . $h . 'h:' . $m . 'm:' . $s . 's';
    $UPTIME = $day . 'd ' . $h . 'h:' . $m . 'm';
    $UPTIME = !empty($uptime) ? $UPTIME : 'Unknown';
    return $UPTIME;
}

function grabUrl($url)
{
    $ctn = file_get_contents($url);
    return json_decode($ctn,true);    
}

function debugy($var) 
{
	echo "<pre>";
	print_r($var);
	echo "</pre>";
			die();
}

function ceklogin($uid) 
{


if( $_SESSION['auth'] =='' or $_SESSION['uid'] == '') 
{
	 header('Location: login.php');
}
}


//$treshold batas maksimal waktu eskalasi tiket (dalam menit)
function cron_treshold($conn, $db)
{
	
//	die("s");
	$r = mysql_query("select * from g_params where param ='ESC THRESHOLD' ",$conn);
	$rr = mysql_fetch_assoc($r);
//var_dump($rr);die();
	
	$treshold = $rr['value_param'];
	
	
	$sql ="SELECT IF((TIMESTAMPDIFF(MINUTE , a.start_date_problem, NOW( ) ) >".$treshold." AND a.status <> 'closed' ) , 1, 0 ) AS sending,a.*, b.location_name FROM z_tickets a inner join z_venue b on a.venue_id=b.venue_id";
	
	//die($sql);
$res = mysql_query($sql,$conn);
$row = mysql_fetch_assoc($res);


//$loc = mysql_fetch_assoc($res);
					$loc=array();
    			while($row = mysql_fetch_assoc($res))
    			{
    				$message = $row[ticket_id].":".$row[description].",loc:".$row['location_name'].",sts:".$row[status];	
    			
    			
    			   if($row[sending]==1) 
    			   {
    			  		
							   	$mobile = getmobile($conn, $db, 'group', 7);
							   	//debugy($mobile);
							   	foreach($mobile as $m) 
							   	{
							   	  sending_sms($m, $message);	
							   	}
							   
    			   }
    			}
	
}


function sending_sms($no, $text)
	{
	 return true;
if($no!="" && $text!="") {

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "http://servicebus.telkom.co.id:9001/TelkomSystem/SMSGateway/Services/ProxyService/smsBulk?msisdn=".$no."&message=".$text);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,false);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
  curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);

$data=curl_exec($ch);
curl_close($ch);

if($data='SUCCESS<br>') {
return  true;
} else {

return false;
}

return false;
}
}


function getmobile($db, $param='user', $id) {
		if($param=='user'){
		$sql = "select * from z_users a inner join z_person b on a.person_id=b.person_id where a.id=?";
	
		try{
		$loc = $db->fetchRow($sql, $id);
			if($loc[mobile_phone]!=""){
					return $loc[mobile_phone];
				}else{
					return "";
			}
		} catch (Exception $e) {
			
			return "";

		}
		
		
			} elseif($param=='group') {
	$sql = "select * from z_users a inner join z_person b on a.person_id=b.person_id inner join z_users_groups c on a.id =c.uid where c.gid=?";
	 
	
	 
				try{
					
    			$loc=$db->fetchAll($sql, $id);
    			
    		$data=array();
			
			foreach ($loc as $lk) 
			{
				
				 if($lk['mobile_phone']!="") {
			   $data[]=	$lk['mobile_phone'];
				}
			}
			
			if($data){
	
					return $data;
				}else{
					return "";
			}
		} catch (Exception $e) {
			
			return false;

		}
			
	}
}
	
	/*
	 * 
	 * 
	 * ///kalo diinput dengan isu by Caregory
	 * 
	 * 
	 * $params ==loc_id=11
			uid_reporter = admin
			uid_pic 
			priority = urgent
			category = 1101~2
			problem_descr = tesda
	 
	 
	  ///kalo diinput dengan isu by PIC
	 * 
			* 
			loc_id:11
			uid_reporter:admin
			uid_pic:246_user
			priority:urgent
			problem_descr:asdasdasd
	  
	  
	  
	 * 
	 * 
	 * 
	 * 
	 */
	
    function createRec($sx,$sy,$color = 'green',$i,$class = 'kompie',$id,$withclick = true)
    {        
        if ($withclick)
        $svg = '<g class="'.$class.'" onclick="showdetail(\''.$id.'\');">';
        else
        $svg = '<g class="'.$class.'">';
                        
        $svg .= '<image xlink:href="/assets/images/pc_all_in_one.png" x="'.$sx.'" y="'.$sy.'" height="44px" width="48px"/>
        <polygon points="'.($sx+5).','.($sy+5).' '.($sx+25).','.($sy+4).' '.($sx+29).','.($sy+23).' '.($sx+12).','.($sy+28).'" style="fill:'.$color.';stroke:black;stroke-width:1"/>';
        
        if (strlen($i) > 1)
        $sx -= 3; 
        $svg .= '<text x="'.($sx+15).'" y="'.($sy+18).'" font-size="8pt" fill="#fff">'.$i.'</text></g>';
        
        
        return $svg;        
        
    }
	function create_ticket($conn, $db, $params)
	{
	if(isset($params[category])){
		$bar =  explode('~', $params[category]);
	}
	
	  if($params[uid_pic]!=""){		
		$var =  explode('_', $params[uid_pic]);
	  } else {
		  $var[0]=$bar[1]; 
		  $var[1]='group'; 
	  }
	 
	 $loca = array();
	 if($params['loc_id']!=""){
		$vsql = "select * from z_venue where venue_id =".(int)$params['loc_id'];
		//die($vsql);
		  try {
		    $res = mysql_query($vsql);
		    $loca = mysql_fetch_assoc($res);
		  	  } catch (Exception $e) {
			}
		}	
		//debugy($loca);
	
	if($params[category]!="")
	{
		
		$wall_text ='[TICKET] '. $params['problem_descr'].' [lokasi :'.$loca['location_name'].' , '.$loca['group_name'].']';
	 	$sqlx = "insert into z_group_wall (uid, mess_text, gid, created_date, type) values ('1','".$wall_text."', '".$bar[1]."', now(), 'text')";	
	 	
		mysql_query($sqlx, $conn);
		$lastid =mysql_insert_id();
		//echo ($lastid); die("s");
	}
	
	 $sql = "insert into z_tickets	 (event_id, venue_id, category, issuer, start_date_problem, priority, assign_to, description, subject, workforce, ticket_type) values ('$lastid', '".$params[loc_id]."', '".$bar[0]."', '".$params[uid_reporter]."', now(), '".$params[priority]."','".$var[0]."', '".$params[problem_descr]."', 'SERVICE', '".$var[1]."', 'complaint')";	
	
	//die($sql);	
	
	try {
		    mysql_query($sql,$conn);
		    $idt =   mysql_insert_id();
		    $message = $idt.":".$params['problem_descr'].",loc:".$loca['location_name'].",status:open";
		    if($var[1]=='group') {
		    $mobile = getmobile($conn, $db, 'group', $var[0]);
		    
		    //debugy($mobile);
				foreach($mobile as $m) 
				{
				 sending_sms($m, $message);	
				}
		   	} elseif($var[1]=='user') {
		    		$phone = getmobile($conn,$db,'user', $var[0]);
		    		if($phone)
		    		{
		    		   sending_sms($phone, $message);		
		    		}
		    }
		
		} catch (Exception $e) {
			  	
			  
		}
        
     return $sql;
     die;
        
	}
    
    
    function ketupat($x,$y,$w = 12,$cCl = "green", $groupClass="", $strokeWdt=1,$strokeCl = "#fff")
    {
        $kordinat = $x.','.$y.' '.($x + ($w)).','.(($y-$w) - (0.166 * $w)).' '.($x + (2 * $w)).','.$y.' ';
        $yb = (($y+$w) + (0.166*$w));
        $kordinat .= ($x + ($w)).','.$yb;
        
        $svg =
        '<polygon fill="'.$cCl.'" stroke="'.$strokeCl.'" stroke-width="'.$strokeWdt.'" points="'.$kordinat.'" />';    
        
        
        return $svg;
    }
    
    function icon($x,$y,$w = 22,$h = 22,$icon)
    {
        $svg = '<image xlink:href="/assets/dws/images/'.$icon.'" x="'.($x).'" y="'.($y).'" height="'.$h.'px" width="'.$w.'px"/>';
        return $svg;
    }
    function kotak($x,$y,$w = 12,$h = -1,$cCl = "green",$strokeCl = "#fff", $groupClass="", $strokeWdt=1)
    {
        if ($h == -1)
        $h = 0.8 * $w;
        $ya = $y + ($h/4);
        $yb = $ya + ($h/4);
        $yc = $yb + ($h/4);
        $x1 = $x + 3;
        $x2 = $x + ($w-3);
        
        $svg = '<rect x="'.($x).'" y="'.($y).'" width="'.$w.'" height="'.$h.'" style="fill:'.$cCl.'; stroke-width:'.$strokeWdt.'; stroke:'.$strokeCl.'"/>';
        return $svg;
    }
    
    function hexagon($x,$y,$ll = 12,$cCl = "green", $groupClass="", $strokeWdt=1,$strokeCl = "#FFF",$fill="none")
    {
        $kordinat = $x.','.$y.' '.($x + ($ll)).','.($y - (0.58 * $ll)).' '.($x + (2 * $ll)).','.$y.' ';
        $yb = (($y+$ll) + (0.166*$ll));
        $kordinat .= ($x + (2 * $ll)).','.$yb.' '.($x + ($ll)).','.($yb + (0.58 * $ll)).' '.$x.','.(($y+$ll) + (0.166*$ll));
        
        $svg =
        '<polygon fill="'.$cCl.'" stroke="'.$strokeCl.'" stroke-width="'.$strokeWdt.'" points="'.$kordinat.'" />';    
        
        
        return $svg;
        
    }

    function lingkaran($x,$y,$cr = 11,$cCl = "green", $groupClass="", $strokeWdt=1,$strokeCl = "#fff")
    {
        $svg = '<circle cx="'.($x+8).'" cy="'.($y+3).'" r="'.$cr.'" style="fill:'.$cCl.'; stroke-width:'.$strokeWdt.'; stroke:'.$strokeCl.';" />';
        return $svg;        
    }
    
    function wifiLogo($x,$y,$cr = 11,$cCl = "green", $groupClass="", $strokeWdt=1,$strokeCl = "#fff")
    {
        $x1 = $x+4; $y1 = $y-5; $x2 = $x1+8; $y2 = $y1;
        $xend = $x + 16; $yend = $y;
        $svg = '';
        for($i=0;$i<3;$i++)
        {
            $xs = $x + ($i * 2); $ys = $y + ($i * 2);
            $x1s = $x1 + ($i * 2); $y1s = $y1 + ($i * 3);             
            $x2s = $x2 - ($i * 2); $y2s = $y1s;
            if ($i <> 0) {
            $xend = $xend - 2; $yend = $yend + 2;
            }
            
            $svg .= '<path d="M'.$xs.','.$ys.' C'.$x1s.','.$y1s.' '.$x2s.','.$y2s.' '.$xend.','.$yend.'" style="stroke:#fff; stroke-width:2; fill:none;" />';
        }
        $svg .= '<circle cx="'.($xs+4).'" cy="'.($ys+4).'" r="2" style="fill:#fff; stroke-width:2; stroke:#fff;" />';
        
        if ($groupClass != '')
        {
            $svg = '<g class="'.$groupClass.'">'.$svg.'</g>';
        }
        
        return $svg;
        
    }
    function wifiLogo_old($x,$y,$cr = 11,$cCl = "green", $groupClass="", $strokeWdt=1,$strokeCl = "#fff",$fill="none")
    {
        $x1 = $x+4; $y1 = $y-5; $x2 = $x1+8; $y2 = $y1;
        $xend = $x + 16; $yend = $y;
        $svg = '<circle cx="'.($x+8).'" cy="'.($y+3).'" r="11" style="fill:'.$cCl.'; stroke-width:'.$strokeWdt.'; stroke:'.$strokeCl.';" />';
        for($i=0;$i<3;$i++)
        {
            $xs = $x + ($i * 2); $ys = $y + ($i * 2);
            $x1s = $x1 + ($i * 2); $y1s = $y1 + ($i * 3);             
            $x2s = $x2 - ($i * 2); $y2s = $y1s;
            if ($i <> 0) {
            $xend = $xend - 2; $yend = $yend + 2;
            }
            
            $svg .= '<path d="M'.$xs.','.$ys.' C'.$x1s.','.$y1s.' '.$x2s.','.$y2s.' '.$xend.','.$yend.'" style="stroke:#fff; stroke-width:2; fill:none;" />';
        }
        $svg .= '<circle cx="'.($xs+4).'" cy="'.($ys+4).'" r="2" style="fill:#fff; stroke-width:2; stroke:#fff;" />';
        
        if ($groupClass != '')
        {
            $svg = '<g class="'.$groupClass.'">'.$svg.'</g>';
        }
        
        return $svg;
        
    }
    
	function coba()
	{
		die('CCCCC');
	}
function summary_table_all_apec($title,$data_switch,$data_ap,$data_ont,$table_prefix,$eventid,$db,$conn,$complete_table = true)
{
    if ($complete_table)
    {
            $html = '<table class="tlist_dashboard fl" width="100%">
            <tr>
                <td colspan="8" class="tc kolomjudul">'.ucwords($title).'</td>
            </tr>                      
            <tr>
                <td class="kolomjudul tc">No.</td>
                <td class="kolomjudul">Location</td>
                <td class="tc"><img title="Access Point UP" src="/assets/images/wifi-up.png" style="height: 16px;" class="img_wifi" /></td>
                <td class="tc"><img title="Access Point Down" src="/assets/images/wifi-down.png" style="height: 16px;" class="img_wifi" /></td>
                <td class="tc">Tot</td>';
            if ($title == 'Reguler')
            {
             $html.='
                <td class="tc"><img title="Switch UP" src="/assets/images/switch-up.png" style="height: 16px;" class="img_wifi" /><br />O | A</td>
                <td class="tc"><img title="Switch Down" src="/assets/images/switch-down.png" style="height: 16px;" class="img_wifi" /><br />O | A</td>';
            }
            else
            {
             $html.='
                <td class="tc"><img title="Switch UP" src="/assets/images/switch-up.png" style="height: 16px;" class="img_wifi" /><br />O | C | A</td>
                <td class="tc"><img title="Switch Down" src="/assets/images/switch-down.png" style="height: 16px;" class="img_wifi" /><br />O | C | A</td>';
            }
            $html.='
                <td class="tc">Tot</td>
            </tr>';
    } else { $html = ''; }
            
            $ttotal = $tt_up = $tt_down = $ttotal_switch_cr = $ttotal_switch_acc = 0;
            $tt_up_switch_cr = $tt_up_switch_acc = $tt_down_switch_cr = $tt_down_switch_acc = 0;
            $ttotal_ce = $ttotal_up_ce = $ttotal_down_ce = 0;
            $ttotal_olt = $ttotal_up_olt = $ttotal_down_olt = 0;
            $ttotal_ont = $ttotal_up_ont = $ttotal_down_ont = 0;
            $no = 1;

            foreach($data_switch as $hotel => $lokasi) {
                $alias = "";
                $t_down_switch_cr = $t_down_switch_acc = $t_up_switch_cr = $t_up_switch_acc = $t_up = $t_down = 0;
                $t_down_olt = $t_up_olt = 0;
                $t_down_ce = $t_up_ce = 0;
                $t_down_ont = $t_up_ont = 0;
                
                $loc_id = $lokasi[0]['loc_id'];
                $t_up = array_key_exists($hotel,$data_ap) ? @$data_ap[$hotel]['UP'] : 0;
                $t_down = array_key_exists($hotel,$data_ap) ? @$data_ap[$hotel]['DOWN'] : 0;
                
                $t_up_ont = array_key_exists($hotel,$data_ont) ? @$data_ont[$hotel]['UP'] : 0;
                $t_down_ont = array_key_exists($hotel,$data_ont) ? @$data_ont[$hotel]['DOWN'] : 0;

                foreach($lokasi as $row)
                {
                    $alias = $row['alias'];
                    $switchs[] = $row['ip_address'];
                    if ($row['TIPE_SW'] == 'COR')
                    {
                        if ($row['current_state'] == '')
                            $t_down_switch_cr++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_switch_cr++;
                        elseif ($row['current_state'] == 1)
                            $t_down_switch_cr++;
                        
                    }
                    elseif ($row['TIPE_SW'] == 'ACC' || $row['TIPE_SW'] == '8AC')
                    {
                        if ($row['current_state'] == '')
                            $t_down_switch_acc++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_switch_acc++;
                        elseif ($row['current_state'] == 1)
                            $t_down_switch_acc++;
                        
                    }
                    elseif ($row['TIPE_SW'] == 'OLT')
                    {
                        if ($row['current_state'] == '')
                            $t_down_olt++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_olt++;
                        elseif ($row['current_state'] == 1)
                            $t_down_olt++;
                        
                    }
                    elseif ($row['TIPE_SW'] == 'CE-')
                    {
                        if ($row['current_state'] == '')
                            $t_down_ce++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_ce++;
                        elseif ($row['current_state'] == 1)
                            $t_down_ce++;
                        
                    }
                }
                
                $total = $t_up + $t_down;
                $link_t_down = number_format($t_down,0);
                $link_param = $BASEURL.'index.php?event_id='.$eventid.'&loc_name='.$hotel.'&loc_id='.$loc_id;
                if ( ($t_down > 0)) 
                {
                    $warna = ' merah';
                    $fclass = ' fmerah'; 
                    $trclass = 'highlight';
                    $link_t_down = '<span style="color:red;">'.number_format($t_down,0).'</span>';
                    
                }
                elseif ( ($t_up == 0) and ($t_down == 0))
                { 
                    $warna = ' abu';
                    $fclass = ' fputih'; 
                    $trclass = '';
                }
                elseif ( ($t_up > 0) and ($t_down == 0))
                { 
                    $warna = ' hijau';
                    $fclass = ' fhijau'; 
                    $trclass = '';
                }
                $fclass_up = $fclass_down = ' style="color: #fff;" ';
                
                if ($t_up > 0)
                    $fclass_up = ' style="color: #82D711;" ';
                if ($t_down > 0)
                    $fclass_down = ' style="color: #FF0000;" ';
                

                $total_switch_cr = $t_up_switch_cr + $t_down_switch_cr;
                $total_switch_acc = $t_up_switch_acc + $t_down_switch_acc;
                $link_t_down_switch_cr = number_format($t_down_switch_cr,0);
                $link_t_down_switch_acc = number_format($t_down_switch_acc,0);
                
                /** STYLING SWITCH COUNT STATISTIC */
                /** SWITCH CORE ========================= */
                if ( ($t_down_switch_cr > 0)) 
                {
                    $warna_switch_cr = ' merah';
                    $trclass = 'highlight';
                    #$link_t_down_switch_cr = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-1-switch" style="color:red;" class="link_switch">'.number_format($t_down_switch_cr,0).'</a>';
                    $link_t_down_switch_cr = '<span style="color:red;">'.number_format($t_down_switch_cr,0).'</span>';
                    
                }
                elseif ( ($t_up_switch_cr == 0) and ($t_down_switch_cr == 0))
                { 
                    $warna_switch_cr = ' abu';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_cr > 0) and ($t_down_switch_cr == 0))
                { 
                    $warna_switch_cr = ' hijau';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_cr > 0))
                {
                    //$trclass = '';
                }
                
                $fclass_switch_up_cr = $fclass_switch_down_cr = ' style="color: #fff;" ';
                $link_t_up_switch_cr = number_format($t_up_switch_cr,0);
                
                if ($t_up_switch_cr > 0) {
                    $fclass_switch_up_cr = ' style="color: #82D711;" ';
                    $link_t_up_switch_cr = '<span style="color:#82D711;" class="link_switch">'.number_format($t_up_switch_cr,0).'</span>';
                    }
                if ($t_down_switch_cr > 0)
                    $fclass_switch_down_cr = ' style="color: #FF0000;" ';
                
                $link_total_switch_cr = '<span style="color:#FFF;" class="link_switch">'.number_format($total_switch_cr,0).'</span>';              
            

                /** SWITCH ACCESS ========================= */
                if ( ($t_down_switch_acc > 0)) 
                {
                    $warna_switch_acc = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_switch_acc = '<span style="color:red;" class="link_switch">'.number_format($t_down_switch_acc,0).'</span>';
                    
                }
                elseif ( ($t_up_switch_acc == 0) and ($t_down_switch_acc == 0))
                { 
                    $warna_switch_acc = ' abu';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_acc > 0) and ($t_down_switch_acc == 0))
                { 
                    $warna_switch_acc = ' hijau';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_acc > 0))
                {
                    //$trclass = '';
                }
                
                $fclass_switch_up_acc = $fclass_switch_down_acc = ' style="color: #fff;" ';
                $link_t_up_switch_acc = number_format($t_up_switch_acc,0);
                
                if ($t_up_switch_acc > 0) {
                    $fclass_switch_up_acc = ' style="color: #82D711;" ';
                    $link_t_up_switch_acc = '<span style="color:#82D711;" class="link_switch">'.number_format($t_up_switch_acc,0).'</span>';
                    }
                if ($t_down_switch_acc > 0)
                    $fclass_switch_down_acc = ' style="color: #FF0000;" ';
                
                $link_total_switch_acc = '<span style="color:#FFF;" class="link_switch">'.number_format($total_switch_acc,0).'</span>';              

                
                /** OLT ========================= */
                $total_olt = $t_up_olt + $t_down_olt;
                $link_t_down_olt = number_format($t_down_olt,0);
                if ( ($t_down_olt > 0)) 
                {
                    $warna_olt = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_olt = '<span style="color:red;" class="link_olt">'.number_format($t_down_olt,0).'</span>';
                    
                }
                elseif ( ($t_up_olt == 0) and ($t_down_olt == 0))
                { 
                    $warna_olt = ' abu';
                    #$trclass = '';
                }
                elseif ( ($t_up_olt > 0) and ($t_down_olt == 0))
                { 
                    $warna_olt = ' hijau';
                    #$trclass = '';
                }
                elseif ( ($t_up_olt > 0))
                {
                    #$trclass = '';
                }
                
                $fclass_olt_up = $fclass_olt_down = ' style="color: #fff;" ';
                $link_t_up_olt = number_format($t_up_olt,0);
                
                if ($t_up_olt > 0) {
                    $fclass_olt_up = ' style="color: #82D711;" ';
                    $link_t_up_olt = '<span style="color:#82D711;" class="link_olt">'.number_format($t_up_olt,0).'</span>';
                    }
                if ($t_down_olt > 0)
                    $fclass_olt_down = ' style="color: #FF0000;" ';
                $link_total_olt = '<span style="color:#FFF;" class="link_olt">'.number_format($total_olt,0).'</span>';              
                

                /** CE ========================= */
                $total_ce = $t_up_ce + $t_down_ce;
                $link_t_down_ce = number_format($t_down_ce,0);
                if ( ($t_down_ce > 0)) 
                {
                    $warna_ce = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_ce = '<span style="color:red;" class="link_ce">'.number_format($t_down_ce,0).'</span>';
                    
                }
                elseif ( ($t_up_ce == 0) and ($t_down_ce == 0))
                { 
                    $warna_ce = ' abu';
                    #$trclass = '';
                }
                elseif ( ($t_up_ce > 0) and ($t_down_ce == 0))
                { 
                    $warna_ce = ' hijau';
                    #$trclass = '';
                }
                elseif ( ($t_up_ce > 0))
                {
                    #$trclass = '';
                }
                
                $fclass_ce_up = $fclass_ce_down = ' style="color: #fff;" ';
                $link_t_up_ce = number_format($t_up_ce,0);
                
                if ($t_up_ce > 0) {
                    $fclass_ce_up = ' style="color: #82D711;" ';
                    $link_t_up_ce = '<span style="color:#82D711;" class="link_ce">'.number_format($t_up_ce,0).'</span>';
                    }
                if ($t_down_ce > 0)
                    $fclass_ce_down = ' style="color: #FF0000;" ';
                $link_total_ce = '<span style="color:#FFF;" class="link_ce">'.number_format($total_ce,0).'</span>';              
                /** ================================= */

                /** ONT ========================= */
                $total_ont = $t_up_ont + $t_down_ont;
                $link_t_down_ont = number_format($t_down_ont,0);
                if ( ($t_down_ont > 0)) 
                {
                    $trclass = 'highlight';
                    $link_t_down_ont = '<span style="color:red;" class="link_ont">'.number_format($t_down_ont,0).'</span>';
                    
                }
                
                $fclass_ont_up = $fclass_ont_down = ' style="color: #fff;" ';
                $link_t_up_ont = number_format($t_up_ont,0);
                
                if ($t_up_ont > 0) {
                    $fclass_ont_up = ' style="color: #82D711;" ';
                    $link_t_up_ont = '<span style="color:#82D711;" class="link_ce">'.number_format($t_up_ont,0).'</span>';
                    }
                if ($t_down_ont > 0)
                    $fclass_ont_down = ' style="color: #FF0000;" ';
                $link_total_ont = '<span style="color:#FFF;" class="link_ce">'.number_format($total_ont,0).'</span>';              
                /** ================================= */


                //$link_t_down = '<a href="'.$link_param.'" target="_blank" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-ap" style="color:red;" class="link_ap">'.number_format($t_down,0).'</a>';

                $html .= '<tr class="link_ap '.$trclass.'" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'">';                
                $html .= '<td class="tc">'.$no.'</td>';                
                $html .= '<td class="tl">'.strtoupper($alias).'</td>';                
                
                if (strtoupper($title) == 'TELKOM OFFICE')
                {
                    $html .= '<td class="tr"'.$fclass_ce_up.'>'.$link_t_up_ce.'</td>';                
                    $html .= '<td class="tr"'.$fclass_ce_down.'>'.$link_t_down_ce.'</td>';                
                    $html .= '<td class="tr">'.$link_total_ce.'</td>';                                    
                    $html .= '<td class="tr"'.$fclass_olt_up.'>'.$link_t_up_olt.'</td>';                
                    $html .= '<td class="tr"'.$fclass_olt_down.'>'.$link_t_down_olt.'</td>';                
                    $html .= '<td class="tr">'.$link_total_olt.'</td>';                                    
                }
                else
                {
                    $html .= '<td class="tr"'.$fclass_up.'>'.number_format($t_up,0).'</td>';                
                    $html .= '<td class="tr"'.$fclass_down.'>'.$link_t_down.'</td>';                
                    $html .= '<td class="tr">'.number_format($total,0).'</td>';       
                    if (ucwords($title) == 'Reguler')
                    {
                    $html .= '<td class="tr"'.$fclass_switch_up.'>'.$link_t_up_ont.' | '.$link_t_up_switch_acc.'</td>';                
                    $html .= '<td class="tr"'.$fclass_switch_down.'>'.$link_t_down_ont.' | '.$link_t_down_switch_acc.'</td>';                
                    $html .= '<td class="tr">'.$link_total_ont.' | '.$link_total_switch_acc.'</td>';                
                    } else {
                    $html .= '<td class="tr"'.$fclass_switch_up.'>'.$link_t_up_ont.' | '.$link_t_up_switch_cr.' | '.$link_t_up_switch_acc.'</td>';                
                    $html .= '<td class="tr"'.$fclass_switch_down.'>'.$link_t_down_ont.' | '.$link_t_down_switch_cr.' | '.$link_t_down_switch_acc.'</td>';                
                    $html .= '<td class="tr">'.$link_total_ont.' | '.$link_total_switch_cr.' | '.$link_total_switch_acc.'</td>';                
                    }
                    
                    
                }
                $html .= '</tr>';   
                $no++;
                $tt_up += $t_up; 
                $tt_down += $t_down; 
                $ttotal += $total; 
                $tt_up_switch_cr += $t_up_switch_cr; 
                $tt_down_switch_cr += $t_down_switch_cr; 
                $ttotal_switch_cr += $total_switch_cr; 
                $tt_up_switch_acc += $t_up_switch_acc; 
                $tt_down_switch_acc += $t_down_switch_acc; 
                $ttotal_switch_acc += $total_switch_acc; 
                $tt_up_olt += $t_up_olt; 
                $tt_down_olt += $t_down_olt; 
                $ttotal_olt += $total_olt; 
                $tt_up_ce += $t_up_ce; 
                $tt_down_ce += $t_down_ce; 
                $ttotal_ce += $total_ce; 
                $tt_up_ont += $t_up_ont; 
                $tt_down_ont += $t_down_ont; 
                $ttotal_ont += $total_ont; 
                
            } 
            
                $html .= '<tr class="no-border">';                
                $html .= '<td colspan="2" class="no-border">&nbsp;</td>';                

                if (strtoupper($title) == 'TELKOM OFFICE')
                {
                                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.$tt_up_ce.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.$tt_down_ce.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.$ttotal_ce.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.$tt_up_olt.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.$tt_down_olt.'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.$ttotal_olt.'</td>';                
                }
                else
                {
                                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.number_format($tt_up,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.number_format($tt_down,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.number_format($ttotal,0).'</td>';
                    if (ucwords($title) == 'Reguler')
                    {
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_up_ont,0).' | '.number_format($tt_up_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_down_ont,0).' | '.number_format($tt_down_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.number_format($ttotal_ont,0).' | '.number_format($ttotal_switch_acc,0).'</td>';                
                    } else {
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_up_ont,0).' | '.number_format($tt_up_switch_cr,0).' | '.number_format($tt_up_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_down_ont,0).' | '.number_format($tt_down_switch_cr,0).' | '.number_format($tt_down_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.number_format($ttotal_ont,0).' | '.number_format($ttotal_switch_cr,0).' | '.number_format($ttotal_switch_acc,0).'</td>';                
                    }
                    
                    #$html .= '<td class="tr '.$fclass.' alternate_black">'.number_format($ttotal_ont,0).' | '.number_format($ttotal_switch_cr,0).' | '.number_format($ttotal_switch_acc,0).'</td>';                
                    
                }
                    $html .= '</tr>';
                   
    if ($complete_table)
    {
            $html .= '</table>';
    }   
                $res = array();
                $res['total_sw_up_cr'] = $tt_up_switch_cr;
                $res['total_sw_down_cr'] = $tt_down_switch_cr;
                $res['total_sw_up_acc'] = $tt_up_switch_acc;
                $res['total_sw_down_acc'] = $tt_down_switch_acc;
                $res['total_ap_up'] = @$tt_up_ap;
                $res['total_ap_down'] = @$tt_down_ap;
                $res['total_olt_up'] = @$tt_up_olt;
                $res['total_olt_down'] = @$tt_down_olt;
                $res['total_ont_up'] = @$tt_up_ont;
                $res['total_ont_down'] = @$tt_down_ont;
                $res['html'] = $html;
                return $res;
    
}
function summary_table_reguler_apec($title,$arr_lokasi = array(),$data_switch,$data_ap,$data_ont,$table_prefix,$eventid,$db,$conn, $complete_table = true)
{
    if ($complete_table)
    {
            $html = '<table class="tlist_dashboard fl" width="100%">
            <tr>
                <td colspan="8" class="tc kolomjudul">'.ucwords($title).'</td>
            </tr>                      
            <tr>
                <td class="kolomjudul tc">No.</td>
                <td class="kolomjudul">Location</td>
                <td class="tc"><img title="Access Point UP" src="/assets/images/wifi-up.png" style="height: 16px;" class="img_wifi" /></td>
                <td class="tc"><img title="Access Point Down" src="/assets/images/wifi-down.png" style="height: 16px;" class="img_wifi" /></td>
                <td class="tc">Tot</td>';
            if ($title == 'Reguler')
            {
             $html.='
                <td class="tc"><img title="Switch UP" src="/assets/images/switch-up.png" style="height: 16px;" class="img_wifi" /><br />O | A</td>
                <td class="tc"><img title="Switch Down" src="/assets/images/switch-down.png" style="height: 16px;" class="img_wifi" /><br />O | A</td>';
            }
            else
            {
             $html.='
                <td class="tc"><img title="Switch UP" src="/assets/images/switch-up.png" style="height: 16px;" class="img_wifi" /><br />O | C | A</td>
                <td class="tc"><img title="Switch Down" src="/assets/images/switch-down.png" style="height: 16px;" class="img_wifi" /><br />O | C | A</td>';
            }
            $html.='
                <td class="tc">Tot</td>
            </tr>';
    } else { $html = ''; }
            
            $ttotal = $tt_up = $tt_down = $ttotal_switch_cr = $ttotal_switch_acc = 0;
            $tt_up_switch_cr = $tt_up_switch_acc = $tt_down_switch_cr = $tt_down_switch_acc = 0;
            $ttotal_ce = $ttotal_up_ce = $ttotal_down_ce = 0;
            $ttotal_olt = $ttotal_up_olt = $ttotal_down_olt = 0;
            $ttotal_ont = $ttotal_up_ont = $ttotal_down_ont = 0;
            $no = 1;

            foreach($arr_lokasi as $hotel => $rows) {
                $alias = "";
                $t_down_switch_cr = $t_down_switch_acc = $t_up_switch_cr = $t_up_switch_acc = $t_up = $t_down = 0;
                $t_down_olt = $t_up_olt = 0;
                $t_down_ce = $t_up_ce = 0;
                $t_down_ont = $t_up_ont = 0;
                
                $loc_id = $rows[0]['loc_id'];
                $t_up = array_key_exists($hotel,$data_ap) ? @$data_ap[$hotel]['UP'] : 0;
                $t_down = array_key_exists($hotel,$data_ap) ? @$data_ap[$hotel]['DOWN'] : 0;
                
                $t_up_ont = array_key_exists($hotel,$data_ont) ? @$data_ont[$hotel]['UP'] : 0;
                $t_down_ont = array_key_exists($hotel,$data_ont) ? @$data_ont[$hotel]['DOWN'] : 0;

                foreach($rows as $row)
                {
                    $alias = $row['alias'];
                    $switchs[] = $row['ip_address'];
                    if ($row['TIPE_SW'] == 'COR')
                    {
                        if ($row['current_state'] == '')
                            $t_down_switch_cr++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_switch_cr++;
                        elseif ($row['current_state'] == 1)
                            $t_down_switch_cr++;
                        
                    }
                    elseif ($row['TIPE_SW'] == 'ACC' || $row['TIPE_SW'] == '8AC')
                    {
                        if ($row['current_state'] == '')
                            $t_down_switch_acc++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_switch_acc++;
                        elseif ($row['current_state'] == 1)
                            $t_down_switch_acc++;
                        
                    }
                    elseif ($row['TIPE_SW'] == 'OLT')
                    {
                        if ($row['current_state'] == '')
                            $t_down_olt++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_olt++;
                        elseif ($row['current_state'] == 1)
                            $t_down_olt++;
                        
                    }
                    elseif ($row['TIPE_SW'] == 'CE-')
                    {
                        if ($row['current_state'] == '')
                            $t_down_ce++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_ce++;
                        elseif ($row['current_state'] == 1)
                            $t_down_ce++;
                        
                    }
                }
                
                $total = $t_up + $t_down;
                $link_t_down = number_format($t_down,0);
                $link_param = $BASEURL.'index.php?event_id='.$eventid.'&loc_name='.$hotel.'&loc_id='.$loc_id;
                if ( ($t_down > 0)) 
                {
                    $warna = ' merah';
                    $fclass = ' fmerah'; 
                    $trclass = 'highlight';
                    $link_t_down = '<span style="color:red;">'.number_format($t_down,0).'</span>';
                    
                }
                elseif ( ($t_up == 0) and ($t_down == 0))
                { 
                    $warna = ' abu';
                    $fclass = ' fputih'; 
                    $trclass = '';
                }
                elseif ( ($t_up > 0) and ($t_down == 0))
                { 
                    $warna = ' hijau';
                    $fclass = ' fhijau'; 
                    $trclass = '';
                }
                $fclass_up = $fclass_down = ' style="color: #fff;" ';
                
                if ($t_up > 0)
                    $fclass_up = ' style="color: #82D711;" ';
                if ($t_down > 0)
                    $fclass_down = ' style="color: #FF0000;" ';
                

                $total_switch_cr = $t_up_switch_cr + $t_down_switch_cr;
                $total_switch_acc = $t_up_switch_acc + $t_down_switch_acc;
                $link_t_down_switch_cr = number_format($t_down_switch_cr,0);
                $link_t_down_switch_acc = number_format($t_down_switch_acc,0);
                
                /** STYLING SWITCH COUNT STATISTIC */
                /** SWITCH CORE ========================= */
                if ( ($t_down_switch_cr > 0)) 
                {
                    $warna_switch_cr = ' merah';
                    $trclass = 'highlight';
                    #$link_t_down_switch_cr = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-1-switch" style="color:red;" class="link_switch">'.number_format($t_down_switch_cr,0).'</a>';
                    $link_t_down_switch_cr = '<span style="color:red;">'.number_format($t_down_switch_cr,0).'</span>';
                    
                }
                elseif ( ($t_up_switch_cr == 0) and ($t_down_switch_cr == 0))
                { 
                    $warna_switch_cr = ' abu';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_cr > 0) and ($t_down_switch_cr == 0))
                { 
                    $warna_switch_cr = ' hijau';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_cr > 0))
                {
                    //$trclass = '';
                }
                
                $fclass_switch_up_cr = $fclass_switch_down_cr = ' style="color: #fff;" ';
                $link_t_up_switch_cr = number_format($t_up_switch_cr,0);
                
                if ($t_up_switch_cr > 0) {
                    $fclass_switch_up_cr = ' style="color: #82D711;" ';
                    $link_t_up_switch_cr = '<span style="color:#82D711;" class="link_switch">'.number_format($t_up_switch_cr,0).'</span>';
                    }
                if ($t_down_switch_cr > 0)
                    $fclass_switch_down_cr = ' style="color: #FF0000;" ';
                
                $link_total_switch_cr = '<span style="color:#FFF;" class="link_switch">'.number_format($total_switch_cr,0).'</span>';              
            

                /** SWITCH ACCESS ========================= */
                if ( ($t_down_switch_acc > 0)) 
                {
                    $warna_switch_acc = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_switch_acc = '<span style="color:red;" class="link_switch">'.number_format($t_down_switch_acc,0).'</span>';
                    
                }
                elseif ( ($t_up_switch_acc == 0) and ($t_down_switch_acc == 0))
                { 
                    $warna_switch_acc = ' abu';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_acc > 0) and ($t_down_switch_acc == 0))
                { 
                    $warna_switch_acc = ' hijau';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_acc > 0))
                {
                    //$trclass = '';
                }
                
                $fclass_switch_up_acc = $fclass_switch_down_acc = ' style="color: #fff;" ';
                $link_t_up_switch_acc = number_format($t_up_switch_acc,0);
                
                if ($t_up_switch_acc > 0) {
                    $fclass_switch_up_acc = ' style="color: #82D711;" ';
                    $link_t_up_switch_acc = '<span style="color:#82D711;" class="link_switch">'.number_format($t_up_switch_acc,0).'</span>';
                    }
                if ($t_down_switch_acc > 0)
                    $fclass_switch_down_acc = ' style="color: #FF0000;" ';
                
                $link_total_switch_acc = '<span style="color:#FFF;" class="link_switch">'.number_format($total_switch_acc,0).'</span>';              

                
                /** OLT ========================= */
                $total_olt = $t_up_olt + $t_down_olt;
                $link_t_down_olt = number_format($t_down_olt,0);
                if ( ($t_down_olt > 0)) 
                {
                    $warna_olt = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_olt = '<span style="color:red;" class="link_olt">'.number_format($t_down_olt,0).'</span>';
                    
                }
                elseif ( ($t_up_olt == 0) and ($t_down_olt == 0))
                { 
                    $warna_olt = ' abu';
                    #$trclass = '';
                }
                elseif ( ($t_up_olt > 0) and ($t_down_olt == 0))
                { 
                    $warna_olt = ' hijau';
                    #$trclass = '';
                }
                elseif ( ($t_up_olt > 0))
                {
                    #$trclass = '';
                }
                
                $fclass_olt_up = $fclass_olt_down = ' style="color: #fff;" ';
                $link_t_up_olt = number_format($t_up_olt,0);
                
                if ($t_up_olt > 0) {
                    $fclass_olt_up = ' style="color: #82D711;" ';
                    $link_t_up_olt = '<span style="color:#82D711;" class="link_olt">'.number_format($t_up_olt,0).'</span>';
                    }
                if ($t_down_olt > 0)
                    $fclass_olt_down = ' style="color: #FF0000;" ';
                $link_total_olt = '<span style="color:#FFF;" class="link_olt">'.number_format($total_olt,0).'</span>';              
                

                /** CE ========================= */
                $total_ce = $t_up_ce + $t_down_ce;
                $link_t_down_ce = number_format($t_down_ce,0);
                if ( ($t_down_ce > 0)) 
                {
                    $warna_ce = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_ce = '<span style="color:red;" class="link_ce">'.number_format($t_down_ce,0).'</span>';
                    
                }
                elseif ( ($t_up_ce == 0) and ($t_down_ce == 0))
                { 
                    $warna_ce = ' abu';
                    #$trclass = '';
                }
                elseif ( ($t_up_ce > 0) and ($t_down_ce == 0))
                { 
                    $warna_ce = ' hijau';
                    #$trclass = '';
                }
                elseif ( ($t_up_ce > 0))
                {
                    #$trclass = '';
                }
                
                $fclass_ce_up = $fclass_ce_down = ' style="color: #fff;" ';
                $link_t_up_ce = number_format($t_up_ce,0);
                
                if ($t_up_ce > 0) {
                    $fclass_ce_up = ' style="color: #82D711;" ';
                    $link_t_up_ce = '<span style="color:#82D711;" class="link_ce">'.number_format($t_up_ce,0).'</span>';
                    }
                if ($t_down_ce > 0)
                    $fclass_ce_down = ' style="color: #FF0000;" ';
                $link_total_ce = '<span style="color:#FFF;" class="link_ce">'.number_format($total_ce,0).'</span>';              
                /** ================================= */

                /** ONT ========================= */
                $total_ont = $t_up_ont + $t_down_ont;
                $link_t_down_ont = number_format($t_down_ont,0);
                if ( ($t_down_ont > 0)) 
                {
                    $trclass = 'highlight';
                    $link_t_down_ont = '<span style="color:red;" class="link_ont">'.number_format($t_down_ont,0).'</span>';
                    
                }
                
                $fclass_ont_up = $fclass_ont_down = ' style="color: #fff;" ';
                $link_t_up_ont = number_format($t_up_ont,0);
                
                if ($t_up_ont > 0) {
                    $fclass_ont_up = ' style="color: #82D711;" ';
                    $link_t_up_ont = '<span style="color:#82D711;" class="link_ce">'.number_format($t_up_ont,0).'</span>';
                    }
                if ($t_down_ont > 0)
                    $fclass_ont_down = ' style="color: #FF0000;" ';
                $link_total_ont = '<span style="color:#FFF;" class="link_ce">'.number_format($total_ont,0).'</span>';              
                /** ================================= */


                //$link_t_down = '<a href="'.$link_param.'" target="_blank" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-ap" style="color:red;" class="link_ap">'.number_format($t_down,0).'</a>';

                $html .= '<tr class="link_ap '.$trclass.'" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'">';                
                $html .= '<td class="tc">'.$no.'</td>';                
                $html .= '<td class="tl">'.strtoupper($alias).'</td>';                
                
                if (strtoupper($title) == 'TELKOM OFFICE')
                {
                    $html .= '<td class="tr"'.$fclass_ce_up.'>'.$link_t_up_ce.'</td>';                
                    $html .= '<td class="tr"'.$fclass_ce_down.'>'.$link_t_down_ce.'</td>';                
                    $html .= '<td class="tr">'.$link_total_ce.'</td>';                                    
                    $html .= '<td class="tr"'.$fclass_olt_up.'>'.$link_t_up_olt.'</td>';                
                    $html .= '<td class="tr"'.$fclass_olt_down.'>'.$link_t_down_olt.'</td>';                
                    $html .= '<td class="tr">'.$link_total_olt.'</td>';                                    
                }
                else
                {
                    $html .= '<td class="tr"'.$fclass_up.'>'.number_format($t_up,0).'</td>';                
                    $html .= '<td class="tr"'.$fclass_down.'>'.$link_t_down.'</td>';                
                    $html .= '<td class="tr">'.number_format($total,0).'</td>';       
                    if (ucwords($title) == 'Reguler')
                    {
                    $html .= '<td class="tr"'.$fclass_switch_up.'>'.$link_t_up_ont.' | '.$link_t_up_switch_acc.'</td>';                
                    $html .= '<td class="tr"'.$fclass_switch_down.'>'.$link_t_down_ont.' | '.$link_t_down_switch_acc.'</td>';                
                    $html .= '<td class="tr">'.$link_total_ont.' | '.$link_total_switch_acc.'</td>';                
                    } else {
                    $html .= '<td class="tr"'.$fclass_switch_up.'>'.$link_t_up_ont.' | '.$link_t_up_switch_cr.' | '.$link_t_up_switch_acc.'</td>';                
                    $html .= '<td class="tr"'.$fclass_switch_down.'>'.$link_t_down_ont.' | '.$link_t_down_switch_cr.' | '.$link_t_down_switch_acc.'</td>';                
                    $html .= '<td class="tr">'.$link_total_ont.' | '.$link_total_switch_cr.' | '.$link_total_switch_acc.'</td>';                
                    }
                    
                    
                }
                $html .= '</tr>';   
                $no++;
                $tt_up += $t_up; 
                $tt_down += $t_down; 
                $ttotal += $total; 
                $tt_up_switch_cr += $t_up_switch_cr; 
                $tt_down_switch_cr += $t_down_switch_cr; 
                $ttotal_switch_cr += $total_switch_cr; 
                $tt_up_switch_acc += $t_up_switch_acc; 
                $tt_down_switch_acc += $t_down_switch_acc; 
                $ttotal_switch_acc += $total_switch_acc; 
                $tt_up_olt += $t_up_olt; 
                $tt_down_olt += $t_down_olt; 
                $ttotal_olt += $total_olt; 
                $tt_up_ce += $t_up_ce; 
                $tt_down_ce += $t_down_ce; 
                $ttotal_ce += $total_ce; 
                $tt_up_ont += $t_up_ont; 
                $tt_down_ont += $t_down_ont; 
                $ttotal_ont += $total_ont; 
                
            } 
            
                $html .= '<tr class="no-border">';                
                $html .= '<td colspan="2" class="no-border">&nbsp;</td>';                

                if (strtoupper($title) == 'TELKOM OFFICE')
                {
                                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.$tt_up_ce.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.$tt_down_ce.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.$ttotal_ce.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.$tt_up_olt.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.$tt_down_olt.'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.$ttotal_olt.'</td>';                
                }
                else
                {
                                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.number_format($tt_up,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.number_format($tt_down,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.number_format($ttotal,0).'</td>';
                    if (ucwords($title) == 'Reguler')
                    {
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_up_ont,0).' | '.number_format($tt_up_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_down_ont,0).' | '.number_format($tt_down_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.number_format($ttotal_ont,0).' | '.number_format($ttotal_switch_acc,0).'</td>';                
                    } else {
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_up_ont,0).' | '.number_format($tt_up_switch_cr,0).' | '.number_format($tt_up_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_down_ont,0).' | '.number_format($tt_down_switch_cr,0).' | '.number_format($tt_down_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.number_format($ttotal_ont,0).' | '.number_format($ttotal_switch_cr,0).' | '.number_format($ttotal_switch_acc,0).'</td>';                
                    }
                    
                    #$html .= '<td class="tr '.$fclass.' alternate_black">'.number_format($ttotal_ont,0).' | '.number_format($ttotal_switch_cr,0).' | '.number_format($ttotal_switch_acc,0).'</td>';                
                    
                }
                    $html .= '</tr>';
                   
    if ($complete_table)
    {
            $html .= '</table>';
    }   
                $res = array();
                $res['total_sw_up_cr'] = $tt_up_switch_cr;
                $res['total_sw_down_cr'] = $tt_down_switch_cr;
                $res['total_sw_up_acc'] = $tt_up_switch_acc;
                $res['total_sw_down_acc'] = $tt_down_switch_acc;
                $res['total_ap_up'] = @$tt_up_ap;
                $res['total_ap_down'] = @$tt_down_ap;
                $res['total_olt_up'] = @$tt_up_olt;
                $res['total_olt_down'] = @$tt_down_olt;
                $res['total_ont_up'] = @$tt_up_ont;
                $res['total_ont_down'] = @$tt_down_ont;
                $res['html'] = $html;
                return $res;
    
}
	
}
