<?
/* By : BEE-IT 2009 */




class CMS_FunctionsRafinaru {
	function showError($pesan) {
		return "<br><br><br><br><div class='bit_error'>$pesan</div>";
	}
	
	function getVariabelGET() {
		while (list($k,$v)=each($_GET)) {
			$tmp .=$k."=".$v."&";
		}
		$tmp=substr($tmp,0,strlen($tmp)-1);
		return $tmp;
	}
	
	function cekSuperAdmin() {
		$arrProfileData=explode(",",$this->getSessionProfileID());
		for ($i=0;$i<count($arrProfileData);$i++) {
			$arrProfile[$arrProfileData[$i]]=1;
		}
		
		return $arrProfile[8];
	}
	
	function destroySession() {
		global $ora;
		
		$qry="delete from c_log_user where nik='".$this->getSessionUserID()."'";
		$ora->sql_no_fetch($qry);

		unset($_SESSION);
		session_destroy();
		session_unregister("bit_uid");
		session_unregister("bit_uname");
		session_unregister("bit_profile");
		session_unregister("bit_profile_name");
		session_unregister("bit_jabatan");
		session_unregister("bit_group");
		session_unregister("bit_profile_group");
		session_unregister("bit_ubis");
		session_unregister("bit_sub_ubis");
	}
	
	function seq_id() {
		global $ora;
		global $funct;
		$qry="insert into m_seq(seq_date,seq_by) values(sysdate(),'".$funct->getSessionUserID()."')";
		$ora->sql_no_fetch($qry);
		
		$qry="select last_insert_id() id";
		$rsID=$ora->sql_fetch($qry);
		$ID=$rsID->value[1][1];
		
		return $ID;
	}
	
	function getSessionName() {
		return $_SESSION["bit_uname"];
	}
	
	function getSessionGroup() {
		return $_SESSION["bit_group"];
	}
	
	function getSessionUserID() {
		return $_SESSION["bit_uid"];
	}
	
	function getSessionProfileID() {
		return $_SESSION["bit_profile"];
	}
	
	function getSessionProfileName() {
		return $_SESSION["bit_profile_name"];
	}
	
	function getSessionProfileGroup() {
		return $_SESSION["bit_profile_group"];
	}
	
	function getSessionJabatan() {
		return $_SESSION["bit_jabatan"];
	}

	function getSessionUserUbis() {
		return $_SESSION["bit_ubis"];
	}

	function getSessionUserSubUbis() {
		return $_SESSION["bit_sub_ubis"];
	}
	
	function getSessionIsTelkom() {
		return $_SESSION["bit_is_telkom"];
	}
	
	function escape($str) {
		$str=stripslashes($str);
		return str_replace("'","''",$str);
	}
	
	
	function getDataUser($userid) {
		global $ora;
		$qry="select * from m_user where userid='".$userid."'";
		$rsName=$ora->sql_fetch($qry);
		if ($rsName->jumrec==0) 
			return "";
		else
			return $rsName->value[1]["username"]." / ".$rsName->value[1]["userid"]." / ".$rsName->value[1]["jabatan"];
	}
	
	function getDataUser2($userid) {
		global $ora;
		$qry="select * from m_user where userid='".$userid."'";
		$rsName=$ora->sql_fetch($qry);
		if ($rsName->jumrec==0) 
			return "";
		else
			return $rsName->value[1]["username"]." / ".$rsName->value[1]["userid"];
	}

	function getDataUserRecord($userid) {
		global $ora;
		$qry="select * from m_user where userid='".$userid."'";
		$rs=$ora->sql_fetch($qry);
		return $rs;
	}
	
	function listUser() {
		global $ora;
		global $bit_app;
	
		$qry="select * from m_karyawan";
		$rsName=$ora->sql_fetch($qry);
		for ($i=1;$i<=$rsName->jumrec;$i++) {
			$dtUser[$rsName->value[$i]["nik"]][0]=$rsName->value[$i]["nama"]." / ".$rsName->value[$i]["nik"];
			$dtUser[$rsName->value[$i]["nik"]][1]=$rsName->value[$i]["nama"]." / ".$rsName->value[$i]["jabatan"];
			$dtUser[$rsName->value[$i]["nik"]][2]=$rsName->value[$i]["unit"];
			$dtUser[$rsName->value[$i]["nik"]][3]=$rsName->value[$i]["nama"]." / ".$rsName->value[$i]["nik"]." / ".$rsName->value[$i]["jabatan"];
			if ($rsName->value[$i]["foto"])
				$dtUser[$rsName->value[$i]["nik"]][4]=$bit_app["foto_url"]."/".$rsName->value[$i]["foto"];
			else
				$dtUser[$rsName->value[$i]["nik"]][4]="http://10.2.15.232/drp/0PhotoNAS/".$rsName->value[$i]["nik"].".jpg";
			$dtUser[$rsName->value[$i]["nik"]][5]=$rsName->value[$i]["nik"]."-".$rsName->value[$i]["nama"];
			$dtUser[$rsName->value[$i]["nik"]][6]="<u>".$rsName->value[$i]["nama"]."</u><br /> NIK : ".$rsName->value[$i]["nik"];
			$dtUser[$rsName->value[$i]["nik"]][7]=$rsName->value[$i]["nama"]." / ".$rsName->value[$i]["nik"];
			$dtUser[$rsName->value[$i]["nik"]][8]=$rsName->value[$i]["nik"];
			
		}
		return $dtUser;
	}
	
	function getUserFoto($nik) {
		global $ora;
		global $bit_app;
	
		$qry="select * from m_karyawan where nik='".$nik."'";
		$rsName=$ora->sql_fetch($qry);
		for ($i=1;$i<=$rsName->jumrec;$i++) {
			$dtUser[$rsName->value[$i]["nik"]][0]=$rsName->value[$i]["nama"]." / ".$rsName->value[$i]["nik"];
			$dtUser[$rsName->value[$i]["nik"]][1]=$rsName->value[$i]["nama"]." / ".$rsName->value[$i]["jabatan"];
			$dtUser[$rsName->value[$i]["nik"]][2]=$rsName->value[$i]["unit"];
			$dtUser[$rsName->value[$i]["nik"]][3]=$rsName->value[$i]["nama"]." / ".$rsName->value[$i]["nik"]." / ".$rsName->value[$i]["jabatan"];
			if ($rsName->value[$i]["foto"])
				$dtUser[$rsName->value[$i]["nik"]][4]=$bit_app["foto_url"]."/".$rsName->value[$i]["foto"];
			else
				$dtUser[$rsName->value[$i]["nik"]][4]="http://10.2.15.232/drp/0PhotoNAS/".$rsName->value[$i]["nik"].".jpg";
			$dtUser[$rsName->value[$i]["nik"]][5]=$rsName->value[$i]["nik"]."-".$rsName->value[$i]["nama"];
			$dtUser[$rsName->value[$i]["nik"]][6]="<u>".$rsName->value[$i]["nama"]."</u><br /> NIK : ".$rsName->value[$i]["nik"];
			$dtUser[$rsName->value[$i]["nik"]][7]=$rsName->value[$i]["nama"]." / ".$rsName->value[$i]["nik"];
			$dtUser[$rsName->value[$i]["nik"]][8]=$rsName->value[$i]["nik"];
			
		}
		return $dtUser;
	}

	function parent_refresh_self() {
		echo "<script>parent.window.location.href=parent.window.location</script>";
	}
	
	function alert($str) {
		echo "<script>alert('$str')</script>";
	}
	
	function url($url) {
		echo "<script>window.location.href='$url'</script>";
	}
	
	function getMonth($val) {
		$arrMonth=array(""=>"",
					"01"=>"Januari",
					"02"=>"Februari",
					"03"=>"Maret",
					"04"=>"April",
					"05"=>"Mei",
					"06"=>"Juni",
					"07"=>"Juli",
					"08"=>"Agustus",
					"09"=>"September",
					"10"=>"Oktober",
					"11"=>"November",
					"12"=>"Desember");
		while (list($key,$value)=each($arrMonth)) {
			if ((int)$key==(int)$val)
				return $value;
		} 
	}
	
	function getPeriode($periode) {
		return $this->getMonth(substr($periode,4,2))." ".substr($periode,0,4);
	}
	
	function format($val,$prec=0) {
		//if (strlen($val)>0)
			return number_format($val,$prec,",",".");
		//else
		//	return "";
	}
	
	function formatR($val,$prec=0) {
		if (strlen($val)>0)
			return number_format($val,$prec,".",",");
		else
			return "";
	}
	
	function checkValueFromArray($arr,$val) {
		while (list($k,$v)=each($arr)) {
			if ($v==$val)
				return true;
		}
		return false;
	}
	
	function selectProfile() {
		global $ora;
		unset($dtSelect);
		$iCounter=0;
		$qry="select * from m_user a where activeflag=1";
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["userid"];
			$dtSelect[$iCounter][1]=$tmpSpasi.$rs->value[$i]["jabatan"];
			$iCounter++;
		}
		
		return $dtSelect;
	}
	
	function spasi($level) {
		for ($i=1;$i<=$level;$i++) {
			$tmp .="&nbsp;&nbsp;&nbsp;";
		}
		return $tmp;	
	}
	
	
	function getSelectYear() {
		$iCounter=0;
		for ($i=date("Y");$i>=2000;$i--) {
			$dt[$iCounter][0]=$i;	
			$dt[$iCounter][1]=$i;	
			$iCounter++;
		}
		return $dt;
	}

	function getSelectMonth() {
		global $ora;
			
		for ($i=1;$i<=12;$i++) {
			if (strlen($i)==1)
				$i="0".$i;
			
			$dtSelectRespon[$i-1][0]=$i;
			$dtSelectRespon[$i-1][1]=$i;
		}
		
		return $dtSelectRespon;
	}
	
	function convertDate($startDate) {
		$arr=split("-",$startDate);
		if (count($arr)==3)
			return $stDateLabel=$arr[2]."/".$arr[1]."/".$arr[0];
		else
			return $startDate;
	}	
	
	function setNumber($str) {
		return number_format($str,0,"","");
	}
	
	function formatValue($value) {
		$array_value=explode("-",$value);
		if (count($array_value)==3) {
			return $array_value[2]."/".$array_value[1]."/".$array_value[0];
		} else {
			return $this->format($value,2);
		}	
	}
	
	function isNumber($value) {
		return is_numeric($value);
	}
	
	function url_encode($url)
	{
		return urlencode($url);
	} 
	
	function url_decode($url)
	{
		return urldecode($url);
	} 
	
	function datediff($start, $end)
	{
		$start_ts = strtotime($start);
		$end_ts = strtotime($end);

		$diff = $end_ts - $start_ts;
		return round($diff / 86400);
	}
	
	function getDays($date1, $date2){
		$date1 = explode('-', $date1);
		$date2 = explode('-', $date2);
		$date1 = gregoriantojd($date1[1], $date1[2], $date1[0]);
		$date2 = gregoriantojd($date2[1], $date2[2], $date2[0]);
		//return abs($date1 � $date2);
	}

	
	function cleansingNumber($val) {
		$val = str_replace('.','',$val);
		$val = str_replace(',','.',$val);
		
		return $val;
	}
	
	/*==============================================================================================*/
	
	function getTwo($i) {
		return (strlen($i)==1?"0".$i:$i);
	}
	function getParameterJam() {
		global $ora;
		
		$iCounter=0;
		for ($i=0;$i<=23;$i++) {
			$dtSelect[$iCounter][0]=$this->getTwo($i);
			$dtSelect[$iCounter][1]=$this->getTwo($i);
			$iCounter++;
		}
		
		return $dtSelect;
	}

	function getParameterMenit($title="") {
		global $ora;
		
		$iCounter=0;
		for ($i=0;$i<=59;$i++) {
			$dtSelect[$iCounter][0]=$this->getTwo($i);
			$dtSelect[$iCounter][1]=$this->getTwo($i);
			$iCounter++;
		}
		
		return $dtSelect;
	}


	function getParameterUbis($title="Pilih Ubis") {
		global $ora;
		
		if ($title) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]=$title;
			$iCounter=1;
		}

		$qry="select * from p_ubis where ifnull(flag,0)=0 order by `name`";
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["name"];
			$iCounter++;
		}
		
		return $dtSelect;
	}

	function getParameterUbisKhusus($title="Pilih Ubis",$ubiss) {
		global $ora;
		
		if ($title) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]=$title;
			$iCounter=1;
		}

		$qry="select * from p_ubis where ifnull(flag,0)=0 and id in ($ubiss,47) order by `name`";
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["name"];
			$iCounter++;
		}
		
		return $dtSelect;
	}

	function getParameterUbisRAC($title="Pilih Ubis") {
		global $ora;
		
		if ($title) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]=$title;
			$iCounter=1;
		}

		$qry="select * from p_ubis where id in (1,7) order by `name`";
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["name"];
			$iCounter++;
		}
		
		return $dtSelect;
	}

	function getParameterUbisDES($title="Pilih Ubis") {
		global $ora;
		
		if ($title) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]=$title;
			$iCounter=1;
		}

		$qry="select * from p_ubis where id in (1,4,33,34) order by `name`";
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["name"];
			$iCounter++;
		}
		
		return $dtSelect;
	}

	function getParameterUbisDWS($title="Pilih Ubis") {
		global $ora;
		
		if ($title) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]=$title;
			$iCounter=1;
		}

		$qry="select * from p_ubis where id in (1,3,3,34) order by `name`";
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["name"];
			$iCounter++;
		}
		
		return $dtSelect;
	}

	function getParameterUbisSigma($title="Pilih Ubis") {
		global $ora;
		
		if ($title) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]=$title;
			$iCounter=1;
		}

		$qry="select * from p_ubis where id in (31) order by `name`";
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["name"];
			$iCounter++;
		}
		
		return $dtSelect;
	}

	function getParameterUbisTSel($title="Pilih Ubis") {
		global $ora;
		
		if ($title) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]=$title;
			$iCounter=1;
		}

		$qry="select * from p_ubis where id in (43) order by `name`";
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["name"];
			$iCounter++;
		}
		
		return $dtSelect;
	}

	function getParameterUbisMetra($title="Pilih Ubis") {
		global $ora;
		
		if ($title) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]=$title;
			$iCounter=1;
		}

		$qry="select * from p_ubis where id in (44) order by `name`";
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["name"];
			$iCounter++;
		}
		
		return $dtSelect;
	}

	function getParameterUbisTelin($title="Pilih Ubis") {
		global $ora;
		
		if ($title) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]=$title;
			$iCounter=1;
		}

		$qry="select * from p_ubis where id in (45) order by `name`";
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["name"];
			$iCounter++;
		}
		
		return $dtSelect;
	}

	function getParameterUbisMTel($title="Pilih Ubis") {
		global $ora;
		
		if ($title) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]=$title;
			$iCounter=1;
		}

		$qry="select * from p_ubis where id in (46) order by `name`";
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["name"];
			$iCounter++;
		}
		
		return $dtSelect;
	}

	function getParameterUbisAkses($title="Pilih Ubis") {
		global $ora;
		
		if ($title) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]=$title;
			$iCounter=1;
		}

		$qry="select * from p_ubis where id in (24) order by `name`";
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["name"];
			$iCounter++;
		}
		
		return $dtSelect;
	}

	function getParameterUbisRAC1($title="Pilih Ubis") {
		global $ora;
		
		if ($title) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]=$title;
			$iCounter=1;
		}

		$qry="select * from p_ubis where id in (7) order by `name`";
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["name"];
			$iCounter++;
		}
		
		return $dtSelect;
	}

	function getParameterUbisDiva($title="Pilih Ubis") {
		global $ora;
		
		if ($title) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]=$title;
			$iCounter=1;
		}

		$qry="select * from p_ubis where id in (1) order by `name`";
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["name"];
			$iCounter++;
		}
		
		return $dtSelect;
	}
	
	function getUbis($id) {
		global $ora;
		$qry="select * from p_ubis where id=".$id;
		$rsName=$ora->sql_fetch($qry);
		return $rsName->value[1]["name"];
	}

	function getParameterPerangkat($title="Pilih Perangkat") {
		global $ora;
		
		if ($title) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]=$title;
			$iCounter=1;
		}

		$qry="select * from p_perangkat order by `name`";
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["name"];
			$iCounter++;
		}
		
		return $dtSelect;
	}

	function getParameterSpecialCase($title="Pilih Special Case") {
		global $ora;
		
		if ($title) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]=$title;
			$iCounter=1;
		}

		$qry="select * from p_special_case order by `name`";
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["name"];
			$iCounter++;
		}
		
		return $dtSelect;
	}
	
	function getParameterCase($param) {
		global $ora;
		
		if ($param["title"]) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]=$param["title"];
			$iCounter=1;
		}

		$qry="select * from p_case where special_case_id=".$param["id"]." order by `name`";
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["name"];
			$iCounter++;
		}
		
		return $dtSelect;
	}

	function getParameterObjek($param) {
		global $ora;
		
		if ($param["title"]) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]=$param["title"];
			$iCounter=1;
		}

		$qry="select * from p_objek where perangkat_id=".$param["id"]." order by `name`";
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["name"];
			$iCounter++;
		}
		
		return $dtSelect;
	}
	
	function getPerangkat($id) {
		global $ora;
		$qry="select * from p_perangkat where id=".$id;
		$rsName=$ora->sql_fetch($qry);
		return $rsName->value[1]["name"];
	}

	function getSpecialCase($id) {
		global $ora;
		$qry="select * from p_special_case where id=".$id;
		$rsName=$ora->sql_fetch($qry);
		return $rsName->value[1]["name"];
	}

	function getParameterService($title="Pilih Service") {
		global $ora;
		
		if ($title) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]=$title;
			$iCounter=1;
		}

		$qry="select * from p_service order by `name`";
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["name"];
			$iCounter++;
		}
		
		return $dtSelect;
	}

	function getParameterServiceNew($ubis,$title="Pilih Service") {
		global $ora;
		
		if ($title) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]=$title;
			$iCounter=1;
		}

		if($ubis=='4'){
			$qry="select * from p_service where id in ('1','5','7') order by `name`";
		}else{
			$qry="select * from p_service where id not in ('1','5','7') order by `name`";
		}
		
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["name"];
			$iCounter++;
		}
		
		return $dtSelect;
	}

	function getParameterServiceAP($title="Pilih Service") {
		global $ora;
		
		if ($title) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]=$title;
			$iCounter=1;
		}

		$qry="select * from p_service where id not in ('1','2','3','4','5','6','7') order by `name`";
		
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["name"];
			$iCounter++;
		}
		
		return $dtSelect;
	}

	function getParameterJenisLayanan($param) {
		global $ora;
		
		if ($param["title"]) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]=$param["title"];
			$iCounter=1;
		}

		$qry="select * from p_jenis_layanan where service_id=".$param["id"]." order by `name`";
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["name"];
			$iCounter++;
		}
		
		return $dtSelect;
	}


	
	function getService($id) {
		global $ora;
		$qry="select * from p_service where id=".$id;
		$rsName=$ora->sql_fetch($qry);
		return $rsName->value[1]["name"];
	}
	

	function getParameterSubUbis($param) {
		global $ora;

		
		if ($param["title"]) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]=$param["title"];
			$iCounter=1;
		}

		if ($param["all"]==0) {
			$where = "and ifnull(pic,0)=1";
		}

		$qry="select * from p_sub_ubis where ubis_id=".$param["id"]." and ifnull(flag,0)=0 $where order by `name`";
		//echo $qry;
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["name"];
			$iCounter++;
		}
		
		return $dtSelect;
	}
	
	function getSubUbis($id) {
		global $ora;
		$qry="select * from p_sub_ubis where id=".$id;
		$rsName=$ora->sql_fetch($qry);
		return $rsName->value[1]["name"];
	}

	function getParameterLokasi($title="Pilih Divisi") {
		global $ora;
		
		if ($title) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]=$title;
			$iCounter=1;
		}

		$qry="select * from p_lokasi order by `name`";
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["name"];
			$iCounter++;
		}
		
		return $dtSelect;
	}

	function getParameterSubLokasi($param) {
		global $ora;
		
		if ($param["title"]) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]=$param["title"];
			$iCounter=1;
		}

		$qry="select * from p_sub_lokasi where lokasi_id=".$param["id"]." order by `name`";
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["name"];
			$iCounter++;
		}
		
		return $dtSelect;
	}
	
	function getSubLokasi($id) {
		global $ora;
		$qry="select * from p_sub_lokasi where id=".$id;
		$rsName=$ora->sql_fetch($qry);
		return $rsName->value[1]["name"];
	}

	function getParameterStatusTiket($title="Pilih Status") {
		global $ora;
		
		if ($title) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]=$title;
			$iCounter=1;
		}

		$qry="select * from p_status_tiket order by `name`";
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["name"];
			$iCounter++;
		}
		
		return $dtSelect;
	}

	function getParameterStatusPIC($title="Pilih Status") {
		global $ora;
		
		if ($title) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]=$title;
			$iCounter=1;
		}

		$qry="select * from p_status_pic order by `name`";
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["name"];
			$iCounter++;
		}
		
		return $dtSelect;
	}
	
	function getParameterPIC($param) {
		global $ora;
		
		if ($param["title"]) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]=$param["title"];
			$iCounter=1;
		}

		$qry="select * from p_ubis where ubis_id=".$param["ubis_id"]." order by `name`";
		$rs=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["name"];
			$iCounter++;
		}
		
		return $dtSelect;
	}

	function getLokasi($id) {
		global $ora;
		$qry="select * from p_lokasi where id=".$id;
		$rsName=$ora->sql_fetch($qry);
		return $rsName->value[1]["name"];
	}

	function getDataKaryawanRecord($nik) {
		global $ora;
		$qry="select * from m_karyawan where nik='".$nik."'";
		$rs=$ora->sql_fetch($qry);
		return $rs;
	}

	function getJenis($id) {
		global $ora;
		
		if ($id==1)
			return "Perangkat";
		elseif ($id==3)
			return "Special Case";
		else
			return "Service";

	}
	
	function getLayananTerganggu($jenis_id,$perangkat,$objek,$service,$jenis_layanan,$special_case,$case) {
		global $ora;
		if ($jenis_id==1) {
			$qry="select * from p_perangkat where id=".$perangkat;
			$rsName=$ora->sql_fetch($qry);

			$qry="select * from p_objek where id=".$objek;
			$rsSubName=$ora->sql_fetch($qry);
			
			if ($rsSubName->value[1]["name"])
				return $rsName->value[1]["name"]." / ".$rsSubName->value[1]["name"];
			else
				return $rsName->value[1]["name"];
		}
		
		if ($jenis_id==2) {
			$qry="select * from p_service where id=".$service;
			$rsName=$ora->sql_fetch($qry);

			$qry="select * from p_jenis_layanan where id=".$jenis_layanan;
			$rsSubName=$ora->sql_fetch($qry);
			
			if ($rsSubName->value[1]["name"])
				return $rsName->value[1]["name"]." / ".$rsSubName->value[1]["name"];
			else
				return $rsName->value[1]["name"];
		}

		if ($jenis_id==3) {
			$qry="select * from p_special_case where id=".$special_case;
			$rsName=$ora->sql_fetch($qry);

			$qry="select * from p_case where id=".$case;
			$rsSubName=$ora->sql_fetch($qry);
			
			if ($rsSubName->value[1]["name"])
				return $rsName->value[1]["name"]." / ".$rsSubName->value[1]["name"];
			else
				return $rsName->value[1]["name"];
		}
	}
	
	function getDataKaryawan($nik,$withLoker = false) {
		global $ora;
		$qry="select * from m_karyawan where nik='".$nik."'";
		if ($withLoker)
		   $qry = "SELECT k.*,u.name nama_ubis,su.name nama_sububis FROM posko.m_karyawan k left outer join p_ubis u on k.ubis_id = u.id left outer join p_sub_ubis su on k.sub_ubis_id = su.id where nik = '".$nik."'";
		$rsName=$ora->sql_fetch($qry);
		
		if ($rsName->jumrec==0) 
			return "";
		else
		{
			if (!$withLoker)
			return $rsName->value[1]["nama"]." / ".$rsName->value[1]["nik"]; 
			else return $rsName->value[1]["nama"]." / ".$rsName->value[1]["nik"]." / ".$rsName->value[1]["nama_ubis"]." / ".$rsName->value[1]["nama_sububis"];
		}
	}
	
	
	function getStatus($id) {
		global $ora;
		
		$qry="select * from p_status_tiket where id='".$id."'";
		$rs=$ora->sql_fetch($qry);
		return $rs->value[1]["name"];
	}

	

	function getDataJagaRecordByID($id) {
		global $ora;
		
		$qry="select * from m_jaga a where id='".$id."'";
		$rs=$ora->sql_fetch($qry);
		return $rs;
	}
	
	function getDataJagaRecord($nik) {
		global $ora;
		
		if ($param["event_id"]) 
			$where .=" and event_id=".$param["event_id"];
		else
			$where .=" and event_id=".$this->getEventActiveID();

		if ($param["tgl_jaga"]) 
			$where .=" and tgl_jaga='".$param["tgl_jaga"]."'";
		else
			$where .=" and tgl_jaga='".date("Y-m-d")."'";
		
		$qry="select * from m_jaga a where a.nik='".$nik."'";
		$rs=$ora->sql_fetch($qry);
		return $rs;
	}
	
	function selectEvent() {
		global $ora;
		
		$qry="select * from m_event where status=1 order by event";
		$rs=$ora->sql_fetch($qry);
		
		if (isset($param["header"])) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]="Pilih Event";
			$iCounter=1;
		} else
			$iCounter=0;
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["event"];
			$iCounter++;
		}
		
		return $dtSelect;
	}

	function selectEventReport() {
		global $ora;
		
		$qry="select * from m_event where flag=0 order by id desc";
		$rs=$ora->sql_fetch($qry);
		
		$dtSelect[0][0]=0;
		$dtSelect[0][1]="All Event";
		$iCounter=1;
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["event"];
			$iCounter++;
		}
		
		return $dtSelect;
	}

	function selectAllEvent($param) {
		global $ora;
		
		$qry="select * from m_event where ifnull(flag,0)=0 order by event";
		$rs=$ora->sql_fetch($qry);
		
		if (isset($param["header"])) {
			$dtSelect[0][0]=0;
			$dtSelect[0][1]="All Event";
			$iCounter=1;
		} else
			$iCounter=0;
		
		for ($i=1;$i<=$rs->jumrec;$i++) {
			$dtSelect[$iCounter][0]=$rs->value[$i]["id"];
			$dtSelect[$iCounter][1]=$rs->value[$i]["event"];
			$iCounter++;
		}
		
		return $dtSelect;
	}

	function getEventActive() {
		global $ora;
		
		$qry="select * from m_event where status=1";
		$rs=$ora->sql_fetch($qry);
		
		return $rs->value[1]["event"];
	}
	
	function getEventActiveID() {
		global $ora;
		
		$qry="select * from m_event where status=1";
		$rs=$ora->sql_fetch($qry);
		
		return $rs->value[1]["id"];
	}
	
	function getEvent($id) {
		global $ora;
		
		$qry="select * from m_event where id=".$id;
		$rs=$ora->sql_fetch($qry);
		
		return $rs->value[1]["event"];
	}

	function getShift($id) {
		global $ora;
		
		switch ($id) {
		case 1 : 
			return "PAGI";
			break;
		case 2 : 
			return "MALAM";
			break;
		}
	}

	function send_email($tipe,$info) {
		global $ora;
		global $bit_app;
		
		$from ="admin_posko_2013@telkom.co.id";
		$to=$to."@telkom.co.id";
		switch ($tipe) {
		case "gangguan" :
			$subject="Laporan Gangguan";
			$content=$info["content"];
			break;
		}
		$to=$info["to"];
		$from="admin_posko_2013@telkom.co.id";

		#Debug to Database
		$qry="insert into d_email values('$from','$to','$subject','$content',sysdate())";
		$ora->sql_no_fetch($qry);
		
		$headers = "From: $from <$from>\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
		if ($bit_app["email"])	  
			MailSend($to,$subject,$content,$from,"Posko Always On 2013","","");
	}



	function listadmin($item) {
		
		$arrList=array(
				"Ubis"=>"admin_ubis",
				"Sub Ubis"=>"admin_sub_ubis",
				"User"=>"admin_user",
				"Perangkat"=>"admin_perangkat",
				"Objek"=>"admin_objek",
				"Service"=>"admin_service",
				"Layanan"=>"admin_jenis_layanan",
				"Posko"=>"admin_posko",
		);
		
		while (list($k,$v)=each($arrList)) {
				if ($item==$v)
					$tmp .='<a href="?app_parent='.md5("admin").'&app='.md5($v).'" class="bit_a_process_on">'.$k.'</a>';
				else
					$tmp .='<a href="?app_parent='.md5("admin").'&app='.md5($v).'" class="bit_a_process">'.$k.'</a>';
		}		
			
		return '<tr><th  class="bit_th_10" colspan="2" align="left">'.$tmp.'</th></tr>';
	}
	

	function listadmin2($item) {
		
		$arrList=array(
				"Divisi"=>"admin_lokasi",
				"Witel"=>"admin_sub_lokasi",
				"Special Case"=>"admin_special_case",
				"Case"=>"admin_case",
				"Treshold"=>"admin_treshold",
				"Tiket"=>"admin_ac",
			    "Wifi"=>"admin_wifi",
				"Jadual Posko"=>"admin_jadual",
				#"St. Tiket"=>"admin_status_tiket",
				#"St. PIC"=>"admin_status_pic"
				);
		
		while (list($k,$v)=each($arrList)) {
				if ($item==$v)
					$tmp .='<a href="?app_parent='.md5("admin").'&app='.md5($v).'" class="bit_a_process_on">'.$k.'</a>';
				else
					$tmp .='<a href="?app_parent='.md5("admin").'&app='.md5($v).'" class="bit_a_process">'.$k.'</a>';
		}		
			
		return '<tr><th  class="bit_th_10" colspan="2" align="left">'.$tmp.'</th></tr>';
	}


	function listadministelkom($item) {
		
		$arrList=array(
				"User"=>"admin_user",
				#"St. Tiket"=>"admin_status_tiket",
				#"St. PIC"=>"admin_status_pic"
				);
		
		while (list($k,$v)=each($arrList)) {
				if ($item==$v)
					$tmp .='<a href="?app_parent='.md5("admin").'&app='.md5($v).'" class="bit_a_process_on">'.$k.'</a>';
				else
					$tmp .='<a href="?app_parent='.md5("admin").'&app='.md5($v).'" class="bit_a_process">'.$k.'</a>';
		}		
			
		return '<tr><th  class="bit_th_10" colspan="2" align="left">'.$tmp.'</th></tr>';
	}
	
	function getMenit($date1,$date2) {
		

		$diff = abs(strtotime($date2) - strtotime($date1)); 


		$years   = floor($diff / (365*60*60*24)); 
		$months  = floor(($diff - $years * 365*60*60*24) / (30*60*60*24)); 
		$days    = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

		$hours   = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60)); 

		$minuts  = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60); 

		$seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60)); 

		return $minuts;

	}
	function listreport($item) {
		$arrList=array(
			"Tabel Gangguan"=>"report_gangguan",
			"Grafik Gangguan"=>"rekap"
			);
		
		while (list($k,$v)=each($arrList)) {
				if ($item==$v)
					$tmp .='<a href="?app_parent='.md5("report").'&app='.md5($v).'" class="bit_a_process_on">'.$k.'</a>';
				else
					$tmp .='<a href="?app_parent='.md5("report").'&app='.md5($v).'" class="bit_a_process">'.$k.'</a>';
		}		
			
		return '<tr><th  class="bit_th_10" colspan="2" align="left">'.$tmp.'</th></tr>';
	}

function listlink($item) {
		$arrList=array(
			"Arium"=>"arium",
			"Wifi ID"=>"wifiid",
			"VisCor"=>"viscor",
			"T3 Online"=>"t3ol",
			"TeNOSS"=>"tenos"
			);
		
		while (list($k,$v)=each($arrList)) {
				if ($item==$v)
					$tmp .='<a href="?app_parent='.md5("report").'&app='.md5($v).'" class="bit_a_process_on">'.$k.'</a>';
				else
					$tmp .='<a href="?app_parent='.md5("report").'&app='.md5($v).'" class="bit_a_process">'.$k.'</a>';
		}		
			
		return '<tr><th  class="bit_th_10" colspan="2" align="left">'.$tmp.'</th></tr>';
	}


	function default_proses() {
		global $ora;
		
		$arrModuleID=array(
				1=>"Dispatch",
				3=>"PIC",
				5=>"Closing Gangguan"
			);
		
		$arrModuleMenu=array(
				1=>"dispatch",
				3=>"pic",
				5=>"closing"
		);
		
		$qry="select * from c_workflow_module";
		$rsC=$ora->sql_fetch($qry);
		
		/*
		if ($rsC->jumrec && $this->getSessionProfileID()==1 && $this->getSessionUserUbis()==1 && $this->getSessionUserSubUbis()==1) 
			return $arrModuleMenu[100];
		*/

		$qry="select module_id,count(1) from c_workflow_module where wf_to='".$this->getSessionUserUbis()."' and  id1='".$this->getSessionUserSubUbis()."' group by module_id order by module_id desc";
		$rsInbox=$ora->sql_fetch($qry);
		
		return $arrModuleMenu[$rsInbox->value[1][1]];
	}

	function listproses($item) {
		global $ora;
		
		$arrModuleID=array(
				1=>"Dispatch",
				3=>"PIC",
				5=>"Closing Gangguan"
			);
		
		$arrModuleMenu=array(
				1=>"dispatch",
				3=>"pic",
				5=>"closing"
		);
				
		$qry="select module_id,count(1) from c_workflow_module where wf_to='".$this->getSessionUserUbis()."' and  id1='".$this->getSessionUserSubUbis()."' group by module_id";
		$rsInbox=$ora->sql_fetch($qry);
		
		for ($i=1;$i<=$rsInbox->jumrec;$i++) {
			$arrList[$arrModuleID[$rsInbox->value[$i][1]]]=$arrModuleMenu[$rsInbox->value[$i][1]];
		}
		
		/*
		$qry="select * from c_workflow_module";
		$rsC=$ora->sql_fetch($qry);
		
		if ($rsC->jumrec && $this->getSessionProfileID()==1 && $this->getSessionUserUbis()==1 && $this->getSessionUserSubUbis()==1) {
			if ($item=="admin_ac")
				$tmp .='<a href="?app_parent='.md5("proses").'&app='.md5("admin_ac").'" class="bit_a_process_on">Admin AC</a>';
			else
				$tmp .='<a href="?app_parent='.md5("proses").'&app='.md5("admin_ac").'" class="bit_a_process">Admin AC</a>';
		}
		*/

		if ($rsInbox->jumrec) {
			while (list($k,$v)=each($arrList)) {
				if ($item==$v)
					$tmp .='<a href="?app_parent='.md5("proses").'&app='.md5($v).'" class="bit_a_process_on">'.$k.'</a>';
				else
					$tmp .='<a href="?app_parent='.md5("proses").'&app='.md5($v).'" class="bit_a_process">'.$k.'</a>';
			}	
		}
			
		return '<tr><th  class="bit_th_10" colspan="2" align="left">'.$tmp.'</th></tr>';
	}

	function getGangguan($id) {
		global $ora;
		$qry="select * from m_gangguan where id='".$id."'";
		$rs=$ora->sql_fetch($qry);
		return $rs;
	}

	function convert_time($s) {
		if ($s=="")
			return "-";
		/* untuk forman xx jam yy menit
		$m=$s / 60;
		$h=$m / 60;
		$m=$m % 60;
		$t=floor($h)." Jam";
		$t .=" ".floor($m)." menit";
		*/
		$m=$s / 60;
		$h=$m / 60;
		$m=$m % 60;
		$t=number_format($h,2)." Jam";

		return $t;
	}

	function getParameterShift() {
		global $ora;
		
		$iCounter=0;
		$dtSelect[0][0]=1;
		$dtSelect[0][1]="PAGI";
		
		$dtSelect[1][0]=2;
		$dtSelect[1][1]="MALAM";
		
		return $dtSelect;
	}
}	
?>
