<?php
class CMS_Functions
{
    

function index_warna($nilai)
{
    $nilai = $nilai * 100;    
    $idx = -1;
    if ($nilai < 90)
        $idx = 1;
    elseif ($nilai >= 90 and $nilai < 97)
        $idx = 3;
    elseif ($nilai >= 97 and $nilai < 100)
        $idx = 2;
    elseif ($nilai >= 100)
        $idx = 0;
    
    return $idx;
}

 function dd_html($name,$options=array(), $label='', $terpilih = -1)
 {
    $html="";
    $html = '<label for="'.$name.'">'.$label.'</label>'; 
    $html .= '<select style="font-family: TitilliumWeb-Regular;" class="'.$class.'" data-mini="true" data-theme="c" name="'.$name.'" id="'.$name.'">'."\n";
    #$selected = $terpilih == -1 ? ' selected="selected" ' : ' ';
    #$html .= "<option ".$selected."  value='-1'>-- Pilih salah satu --</option>\n";
    foreach($options as $val => $label)
    {
        $selected = $terpilih == $val ? ' selected="selected" ' : ' ';
        $html .= "<option ".$selected."  value='" . $val . "'>" . $label . "</option>\n";
    }
    $html .= '</select>';
    return $html;
}

 function dd_html_II($name,$options=array(), $terpilih=-1,$class='dd_s1',$style='width: 150px;')
 {
    $html="";
    $html = '<select class="'.$class.'" style="'.$style.'" name="'.$name.'" id="'.$name.'">'."\n";
    #$selected = $terpilih == -1 ? ' selected="selected" ' : ' ';
    #$html .= "<option ".$selected."  value='-1'>-- Silahkan Pilih --</option>\n";
    foreach($options as $val => $label)
    {
        $selected = $terpilih == $val ? ' selected="selected" ' : ' ';
        $html .= "<option ".$selected."  value='" . $val . "'>" . $label . "</option>\n";
    }
    $html .= '</select>';
    return $html;
}   

function get_ap_in_location()
{
    $qap = "select * from ".$table_prefix."p_map_event_nms where event_id = ".$max_eventid." and loc_id = ".$loc_id;
    $res_ap = mysql_query($qap);
    
}

function get_ont_status($conn,$db,$table_prefix,$loc_id = -1)
{
    $q = "select a.loc_id,a.loc_area,b.id_perangkat,d.current_state as status,b.olt_node,b.olt_port,b.nama_pelanggan,b.ont_nama,c.group_loc_name 
from ".$table_prefix."list_location a left join ".$table_prefix."ont_devices b on a.loc_id = b.loc_id 
left join ".$table_prefix."list_group_location c on  b.group_loc_id=c.group_loc_id left join v_status d on b.ont_ip_address = d.address"; 

    if ($loc_id != -1)
    {
        $q .= " where a.loc_id = ".$loc_id;
    }
    $q .= " order by a.loc_id, c.group_loc_name ";
    
    #print_out($q);
    
    $res = mysql_query($q,$conn);
    $rows = array();
    while ($row = mysql_fetch_assoc($res))
    {
        $rows[] = $row;
    }    
    return $rows;
}

function summary_table_all($title,$data_switch,$data_ap,$data_ont,$table_prefix,$eventid,$db,$conn,$complete_table = true)
{
    if ($complete_table)
    {
            $html = '<table class="tlist_dashboard fl" width="100%">
            <tr>
                <td colspan="8" class="tc kolomjudul">'.ucwords($title).'</td>
            </tr>                      
            <tr>
                <td class="kolomjudul tc">No.</td>
                <td class="kolomjudul">Location</td>
                <td class="tc"><img title="Access Point UP" src="assets/images/wifi-up.png" style="height: 16px;" class="img_wifi" /></td>
                <td class="tc"><img title="Access Point Down" src="assets/images/wifi-down.png" style="height: 16px;" class="img_wifi" /></td>
                <td class="tc">Tot</td>';
            if ($title == 'Reguler')
            {
             $html.='
                <td class="tc"><img title="Switch UP" src="assets/images/switch-up.png" style="height: 16px;" class="img_wifi" /><br />O | A</td>
                <td class="tc"><img title="Switch Down" src="assets/images/switch-down.png" style="height: 16px;" class="img_wifi" /><br />O | A</td>';
            }
            else
            {
             $html.='
                <td class="tc"><img title="Switch UP" src="assets/images/switch-up.png" style="height: 16px;" class="img_wifi" /><br />O | C | A</td>
                <td class="tc"><img title="Switch Down" src="assets/images/switch-down.png" style="height: 16px;" class="img_wifi" /><br />O | C | A</td>';
            }
            $html.='
                <td class="tc">Tot</td>
            </tr>';
    } else { $html = ''; }
            
            $ttotal = $tt_up = $tt_down = $ttotal_switch_cr = $ttotal_switch_acc = 0;
            $tt_up_switch_cr = $tt_up_switch_acc = $tt_down_switch_cr = $tt_down_switch_acc = 0;
            $ttotal_ce = $ttotal_up_ce = $ttotal_down_ce = 0;
            $ttotal_olt = $ttotal_up_olt = $ttotal_down_olt = 0;
            $ttotal_ont = $ttotal_up_ont = $ttotal_down_ont = 0;
            $no = 1;

            foreach($data_switch as $hotel => $lokasi) {
                $alias = "";
                $t_down_switch_cr = $t_down_switch_acc = $t_up_switch_cr = $t_up_switch_acc = $t_up = $t_down = 0;
                $t_down_olt = $t_up_olt = 0;
                $t_down_ce = $t_up_ce = 0;
                $t_down_ont = $t_up_ont = 0;
                
                $loc_id = $lokasi[0]['loc_id'];
                $t_up = array_key_exists($hotel,$data_ap) ? @$data_ap[$hotel]['UP'] : 0;
                $t_down = array_key_exists($hotel,$data_ap) ? @$data_ap[$hotel]['DOWN'] : 0;
                
                $t_up_ont = array_key_exists($hotel,$data_ont) ? @$data_ont[$hotel]['UP'] : 0;
                $t_down_ont = array_key_exists($hotel,$data_ont) ? @$data_ont[$hotel]['DOWN'] : 0;

                foreach($lokasi as $row)
                {
                    $alias = $row['alias'];
                    $switchs[] = $row['ip_address'];
                    if ($row['TIPE_SW'] == 'COR')
                    {
                        if ($row['current_state'] == '')
                            $t_down_switch_cr++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_switch_cr++;
                        elseif ($row['current_state'] == 1)
                            $t_down_switch_cr++;
                        
                    }
                    elseif ($row['TIPE_SW'] == 'ACC' || $row['TIPE_SW'] == '8AC')
                    {
                        if ($row['current_state'] == '')
                            $t_down_switch_acc++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_switch_acc++;
                        elseif ($row['current_state'] == 1)
                            $t_down_switch_acc++;
                        
                    }
                    elseif ($row['TIPE_SW'] == 'OLT')
                    {
                        if ($row['current_state'] == '')
                            $t_down_olt++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_olt++;
                        elseif ($row['current_state'] == 1)
                            $t_down_olt++;
                        
                    }
                    elseif ($row['TIPE_SW'] == 'CE-')
                    {
                        if ($row['current_state'] == '')
                            $t_down_ce++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_ce++;
                        elseif ($row['current_state'] == 1)
                            $t_down_ce++;
                        
                    }
                }
                
                $total = $t_up + $t_down;
                $link_t_down = number_format($t_down,0);
                $link_param = $BASEURL.'index.php?event_id='.$eventid.'&loc_name='.$hotel.'&loc_id='.$loc_id;
                if ( ($t_down > 0)) 
                {
                    $warna = ' merah';
                    $fclass = ' fmerah'; 
                    $trclass = 'highlight';
                    $link_t_down = '<span style="color:red;">'.number_format($t_down,0).'</span>';
                    
                }
                elseif ( ($t_up == 0) and ($t_down == 0))
                { 
                    $warna = ' abu';
                    $fclass = ' fputih'; 
                    $trclass = '';
                }
                elseif ( ($t_up > 0) and ($t_down == 0))
                { 
                    $warna = ' hijau';
                    $fclass = ' fhijau'; 
                    $trclass = '';
                }
                $fclass_up = $fclass_down = ' style="color: #fff;" ';
                
                if ($t_up > 0)
                    $fclass_up = ' style="color: #82D711;" ';
                if ($t_down > 0)
                    $fclass_down = ' style="color: #FF0000;" ';
                

                $total_switch_cr = $t_up_switch_cr + $t_down_switch_cr;
                $total_switch_acc = $t_up_switch_acc + $t_down_switch_acc;
                $link_t_down_switch_cr = number_format($t_down_switch_cr,0);
                $link_t_down_switch_acc = number_format($t_down_switch_acc,0);
                
                /** STYLING SWITCH COUNT STATISTIC */
                /** SWITCH CORE ========================= */
                if ( ($t_down_switch_cr > 0)) 
                {
                    $warna_switch_cr = ' merah';
                    $trclass = 'highlight';
                    #$link_t_down_switch_cr = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-1-switch" style="color:red;" class="link_switch">'.number_format($t_down_switch_cr,0).'</a>';
                    $link_t_down_switch_cr = '<span style="color:red;">'.number_format($t_down_switch_cr,0).'</span>';
                    
                }
                elseif ( ($t_up_switch_cr == 0) and ($t_down_switch_cr == 0))
                { 
                    $warna_switch_cr = ' abu';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_cr > 0) and ($t_down_switch_cr == 0))
                { 
                    $warna_switch_cr = ' hijau';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_cr > 0))
                {
                    //$trclass = '';
                }
                
                $fclass_switch_up_cr = $fclass_switch_down_cr = ' style="color: #fff;" ';
                $link_t_up_switch_cr = number_format($t_up_switch_cr,0);
                
                if ($t_up_switch_cr > 0) {
                    $fclass_switch_up_cr = ' style="color: #82D711;" ';
                    $link_t_up_switch_cr = '<span style="color:#82D711;" class="link_switch">'.number_format($t_up_switch_cr,0).'</span>';
                    }
                if ($t_down_switch_cr > 0)
                    $fclass_switch_down_cr = ' style="color: #FF0000;" ';
                
                $link_total_switch_cr = '<span style="color:#FFF;" class="link_switch">'.number_format($total_switch_cr,0).'</span>';              
            

                /** SWITCH ACCESS ========================= */
                if ( ($t_down_switch_acc > 0)) 
                {
                    $warna_switch_acc = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_switch_acc = '<span style="color:red;" class="link_switch">'.number_format($t_down_switch_acc,0).'</span>';
                    
                }
                elseif ( ($t_up_switch_acc == 0) and ($t_down_switch_acc == 0))
                { 
                    $warna_switch_acc = ' abu';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_acc > 0) and ($t_down_switch_acc == 0))
                { 
                    $warna_switch_acc = ' hijau';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_acc > 0))
                {
                    //$trclass = '';
                }
                
                $fclass_switch_up_acc = $fclass_switch_down_acc = ' style="color: #fff;" ';
                $link_t_up_switch_acc = number_format($t_up_switch_acc,0);
                
                if ($t_up_switch_acc > 0) {
                    $fclass_switch_up_acc = ' style="color: #82D711;" ';
                    $link_t_up_switch_acc = '<span style="color:#82D711;" class="link_switch">'.number_format($t_up_switch_acc,0).'</span>';
                    }
                if ($t_down_switch_acc > 0)
                    $fclass_switch_down_acc = ' style="color: #FF0000;" ';
                
                $link_total_switch_acc = '<span style="color:#FFF;" class="link_switch">'.number_format($total_switch_acc,0).'</span>';              

                
                /** OLT ========================= */
                $total_olt = $t_up_olt + $t_down_olt;
                $link_t_down_olt = number_format($t_down_olt,0);
                if ( ($t_down_olt > 0)) 
                {
                    $warna_olt = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_olt = '<span style="color:red;" class="link_olt">'.number_format($t_down_olt,0).'</span>';
                    
                }
                elseif ( ($t_up_olt == 0) and ($t_down_olt == 0))
                { 
                    $warna_olt = ' abu';
                    #$trclass = '';
                }
                elseif ( ($t_up_olt > 0) and ($t_down_olt == 0))
                { 
                    $warna_olt = ' hijau';
                    #$trclass = '';
                }
                elseif ( ($t_up_olt > 0))
                {
                    #$trclass = '';
                }
                
                $fclass_olt_up = $fclass_olt_down = ' style="color: #fff;" ';
                $link_t_up_olt = number_format($t_up_olt,0);
                
                if ($t_up_olt > 0) {
                    $fclass_olt_up = ' style="color: #82D711;" ';
                    $link_t_up_olt = '<span style="color:#82D711;" class="link_olt">'.number_format($t_up_olt,0).'</span>';
                    }
                if ($t_down_olt > 0)
                    $fclass_olt_down = ' style="color: #FF0000;" ';
                $link_total_olt = '<span style="color:#FFF;" class="link_olt">'.number_format($total_olt,0).'</span>';              
                

                /** CE ========================= */
                $total_ce = $t_up_ce + $t_down_ce;
                $link_t_down_ce = number_format($t_down_ce,0);
                if ( ($t_down_ce > 0)) 
                {
                    $warna_ce = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_ce = '<span style="color:red;" class="link_ce">'.number_format($t_down_ce,0).'</span>';
                    
                }
                elseif ( ($t_up_ce == 0) and ($t_down_ce == 0))
                { 
                    $warna_ce = ' abu';
                    #$trclass = '';
                }
                elseif ( ($t_up_ce > 0) and ($t_down_ce == 0))
                { 
                    $warna_ce = ' hijau';
                    #$trclass = '';
                }
                elseif ( ($t_up_ce > 0))
                {
                    #$trclass = '';
                }
                
                $fclass_ce_up = $fclass_ce_down = ' style="color: #fff;" ';
                $link_t_up_ce = number_format($t_up_ce,0);
                
                if ($t_up_ce > 0) {
                    $fclass_ce_up = ' style="color: #82D711;" ';
                    $link_t_up_ce = '<span style="color:#82D711;" class="link_ce">'.number_format($t_up_ce,0).'</span>';
                    }
                if ($t_down_ce > 0)
                    $fclass_ce_down = ' style="color: #FF0000;" ';
                $link_total_ce = '<span style="color:#FFF;" class="link_ce">'.number_format($total_ce,0).'</span>';              
                /** ================================= */

                /** ONT ========================= */
                $total_ont = $t_up_ont + $t_down_ont;
                $link_t_down_ont = number_format($t_down_ont,0);
                if ( ($t_down_ont > 0)) 
                {
                    $trclass = 'highlight';
                    $link_t_down_ont = '<span style="color:red;" class="link_ont">'.number_format($t_down_ont,0).'</span>';
                    
                }
                
                $fclass_ont_up = $fclass_ont_down = ' style="color: #fff;" ';
                $link_t_up_ont = number_format($t_up_ont,0);
                
                if ($t_up_ont > 0) {
                    $fclass_ont_up = ' style="color: #82D711;" ';
                    $link_t_up_ont = '<span style="color:#82D711;" class="link_ce">'.number_format($t_up_ont,0).'</span>';
                    }
                if ($t_down_ont > 0)
                    $fclass_ont_down = ' style="color: #FF0000;" ';
                $link_total_ont = '<span style="color:#FFF;" class="link_ce">'.number_format($total_ont,0).'</span>';              
                /** ================================= */


                //$link_t_down = '<a href="'.$link_param.'" target="_blank" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-ap" style="color:red;" class="link_ap">'.number_format($t_down,0).'</a>';

                $html .= '<tr class="link_ap '.$trclass.'" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'">';                
                $html .= '<td class="tc">'.$no.'</td>';                
                $html .= '<td class="tl">'.strtoupper($alias).'</td>';                
                
                if (strtoupper($title) == 'TELKOM OFFICE')
                {
                    $html .= '<td class="tr"'.$fclass_ce_up.'>'.$link_t_up_ce.'</td>';                
                    $html .= '<td class="tr"'.$fclass_ce_down.'>'.$link_t_down_ce.'</td>';                
                    $html .= '<td class="tr">'.$link_total_ce.'</td>';                                    
                    $html .= '<td class="tr"'.$fclass_olt_up.'>'.$link_t_up_olt.'</td>';                
                    $html .= '<td class="tr"'.$fclass_olt_down.'>'.$link_t_down_olt.'</td>';                
                    $html .= '<td class="tr">'.$link_total_olt.'</td>';                                    
                }
                else
                {
                    $html .= '<td class="tr"'.$fclass_up.'>'.number_format($t_up,0).'</td>';                
                    $html .= '<td class="tr"'.$fclass_down.'>'.$link_t_down.'</td>';                
                    $html .= '<td class="tr">'.number_format($total,0).'</td>';       
                    if (ucwords($title) == 'Reguler')
                    {
                    $html .= '<td class="tr"'.$fclass_switch_up.'>'.$link_t_up_ont.' | '.$link_t_up_switch_acc.'</td>';                
                    $html .= '<td class="tr"'.$fclass_switch_down.'>'.$link_t_down_ont.' | '.$link_t_down_switch_acc.'</td>';                
                    $html .= '<td class="tr">'.$link_total_ont.' | '.$link_total_switch_acc.'</td>';                
                    } else {
                    $html .= '<td class="tr"'.$fclass_switch_up.'>'.$link_t_up_ont.' | '.$link_t_up_switch_cr.' | '.$link_t_up_switch_acc.'</td>';                
                    $html .= '<td class="tr"'.$fclass_switch_down.'>'.$link_t_down_ont.' | '.$link_t_down_switch_cr.' | '.$link_t_down_switch_acc.'</td>';                
                    $html .= '<td class="tr">'.$link_total_ont.' | '.$link_total_switch_cr.' | '.$link_total_switch_acc.'</td>';                
                    }
                    
                    
                }
                $html .= '</tr>';   
                $no++;
                $tt_up += $t_up; 
                $tt_down += $t_down; 
                $ttotal += $total; 
                $tt_up_switch_cr += $t_up_switch_cr; 
                $tt_down_switch_cr += $t_down_switch_cr; 
                $ttotal_switch_cr += $total_switch_cr; 
                $tt_up_switch_acc += $t_up_switch_acc; 
                $tt_down_switch_acc += $t_down_switch_acc; 
                $ttotal_switch_acc += $total_switch_acc; 
                $tt_up_olt += $t_up_olt; 
                $tt_down_olt += $t_down_olt; 
                $ttotal_olt += $total_olt; 
                $tt_up_ce += $t_up_ce; 
                $tt_down_ce += $t_down_ce; 
                $ttotal_ce += $total_ce; 
                $tt_up_ont += $t_up_ont; 
                $tt_down_ont += $t_down_ont; 
                $ttotal_ont += $total_ont; 
                
            } 
            
                $html .= '<tr class="no-border">';                
                $html .= '<td colspan="2" class="no-border">&nbsp;</td>';                

                if (strtoupper($title) == 'TELKOM OFFICE')
                {
                                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.$tt_up_ce.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.$tt_down_ce.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.$ttotal_ce.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.$tt_up_olt.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.$tt_down_olt.'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.$ttotal_olt.'</td>';                
                }
                else
                {
                                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.number_format($tt_up,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.number_format($tt_down,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.number_format($ttotal,0).'</td>';
                    if (ucwords($title) == 'Reguler')
                    {
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_up_ont,0).' | '.number_format($tt_up_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_down_ont,0).' | '.number_format($tt_down_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.number_format($ttotal_ont,0).' | '.number_format($ttotal_switch_acc,0).'</td>';                
                    } else {
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_up_ont,0).' | '.number_format($tt_up_switch_cr,0).' | '.number_format($tt_up_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_down_ont,0).' | '.number_format($tt_down_switch_cr,0).' | '.number_format($tt_down_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.number_format($ttotal_ont,0).' | '.number_format($ttotal_switch_cr,0).' | '.number_format($ttotal_switch_acc,0).'</td>';                
                    }
                    
                    #$html .= '<td class="tr '.$fclass.' alternate_black">'.number_format($ttotal_ont,0).' | '.number_format($ttotal_switch_cr,0).' | '.number_format($ttotal_switch_acc,0).'</td>';                
                    
                }
                    $html .= '</tr>';
                   
    if ($complete_table)
    {
            $html .= '</table>';
    }   
                $res = array();
                $res['total_sw_up_cr'] = $tt_up_switch_cr;
                $res['total_sw_down_cr'] = $tt_down_switch_cr;
                $res['total_sw_up_acc'] = $tt_up_switch_acc;
                $res['total_sw_down_acc'] = $tt_down_switch_acc;
                $res['total_ap_up'] = @$tt_up_ap;
                $res['total_ap_down'] = @$tt_down_ap;
                $res['total_olt_up'] = @$tt_up_olt;
                $res['total_olt_down'] = @$tt_down_olt;
                $res['total_ont_up'] = @$tt_up_ont;
                $res['total_ont_down'] = @$tt_down_ont;
                $res['html'] = $html;
                return $res;
    
}

function summary_table_sto($title,$conn,$complete_table = true)
{
        $rows_sto = $jenis = array();
        $arr_jenis = array(
        'TERRA ROUTER' => 'T',
        'PROVIDER EDGE' => 'PE',
        'METRO' => 'ME',
        'CUSTOMER EDGE' => 'CE',
        'WIRELESS ACCESS CONTROL' => 'WAC',
        );
        $q_sto = "SELECT c.alias,b.monitor,a.device_type,count(*) jml FROM
x_infra_devices a LEFT OUTER JOIN infra_host b ON (a.hostname = b.hostname)
INNER JOIN x_list_location c ON (a.loc_id = c.loc_id)
GROUP BY c.alias,b.monitor,a.device_type
order by alias, device_type
";
        $res_sto = mysql_query($q_sto,$conn);
        $i = 0;
        while($row = mysql_fetch_assoc($res_sto))
        {          
            $tipe = $arr_jenis[$row['device_type']];  
            $status = $row['monitor'] == 'on' ? 'up' : 'down'; 
            $jenis[$tipe] = $tipe;
            $rows_sto[$row['alias']][$tipe][$status] = $row['jml'];
            $i++;
        }
        #print_out($jenis);
        #print_out($rows_sto);
        $table_sto = "";
        $no = 1;
        foreach($rows_sto as $lokasi => $row)
        {
        $total = 0;
        $t_up = $t_down = 0;    
        $table_sto .= '<tr><td>'.$no.'</td><td>'.$lokasi.'</td>';
            foreach($arr_jenis as $long => $short)
            {
                $up = array_key_exists('up',$rows_sto[$lokasi][$short]) ? $rows_sto[$lokasi][$short]['up'] : 0;
                $down = array_key_exists('down',$rows_sto[$lokasi][$short]) ? $rows_sto[$lokasi][$short]['down'] : 0;
                #print_out($rows_sto[$lokasi][$short]);
                $table_sto .= '<td class="tr"><span style="color: green;">'.$up.'</span> | <span style="color: red;">'.$down.'</span></td>';
                $t_up += $up;
                $t_down += $down;
            }
                $table_sto .= '<td class="tr">'.$t_up." | ".$t_down.'</td>';
        $table_sto .= '</tr>';
        $no++;
        }    
}

function summary_table_reguler($title,$arr_lokasi = array(),$data_switch,$data_ap,$data_ont,$table_prefix,$eventid,$db,$conn, $complete_table = true)
{
    if ($complete_table)
    {
            $html = '<table class="tlist_dashboard fl" width="100%">
            <tr>
                <td colspan="8" class="tc kolomjudul">'.ucwords($title).'</td>
            </tr>                      
            <tr>
                <td class="kolomjudul tc">No.</td>
                <td class="kolomjudul">Location</td>
                <td class="tc"><img title="Access Point UP" src="assets/images/wifi-up.png" style="height: 16px;" class="img_wifi" /></td>
                <td class="tc"><img title="Access Point Down" src="assets/images/wifi-down.png" style="height: 16px;" class="img_wifi" /></td>
                <td class="tc">Tot</td>';
            if ($title == 'Reguler')
            {
             $html.='
                <td class="tc"><img title="Switch UP" src="assets/images/switch-up.png" style="height: 16px;" class="img_wifi" /><br />O | A</td>
                <td class="tc"><img title="Switch Down" src="assets/images/switch-down.png" style="height: 16px;" class="img_wifi" /><br />O | A</td>';
            }
            else
            {
             $html.='
                <td class="tc"><img title="Switch UP" src="assets/images/switch-up.png" style="height: 16px;" class="img_wifi" /><br />O | C | A</td>
                <td class="tc"><img title="Switch Down" src="assets/images/switch-down.png" style="height: 16px;" class="img_wifi" /><br />O | C | A</td>';
            }
            $html.='
                <td class="tc">Tot</td>
            </tr>';
    } else { $html = ''; }
            
            $ttotal = $tt_up = $tt_down = $ttotal_switch_cr = $ttotal_switch_acc = 0;
            $tt_up_switch_cr = $tt_up_switch_acc = $tt_down_switch_cr = $tt_down_switch_acc = 0;
            $ttotal_ce = $ttotal_up_ce = $ttotal_down_ce = 0;
            $ttotal_olt = $ttotal_up_olt = $ttotal_down_olt = 0;
            $ttotal_ont = $ttotal_up_ont = $ttotal_down_ont = 0;
            $no = 1;

            foreach($arr_lokasi as $hotel => $rows) {
                $alias = "";
                $t_down_switch_cr = $t_down_switch_acc = $t_up_switch_cr = $t_up_switch_acc = $t_up = $t_down = 0;
                $t_down_olt = $t_up_olt = 0;
                $t_down_ce = $t_up_ce = 0;
                $t_down_ont = $t_up_ont = 0;
                
                $loc_id = $rows[0]['loc_id'];
                $t_up = array_key_exists($hotel,$data_ap) ? @$data_ap[$hotel]['UP'] : 0;
                $t_down = array_key_exists($hotel,$data_ap) ? @$data_ap[$hotel]['DOWN'] : 0;
                
                $t_up_ont = array_key_exists($hotel,$data_ont) ? @$data_ont[$hotel]['UP'] : 0;
                $t_down_ont = array_key_exists($hotel,$data_ont) ? @$data_ont[$hotel]['DOWN'] : 0;

                foreach($rows as $row)
                {
                    $alias = $row['alias'];
                    $switchs[] = $row['ip_address'];
                    if ($row['TIPE_SW'] == 'COR')
                    {
                        if ($row['current_state'] == '')
                            $t_down_switch_cr++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_switch_cr++;
                        elseif ($row['current_state'] == 1)
                            $t_down_switch_cr++;
                        
                    }
                    elseif ($row['TIPE_SW'] == 'ACC' || $row['TIPE_SW'] == '8AC')
                    {
                        if ($row['current_state'] == '')
                            $t_down_switch_acc++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_switch_acc++;
                        elseif ($row['current_state'] == 1)
                            $t_down_switch_acc++;
                        
                    }
                    elseif ($row['TIPE_SW'] == 'OLT')
                    {
                        if ($row['current_state'] == '')
                            $t_down_olt++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_olt++;
                        elseif ($row['current_state'] == 1)
                            $t_down_olt++;
                        
                    }
                    elseif ($row['TIPE_SW'] == 'CE-')
                    {
                        if ($row['current_state'] == '')
                            $t_down_ce++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_ce++;
                        elseif ($row['current_state'] == 1)
                            $t_down_ce++;
                        
                    }
                }
                
                $total = $t_up + $t_down;
                $link_t_down = number_format($t_down,0);
                $link_param = $BASEURL.'index.php?event_id='.$eventid.'&loc_name='.$hotel.'&loc_id='.$loc_id;
                if ( ($t_down > 0)) 
                {
                    $warna = ' merah';
                    $fclass = ' fmerah'; 
                    $trclass = 'highlight';
                    $link_t_down = '<span style="color:red;">'.number_format($t_down,0).'</span>';
                    
                }
                elseif ( ($t_up == 0) and ($t_down == 0))
                { 
                    $warna = ' abu';
                    $fclass = ' fputih'; 
                    $trclass = '';
                }
                elseif ( ($t_up > 0) and ($t_down == 0))
                { 
                    $warna = ' hijau';
                    $fclass = ' fhijau'; 
                    $trclass = '';
                }
                $fclass_up = $fclass_down = ' style="color: #fff;" ';
                
                if ($t_up > 0)
                    $fclass_up = ' style="color: #82D711;" ';
                if ($t_down > 0)
                    $fclass_down = ' style="color: #FF0000;" ';
                

                $total_switch_cr = $t_up_switch_cr + $t_down_switch_cr;
                $total_switch_acc = $t_up_switch_acc + $t_down_switch_acc;
                $link_t_down_switch_cr = number_format($t_down_switch_cr,0);
                $link_t_down_switch_acc = number_format($t_down_switch_acc,0);
                
                /** STYLING SWITCH COUNT STATISTIC */
                /** SWITCH CORE ========================= */
                if ( ($t_down_switch_cr > 0)) 
                {
                    $warna_switch_cr = ' merah';
                    $trclass = 'highlight';
                    #$link_t_down_switch_cr = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-1-switch" style="color:red;" class="link_switch">'.number_format($t_down_switch_cr,0).'</a>';
                    $link_t_down_switch_cr = '<span style="color:red;">'.number_format($t_down_switch_cr,0).'</span>';
                    
                }
                elseif ( ($t_up_switch_cr == 0) and ($t_down_switch_cr == 0))
                { 
                    $warna_switch_cr = ' abu';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_cr > 0) and ($t_down_switch_cr == 0))
                { 
                    $warna_switch_cr = ' hijau';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_cr > 0))
                {
                    //$trclass = '';
                }
                
                $fclass_switch_up_cr = $fclass_switch_down_cr = ' style="color: #fff;" ';
                $link_t_up_switch_cr = number_format($t_up_switch_cr,0);
                
                if ($t_up_switch_cr > 0) {
                    $fclass_switch_up_cr = ' style="color: #82D711;" ';
                    $link_t_up_switch_cr = '<span style="color:#82D711;" class="link_switch">'.number_format($t_up_switch_cr,0).'</span>';
                    }
                if ($t_down_switch_cr > 0)
                    $fclass_switch_down_cr = ' style="color: #FF0000;" ';
                
                $link_total_switch_cr = '<span style="color:#FFF;" class="link_switch">'.number_format($total_switch_cr,0).'</span>';              
            

                /** SWITCH ACCESS ========================= */
                if ( ($t_down_switch_acc > 0)) 
                {
                    $warna_switch_acc = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_switch_acc = '<span style="color:red;" class="link_switch">'.number_format($t_down_switch_acc,0).'</span>';
                    
                }
                elseif ( ($t_up_switch_acc == 0) and ($t_down_switch_acc == 0))
                { 
                    $warna_switch_acc = ' abu';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_acc > 0) and ($t_down_switch_acc == 0))
                { 
                    $warna_switch_acc = ' hijau';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_acc > 0))
                {
                    //$trclass = '';
                }
                
                $fclass_switch_up_acc = $fclass_switch_down_acc = ' style="color: #fff;" ';
                $link_t_up_switch_acc = number_format($t_up_switch_acc,0);
                
                if ($t_up_switch_acc > 0) {
                    $fclass_switch_up_acc = ' style="color: #82D711;" ';
                    $link_t_up_switch_acc = '<span style="color:#82D711;" class="link_switch">'.number_format($t_up_switch_acc,0).'</span>';
                    }
                if ($t_down_switch_acc > 0)
                    $fclass_switch_down_acc = ' style="color: #FF0000;" ';
                
                $link_total_switch_acc = '<span style="color:#FFF;" class="link_switch">'.number_format($total_switch_acc,0).'</span>';              

                
                /** OLT ========================= */
                $total_olt = $t_up_olt + $t_down_olt;
                $link_t_down_olt = number_format($t_down_olt,0);
                if ( ($t_down_olt > 0)) 
                {
                    $warna_olt = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_olt = '<span style="color:red;" class="link_olt">'.number_format($t_down_olt,0).'</span>';
                    
                }
                elseif ( ($t_up_olt == 0) and ($t_down_olt == 0))
                { 
                    $warna_olt = ' abu';
                    #$trclass = '';
                }
                elseif ( ($t_up_olt > 0) and ($t_down_olt == 0))
                { 
                    $warna_olt = ' hijau';
                    #$trclass = '';
                }
                elseif ( ($t_up_olt > 0))
                {
                    #$trclass = '';
                }
                
                $fclass_olt_up = $fclass_olt_down = ' style="color: #fff;" ';
                $link_t_up_olt = number_format($t_up_olt,0);
                
                if ($t_up_olt > 0) {
                    $fclass_olt_up = ' style="color: #82D711;" ';
                    $link_t_up_olt = '<span style="color:#82D711;" class="link_olt">'.number_format($t_up_olt,0).'</span>';
                    }
                if ($t_down_olt > 0)
                    $fclass_olt_down = ' style="color: #FF0000;" ';
                $link_total_olt = '<span style="color:#FFF;" class="link_olt">'.number_format($total_olt,0).'</span>';              
                

                /** CE ========================= */
                $total_ce = $t_up_ce + $t_down_ce;
                $link_t_down_ce = number_format($t_down_ce,0);
                if ( ($t_down_ce > 0)) 
                {
                    $warna_ce = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_ce = '<span style="color:red;" class="link_ce">'.number_format($t_down_ce,0).'</span>';
                    
                }
                elseif ( ($t_up_ce == 0) and ($t_down_ce == 0))
                { 
                    $warna_ce = ' abu';
                    #$trclass = '';
                }
                elseif ( ($t_up_ce > 0) and ($t_down_ce == 0))
                { 
                    $warna_ce = ' hijau';
                    #$trclass = '';
                }
                elseif ( ($t_up_ce > 0))
                {
                    #$trclass = '';
                }
                
                $fclass_ce_up = $fclass_ce_down = ' style="color: #fff;" ';
                $link_t_up_ce = number_format($t_up_ce,0);
                
                if ($t_up_ce > 0) {
                    $fclass_ce_up = ' style="color: #82D711;" ';
                    $link_t_up_ce = '<span style="color:#82D711;" class="link_ce">'.number_format($t_up_ce,0).'</span>';
                    }
                if ($t_down_ce > 0)
                    $fclass_ce_down = ' style="color: #FF0000;" ';
                $link_total_ce = '<span style="color:#FFF;" class="link_ce">'.number_format($total_ce,0).'</span>';              
                /** ================================= */

                /** ONT ========================= */
                $total_ont = $t_up_ont + $t_down_ont;
                $link_t_down_ont = number_format($t_down_ont,0);
                if ( ($t_down_ont > 0)) 
                {
                    $trclass = 'highlight';
                    $link_t_down_ont = '<span style="color:red;" class="link_ont">'.number_format($t_down_ont,0).'</span>';
                    
                }
                
                $fclass_ont_up = $fclass_ont_down = ' style="color: #fff;" ';
                $link_t_up_ont = number_format($t_up_ont,0);
                
                if ($t_up_ont > 0) {
                    $fclass_ont_up = ' style="color: #82D711;" ';
                    $link_t_up_ont = '<span style="color:#82D711;" class="link_ce">'.number_format($t_up_ont,0).'</span>';
                    }
                if ($t_down_ont > 0)
                    $fclass_ont_down = ' style="color: #FF0000;" ';
                $link_total_ont = '<span style="color:#FFF;" class="link_ce">'.number_format($total_ont,0).'</span>';              
                /** ================================= */


                //$link_t_down = '<a href="'.$link_param.'" target="_blank" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-ap" style="color:red;" class="link_ap">'.number_format($t_down,0).'</a>';

                $html .= '<tr class="link_ap '.$trclass.'" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'">';                
                $html .= '<td class="tc">'.$no.'</td>';                
                $html .= '<td class="tl">'.strtoupper($alias).'</td>';                
                
                if (strtoupper($title) == 'TELKOM OFFICE')
                {
                    $html .= '<td class="tr"'.$fclass_ce_up.'>'.$link_t_up_ce.'</td>';                
                    $html .= '<td class="tr"'.$fclass_ce_down.'>'.$link_t_down_ce.'</td>';                
                    $html .= '<td class="tr">'.$link_total_ce.'</td>';                                    
                    $html .= '<td class="tr"'.$fclass_olt_up.'>'.$link_t_up_olt.'</td>';                
                    $html .= '<td class="tr"'.$fclass_olt_down.'>'.$link_t_down_olt.'</td>';                
                    $html .= '<td class="tr">'.$link_total_olt.'</td>';                                    
                }
                else
                {
                    $html .= '<td class="tr"'.$fclass_up.'>'.number_format($t_up,0).'</td>';                
                    $html .= '<td class="tr"'.$fclass_down.'>'.$link_t_down.'</td>';                
                    $html .= '<td class="tr">'.number_format($total,0).'</td>';       
                    if (ucwords($title) == 'Reguler')
                    {
                    $html .= '<td class="tr"'.$fclass_switch_up.'>'.$link_t_up_ont.' | '.$link_t_up_switch_acc.'</td>';                
                    $html .= '<td class="tr"'.$fclass_switch_down.'>'.$link_t_down_ont.' | '.$link_t_down_switch_acc.'</td>';                
                    $html .= '<td class="tr">'.$link_total_ont.' | '.$link_total_switch_acc.'</td>';                
                    } else {
                    $html .= '<td class="tr"'.$fclass_switch_up.'>'.$link_t_up_ont.' | '.$link_t_up_switch_cr.' | '.$link_t_up_switch_acc.'</td>';                
                    $html .= '<td class="tr"'.$fclass_switch_down.'>'.$link_t_down_ont.' | '.$link_t_down_switch_cr.' | '.$link_t_down_switch_acc.'</td>';                
                    $html .= '<td class="tr">'.$link_total_ont.' | '.$link_total_switch_cr.' | '.$link_total_switch_acc.'</td>';                
                    }
                    
                    
                }
                $html .= '</tr>';   
                $no++;
                $tt_up += $t_up; 
                $tt_down += $t_down; 
                $ttotal += $total; 
                $tt_up_switch_cr += $t_up_switch_cr; 
                $tt_down_switch_cr += $t_down_switch_cr; 
                $ttotal_switch_cr += $total_switch_cr; 
                $tt_up_switch_acc += $t_up_switch_acc; 
                $tt_down_switch_acc += $t_down_switch_acc; 
                $ttotal_switch_acc += $total_switch_acc; 
                $tt_up_olt += $t_up_olt; 
                $tt_down_olt += $t_down_olt; 
                $ttotal_olt += $total_olt; 
                $tt_up_ce += $t_up_ce; 
                $tt_down_ce += $t_down_ce; 
                $ttotal_ce += $total_ce; 
                $tt_up_ont += $t_up_ont; 
                $tt_down_ont += $t_down_ont; 
                $ttotal_ont += $total_ont; 
                
            } 
            
                $html .= '<tr class="no-border">';                
                $html .= '<td colspan="2" class="no-border">&nbsp;</td>';                

                if (strtoupper($title) == 'TELKOM OFFICE')
                {
                                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.$tt_up_ce.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.$tt_down_ce.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.$ttotal_ce.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.$tt_up_olt.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.$tt_down_olt.'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.$ttotal_olt.'</td>';                
                }
                else
                {
                                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.number_format($tt_up,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.number_format($tt_down,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.number_format($ttotal,0).'</td>';
                    if (ucwords($title) == 'Reguler')
                    {
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_up_ont,0).' | '.number_format($tt_up_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_down_ont,0).' | '.number_format($tt_down_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.number_format($ttotal_ont,0).' | '.number_format($ttotal_switch_acc,0).'</td>';                
                    } else {
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_up_ont,0).' | '.number_format($tt_up_switch_cr,0).' | '.number_format($tt_up_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_down_ont,0).' | '.number_format($tt_down_switch_cr,0).' | '.number_format($tt_down_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.number_format($ttotal_ont,0).' | '.number_format($ttotal_switch_cr,0).' | '.number_format($ttotal_switch_acc,0).'</td>';                
                    }
                    
                    #$html .= '<td class="tr '.$fclass.' alternate_black">'.number_format($ttotal_ont,0).' | '.number_format($ttotal_switch_cr,0).' | '.number_format($ttotal_switch_acc,0).'</td>';                
                    
                }
                    $html .= '</tr>';
                   
    if ($complete_table)
    {
            $html .= '</table>';
    }   
                $res = array();
                $res['total_sw_up_cr'] = $tt_up_switch_cr;
                $res['total_sw_down_cr'] = $tt_down_switch_cr;
                $res['total_sw_up_acc'] = $tt_up_switch_acc;
                $res['total_sw_down_acc'] = $tt_down_switch_acc;
                $res['total_ap_up'] = @$tt_up_ap;
                $res['total_ap_down'] = @$tt_down_ap;
                $res['total_olt_up'] = @$tt_up_olt;
                $res['total_olt_down'] = @$tt_down_olt;
                $res['total_ont_up'] = @$tt_up_ont;
                $res['total_ont_down'] = @$tt_down_ont;
                $res['html'] = $html;
                return $res;
    
}

function summary_table_all_old($title,$data_switch,$data_ap,$data_ont,$table_prefix,$eventid,$db,$conn,$complete_table = true)
{


    if ($complete_table)
    {
            $html = '<table class="tlist fl" width="100%">
            <tr>
                <td colspan="8" class="tc kolomjudul">'.ucwords($title).'</td>
            </tr>
            <tr>
                <td class="kolomjudul alternate_black tc">No.</td>
                <td class="kolomjudul alternate_black">Location</td>
                <td class="tc alternate_black"><img title="Access Point UP" src="assets/images/wifi-up.png" class="img_wifi" /></td>
                <td class="tc alternate_black"><img title="Access Point Down" src="assets/images/wifi-down.png" class="img_wifi" /></td>
                <td class="alternate_black tc">Tot</td>
                <td class="tc alternate_black"><img title="Switch UP" src="assets/images/switch-up.png" class="img_wifi" /> O/C/A</td>
                <td class="tc alternate_black"><img title="Switch Down" src="assets/images/switch-down.png" class="img_wifi" /> O/C/A</td>
                <td class="alternate_black tc">Tot</td>
            </tr>';
    } else { $html = ''; }
            
            $ttotal = $tt_up = $tt_down = $ttotal_switch_cr = $ttotal_switch_acc = 0;
            $tt_up_switch_cr = $tt_up_switch_acc = $tt_down_switch_cr = $tt_down_switch_acc = 0;
            $ttotal_ce = $ttotal_up_ce = $ttotal_down_ce = 0;
            $ttotal_olt = $ttotal_up_olt = $ttotal_down_olt = 0;
            $ttotal_ont = $ttotal_up_ont = $ttotal_down_ont = 0;
            $no = 1;

            foreach($data_switch as $hotel => $lokasi) {
                $alias = "";
                $t_down_switch_cr = $t_down_switch_acc = $t_up_switch_cr = $t_up_switch_acc = $t_up = $t_down = 0;
                $t_down_olt = $t_up_olt = 0;
                $t_down_ce = $t_up_ce = 0;
                $t_down_ont = $t_up_ont = 0;
                
                $loc_id = $lokasi[0]['loc_id'];
                $t_up = array_key_exists($hotel,$data_ap) ? @$data_ap[$hotel]['UP'] : 0;
                $t_down = array_key_exists($hotel,$data_ap) ? @$data_ap[$hotel]['DOWN'] : 0;
                
                $t_up_ont = array_key_exists($hotel,$data_ont) ? @$data_ont[$hotel]['UP'] : 0;
                $t_down_ont = array_key_exists($hotel,$data_ont) ? @$data_ont[$hotel]['DOWN'] : 0;

                foreach($lokasi as $row)
                {
                    $alias = $row['alias'];
                    $switchs[] = $row['ip_address'];
                    if ($row['TIPE_SW'] == 'COR')
                    {
                        if ($row['current_state'] == '')
                            $t_down_switch_cr++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_switch_cr++;
                        elseif ($row['current_state'] == 1)
                            $t_down_switch_cr++;
                        
                    }
                    elseif ($row['TIPE_SW'] == 'ACC')
                    {
                        if ($row['current_state'] == '')
                            $t_down_switch_acc++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_switch_acc++;
                        elseif ($row['current_state'] == 1)
                            $t_down_switch_acc++;
                        
                    }
                    elseif ($row['TIPE_SW'] == 'OLT')
                    {
                        if ($row['current_state'] == '')
                            $t_down_olt++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_olt++;
                        elseif ($row['current_state'] == 1)
                            $t_down_olt++;
                        
                    }
                    elseif ($row['TIPE_SW'] == 'CE-')
                    {
                        if ($row['current_state'] == '')
                            $t_down_ce++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_ce++;
                        elseif ($row['current_state'] == 1)
                            $t_down_ce++;
                        
                    }
                }
                
                $total = $t_up + $t_down;
                $link_t_down = number_format($t_down,0);
                $link_param = $BASEURL.'index.php?event_id='.$eventid.'&loc_name='.$hotel.'&loc_id='.$loc_id;
                if ( ($t_down > 0)) 
                {
                    $warna = ' merah';
                    $fclass = ' fmerah'; 
                    $trclass = 'highlight';
                    #$link_t_down = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-ap" style="color:red;" class="link_ap">'.$t_down.'</a>';
                    $link_t_down = '<a href="'.$link_param.'" target="_blank" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-ap" style="color:red;" class="link_ap">'.number_format($t_down,0).'</a>';
                    
                }
                elseif ( ($t_up == 0) and ($t_down == 0))
                { 
                    $warna = ' abu';
                    $fclass = ' fputih'; 
                    $trclass = '';
                }
                elseif ( ($t_up > 0) and ($t_down == 0))
                { 
                    $warna = ' hijau';
                    $fclass = ' fhijau'; 
                    $trclass = '';
                }
                $fclass_up = $fclass_down = ' style="color: #fff;" ';
                
                if ($t_up > 0)
                    $fclass_up = ' style="color: #82D711;" ';
                if ($t_down > 0)
                    $fclass_down = ' style="color: #FF0000;" ';
                

                $total_switch_cr = $t_up_switch_cr + $t_down_switch_cr;
                $total_switch_acc = $t_up_switch_acc + $t_down_switch_acc;
                $link_t_down_switch_cr = number_format($t_down_switch_cr,0);
                $link_t_down_switch_acc = number_format($t_down_switch_acc,0);
                
                /** STYLING SWITCH COUNT STATISTIC */
                /** SWITCH CORE ========================= */
                if ( ($t_down_switch_cr > 0)) 
                {
                    $warna_switch_cr = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_switch_cr = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-1-switch" style="color:red;" class="link_switch">'.number_format($t_down_switch_cr,0).'</a>';
                    
                }
                elseif ( ($t_up_switch_cr == 0) and ($t_down_switch_cr == 0))
                { 
                    $warna_switch_cr = ' abu';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_cr > 0) and ($t_down_switch_cr == 0))
                { 
                    $warna_switch_cr = ' hijau';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_cr > 0))
                {
                    //$trclass = '';
                }
                
                $fclass_switch_up_cr = $fclass_switch_down_cr = ' style="color: #fff;" ';
                $link_t_up_switch_cr = number_format($t_up_switch_cr,0);
                
                if ($t_up_switch_cr > 0) {
                    $fclass_switch_up_cr = ' style="color: #82D711;" ';
                    $link_t_up_switch_cr = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-0-switch" style="color:#82D711;" class="link_switch">'.number_format($t_up_switch_cr,0).'</a>';
                    }
                if ($t_down_switch_cr > 0)
                    $fclass_switch_down_cr = ' style="color: #FF0000;" ';
                
                $link_total_switch_cr = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-ALL-switch" style="color:#FFF;" class="link_switch">'.number_format($total_switch_cr,0).'</a>';              
            

                /** SWITCH ACCESS ========================= */
                if ( ($t_down_switch_acc > 0)) 
                {
                    $warna_switch_acc = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_switch_acc = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-1-switch" style="color:red;" class="link_switch">'.number_format($t_down_switch_acc,0).'</a>';
                    
                }
                elseif ( ($t_up_switch_acc == 0) and ($t_down_switch_acc == 0))
                { 
                    $warna_switch_acc = ' abu';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_acc > 0) and ($t_down_switch_acc == 0))
                { 
                    $warna_switch_acc = ' hijau';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_acc > 0))
                {
                    //$trclass = '';
                }
                
                $fclass_switch_up_acc = $fclass_switch_down_acc = ' style="color: #fff;" ';
                $link_t_up_switch_acc = number_format($t_up_switch_acc,0);
                
                if ($t_up_switch_acc > 0) {
                    $fclass_switch_up_acc = ' style="color: #82D711;" ';
                    $link_t_up_switch_acc = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-0-switch" style="color:#82D711;" class="link_switch">'.number_format($t_up_switch_acc,0).'</a>';
                    }
                if ($t_down_switch_acc > 0)
                    $fclass_switch_down_acc = ' style="color: #FF0000;" ';
                
                $link_total_switch_acc = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-ALL-switch" style="color:#FFF;" class="link_switch">'.number_format($total_switch_acc,0).'</a>';              

                
                /** OLT ========================= */
                $total_olt = $t_up_olt + $t_down_olt;
                $link_t_down_olt = number_format($t_down_olt,0);
                if ( ($t_down_olt > 0)) 
                {
                    $warna_olt = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_olt = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-1-olt" style="color:red;" class="link_olt">'.number_format($t_down_olt,0).'</a>';
                    
                }
                elseif ( ($t_up_olt == 0) and ($t_down_olt == 0))
                { 
                    $warna_olt = ' abu';
                    #$trclass = '';
                }
                elseif ( ($t_up_olt > 0) and ($t_down_olt == 0))
                { 
                    $warna_olt = ' hijau';
                    #$trclass = '';
                }
                elseif ( ($t_up_olt > 0))
                {
                    #$trclass = '';
                }
                
                $fclass_olt_up = $fclass_olt_down = ' style="color: #fff;" ';
                $link_t_up_olt = number_format($t_up_olt,0);
                
                if ($t_up_olt > 0) {
                    $fclass_olt_up = ' style="color: #82D711;" ';
                    $link_t_up_olt = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-0-switch" style="color:#82D711;" class="link_olt">'.number_format($t_up_olt,0).'</a>';
                    }
                if ($t_down_olt > 0)
                    $fclass_olt_down = ' style="color: #FF0000;" ';
                $link_total_olt = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-ALL-switch" style="color:#FFF;" class="link_olt">'.number_format($total_olt,0).'</a>';              
                

                /** CE ========================= */
                $total_ce = $t_up_ce + $t_down_ce;
                $link_t_down_ce = number_format($t_down_ce,0);
                if ( ($t_down_ce > 0)) 
                {
                    $warna_ce = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_ce = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-1-ce" style="color:red;" class="link_ce">'.number_format($t_down_ce,0).'</a>';
                    
                }
                elseif ( ($t_up_ce == 0) and ($t_down_ce == 0))
                { 
                    $warna_ce = ' abu';
                    #$trclass = '';
                }
                elseif ( ($t_up_ce > 0) and ($t_down_ce == 0))
                { 
                    $warna_ce = ' hijau';
                    #$trclass = '';
                }
                elseif ( ($t_up_ce > 0))
                {
                    #$trclass = '';
                }
                
                $fclass_ce_up = $fclass_ce_down = ' style="color: #fff;" ';
                $link_t_up_ce = number_format($t_up_ce,0);
                
                if ($t_up_ce > 0) {
                    $fclass_ce_up = ' style="color: #82D711;" ';
                    $link_t_up_ce = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-0-ce" style="color:#82D711;" class="link_ce">'.number_format($t_up_ce,0).'</a>';
                    }
                if ($t_down_ce > 0)
                    $fclass_ce_down = ' style="color: #FF0000;" ';
                $link_total_ce = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-ALL-ce" style="color:#FFF;" class="link_ce">'.number_format($total_ce,0).'</a>';              
                /** ================================= */

                /** ONT ========================= */
                $total_ont = $t_up_ont + $t_down_ont;
                $link_t_down_ont = number_format($t_down_ont,0);
                if ( ($t_down_ont > 0)) 
                {
                    $trclass = 'highlight';
                    $link_t_down_ont = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-1-ce" style="color:red;" class="link_ont">'.number_format($t_down_ont,0).'</a>';
                    
                }
                
                $fclass_ont_up = $fclass_ont_down = ' style="color: #fff;" ';
                $link_t_up_ont = number_format($t_up_ont,0);
                
                if ($t_up_ont > 0) {
                    $fclass_ont_up = ' style="color: #82D711;" ';
                    $link_t_up_ont = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-0-ce" style="color:#82D711;" class="link_ce">'.number_format($t_up_ont,0).'</a>';
                    }
                if ($t_down_ont > 0)
                    $fclass_ont_down = ' style="color: #FF0000;" ';
                $link_total_ont = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-ALL-ce" style="color:#FFF;" class="link_ce">'.number_format($total_ont,0).'</a>';              
                /** ================================= */


                $html .= '<tr class="'.$trclass.'">';                
                $html .= '<td class="tc">'.$no.'</td>';                
                $html .= '<td class="tl">'.strtoupper($alias).'</td>';                
                
                if (strtoupper($title) == 'TELKOM OFFICE')
                {
                    $html .= '<td class="tr"'.$fclass_ce_up.'>'.$link_t_up_ce.'</td>';                
                    $html .= '<td class="tr"'.$fclass_ce_down.'>'.$link_t_down_ce.'</td>';                
                    $html .= '<td class="tr">'.$link_total_ce.'</td>';                                    
                    $html .= '<td class="tr"'.$fclass_olt_up.'>'.$link_t_up_olt.'</td>';                
                    $html .= '<td class="tr"'.$fclass_olt_down.'>'.$link_t_down_olt.'</td>';                
                    $html .= '<td class="tr">'.$link_total_olt.'</td>';                                    
                }
                else
                {
                    $html .= '<td class="tr"'.$fclass_up.'>'.number_format($t_up,0).'</td>';                
                    $html .= '<td class="tr"'.$fclass_down.'>'.$link_t_down.'</td>';                
                    $html .= '<td class="tr">'.number_format($total,0).'</td>';                
                    $html .= '<td class="tr"'.$fclass_switch_up.'>'.$link_t_up_ont.' / '.$link_t_up_switch_cr.' / '.$link_t_up_switch_acc.'</td>';                
                    $html .= '<td class="tr"'.$fclass_switch_down.'>'.$link_t_down_ont.' / '.$link_t_down_switch_cr.' / '.$link_t_down_switch_acc.'</td>';                
                    $html .= '<td class="tr">'.$link_total_ont.' / '.$link_total_switch_cr.' / '.$link_total_switch_acc.'</td>';                
                    
                }
                $html .= '</tr>';   
                $no++;
                $tt_up += $t_up; 
                $tt_down += $t_down; 
                $ttotal += $total; 
                $tt_up_switch_cr += $t_up_switch_cr; 
                $tt_down_switch_cr += $t_down_switch_cr; 
                $ttotal_switch_cr += $total_switch_cr; 
                $tt_up_switch_acc += $t_up_switch_acc; 
                $tt_down_switch_acc += $t_down_switch_acc; 
                $ttotal_switch_acc += $total_switch_acc; 
                $tt_up_olt += $t_up_olt; 
                $tt_down_olt += $t_down_olt; 
                $ttotal_olt += $total_olt; 
                $tt_up_ce += $t_up_ce; 
                $tt_down_ce += $t_down_ce; 
                $ttotal_ce += $total_ce; 
                $tt_up_ont += $t_up_ont; 
                $tt_down_ont += $t_down_ont; 
                $ttotal_ont += $total_ont; 
                
            } 
            
                $html .= '<tr class="no-border">';                
                $html .= '<td colspan="2" class="no-border">TOTAL</td>';                

                if (strtoupper($title) == 'TELKOM OFFICE')
                {
                                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.$tt_up_ce.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.$tt_down_ce.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.$ttotal_ce.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.$tt_up_olt.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.$tt_down_olt.'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.$ttotal_olt.'</td>';                
                }
                else
                {
                                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.number_format($tt_up,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.number_format($tt_down,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgoren2">'.number_format($ttotal,0).'</td>';
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_up_ont,0).' / '.number_format($tt_up_switch_cr,0).' / '.number_format($tt_up_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($tt_down_ont,0).' / '.number_format($tt_down_switch_cr,0).' / '.number_format($tt_down_switch_acc,0).'</td>';                
                    $html .= '<td class="tr '.$fclass.' bgpurple">'.number_format($ttotal_ont,0).' / '.number_format($ttotal_switch_cr,0).' / '.number_format($ttotal_switch_acc,0).'</td>';                
                    
                }
                    $html .= '</tr>';
                   
    if ($complete_table)
    {
            $html .= '</table>';
    }   
                $res = array();
                $res['total_sw_up_cr'] = $tt_up_switch_cr;
                $res['total_sw_down_cr'] = $tt_down_switch_cr;
                $res['total_sw_up_acc'] = $tt_up_switch_acc;
                $res['total_sw_down_acc'] = $tt_down_switch_acc;
                $res['total_ap_up'] = @$tt_up_ap;
                $res['total_ap_down'] = @$tt_down_ap;
                $res['total_olt_up'] = @$tt_up_olt;
                $res['total_olt_down'] = @$tt_down_olt;
                $res['total_ont_up'] = @$tt_up_ont;
                $res['total_ont_down'] = @$tt_down_ont;
                $res['html'] = $html;
                return $res;
    
}

function summary_table($title,$data_switch,$data_ap,$table_prefix,$eventid,$db,$conn,$complete_table = true)
{


    if ($complete_table)
    {
            $html = '<table class="tlist fl" width="100%">
            <tr>
                <td colspan="8" class="tc kolomjudul">'.ucwords($title).'</td>
            </tr>
            <tr>
                <td class="kolomjudul alternate_black tc">No.</td>
                <td class="kolomjudul alternate_black">Location</td>
                <td class="tc alternate_black"><img title="Access Point UP" src="assets/images/wifi-up.png" class="img_wifi" /></td>
                <td class="tc alternate_black"><img title="Access Point Down" src="assets/images/wifi-down.png" class="img_wifi" /></td>
                <td class="alternate_black tc">Tot</td>
                <td class="tc alternate_black"><img title="Switch UP" src="assets/images/switch-up.png" class="img_wifi" /></td>
                <td class="tc alternate_black"><img title="Switch Down" src="assets/images/switch-down.png" class="img_wifi" /></td>
                <td class="alternate_black tc">Tot</td>
            </tr>';
    } else { $html = ''; }
            
            $ttotal = $tt_up = $tt_down = $ttotal_switch_cr = $ttotal_switch_acc = $tt_up_switch_cr = $tt_up_switch_acc = $tt_down_switch_cr = $tt_down_switch_acc = 0;
            $ttotal_olt = $ttotal_up_olt = $ttotal_down_olt = 0;
            $ttotal_ce = $ttotal_up_ce = $ttotal_down_ce = 0;
            $no = 1;

            foreach($data_switch as $hotel => $lokasi) {
                $alias = "";
                $t_down_switch_cr = $t_down_switch_acc = $t_up_switch_cr = $t_up_switch_acc = $t_up = $t_down = 0;
                $t_down_olt = $t_up_olt = 0;
                $t_down_ce = $t_up_ce = 0;
                
                $loc_id = $lokasi[0]['loc_id'];
                $t_up = array_key_exists($hotel,$data_ap) ? @$data_ap[$hotel]['UP'] : 0;
                $t_down = array_key_exists($hotel,$data_ap) ? @$data_ap[$hotel]['DOWN'] : 0;
                foreach($lokasi as $row)
                {
                    $alias = $row['alias'];
                    $switchs[] = $row['ip_address'];
                    if ($row['TIPE_SW'] == 'COR')
                    {
                        if ($row['current_state'] == '')
                            $t_down_switch_cr++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_switch_cr++;
                        elseif ($row['current_state'] == 1)
                            $t_down_switch_cr++;
                        
                    }
                    elseif ($row['TIPE_SW'] == 'ACC')
                    {
                        if ($row['current_state'] == '')
                            $t_down_switch_acc++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_switch_acc++;
                        elseif ($row['current_state'] == 1)
                            $t_down_switch_acc++;
                        
                    }
                    elseif ($row['TIPE_SW'] == 'OLT')
                    {
                        if ($row['current_state'] == '')
                            $t_down_olt++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_olt++;
                        elseif ($row['current_state'] == 1)
                            $t_down_olt++;
                        
                    }
                    elseif ($row['TIPE_SW'] == 'CE-')
                    {
                        if ($row['current_state'] == '')
                            $t_down_ce++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_ce++;
                        elseif ($row['current_state'] == 1)
                            $t_down_ce++;
                        
                    }
                    /*
                    if ($alias == 'BNDCC')
                    {
                        print_out($row['TIPE_SW']."|".$row['current_state']."|");
                    }
                    */  
                    #$rows[] = $row;
                }
                
                #echo mysql_error();
                #print_out($res);
                #print_out($q);
                #print_out($rows);
                //die;
                $total = $t_up + $t_down;
                $link_t_down = number_format($t_down,0);
                $link_param = $BASEURL.'index.php?event_id='.$eventid.'&loc_name='.$hotel.'&loc_id='.$loc_id;
                if ( ($t_down > 0)) 
                {
                    $warna = ' merah';
                    $fclass = ' fmerah'; 
                    $trclass = 'highlight';
                    #$link_t_down = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-ap" style="color:red;" class="link_ap">'.$t_down.'</a>';
                    $link_t_down = '<a href="'.$link_param.'" target="_blank" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-ap" style="color:red;" class="link_ap">'.number_format($t_down,0).'</a>';
                    
                }
                elseif ( ($t_up == 0) and ($t_down == 0))
                { 
                    $warna = ' abu';
                    $fclass = ' fputih'; 
                    $trclass = '';
                }
                elseif ( ($t_up > 0) and ($t_down == 0))
                { 
                    $warna = ' hijau';
                    $fclass = ' fhijau'; 
                    $trclass = '';
                }
                $fclass_up = $fclass_down = ' style="color: #fff;" ';
                
                if ($t_up > 0)
                    $fclass_up = ' style="color: #82D711;" ';
                if ($t_down > 0)
                    $fclass_down = ' style="color: #FF0000;" ';
                

                $total_switch_cr = $t_up_switch_cr + $t_down_switch_cr;
                $total_switch_acc = $t_up_switch_acc + $t_down_switch_acc;
                $link_t_down_switch_cr = $t_down_switch_cr;
                $link_t_down_switch_acc = $t_down_switch_acc;
                
                /** STYLING SWITCH COUNT STATISTIC */
                /** SWITCH CORE ========================= */
                if ( ($t_down_switch_cr > 0)) 
                {
                    $warna_switch_cr = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_switch_cr = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-1-switch" style="color:red;" class="link_switch">'.number_format($t_down_switch_cr,0).'</a>';
                    
                }
                elseif ( ($t_up_switch_cr == 0) and ($t_down_switch_cr == 0))
                { 
                    $warna_switch_cr = ' abu';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_cr > 0) and ($t_down_switch_cr == 0))
                { 
                    $warna_switch_cr = ' hijau';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_cr > 0))
                {
                    //$trclass = '';
                }
                
                $fclass_switch_up_cr = $fclass_switch_down_cr = ' style="color: #fff;" ';
                $link_t_up_switch_cr = $t_up_switch_cr;
                
                if ($t_up_switch_cr > 0) {
                    $fclass_switch_up_cr = ' style="color: #82D711;" ';
                    $link_t_up_switch_cr = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-0-switch" style="color:#82D711;" class="link_switch">'.number_format($t_up_switch_cr,0).'</a>';
                    }
                if ($t_down_switch_cr > 0)
                    $fclass_switch_down_cr = ' style="color: #FF0000;" ';
                
                $link_total_switch_cr = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-ALL-switch" style="color:#FFF;" class="link_switch">'.number_format($total_switch_cr,0).'</a>';              
            

                /** SWITCH ACCESS ========================= */
                if ( ($t_down_switch_acc > 0)) 
                {
                    $warna_switch_acc = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_switch_acc = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-1-switch" style="color:red;" class="link_switch">'.number_format($t_down_switch_acc,0).'</a>';
                    
                }
                elseif ( ($t_up_switch_acc == 0) and ($t_down_switch_acc == 0))
                { 
                    $warna_switch_acc = ' abu';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_acc > 0) and ($t_down_switch_acc == 0))
                { 
                    $warna_switch_acc = ' hijau';
                    //$trclass = '';
                }
                elseif ( ($t_up_switch_acc > 0))
                {
                    //$trclass = '';
                }
                
                $fclass_switch_up_acc = $fclass_switch_down_acc = ' style="color: #fff;" ';
                $link_t_up_switch_acc = $t_up_switch_acc;
                
                if ($t_up_switch_acc > 0) {
                    $fclass_switch_up_acc = ' style="color: #82D711;" ';
                    $link_t_up_switch_acc = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-0-switch" style="color:#82D711;" class="link_switch">'.number_format($t_up_switch_acc,0).'</a>';
                    }
                if ($t_down_switch_acc > 0)
                    $fclass_switch_down_acc = ' style="color: #FF0000;" ';
                
                $link_total_switch_acc = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-ALL-switch" style="color:#FFF;" class="link_switch">'.number_format($total_switch_acc,0).'</a>';              

                
                /** OLT ========================= */
                $total_olt = $t_up_olt + $t_down_olt;
                $link_t_down_olt = $t_down_olt;
                if ( ($t_down_olt > 0)) 
                {
                    $warna_olt = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_olt = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-1-olt" style="color:red;" class="link_olt">'.number_format($t_down_olt,0).'</a>';
                    
                }
                elseif ( ($t_up_olt == 0) and ($t_down_olt == 0))
                { 
                    $warna_olt = ' abu';
                    #$trclass = '';
                }
                elseif ( ($t_up_olt > 0) and ($t_down_olt == 0))
                { 
                    $warna_olt = ' hijau';
                    #$trclass = '';
                }
                elseif ( ($t_up_olt > 0))
                {
                    #$trclass = '';
                }
                
                $fclass_olt_up = $fclass_olt_down = ' style="color: #fff;" ';
                $link_t_up_olt = $t_up_olt;
                
                if ($t_up_olt > 0) {
                    $fclass_olt_up = ' style="color: #82D711;" ';
                    $link_t_up_olt = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-0-switch" style="color:#82D711;" class="link_olt">'.number_format($t_up_olt,0).'</a>';
                    }
                if ($t_down_olt > 0)
                    $fclass_olt_down = ' style="color: #FF0000;" ';
                $link_total_olt = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-ALL-switch" style="color:#FFF;" class="link_olt">'.number_format($total_olt,0).'</a>';              
                

                /** CE ========================= */
                $total_ce = $t_up_ce + $t_down_ce;
                $link_t_down_ce = $t_down_ce;
                if ( ($t_down_ce > 0)) 
                {
                    $warna_ce = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_ce = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-1-ce" style="color:red;" class="link_ce">'.number_format($t_down_ce,0).'</a>';
                    
                }
                elseif ( ($t_up_ce == 0) and ($t_down_ce == 0))
                { 
                    $warna_ce = ' abu';
                    #$trclass = '';
                }
                elseif ( ($t_up_ce > 0) and ($t_down_ce == 0))
                { 
                    $warna_ce = ' hijau';
                    #$trclass = '';
                }
                elseif ( ($t_up_ce > 0))
                {
                    #$trclass = '';
                }
                
                $fclass_ce_up = $fclass_ce_down = ' style="color: #fff;" ';
                $link_t_up_ce = $t_up_ce;
                
                if ($t_up_ce > 0) {
                    $fclass_ce_up = ' style="color: #82D711;" ';
                    $link_t_up_ce = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-0-ce" style="color:#82D711;" class="link_ce">'.number_format($t_up_ce,0).'</a>';
                    }
                if ($t_down_ce > 0)
                    $fclass_ce_down = ' style="color: #FF0000;" ';
                $link_total_ce = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-ALL-ce" style="color:#FFF;" class="link_ce">'.number_format($total_ce,0).'</a>';              
                /** ================================= */

                $html .= '<tr class="'.$trclass.'">';                
                $html .= '<td class="tc">'.$no.'</td>';                
                $html .= '<td class="tl">'.strtoupper($alias).'</td>';                
                
                if (strtoupper($title) == 'TELKOM OFFICE')
                {
                    $html .= '<td class="tr"'.$fclass_ce_up.'>'.$link_t_up_ce.'</td>';                
                    $html .= '<td class="tr"'.$fclass_ce_down.'>'.$link_t_down_ce.'</td>';                
                    $html .= '<td class="tr">'.$link_total_ce.'</td>';                                    
                    $html .= '<td class="tr"'.$fclass_olt_up.'>'.$link_t_up_olt.'</td>';                
                    $html .= '<td class="tr"'.$fclass_olt_down.'>'.$link_t_down_olt.'</td>';                
                    $html .= '<td class="tr">'.$link_total_olt.'</td>';                                    
                }
                else
                {
                    $html .= '<td class="tr"'.$fclass_up.'>'.number_format($t_up,0).'</td>';                
                    $html .= '<td class="tr"'.$fclass_down.'>'.$link_t_down.'</td>';                
                    $html .= '<td class="tr">'.number_format($total,0).'</td>';                
                    $html .= '<td class="tr"'.$fclass_switch_up.'>'.$link_t_up_switch_cr.' / '.$link_t_up_switch_acc.'</td>';                
                    $html .= '<td class="tr"'.$fclass_switch_down.'>'.$link_t_down_switch_cr.' / '.$link_t_down_switch_acc.'</td>';                
                    $html .= '<td class="tr">'.$link_total_switch_cr.' / '.$link_total_switch_acc.'</td>';                
                    
                }
                $html .= '</tr>';   
                $no++;
                $tt_up += $t_up; 
                $tt_down += $t_down; 
                $ttotal += $total; 
                $tt_up_switch_cr += $t_up_switch_cr; 
                $tt_down_switch_cr += $t_down_switch_cr; 
                $ttotal_switch_cr += $total_switch_cr; 
                $tt_up_switch_acc += $t_up_switch_acc; 
                $tt_down_switch_acc += $t_down_switch_acc; 
                $ttotal_switch_acc += $total_switch_acc; 
                $tt_up_olt += $t_up_olt; 
                $tt_down_olt += $t_down_olt; 
                $ttotal_olt += $total_olt; 
                $tt_up_ce += $t_up_ce; 
                $tt_down_ce += $t_down_ce; 
                $ttotal_ce += $total_ce; 
                
            } 
            
                $html .= '<tr>';                
                $html .= '<td colspan="2" class="kolomjudul alternate_black">TOTAL</td>';                

                if (strtoupper($title) == 'TELKOM OFFICE')
                {
                                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_up_ce.'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_down_ce.'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.$ttotal_ce.'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_up_olt.'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_down_olt.'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.$ttotal_olt.'</td>';                
                }
                else
                {
                                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_up.'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_down.'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.$ttotal.'</td>';
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_up_switch_cr.' / '.$tt_up_switch_acc.'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_down_switch_cr.' / '.$tt_down_switch_acc.'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.$ttotal_switch_cr.' / '.$ttotal_switch_acc.'</td>';                
                    
                }
                    $html .= '</tr>';
                   
    if ($complete_table)
    {
            $html .= '</table>';
    }   
                $res = array();
                $res['total_sw_up_cr'] = $tt_up_switch_cr;
                $res['total_sw_down_cr'] = $tt_down_switch_cr;
                $res['total_sw_up_acc'] = $tt_up_switch_acc;
                $res['total_sw_down_acc'] = $tt_down_switch_acc;
                $res['total_ap_up'] = @$tt_up_ap;
                $res['total_ap_down'] = @$tt_down_ap;
                $res['total_olt_up'] = @$tt_up_olt;
                $res['total_olt_down'] = @$tt_down_olt;
                $res['html'] = $html;
                return $res;
    
}

function summary_table_dev($title,$data_switch,$data_ap,$table_prefix,$eventid,$db,$conn,$complete_table = true)
{


    if ($complete_table)
    {
            $html = '<table class="tlist fl" width="100%">
            <tr>
                <td colspan="8" class="tc kolomjudul">'.ucwords($title).'</td>
            </tr>
            <tr>
                <td class="kolomjudul alternate_black tc">No.</td>
                <td class="kolomjudul alternate_black">Location</td>
                <td class="tc alternate_black"><img title="Access Point UP" src="assets/images/wifi-up.png" class="img_wifi" /></td>
                <td class="tc alternate_black"><img title="Access Point Down" src="assets/images/wifi-down.png" class="img_wifi" /></td>
                <td class="alternate_black tc">Tot</td>
                <td class="tc alternate_black"><img title="Switch UP" src="assets/images/switch-up.png" class="img_wifi" /></td>
                <td class="tc alternate_black"><img title="Switch Down" src="assets/images/switch-down.png" class="img_wifi" /></td>
                <td class="alternate_black tc">Tot</td>
            </tr>';
    } else { $html = ''; }
            
            $ttotal = $tt_up = $tt_down = $ttotal_switch_cr = $ttotal_switch_acc = $tt_up_switch_cr = $tt_up_switch_acc = $tt_down_switch_cr = $tt_down_switch_acc = 0;
            $ttotal_olt = $ttotal_up_olt = $ttotal_down_olt = 0;
            $no = 1;

            foreach($data_switch as $hotel => $lokasi) {
                $alias = "";
                $t_down_switch_cr = $t_down_switch_acc = $t_up_switch_cr = $t_up_switch_acc = $t_up = $t_down = 0;
                $t_down_olt = $t_up_olt = 0;
                
                $loc_id = $lokasi[0]['location_id'];
                $t_up = array_key_exists($hotel,$data_ap) ? @$data_ap[$hotel]['UP'] : 0;
                $t_down = array_key_exists($hotel,$data_ap) ? @$data_ap[$hotel]['DOWN'] : 0;
                foreach($lokasi as $row)
                {
                    $alias = $row['alias'];
                    $switchs[] = $row['ip_address'];
                    if ($row['TIPE_SW'] == 'COR')
                    {
                        if ($row['current_state'] == '')
                            $t_down_switch_cr++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_switch_cr++;
                        elseif ($row['current_state'] == 1)
                            $t_down_switch_cr++;
                        
                    }
                    elseif ($row['TIPE_SW'] == 'ACC')
                    {
                        if ($row['current_state'] == '')
                            $t_down_switch_acc++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_switch_acc++;
                        elseif ($row['current_state'] == 1)
                            $t_down_switch_acc++;
                        
                    }
                    elseif ($row['TIPE_SW'] == 'OLT')
                    {
                        if ($row['current_state'] == '')
                            $t_down_olt++;                    
                        elseif ($row['current_state'] == 0)
                            $t_up_olt++;
                        elseif ($row['current_state'] == 1)
                            $t_down_olt++;
                        
                    }
                    /*
                    if ($alias == 'BNDCC')
                    {
                        print_out($row['TIPE_SW']."|".$row['current_state']."|");
                    }
                    */  
                    #$rows[] = $row;
                }
                
                #echo mysql_error();
                #print_out($res);
                #print_out($q);
                #print_out($rows);
                //die;
                $total = $t_up + $t_down;
                $link_t_down = $t_down;
                if ( ($t_down > 0)) 
                {
                    $warna = ' merah';
                    $fclass = ' fmerah'; 
                    $trclass = 'highlight';
                    #$link_t_down = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-ap" style="color:red;" class="link_ap">'.$t_down.'</a>';
                    $link_t_down = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-ap" style="color:red;" class="">'.$t_down.'</a>';
                    
                }
                elseif ( ($t_up == 0) and ($t_down == 0))
                { 
                    $warna = ' abu';
                    $fclass = ' fputih'; 
                    $trclass = '';
                }
                elseif ( ($t_up > 0) and ($t_down == 0))
                { 
                    $warna = ' hijau';
                    $fclass = ' fhijau'; 
                    $trclass = '';
                }
                $fclass_up = $fclass_down = ' style="color: #fff;" ';
                
                if ($t_up > 0)
                    $fclass_up = ' style="color: #82D711;" ';
                if ($t_down > 0)
                    $fclass_down = ' style="color: #FF0000;" ';
                

                $total_switch_cr = $t_up_switch_cr + $t_down_switch_cr;
                $total_switch_acc = $t_up_switch_acc + $t_down_switch_acc;
                $link_t_down_switch_cr = $t_down_switch_cr;
                $link_t_down_switch_acc = $t_down_switch_acc;
                
                /** STYLING SWITCH COUNT STATISTIC */
                /** SWITCH CORE ========================= */
                if ( ($t_down_switch_cr > 0)) 
                {
                    $warna_switch_cr = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_switch_cr = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-1-switch" style="color:red;" class="link_switch">'.$t_down_switch_cr.'</a>';
                    
                }
                elseif ( ($t_up_switch_cr == 0) and ($t_down_switch_cr == 0))
                { 
                    $warna_switch_cr = ' abu';
                    $trclass = '';
                }
                elseif ( ($t_up_switch_cr > 0) and ($t_down_switch_cr == 0))
                { 
                    $warna_switch_cr = ' hijau';
                    $trclass = '';
                }
                elseif ( ($t_up_switch_cr > 0))
                {
                    $trclass = '';
                }
                
                $fclass_switch_up_cr = $fclass_switch_down_cr = ' style="color: #fff;" ';
                $link_t_up_switch_cr = $t_up_switch_cr;
                
                if ($t_up_switch_cr > 0) {
                    $fclass_switch_up_cr = ' style="color: #82D711;" ';
                    $link_t_up_switch_cr = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-0-switch" style="color:#82D711;" class="link_switch">'.$t_up_switch_cr.'</a>';
                    }
                if ($t_down_switch_cr > 0)
                    $fclass_switch_down_cr = ' style="color: #FF0000;" ';
                
                $link_total_switch_cr = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-ALL-switch" style="color:#FFF;" class="link_switch">'.$total_switch_cr.'</a>';              
            

                /** SWITCH ACCESS ========================= */
                if ( ($t_down_switch_acc > 0)) 
                {
                    $warna_switch_acc = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_switch_acc = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-1-switch" style="color:red;" class="link_switch">'.$t_down_switch_acc.'</a>';
                    
                }
                elseif ( ($t_up_switch_acc == 0) and ($t_down_switch_acc == 0))
                { 
                    $warna_switch_acc = ' abu';
                    $trclass = '';
                }
                elseif ( ($t_up_switch_acc > 0) and ($t_down_switch_acc == 0))
                { 
                    $warna_switch_acc = ' hijau';
                    $trclass = '';
                }
                elseif ( ($t_up_switch_acc > 0))
                {
                    $trclass = '';
                }
                
                $fclass_switch_up_acc = $fclass_switch_down_acc = ' style="color: #fff;" ';
                $link_t_up_switch_acc = $t_up_switch_acc;
                
                if ($t_up_switch_acc > 0) {
                    $fclass_switch_up_acc = ' style="color: #82D711;" ';
                    $link_t_up_switch_acc = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-0-switch" style="color:#82D711;" class="link_switch">'.$t_up_switch_acc.'</a>';
                    }
                if ($t_down_switch_acc > 0)
                    $fclass_switch_down_acc = ' style="color: #FF0000;" ';
                
                $link_total_switch_acc = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-ALL-switch" style="color:#FFF;" class="link_switch">'.$total_switch_acc.'</a>';              

                
                $total_olt = $t_up_olt + $t_down_olt;
                $link_t_down_olt = $t_down_olt;
                /** OLT ========================= */
                if ( ($t_down_olt > 0)) 
                {
                    $warna_olt = ' merah';
                    $trclass = 'highlight';
                    $link_t_down_olt = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-1-switch" style="color:red;" class="link_switch">'.$t_down_olt.'</a>';
                    
                }
                elseif ( ($t_up_olt == 0) and ($t_down_olt == 0))
                { 
                    $warna_olt = ' abu';
                    $trclass = '';
                }
                elseif ( ($t_up_olt > 0) and ($t_down_olt == 0))
                { 
                    $warna_olt = ' hijau';
                    $trclass = '';
                }
                elseif ( ($t_up_olt > 0))
                {
                    $trclass = '';
                }
                
                $fclass_olt_up = $fclass_olt_down = ' style="color: #fff;" ';
                $link_t_up_olt = $t_up_olt;
                
                if ($t_up_olt > 0) {
                    $fclass_olt_up = ' style="color: #82D711;" ';
                    $link_t_up_olt = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-0-switch" style="color:#82D711;" class="link_switch">'.$t_up_olt.'</a>';
                    }
                if ($t_down_olt > 0)
                    $fclass_olt_down = ' style="color: #FF0000;" ';
                $link_total_olt = '<a href="#" id="'.$eventid.'-'.$loc_id.'-'.$hotel.'-ALL-switch" style="color:#FFF;" class="link_switch">'.$total_olt.'</a>';              
                
                $html .= '<tr class="'.$trclass.'">';                
                $html .= '<td class="tc">'.$no.'</td>';                
                $html .= '<td class="tl">'.strtoupper($alias).'</td>';                
                $html .= '<td class="tr"'.$fclass_up.'>'.$t_up.'</td>';                
                $html .= '<td class="tr"'.$fclass_down.'>'.$link_t_down.'</td>';                
                $html .= '<td class="tr">'.$total.'</td>';                
                
                if (strtoupper($title) == 'TELKOM OFFICE')
                {
                    $html .= '<td class="tr"'.$fclass_olt_up.'>'.$link_t_up_olt.'</td>';                
                    $html .= '<td class="tr"'.$fclass_olt_down.'>'.$link_t_down_olt.'</td>';                
                    $html .= '<td class="tr">'.$link_total_olt.'</td>';                
                }
                else
                {
                    $html .= '<td class="tr"'.$fclass_switch_up.'>'.$link_t_up_switch_cr.' / '.$link_t_up_switch_acc.'</td>';                
                    $html .= '<td class="tr"'.$fclass_switch_down.'>'.$link_t_down_switch_cr.' / '.$link_t_down_switch_acc.'</td>';                
                    $html .= '<td class="tr">'.$link_total_switch_cr.' / '.$link_total_switch_acc.'</td>';                
                    
                }
                $html .= '</tr>';   
                $no++;
                $tt_up += $t_up; 
                $tt_down += $t_down; 
                $ttotal += $total; 
                $tt_up_switch_cr += $t_up_switch_cr; 
                $tt_down_switch_cr += $t_down_switch_cr; 
                $ttotal_switch_cr += $total_switch_cr; 
                $tt_up_switch_acc += $t_up_switch_acc; 
                $tt_down_switch_acc += $t_down_switch_acc; 
                $ttotal_switch_acc += $total_switch_acc; 
                $tt_up_olt += $t_up_olt; 
                $tt_down_olt += $t_down_olt; 
                $ttotal_olt += $total_olt; 
                
            } 
            
                $html .= '<tr>';                
                $html .= '<td colspan="2" class="kolomjudul alternate_black">TOTAL</td>';                
                $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_up.'</td>';                
                $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_down.'</td>';                
                $html .= '<td class="tr '.$fclass.' alternate_black">'.$ttotal.'</td>';

                if (strtoupper($title) == 'TELKOM OFFICE')
                {
                                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_up_olt.'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_down_olt.'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.$ttotal_olt.'</td>';                
                }
                else
                {
                                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_up_switch_cr.' / '.$tt_up_switch_acc.'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_down_switch_cr.' / '.$tt_down_switch_acc.'</td>';                
                    $html .= '<td class="tr '.$fclass.' alternate_black">'.$ttotal_switch_cr.' / '.$ttotal_switch_acc.'</td>';                
                    
                }
                    $html .= '</tr>';
                   
    if ($complete_table)
    {
            $html .= '</table>';
    }   
                $res = array();
                $res['total_sw_up_cr'] = $tt_up_switch_cr;
                $res['total_sw_down_cr'] = $tt_down_switch_cr;
                $res['total_sw_up_acc'] = $tt_up_switch_acc;
                $res['total_sw_down_acc'] = $tt_down_switch_acc;
                $res['total_ap_up'] = @$tt_up_ap;
                $res['total_ap_down'] = @$tt_down_ap;
                $res['total_olt_up'] = @$tt_up_olt;
                $res['total_olt_down'] = @$tt_down_olt;
                $res['html'] = $html;
                return $res;
    
}

function statistic_table($title,$data,$db_cacti,$conn_cacti,$table_prefix,$max_eventid,$db_icinga,$conn_icinga,$complete_table = true)
{
    if ($complete_table)
    {
            $html = '
        <table class="tlist fl" width="100%">
            <tr>
                <td colspan="8" class="tc kolomjudul">'.ucwords($title).'</td>
            </tr>
            <tr>
                <td class="kolomjudul alternate_black tc">No.</td>
                <td class="kolomjudul alternate_black">Location</td>
                <td class="tc alternate_black"><img title="Access Point UP" src="assets/images/wifi-up.png" class="img_wifi" /></td>
                <td class="tc alternate_black"><img title="Access Point Down" src="assets/images/wifi-down.png" class="img_wifi" /></td>
                <td class="alternate_black">Tot</td>                
                <td class="tc alternate_black"><img title="Switch UP" src="assets/images/switch-up.png" class="img_wifi" /> O/C/A</td>
                <td class="tc alternate_black"><img title="Switch UP" src="assets/images/switch-down.png" class="img_wifi" /> O/C/A</td>
                
                <td class="alternate_black">Tot</td>
            </tr>';
    } else { $html = ''; }
    
            $ttotal = $tt_up = $tt_down = $ttotal_switch = $tt_up_switch = $tt_down_switch = 0;
            $no = 1;
            foreach($data as $id => $lokasi) {
                $loc_id = $lokasi['loc_id'];
                $api_addr = $lokasi['ip_address'];
                $loc_name = $lokasi['loc_area'];
                $aps = array();
                mysql_select_db($db_cacti,$conn_cacti);
                $qap = "select * from ".$table_prefix."p_map_event_nms where event_id = ".$max_eventid." and loc_id = ".$loc_id;
                #print_out($qap);
                $res_ap = mysql_query($qap);
               
                while($row = mysql_fetch_assoc($res_ap))
                {
                    $aps[] = $row['nms_name'];
                }
                
                $total = sizeof($aps);
                $t_up = $t_down = 0;
                if (sizeof($aps))
                {
                    $nmsData = $Array_details = array();
                    /** GET AP STATUS */
                    /*
                    $str_apname = '%22'.implode('%22,%22',$aps).'%22';
                    $status_url = 'https://'.$api_addr.'/webacs/api/v1/data/Radios?.full=true&apName=in('.$str_apname.')';
                    $datar = file_get_contents($status_url, false, $context);
                    $domr = new DOMDocument;
                    $domr->loadXML($datar);
                    if (!$domr) {
                        echo 'Error while parsing the document';
                        exit;
                    }
                    $xr = simplexml_import_dom($domr);
                    #print_out($xr->entity);
                    
                    foreach ($xr->entity as $radio)
                    {
                        $nms_id = (string)$radio->radiosDTO->apName;
                        $operstatus = (string)$radio->radiosDTO->operStatus;
                        
                        if (array_key_exists($nms_id,$nmsData) && $nmsData[(string)$nms_id]['OPER_STATUS'] == 'UP')
                        {
                            $operstatus = 'UP';
                        }
                        $nmsData[(string)$nms_id]['OPER_STATUS'] = $operstatus;
                    }
#print_out($nmsData);
                    */

                    foreach($aps as $namaap)
                    {
                        if (array_key_exists($namaap,$nmsData))
                        {
                            $Array_details[$namaap]['OPER_STATUS'] = $nmsData[(string)$namaap]['OPER_STATUS'];
                        } else {
                            $Array_details[$namaap]['OPER_STATUS'] = 'DOWN';
                        }
                    }
                    
#print_out($Array_details);
                    foreach($Array_details as $namaAP)
                    {
                        if (strtoupper($namaAP['OPER_STATUS']) == 'UP') $t_up++;
                        elseif (strtoupper($namaAP['OPER_STATUS']) == 'DOWN') $t_down++;
                        else $t_unk++;
                        #$APSTATUS = @$Array_details[$apname]['OPER_STATUS'];
                        
                    }
                }
                
                /** GET SWITCH STATUS */
                $t_up_switch = $t_down_switch = 0;
                mysql_select_db($db_icinga,$conn_icinga);
                $q = "select b.hotel_name,b.ip_address,c.current_state ". 
                    "from hotel_ip_switchs b, icinga_hosts a, icinga_hoststatus c ". 
                    "where b.ip_address = a.address and a.host_object_id = c.host_object_id ".
                    "and upper(b.hotel_name) = upper('".$loc_name."') ";
                    #print_out($q);

                /*                    
                $q = "select substr(output,1,15) status,current_state,count(*) jml from icinga_hosts a, icinga_hoststatus b,hotel_ip_switch c  where a.host_object_id = b.host_object_id 
and a.address = c.ip_address and upper(c.hotel_name) = upper('".$loc_name."')  group by substr(output,1,15) ";
*/

                $res = mysql_query($q,$conn_icinga);
                $switchs = array();
                $rows = array();
                
                while($row = mysql_fetch_assoc($res))
                {
                    /*
                    $status = explode('-',$row['status']);
                    if (trim($status[0]) == 'PING OK')
                       $t_up_switch++;
                    else
                        $t_down_switch++;     
                    */
                    $switchs[] = $row['ip_address'];
                    
                    
                    if ($row['current_state'] == '')
                        $t_down_switch++;                    
                    elseif ($row['current_state'] == 0)
                        $t_up_switch++;
                    elseif ($row['current_state'] == 1)
                        $t_down_switch++;
                        
                    $rows[] = $row;
                    
                    
                }
                #echo mysql_error();
                #print_out($res);
                #print_out($q);
                #print_out($rows);
                //die;
                $total_switch = $t_up_switch + $t_down_switch;
                $link_t_down = $t_down;
                $link_t_down_switch = $t_down_switch;
                
                if ( ($t_down > 0)) 
                {
                    $warna = ' merah';
                    $fclass = ' fmerah'; 
                    $trclass = 'highlight';
                    $link_t_down = '<a href="#" id="'.$max_eventid.'-'.$lokasi['loc_id'].'-ap" style="color:red;" class="link_ap">'.$t_down.'</a>';
                    
                }
                elseif ( ($t_up == 0) and ($t_down == 0))
                { 
                    $warna = ' abu';
                    $fclass = ' fputih'; 
                    $trclass = '';
                }
                elseif ( ($t_up > 0) and ($t_down == 0))
                { 
                    $warna = ' hijau';
                    $fclass = ' fputih'; 
                    $trclass = '';
                }
                /** STYLING SWITCH COUNT STATISTIC */
                if ( ($t_down_switch > 0)) 
                {
                    $warna_switch = ' merah';
                    $fclass_switch = ' fmerah'; 
                    $trclass = 'highlight';
                    $link_t_down_switch = '<a href="#" id="'.$max_eventid.'-'.$lokasi['loc_id'].'-switch" style="color:red;" class="link_switch">'.$t_down_switch.'</a>';
                    
                }
                elseif ( ($t_up_switch == 0) and ($t_down_switch == 0))
                { 
                    $warna_switch = ' abu';
                    $fclass_switch = ' fputih'; 
                    $trclass = '';
                }
                elseif ( ($t_up_switch > 0) and ($t_down_switch == 0))
                { 
                    $warna_switch = ' hijau';
                    $fclass_switch = ' fputih'; 
                    $trclass = '';
                }
                
                $html .= '<tr class="'.$trclass.'">';                
                $html .= '<td class="tc">'.$no.'</td>';                
                $html .= '<td class="tl">'.strtoupper($lokasi['loc_area']).'</td>';                
                $html .= '<td class="tr">'.$t_up.'</td>';                
                $html .= '<td class="tr '.$fclass.'">'.$link_t_down.'</td>';                
                $html .= '<td class="tr">'.$total.'</td>';                
                $html .= '<td class="tr">'.$t_up_switch.'</td>';                
                $html .= '<td class="tr '.$fclass_switch.'">'.$link_t_down_switch.'</td>';                
                $html .= '<td class="tr">'.$total_switch.'</td>';                
                $html .= '</tr>';   
                $no++;
                $tt_up += $t_up; 
                $tt_down += $t_down; 
                $ttotal += $total; 
                $tt_up_switch += $t_up_switch; 
                $tt_down_switch += $t_down_switch; 
                $ttotal_switch += $total_switch; 
                
            } 
            
                $html .= '<tr>';                
                $html .= '<td colspan="2" class="kolomjudul alternate_black">TOTAL</td>';                
                $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_up.'</td>';                
                $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_down.'</td>';                
                $html .= '<td class="tr '.$fclass.' alternate_black">'.$ttotal.'</td>';                
                $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_up_switch.'</td>';                
                $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_down_switch.'</td>';                
                $html .= '<td class="tr '.$fclass.' alternate_black">'.$ttotal_switch.'</td>';                
                $html .= '</tr>';
                   
    if ($complete_table)
    {
            $html .= '</table>';
    }   
                $res = array();
                $res['total_sw_up'] = $tt_up_switch;
                $res['total_sw_down'] = $tt_down_switch;
                $res['total_ap_up'] = @$tt_up_ap;
                $res['total_ap_down'] = @$tt_down_ap;
                $res['html'] = $html;
                return $res;
    
}

function statistic_table_new($title,$data,$db_cacti,$conn_cacti,$table_prefix,$max_eventid,$db_icinga,$conn_icinga,$complete_table = true)
{

    if ($complete_table)
    {
            $html = '
        <table class="tlist fl" width="100%">
            <tr>
                <td colspan="8" class="tc kolomjudul">'.ucwords($title).'</td>
            </tr>
            <tr>
                <td class="kolomjudul alternate_black tc">No.</td>
                <td class="kolomjudul alternate_black">Location</td>
                <td class="tc alternate_black"><img title="Access Point UP" src="assets/images/wifi-up.png" class="img_wifi" /></td>
                <td class="tc alternate_black"><img title="Access Point Down" src="assets/images/wifi-down.png" class="img_wifi" /></td>
                <td class="alternate_black">Tot</td>
                <td class="tc alternate_black"><img title="Switch UP" src="assets/images/switch-up.png" class="img_wifi" /> O/C/A</td>
                <td class="tc alternate_black"><img title="Switch UP" src="assets/images/switch-down.png" class="img_wifi" /> O/C/A</td>
                <td class="alternate_black">Tot</td>
            </tr>';
    } else { $html = ''; }
            
            /** SQL STATUS AP */
            $sql_ap = "SELECT   a.loc_area, b.status, count(*) jml
            FROM     x_list_location a, x_p_map_event_nms b 
            where    a.loc_id = b.loc_id and a.event_id = ".$max_eventid." group by a.loc_area, b.status order by a.loc_area, b.status ";
            $res_ap = mysql_query($sql_ap,$conn_cacti);
            $rows_ap = array();
            while($row = mysql_fetch_assoc($res_ap))
            {
                $rows_ap[$row['loc_area']][$row['status']] = $row['jml'];
            }
    
            $ttotal = $tt_up = $tt_down = $ttotal_switch = $tt_up_switch = $tt_down_switch = 0;
            $no = 1;

            foreach($data as $hotel => $lokasi) {
                
                $t_down_switch = $t_up_switch = $t_up = $t_down = 0;
                $loc_id = $lokasi[0]['location_id'];
                $t_up = array_key_exists($hotel,$rows_ap) ? @$rows_ap[$hotel]['UP'] : 0;
                $t_down = array_key_exists($hotel,$rows_ap) ? @$rows_ap[$hotel]['DOWN'] : 0;
                foreach($lokasi as $row)
                {
                    $switchs[] = $row['ip_address'];
                    if ($row['current_state'] == '')
                        $t_down_switch++;                    
                    elseif ($row['current_state'] == 0)
                        $t_up_switch++;
                    elseif ($row['current_state'] == 1)
                        $t_down_switch++;
                        
                    $rows[] = $row;
                }
                
                #echo mysql_error();
                #print_out($res);
                #print_out($q);
                #print_out($rows);
                //die;
                $total = $t_up + $t_down;
                $total_switch = $t_up_switch + $t_down_switch;
                $link_t_down = $t_down;
                $link_t_down_switch = $t_down_switch;
                
                if ( ($t_down > 0)) 
                {
                    $warna = ' merah';
                    $fclass = ' fmerah'; 
                    $trclass = 'highlight';
                    #$link_t_down = '<a href="#" id="'.$max_eventid.'-'.$loc_id.'-'.$hotel.'-ap" style="color:red;" class="link_ap">'.$t_down.'</a>';
                    $link_t_down = '<a href="#" id="'.$max_eventid.'-'.$loc_id.'-'.$hotel.'-ap" style="color:red;" class="">'.$t_down.'</a>';
                    
                }
                elseif ( ($t_up == 0) and ($t_down == 0))
                { 
                    $warna = ' abu';
                    $fclass = ' fputih'; 
                    $trclass = '';
                }
                elseif ( ($t_up > 0) and ($t_down == 0))
                { 
                    $warna = ' hijau';
                    $fclass = ' fhijau'; 
                    $trclass = '';
                }
                $fclass_up = $fclass_down = ' style="color: #fff;" ';
                
                if ($t_up > 0)
                    $fclass_up = ' style="color: #82D711;" ';
                if ($t_down > 0)
                    $fclass_down = ' style="color: #FF0000;" ';
                
                /** STYLING SWITCH COUNT STATISTIC */
                if ( ($t_down_switch > 0)) 
                {
                    $warna_switch = ' merah';
                    /*
                    $fclass_switch_down = ' style="color: #FF0000;" ';
                    if ($t_up_switch > 0)
                        $fclass_switch_up = ' style="color: #fff;" '; 
                    */    
                    $trclass = 'highlight';
                    $link_t_down_switch = '<a href="#" id="'.$max_eventid.'-'.$loc_id.'-'.$hotel.'-1-switch" style="color:red;" class="link_switch">'.$t_down_switch.'</a>';
                    
                }
                elseif ( ($t_up_switch == 0) and ($t_down_switch == 0))
                { 
                    $warna_switch = ' abu';
                    $trclass = '';
                    /*                    
                    $fclass_switch_up = ' style="color: #fff;" ';
                    $fclass_switch_down = ' style="color: #fff;" ';
                    */ 
                }
                elseif ( ($t_up_switch > 0) and ($t_down_switch == 0))
                { 
                    $warna_switch = ' hijau';
                    $trclass = '';
                    /*
                    $fclass_switch_up = ' style="color: #82D711;" ';
                    $fclass_switch_down = ' style="color: #fff;" ';
                    */
                }
                elseif ( ($t_up_switch > 0))
                {
                    /*
                    $fclass_switch_up = ' style="color: #82D711;" ';
                    if ($t_down_switch > 0) 
                    $fclass_switch_down = ' style="color: #FF0000;" ';
                    */ 
                    $trclass = '';
                }
                
                $fclass_switch_up = $fclass_switch_down = ' style="color: #fff;" ';
                
                $link_t_up_switch = $t_up_switch;
                
                if ($t_up_switch > 0) {
                    $fclass_switch_up = ' style="color: #82D711;" ';
                    $link_t_up_switch = '<a href="#" id="'.$max_eventid.'-'.$loc_id.'-'.$hotel.'-0-switch" style="color:#82D711;" class="link_switch">'.$t_up_switch.'</a>';
                    }
                if ($t_down_switch > 0)
                    $fclass_switch_down = ' style="color: #FF0000;" ';
                
                $link_total_switch = '<a href="#" id="'.$max_eventid.'-'.$loc_id.'-'.$hotel.'-ALL-switch" style="color:#FFF;" class="link_switch">'.$total_switch.'</a>';              
            
                
                $html .= '<tr class="'.$trclass.'">';                
                $html .= '<td class="tc">'.$no.'</td>';                
                $html .= '<td class="tl">'.strtoupper($hotel).'</td>';                
                $html .= '<td class="tr"'.$fclass_up.'>'.$t_up.'</td>';                
                $html .= '<td class="tr"'.$fclass_down.'>'.$link_t_down.'</td>';                
                $html .= '<td class="tr">'.$total.'</td>';                
                $html .= '<td class="tr"'.$fclass_switch_up.'>'.$link_t_up_switch.'</td>';                
                $html .= '<td class="tr"'.$fclass_switch_down.'>'.$link_t_down_switch.'</td>';                
                $html .= '<td class="tr">'.$link_total_switch.'</td>';                
                $html .= '</tr>';   
                $no++;
                $tt_up += $t_up; 
                $tt_down += $t_down; 
                $ttotal += $total; 
                $tt_up_switch += $t_up_switch; 
                $tt_down_switch += $t_down_switch; 
                $ttotal_switch += $total_switch; 
                
            } 
            
                $html .= '<tr>';                
                $html .= '<td colspan="2" class="kolomjudul alternate_black">TOTAL</td>';                
                $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_up.'</td>';                
                $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_down.'</td>';                
                $html .= '<td class="tr '.$fclass.' alternate_black">'.$ttotal.'</td>';                
                $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_up_switch.'</td>';                
                $html .= '<td class="tr '.$fclass.' alternate_black">'.$tt_down_switch.'</td>';                
                $html .= '<td class="tr '.$fclass.' alternate_black">'.$ttotal_switch.'</td>';                
                $html .= '</tr>';
                   
    if ($complete_table)
    {
            $html .= '</table>';
    }   
                $res = array();
                $res['total_sw_up'] = $tt_up_switch;
                $res['total_sw_down'] = $tt_down_switch;
                $res['total_ap_up'] = @$tt_up_ap;
                $res['total_ap_down'] = @$tt_down_ap;
                $res['html'] = $html;
                return $res;
    
}
 
 
function format_traffic($traffic)
{
    $str_traffic = "";
    $traffic = is_numeric($traffic) ? ceil(($traffic / 1048576)) : $traffic;
    if ($traffic > 1000)
    { $str_traffic = number_format(($traffic / 1000),2,',','.'); $satuan = 'GB'; }
    else             
    { $str_traffic = number_format($traffic,2,',','.'); $satuan = 'MB'; }
    
    return $str_traffic.' '.$satuan;
    
}

function format_uptime($uptime,$inmSec = true)
{
    
    $multiplier = 1; // Default dalam detik
    if ($inmSec)    
        $multiplier = $multiplier * 1000;
    
    $oneD = 86400 * $multiplier; 
    $oneH = 3600 * $multiplier;
    $oneMin = 60 * $multiplier;
    $oneSec = 1 * $multiplier;
    
    $day = floor($uptime / $oneD);
    $h = floor(($uptime % $oneD) / $oneH); 
    $m = floor( (($uptime % $oneD) % $oneH) / $oneMin );
    $s = floor( ((($uptime % $oneD) % $oneH) % $oneMin ) / $oneSec);

    #$UPTIME = $day . 'd ' . $h . 'h:' . $m . 'm:' . $s . 's';
    $UPTIME = $day . 'd ' . $h . 'h:' . $m . 'm:';
    $UPTIME = !empty($uptime) ? $UPTIME : 'Unknown';
    return $UPTIME;
}

function grabUrl($url)
{
    $ctn = file_get_contents($url);
    return json_decode($ctn,true);    
}

function debugy($var) 
{
	echo "<pre>";
	print_r($var);
	echo "</pre>";
			die();
}

function ceklogin($uid) 
{


if( $_SESSION['auth'] =='' or $_SESSION['uid'] == '') 
{
	 header('Location: login.php');
}
}


//$treshold batas maksimal waktu eskalasi tiket (dalam menit)
function cron_treshold($conn, $db, $treshold)
{
	
	
	
	$sql ="SELECT IF((TIMESTAMPDIFF(MINUTE , a.start_date_problem, NOW( ) ) >".$treshold." AND a.status <> 'closed' ) , 1, 0 ) AS sending,a.*, b.location_name FROM z_tickets a inner join z_venue b on a.venue_id=b.venue_id";
$res = mysql_query($sql,$conn);
//$loc = mysql_fetch_assoc($res);
					$loc=array();
    			while($row = mysql_fetch_assoc($res))
    			{
    				$message = $row[ticket_id].":".$row[description].",loc:".$row['location_name'].",sts:".$row[status];	
    			
    			
    			   if($row[sending]==1) 
    			   {
    			  
							   	$mobile = getmobile($conn, $db, 'group', 7);
							   	foreach($mobile as $m) 
							   	{
							   	  sending_sms($m, $message);	
							   	}
							   
							 
    			     	
    			     	
    			   
    			   
    			   }
    			}
	
}


function sending_sms($no, $text)
	{
	 
if($no!="" && $text!="") {

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "http://servicebus.telkom.co.id:9001/TelkomSystem/SMSGateway/Services/ProxyService/smsBulk?msisdn=".$no."&message=".$text);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,false);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
  curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);

$data=curl_exec($ch);
curl_close($ch);

if($data='SUCCESS<br>') {
return  true;
} else {

return false;
}

return false;
}
}


function getmobile($conn, $db, $param='user', $id) {
		
		
		
		if($param='user'){
	$sql = "select * from z_users a inner join z_person b on a.person_id=b.person_id where a.id=".$id;
			try{
		
		$res = mysql_query($sql,$conn);
    $loc = mysql_fetch_assoc($res);
			if($loc[mobile_phone]!=""){
	
					return $loc[mobile_phone];
				}else{
					return "";
			}
		} catch (Exception $e) {
			
			return "";

		}
		
		
			} elseif($param='group') {
	$sql = "select * from z_users a inner join z_person b on a.person_id=b.person_id inner join z_users_groups c on a.id =c.uid where c.gid=".$id;
	 }
	 
	 
	 
	 
				try{
					$res = mysql_query($sql,$conn);
    			//$loc = mysql_fetch_assoc($res);
    			$loc=array();
    			while($row = mysql_fetch_assoc($res))
    			{
    				$loc[]=$row;
    			}
    	$data=array();
			
			foreach ($loc as $lk) 
			{
				
				 if($lk['mobile_phone']!="") {
			   $data[]=	$lk['mobile_phone'];
					}
			}
			
			
			if($data){
	
					return $data;
				}else{
					return "";
			}
		} catch (Exception $e) {
			//die("SSS");
			return "";

		}
			
	}



}
