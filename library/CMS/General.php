<?php

class CMS_General {
	private $file;
	private $config = array();
	private $section;

	function find_date_id($string) {

		$shortenize = function($string) {
			return substr($string, 0, 3);
		}
		;
		// Define month name:
		$month_names = array("januari",
							 "februari",
							 "maret",
							 "april",
							 "mei",
							 "juni",
							 "juli",
							 "agustus",
							 "september",
							 "oktober",
							 "nopember",
							 "desember");
		$short_month_names = array_map($shortenize, $month_names);
		// Define day name
		$day_names = array("senin",
						   "selasa",
						   "rabu",
						   "kamis",
						   "jumat",
						   "sabtu",
						   "minggu");
		$short_day_names = array_map($shortenize, $day_names);
		// Define ordinal number
		$ordinal_number =['st', 'nd', 'rd', 'th'];
		$day = "";
		$month = "";
		$year = "";
		// Match dates: 01/01/2012 or 30-12-11 or 1 2 1985
		preg_match('/([0-9]?[0-9])[\.\-\/ ]+([0-1]?[0-9])[\.\-\/ ]+([0-9]{2,4})/', $string, $matches);
		if($matches) {
			if($matches[1])
				$day = $matches[1];
			if($matches[2])
				$month = $matches[2];
			if($matches[3])
				$year = $matches[3];
		}
		// Match dates: Sunday 1st March 2015; Sunday, 1 March 2015; Sun 1 Mar 2015; Sun-1-March-2015
		preg_match('/(?:(?:' . 
						 implode('|', $day_names). 
								 '|' . 
								 implode('|', $short_day_names). 
										 ')[ ,\-_\/]*)?([0-9]?[0-9])[ ,\-_\/]*(?:)?[ ,\-_\/]*(' . 
																							  implode('|', $month_names). 
																									  '|' . 
																									  implode('|', $short_month_names). 
																											  ')[ ,\-_\/]+([0-9]{4})/i', $string, $matches);
		if($matches) {
			if(empty($day)&& $matches[1])
				$day = $matches[1];
			if(empty($month)&& $matches[2]) {
				$month = array_search(strtolower($matches[2]), $short_month_names);
				if(!$month)
					$month = array_search(strtolower($matches[2]), $month_names);
				$month = $month + 1;
			}
			if(empty($year)&& $matches[3])
				$year = $matches[3];
		}
		// Match dates: March 1st 2015; March 1 2015; March-1st-2015
		preg_match('/(' . 
					  implode('|', $month_names). 
							  '|' . 
							  implode('|', $short_month_names). 
									  ')[ ,\-_\/]*([0-9]?[0-9])[ ,\-_\/]*(?:' . 
																		  implode('|', $ordinal_number). 
																				  ')?[ ,\-_\/]+([0-9]{4})/i', $string, $matches);
		if($matches) {
			if(empty($month)&& $matches[1]) {
				$month = array_search(strtolower($matches[1]), $short_month_names);
				if(!$month)
					$month = array_search(strtolower($matches[1]), $month_names);
				$month = $month + 1;
			}
			if(empty($day)&& $matches[2])
				$day = $matches[2];
			if(empty($year)&& $matches[3])
				$year = $matches[3];
		}
		// Match month name:
		if(empty($month)) {
			preg_match('/(' . 
						  implode('|', $month_names). 
								  ')/i', $string, $matches_month_word);
			if($matches_month_word && $matches_month_word[1])
				$month = array_search(strtolower($matches_month_word[1]), $month_names);
			// Match short month names
			if(empty($month)) {
				preg_match('/(' . 
							  implode('|', $short_month_names). 
									  ')/i', $string, $matches_month_word);
				if($matches_month_word && $matches_month_word[1])
					$month = array_search(strtolower($matches_month_word[1]), $short_month_names);
			}
			$month = $month + 1;
		}
		// Match 5th 1st day:
		if(empty($day)) {
			preg_match('/([0-9]?[0-9])(' . 
									   implode('|', $ordinal_number). 
											   ')/', $string, $matches_day);
			if($matches_day && $matches_day[1])
				$day = $matches_day[1];
		}
		// Match Year if not already setted:
		if(empty($year)) {
			preg_match('/[0-9]{4}/', $string, $matches_year);
			if($matches_year && $matches_year[0])
				$year = $matches_year[0];
		}
		if(!empty($day)&& !empty($month)&& empty($year)) {
			preg_match('/[0-9]{2}/', $string, $matches_year);
			if($matches_year && $matches_year[0])
				$year = $matches_year[0];
		}
		// Day leading 0
		if(1 == strlen($day))
			$day = '0' . $day;
		// Month leading 0
		if(1 == strlen($month))
			$month = '0' . $month;
		// Check year:
		if(2 == strlen($year)&& $year > 20)
			$year = '19' . $year;
		else if(2 == strlen($year)&& $year < 20)
			$year = '20' . $year;
		$date = array('year' =>$year,
					  'month' =>$month,
					  'day' =>$day);
		// Return false if nothing found:
		if(empty($year)&& empty($month)&& empty($day))
			return false;
		else 
			return $date['day'] . "-" . $date['month'] . "-" . $date['year'];
	}

	function find_date($string) {

		$shortenize = function($string) {
			return substr($string, 0, 3);
		}
		;
		// Define month name:
		$month_names = array("january",
							 "february",
							 "march",
							 "april",
							 "may",
							 "june",
							 "july",
							 "august",
							 "september",
							 "october",
							 "november",
							 "december");
		$short_month_names = array_map($shortenize, $month_names);
		// Define day name
		$day_names = array("monday",
						   "tuesday",
						   "wednesday",
						   "thursday",
						   "friday",
						   "saturday",
						   "sunday");
		$short_day_names = array_map($shortenize, $day_names);
		// Define ordinal number
		$ordinal_number =['st', 'nd', 'rd', 'th'];
		$day = "";
		$month = "";
		$year = "";
		// Match dates: 01/01/2012 or 30-12-11 or 1 2 1985
		preg_match('/([0-9]?[0-9])[\.\-\/ ]+([0-1]?[0-9])[\.\-\/ ]+([0-9]{2,4})/', $string, $matches);
		if($matches) {
			if($matches[1])
				$day = $matches[1];
			if($matches[2])
				$month = $matches[2];
			if($matches[3])
				$year = $matches[3];
		}
		// Match dates: Sunday 1st March 2015; Sunday, 1 March 2015; Sun 1 Mar 2015; Sun-1-March-2015
		preg_match('/(?:(?:' . 
						 implode('|', $day_names). 
								 '|' . 
								 implode('|', $short_day_names). 
										 ')[ ,\-_\/]*)?([0-9]?[0-9])[ ,\-_\/]*(?:' . 
																			   implode('|', $ordinal_number). 
																					   ')?[ ,\-_\/]*(' . 
																									 implode('|', $month_names). 
																											 '|' . 
																											 implode('|', $short_month_names). 
																													 ')[ ,\-_\/]+([0-9]{4})/i', $string, $matches);
		if($matches) {
			if(empty($day)&& $matches[1])
				$day = $matches[1];
			if(empty($month)&& $matches[2]) {
				$month = array_search(strtolower($matches[2]), $short_month_names);
				if(!$month)
					$month = array_search(strtolower($matches[2]), $month_names);
				$month = $month + 1;
			}
			if(empty($year)&& $matches[3])
				$year = $matches[3];
		}
		// Match dates: March 1st 2015; March 1 2015; March-1st-2015
		preg_match('/(' . 
					  implode('|', $month_names). 
							  '|' . 
							  implode('|', $short_month_names). 
									  ')[ ,\-_\/]*([0-9]?[0-9])[ ,\-_\/]*(?:' . 
																		  implode('|', $ordinal_number). 
																				  ')?[ ,\-_\/]+([0-9]{4})/i', $string, $matches);
		if($matches) {
			if(empty($month)&& $matches[1]) {
				$month = array_search(strtolower($matches[1]), $short_month_names);
				if(!$month)
					$month = array_search(strtolower($matches[1]), $month_names);
				$month = $month + 1;
			}
			if(empty($day)&& $matches[2])
				$day = $matches[2];
			if(empty($year)&& $matches[3])
				$year = $matches[3];
		}
		// Match month name:
		if(empty($month)) {
			preg_match('/(' . 
						  implode('|', $month_names). 
								  ')/i', $string, $matches_month_word);
			if($matches_month_word && $matches_month_word[1])
				$month = array_search(strtolower($matches_month_word[1]), $month_names);
			// Match short month names
			if(empty($month)) {
				preg_match('/(' . 
							  implode('|', $short_month_names). 
									  ')/i', $string, $matches_month_word);
				if($matches_month_word && $matches_month_word[1])
					$month = array_search(strtolower($matches_month_word[1]), $short_month_names);
			}
			$month = $month + 1;
		}
		// Match 5th 1st day:
		if(empty($day)) {
			preg_match('/([0-9]?[0-9])(' . 
									   implode('|', $ordinal_number). 
											   ')/', $string, $matches_day);
			if($matches_day && $matches_day[1])
				$day = $matches_day[1];
		}
		// Match Year if not already setted:
		if(empty($year)) {
			preg_match('/[0-9]{4}/', $string, $matches_year);
			if($matches_year && $matches_year[0])
				$year = $matches_year[0];
		}
		if(!empty($day)&& !empty($month)&& empty($year)) {
			preg_match('/[0-9]{2}/', $string, $matches_year);
			if($matches_year && $matches_year[0])
				$year = $matches_year[0];
		}
		// Day leading 0
		if(1 == strlen($day))
			$day = '0' . $day;
		// Month leading 0
		if(1 == strlen($month))
			$month = '0' . $month;
		// Check year:
		if(2 == strlen($year)&& $year > 20)
			$year = '19' . $year;
		else if(2 == strlen($year)&& $year < 20)
			$year = '20' . $year;
		$date = array('year' =>$year,
					  'month' =>$month,
					  'day' =>$day);
		// Return false if nothing found:
		if(empty($year)&& empty($month)&& empty($day))
			return false;
		else 
			return $date['day'] . "-" . $date['month'] . "-" . $date['year'];
	}

	function cachefunc($data = array()) {
		$config = new Zend_Config_Ini(APPLICATION_PATH . 
									  '/configs/application.ini', 'production');
		$config = $config->toArray();
		$vconfig = $config['resources']['cache'];
		if(is_null($data['backendcache'])) {
			$data['backendcache'] = $vconfig['backEnd'];
		}
		$cache_time = $vconfig['frontEndOptions']['lifetime'];
		if($data['cache_time'] == "") {
			$data['cache_time'] = 0;
		}
		if($data['cache_time'] == '~') {
			$front = array('lifetime' => NULL,
						   'automatic_serialization' => true);
		} elseif($data['cache_time'] == '0') {
			$front = array('caching' => FALSE,
						   'lifetime' => ( int )$data['cache_time'],
						   'automatic_serialization' => true);
		} elseif($data['cache_time'] != NULL) {
			$front = array('lifetime' => ( int )$data['cache_time'],
						   'automatic_serialization' => true);
		} else {
			$front = array('lifetime' =>$cache_time,
						   'automatic_serialization' => true);
		}
		switch($data['backendcache']) {
			case 'file' : default : $back = array('cache_dir' => APPLICATION_PATH . '/../cache/');
			return Zend_Cache::factory('Core', 'File', $front, $back);
			break;
			case 'memcached' : $backendOpts = array('servers' => array(array('host' =>$config['data']['server']['memcached'],
													'port' =>$config['data']['port']['memcached'])),
													'compression' => false);
			return Zend_Cache::factory('Core', 'Memcached', $front, $backendOpts);
			break;
			case 'mongodb' : $backendOptions = array('database_name' => 'zend_cache',
													 'collection' => 'cache');
			return Zend_Cache::factory('Core', 'MongoDb', $front, $backendOptions);
			break;
			case 'libmemcached' : $backendOptions = array('database_name' => 'zend_cache',
														  'collection' => 'cache');
			return Zend_Cache::factory('Core', 'Libmemcached', $front, array());
			break;
			case 'redis' : return Zend_Cache::factory(new Zend_Cache_Core($front, new Extended_Cache_Backend_Redis(array('servers' => array(array('host' => $config['data']['server']['redis'], 'port' => $config['data']['port']['redis'], 'dbindex' => 1))))));
			break;
		}
	}

	function setIni($file) {
		$this->setFile($file);
	}

	public function getSection() {
		return $this->section;
	}

	public function setSection($section) {
		$this->section = $section;
	}

	public function getFile() {
		return $this->file;
	}

	public function setFile($file) {
		$this->file = $file;
	}

	public function loadConfig() {
		$this->config = parse_ini_file($this->getFile(), true);
	}

	public function getParamValue($paramName) {
		if(isset($this->config[$this->section][$paramName])) {
			return $this->config[$this->section][$paramName];
		} else {
			return "NA";
		}
	}

	public static function debug($var) {
		echo "<pre>";
		print_r($var);
		echo "</pre>";
		die();
	}

	function time_elapsed_string($ptime) {
		$etime = time()- $ptime;
		if($etime < 1) {
			return '0 seconds';
		}
		$a = array(365 * 24 * 60 * 60 => 'year',
				   30 * 24 * 60 * 60 => 'month',
				   24 * 60 * 60 => 'day',
				   60 * 60 => 'hour',
				   60 => 'minute',
				   1 => 'second');
		$a_plural = array('year' => 'years',
						  'month' => 'months',
						  'day' => 'days',
						  'hour' => 'hours',
						  'minute' => 'minutes',
						  'second' => 'seconds');

		foreach($a as $secs => $str) {
			$d = $etime / $secs;
			if($d >= 1) {
				$r = round($d);
				return $r . ' ' .($r > 1 ? $a_plural[$str] : $str). ' ago';
			}
		}
	}

	public function get_string_between($string, $start, $end) {
		//die($string);
		$string = " " . $string;
		$ini = strpos($string, $start);
		if($ini == 0)
			return "";
		$ini += strlen($start);
		$len = strpos($string, $end, $ini)- $ini;
		return substr($string, $ini, $len);
	}

	public function replaceTags($source, $startPoint, $endPoint, $newText) {
		return preg_replace('#(' . 
							   preg_quote($startPoint). 
										  ')(.*)(' . 
												 preg_quote($endPoint). 
															')#si', '$1' . 
															$newText . 
															'$3', $source);
	}

	public function objectToArray($obj) {
		if(is_object($obj))
			$obj = (array) $obj;
		if(is_array($obj)) {
			$new = array();

			foreach($obj as $key => $val) {
				$new[$key] = $this->objectToArray($val);
			}
		} else 
			$new = $obj;
		return $new;
	}

	public function arrayToObject($array) {
		if(!is_array($array)) {
			return $array;
		}
		//Zend_Debug::dump($array); die();
		$object = new stdClass();
		if(is_array($array)&& count($array)> 0) {

			foreach($array as $name => $value) {
				$name = strtolower(trim($name));
				if(!empty($name)) {
					$object->$name = $this->arrayToObject($value);
				}
			}
			return $object;
		} else {
			return FALSE;
		}
	}

	public function get_ip_address() {
		if(isset($_SERVER)) {
			if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} elseif(isset($_SERVER['HTTP_CLIENT_IP'])) {
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			} else {
				$ip = $_SERVER['REMOTE_ADDR'];
			}
		} else {
			if(getenv('HTTP_X_FORWARDED_FOR')) {
				$ip = getenv('HTTP_X_FORWARDED_FOR');
			} elseif(getenv('HTTP_CLIENT_IP')) {
				$ip = getenv('HTTP_CLIENT_IP');
			} else {
				$ip = getenv('REMOTE_ADDR');
			}
		}
		return $ip;
	}

	function curPageURL() {
		$pageURL = 'http';
		if($_SERVER["HTTPS"] == "on") {
			$pageURL .= "s";
		}
		$pageURL .= "://";
		if($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
		} else {
			$pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
		}
		return $pageURL;
	}

	function array2xml($array, $xml = false) {
		if($xml === false) {
			$xml = new SimpleXMLElement('<root/>');
		}

		foreach($array as $key => $value) {
			if(is_array($value)) {
				array2xml($value, $xml->addChild($key));
			} else {
				$xml->addChild($key, $value);
			}
		}
		return $xml->asXML();
	}

	function __unserialize($text) {
		$text = unserialize(base64_decode($text));
		$text = preg_replace(array("@&#58;@", "@&#37;@", "@&###;@", "@&#60;@", "@&#62;@"), array(':', '%', '/', '<', '>'), $text);
		return $text;
	}

	function __serialize($text) {
		$text = preg_replace(array("@:@", "@%@", "@\/@", "@\<@", "@\>@"), array('&#58;', '&#37;', '&###;', '&#60;', '&#62;'), $text);
		return base64_encode(serialize($text));
	}

	function humanTiming($time) {
		$time = time()- $time;
		// to get the time since that moment
		$time =($time < 1)? 1 : $time;
		$tokens = array(31536000 => 'year',
						2592000 => 'month',
						604800 => 'week',
						86400 => 'day',
						3600 => 'hour',
						60 => 'minute',
						1 => 'second');

		foreach($tokens as $unit => $text) {
			if($time < $unit)
				continue;
			$numberOfUnits = floor($time / $unit);
			return $numberOfUnits . ' ' . $text .(($numberOfUnits > 1)? 's' : '');
		}
	}

	public function conn_odbc($db, $user, $password) {
		try {
			$conn = odbc_connect($db, $user, $password);
			return $conn;
		}
		catch(Exception $e) {
			echo $e->getMessage();
		}
	}

	function get_x_words($string, $x = 200) {
		$parts = explode(' ', $string);
		if(sizeof($parts)> $x) {
			$parts = array_slice($parts, 0, $x);
		}
		echo implode(' ', $parts);
	}

	function chop_string($string, $x = 200) {
		$string = strip_tags(stripslashes($string));
		// Zend_Debug::dump($string);
		// convert to plaintext
		// $tmp = substr($string, 0, strpos(wordwrap($string, $x), "\n"));
		// Zend_Debug::dump($tmp);
		if(strlen($string)> $x){
			return substr($string, 0, strpos(wordwrap($string, $x), "\n"));
		}else{
			return $string;
		}
	}
}
