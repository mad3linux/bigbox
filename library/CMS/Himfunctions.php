<?php
class CMS_Himfunctions
{
	function curl_download($Url){
	 
		// is cURL installed yet?
		if (!function_exists('curl_init')){
			die('Sorry cURL is not installed!');
		}
	 
		// OK cool - then let's create a new cURL resource handle
		$ch = curl_init();
	 
		// Now set some options (most are optional)
	 
		// Set URL to download
		curl_setopt($ch, CURLOPT_URL, $Url);
	 
		// Set a referer
		//curl_setopt($ch, CURLOPT_REFERER, "http://www.example.org/yay.htm");
	 
		// User agent
		//curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
	 
		// Include header in result? (0 = yes, 1 = no)
		curl_setopt($ch, CURLOPT_HEADER, 0);
	 
		// Should cURL return or print out the data? (true = return, false = print)
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	 
		// Timeout in seconds
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	 
		// Download the given URL, and return output
		$output = curl_exec($ch);
	 
		// Close the cURL resource, and free system resources
		curl_close($ch);
	 
		return $output;
	}
	
	function curl_whatsapp($target,$msg){
	 
		// is cURL installed yet?
		if (!function_exists('curl_init')){
			die('Sorry cURL is not installed!');
		}
	 
		// OK cool - then let's create a new cURL resource handle
		$ch = curl_init();
	 
		// Now set some options (most are optional)
	 
		// Set URL to download
		curl_setopt($ch, CURLOPT_URL, "http://telkomcare.telkom.co.id/api/wa/action.php");
	 
		// Set a referer
		//curl_setopt($ch, CURLOPT_REFERER, "http://www.example.org/yay.htm");
	 
		// User agent
		//curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
	 
		// Include header in result? (0 = yes, 1 = no)
		curl_setopt($ch, CURLOPT_HEADER, 0);
		
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,"act=sendmessage&target=".$target."&msg=".$msg);

		// Should cURL return or print out the data? (true = return, false = print)
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	 
		// Timeout in seconds
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	 
		// Download the given URL, and return output
		$output = curl_exec($ch);
	 
		// Close the cURL resource, and free system resources
		curl_close($ch);
	 
		return $output;
	}
	
	function get_content($url){
		$content = file_get_contents($url);
		return $content;
	}

	function get_content_basic($url,$username,$password){
		$context = stream_context_create(array(
			'http' => array(
				'header'  => "Authorization: Basic " . base64_encode("$username:$password")
			)
		));
		$data = file_get_contents($url, false, $context);
		
		return $data;
	}

	function my_dump($dump){
		echo '<pre>';
		var_dump($dump);
		echo '</pre>';
	}

	function my_dump_exit($dump){
		echo '<pre>';
		var_dump($dump);
		echo '</pre>';
		exit;
	}

	function _isCurl(){
	    return function_exists('curl_version');
	}

	function get_curl($url){
		if($this->_isCurl()){
			$ch = curl_init();
			
			curl_setopt($ch,CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
			
			$result = curl_exec($ch);
			curl_close($ch);
			
			return $result;
		}else{
			die("CURL NOT AVAILABLE.");
		}
	}

	function get_curl_basic($url,$username,$password){
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		
		$output = curl_exec($ch);
		//$info = curl_getinfo($ch);
		
		curl_close($ch);
		
		return $output;
	}

	function calculate_second($s){
		$day = (int)($s/86400);
		
		if($day!=0){
			$dif = $s-(86400*$day);
		}else{
			$dif = $s;
		}
		
		if($day>1){
			$dis_day = $day.' days ';
		}else{
			$dis_day = $day.' day ';
		}
		$dis_day = $day.'d ';
		
		$hour = (int)($dif/3600);
		
		if($hour!=0){
			$dif = $dif-(3600*$hour);
		}else{
			$dif = $dif;
		}
		
		if($hour>1){
			$dis_hour = $hour.' hours ';
		}else{
			$dis_hour = $hour.' hour ';
		}
		$dis_hour = $hour.'h ';
		
		$minute = (int)($dif/60);
		
		if($minute!=0){
			$dif = $dif-(60*$minute);
		}else{
			$dif = $dif;
		}
		
		if($minute>1){
			$dis_minute = $minute.' minutes ';
		}else{
			$dis_minute = $minute.' minute ';
		}
		$dis_minute = $minute.'m ';
		
		$second = (int)($dif);
		
		if($second>1){
			$dis_second = $second.' seconds ';
		}else{
			$dis_second = $second.' second ';
		}
		$dis_second = $second.'s ';
		
		return $dis_day.$dis_hour.$dis_minute.$dis_second;
	}
	
	function curPageURL($uri = false) {
		$pageURL = 'http';
		if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
		if($uri){
			$pageURL = $_SERVER["REQUEST_URI"];
		}else{
			$pageURL .= "://";
			if ($_SERVER["SERVER_PORT"] != "80") {
				$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
			} else {
				$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
			}
		}
		return $pageURL;
	}
}
