<?php 
class Hook
{
    protected static $hooks = [];

    public function listen($hook, $closure = null)
    {
        if (!isset(static::$hooks[$hook])) static::$hooks[$hook] = [];

        static::$hooks[$hook][] = $closure;

        return $this;
    }

   
    public function fire($hook, $result = '')
    {
        if (!isset(static::$hooks[$hook])) return $result;


        foreach(static::$hooks[$hook] as $closure) {

            $result = call_user_func($closure, $result);

        }

        return $result;
    }
}