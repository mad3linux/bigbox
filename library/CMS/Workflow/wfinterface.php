<?php

/**
 * @file
 * maestro_task_interface.class.php
 */
 

require_once('wftaskinterface.php');

class CMS_Workflow_wfinterface {
  private $_template_id;

  function __construct($template_id) {
		   $this->_template_id = $template_id;
  }

  //displays the main task page
  function displayPage() {
    
    $maestro_url = 'http://localhost:8127';
    
    $build['workflow_template'] = array(
      '#tid' => $this->_template_id,
      '#mi' => $this,
      '#maestro_url' => $maestro_url,
      
    );
  
 
    return $this;
  
  
  }

  function displayTasks() {
    $html = '';
   
    
    $c = new Model_Zprabawf();
    
    
    $res=$c->get_process($this->_template_id);
    
 //   Zend_Debug::dump($res); die();
    
    foreach ($res as $rec) {
        $task_type = substr($rec->TASK_CLASS_NAME,15);
        $task_class = 'MaestroTaskInterface' . $task_type;
        
       //die($task_class);
        
        if (class_exists($task_class)) {
        $ti = new $task_class($rec->ID);
      

        }
      else {
   
        $ti = new MaestroTaskInterfaceUnknown($rec->ID, 0, $task_class);
      }

      $html .= $ti->displayTask();
      
    }
    
    
  
    return $html;
  }

  //should get the valid task types to create, excluding start and end tasks, from the drupal cache
  function getContextMenu() {
    //we need to rebuild the cache in the event it is empty.
    $c = new  CMS_Workflow_wfgeneral();
    $options =$c->wf_get_taskobject_info();
		return $options;
  }

  function getContextMenuHTML() {
    $options = $this->getContextMenu();
    $html = "<div id=\"maestro_main_context_menu\" class=\"maestro_context_menu\"><ul>\n";
   foreach ($options as $key => $option) {
      $task_type =$option['class_name'];
      $option =  $option['display_name'];
      $html .= "<li style=\"white-space: nowrap;\" id=\"$task_type\">$option</li>\n";
    }
    $html .= "</ul></div>\n";

    return $html;
  }

  function getContextMenuJS() {
    $options = $this->getContextMenu();
    $js  = "(function ($) {\n";
    $js .= "\$('#maestro_workflow_container').contextMenu('maestro_main_context_menu', {\n";
    $js .= "menuStyle: {\n";
    $js .= "width: 175,\n";
    $js .= "fontSize: 12,\n";
    $js .= "},\n";

    $js .= "itemStyle: {\n";
    $js .= "padding: 0,\n";
    $js .= "paddingLeft: 10,\n";
    $js .= "},\n";

    $js .= "bindings: {\n";

    foreach ($options  as $key => $option) {
      $task_type = $option['class_name'];
      $js .= "'$task_type': function(t) {\n";
      $js .= "enable_ajax_indicator();\n";
      $js .= "\$.ajax({
        type: 'POST',
        url: ajax_url + '$task_type/taskid/0/tempid/{$this->_template_id}/act/create/format/json',
        cache: false,
        data: {task_type: '$task_type', offset_left: posx, offset_top: posy},
        dataType: 'json',
        success: add_task_success,
        error: editor_ajax_error
      });\n";
      $js .= "},\n";
    }

    $js .= "}\n";
    $js .= "});\n";
    $js .= "})(jQuery);\n";

    return $js;
  }

  function initializeJavascriptArrays() {
    $js = '';

    $c = new Model_Zprabawf();
    $res=$c->get_process($this->_template_id);

   // Zend_Debug::dump($res);die();
    $i = 0;
    $j = 0;
    foreach ($res as $rec) {
      $js .= "existing_tasks[{$i}] = ['task{$rec['ID']}', {$rec['offset_left']}, {$rec['offset_left']}];\n";
      $i++;

      $c2 = new Model_Zprabawf();

      $res2 = $c2->fetchdatafrom($rec['pid']);
      // Zend_Debug::dump($res2);die("a");
      if($res2){
      foreach ($res2 as $rec2) {
        $to = intval ($rec2['process_to']);
        $to_false = intval ($rec2['process_to_false']);
        if ($to != 0) {
          $js .= "line_ids[{$j}] = ['task{$rec[pid]}', 'task{$to}', '{$rec2['line_type']}'];\n";
          $j++;
        }
        if ($to_false != 0) {
          $js .= "line_ids[{$j}] = ['task{$re[pid]}', 'task{$to_false}', false];\n";
          $j++;
        }
      }
    }
    }

    return $js;
  }
}

?>
