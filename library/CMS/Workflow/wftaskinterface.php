<?php

/*
 * Copyright (C) Prabtech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

require_once (APPLICATION_PATH).'/fixed_libs/maestro/maestro_constants.class.php';


abstract class CMS_Workflow_wftaskinterface
{
  protected $_task_id;
  protected $_template_id;
  protected $_task_type;
  protected $_is_interactive;
  protected $_task_data;  //used when fetching task information
  protected $_task_edit_tabs;
  protected $_taskname;

  function __construct($task_id=0, $template_id=0, $ltype="default") {
    $this->_task_id = $task_id;
    $this->_task_data = NULL;
    $this->_ltype = $ltype;
    $this->_task_process_data = NULL;

    if ($template_id == 0 && $task_id > 0) {
     
      $c = new Model_Zprabawf();
      $rec = $c->get_process_by_pid($task_id);

	 // Zend_Debug::Dump($rec); die('qqq');
      if($rec){
      $this->_taskname = $rec['class_type'];
      $this->_template_id = intval(@($rec['act_id']));
      }
      
    }
    else {
      $this->_template_id = $template_id;
    }

    if ($this->_is_interactive == MaestroInteractiveFlag::IS_INTERACTIVE) {
      $this->_task_edit_tabs = array('assignment' => 1, 'notification' => 1);
    }
    else {
      $this->_task_edit_tabs = array();
    }
  }


function task_type_operator($id, $vars, $task_class){
	$cd = new Model_Zprabawf();
    $conn = $cd->get_conn_name();
	$this->_task_id;
	$data = unserialize($vars['zparams_serialize']);
	$optype  = $data['OperatorType'];
	$out = '<script> 
		$( "#set" ).hide();
		$( "#mapping" ).hide(); 
		$( ".start" ).hide(); 
		$( "#ifmapping" ).hide();';
	if($optype=="startlooping") {
		$out .= '$( ".start" ).show(); ';
	} else {
		$out .= '$( ".start" ).hide(); ';
	}	
	if($optype=="branch") {
		$out .= '$( "#ifmapping" ).show(); ';
	} else {
		$out .= '$( "#ifmapping" ).hide(); ';
	}
	
    if($optype=="map") {
		$out .= '$( "#mapping" ).show();';
	} else {
		$out .= '$( "#mapping" ).hide(); ';
	}
	
	if($optype=="setreturn") {
		$out .= '$( "#set" ).show();';
	} else {
		$out .= '$( "#set" ).hide(); ';
	}
	if($optype=="custom") {
		$out .= '$( ".acustom" ).show();
		$.ajax({
			url: "/prabawf/getcustom/xid/'.$data['OperatorData']['model'].'/xid2/'.$data['OperatorData']['method'].'",
				dataType: "html",	
				beforeSend: function() {
				   
				},
				success: function(data) {
						$("#custom").html(data);
				},
				timeout:9000,
				error:function(){
					
				},
			});
		';
	} else {
		$out .= '$( ".acustom" ).hide(); ';
	}
	
	$out .= '
	function changeModel(me){
	var $me = $(me);
	var $val = $me.val();
	//alert($val);
	$.ajax({
			url: "/prabawf/getcustom2/id/"+$val,
				dataType: "html",	
				beforeSend: function() {
				   
				},
				success: function(data) {
						$("#method").html(data);
				},
				timeout:9000,
				error:function(){
					
				},
			});
	
	}
	
	 function comp0(me) {
		var val = $(me).val();
		
		if(val=="map") {
		$( "#mapping" ).show();
		} else {
		$( "#mapping" ).hide();
		}
		if(val=="branch") {
		$( "#ifmapping" ).show();
		} else {
		$( "#ifmapping" ).hide();
		}
		if(val=="startlooping") {
		$( ".start" ).show();
		} else {
		$( ".start" ).hide();
		}
			
		if(val=="setreturn") {
		$( "#set" ).show();
		} else {
		$( "#set" ).hide();
		}
		
		if(val=="custom") {
			$( ".acustom" ).show();
			$.ajax({
			url: "/prabawf/getcustom",
				dataType: "html",	
				beforeSend: function() {
				   
				},
				success: function(data) {
						$("#custom").html(data);
				},
				timeout:9000,
				error:function(){
					
				},
			});
			
		} else {
			$( ".acustom" ).hide();
		}	
			
			
	}
    function addPIC(me) {
		$("#tutorial tbody").append("<tr><td><input name=awal[]></td><td><input name=akhir[]></td></tr>");
		
	}
    </script>
   
	<div>
	<div style="margin: 0px 0px 0px 10px; float: l
	<div style="margin: 0px 10px 0px 0px; float: right;">&nbsp;</div>
	<div class="active"><div class="maestro_task_edit_tab_close" style="float: right;"><div class="t"><div class=""><div class="r"><div class="l"><div class="bl-cl"><div class="br-cl"><div class="tl-cl"><div class="tr-cl">
	<a href="#" onclick="(function($) { $.modal.close(); disable_ajax_indicator(); select_boxes = []; })(jQuery); return false;"><img src="/assets/core/plugins/prabawf/images/admin/close.png"></a>
  </div></div></div></div></div></div></div></div></div></div>

  <div style="clear: both;"></div>
  <div class="maestro_task_edit_panel">
    <div class="t"><div class="b"><div class="r"><div class="l"><div class="bl-wht"><div class="br-wht"><div class="tl-wht"><div class="tr-wht">
    
      <form id="maestro_task_edit_form" method="post" action="" onsubmit="return save_task(this);">
        <input type="hidden" name="task_class" value="'.$task_class.'">
        <input type="hidden" name="template_data_id" value="'.$id.'">
		  <div id="task_edit_main">
          <div style="float: none;" class="maestro_tool_tip maestro_taskname"><div class="t"><div class="b"><div class="r"><div class="l"><div class="bl-bge"><div class="br-bge"><div class="tl-bge"><div class="tr-bge">
          
          <table width="100%">
                  <tbody>
                  <tr>
                    <td>Operator Type </td>
                    <td style="width: 70%;">
                    <select name="opr_type" onchange="comp0(this)">';
                    
                    
					$out .=	'<option ';
					if($optype=='mergeAll'){$out.=' selected ';}
					$out .=	' value="mergeAll">Merger All</option>';
					
					$out .=	'<option ';
					if($optype=='branch'){$out .=' selected ';}
					$out .=	'value="branch">Branch</option>';
					$out .=	 '<option value="map" ';
					if($optype=='map'){$out .=' selected ';}
					$out .='>Map</option>';
					$out .='<option ';
					if($optype=='startlooping'){$out .=' selected ';}
					$out .=	 'value="startlooping">Start Looping</option>';
					$out .=	 '<option ';
					if($optype=='endlooping'){$out .=' selected ';}
					$out .=	 'value="endlooping">End Looping</option>';
					$out .=	 '<option ';
					if($optype=='custom'){$out .=' selected ';}
					$out .=	 ' value="custom">Custom</option>';
					$out .=	 '<option ';
					if($optype=='setreturn'){$out .=' selected ';}
					$out .=	 ' value="setreturn">Set Return</option>
                    </select>
                    </td>
                  </tr>';
                  
                  $out .='<tr id="ifmapping">
                    <td style="vertical-align: top;">IfMapping</td>
                    <td style="width: 70%;">
                    
                    <table  style="border: 1px solid green;" width= 100% >
                    <thead>
                    <tr><td>LineType</td><td>If Formula</td></tr>
                    </thead>
                    <tbody>';
                    	
					$out .= "<tr><td>
					<input name=default value='default' disabled=yes></td><td><input name=ltype[default] value= '".$data['OperatorData']['ltype']['default']."'></td></tr>";
					
					$out .= "<tr><td>
					<input name=red value=\"redline\" disabled=yes></td><td><input name=ltype[red] value= '".$data['OperatorData']['ltype']['red']."'></td></tr>";

					$out .= "<tr><td>
					<input name=green value=\"greenline\" disabled=yes></td><td><input name=ltype[green] value= '".$data['OperatorData']['ltype']['green']."'></td></tr>";
					
					$out .= "<tr><td>
					<input name=green value=\"orangeline\" disabled=yes></td><td><input name=ltype[orange] value='".$data['OperatorData']['ltype']['orange']."'></td></tr>";
					$out .= '</tbody>
                     </table>
                    
                   </td>
                  </tr>';
                  
                   $out .= '<tr id="mapping">
                    <td style="vertical-align: top;">Map</td>
                    <td style="width: 70%;">
                    
                    <table  id="tutorial" style="border: 1px solid green;" width= 100% >
                    <thead>
                    <tr><td>From</td><td>To</td></tr>
                    </thead>
                    <tbody>';
                    
						if(count($data['OperatorData']['awal']>0)) {
						$i=0;	
						foreach ($data['OperatorData']['awal'] as $v) { 	
							$out .= '<tr><td><input name=awal[] value="'.$v.'"></td><td><input name=akhir[] value="'.$data['OperatorData']['akhir'][$i].'"></td></tr>';
								$i++;
							}
						} else {
						$out .= '<tr><td><input name=awal[]></td><td><input name=akhir[]></td></tr>';	
							}
					 $out .= '</tbody>
                     </table>
                     <button type="button" class="btn grey" onclick="addPIC(); return false;" data-act="addPIC" data-id="1" style="padding: 5px;">Add Map</button>
                   </td>
                  </tr>';
                  
         $out .= '<tr id="set">
                    <td style="vertical-align: top;"></td>
                    <td style="width: 70%;">
                    <textarea  rows="4" cols="50" name="ret" id="ret" >'.$data['OperatorData']['ret'].'</textarea>
					</td>
                  </tr>
                  <tr id="custom" class="acustom">
                    
                  </tr>
                  <tr class="acustom">
                  <td style="vertical-align: top;">Array Input</td>
                    <td style="width: 70%;"><br>
                    <textarea  rows="4" cols="50" name="inputcustom" id="inputcustom" >'.$data['OperatorData']['inputcustom'].'</textarea>
                    </td>
                  </tr>
                   <tr class="start">
                  <td style="vertical-align: middle;">Variable for Looping</td>
                    <td style="width: 70%;"><br>
                    <input name="varstart" id="varstart" value="'.$data['OperatorData']['varstart'].'"/>
                    </td>
                  </tr>
                  
                </tbody></table>';
                
         $out  .=  '</div></div></div></div></div></div></div></div></div><br />

          <br></br>

        </div>
        <div class="maestro_task_edit_save_div"><input class="form-submit" type="submit" value="Save"></div>

      </form>
    </div></div></div></div></div></div></div></div>
  </div>
</div>';


	return $out;
}


function task_type_exec($id, $vars, $task_class){
	$cd = new Model_Zprabawf();
	$ee = new Model_Prabasystem();
	
	
	$vz  = $cd->get_api_by_id ($vars['ext_id']);
	$vdat  = $cd->get_api_by_id_for_conn ($vars['ext_id']); 
	
	
   /// $conn = $cd->get_conn_id();
    $conn  =  $ee->get_conn (true);
   // var_dump($vdat); die(); 
	
	$out = '
    <script>
	function comp0(me) {
		var val = $(me).val();
		$.ajax({
			url: "/prabawf/ajaxer2/id/"+val,
			dataType: "html",	
			beforeSend: function() {
			   
			},
			success: function(data) {
					$("#vapi").html(data);
			},
			timeout:9000,
			error:function(){
				
			},
		});
	}
    </script>
	<div>
	<div style="margin: 0px 0px 0px 10px; float: l
	<div style="margin: 0px 10px 0px 0px; float: right;">&nbsp;</div>
	<div class="active"><div class="maestro_task_edit_tab_close" style="float: right;"><div class="t"><div class=""><div class="r"><div class="l"><div class="bl-cl"><div class="br-cl"><div class="tl-cl"><div class="tr-cl">
	<a href="#" onclick="(function($) { $.modal.close(); disable_ajax_indicator(); select_boxes = []; })(jQuery); return false;"><img src="/assets/core/plugins/prabawf/images/admin/close.png"></a>
  </div></div></div></div></div></div></div></div></div></div>

  <div style="clear: both;"></div>
  <div class="maestro_task_edit_panel">
    <div class="t"><div class="b"><div class="r"><div class="l"><div class="bl-wht"><div class="br-wht"><div class="tl-wht"><div class="tr-wht">
    
      <form id="maestro_task_edit_form" method="post" action="" onsubmit="return save_task(this);">
        <input type="hidden" name="task_class" value="'.$task_class.'">
        <input type="hidden" name="template_data_id" value="'.$id.'">
		  <div id="task_edit_main">
          <div style="float: none;" class="maestro_tool_tip maestro_taskname"><div class="t"><div class="b"><div class="r"><div class="l"><div class="bl-bge"><div class="br-bge"><div class="tl-bge"><div class="tr-bge">
          
          <table width="100%">
                  <tbody>
                  <tr>
                    <td>Connection Name </td>
                    <td style="width: 70%;">
                   
                    <select onchange="comp0(this)"; name="conn" id="conn">
            	<option value="" ">--choose conn--</option>';
					
				foreach ($conn  as $vv) 
            	{
					if( $vv['id'] == $vz['conn_id'] ){ $sel="selected"; }else{ $sel=""; }
					$out .=	'<option '.$sel.' value="'.$vv['id'].'">'.$vv['conn_name'].'</option>';
					
				} 
            $out .=	'</select> 
              
                    </td>
                  </tr>
                  <tr>
                    <td>Api Id </td>
                    <td style="width: 70%;">
                     <span id="vapi">
                    <select name="api" id="api">';
                    
                    
                if($vdat[0]['id']!="" ) {
				
					foreach ($vdat as $kk=>$vv) {
						if( $vv['id'] == $vz['id'] ){ $sel="selected"; }else{ $sel=""; }
						$out .=	'<option '.$sel.' value="'.$vv['id'].'">'.$vv['id'].'-['.substr($vv['sql_text'], 0, 30).']</option>';
					}	
				} else {	
					$out .=	'<option value="">--choose api--</option>';
				}
				
				$out .=	'</select> 
                    </span>
                    </td>
                  </tr>
                  
                  <tr>
                    <td style="vertical-align: top;">If false</td>
                    <td style="width: 70%;">
                    <input type="radio" name="false" value="-1" />Exit<br>
                    <input type="radio" name="false" value="1" />Force to nextstep<br>
						
                   </td>
                  </tr>
                </tbody></table>';
                
         $out  .=  '</div></div></div></div></div></div></div></div></div><br />

          <br></br>

        </div>
        <div class="maestro_task_edit_save_div"><input class="form-submit" type="submit" value="Save"></div>

      </form>
    </div></div></div></div></div></div></div></div>
  </div>
</div>';


	return $out;
}

  function get_task_id(){
    return $this->_task_id;
  }

  protected function _fetchTaskInformation() {
    $res = db_select('maestro_template_data', 'a');
    //var_dump($res);die();
    $res->fields('a', array('task_data'));
    $res->condition('a.id', $this->_task_id, '=');
    $td_rec = current($res->execute()->fetchAll());
    $td_rec->task_data = unserialize($td_rec->task_data);
	$this->_task_data = $td_rec;
  }

  //create task will insert the shell record of the task, and then the child class will handle the edit.
  function create() {
    $cc = new Model_Zprabawf();
    $id =  $cc -> create_new_task($this->_task_type, $this->_template_id, $_POST['offset_left'], $_POST['offset_top']);
    $this->_task_id = $id;
	return;
  }

  //deletes the task
  function destroy() {
  
  //   die($this->_task_id);
    if ($this->_task_id) {
  //  $cx = new Model_Zwfassign();
    $cz = new Model_Zprabawf();
   // $cda  = new Model_Zwfassignment();
    $cz->delete_process($this->_task_id);    
    //$cx->deltbytdid($this->_task_id);
      $retval = '';
      $success = 1;
    }
   

    return array('message' => $retval, 'success' => $success, 'task_id' => $this->_task_id);
      
  }

  function displayTask() {
	

    $c = new Model_Zwftemplatedata();
    $res=$c->fetchdatabyid($this->_task_id);
    $task_type = substr($res->TASK_CLASS_NAME, 15);
    $task_class = 'MaestroTaskInterface' . $task_type;

    //return theme('maestro_workflow_task_frame', array('rec' => $rec, 'ti' => $this, 'task_class' => $task_class));
  }

  function edit() {

  

	$cd = new Model_Zprabawf();
    //$dd = new Model_Swgeneral();
    
  //  $conn = $cd->get_conn_name();
    $id = $this->_task_id;
    $vars = $cd->get_process_by_pid($id);
    
	//Zend_Debug::dump($vars); die($vars->TASK_CLASS_NAME);
	$task_type = 'Praba'.$vars['class_type'];
	$task_class = $task_type;
   
	
	switch($task_class) {
		
	case  'PrabaTask':
		$out = $this->task_type_exec($id, $vars, $task_class);
	break;
	
	case  'PrabaOperator':
		$out = $this->task_type_operator($id, $vars, $task_class);
	break;
		
		
		
	}
	
	
  
    return array('html' => $out);
  }

  function save() {
	 
	
    //drupal_write_record('maestro_template_data', $rec, array('id'));

    //return array('task_id' => $this->_task_id);
  
  
  }

  //handles the update for the drag and drop
  function move() {
    $offset_left = intval($_POST['offset_left']);
    $offset_top = intval($_POST['offset_top']);

    if ($offset_left < 0) $offset_left = 0;
    if ($offset_top < 0) $offset_top = 0;
	//die($this->_task_id);
   // db_query("UPDATE {maestro_template_data} SET offset_left=:ofl, offset_top=:ofr WHERE id=:tdid", array(':ofl' => $offset_left, ':ofr' => $offset_top, ':tdid' => $this->_task_id));

    $c = new Model_Zprabawf();
    $c->move( $this->_task_id, $offset_left, $offset_top);

  }

  //handles the update when adding a line (insert the next step record)
  function drawLine() {
    //die($this->_task_id);
    $cs = new Model_Zprabawf();
    
    $rec = $cs->cek_new_con($this->_task_id, $_POST['line_to']);
   // die("zzzz");
    if ($rec == '') {
     
    
      $cs->create_new_conn($this->_task_id, $_POST['line_to'], 0);
      
    }
    else {
      //perhaps return a simple true/false with error message if the select failed
    }
  }

  //in theory only the if task will use this method
  function drawLineFalse() {
    $res = db_select('maestro_template_data_next_step', 'a');
    $res->fields('a', array('id'));

    $cond1 = db_or()->condition('a.template_data_to', $_POST['line_to'], '=')->condition('a.template_data_to_false', $_POST['line_to'], '=');
    $cond1fin = db_and()->condition('a.template_data_from', $this->_task_id, '=')->condition($cond1);

    $cond2 = db_or()->condition('a.template_data_to', $this->_task_id, '=')->condition('a.template_data_to_false', $this->_task_id, '=');
    $cond2fin = db_and()->condition('a.template_data_from', $_POST['line_to'], '=')->condition($cond2);

    $cond = db_or()->condition($cond1fin)->condition($cond2fin);

    $res->condition($cond);
    $rec = current($res->execute()->fetchAll());

    if ($rec == '') {
      $rec = new stdClass();
      $rec->template_data_from = $this->_task_id;
      $rec->template_data_to = 0;
      $rec->template_data_to_false = $_POST['line_to'];
      drupal_write_record('maestro_template_data_next_step', $rec);
    }
    else {
      //perhaps return a simple true/false with error message if the select failed
    }
  }

  //remove any next step records pertaining to this task
  function clearAdjacentLines() {
    //RK -- had to change the logic on this delete as PDO for SQL Server was failing for some reason even though the
    //resulting query was 100% correct.
        $taskid=intval($this->_task_id);
		$c = new Model_Zprabawf();
		$c->del1($taskid, $taskid, $taskid);
    

  }

  //returns an array of options for when the user right-clicks the task
  function getContextMenu() {
    $draw_line_msg = 'Select a task to draw the line to.';
    $options = array (
      'draw_line' => array(
        'label' => 'Draw Line',
        'js' => "draw_status = 1; draw_type = 'default'; line_start = document.getElementById('task{$this->_task_id}'); set_tool_tip('$draw_line_msg');\n"
      ),
      
		
      'clear_lines' => array(
        'label' => 'Clear Adjacent Lines',
        'js' => "clear_task_lines(document.getElementById('task{$this->_task_id}'));\n"
      ),
      'edit_task' => array(
        'label' => 'Edit',
        'js' => "enable_ajax_indicator(); \$.ajax({
          type: 'POST',
          url: ajax_url + 'Praba{$this->_task_type}/taskid/{$this->_task_id}/tempid/0/act/edit/format/json/',
          cache: false,
          dataType: 'json',
          success: display_task_panel,
          error: editor_ajax_error
        });"
      ),
      'delete_task' => array(
        'label' => 'Delete',
        'js' => "enable_ajax_indicator(); \$.ajax({
          type: 'POST',
          url: ajax_url + 'Praba{$this->_task_type}/taskid/{$this->_task_id}/tempid/0/act/destroy/format/json/',
          cache: false,
          success: delete_task,
          dataType: 'json',
          error: editor_ajax_error
        });\n"
      )
    );

    return $options;
  }

  function getContextMenuHTML() {
    $options = $this->getContextMenu();
    $html = "<div id=\"maestro_task{$this->_task_id}_context_menu\" class=\"maestro_context_menu\"><ul>\n";

    foreach ($options as $key => $option) {
      $o =  $option['label'];
      $html .= "<li style=\"white-space: nowrap;\" id=\"$key\">{$o}</li>\n";
    }
    $html .= "</ul></div>\n";

    return $html;
  }

  function getContextMenuJS() {
    $options = $this->getContextMenu();
    $js  = "(function ($) {\n";
    $js .= "\$('#task{$this->_task_id}').contextMenu('maestro_task{$this->_task_id}_context_menu', {\n";
    $js .= "menuStyle: {\n";
    $js .= "width: 175,\n";
    $js .= "fontSize: 12,\n";
    $js .= "},\n";

    $js .= "itemStyle: {\n";
    $js .= "padding: 0,\n";
    $js .= "paddingLeft: 10,\n";
    $js .= "},\n";

    $js .= "bindings: {\n";

    foreach ($options as $key => $option) {
      $js .= "'$key': function(t) {\n";
      $js .= $option['js'];
      $js .= "},\n";
    }

    $js .= "}\n";
    $js .= "});\n";
    $js .= "})(jQuery);\n";

    return $js;
  }

  function getAssignmentDisplay() {
    $display = t('Assigned to:') . ' ';

    $query = db_select('maestro_template_assignment', 'a');
    $query->leftJoin('maestro_template_variables', 'b', 'a.assign_id=b.id');
    $query->leftJoin('users', 'c', 'a.assign_id=c.uid');
    $query->leftJoin('role', 'd', 'a.assign_id=d.rid');
    if (module_exists('og')) {
      $query->leftJoin('og', 'e', 'a.assign_id=e.gid');
      $query->addField('e', 'label', 'groupname');
    }
    $query->fields('a', array('assign_id', 'assign_type', 'assign_by'));
    $query->fields('b', array('variable_name'));
    $query->addField('c', 'name', 'username');
    $query->addField('d', 'name', 'rolename');
    $query->condition('a.template_data_id', $this->_task_id, '=');

    $res = $query->execute();

    $assigned_list = '';
    foreach ($res as $rec) {
      if ($assigned_list != '') {
        $assigned_list .= ', ';
      }

      if ($rec->assign_by == MaestroAssignmentBy::FIXED) {
        switch ($rec->assign_type) {
        case MaestroAssignmentTypes::USER:
          $assigned_list .= $rec->username;
          break;

        case MaestroAssignmentTypes::ROLE:
          $assigned_list .= $rec->rolename;
          break;

        case MaestroAssignmentTypes::GROUP:
          $assigned_list .= $rec->groupname;
          break;
        }
      }
      else {
        $assigned_list .= $rec->variable_name;
      }
    }

    if ($assigned_list == '') {
      $assigned_list = '<i>' . t('nobody') . '</i>';
    }
    $display .= $assigned_list;

    return $display;
  }

  function getHandlerOptions() {
    $all_handler_options = cache_get('maestro_handler_options');
    $handler_options = array();
    if (array_key_exists('MaestroTaskType' . $this->_task_type, $all_handler_options->data)) {
      $handler_options = $all_handler_options->data['MaestroTaskType' . $this->_task_type];
    }

    return $handler_options;
  }

  function setCanvasHeight() {
    $rec = new stdClass();
    $rec->id = $this->_template_id;
    $rec->canvas_height = $_POST['height'];
    drupal_write_record('maestro_template', $rec, array('id'));
  }

  /*
   * Receive an incoming serialized data chunk.. return it to the callee serialized.
   */
  function modulateExportTaskData($data, $xref_array) {
    return $data;
  }

  /*
   * During the import routine, we take the exported username and try to find the UID.
   */
  function modulateExportUser($username) {
    $query = db_select('users', 'a');
    $query->fields('a',array('uid'));
    $query->condition('a.name',$username,'=');
    $uid=current($query->execute()->fetchAll());
    if(is_object($uid)) {
      return intval($uid->uid);
    }
    else {
      return 0;
    }
  }

  /*
   * During the import routine, we take the exported role and try to find the rid (role id).
   */
  function modulateExportRole($role) {
    $query = db_select('role', 'a');
    $query->fields('a',array('rid'));
    $query->condition('a.name', $role, '=');
    $rid=current($query->execute()->fetchAll());
    if(is_numeric($rid->rid)) {
      return intval($rid->rid);
    }
    else { // lets return roleID 2 which is technically the authenticated user role
      return 2;
    }
  }

 /*
   * During the import routine, we take the exported OG and try to find the gid (group id).
   */
  function modulateExportOG($group) {
    if(module_exists('og')) {
      $query = db_select('og', 'a');
      $query->fields('a',array('gid'));
      $query->condition('a.label', $group, '=');
      $gid=current($query->execute()->fetchAll());
      if(is_numeric($gid->gid)) {
        return intval($gid->gid);
      }
      else { //here's the problem... we should return false here so that the import can insert the initiator
        return FALSE;
      }
    }
  }

  abstract function display();
  abstract function getEditFormContent();
}

class MaestroTaskInterfaceUnknown extends CMS_Workflow_wftaskinterface {
  public $_task_class;

  function __construct($task_id=0, $template_id=0, $task_class) {
    $this->_task_type = 'Unknown';
    $this->_is_interactive = MaestroInteractiveFlag::IS_NOT_INTERACTIVE;
    $this->_task_class = $task_class;

    parent::__construct($task_id, $template_id);
  }

  function display() {
    return theme('maestro_task_unknown', array('tdid' => $this->_task_id, 'taskname' => $this->_taskname, 'ti' => $this));
  }

  function getEditFormContent() {
    return '';
  }

  function getContextMenu() {
    $draw_line_msg = t('Select a task to draw the line to.');
    $options = array (
      'draw_line' => array(
        'label' => t('Draw Line'),
        'js' => "draw_status = 1; draw_type = 1; line_start = document.getElementById('task{$this->_task_id}'); set_tool_tip('$draw_line_msg');\n"
      ),
      'clear_lines' => array(
        'label' => t('Clear Adjacent Lines'),
        'js' => "clear_task_lines(document.getElementById('task{$this->_task_id}'));\n"
      ),
      'delete_task' => array(
        'label' => t('Delete Task'),
        'js' => "enable_ajax_indicator(); \$.ajax({
          type: 'POST',
          url: ajax_url + 'MaestroTaskInterface{$this->_task_type}/taskid/{$this->_task_id}/tempid/0/act/destroy/',
          cache: false,
          success: delete_task,
          dataType: 'json',
          error: editor_ajax_error
        });\n"
      )
    );

    return $options;
  }
}

class PrabaStart extends CMS_Workflow_wftaskinterface {
  function __construct($task_id=0, $template_id=0) {
    $this->_task_type = 'Start';
    $this->_is_interactive = MaestroInteractiveFlag::IS_NOT_INTERACTIVE;
//    die($this->_is_interactive);

    parent::__construct($task_id, $template_id);
  }

  function create() {
    parent::create();

    $update=db_update('maestro_template_data')
      ->fields(array( 'taskname' => t('Start'),
                      'first_task' => 1,
                      'offset_left' => 34,
                      'offset_top' => 38

      ))
      ->condition('id', $this->_task_id)
      ->execute();
  }

  function display() {
    return theme('maestro_task_start', array('tdid' => $this->_task_id, 'taskname' => $this->_taskname, 'ti' => $this));
  }

  function getEditFormContent() {
    return '';
  }

  function getContextMenu() {
    $draw_line_msg = 'Select a task to draw the line to.';
    $options = array (
      'draw_line' => array(
        'label' => 'tDraw Line',
        'js' => "draw_status = 1; draw_type = 'default'; line_start = document.getElementById('task{$this->_task_id}'); set_tool_tip('$draw_line_msg');\n"
      ),
      'clear_lines' => array(
        'label' => 'Clear Adjacent Lines',
        'js' => "clear_task_lines(document.getElementById('task{$this->_task_id}'));\n"
      )
    );

    return $options;
  }
}

class PrabaEnd extends CMS_Workflow_wftaskinterface {
  function __construct($task_id=0, $template_id=0) {
    $this->_task_type = 'End';
    $this->_is_interactive = MaestroInteractiveFlag::IS_NOT_INTERACTIVE;

    parent::__construct($task_id, $template_id);
  }

  function create() {
    parent::create();
    $update=db_update('maestro_template_data')
      ->fields(array( 'taskname' => t('End'),
                      'offset_left' => 280,
                      'offset_top' => 200
      ))
      ->condition('id', $this->_task_id)
      ->execute();
  }

  function display() {
    return theme('maestro_task_end', array('tdid' => $this->_task_id, 'taskname' => $this->_taskname, 'ti' => $this));
  }

  function getEditFormContent() {
    return '';
  }

  function getContextMenu() {
    $options = array (
      'clear_lines' => array(
        'label' => 'Clear Adjacent Lines',
        'js' => "clear_task_lines(document.getElementById('task{$this->_task_id}'));\n"
      )
    );

    return $options;
  }
}

class PrabaIf extends CMS_Workflow_wftaskinterface {
  function __construct($task_id=0, $template_id=0) {
    $this->_task_type = 'If';
    $this->_is_interactive = MaestroInteractiveFlag::IS_NOT_INTERACTIVE;

    parent::__construct($task_id, $template_id);
  }

  function modulateExportTaskData($data, $xref_array) {
    $fixed_data = @unserialize($data);
    if($fixed_data !== FALSE) {
      if($fixed_data['if_argument_variable'] != '') $fixed_data['if_argument_variable'] = $xref_array[$fixed_data['if_argument_variable']];
      return serialize($fixed_data);
    }
    else {
      return $data;
    }
  }

  function display() {
    return theme('maestro_task_if', array('tdid' => $this->_task_id, 'taskname' => $this->_taskname, 'ti' => $this));
  }

  function getEditFormContent() {
    $this->_fetchTaskInformation();

    $res = db_query("SELECT id, variable_name, variable_value FROM {maestro_template_variables} WHERE template_id=:tid", array('tid' => $this->_template_id));
    $argument_variables = "<option></option>";
    foreach ($res as $rec) {
      $selected = '';
      if($this->_task_data->task_data['if_argument_variable'] == $rec->id) $selected = " selected ";
      $argument_variables .= "<option value='{$rec->id}' {$selected}>{$rec->variable_name}</option>";
    }

    return theme('maestro_task_if_edit', array('tdid' => $this->_task_id, 'td_rec' => $this->_task_data, 'argument_variables' => $argument_variables));
  }

  function save() {
    $rec = new stdClass();
    $rec->id = $_POST['template_data_id'];

    if(check_plain($_POST['ifTaskArguments']) == 'status'){
      $rec->task_data = serialize(array(
                                    'if_operator' => '',
                                    'if_value' => '',
                                    'if_process_arguments' => $_POST['ifProcessArguments'],
                                    'if_argument_variable' => '',
                                    'if_task_arguments' => $_POST['ifTaskArguments']
      ));
    }
    else {
      $rec->task_data = serialize(array(
                                    'if_operator' => $_POST['ifOperator'],
                                    'if_value' => check_plain($_POST['ifValue']),
                                    'if_process_arguments' => '',
                                    'if_argument_variable' => $_POST['argumentVariable'],
                                    'if_task_arguments' => $_POST['ifTaskArguments']
      ));
    }
    drupal_write_record('maestro_template_data', $rec, array('id'));

    return parent::save();
  }

  function getContextMenu() {
    $draw_line_msg = 'Select a task to draw the line to.';
    $options = array (
      'draw_line' => array(
        'label' => 'Draw Success Line',
        'js' => "draw_status = 1; draw_type = 1; line_start = document.getElementById('task{$this->_task_id}'); set_tool_tip('$draw_line_msg');\n"
      ),
      'draw_line_false' => array(
        'label' => 'Draw Fail Line',
        'js' => "draw_status = 1; draw_type = 2; line_start = document.getElementById('task{$this->_task_id}'); set_tool_tip('$draw_line_msg');\n"
      ),
      'clear_lines' => array(
        'label' => 'Clear Adjacent Lines',
        'js' => "clear_task_lines(document.getElementById('task{$this->_task_id}'));\n"
      ),
      'edit_task' => array(
        'label' => 'Edit',
        'js' => "enable_ajax_indicator(); \$.ajax({
          type: 'POST',
           url: ajax_url + 'Praba{$this->_task_type}/taskid/{$this->_task_id}/tempid/0/act/edit/format/json/',

          cache: false,
          dataType: 'json',
          success: display_task_panel,
          error: editor_ajax_error
        });"
      ),
      'delete_task' => array(
        'label' => 'Delete',
        'js' => "enable_ajax_indicator(); \$.ajax({
          type: 'POST',
          url: ajax_url + 'Praba{$this->_task_type}/taskid/{$this->_task_id}/tempid/0/act/destroy/format/json/',
          cache: false,
          dataType: 'json',
          success: delete_task,
          error: editor_ajax_error
        });\n"
      )
    );

    return $options;
  }
}

class MaestroTaskInterfaceBatch extends CMS_Workflow_wftaskinterface {
  function __construct($task_id=0, $template_id=0) {
    $this->_task_type = 'Batch';
    $this->_is_interactive = MaestroInteractiveFlag::IS_NOT_INTERACTIVE;

    parent::__construct($task_id, $template_id);

    $this->_task_edit_tabs = array('notification' => 1);
  }

  function display() {
    return theme('maestro_task_batch', array('tdid' => $this->_task_id, 'taskname' => $this->_taskname, 'ti' => $this));
  }

  function getEditFormContent() {
    $this->_fetchTaskInformation();
    $this->_task_data->task_data['handler_location'] = variable_get('maestro_batch_script_location', drupal_get_path('module','maestro') . "/batch/");
    return theme('maestro_task_batch_edit', array('tdid' => $this->_task_id, 'td_rec' => $this->_task_data));
  }

  function save() {
    $rec = new stdClass();
    $rec->id = $_POST['template_data_id'];
    $rec->task_data = serialize(array('handler' => $_POST['handler']));
    drupal_write_record('maestro_template_data', $rec, array('id'));

    return parent::save();
  }
}

class MaestroTaskInterfaceBatchFunction extends CMS_Workflow_wftaskinterface {
  function __construct($task_id=0, $template_id=0) {
    $this->_task_type = 'BatchFunction';
    $this->_is_interactive = MaestroInteractiveFlag::IS_NOT_INTERACTIVE;

    parent::__construct($task_id, $template_id);

    $this->_task_edit_tabs = array('optional' => 1, 'notification' => 1);
  }

  function display() {
    return theme('maestro_task_batch_function', array('tdid' => $this->_task_id, 'taskname' => $this->_taskname, 'ti' => $this));
  }

  function getEditFormContent() {
    $this->_fetchTaskInformation();
    $batch_function = drupal_get_path('module','maestro') . "/batch/batch_functions.php";
    $this->_task_data->task_data['handler_location'] = $batch_function;

    $handler_options = $this->getHandlerOptions();

    return theme('maestro_task_batch_function_edit', array('tdid' => $this->_task_id, 'td_rec' => $this->_task_data, 'handler_options' => $handler_options));
  }

  function save() {
    $rec = new stdClass();
    $rec->id = $_POST['template_data_id'];
    $rec->task_data = serialize(array('handler' => ($_POST['handler'] == '') ? $_POST['handler_other'] : $_POST['handler']));
    drupal_write_record('maestro_template_data', $rec, array('id'));

    return parent::save();
  }
}

class MaestroTaskInterfaceInteractiveFunction extends CMS_Workflow_wftaskinterface {
  function __construct($task_id=0, $template_id=0) {
    $this->_is_interactive = MaestroInteractiveFlag::IS_INTERACTIVE;
    $this->_task_type = 'InteractiveFunction';

    parent::__construct($task_id, $template_id);

    $this->_task_edit_tabs = array('assignment' => 1, 'notification' => 1, 'optional' => 1);
  }

  function display() {
    return theme('maestro_task_interactive_function', array('tdid' => $this->_task_id, 'taskname' => $this->_taskname, 'ti' => $this));
  }

  function getEditFormContent() {
    $this->_fetchTaskInformation();
    if (@($this->_task_data->optional_parm) == NULL) {
      $this->_task_data->optional_parm = '';
    }

    $handler_options = $this->getHandlerOptions();

    return theme('maestro_task_interactive_function_edit', array('tdid' => $this->_task_id, 'td_rec' => $this->_task_data, 'handler_options' => $handler_options));
  }

  function save() {
    $rec = new stdClass();
    $rec->id = $_POST['template_data_id'];
    $rec->task_data = serialize(array('handler' => ($_POST['handler'] == '') ? $_POST['handler_other'] : $_POST['handler']));

    drupal_write_record('maestro_template_data', $rec, array('id'));

    return parent::save();
  }
}

class MaestroTaskInterfaceSetProcessVariable extends CMS_Workflow_wftaskinterface {
  function __construct($task_id=0, $template_id=0) {
    $this->_task_type = 'SetProcessVariable';
    $this->_is_interactive = MaestroInteractiveFlag::IS_NOT_INTERACTIVE;

    parent::__construct($task_id, $template_id);
  }

  function display() {
    return theme('maestro_task_set_process_variable', array('tdid' => $this->_task_id, 'taskname' => $this->_taskname, 'ti' => $this));
  }

  function modulateExportTaskData($data, $xref_array) {
    $fixed_data = @unserialize($data);
    if($fixed_data !== FALSE) {
      $fixed_data['var_to_set'] = $xref_array[$fixed_data['var_to_set']];
      return serialize($fixed_data);
    }
    else {
      return $data;
    }
  }

  function getEditFormContent() {
    $methods = $this->getSetMethods();

    $this->_fetchTaskInformation();
    if (!is_array(@($this->_task_data->task_data)) || !array_key_exists('set_type', $this->_task_data->task_data)) {
      $this->_task_data->task_data['var_to_set'] = '';
      $this->_task_data->task_data['set_type'] = 0;

      $i = 0;
      foreach ($methods as $key => $method) {
        if ($i++ == 0) {
          $this->_task_data->task_data['set_type'] = $key;
        }
        $this->_task_data->task_data[$key . '_value'] = '';
      }
    }

    $res = db_query("SELECT id, variable_name FROM {maestro_template_variables} WHERE template_id=:tid", array('tid' => $this->_template_id));
    foreach ($res as $rec) {
      $pvars[$rec->id] = $rec->variable_name;
    }

    return theme('maestro_task_set_process_variable_edit', array('tdid' => $this->_task_id, 'td_rec' => $this->_task_data, 'pvars' => $pvars, 'set_methods' => $methods));
  }

  function save() {
    $rec = new stdClass();
    $rec->id = $_POST['template_data_id'];
    $methods = $this->getSetMethods();
    $task_data = array();
    foreach ($methods as $key => $method) {
      $task_data[$key . '_value'] = $_POST[$key . '_value'];
    }
    $task_data['var_to_set'] = $_POST['var_to_set'];
    $task_data['set_type'] = $_POST['set_type'];
    $rec->task_data = serialize($task_data);

    drupal_write_record('maestro_template_data', $rec, array('id'));

    return parent::save();
  }

  function getSetMethods() {
    $set_process_variable_methods = cache_get('maestro_set_process_variable_methods');
    if($set_process_variable_methods === FALSE) {
      $set_process_variable_methods = array();
      foreach (module_implements('maestro_set_process_variable_methods') as $module) {
        $function = $module . '_maestro_set_process_variable_methods';
        if ($arr = $function()) {
          $set_process_variable_methods = maestro_array_merge_keys($set_process_variable_methods, $arr);
        }
      }
      cache_set('maestro_set_process_variable_methods', $set_process_variable_methods);
    }

    $methods = cache_get('maestro_set_process_variable_methods');

    return $methods->data;
  }
}
class PrabaContent extends CMS_Workflow_wftaskinterface {
  function __construct($task_id=0, $template_id=0) {
	 //die("ss");    
	$this->_task_type = 'Content';
    $this->_is_interactive = MaestroInteractiveFlag::IS_INTERACTIVE;
     parent::__construct($task_id, $template_id);
  }

  function display() {
    return theme('maestro_task_content_type', array('tdid' => $this->_task_id, 'taskname' => $this->_taskname, 'ti' => $this));
  }


  function getEditFormContent() {
    $this->_fetchTaskInformation();
    
    //edited by himawijaya 1-12-2011
    
    //fungsi asli
    //$content_types = node_type_get_types();
    
	$process_management = new stdClass();
	$process_management->type = 'process_management';
	$process_management->name = 'Process Management';
	$process_management->orig_type = 'process_management';
	$process_management->name = 'Process Management';

	$request_management = new stdClass();
	$request_management->type = 'request_management';
	$request_management->name = 'Request Management';
	$request_management->orig_type = 'request_management';
	$request_management->name = 'Request Management';

	$ntype=array('process_management'=>$process_management, 'request_management'=>$request_management);

	$content_types = $ntype;
    
    
    return theme('maestro_task_content_type_edit', array('tdid' => $this->_task_id, 'td_rec' => $this->_task_data, 'content_types' => $content_types));
  }

  function save() {
	//Zend_Debug::dump($_POST); die();
    $c = new Model_Zprabawf();
    $var= $c->update_process($_POST);
	
	return array('task_id' => $this->_task_id);
    
  }
   function edit() {
	$cd = new Model_Zprabawf();
	$xx = new Model_System();
	
    $id = $this->_task_id;
    $vars = $cd->get_process_by_pid($id);
    
	$task_type = 'Praba'.$vars['class_type'];
	$task_class = $task_type;
	$vz = unserialize($vars['zparams_serialize']);
	//Zend_Debug::dump($vz); die();
	$vc  = $cd->get_content_type_field($this->_template_id);
	//Zend_Debug::dump($vz); die();
	$ee = new Model_Prabasystem();
	$rol = $xx->get_roles(true);
	#Zend_Debug::dump($vz['Data']);die();
	$out ="";
	
	if($vz['Data']['type']!=""&&$vz['Data']['account']!="") {
		
		$out .= '
    <script>
		var val  = $( "#type").val();
	$.ajax({
			url: "/prabawf/ajaxer3/id/"+val+"/acc/'.$vz['Data']['account'].'",
			dataType: "html",	
			beforeSend: function() {
			   
			},
			success: function(data) {
					$("#vaccount").html(data);
			},
			timeout:9000,
			error:function(){
				
			},
		});
		</script>
		';
	
}
	
	$out .= '
   <script>
    function comp0(me) {
		var val = $(me).val();
		//alert(val);
		$.ajax({
			url: "/prabawf/ajaxer3/id/"+val,
			dataType: "html",	
			beforeSend: function() {
			   
			},
			success: function(data) {
					$("#vaccount").html(data);
			},
			timeout:9000,
			error:function(){
				
			},
		});
	}
	
	function comp1(me) {
		var val = $(me).val();
		//alert(val);
		var type  = $( "#type" ).val();
		var acc  = $( "#account").val();
		$.ajax({
			url: "/prabawf/ajaxer4/id/"+val+"/type/"+type+"/acc/"+acc,
			dataType: "html",	
			beforeSend: function() {
			   
			},
			success: function(data) {
					$("#vgroup").html(data);
			},
			timeout:9000,
			error:function(){
				
			},
		});
	}
	
    </script>
	<div>
	<div style="margin: 0px 0px 0px 10px; float: l
	<div style="margin: 0px 10px 0px 0px; float: right;">&nbsp;</div>
	<div class="active"><div class="maestro_task_edit_tab_close" style="float: right;"><div class="t"><div class=""><div class="r"><div class="l"><div class="bl-cl"><div class="br-cl"><div class="tl-cl"><div class="tr-cl">
	<a href="#" onclick="(function($) { $.modal.close(); disable_ajax_indicator(); select_boxes = []; })(jQuery); return false;">
<img src="/assets/core/plugins/prabawf/images/admin/close.png"></a>
  </div></div></div></div></div></div></div></div></div></div>



  <div id="task_edit_tab_main" class="active"><div class="maestro_task_edit_tab"><div class="t"><div class=""><div class="r"><div class="l"><div class="bl-tab"><div class="br-tab"><div class="tl-tab"><div class="tr-tab">
	  <a href="#" onclick="switch_task_edit_section(\'main\'); return false;">Main</a>
	  </div></div></div></div></div></div></div></div></div></div>


    <div id="task_edit_tab_assignment" class="unactive"><div class="maestro_task_edit_tab"><div class="t"><div class=""><div class="r"><div class="l"><div class="bl-tab"><div class="br-tab"><div class="tl-tab"><div class="tr-tab">
    <a href="#" onclick="switch_task_edit_section(\'assignment\'); set_summary(\'assign\'); return false;">Assignment</a>
    </div></div></div></div></div></div></div></div></div></div>

    <div id="task_edit_tab_notification" class="unactive"><div class="maestro_task_edit_tab"><div class="t"><div class=""><div class="r"><div class="l"><div class="bl-tab"><div class="br-tab"><div class="tl-tab"><div class="tr-tab">
    <a href="#" onclick="switch_task_edit_section(\'notification\'); set_summary(\'notify\'); return false;">Notification</a>
    </div></div></div></div></div></div></div></div></div></div>



  <div style="clear: both;"></div>
  <div class="maestro_task_edit_panel">
    <div class="t"><div class="b"><div class="r"><div class="l"><div class="bl-wht"><div class="br-wht"><div class="tl-wht"><div class="tr-wht">
    
      <form id="maestro_task_edit_form" method="post" action="" onsubmit="return save_task(this);">
        <input type="hidden" name="task_class" value="'.$task_class.'">
        <input type="hidden" name="template_data_id" value="'.$id.'">
        
		  <div id="task_edit_main">
          <div style="float: none;" class="maestro_tool_tip maestro_taskname"><div class="t"><div class="b"><div class="r"><div class="l"><div class="bl-bge"><div class="br-bge"><div class="tl-bge"><div class="tr-bge">
          
          <table width="100%">
                  <tbody>
                  
                   <tr><td>TYPE CONTENT </td><td>'.$vc[0]['bundle'].'</td></tr>
                   <tr><td>Task Name </td><td><input name ="pname" type=input value="'.$vars['process_name'].'" /></td></tr>
                   <tr><td>	Field to show </td><td></td></tr>
                   ';
                  
				foreach ($vc as $v) {
					 $sel="";
					if(isset($vz['Data']['field'][$v['fname']])) {$sel="checked";}
					
					$out .= '<tr><td>&nbsp;&nbsp;'.$v['label'].'</td>
						<td style="width: 70%;">
							<input type="checkbox" '.$sel.'  name="field['.$v['fname'].']"  value=1 />
							<select  name="weight['.$v['fname'].']">';
							
							
							for ($i=1;$i<=20;$i++) {
								$sel ="";
								if($vz['Data']['weight'][$v['fname']]==$i) {$sel="selected";}
								$out .= '<option '.$sel.' value='.$i.'>'.$i.'</option>';
							};
							
							$out .= '</select>
						</td> </tr>';
						}
               $out .=  '
                </tbody></table>';
                
         $out  .=  '</div></div></div></div></div></div></div></div></div><br />

          
        <b>Cum Weight&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b> : <input name=cumweight value="'.$vz['Data']['cumweight'].'" />%<br>
        <b>Template phtml&nbsp;&nbsp;</b> : <input name=template value="'.$vz['Data']['template'].'" /><br> 
		<b>Role for Approval</b>:<br>';
			
		foreach ($rol as $v) {
				$sel="";
				if(in_array($v['gid'], $vz['Data']['approval'])) {$sel="checked";}
				
			
				$out .= '&nbsp;&nbsp;<input name ="approval[]" '.$sel .' type="checkbox"  value="'.$v['gid'].'">'.$v['group_name'].'&nbsp;&nbsp;&nbsp;';
			//}
		}
			
			
        $out .='</div>
        
			<div id="task_edit_assignment"  style="display:none">
          <table>
            <tbody><tr>
              <td colspan="3">
                Assignment Type:&nbsp;
					<select name="assign_type" id="assign_type" onchange="toggle_list_type(\'assign\');">
           
                    <option value="role">Role</option>
                </select>&nbsp;
             
              </td>
            </tr>

            <tr>
              <td style="text-align: center;">Available</td>
              <td></td>
              <td style="text-align: center;">Assigned</td>
            </tr>

            
  <tr class="assign_row role fixed"  style="display: table-row;">
    <td style="width: 200px;">
      <select size="4" multiple="multiple" style="width: 100%;" id="assign_row_role_fixed__unselected">';
		if(!isset($vz['Data']['assign_role'])) {
			$vz['Data']['assign_role']=array();	
		}
		$newrole=array();
		foreach ($rol as $v) {
			$newrole[$v['gid']] = $v['group_name'];
			if(!in_array($v['gid'], $vz['Data']['assign_role'])) {
				$out .= '<option  value="'.$v['gid'].'">'.$v['group_name'].'</option>';
			}
		}
		
		//Zend_Debug::dump($newrole); die();
		$out .= '</select>
    </td>
    <td style="text-align: center;">
      <a href="#" onclick="move_options(\'assign_row_role_fixed_\', \'assign_row_role_fixed__unselected\'); return false;"><img src="/assets/core/plugins/prabawf/images/admin/left-arrow.png"></a>
      &nbsp;&nbsp;&nbsp;
      <a href="#" onclick="move_options(\'assign_row_role_fixed__unselected\', \'assign_row_role_fixed_\'); return false;"><img src="/assets/core/plugins/prabawf/images/admin/right-arrow.png"></a>
    </td>
    <td style="width: 200px;">
      
      <select size="4" multiple="multiple" style="width: 100%;" id="assign_row_role_fixed_" name="assign_role[]">';
      
		foreach ($vz['Data']['assign_role'] as $v) {
			$out .= '<option  selected value="'.$v.'">'.$newrole[$v].'</option>';
		}	
       
      $out .= '</select>
    </td>
  </tr>
            

          </tbody></table>
        </div><br />
          
          <div id="task_edit_notification" style="display:none;">
          <table>
            <tbody><tr>
              <td colspan="3">
                Notification Type:&nbsp;
                <select name="notify_type" id="notify_type" onchange="toggle_list_type(\'notify\');">
                  
                    <option value="role">Role</option>
                </select>&nbsp;
                <select name="notify_by_variable" id="notify_by_variable" onchange="toggle_list_type(\'notify\');">
                    <option value="fixed">Fixed</option>
                  
                </select>&nbsp;
                <select name="notify_when" id="notify_when" onchange="toggle_list_type(\'notify\');">
                    <option value="assignment">On Assignment</option>
                    <option value="completion">On Completion</option>
                    <option value="reminder">Reminder</option>
                    <option value="escalation">Escalation</option>
                </select>&nbsp;
              </td>
            </tr>



            <!-- By Role -->
            
  <tr class="notify_row role fixed assignment" >
    <td style="width: 200px;">
      <select size="4" multiple="multiple" style="width: 100%;" id="notify_row_role_fixed_assignment_unselected">';
         
         
        if(!isset($vz['Data']['notify_assign_role'])) {
			$vz['Data']['notify_assign_role']=array();	
		}
		foreach ($rol as $v) {
			if(!in_array($v['gid'], $vz['Data']['notify_assign_role'])) {
			$out .= '<option value="'.$v['gid'].'">'.$v['group_name'].'</option>';
			}
		}
		$out .= '</select>
    
    </td>
    <td style="text-align: center;">
      <a href="#" onclick="move_options(\'notify_row_role_fixed_assignment\', \'notify_row_role_fixed_assignment_unselected\'); return false;"><img src="/assets/core/plugins/prabawf/images/admin/left-arrow.png"></a>
      &nbsp;&nbsp;&nbsp;
      <a href="#" onclick="move_options(\'notify_row_role_fixed_assignment_unselected\', \'notify_row_role_fixed_assignment\'); return false;"><img src="/assets/core/plugins/prabawf/images/admin/right-arrow.png"></a>
    </td>
    <td style="width: 200px;">
      
      <select size="4" multiple="multiple" style="width: 100%;" id="notify_row_role_fixed_assignment" name="notify_assign_role[]">';

		
      foreach ($vz['Data']['notify_assign_role'] as $v) {
		
			$out .= '<option  selected value="'.$v.'">'.$newrole[$v].'</option>';
			
	   }	
      
      $out .= '</select>
    </td>
  </tr>
       

            
  <tr class="notify_row role fixed completion" style="display: none;">
    <td style="width: 200px;">
      <select size="4" multiple="multiple" style="width: 100%;" id="notify_row_role_fixed_completion_unselected">';
         if(!isset($vz['Data']['notify_complete_role'])) {
			$vz['Data']['notify_complete_role']=array();	
		}
		foreach ($rol as $v) {
			if(!in_array($v['gid'], $vz['Data']['notify_complete_role'])) {
		  $out .= '<option value="'.$v['gid'].'">'.$v['group_name'].'</option>';
		}
		}
		$out .= '</select>

    </td>
    <td style="text-align: center;">
      <a href="#" onclick="move_options(\'notify_row_role_fixed_completion\', \'notify_row_role_fixed_completion_unselected\'); return false;"><img src="/assets/core/plugins/prabawf/images/admin/left-arrow.png"></a>
      &nbsp;&nbsp;&nbsp;
      <a href="#" onclick="move_options(\'notify_row_role_fixed_completion_unselected\', \'notify_row_role_fixed_completion\'); return false;"><img src="/assets/core/plugins/prabawf/images/admin/right-arrow.png"></a>
    </td>
    <td style="width: 200px;">
      
      <select size="4" multiple="multiple" style="width: 100%;" id="notify_row_role_fixed_completion" name="notify_complete_role[]">';
      
      
      foreach ($vz['Data']['notify_complete_role'] as $v) {
		
			$out .= '<option  selected value="'.$v.'">'.$newrole[$v].'</option>';
		}	
      
      $out .= '</select>
    </td>
  </tr>
            
  

            
  <tr class="notify_row role fixed reminder" style="display: none;">
    <td style="width: 200px;">
      <select size="4" multiple="multiple" style="width: 100%;" id="notify_row_role_fixed_reminder_unselected">';
         
          if(!isset($vz['Data']['notify_reminder_role'])) {
			$vz['Data']['notify_reminder_role']=array();	
		}
         
		foreach ($rol as $v) {
				if(!in_array($v['gid'], $vz['Data']['notify_reminder_role'])) {
		  $out .= '<option value="'.$v['gid'].'">'.$v['group_name'].'</option>';
			}
		}
		$out .= '</select>
    </td>
    <td style="text-align: center;">
      <a href="#" onclick="move_options(\'notify_row_role_fixed_reminder\', \'notify_row_role_fixed_reminder_unselected\'); return false;"><img src="/assets/core/plugins/prabawf/images/admin/left-arrow.png"></a>
      &nbsp;&nbsp;&nbsp;
      <a href="#" onclick="move_options(\'notify_row_role_fixed_reminder_unselected\', \'notify_row_role_fixed_reminder\'); return false;"><img src="/assets/core/plugins/prabawf/images/admin/right-arrow.png"></a>
    </td>
    <td style="width: 200px;">
      
      <select size="4" multiple="multiple" style="width: 100%;" id="notify_row_role_fixed_reminder" name="notify_reminder_role[]">';
         foreach ($vz['Data']['notify_reminder_role'] as $v) {
		
			$out .= '<option  selected value="'.$v.'">'.$newrole[$v].'</option>';
		}	
      
      
      $out .= '</select>
    </td>
  </tr>
            
  
            
  <tr class="notify_row role fixed escalation" style="display: none;">
    <td style="width: 200px;">
      <select size="4" multiple="multiple" style="width: 100%;" id="notify_row_role_fixed_escalation_unselected">';
                if(!isset($vz['Data']['notify_escalation_role'])) {
			$vz['Data']['notify_escalation_role']=array();	
		}
		foreach ($rol as $v) {
			if(!in_array($v['gid'], $vz['Data']['notify_escalation_role'])) {
		  $out .= '<option value="'.$v['gid'].'">'.$v['group_name'].'</option>';
		}
		}
		$out .= '</select>
    </td>
    <td style="text-align: center;">
      <a href="#" onclick="move_options(\'notify_row_role_fixed_escalation\', \'notify_row_role_fixed_escalation_unselected\'); return false;"><img src="/assets/core/plugins/prabawf/images/admin/left-arrow.png"></a>
      &nbsp;&nbsp;&nbsp;
      <a href="#" onclick="move_options(\'notify_row_role_fixed_escalation_unselected\', \'notify_row_role_fixed_escalation\'); return false;"><img src="/assets/core/plugins/prabawf/images/admin/right-arrow.png"></a>
    </td>
    <td style="width: 200px;">
      
      <select size="4" multiple="multiple" style="width: 100%;" id="notify_row_role_fixed_escalation" name="notify_escalation_role[]">';
      
        foreach ($vz['Data']['notify_escalation_role'] as $v) {
			$out .= '<option  selected value="'.$v.'">'.$newrole[$v].'</option>';
		}	
      $out .= '</select>
    </td>
  </tr>
            <tr>
              <td colspan="3" style="text-align: center;">
                <i>(leave subject and/or message blank for default)</i>
              </td>
            </tr>
            <tr class="notify_row user role group fixed variable assignment">
              <td colspan="3">
                <table width="100%">
                  <tbody><tr>
                    <td>Subject:</td>
                    <td style="width: 90%;"><input type="text" name="pre_notify_subject" value="'.$vz['Data']['pre_notify_subject'].'"></td>
                  </tr>
                  <tr>
                    <td style="vertical-align: top;">Message:</td>
                    <td style="width: 90%;"><textarea style="width: 100%;" rows="4" name="pre_notify_message">'.$vz['Data']['pre_notify_message'].'</textarea></td>
                  </tr>
                </tbody></table>
              </td>
            </tr>

            <tr class="notify_row user role group fixed variable completion" style="display: none;">
              <td colspan="3">
                <table width="100%">
                  <tbody><tr>
                    <td>Subject:</td>
                    <td style="width: 90%;"><input type="text" name="post_notify_subject" value="'.$vz['Data']['post_notify_subject'].'"></td>
                  </tr>
                  <tr>
                    <td style="vertical-align: top;">Message:</td>
                    <td style="width: 90%;"><textarea style="width: 100%;" rows="4" name="post_notify_message">'.$vz['Data']['post_notify_message'].'</textarea></td>
                  </tr>
                </tbody></table>
              </td>
            </tr>

            <tr class="notify_row user role group fixed variable reminder" style="display: none;">
              <td colspan="3">
                <table width="100%">
                  <tbody><tr>
                    <td nowrap="">Subject:</td>
                    <td style="width: 90%;"><input type="text" name="reminder_subject" value="'.$vz['Data']['reminder_subject'].'"></td>
                  </tr>
                  <tr>
                    <td nowrap="" style="vertical-align: top;">Message:</td>
                    <td style="width: 90%;"><textarea style="width: 100%;" rows="4" name="reminder_message">'.$vz['Data']['reminder_message'].'</textarea></td>
                  </tr>
                  <tr>
                    <td nowrap="">Reminder Interval (days):</td>
                    <td><input type="text" style="width: 30px;" name="reminder_interval" value="'.$vz['Data']['reminder_interval'].'"></td>
                  </tr>
                </tbody></table>
              </td>
            </tr>

            <tr class="notify_row user role group fixed variable escalation" style="display: none;">
              <td colspan="3">
                <table width="100%">
                  <tbody><tr>
                    <td nowrap="">Subject:</td>
                    <td style="width: 90%;"><input type="text" name="escalation_subject" value=""></td>
                  </tr>
                  <tr>
                    <td nowrap="" style="vertical-align: top;">Message:</td>
                    <td style="width: 90%;"><textarea style="width: 100%;" rows="4" name="escalation_message"></textarea></td>
                  </tr>
                  <tr>
                    <td nowrap="">Escalate After (days):</td>
                    <td><input type="text" style="width: 30px;" name="escalation_interval" value="0"></td>
                  </tr>
                </tbody></table>
              </td>
            </tr>
			<tr >
              <td colspan="3">
                <table width="100%">
                  <tbody><tr>
                    <td style="vertical-align: top;">Replace:</td>
                    <td style="width: 90%;">$taskname by taskname, $fullname by FullName User or Rolename<br> $title by Content Title, $link by link redirect Inbox    </td>
                  </tr>
                </tbody></table>
              </td>
            </tr>
            <tr>
              <td colspan="3">
                <input type=checkbox name=channel[] ';
                if(in_array('wa', $vz['Data']['channel'])) {$out .= ' checked ';}
                $out .= ' value="wa" /> &nbsp;<b>via WA</b>
                <input type=checkbox name=channel[] ';
                if(in_array('telegram', $vz['Data']['channel'])) {$out .= ' checked ';}
                $out .= '  value="telegram" />&nbsp;<b>via Telegram</b>
                <input type=checkbox name=channel[] ';
                if(in_array('sms', $vz['Data']['channel'])) {$out .= ' checked ';}
                $out .= ' value="sms" />&nbsp;<b>via SMS</b>
                <input type=checkbox name=channel[] ';
                if(in_array('email', $vz['Data']['channel'])) {$out .= ' checked ';}
                $out .= ' value="email" />&nbsp;<b>via email </b>
              </td>
            </tr>

          </tbody></table>
        </div>
          
          <br />
          
          
        <div class="maestro_task_edit_save_div"><input class="form-submit" type="submit" value="Save"></div>

      </form>
    </div></div></div></div></div></div></div></div>
  </div>
</div>';


	//return $out;
	
	
	
  
    return array('html' => $out);
    
  }
  
}


class PrabaOperator extends CMS_Workflow_wftaskinterface {
  function __construct($task_id=0, $template_id=0, $ltype = 'default') {
	 
    $this->_task_type = 'Operator';
    $this->_is_interactive = MaestroInteractiveFlag::IS_NOT_INTERACTIVE;

    parent::__construct($task_id, $template_id, $ltype);
  }
  
   function drawLine() {
    //die($this->_task_id);
    $cs = new Model_Zprabawf();
	//echo "<script>alert('s');</script>"	;
    $rec = $cs->cek_new_con($this->_task_id, $_POST['line_to']);
   // die("zzzz");
    if ($rec == '') {
     
    //echo $this->_ltype; die();
      $cs->create_new_conn($this->_task_id, $_POST['line_to'], 0, $this->_ltype);
      
    }
    else {
      //perhaps return a simple true/false with error message if the select failed
    }
  }

  function display() {
    return theme('maestro_task_and', array('tdid' => $this->_task_id, 'taskname' => $this->_taskname, 'ti' => $this));
  }

  function getEditFormContent() {
    return '';
  }
  
  function save() {
	//Zend_Debug::dump($_POST); die();
    $c = new Model_Zprabawf();
    $var= $c->update_process($_POST);
	return array('task_id' => $this->_task_id);
    
  }
  function getContextMenu() {
	  $cd = new Model_Zprabawf();
	$id = $this->_task_id;
    $vars = $cd->get_process_by_pid($id);
     
	$data = unserialize($vars['zparams_serialize']);
	$optype  = $data['OperatorType'];
     
    $draw_line_msg = 'Select a task to draw the line to.';
    
    if(	$optype == 'branch') {
    
		$options = array (
      'draw_line' => array(
        'label' => 'Draw Line',
        'js' => "draw_status = 1; draw_type = 'default'; line_start = document.getElementById('task{$this->_task_id}'); set_tool_tip('$draw_line_msg');\n"
      ),
      
      'draw_line_1' => array(
        'label' => 'Draw Red Line',
        'js' => "draw_status = 1; draw_type = 'red'; line_start = document.getElementById('task{$this->_task_id}'); set_tool_tip('$draw_line_msg');\n"
      ),
      'draw_line_2' => array(
        'label' => 'Draw Green Line',
        'js' => "draw_status = 1; draw_type = 'green'; line_start = document.getElementById('task{$this->_task_id}'); set_tool_tip('$draw_line_msg');\n"
      ),
      
      'draw_line_3' => array(
        'label' => 'Draw Orange Line',
        'js' => "draw_status = 1; draw_type ='orange'; line_start = document.getElementById('task{$this->_task_id}'); set_tool_tip('$draw_line_msg');\n"
      ),
      'clear_lines' => array(
        'label' => 'Clear Adjacent Lines',
        'js' => "clear_task_lines(document.getElementById('task{$this->_task_id}'));\n"
      ),
      'edit_task' => array(
        'label' => 'Edit',
        'js' => "enable_ajax_indicator(); \$.ajax({
          type: 'POST',
          url: ajax_url + 'Praba{$this->_task_type}/taskid/{$this->_task_id}/tempid/0/act/edit/format/json/',
          cache: false,
          dataType: 'json',
          success: display_task_panel,
          error: editor_ajax_error
        });"
      ),
      'delete_task' => array(
        'label' => 'Delete',
        'js' => "enable_ajax_indicator(); \$.ajax({
          type: 'POST',
          url: ajax_url + 'Praba{$this->_task_type}/taskid/{$this->_task_id}/tempid/0/act/destroy/format/json/',
          cache: false,
          success: delete_task,
          dataType: 'json',
          error: editor_ajax_error
        });\n"
      )
    );
		
	} else  {
		$options = array (
		  'draw_line' => array(
			'label' => 'Draw Line',
			'js' => "draw_status = 1; draw_type = 'default'; line_start = document.getElementById('task{$this->_task_id}'); set_tool_tip('$draw_line_msg');\n"
		  ),
		  
		  'clear_lines' => array(
			'label' => 'Clear Adjacent Lines',
			'js' => "clear_task_lines(document.getElementById('task{$this->_task_id}'));\n"
		  ),
		  'edit_task' => array(
			'label' => 'Edit',
			'js' => "enable_ajax_indicator(); \$.ajax({
			  type: 'POST',
			  url: ajax_url + 'Praba{$this->_task_type}/taskid/{$this->_task_id}/tempid/0/act/edit/format/json/',
			  cache: false,
			  dataType: 'json',
			  success: display_task_panel,
			  error: editor_ajax_error
			});"
		  ),
		  'delete_task' => array(
			'label' => 'Delete',
			'js' => "enable_ajax_indicator(); \$.ajax({
			  type: 'POST',
			  url: ajax_url + 'Praba{$this->_task_type}/taskid/{$this->_task_id}/tempid/0/act/destroy/format/json/',
			  cache: false,
			  success: delete_task,
			  dataType: 'json',
			  error: editor_ajax_error
			});\n"
		  )
		);
		
	}
    return $options;
  }
  
}

class MaestroTaskInterfaceManualWeb extends CMS_Workflow_wftaskinterface {
  function __construct($task_id=0, $template_id=0) {
    $this->_task_type = 'ManualWeb';
    $this->_is_interactive = MaestroInteractiveFlag::IS_INTERACTIVE;

    parent::__construct($task_id, $template_id);
  }

  function display() {
    return theme('maestro_task_manual_web', array('tdid' => $this->_task_id, 'taskname' => $this->_taskname, 'ti' => $this));
  }

  function getEditFormContent() {
    $this->_fetchTaskInformation();
    return theme('maestro_task_manual_web_edit', array('tdid' => $this->_task_id, 'td_rec' => $this->_task_data));
  }

  function save() {
    $rec = new stdClass();
    $rec->id = $_POST['template_data_id'];
    $rec->task_data = serialize(array(
                                    'handler' => $_POST['handler'],
                                    'new_window' => $_POST['newWindow'],
                                    'use_token' => $_POST['useToken'],

      ));
    drupal_write_record('maestro_template_data', $rec, array('id'));

    return parent::save();
  }
}


class PrabaPrahu extends CMS_Workflow_wftaskinterface {
  function __construct($task_id=0, $template_id=0) {
	 //die("ss");    
	$this->_task_type = 'Prahu';
    $this->_is_interactive = MaestroInteractiveFlag::IS_INTERACTIVE;
     parent::__construct($task_id, $template_id);
  }

  function display() {
    return theme('maestro_task_content_type', array('tdid' => $this->_task_id, 'taskname' => $this->_taskname, 'ti' => $this));
  }

  function getEditFormContent() {
    $this->_fetchTaskInformation();
    
    //edited by himawijaya 1-12-2011
    
    //fungsi asli
    //$content_types = node_type_get_types();
    
	$process_management = new stdClass();
	$process_management->type = 'process_management';
	$process_management->name = 'Process Management';
	$process_management->orig_type = 'process_management';
	$process_management->name = 'Process Management';

	$request_management = new stdClass();
	$request_management->type = 'request_management';
	$request_management->name = 'Request Management';
	$request_management->orig_type = 'request_management';
	$request_management->name = 'Request Management';

	$ntype=array('process_management'=>$process_management, 'request_management'=>$request_management);

	$content_types = $ntype;
    
    
    return theme('maestro_task_content_type_edit', array('tdid' => $this->_task_id, 'td_rec' => $this->_task_data, 'content_types' => $content_types));
  }

  function save() {
	//Zend_Debug::dump($_POST); die();
    $c = new Model_Zprabawf();
    $var= $c->update_process($_POST);
	
	return array('task_id' => $this->_task_id);
    
  }
   function edit() {
	$cd = new Model_Zprabawf();
    $id = $this->_task_id;
    $vars = $cd->get_process_by_pid($id);
    $zz = new CMS_General();
	$task_type = 'Praba'.$vars['class_type'];
	$task_class = $task_type;
	$vz = unserialize($vars['zparams_serialize']);
	//Zend_Debug::dump($vz);die();
	
	$groups = $zz->__serialize($vz['Data']['group']);
	
	$cd = new Model_Zprabawf();
	$ee = new Model_Prabasystem();
	
	$out ="";
	
	if($vz['Data']['type']!=""&&$vz['Data']['account']!="") {
		
	$out .= '
    <script>
	var val  = $( "#type").val();
	var acc  = "'.$vz['Data']['account'].'";
	$.ajax({
			url: "/prabawf/ajaxer3/id/"+val+"/acc/'.$vz['Data']['account'].'",
			dataType: "html",	
			beforeSend: function() {
			   
			},
			success: function(data) {
					$("#vaccount").html(data);
			},
			timeout:9000,
			error:function(){
				
			},
		});
		</script>
		';
	
}

if($vz['Data']['type']!=""&& count($vz['Data']['group'])>=1) {
		
	$out .= '
    <script>
		//var val  = $( "#type").val();
	
		var type  = $( "#type" ).val();
		//var acc  = $( "#account").val();
	
	$.ajax({
			url: "/prabawf/ajaxer4/id/"+acc+"/type/"+type+"/acc/"+acc+"/group/'.$groups.'",
			dataType: "html",	
			beforeSend: function() {
			   
			},
			success: function(data) {
						$("#vgroup").html(data);
			},
			timeout:9000,
			error:function(){
				
			},
		});
		</script>
		';
	
}
	
	$out .= '
   <script>
    function comp0(me) {
		var val = $(me).val();
		//alert(val);
		$.ajax({
			url: "/prabawf/ajaxer3/id/"+val,
			dataType: "html",	
			beforeSend: function() {
			   
			},
			success: function(data) {
					$("#vaccount").html(data);
			},
			timeout:9000,
			error:function(){
				
			},
		});
	}
	
	function comp1(me) {
		var val = $(me).val();
		//alert(val);
		var type  = $( "#type" ).val();
		var acc  = $( "#account").val();
		$.ajax({
			url: "/prabawf/ajaxer4/id/"+val+"/type/"+type+"/acc/"+acc,
			dataType: "html",	
			beforeSend: function() {
			   
			},
			success: function(data) {
					$("#vgroup").html(data);
			},
			timeout:9000,
			error:function(){
				
			},
		});
	}
	
    </script>
	<div>
	<div style="margin: 0px 0px 0px 10px; float: l
	<div style="margin: 0px 10px 0px 0px; float: right;">&nbsp;</div>
	<div class="active"><div class="maestro_task_edit_tab_close" style="float: right;"><div class="t"><div class=""><div class="r"><div class="l"><div class="bl-cl"><div class="br-cl"><div class="tl-cl"><div class="tr-cl">
	<a href="#" onclick="(function($) { $.modal.close(); disable_ajax_indicator(); select_boxes = []; })(jQuery); return false;">
<img src="/assets/core/plugins/prabawf/images/admin/close.png"></a>
  </div></div></div></div></div></div></div></div></div></div>

  <div style="clear: both;"></div>
  <div class="maestro_task_edit_panel">
    <div class="t"><div class="b"><div class="r"><div class="l"><div class="bl-wht"><div class="br-wht"><div class="tl-wht"><div class="tr-wht">
    
      <form id="maestro_task_edit_form" method="post" action="" onsubmit="return save_task(this);">
        <input type="hidden" name="task_class" value="'.$task_class.'">
        <input type="hidden" name="template_data_id" value="'.$id.'">
		  <div id="task_edit_main">
          <div style="float: none;" class="maestro_tool_tip maestro_taskname"><div class="t"><div class="b"><div class="r"><div class="l"><div class="bl-bge"><div class="br-bge"><div class="tl-bge"><div class="tr-bge">
          
          <table width="100%">
                  <tbody>
                  <tr>
                    <td>Type</td>
                    <td style="width: 70%;">
                    <select onchange="comp0(this)"; name="type" id="type">
						
						<option  value="" >---------</option>
						<option ';
						if($vz['Data']['type']=='telegram'){ $out .='selected';}
						$out .=	 ' value="telegram" >Telegram</option>
						<option ';
						if($vz['Data']['type']=='wa'){ $out .='selected';}
						$out .=	' value="wa">WA</option>
						<option ';
						if($vz['Data']['type']=='gtalk'){ $out .='selected';}
						$out .= 'value="gtalk">Gtalk</option>
						<option ';
						if($vz['Data']['type']=='email'){ $out .='selected';}
						$out .= 'value="email">Email</option>
						<option ';
						if($vz['Data']['type']=='sms'){ $out .='selected';} 
						
						$out .= 'value="sms">SMS</option>';
					 
            $out .=	'</select> 
              
                    </td>
                  </tr>
                  <tr>
                    <td>Account </td>
                    <td style="width: 70%;">
                     <span id="vaccount">
                    <select name="account"  onchange="comp1(this)" id="account">';
					$out .=	'</select> 
                    </span>
                    </td>
                  </tr>';
                  
                  
            $out .=' <tr>
                    <td>Group </td>
                    <td style="width: 70%;">
                     <div id="vgroup" style="height:160px;overflow:scroll;">
                    <select name="group"  id="group">';
			
			
			$out .=	'</select> 
                    </span> OR AccountId <input type=text name=accid />
                    </td>
                  </tr>';      
                  
                           
            $out .=' <tr>
                    <td>Subject </td>
                    <td style="width: 70%;">
                  
                    <input name="subject"  id="subject" value="'.$vz['Data']['subject'].'"/> 
                    
                    </td>
                  </tr>';
                  
             $out .=' <tr>
                    <td>Message </td>
                    <td style="width: 70%;">
                     <span id="vgroup">
                    <textarea name="body"  id="body">'.$vz['Data']['body'].'</textarea> 
                    </span>
                    </td>
                  </tr>';
                  
                      
               $out .=  '
                </tbody></table>';
                
         $out  .=  '</div></div></div></div></div></div></div></div></div><br />

          <br></br>

        </div>
        <div class="maestro_task_edit_save_div"><input class="form-submit" type="submit" value="Save"></div>

      </form>
    </div></div></div></div></div></div></div></div>
  </div>
</div>';


	//return $out;
	
	
	
  
    return array('html' => $out);
    
  }
  
}



class PrabaTrigger extends CMS_Workflow_wftaskinterface {
  function __construct($task_id=0, $template_id=0) {
	 // die("ss");    
	  $this->_task_type = 'Trigger';
    $this->_is_interactive = MaestroInteractiveFlag::IS_INTERACTIVE;
     parent::__construct($task_id, $template_id);
  }
 function edit() {
	$cd = new Model_Zprabawf();
    $id = $this->_task_id;
    $vars = $cd->get_process_by_pid($id);
    
	$task_type = 'Praba'.$vars['class_type'];
	$task_class = $task_type;
   
	
	$cd = new Model_Zprabawf();
	$ee = new Model_Prabasystem();
	
	
	$vdat  = $cd->get_api_by_id ($vars['ext_id']); 
   /// $conn = $cd->get_conn_id();
    $conn  =  $ee->get_conn ();
   // var_dump($vdat); die(); 
	
	$out = '
    <script>
 
    function comp0(me) {
		var val = $(me).val();
		//alert(val);
		$.ajax({
			url: "/prabawf/ajaxer3/id/"+val,
			dataType: "html",	
			beforeSend: function() {
			   
			},
			success: function(data) {
					$("#vaccount").html(data);
			},
			timeout:9000,
			error:function(){
				
			},
		});
	}
	
	function comp1(me) {
		var val = $(me).val();
		//alert(val);
		$.ajax({
			url: "/prabawf/ajaxer4/id/"+val,
			dataType: "html",	
			beforeSend: function() {
			   
			},
			success: function(data) {
					$("#vgroup").html(data);
			},
			timeout:9000,
			error:function(){
				
			},
		});
	}
	
    </script>
	<div>
	<div style="margin: 0px 0px 0px 10px; float: l
	<div style="margin: 0px 10px 0px 0px; float: right;">&nbsp;</div>
	<div class="active"><div class="maestro_task_edit_tab_close" style="float: right;"><div class="t"><div class=""><div class="r"><div class="l"><div class="bl-cl"><div class="br-cl"><div class="tl-cl"><div class="tr-cl">
	<a href="#" onclick="(function($) { $.modal.close(); disable_ajax_indicator(); select_boxes = []; })(jQuery); return false;">
<img src="/assets/core/plugins/prabawf/images/admin/close.png"></a>
  </div></div></div></div></div></div></div></div></div></div>

  <div style="clear: both;"></div>
  <div class="maestro_task_edit_panel">
    <div class="t"><div class="b"><div class="r"><div class="l"><div class="bl-wht"><div class="br-wht"><div class="tl-wht"><div class="tr-wht">
    
      <form id="maestro_task_edit_form" method="post" action="" onsubmit="return save_task(this);">
        <input type="hidden" name="task_class" value="'.$task_class.'">
        <input type="hidden" name="template_data_id" value="'.$id.'">
		  <div id="task_edit_main">
          <div style="float: none;" class="maestro_tool_tip maestro_taskname"><div class="t"><div class="b"><div class="r"><div class="l"><div class="bl-bge"><div class="br-bge"><div class="tl-bge"><div class="tr-bge">
          
          <table width="100%">
                  <tbody>
                  <tr>
                    <td>Type</td>
                    <td style="width: 70%;">
                    <select onchange="comp0(this)"; name="type" id="type">
						<option value="" >---------</option>
						<option value="form" >Html Portlet</option>
						<option value="Cron">Cron</option>';
					 
            $out .=	'</select> 
              
                    </td>
                  </tr>
                  <tr>
                    <td>Id </td>
                    <td style="width: 70%;">
                     <span id="vid">
                    <select name="fid"  onchange="comp1(this)" id="fid">';
					$out .=	'</select> 
                    </span>
                    </td>
                  </tr>';
                  
                  
 
                  
                      
               $out .=  '
                </tbody></table>';
                
         $out  .=  '</div></div></div></div></div></div></div></div></div><br />

          <br></br>

        </div>
        <div class="maestro_task_edit_save_div"><input class="form-submit" type="submit" value="Save"></div>

      </form>
    </div></div></div></div></div></div></div></div>
  </div>
</div>';


	//return $out;
	
	
	
  
    return array('html' => $out);
    
  }
  function display() {
    return theme('maestro_task_content_type', array('tdid' => $this->_task_id, 'taskname' => $this->_taskname, 'ti' => $this));
  }

  function getEditFormContent() {
    $this->_fetchTaskInformation();
    
    //edited by himawijaya 1-12-2011
    
    //fungsi asli
    //$content_types = node_type_get_types();
    
	$process_management = new stdClass();
	$process_management->type = 'process_management';
	$process_management->name = 'Process Management';
	$process_management->orig_type = 'process_management';
	$process_management->name = 'Process Management';

	$request_management = new stdClass();
	$request_management->type = 'request_management';
	$request_management->name = 'Request Management';
	$request_management->orig_type = 'request_management';
	$request_management->name = 'Request Management';

	$ntype=array('process_management'=>$process_management, 'request_management'=>$request_management);

	$content_types = $ntype;
    
    
    return theme('maestro_task_content_type_edit', array('tdid' => $this->_task_id, 'td_rec' => $this->_task_data, 'content_types' => $content_types));
  }

  function save() {
	//Zend_Debug::dump($_POST); die();
    $c = new Model_Zprabawf();
    $var= $c->update_process($_POST);
	
	return array('task_id' => $this->_task_id);
    
  }
}


class PrabaTask extends CMS_Workflow_wftaskinterface {
  function __construct($task_id=0, $template_id=0) {
	 // die("ss");    
	  $this->_task_type = 'Task';
    $this->_is_interactive = MaestroInteractiveFlag::IS_INTERACTIVE;
     parent::__construct($task_id, $template_id);
  }

  function display() {
    return theme('maestro_task_content_type', array('tdid' => $this->_task_id, 'taskname' => $this->_taskname, 'ti' => $this));
  }

  function getEditFormContent() {
    $this->_fetchTaskInformation();
    
    //edited by himawijaya 1-12-2011
    
    //fungsi asli
    //$content_types = node_type_get_types();
    
	$process_management = new stdClass();
	$process_management->type = 'process_management';
	$process_management->name = 'Process Management';
	$process_management->orig_type = 'process_management';
	$process_management->name = 'Process Management';

	$request_management = new stdClass();
	$request_management->type = 'request_management';
	$request_management->name = 'Request Management';
	$request_management->orig_type = 'request_management';
	$request_management->name = 'Request Management';

	$ntype=array('process_management'=>$process_management, 'request_management'=>$request_management);

	$content_types = $ntype;
    
    
    return theme('maestro_task_content_type_edit', array('tdid' => $this->_task_id, 'td_rec' => $this->_task_data, 'content_types' => $content_types));
  }

  function save() {
	//Zend_Debug::dump($_POST); die();
    $c = new Model_Zprabawf();
    $var= $c->update_process($_POST);
	
	return array('task_id' => $this->_task_id);
    
  }
}


class MaestroTaskInterfaceFireTrigger extends CMS_Workflow_wftaskinterface {
  function __construct($task_id=0, $template_id=0) {
    $this->_task_type = 'FireTrigger';
    $this->_is_interactive = MaestroInteractiveFlag::IS_NOT_INTERACTIVE;

    parent::__construct($task_id, $template_id);

    $this->_task_edit_tabs = array('optional' => 1);
  }

  function display() {
    return theme('maestro_task_fire_trigger', array('tdid' => $this->_task_id, 'taskname' => $this->_taskname, 'ti' => $this));
  }

  function getEditFormContent() {
    $this->_fetchTaskInformation();
    $query = db_select('trigger_assignments', 'a');
    $query->fields('a', array('hook'));
    $query->fields('b', array('aid', 'label'));
    $query->leftJoin('actions', 'b', 'a.aid=b.aid');
    $query->condition('a.hook', "fire_trigger_task{$this->_task_id}", '=');
    $aa_res = $query->execute();

    $options = array();
    $functions = array();
    $hook = 'fire_trigger_task' . $this->_task_id;
    // Restrict the options list to actions that declare support for this hook.
    foreach (actions_list() as $func => $metadata) {
      if (isset($metadata['triggers']) && array_intersect(array($hook, 'any'), $metadata['triggers'])) {
        $functions[] = $func;
      }
    }
    foreach (actions_get_all_actions() as $aid => $action) {
      if (in_array($action['callback'], $functions)) {
        $options[$action['type']][$aid] = $action['label'];
      }
    }

    return theme('maestro_task_fire_trigger_edit', array('tdid' => $this->_task_id, 'td_rec' => $this->_task_data, 'aa_res' => $aa_res, 'options' => $options));
  }

  function save() {
    $actions = $_POST['actions'];
    $hook = 'fire_trigger_task' . $this->_task_id;

    $res = db_delete('trigger_assignments')
      ->condition('hook', $hook)
      ->execute();

    $weight = 1;
    foreach ($actions as $aid) {
      $rec = new stdClass();
      $rec->hook = $hook;
      $rec->aid = $aid;
      $rec->weight = $weight++;
      drupal_write_record('trigger_assignments', $rec);
    }

    return parent::save();
  }
}

