//== Class definition


var Datatable_listpage = function() {
  //== Private functions

  // basic demo
  var demo = function() {

    var datatable_2 = $('#listpage').mDatatable({
      // datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            // sample GET method
            method: 'GET',
            url: '/bigbox/bigbuilder/listpagej',
            // url: 'https://keenthemes.com/metronic/preview/inc/api/datatables/demos/default.php',
            map: function(raw) {
              // sample data mapping
              var dataSet = raw;
              if (typeof raw.data !== 'undefined') {
                dataSet = raw.data;
              }
              return dataSet;
            },
          },
        },
        pageSize: 10,
        serverPaging: true,
        serverFiltering: true,
        serverSorting: true,
      },

      // layout definition
      layout: {
        scroll: false,
        footer: false
      },

      // column sorting
      sortable: true,

      pagination: true,

      toolbar: {
        // toolbar items
        items: {
          // pagination
          pagination: {
            // page size select
            pageSizeSelect: [10, 20, 30, 50, 100],
          },
        },
      },

      search: {
        input: $('#generalSearch'),
      },

      // columns definition
      columns: [
        {
          field: 'No',
          title: 'No',
          sortable: true,
          width: 40,
          selector: false,
          textAlign: 'center',
        },
		{
          field: 'id',
          title: 'ID',
          sortable: true,
          width: 40,
          selector: false,
          textAlign: 'center',
        },{
          field: 'name_alias',
          title: 'Name Alias',
          width: 150,
          template: function(row) {
            // callback function support for column rendering
            return row.name_alias;
          },
        }, {
          field: 'layout_id',
          title: 'Layout ID',
          width: 120,
          template: function(row) {
            // callback function support for column rendering
            return row.layout_id;
          },
        }, {
          field: 'workspace_id',
          title: 'Workspace ID',
          width: 120,
          template: function(row) {
            // callback function support for column rendering
            return row.workspace_id;
          },
        }, {
          field: 'app_id',
          title: 'App ID',
          width: 120,
          template: function(row) {
            // callback function support for column rendering
            return row.app_id;
          },
        }, {
          field: 'title',
          title: 'Title',
          width: 120,
          template: function(row) {
            // callback function support for column rendering
            return row.title;
          },
        }, {
          field: 'Actions',
          width: 150,
          title: 'Actions',
          sortable: false,
          overflow: 'visible',
          template: function (row, index, datatable) {
            var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
            return '\
						<a href="/prabagen/index/page/'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon m-btn--icon-only m-btn--pill" title="View">\
							<i class="la la-search"></i>\
						</a>\
						<a href="/bigbox/bigbuilder/editpage/id/'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit">\
							<i class="la la-edit"></i>\
						</a>\
						<a data-toggle="modal" data-target="#m_modal_1" data-id="'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btn-delete" title="Delete">\
							<i class="la la-trash"></i>\
						</a>\
					';
          },
        }],
    });
	$('#reload').on('click', function() {
		datatable_2.reload();
    });
    // $('#m_form_status').on('change', function() {
      // datatable_2.search($(this).val().toLowerCase(), 'Status');
    // });

    // $('#m_form_type').on('change', function() {
      // datatable_2.search($(this).val().toLowerCase(), 'Type');
    // });

    // $('#m_form_status, #m_form_type').selectpicker();

  };

  
  return {
    // public functions
    init: function() {
      demo();
    },
  };
}();
jQuery(document).ready(function() {
  Datatable_listpage.init();
  
});