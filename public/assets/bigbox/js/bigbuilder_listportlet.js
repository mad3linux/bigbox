//List Portlet Javascript
var Datatable_listportlet = function() {
  //== Private functions

  // basic demo
  var listportlet = function() {

    var datatable_1 = $('#listportlet').mDatatable({
      // datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            // sample GET method
            method: 'GET',
            url: '/bigbox/bigbuilder/listportletj',
            map: function(raw) {
              // sample data mapping
              var dataSet = raw;
              if (typeof raw.data !== 'undefined') {
                dataSet = raw.data;
              }
              return dataSet;
            },
          },
        },
        pageSize: 10,
        serverPaging: true,
        serverFiltering: true,
        serverSorting: true,
      },

      // layout definition
      layout: {
        scroll: false,
        footer: false
      },

      // column sorting
      sortable: true,

      pagination: true,

      toolbar: {
        // toolbar items
        items: {
          // pagination
          pagination: {
            // page size select
            pageSizeSelect: [10, 20, 30, 50, 100],
          },
        },
      },
      search: {
        input: $('#generalSearch'),
      },
      // columns definition
      columns: [
        {
          field: 'No',
          title: 'No',
          sortable: true,
          width: 40,
          selector: false,
          textAlign: 'center',
        },
		{
          field: 'id',
          title: 'ID',
          sortable: true,
          width: 40,
          selector: false,
          textAlign: 'center',
        },{
          field: 'portlet_name',
          title: 'Name',
          width: 120,
          template: function(row) {
            // callback function support for column rendering
            return row.portlet_name;
          },
        }, {
          field: 'portlet_title',
          title: 'Title',
          width: 120,
          template: function(row) {
            // callback function support for column rendering
            return row.portlet_title;
          },
        }, {
          field: 'portlet_type',
          title: 'Type',
          width: 100,
          template: function(row) {
            // callback function support for column rendering
            return row.portlet_type;
          },
        }, {
          field: 'Actions',
          width: 150,
          title: 'Actions',
          sortable: false,
          overflow: 'visible',
          template: function (row, index, datatable) {
            var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
            return '\
						<a href="/bigbox/bigbuilder/viewportlet/id/'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon m-btn--icon-only m-btn--pill" title="View">\
							<i class="la la-search"></i>\
						</a>\
						<a href="/bigbox/bigbuilder/editportlet/id/'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit">\
							<i class="la la-edit"></i>\
						</a>\
						<a data-toogle="modal"  data-target="#ajax-modal"  data-id="'+row.id+'"  class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill getscript" title="Download Script">\
							<i class="la la-download"></i>\
						</a>\
						<a data-toggle="modal" data-target="#m_modal_1" data-id="'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btn-delete" title="Delete">\
							<i class="la la-trash"></i>\
						</a>\
					';
          },
        }],
    });
	$('#sample_2_column_toggler input[type="checkbox"]').on('change', function() {
		/* Get the DataTables object again - this is not a recreation, just a get of the object */
		
		var iCol = $(this).attr("data-column");
		datatable_1.hideColumn(iCol);
		
	});
	$('#reload').on('click', function() {
		datatable_1.reload();
    });
    

    // $('#m_form_type').on('change', function() {
      // datatable_1.search($(this).val().toLowerCase(), 'Type');
    // });

    // $('#m_form_status, #m_form_type').selectpicker();

  };

  
  return {
    // public functions
    init: function() {
      listportlet();
    },
  };
}();

var ModalGetScript = function () {
    return {
        //main function to initiate the module
        init: function () {
            var $modal = $('#ajax-modal');
           // $('.ajax-demo').on('click', function(){
              $('body').on('click', '#listportlet .getscript', function() {
				var id =  $(this).attr("data-id");
				//  alert(id);  
				// create the backdrop and wait for next modal to be triggered
				// $('body').modalmanager('loading');
				setTimeout(function(){
				  $modal.load('/bigbox/bigbuilder/getscript/id/'+id , '', function(){
						$modal.modal();
				  });
				}, 1000);
            });
        }

    };

}();

var ModalDelete = function () {
    return {
        //main function to initiate the module
        init: function () {
			// handle record edit/remove
			var $modal2 = $('#m_modal_1');
			$('body').on('click', '#listportlet .btn-delete', function() {
			  var id =  $(this).attr("data-id");
			 
			  if($modal2.find('td#static_id')!=undefined){
				$modal2.find('td#static_id').html(id);
			  }
			  $modal2.modal();                
			});
			
			$modal2.on('click', 'button#deleteconfirm', function(){
				var id = $modal2.find('td#static_id').html();
				// alert(id);
				exit();
				$.ajax({
					url:"/prabajax/deleteportlet",
					content:'json',
					type:'post',
					data:{uid:id},
					beforeSend:function(){
						$('#processbar2').removeClass('m--hide');
						$('#alertinfo2').removeClass('alert-success');
						$('#alertinfo2').removeClass('alert-warning');
						$('#alertinfo2').removeClass('alert-danger');
						$('#alertinfo2').removeClass('alert-info');
					},
					complete:function(result){
						$('#processbar2').addClass('m--hide');
					},
					success: function(data){
						//console.log(data);
						if(data.result!=undefined){
							if(data.result==true){
								$('#alertinfo2').addClass('alert-success');
								$('#alertmsg2').html("<strong>Success!</strong> "+data.retMsg);
								$("a.reload").click();
								$modal2.modal('hide');
							}else{
								$('#alertinfo2').addClass('alert-warning');
								$('#alertmsg2').html("<strong>Failed!</strong> "+data.retMsg);
							}
						}else{
							$('#alertinfo2').addClass('alert-info');
							$('#alertmsg2').html("<strong>Error!</strong> System is busy, please try again.");
						}
						$('#alertinfo2').removeClass('m--hide');
					},
					error:function(){
						$('#alertinfo2').addClass('alert-info');
						$('#alertmsg2').html("<strong>Error!</strong> System is busy, please try again.");
						$('#alertinfo2').removeClass('m--hide');
					}
				});
			});
           
        }

    };

}();

var EditApi = function () {

	var handleAdd = function() {

		$('form#addapi').validate({
	            //errorElement: 'span', //default input error message container
	            //errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	                uname: {
	                    required: true
	                },
	                fullname: {
	                    required: true
	                },
	                pass: {
	                    required: true
	                },
	                email: {
	                    email: true
	                }
	            },

	            messages: {
					
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
	                //$('.alert-danger', $('.login-form')).show();
	                //console.log(event);
	                //console.log(validator);
	                
					//$('span.help-block').show();
					$('span.help-block').hide();
	                var errorlist = validator.errorList;
	                if(errorlist.length>0){
						for(var i = 0; i<errorlist.length; i++){
							//console.log(errorlist[i].element);
							var el = errorlist[i].element; 
							var next = $(el).next('span.help-block'); 
							$(next).slideDown();
						}
						$('html,body').animate({
							scrollTop: $(el).offset().top-100
						}, 800);
					}
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                //label.remove();
	            },

	            errorPlacement: function (error, element) {
					$('span.help-block').hide();
					var next = element.next('span.help-block'); 
					//console.log(error);
	                if(error[0].textContent!=""){
						$(next).children('span.label').html("<i class='fa fa-warning'></i> "+ error[0].textContent);
						$(element).next().slideDown();
						$('html,body').animate({
							scrollTop: $(element).offset().top-100
						}, 800);
					}
	            },

	            submitHandler: function (form) {
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	                //form.submit(); // form validation success, call ajax form submit
	                submitForm();
	                return false;
	            }
	        });

	        $('form#addapi input').keypress(function (e) {
	            if (e.which == 13) {
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	                if ($('form#addapi').validate().form()) {
	                   //$('form#addapi').submit(); //form validation success, call ajax form submit
	                   submitForm();
	                }
	                return false;
	            }
	        });
	}
	
	var submitForm = function(){
		//console.log($('form#addapi').serialize());
		$.ajax({
			url:"/ajax/adduser",
			content:'json',
			type:'post',
			data:$('form#addapi').serialize(),
			beforeSend:function(){
				$('#alertinfo').addClass('hidden');
				$('#processbar').removeClass('hidden');
				$('html,body').animate({
					scrollTop: $('h3.page-title').offset().top
				}, 800);
				$('#alertinfo').removeClass('alert-success');
				$('#alertinfo').removeClass('alert-warning');
				$('#alertinfo').removeClass('alert-danger');
				$('#alertinfo').removeClass('alert-info');
			},
			complete:function(result){
				$('#processbar').addClass('hidden');
			},
			success: function(data){
				//console.log(data);
				if(data.result!=undefined){
					if(data.result==true){
						$('#alertinfo').addClass('alert-success');
						$('#alertmsg').html("<strong>Success!</strong> "+data.retMsg);
					}else{
						$('#alertinfo').addClass('alert-warning');
						$('#alertmsg').html("<strong>Failed!</strong> "+data.retMsg);
					}
				}else{
					$('#alertinfo').addClass('alert-info');
					$('#alertmsg').html("<strong>Error!</strong> System is busy, please try again.");
				}
				$('#alertinfo').removeClass('hidden');
			},
			error:function(){
				$('#alertinfo').addClass('alert-info');
				$('#alertmsg').html("<strong>Error!</strong> System is busy, please try again.");
				$('#alertinfo').removeClass('hidden');
			}
		});
	}
	
	

	var resetForm = function(){
		$('input','form#addapi')
			.not(':button, :submit, :reset, :hidden')
			.val('');
			
		$('select,option','form#addapi')
			.removeAttr('checked')
			.removeAttr('selected');
		
		$('.make-switch').bootstrapSwitch('setState',false);
		
		$('.ms-selectable').find('li').removeClass('ms-selected');
		$('.ms-selection').find('li').removeClass('ms-hover');
		$('.ms-selectable').find('li').removeAttr('style');
		
		$('.ms-selection').find('li').removeClass('ms-selected');
		$('.ms-selection').find('li').removeClass('ms-hover');
		$('.ms-selection').find('li').css({display:'none'});
	}
    
    return {
        //main function to initiate the module
        init: function () {
            handleAdd();
        }

    };

}();

jQuery(document).ready(function() {
	Datatable_listportlet.init();
	ModalDelete.init();
	ModalGetScript.init();
	EditApi.init();
});