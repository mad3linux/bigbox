
var UIExtendedModals3 = function () {
    
    return {
        //main function to initiate the module
        init: function () {
        
            // general settings
            // $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner = 
              // '<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
                // '<div class="progress progress-striped active">' +
                  // '<div class="progress-bar" style="width: 100%;"></div>' +
                // '</div>' +
              // '</div>';

            // $.fn.modalmanager.defaults.resize = true;
            
            //static demo:
            var $modal2 = $('#static');

			// handle record edit/remove
            $('body').on('click', '#user_table_wrapper .btn-delete', function() {
              var id =  $(this).attr("data-id");
             
              if($modal2.find('td#static_id')!=undefined){
                $modal2.find('td#static_id').html(id);
              }
              
              $modal2.modal();                
            });
			
            $modal2.on('click', 'button#deleteconfirm', function(){
				var id = $modal2.find('td#static_id').html();
				//alert(id);
				$.ajax({
					url:"/prabajax/deleteapi",
					content:'json',
					type:'post',
					data:{uid:id},
					beforeSend:function(){
						$('#processbar2').removeClass('hidden');
						$('#alertinfo2').removeClass('alert-success');
						$('#alertinfo2').removeClass('alert-warning');
						$('#alertinfo2').removeClass('alert-danger');
						$('#alertinfo2').removeClass('alert-info');
					},
					complete:function(result){
						$('#processbar2').addClass('hidden');
					},
					success: function(data){
						//console.log(data);
						if(data.result!=undefined){
							if(data.result==true){
								$('#alertinfo2').addClass('alert-success');
								$('#alertmsg2').html("<strong>Success!</strong> "+data.retMsg);
								$("a.reload").click();
								$modal2.modal('hide');
							}else{
								$('#alertinfo2').addClass('alert-warning');
								$('#alertmsg2').html("<strong>Failed!</strong> "+data.retMsg);
							}
						}else{
							$('#alertinfo2').addClass('alert-info');
							$('#alertmsg2').html("<strong>Error!</strong> System is busy, please try again.");
						}
						$('#alertinfo2').removeClass('hidden');
					},
					error:function(){
						$('#alertinfo2').addClass('alert-info');
						$('#alertmsg2').html("<strong>Error!</strong> System is busy, please try again.");
						$('#alertinfo2').removeClass('hidden');
					}
				});
            });
            
            var $modal3 = $('#static2');

			// handle record edit/remove
            $('body').on('click', '#btn-addnew', function() {
              $modal3.modal();   
            });
            
            $modal3.on('hide.bs.modal', function (e) {
				$('#processbar').addClass('hidden');   
				$('#alertinfo').addClass('hidden'); 
			});
            
            $modal3.on('hidden.bs.modal', function (e) {
				$('#processbar').addClass('hidden');   
				$('#alertinfo').addClass('hidden');
			});
        }

    };

}();



var AddConn = function () {

	var handleAdd = function() {

		$('form#addconn').validate({
	            //errorElement: 'span', //default input error message container
	            //errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	                conn: {
	                    required: true
	                },
	                
	               // dbname: {
	                //    required: true
	               // },
	               // host: {
	                 //   required: true
	                //},
	                // username: {
	                //  ''  required: true
	                //}
	            },

	            messages: {
					
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
	                //$('.alert-danger', $('.login-form')).show();
	                //console.log(event);
	                //console.log(validator);
	                
					//$('span.help-block').show();
					$('span.help-block').hide();
	                var errorlist = validator.errorList;
	                if(errorlist.length>0){
						for(var i = 0; i<errorlist.length; i++){
							//console.log(errorlist[i].element);
							var el = errorlist[i].element; 
							var next = $(el).next('span.help-block'); 
							$(next).slideDown();
						}
						$('html,body').animate({
							scrollTop: $(el).offset().top-100
						}, 800);
					}
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                //label.remove();
	            },

	            errorPlacement: function (error, element) {
					$('span.help-block').hide();
					var next = element.next('span.help-block'); 
					//console.log(error);
	                if(error[0].textContent!=""){
						$(next).children('span.label').html("<i class='fa fa-warning'></i> "+ error[0].textContent);
						$(element).next().slideDown();
						$('html,body').animate({
							scrollTop: $(element).offset().top-100
						}, 800);
					}
	            },

	            submitHandler: function (form) {
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	                //form.submit(); // form validation success, call ajax form submit
	                submitForm();
	                return false;
	            }
	        });

	        $('form#addapi input').keypress(function (e) {
	            if (e.which == 13) {
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	                if ($('form#addconn').validate().form()) {
	                   //$('form#addapi').submit(); //form validation success, call ajax form submit
	                   submitForm();
	                }
	                return false;
	            }
	        });
	}
	
	var submitForm = function(){
		//console.log($('form#addapi').serialize());
		$.ajax({
			url:"/prabajax/addconn",
			content:'json',
			type:'post',
			data:$('form#addconn').serialize(),
			beforeSend:function(){
				$('#alertinfo').addClass('hidden');
				$('#processbar').removeClass('hidden');
				$('html,body').animate({
					scrollTop: $('h3.page-title').offset().top
				}, 800);
				$('#alertinfo').removeClass('alert-success');
				$('#alertinfo').removeClass('alert-warning');
				$('#alertinfo').removeClass('alert-danger');
				$('#alertinfo').removeClass('alert-info');
			},
			complete:function(result){
				$('#processbar').addClass('hidden');
			},
			success: function(data){
				//console.log(data);
				if(data.result!=undefined){
					if(data.result==true){
						$('#alertinfo').addClass('alert-success');
						$('#alertmsg').html("<strong>Success!</strong> "+data.retMsg);
						window.location.replace("/prabaeditor/listconn");
					}else{
						$('#alertinfo').addClass('alert-warning');
						$('#alertmsg').html("<strong>Failed!</strong> "+data.retMsg);
					}
				}else{
					$('#alertinfo').addClass('alert-info');
					$('#alertmsg').html("<strong>Error!</strong> System is busy, please try again.");
				}
				$('#alertinfo').removeClass('hidden');
			},
			error:function(){
				$('#alertinfo').addClass('alert-info');
				$('#alertmsg').html("<strong>Error!</strong> System is busy, please try again.");
				$('#alertinfo').removeClass('hidden');
			}
		});
	}
	
	

	var resetForm = function(){
		$('input','form#addconn')
			.not(':button, :submit, :reset, :hidden')
			.val('');
			
		$('select,option','form#addapi')
			.removeAttr('checked')
			.removeAttr('selected');
		
		$('.make-switch').bootstrapSwitch('setState',false);
		
		$('.ms-selectable').find('li').removeClass('ms-selected');
		$('.ms-selection').find('li').removeClass('ms-hover');
		$('.ms-selectable').find('li').removeAttr('style');
		
		$('.ms-selection').find('li').removeClass('ms-selected');
		$('.ms-selection').find('li').removeClass('ms-hover');
		$('.ms-selection').find('li').css({display:'none'});
	}
    
    return {
        //main function to initiate the module
        init: function () {
            handleAdd();
        }

    };

}();
jQuery(document).ready(function() {
  AddConn.init();
  UIExtendedModals3.init();
  
});