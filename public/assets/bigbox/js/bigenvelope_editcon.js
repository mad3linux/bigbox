
var EditApi = function () {

	var handleAdd = function() {

		$('form#addapi').validate({
	            //errorElement: 'span', //default input error message container
	            //errorClass: 'm-form__help', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	                sqltext: {
	                    required: true
	                }
	            },
	         

	            messages: {
					
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
	                //$('.alert-danger', $('.login-form')).show();
	                //console.log(event);
	                //console.log(validator);
	                
					//$('span.m-form__help').show();
					$('span.m-form__help').hide();
	                var errorlist = validator.errorList;
	                if(errorlist.length>0){
						for(var i = 0; i<errorlist.length; i++){
							//console.log(errorlist[i].element);
							var el = errorlist[i].element; 
							var next = $(el).next('span.m-form__help'); 
							$(next).slideDown();
						}
						$('html,body').animate({
							scrollTop: $(el).offset().top-100
						}, 800);
					}
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                //label.remove();
	            },

	            errorPlacement: function (error, element) {
					$('span.m-form__help').hide();
					var next = element.next('span.m-form__help'); 
					//console.log(error);
	                if(error[0].textContent!=""){
						$(next).children('span.label').html("<i class='fa fa-warning'></i> "+ error[0].textContent);
						$(element).next().slideDown();
						$('html,body').animate({
							scrollTop: $(element).offset().top-100
						}, 800);
					}
	            },

	            submitHandler: function (form) {
					$('span.m-form__help').hide();
					$('.form-group').removeClass('has-error');
	                //form.submit(); // form validation success, call ajax form submit
	                submitForm();
	                return false;
	            }
	        });

	        $('form#addapi input').keypress(function (e) {
	            if (e.which == 13) {
					$('span.m-form__help').hide();
					$('.form-group').removeClass('has-error');
	                if ($('form#addapi').validate().form()) {
	                   //$('form#addapi').submit(); //form validation success, call ajax form submit
	                   submitForm();
	                }
	                return false;
	            }
	        });
	}
	
	var submitForm = function(){
		//console.log($('form#addapi').serialize());
		$.ajax({
			url:"/prabajax/editapi",
			content:'json',
			type:'post',
			data:$('form#addapi').serialize(),
			beforeSend:function(){
				$('#alertinfo').addClass('hidden');
				$('#processbar').removeClass('hidden');
				$('html,body').animate({
					scrollTop: $('h3.page-title').offset().top
				}, 800);
				$('#alertinfo').removeClass('alert-success');
				$('#alertinfo').removeClass('alert-warning');
				$('#alertinfo').removeClass('alert-danger');
				$('#alertinfo').removeClass('alert-info');
			},
			complete:function(result){
				$('#processbar').addClass('hidden');
			},
			success: function(data){
				//console.log(data);
				if(data.result!=undefined){
					if(data.result==true){
						$('#alertinfo').addClass('alert-success');
						$('#alertmsg').html("<strong>Success!</strong> "+data.retMsg);
					}else{
						$('#alertinfo').addClass('alert-warning');
						$('#alertmsg').html("<strong>Failed!</strong> "+data.retMsg);
					}
				}else{
					$('#alertinfo').addClass('alert-info');
					$('#alertmsg').html("<strong>Error!</strong> System is busy, please try again.");
				}
				$('#alertinfo').removeClass('hidden');
			},
			error:function(){
				$('#alertinfo').addClass('alert-info');
				$('#alertmsg').html("<strong>Error!</strong> System is busy, please try again.");
				$('#alertinfo').removeClass('hidden');
			}
		});
	}
	
	

	var resetForm = function(){
		$('input','form#addapi')
			.not(':button, :submit, :reset, :hidden')
			.val('');
			
		$('select,option','form#addapi')
			.removeAttr('checked')
			.removeAttr('selected');
		
		$('.make-switch').bootstrapSwitch('setState',false);
		
		$('.ms-selectable').find('li').removeClass('ms-selected');
		$('.ms-selection').find('li').removeClass('ms-hover');
		$('.ms-selectable').find('li').removeAttr('style');
		
		$('.ms-selection').find('li').removeClass('ms-selected');
		$('.ms-selection').find('li').removeClass('ms-hover');
		$('.ms-selection').find('li').css({display:'none'});
	}
    
    return {
        //main function to initiate the module
        init: function () {
            handleAdd();
        }

    };

}();

jQuery(document).ready(function() {
	//$('body').addClass('page-header-fixed page-sidebar-fixed page-footer-fixed');
	//App.init();
	//FormComponents3.init();
	//$('span.help-block').hide();
	$('span.m-form__help').hide();
	EditApi.init();
	$( "#testig" ).click(function() {
		 $.ajax({
				
				url:"/prabajax/tesapi",
				content:'html',
				type:'post',
				data:$('form#addapi').serialize(),
				
				complete:function(result){
							
				},
				success: function(result){
					//alert("ok");
					//var jsonObj = $.parseJSON('[' + result + ']');
					//alert (jsonObj); 
					
					
					//	$outputsqltext.html(result);
				//	$('#outputsqltext').html(result);
					
					$('#outputsqltext').html(result);
						$.ajax({
						
						url:"/prabajax/getout",
						content:'html',
						type:'post',
						data:$('form#addapi').serialize(),
						
						complete:function(result){
									
						},
						success: function(result){
							
							$('#arrayout').html(result);
							
							
						},
						error:function(){
							$(next).children('span.label').html('<i class="fa fa-warning"></i>&nbsp;Failed to retrieve data.');
						}
						});
						
				},
				error:function(){
					$(next).children('span.label').html('<i class="fa fa-warning"></i>&nbsp;Failed to retrieve data.');
				}
				});
		});
});