//== Class definition

var Datatable_listpage = function() {
  // basic demo
  var demo = function() {
    var datatable_2 = $('#listapi').mDatatable({
      // datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            // sample GET method
            method: 'GET',
            url: '/bigbox/bigenvelope/listapij',
            // url: 'https://keenthemes.com/metronic/preview/inc/api/datatables/demos/default.php',
            map: function(raw) {
              // sample data mapping
              var dataSet = raw;
              if (typeof raw.data !== 'undefined') {
                dataSet = raw.data;
              }
              return dataSet;
            },
          },
        },
        pageSize: 10,
        serverPaging: true,
        serverFiltering: true,
        serverSorting: true,
      },

      // layout definition
      layout: {
        scroll: false,
        footer: false
      },

      // column sorting
      sortable: true,

      pagination: true,

      toolbar: {
        // toolbar items
        items: {
          // pagination
          pagination: {
            // page size select
            pageSizeSelect: [10, 20, 30, 50, 100],
          },
        },
      },

      search: {
        input: $('#generalSearch'),
      },

      // columns definition
      columns: [
        {
          field: 'NO',
          title: 'No',
          sortable: true,
          width: 40,
          selector: false,
          textAlign: 'center',
        },
		{
          field: 'id',
          title: 'ID',
          sortable: true,
          width: 40,
          selector: false,
          textAlign: 'center',
        },{
          field: 'conn_name',
          title: 'Conn Name',
          width: 150,
          template: function(row) {
            // callback function support for column rendering
            return row.conn_name;
          },
        },{
          field: 'sql_text',
          title: 'SQL Text',
          width: 200,
          template: function(row) {
            // callback function support for column rendering
            return row.sql_text;
          },
        },{
          field: 'api_desc',
          title: 'API Desc',
          width: 150,
          template: function(row) {
            // callback function support for column rendering
            return row.api_desc;
          },
        },{
          field: 'api_type',
          title: 'API Type',
          width: 150,
          template: function(row) {
            // callback function support for column rendering
            return row.api_type;
          },
        },{
          field: 'api_str_replace',
          title: 'API Str Replace',
          width: 100,
          template: function(row) {
            // callback function support for column rendering
            return row.api_str_replace;
          },
        },{
          field: 'attrs1',
          title: 'Attrs1',
          width: 100,
          template: function(row) {
            // callback function support for column rendering
            return row.attrs1;
          },
        },{
          field: 'cache_time',
          title: 'Cache Time',
          width: 100,
          template: function(row) {
            // callback function support for column rendering
            return row.cache_time;
          },
        }, {
          field: 'action',
          title: 'Actions',
          width: 200,
          template: function(row) {
            // callback function support for column rendering
            return row.action;
          },
        }],
    });
	$('#reload').on('click', function() {
		datatable_2.reload();
    });
	
	$('#filterid').on('change', function() {
        datatable_2.search($(this).val().toLowerCase(), 'id');
    });
	
	$('#filterconn').on('change', function() {
        datatable_2.search($(this).val().toLowerCase(), 'conn_name');
    });
	
	$('#filtersql').on('change', function() {
        datatable_2.search($(this).val().toLowerCase(), 'sql_text');
    });
	
	$('#filterdesc').on('change', function() {
        datatable_2.search($(this).val().toLowerCase(), 'api_desc');
    });
    // $('#m_form_status').on('change', function() {
      // datatable_2.search($(this).val().toLowerCase(), 'Status');
    // });

    // $('#m_form_type').on('change', function() {
      // datatable_2.search($(this).val().toLowerCase(), 'Type');
    // });

    // $('#m_form_status, #m_form_type').selectpicker();

  };

  
  return {
    // public functions
    init: function() {
      demo();
    },
  };
}();
jQuery(document).ready(function() {
  Datatable_listpage.init();
});