var timestart,CountupTimer,toSA,toSTC,xhrS,xhrVS,xhr,editor,timeout,xhrSTC;
var inputval = [];
var ul_navpills = $('#portlet1b > .m-portlet__body > .nav-pills');
var div_tabcontent = $('#portlet1b > .m-portlet__body > .tab-content');
var oTable = [];
var oTable1 = [];
var tabId = 0;
var offset = 1; 
var limit = 1000;
var is_cache = true;
var is_rowcount = false;

function getSTC(me){

	if(xhrSTC!=null && xhrSTC!=undefined){
		xhrSTC.abort();
	}

	var search = '';
	if($(me).val()!=undefined){
		search = $(me).val();
	}
	// console.log(search);

	xhrSTC = $.ajax( {
        "url": '/bigbox/bigquery/getSTC',
        "data": {
        	'search':search
        },
        "dataType": "json",
        "method": "POST",
        "beforeSend": function(){
            mApp.block('#portlet0 > .m-portlet__body', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'success',
                message: 'Please wait...'
            });
        },
        "complete": function(res){
            mApp.unblock('#portlet0 > .m-portlet__body');
        },
        "success": function(json){
        	// console.log(json);
        	if(json.transaction && json.data.length>0){
        		var html = '', display='', m__hide='';
        		$.each(json.data, function(k,v){

        			if(v.alias!=undefined){
        				display = v.alias;
        			} else {
        				display = v.database_name;
        			}

        			html +='<tr class="tr-schemas '+v.database_name+'">'+
								'<td>'+
									'<a class="btn toggle" data-database="'+v.database_name+'" onclick="toggleSchemas(this);">'+
										'<i class="la la-database"></i> '+display+
									'</a> '+
									'<span class="ctchild m--font-info">'+
										'('+
										'<span class="ttb">'+(v.tables!=undefined?v.tables.length:0)+'</span>'+
										')'+
									'<span>'+
								'</td>'+
							'</tr>';
					if(v.tables!=undefined){
						$.each(v.tables, function(k1,v1){

		        			if(v1.alias!=undefined){
		        				display = v1.alias;
		        			} else {
		        				display = v1.tab_name;
		        			}

		        			if(v1.show!=undefined && v1.show==true){
		        				m__hide = '';
		        			} else {
		        				m__hide = ' m--hide';
		        			}

							html +='<tr class="tr-tables '+v.database_name+' '+v1.tab_name+m__hide+'">'+
									 	'<td>'+
									 		'<i class="la la-table" data-toggle="modal" data-target="#modal-statistic" data-database_name="'+v.database_name+'" data-table_name="'+v1.tab_name+'" onclick="viewStatistics(this);"></i> '+
											'<a class="btn toggle" data-database="'+v.database_name+'" data-table="'+v1.tab_name+'" onclick="toggleTables(this);">'+
												display+
											'</a>'+
											'<span class="ctchild m--font-warning">'+
												'('+
												'<span class="ttb">'+(v1.columns!=undefined?v1.columns.length:0)+'</span>'+
												')'+
											'<span>'+
								 			'<button type="button" class="btn btn-primary btn-xs m-btn m-btn--icon m-btn--icon-only m-btn--pill pull-right" data-toggle="modal" data-target="#modal-statistic" data-database_name="'+v.database_name+'" data-table_name="'+v1.tab_name+'" onclick="viewStatistics(this);">'+
								 				'<i class="la la-bar-chart"></i>'+
								 			'</button>'+
										'</td>'+
									'</tr>';
							if(v1.columns!=undefined){
								$.each(v1.columns, function(k2,v2){

				        			if(v2.alias!=undefined){
				        				display = v2.alias;
				        			} else {
				        				display = v2.col_name;
				        			}

				        			if(v2.show!=undefined && v2.show==true){
				        				m__hide = '';
				        			} else {
				        				m__hide = ' m--hide';
				        			}

									html +='<tr class="tr-columns '+v.database_name+' '+v1.tab_name+' '+v2.col_name+m__hide+'">'+
												'<td>'+
													'<i class="la la-columns"></i> '+display+' <span class="ctchild">('+v2.data_type+')</span>'+
												'</td>'+
											'</tr>';
								});
							}
						});

					}
        		});

				$('#portlet0 > .m-portlet__body > .m-scrollable .table > tbody').html(html);
        	} else {
        		$('#portlet0 > .m-portlet__body > .m-scrollable .table > tbody').html('<tr><td>No Data available.</td></tr>');
        	}
        },
    });

    return false;
}

function toggleSchemas(me){
	// if($('#searchtb').val().trim()==''){
    	var database = $(me).data('database');
    	$('body').find('.tr-tables.'+database).toggleClass('m--hide');

    	var close = $('.tr-tables.'+database).hasClass('m--hide');
    	if(close){
    		$('.tr-columns.'+database).addClass('m--hide');
    	}
	// }
}

function toggleTables(me){
	// if($('#searchtb').val()==''){
		// console.log($(this).data('database'));
		var database = $(me).data('database');
		var table = $(me).data('table');
		// console.log(database);
		$('.tr-columns.'+database+'.'+table).toggleClass('m--hide');
	// }
}

function exetime(){
	CountupTimer = setTimeout(function() {
		var now = new Date().getTime();
		var distance = now - timestart;
		var seconds = Math.floor((distance % (1000 * 60)) / 1000)+'s';
		$("#counter").html(seconds);
		exetime();
	},1000);
}

function stopTimer(){
	clearTimeout(CountupTimer);
}

function columnTogler(me){

	var tab_id = parseInt($(me).data('tab_id')); //console.log(tab_id);
	var iCol = parseInt($(me).data('column')); //console.log(iCol); console.log(oTable); console.log(oTable[tab_id]);
    var bVis = oTable[tab_id].fnSettings().aoColumns[iCol].bVisible;

    oTable[tab_id].fnSetColumnVis(iCol, (bVis ? false : true));
}

function searchall(me){

	if(toSA!=null && toSA!=undefined){
		clearTimeout(toSA);
	}

	toSA = setTimeout(function(){
		var tab_id = $(me).data('tab_id'); //console.log(tab_id);
		var val = $(me).val(); //console.log(val);
		oTable[tab_id].fnFilter(val);
		clearTimeout(toSA);
	}, 300);
}

function headfilter(me) {

	if(toSA!=null && toSA!=undefined){
		clearTimeout(toSA);
	}

	toSA = setTimeout(function(){
		var tab_id = $(me).data('tab_id'); //console.log(tab_id);
		var col = $(me).data('col'); //console.log(col);
		var val = $(me).val(); //console.log(val);

		var tArr = [];
		tArr[tab_id] = [];

		if(tArr[tab_id][col]==undefined){
			tArr[tab_id][col] = null;
		} else {
			tArr[tab_id][col] = val;
		}

		if(inputval[tab_id]==undefined){
			inputval[tab_id] = [];
		} else {
			inputval[tab_id].push(tArr);
		}
		
		inputval.push(tArr);

		if(inputval[tab_id][col] != val){
			inputval[tab_id][col] = val;
		}

		oTable[tab_id].fnFilter( val, col);

		clearTimeout(toSA);
	}, 500);
}

function viewStatistics1(me){

	var clear_cache = 1;
	if($(me).data('toggle')!=undefined){
		$('.btn-reload-cache').data('database',$(me).data('database'));
		$('.btn-reload-cache').data('table',$(me).data('table'));
		clear_cache = 0;
	}
	var database = $(me).data('database');
	var table = $(me).data('table');

	$('#modal-statistic').find('.modal-title span').html(database+'.'+table);

	if(xhrS!=null && xhrS!=undefined){
		xhrS.abort();
	}

	xhrS = $.ajax({
		url:"/bigbox/bigquery/editor/ajax/1",
		content:'json',
		type:'POST',
		data: {'database_name':database,'table_name':table,'clear_cache':clear_cache},
		beforeSend:function(){
            mApp.block('#modal-statistic > .modal-dialog > .modal-content > .modal-body > .tab-content', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'success',
                message: 'Please wait...'
            });
			$('#modal-statistic ul.nav.nav-tabs li a').removeClass('active');
			$('#modal-statistic ul.nav.nav-tabs li a:first').addClass('active');
			$('#modal-statistic div.tab-content .tab-pane').removeClass('active show');
			$('#modal-statistic div.tab-content .tab-pane:first').addClass('active show');

			$('.samples_content').addClass('m--hide');
			$('.columns_content').addClass('m--hide');
			$('.spin-loader-modal').addClass('m--hide');
			$('.properties_content').removeClass('m--hide');
		},
		complete:function(result){
			$('.samples_content').removeClass('m--hide');
			$('.properties_content').removeClass('m--hide');
			$('.columns_content').removeClass('m--hide');
			$('.spin-loader-modal').addClass('m--hide');
			mApp.unblock('#modal-statistic > .modal-dialog > .modal-content > .modal-body > .tab-content');
		},
		error: function(e){
			alert('error');
		},
		success: function(data){
			// console.log(data);
			var properties_content='';
			if(data[231].transaction){
				if(data[231].data.length==0){
					properties_content +='<div class="alert alert-warning">The selected table has no data.</div>';
				} else {
					properties_content +=''+
					'<table class="table table-bordered m-table m-table--head-bg-brand">'+
						'<thead>'+
							'<tr>';
							$.each(data[231].data[0], function(field,value){
								properties_content +='<th>'+field+'</th>';
							});
					properties_content +='</tr>'+
						'</thead>'
						'<tbody>';
					$.each(data[231].data, function(k,v){
						properties_content +='<tr>';
						$.each(data[231].data[0], function(field,value){
							properties_content +='<td>'+v[field]+'</td>';
						});
						properties_content +='</tr>';
					});
					properties_content +='</tbody>'+
					'</table>';
				}
			}

			$('.properties_content .table-scrollable').html(properties_content);
			$('.properties_content .alert span').html(data[231].cache.time);

			var columns_content='';
			if(data[230].transaction){
				if(data[230].data.length==0){
					columns_content +='<div class="alert alert-warning">The selected table has no data.</div>';
				} else {
					columns_content +=''+
					'<table class="table table-bordered m-table m-table--head-bg-brand">'+
						'<thead>'+
							'<tr>';
							$.each(data[230].data[0], function(field,value){
								columns_content +='<th>'+field+'</th>';
							});
					columns_content +='</tr>'+
						'</thead>'
						'<tbody>';
					$.each(data[230].data, function(k,v){
						columns_content +='<tr>';
						$.each(data[230].data[0], function(field,value){
							columns_content +='<td>'+v[field]+'</td>';
						});
						columns_content +='</tr>';
					});
					columns_content +='</tbody>'+
					'</table>';
				}
			}

			$('.columns_content .table-scrollable').html(columns_content);
			$('.columns_content .alert span').html(data[230].cache.time);

			var samples_content='';
			if(data[232].transaction){
				if(data[232].data.length==0){
					samples_content +='<div class="alert alert-warning">The selected table has no data.</div>';
				} else {
					samples_content +=''+
					'<div class="table-responsive">'+
						'<table class="table table-bordered m-table m-table--head-bg-brand">'+
							'<thead>'+
								'<tr>';
								$.each(data[232].data[0], function(field,value){
									samples_content +='<th>'+field+'</th>';
								});
						samples_content +='</tr>'+
							'</thead>'
							'<tbody>';
						$.each(data[232].data, function(k,v){
							samples_content +='<tr>';
							$.each(data[232].data[0], function(field,value){
								samples_content +='<td>'+v[field]+'</td>';
							});
							samples_content +='</tr>';
						});
						samples_content +='</tbody>'+
						'</div>'+
					'</table>';
				}
			} else {
				samples_content +='<p>'+
									+'<span class="m-badge m-badge--danger m-badge--wide m-badge--rounded">ERROR!</span>'+
									+'<span> '+data[232].message+'</span>'+
								+'</p>';
			}

			$('.samples_content .table-scrollable').html(samples_content);
			$('.samples_content .alert span:first').html(data[233].data);
			$('.samples_content .alert span:last').html(data[232].cache.time);
		},
		error:function(){
		}
	});

	return false;
}
function viewStatistics(me){

	if(xhrS!=null && xhrS!=undefined){
		xhrS.abort();
	}

	var clear_cache = 0;
	var tab_name = 'columns';
	if($(me).hasClass('btn-reload-cache')){
		clear_cache = 1;
		tab_name = $.trim($('#modal-statistic').find('li > a.active.show').attr('href')).substr(5);
	} else if($(me).hasClass('nav-link')){
		tab_name = $.trim($(me).attr('href')).substr(5);
	} else {
		$('.columns_content > .alert > .pull-right > span').html('yyyy-mm-dd hh:mi:ss');
		$('.columns_content > .table-scrollable').html('');
		$('.properties_content > .table-scrollable').html('');
		$('.properties_content > .alert > .pull-right > span').html('yyyy-mm-dd hh:mi:ss');
		$('.samples_content > .table-scrollable').html('');
		$('.samples_content > .alert > .pull-right > span').html('yyyy-mm-dd hh:mi:ss');
			
		$('#modal-statistic ul.nav.nav-tabs li a').removeClass('active');
		$('#modal-statistic ul.nav.nav-tabs li a:first').addClass('active');

		$('#modal-statistic div.tab-content .tab-pane').removeClass('active show');
		$('#modal-statistic div.tab-content .tab-pane:first').addClass('active show');

		$('#database_name').val($(me).data('database_name'));
		$('#table_name').val($(me).data('table_name'));
	}

	if($('.'+tab_name+'_content > .table-scrollable').html()!='' && clear_cache==false){
		return false;
	}

	var database_name = $('#database_name').val(); //console.log(database_name);
	var table_name = $('#table_name').val(); //console.log(table_name);

	xhrS = $.ajax({
		url:"/bigbox/bigquery/editor/ajax/1",
		content:'json',
		type:'POST',
		data: {
			'clear_cache':clear_cache,
			'tab_name':tab_name,
			'database_name':database_name,
			'table_name':table_name,
		},
		beforeSend:function(){
            mApp.block('#modal-statistic > .modal-dialog > .modal-content > .modal-body > .tab-content > .tab-pane#tab_'+tab_name+' > .'+tab_name+'_content', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'success',
                message: 'Please wait...'
            });
		},
		complete:function(result){
			mApp.unblock('#modal-statistic > .modal-dialog > .modal-content > .modal-body > .tab-content > .tab-pane#tab_'+tab_name+' > .'+tab_name+'_content');
		},
		error: function(e){
			alert('error');
		},
		success: function(data){
			// console.log(data);

			if(tab_name=='columns'){
				var columns_content='';
				if(data[230].transaction){
					if(data[230].data.length==0){
						columns_content +='<div class="alert alert-warning">The selected table has no data.</div>';
					} else {
						columns_content +=''+
						'<table class="table table-bordered m-table m-table--head-bg-brand">'+
							'<thead>'+
								'<tr>';
								$.each(data[230].data[0], function(field,value){
									columns_content +='<th>'+field+'</th>';
								});
						columns_content +='</tr>'+
							'</thead>'
							'<tbody>';
						$.each(data[230].data, function(k,v){
							columns_content +='<tr>';
							$.each(data[230].data[0], function(field,value){
								columns_content +='<td>'+v[field]+'</td>';
							});
							columns_content +='</tr>';
						});
						columns_content +='</tbody>'+
						'</table>';
					}
				}

				$('.columns_content .table-scrollable').html(columns_content);
				$('.columns_content .alert span').html(data[230].cache.time);
			} else if(tab_name=='properties'){
				var properties_content='';
				if(data[231].transaction){
					if(data[231].data.length==0){
						properties_content +='<div class="alert alert-warning">The selected table has no data.</div>';
					} else {
						properties_content +=''+
						'<table class="table table-bordered m-table m-table--head-bg-brand">'+
							'<thead>'+
								'<tr>';
								$.each(data[231].data[0], function(field,value){
									properties_content +='<th>'+field+'</th>';
								});
						properties_content +='</tr>'+
							'</thead>'
							'<tbody>';
						$.each(data[231].data, function(k,v){
							properties_content +='<tr>';
							$.each(data[231].data[0], function(field,value){
								properties_content +='<td>'+v[field]+'</td>';
							});
							properties_content +='</tr>';
						});
						properties_content +='</tbody>'+
						'</table>';
					}
				}

				$('.properties_content .table-scrollable').html(properties_content);
				$('.properties_content .alert span').html(data[231].cache.time);
			} else if(tab_name=='samples'){
				var samples_content='';
				if(data[232].transaction){
					if(data[232].data.length==0){
						samples_content +='<div class="alert alert-warning">The selected table has no data.</div>';
					} else {
						samples_content +=''+
						'<div class="table-responsive">'+
							'<table class="table table-bordered m-table m-table--head-bg-brand">'+
								'<thead>'+
									'<tr>';
									$.each(data[232].data[0], function(field,value){
										samples_content +='<th>'+field+'</th>';
									});
							samples_content +='</tr>'+
								'</thead>'
								'<tbody>';
							$.each(data[232].data, function(k,v){
								samples_content +='<tr>';
								$.each(data[232].data[0], function(field,value){
									samples_content +='<td>'+v[field]+'</td>';
								});
								samples_content +='</tr>';
							});
							samples_content +='</tbody>'+
							'</div>'+
						'</table>';
					}
				} else {
					samples_content +='<p>'+
										+'<span class="m-badge m-badge--danger m-badge--wide m-badge--rounded">ERROR!</span>'+
										+'<span> '+data[232].message+'</span>'+
									+'</p>';
				}

				$('.samples_content .table-scrollable').html(samples_content);
				$('.samples_content .alert span:first').html(data[233].data);
				$('.samples_content .alert span:last').html(data[232].cache.time);
			}


		},
		error:function(){
		}
	});

	return false;
}

function viewSql(me) {
	var tab_id = $(me).data('tab_id');
	var query = $('#tab_results'+tab_id).find('.qry').val();

	var mtitle = $('#portlet1b .nav.nav-pills li.tabresults.active a').html();

	$('#modal-sql .modal-dialog .modal-content .modal-header h4.modal-title').html(mtitle);
	$('#modal-sql .modal-dialog .modal-content .modal-body div.well').html(query);
}

function exportAPI(tab_id){
	// console.log(qry);


	var qryx = $('#tab_results'+tab_id).find('.qry').val();

	$.ajax({
		url:"/prabajax/addapi",
		content:'json',
		type:'POST',
		data:{
			id :"",
			pid : "",
			conname : 14,
			sqltext : qryx,
			apidesc : "Query from bique",
			apitype : 1,
			str : "",
			attrs1 : "",
			cache : 3600,
			mode : 2,
			backendcache : "file",
			inputsqltext : "",
			outputsqltext : "",
			arrayout : ""
		},
		beforeSend:function(){
            mApp.block('body', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'success',
                message: 'Please wait...'
            });

			$('.spin-loader-modal').removeClass('m--hide');
			$('#modal-exportapi').find('.modal-title span').html("");
			$('#modal-exportapi').find('.modal-body .well').html("");
		},
		complete:function(result){
			mApp.unblock('body');
			$('.spin-loader-modal').addClass('m--hide');
		},
		success: function(data){
			console.log(data);
			if(data.result!=undefined){
				if(data.result==true){
					$('#modal-exportapi').find('.modal-title span').html("Success to export as BigEnvelope");
					$('#modal-exportapi').find('.modal-body .well').html("BigEnvelope API ID : "+data.api_id);
					// alert("Success to export as API (API ID "+data.api_id+"): "+data.retMsg);
				}else{
					$('#modal-exportapi').find('.modal-title span').html("Failed to export as BigEnvelope");
					// alert("Failed to export as API : "+data.retMsg);
				}
			}else{
				$('#modal-exportapi').find('.modal-title span').html("Failed to export as BigEnvelope");
				// alert("Failed to export as API");
			}
			// $('#modal-exportapi').show();
		},
		error:function(){
			$('#modal-exportapi').find('.modal-title span').html("Failed to export as BigEnvelope");
			// $('#modal-exportapi').show();
			// alert("Failed to export as API");
		}
	});
}
function execute() {

	if(xhr!=null && xhr!=undefined){
		xhr.abort();
	}

	var qry = (editor1.getSelection()!=''?editor1.getSelection():editor1.getValue());
	var timelog = new Date();

	xhr = $.ajax( {
        "url": '/bigbox/bigquery/editor/ajax/2',
        "data": {
        	'qry':qry,
        	'is_cache':is_cache,
        	'limit':limit,
        	'offset':offset,
        	'tabId':tabId
        },
        "dataType": "json",
        "method": "POST",
        "beforeSend": function(){
    		ul_navpills.find('li.nav-item > a.nav-link').removeClass('active show');
    		div_tabcontent.find('div.tab-pane').removeClass('active show');

    		ul_navpills.find('li.nav-item:last > a.nav-link').addClass('active show');
    		div_tabcontent.find('div.tab-pane:last').addClass('active show');

			timestart = new Date().getTime();
			exetime();
			setTimeout(function(){
				$('.btn-execute').addClass('m--hide');
				$('.btn-execute-stop').removeClass('m--hide');
	            mApp.block('#tab_results'+tabId, {
	                overlayColor: '#000000',
	                type: 'loader',
	                state: 'success',
	                message: 'Please wait...'
	            });
			}, 100);
        },
        "complete": function(res){
			var now = new Date().getTime();
        	stopTimer();
    		var dist = now - timestart;
		    var hours = Math.floor((dist % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		    var minutes = Math.floor((dist % (1000 * 60 * 60)) / (1000 * 60));
		    var seconds = Math.floor((dist % (1000 * 60)) / 1000);

		    var counter = '';
		    if(hours>0){
		    	counter += hours+'h ';
		    }
		    if(hours>0){
		    	counter += minutes+'m ';
		    }

		    counter += seconds+'.'+((dist/1000).toString().split('.')[1])+'s';
		    $('#counter').html(counter);

        	$('.btn-execute-stop').addClass('m--hide');
        	$('.btn-execute').removeClass('m--hide');
        },
        "success": function(json) {

        	var divlog = $('#portlet1b .tab-content #tab_logs #divlog');
        	if(divlog.find('.table').length==0){
        		divlog.html(
        			'<table class="table table-bordered table-striped">'+
        				'<thead>'+
        					'<tr>'+
        						'<th style="width:125px;"></th>'+
        						'<th style="width:500px;">Query</th>'+
        						'<th>Offset</th>'+
        						'<th>Time</th>'+
        						'<th>Status</th>'+
        						'<th>Message</th>'+
        					'</tr>'+
        				'</thead>'+
        				'<tbody>'+
        				'</tbody>'+
        			'</table>'
    			);
        	}

    		var tbody = divlog.find('.table tbody');
    		var trfirst = divlog.find('.table tbody tr:first');
    		if(trfirst.length==0){
    			tbody.append(
    				'<tr>'+
    					'<td>'+timelog.toLocaleString()+'</td>'+
    					'<td>'+qry+'</td>'+
    					'<td>'+offset+'</td>'+
    					'<td>'+json.duration+'</td>'+
    					'<td>'+(json.transaction?'<span class="m--font-success">SUCCESS</span>':'<span class="m--font-danger">FAIL</span>')+'</td>'+
    					'<td>'+json.message+'</td>'+
    				'</tr>'
    			);
    		} else {
    			trfirst.before(
    				'<tr>'+
    					'<td>'+timelog.toLocaleString()+'</td>'+
    					'<td>'+qry+'</td>'+
    					'<td>'+offset+'</td>'+
    					'<td>'+json.duration+'</td>'+
    					'<td>'+(json.transaction?'<span class="m--font-success">SUCCESS</span>':'<span class="m--font-danger">FAIL</span>')+'</td>'+
    					'<td>'+json.message+'</td>'+
    				'</tr>'
    			);
    		}

        	if(json.transaction){

        		var tableHeaders;
                var tableHeaderInputs;
                var listColumns;
                $.each(json.parseData.aoColumns, function(i, val){
                    tableHeaders += "<th>" + val + "</th>";
                    tableHeaderInputs += '<th><input type="text" class="headfilter" data-col="'+i+'" data-tab_id="'+json.tabId+'" onkeyup="headfilter(this);" onblur="headfilter(this);" onfocus="headfilter(this);"></th>';
                    listColumns += '<label class="m-checkbox m-checkbox--solid"><input type="checkbox" checked data-column="'+i+'" data-tab_id="'+json.tabId+'" onchange="columnTogler(this);"> '+val+'<span></span></label>';
                });

        		var html='';
        		html += '<textarea class="m--hide qry">'+qry+'</textarea>';
				html +=
				'<div class="row">'+
					'<div class="col-md-5">'+
						'<div class="totalOfRecords m--hide">'+
							'<span class="rowcount"></span> total records'+
						'</div>'+
					'</div>'+
					'<div class="col-md-7">'+
						'<div class="btn-group pull-right">'+
							'<input class="form-control searchall" placeholder="Search all column" data-tab_id="'+json.tabId+'" onkeyup="searchall(this)">'+
							'<a class="btn btn-success" href="#modal-exportapi" data-toggle="modal" title="export to BigEnvelope API" onclick=\'exportAPI('+json.tabId+');\'><i class="fa fa-fire"></i></a>'+
							'<a class="btn btn-info" href="#modal-sql" title="view sql" data-toggle="modal" data-tab_id="'+json.tabId+'" onclick="viewSql(this);"><i class="fa fa-file-code-o"></i></a>'+
							'<button class="btn default" title="show/hide column" data-toggle="dropdown">'+
							'<i class="fa fa-columns"></i> <i class="fa fa-angle-down"></i>'+
							'</button>'+
							'<div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="200" style="overflow:hidden;">'+
								'<div class="dropdown-menu hold-on-click dropdown-checkboxes" id="samples_2_column_toggler'+json.tabId+'" style="padding:10px;">'+
									'<div class="m-checkbox-list">'+
									'</div>'+
								'</div>'+
							'</div>'+
							'<button type="button" class="btn btn-default btn-prev" disabled="disabled" data-tab_id="'+json.tabId+'" data-page="0" onclick="getPageData(this);">&lt;</button>'+
							'<button type="button" class="btn btn-default btn-next" data-tab_id="'+json.tabId+'" data-page="2" onclick="getPageData(this);">&gt;</button>'+			
						'</div>'+
					'</div>'+
				'</div>'+
				'<table class="table table-striped table-bordered" id="datatables'+json.tabId+'">'+
					'<thead>'+
						'<tr>'+tableHeaderInputs+'</tr>'+
						'<tr>'+tableHeaders+'</tr>'+
					'</thead>'+
				'</table>'+
				'';

        		$('#tab_results'+json.tabId).html(html);
                $('#tab_results'+json.tabId+' #samples_2_column_toggler'+json.tabId+' .m-checkbox-list').html(listColumns);

        		var dtTable = $('#datatables'+json.tabId).dataTable({
		            data: 			json.parseData.aaData,
		            deferRender: 	true,
		            scrollY: 		350,
		            scrollX: 		'100%',
		            scrollCollapse: true,
		            scroller: 		true
                });

                if(oTable[json.tabId]==undefined){
					oTable.push(dtTable);
                } else {
                	oTable[json.tabId] = dtTable;
                }

                // console.log($('#tab_results'+json.tabId).prop('childNodes'));

                var str_undefined = $('#datatables'+json.tabId+'_wrapper').parent().prop('childNodes')[2];
                if(str_undefined.textContent.indexOf('undefined')>-1){
                	str_undefined.remove();
                }
                str_undefined = $('#samples_2_column_toggler'+json.tabId+' .m-checkbox-list').prop('childNodes')[0];
                if(str_undefined.textContent.indexOf('undefined')>-1){
                	str_undefined.remove();
                }

				if(is_rowcount){				

					xhr = $.ajax( {
				        "url": '/bigbox/bigquery/editor/ajax/3',
				        "data": {
				        	'qry':qry,
				        },
				        "dataType": "json",
				        "method": "POST",
				        "beforeSend": function(){
				        	$('#tab_results'+json.tabId).find('.totalOfRecords').removeClass('m--hide').find('.rowcount').html(' <i class="la la-spinner la-spin"></i>');
				        },
				        "complete": function(res){
				        },
				        "success": function(data){
				        	$('#tab_results'+json.tabId).find('.totalOfRecords > .rowcount').html(data.data);
				        },
				    });

				} else {
					$('#datatables'+json.tabId+'_wrapper .row:last').addClass('m--hide');
				}
        	} else {
        		$('#tab_results'+json.tabId).html('<p style="padding:10px;"><span class="m-badge m-badge--danger m-badge--wide m-badge--rounded">ERROR!</span> '+json.message+'</p>');
        	}
        },
    });

	return false;
}

function getPageData(me){

	if($(me).attr('disabled')!=undefined){
		return false;
	} else if(xhr!=null && xhr!=undefined){
		xhr.abort();
	}

	var page = parseInt($(me).data('page')); //console.log(page);
	var max = (page*1000);
	var offset = max-1000+1;
	var tab_id = parseInt($(me).data('tab_id'));
	var query = $('#tab_results'+tab_id).find('.qry').val();
	var timelog = new Date();

	xhr = $.ajax( {
        "url": '/bigbox/bigquery/editor/ajax/2',
        "data": {
        	'qry':query,
        	'is_cache':is_cache,
        	'limit':1000,
        	'offset':offset,
        	'tabId':tab_id
        },
        "dataType": "json",
        "method": "POST",
        "beforeSend": function(){
            mApp.block('#tab_results'+tab_id, {
                overlayColor: '#000000',
                type: 'loader',
                state: 'success',
                message: 'Please wait...'
            });

        },
        "complete": function(res){
            mApp.unblock('#tab_results'+tab_id);
        },
        "success": function(json){
        	if(json.transaction){
        		oTable[tab_id].fnClearTable();
        		oTable[tab_id].fnAddData(json.parseData.aaData);

        		if($(me).hasClass('btn-prev')){
        			$(me).data('page',(page-1));
        			$(me).parent().find('.btn-next').data('page',(page+1));
        		} else if($(me).hasClass('btn-next')){
        			$(me).data('page',(page+1));
        			$(me).parent().find('.btn-prev').data('page',(page-1));
        		}

        		// console.log(page);

        		$(me).parent().find('.btn-page-info span:first').html(offset);
        		$(me).parent().find('.btn-page-info span:last').html(max);

        		if(page>1){
        			$(me).parent().find('.btn-prev').removeAttr('disabled');
        		} else {
        			$(me).parent().find('.btn-prev').attr('disabled','disabled');
        		}

        		var totalOfRecords = $('#tab_results'+json.tabId).find('.totalOfRecords > .rowcount').html();
        		totalOfRecords = parseInt(totalOfRecords.replace(/\,/g, ''));

        		if(max > totalOfRecords){
        			$(me).next().find('span:last').html(totalOfRecords);
        			$(me).parent().find('.btn-next').attr('disabled','disabled');
        		} else {
        			$(me).parent().find('.btn-next').removeAttr('disabled');
        		}
        	}

        	var divlog = $('#portlet1b .tab-content #tab_logs #divlog');
    		var trfirst = divlog.find('.table tbody tr:first');
			trfirst.before(
				'<tr>'+
					'<td>'+timelog.toLocaleString()+'</td>'+
					'<td>'+query+'</td>'+
					'<td>'+offset+'</td>'+
					'<td>'+json.duration+'</td>'+
					'<td>'+(json.transaction?'<span class="m--font-success">SUCCESS</span>':'<span class="m--font-danger">FAIL</span>')+'</td>'+
					'<td>'+json.message+'</td>'+
				'</tr>'
			);
        },
    });

	return false;
}
jQuery(document).ready(function(){
	getSTC(false);
	var portlet1 = $('#portlet1').mPortlet();
	portlet1.on('afterFullscreenOn',function(portlet1){
		$('#portlet1').find('#portlet1a').parent().removeClass('col-md-12').addClass('col-md-4');
		$('#portlet1').find('#portlet1b').parent().removeClass('col-md-12').addClass('col-md-8');
		var h = $('#portlet1').find('.m-portlet__body').height();
		$('.CodeMirror').css('height',(h-35));
		$('#portlet1b').find('.tab-content > .tab-pane').css('height',(h-100));
	});

    portlet1.on('afterFullscreenOff', function(portlet1) {
		$('#portlet1').find('#portlet1a').parent().removeClass('col-md-4').addClass('col-md-12');
		$('#portlet1').find('#portlet1b').parent().removeClass('col-md-8').addClass('col-md-12');
		$('.CodeMirror').css('height',160);
		$('#portlet1b').find('.tab-content > .tab-pane').css('height',480);
    });

    var ctdbd = [];
    $.each($('.tr-schemas'),function(k,v){
    	var x = $(v).find('a').data('database');
    	var y = parseInt($(v).find('.ttb').text());
    	ctdbd[x] = y;
    });

    $('#searchtb').on('keyup',function(){

		if(toSTC!=null && toSTC!=undefined){
			clearTimeout(toSTC);
		}

    	var me = this;

    	toSTC = setTimeout(function() {
    		getSTC(me);
    	}, 500);

    	return false;
    });

    editor1 = CodeMirror.fromTextArea(document.getElementById("sql"), {
		lineNumbers: true,
		mode: 'text/x-hive',
		matchBrackets: true,
		styleActiveLine: true,
		lineNumbers: true,
		lineWrapping: true,
		extraKeys: {
			"Ctrl-Space": "autocomplete", // To invoke the auto complete
			"Ctrl-Enter": function(){
				$('.btn-execute').click();
			},
			"Shift-Ctrl-Enter": function(){

				tabId++;


				var lia = '<li class="nav-item tabresults tabextend">'+
								'<a class="btn btn-outline-info nav-link" data-toggle="tab" href="#tab_results'+tabId+'">'+
									'<i class="la la-bars"></i>'+
									'Result ('+(tabId+1)+')'+
								'</a>'+
							'</li>';
				var tctp = '<div class="tab-pane tabextend" id="tab_results'+tabId+'" role="tabpanel" style="height: 528px;">'+
								'<div class="divtable">No Results available.</div>'+
							'</div>';

				ul_navpills.append(lia);
				div_tabcontent.append(tctp);

				execute();
			}
		}, 
        hint: CodeMirror.hint.sql,
		hintOptions : hintOptions,
    });

    editor1.on("keyup", function(cm, event) {

        var kcode = event.keyCode;
	    if(
	    	(
	    		(kcode==16) ||
	    		(kcode==190) ||
	    		(kcode>=48 && kcode<=57) ||
	    		(kcode>=65 && kcode<=90)
	    	)
	    	&& !editor1.state.completionActive)
	    {
	        if(timeout) clearTimeout(timeout);
	        timeout = setTimeout(function() {
	            CodeMirror.showHint(cm, CodeMirror.hint.sql, {completeSingle: false});
	        }, 150);
	    }
    });

    $('.btn-is_cache, .btn-is_rowcount').on('click', function(){

    	if($(this).hasClass('btn-is_cache')){
    	$(this).toggleClass('btn-light btn-info');
    		is_cache = $(this).hasClass('btn-info');
    	} else if($(this).hasClass('btn-is_rowcount')){
    		$(this).toggleClass('btn-light btn-warning');
    		is_rowcount = $(this).hasClass('btn-warning');
    	}
    });

    $('.btn-execute').on('click', function(){

		oTable = [];
		oTable1 = [];
		tabId = 0;

		ul_navpills.find('li.tabextend').remove();
		div_tabcontent.find('div.tab-pane.tabextend').remove();

		execute();
    });

    $('.btn-execute-stop').on('click',function(){
    	stopTimer();
		if(xhr!=undefined){
			xhr.abort();
		}

		$('.btn-execute').removeClass('m--hide');
		$(this).addClass('m--hide');

		// mApp.unblock(jQuery('#tab_results'));
		$('.spin-loader').addClass('m--hide');
    });

    $('.modal-header button.close').on('click',function(){
    	if(xhrS!=undefined){
			xhrS.abort();
		}
    });
});