var timestart;
var CountupTimer;
function exetime(){
	CountupTimer = setTimeout(function() {
		var now = new Date().getTime();
		var distance = now - timestart;
		var seconds = Math.floor((distance % (1000 * 60)) / 1000)+'s';
		$("#counter").html(seconds);
		exetime();
	},1000);
}

function stopTimer(){
	clearTimeout(CountupTimer);
}

function columnTogler(me){

	var tab_id = parseInt($(me).data('tab_id')); //console.log(tab_id);
	var iCol = parseInt($(me).data('column')); //console.log(iCol); console.log(oTable); console.log(oTable[tab_id]);
    var bVis = oTable[tab_id].fnSettings().aoColumns[iCol].bVisible;

    oTable[tab_id].fnSetColumnVis(iCol, (bVis ? false : true));
}

var toSA;
function searchall(me){

	if(toSA!=null && toSA!=undefined){
		clearTimeout(toSA);
	}

	toSA = setTimeout(function(){
		var tab_id = $(me).data('tab_id'); //console.log(tab_id);
		var val = $(me).val(); //console.log(val);
		oTable[tab_id].fnFilter(val);
		clearTimeout(toSA);
	}, 300);
}

var inputval = [];
function headfilter(me) {

	if(toSA!=null && toSA!=undefined){
		clearTimeout(toSA);
	}

	toSA = setTimeout(function(){
		var tab_id = $(me).data('tab_id'); //console.log(tab_id);
		var col = $(me).data('col'); //console.log(col);
		var val = $(me).val(); //console.log(val);

		var tArr = [];
		tArr[tab_id] = [];

		if(tArr[tab_id][col]==undefined){
			tArr[tab_id][col] = null;
		} else {
			tArr[tab_id][col] = val;
		}

		if(inputval[tab_id]==undefined){
			inputval[tab_id] = [];
		} else {
			inputval[tab_id].push(tArr);
		}
		
		inputval.push(tArr);

		if(inputval[tab_id][col] != val){
			inputval[tab_id][col] = val;
		}

		oTable[tab_id].fnFilter( val, col);

		clearTimeout(toSA);
	}, 500);
}

var xhrS;
function viewStatistics(me){

	var clear_cache = 1;
	if($(me).data('toggle')!=undefined){
		$('.btn-reload-cache').data('database',$(me).data('database'));
		$('.btn-reload-cache').data('table',$(me).data('table'));
		clear_cache = 0;
	}
	var database = $(me).data('database');
	var table = $(me).data('table');

	$('#modal-statistic').find('.modal-title span').html(database+'.'+table);

	if(xhrS!=null && xhrS!=undefined){
		xhrS.abort();
	}

	xhrS = $.ajax({
		url:"/bigquery/index/ajax/1",
		content:'json',
		type:'post',
		data: {'database_name':database,'table_name':table,'clear_cache':clear_cache},
		beforeSend:function(){

			// console.log($('#modal-statistic ul.nav.nav-tabs li'));

			$.each($('#modal-statistic ul.nav.nav-tabs li'), function(k,v){
				$(v).removeClass('active');
			});
			$('#modal-statistic ul.nav.nav-tabs li:first').addClass('active');
			$('#modal-statistic .tab-content #tab_sample').removeClass('active').removeClass('in');
			$('#modal-statistic .tab-content #tab_columns').removeClass('active').removeClass('in');
			$('#modal-statistic .tab-content #tab_analysis').removeClass('active').removeClass('in');
			$('#modal-statistic .tab-content #tab_analysis').addClass('active').addClass('in');


			$('.sample_content').addClass('hidden');
			$('.analysis_content').addClass('hidden');
			$('.columns_content').addClass('hidden');
			$('.spin-loader-modal').removeClass('hidden');
			// App.blockUI(jQuery('#modal-statistic .tab-content'));
		},
		complete:function(result){
			$('.sample_content').removeClass('hidden');
			$('.analysis_content').removeClass('hidden');
			$('.columns_content').removeClass('hidden');
			$('.spin-loader-modal').addClass('hidden');
			// App.unblockUI(jQuery('#modal-statistic .tab-content'));
		},
		success: function(data){
			// console.log(data);
			var analysis_content='';
			if(data[231].transaction){
				analysis_content +=''+
				'<table class="table table-striped table-bordered">'+
					'<thead>'+
						'<tr>';
						$.each(data[231].data[0], function(field,value){
							analysis_content +='<th>'+field+'</th>';
						});
				analysis_content +='</tr>'+
					'</thead>'
					'<tbody>';
				$.each(data[231].data, function(k,v){
					analysis_content +='<tr>';
					$.each(data[231].data[0], function(field,value){
						analysis_content +='<td>'+v[field]+'</td>';
					});
					analysis_content +='</tr>';
				});
				analysis_content +='</tbody>'+
				'</table>';
			}

			$('.analysis_content .table-scrollable').html(analysis_content);
			$('.analysis_content .alert span').html(data[231].last_cache);

			var columns_content='';
			if(data[230].transaction){
				columns_content +=''+
				'<table class="table table-striped table-bordered">'+
					'<thead>'+
						'<tr>';
						$.each(data[230].data[0], function(field,value){
							columns_content +='<th>'+field+'</th>';
						});
				columns_content +='</tr>'+
					'</thead>'
					'<tbody>';
				$.each(data[230].data, function(k,v){
					columns_content +='<tr>';
					$.each(data[230].data[0], function(field,value){
						columns_content +='<td>'+v[field]+'</td>';
					});
					columns_content +='</tr>';
				});
				columns_content +='</tbody>'+
				'</table>';
			}

			$('.columns_content .table-scrollable').html(columns_content);
			$('.columns_content .alert span').html(data[230].last_cache);

			var sample_content='';
			if(data[232].transaction){
				if(data[232].data.length==0){
					sample_content +='<div class="alert alert-warning">The selected table has no data.</div>';
				} else {
					sample_content +=''+
					'<table class="table table-striped table-bordered">'+
						'<thead>'+
							'<tr>';
							$.each(data[232].data[0], function(field,value){
								sample_content +='<th>'+field+'</th>';
							});
					sample_content +='</tr>'+
						'</thead>'
						'<tbody>';
					$.each(data[232].data, function(k,v){
						sample_content +='<tr>';
						$.each(data[232].data[0], function(field,value){
							sample_content +='<td>'+v[field]+'</td>';
						});
						sample_content +='</tr>';
					});
					sample_content +='</tbody>'+
					'</table>';
				}
			} else {
				sample_content +='<p>'+
									+'<span class="label label-danger">ERROR!</span>'+
									+'<span> '+data[232].message+'</span>'+
								+'</p>';
			}

			$('.sample_content .table-scrollable').html(sample_content);
			$('.sample_content .alert span:first').html(data[233].data);
			$('.sample_content .alert span:last').html(data[232].last_cache);
		},
		error:function(){
		}
	});

	return false;
}

var xhrVS;
function viewSql(me) {
	var tab_id = $(me).data('tab_id');
	var query = $('#tab_results'+tab_id).find('.qry').val();

	var mtitle = $('.tabbale-line .nav.nav-pills li.tabresults.active a').html();

	$('#modal-sql .modal-dialog .modal-content .modal-header h4.modal-title').html(mtitle);
	$('#modal-sql .modal-dialog .modal-content .modal-body div.well').html(query);


}

function exportAPI(tab_id){
	// console.log(qry);


	var qryx = $('#tab_results'+tab_id).find('.qry').val();

	$.ajax({
		url:"/prabajax/addapi",
		content:'json',
		type:'post',
		data:{
			id :"",
			pid : "",
			conname : 14,
			sqltext : qryx,
			apidesc : "Query from bique",
			apitype : 1,
			str : "",
			attrs1 : "",
			cache : 3600,
			mode : 2,
			backendcache : "file",
			inputsqltext : "",
			outputsqltext : "",
			arrayout : ""
		},
		beforeSend:function(){
			App.blockUI(jQuery('body'));
			$('.spin-loader-modal').removeClass('hidden');
			$('#modal-exportapi').find('.modal-title span').html("");
			$('#modal-exportapi').find('.modal-body .well').html("");
		},
		complete:function(result){
			App.unblockUI(jQuery('body'));
			$('.spin-loader-modal').addClass('hidden');
		},
		success: function(data){
			console.log(data);
			if(data.result!=undefined){
				if(data.result==true){
					$('#modal-exportapi').find('.modal-title span').html("Success to export as BigEnvelope");
					$('#modal-exportapi').find('.modal-body .well').html("BigEnvelope API ID : "+data.api_id);
					// alert("Success to export as API (API ID "+data.api_id+"): "+data.retMsg);
				}else{
					$('#modal-exportapi').find('.modal-title span').html("Failed to export as BigEnvelope");
					// alert("Failed to export as API : "+data.retMsg);
				}
			}else{
				$('#modal-exportapi').find('.modal-title span').html("Failed to export as BigEnvelope");
				// alert("Failed to export as API");
			}
			// $('#modal-exportapi').show();
		},
		error:function(){
			$('#modal-exportapi').find('.modal-title span').html("Failed to export as BigEnvelope");
			// $('#modal-exportapi').show();
			// alert("Failed to export as API");
		}
	});
}

var xhr;
var qry;
var pos = 1;
function execute() {

	timestart = new Date().getTime();
	exetime();
	if(xhr!=null && xhr!=undefined){
		xhr.abort();
	}

	var offset = start;

	var timelog = new Date();

	xhr = $.ajax( {
        "url": '/bigquery/index/ajax/2',
        "data": {
        	'qry':qry,
        	'is_cache':is_cache,
        	'limit':1000,
        	'offset':offset,
        	'tabId':tabId
        },
        "dataType": "json",
        "method": "POST",
        "beforeSend": function(){
			$('.btn-execute').removeClass('btn-primary').addClass('btn-warning');
			$('.btn-execute i').removeClass('fa-play').addClass('fa-spinner fa-spin');
			setTimeout(function(){
				$('.btn-execute').addClass('hidden');
				$('.btn-execute-stop').removeClass('hidden');
			}, 100);

			if(pos==1){
				$('#tab_results'+tabId).html('<i class="fa fa-spinner fa-spin spin-loader-result"></i>');	
			} else {
				$('#tab_results'+tabId+' .row:first').before('<i class="fa fa-spinner fa-spin spin-loader-result"></i>');	
			}
        },
        "complete": function(res){
			var now = new Date().getTime();
        	stopTimer();
    		var dist = now - timestart;
		    var hours = Math.floor((dist % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		    var minutes = Math.floor((dist % (1000 * 60 * 60)) / (1000 * 60));
		    var seconds = Math.floor((dist % (1000 * 60)) / 1000);

		    var counter = '';
		    if(hours>0){
		    	counter += hours+'h ';
		    }
		    if(hours>0){
		    	counter += minutes+'m ';
		    }

		    counter += seconds+'.'+((dist/1000).toString().split('.')[1])+'s';
		    $('#counter').html(counter);

        	$('.spin-loader-result').addClass('hidden');
        	$('.btn-execute-stop').addClass('hidden');
        	$('.btn-execute').removeClass('hidden btn-warning').addClass('btn-primary');
			$('.btn-execute i').removeClass('fa-spinner fa-spin').addClass('fa-play');
        },
        "success": function(json) {

        	if(json.transaction){

        		var tableHeaders;
                var tableHeaderInputs;
                var listColumns;
                $.each(json.parseData.aoColumns, function(i, val){
                    tableHeaders += "<th>" + val + "</th>";
                    tableHeaderInputs += '<th><input type="text" class="form-control input-sm input-medium headfilter" data-col="'+i+'" data-tab_id="'+json.tabId+'" onkeyup="headfilter(this);" onblur="headfilter(this);" onfocus="headfilter(this);"></th>';
                    listColumns += '<label><input type="checkbox" checked data-column="'+i+'" data-tab_id="'+json.tabId+'" onchange="columnTogler(this);"> '+val+'</label>';
                });

        		var html='';
        		html += '<textarea class="hidden qry">'+qry+'</textarea>';
				html +=
				'<div class="row">'+
					'<div class="col-md-12">'+
						'<div class="portlet box grey-gallery">'+
							'<div class="portlet-title" style="padding:0px;">'+
								'<div class="caption hidden">'+
									'<span class="rowcount"></span> total records'+
								'</div>'+
								'<div class="tools">'+
									'<input class="form-control input-medium searchall" placeholder="Search all column" data-tab_id="'+json.tabId+'" onkeyup="searchall(this)">'+
								'</div>'+
								'<div class="actions">'+
									'<div class="btn-group">'+
										'<button class="btn default" title="show/hide column" data-toggle="dropdown">'+
										'<i class="fa fa-columns"></i> <i class="fa fa-angle-down"></i>'+
										'</button>'+
										'<div class="dropdown-menu hold-on-click dropdown-checkboxes" id="sample_2_column_toggler'+json.tabId+'">'+
										'</div>'+
									'</div>'+
									'<a class="btn btn-success" href="#modal-exportapi" data-toggle="modal" title="export to BigEnvelope API" onclick=\'exportAPI('+json.tabId+');\'><i class="fa fa-fire"></i></a>'+
									'<a class="btn btn-info" href="#modal-sql" title="view sql" data-toggle="modal" data-tab_id="'+json.tabId+'" onclick="viewSql(this);"><i class="fa fa-file-code-o"></i></a>'+
								'</div>'+
							'</div>'+
							'<div class="portlet-body"style="padding: 0px 5px 5px;">'+
								'<table class="table table-striped table-bordered" id="datatables'+json.tabId+'">'+
									'<thead>'+
										'<tr>'+tableHeaderInputs+'</tr>'+
										'<tr>'+tableHeaders+'</tr>'+
									'</thead>'+
								'</table>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>'+
				'';

        		$('#tab_results'+json.tabId).html(html);
                $('#tab_results'+json.tabId+' #sample_2_column_toggler'+json.tabId).html(listColumns);

        		var dtTable = $('#datatables'+json.tabId).dataTable({
		            data: 			json.parseData.aaData,
		            deferRender: 	true,
		            scrollY: 		318,
		            scrollX: 		'100%',
		            scrollCollapse: true,
		            scroller: 		true
                });

                if(oTable[json.tabId]==undefined){
					oTable.push(dtTable);
                } else {
                	oTable[json.tabId] = dtTable;
                }

                var str_undefined = $('#datatables'+json.tabId+'_wrapper').parent().prop('childNodes')[0];
                if(str_undefined.textContent.indexOf('undefined')>-1){
                	str_undefined.remove();
                }
                str_undefined = $('#sample_2_column_toggler'+json.tabId).prop('childNodes')[0];
                if(str_undefined.textContent.indexOf('undefined')>-1){
                	str_undefined.remove();
                }

		        // $('#sample_2_column_toggler'+json.tabId+' input[type="checkbox"]').change(function(){
		        //     var iCol = parseInt($(this).attr("data-column"));
		        //     var bVis = oTable[json.tabId].fnSettings().aoColumns[iCol].bVisible;
		        //     oTable[json.tabId].fnSetColumnVis(iCol, (bVis ? false : true));
		        // });

			    // $('#tab_results'+json.tabId).find('.searchall').on('keyup',function(){
			    // 	oTable[json.tabId].fnFilter($(this).val());
			    // });

				// var inputval = [];
				// $('#datatables'+json.tabId+'_wrapper thead input').keyup(function(e) {
				// 	var code = (e.keyCode ? e.keyCode : e.which);
				// 		inputval[$(this).data('col')] = this.value;
				// 		oTable[json.tabId].fnFilter( this.value, $(this).data('col'));
				// });
				
				// $('#datatables'+json.tabId+'_wrapper thead input').blur(function(){
				// 	if(inputval[$(this).data('col')] != this.value){
				// 		inputval[$(this).data('col')] = this.value;
				// 		oTable[json.tabId].fnFilter( this.value, $(this).data('col'));
				// 	}
				// });
				
				// $('#datatables'+json.tabId+'_wrapper thead input').focus(function(){
				// 	inputval[$(this).data('col')] = this.value;
				// });

				if(is_rowcount){
					
					$('#tab_results'+json.tabId+' .caption').removeClass('hidden');
					$('#datatables'+json.tabId+'_wrapper .row:last').removeClass('hidden');
					$('#datatables'+json.tabId+'_wrapper .row:last').append(
						'<div class="col-md-7 col-sm-7">'+
							'<div class="btn-group pull-right">'+
		                        '<button type="button" class="btn btn-default disabled btn-prev" data-tab_id="'+json.tabId+'" data-page="0" onclick="getPageData(this);">&lt;</button>'+
		                        '<button type="button" class="btn btn-default disabled btn-page-info"><span>1</span> to <span>1000</span></button>'+
		                        '<button type="button" class="btn btn-default btn-next" data-tab_id="'+json.tabId+'" data-page="2" onclick="getPageData(this);">&gt;</button>'+
		                    '</div>'+
	                    '</div>'
					);

					console.log(json.tabId);

					xhr = $.ajax( {
				        "url": '/bigquery/index/ajax/3',
				        "data": {
				        	'qry':qry,
				        },
				        "dataType": "json",
				        "method": "POST",
				        "beforeSend": function(){
				        	$('#tab_results'+json.tabId).find('.rowcount').html(' <i class="fa fa-spinner fa-spin"></i>');
				        },
				        "complete": function(res){
				        },
				        "success": function(data){
							$('#tab_results'+json.tabId).find('.rowcount').html(data.data);
				        },
				    });

				} else {
					$('#datatables'+json.tabId+'_wrapper .row:last').addClass('hidden');
				}
        	} else {
        		$('#tab_results'+json.tabId).html('<p style="padding:10px;"><span class="label label-danger">ERROR!</span> '+json.message+'</p>');
        	}

        	var divlog = $('.results .tab-content #tab_logs #divlog');
        	if(divlog.find('.table').length==0){
        		divlog.html(
        			'<table class="table table-bordered table-striped">'+
        				'<thead>'+
        					'<tr>'+
        						'<th>Time</th>'+
        						'<th>Query</th>'+
        						'<th>Offset</th>'+
        						'<th>Parsing Data Time</th>'+
        						'<th>Status</th>'+
        						'<th>Message</th>'+
        					'</tr>'+
        				'</thead>'+
        				'<tbody>'+
        				'</tbody>'+
        			'</table>'
    			);
        	}

    		var tbody = divlog.find('.table tbody');
    		var trfirst = divlog.find('.table tbody tr:first');
    		if(trfirst.length==0){
    			tbody.append(
    				'<tr>'+
    					'<td>'+timelog.toLocaleString()+'</td>'+
    					'<td>'+qry+'</td>'+
    					'<td>'+start+'</td>'+
    					'<td>'+json.parsing_data_time+'</td>'+
    					'<td>'+(json.transaction?'<label class="label label-success">SUCCESS</label>':'<label class="label label-danger">FAIL</label>')+'</td>'+
    					'<td>'+json.message+'</td>'+
    				'</tr>'
    			);
    		} else {
    			trfirst.before(
    				'<tr>'+
    					'<td>'+timelog.toLocaleString()+'</td>'+
    					'<td>'+qry+'</td>'+
    					'<td>'+start+'</td>'+
    					'<td>'+json.parsing_data_time+'</td>'+
    					'<td>'+(json.transaction?'<label class="label label-success">SUCCESS</label>':'<label class="label label-danger">FAIL</label>')+'</td>'+
    					'<td>'+json.message+'</td>'+
    				'</tr>'
    			);
    		}
        },
    });

	return false;
}

function getPageData(me){

	if($(me).hasClass('disabled')){
		return false;
	} else if(xhr!=null && xhr!=undefined){
		xhr.abort();
	}

	var page = parseInt($(me).data('page')); //console.log(page);
	var max = (page*1000);
	var offset = max-1000+1;
	var tab_id = parseInt($(me).data('tab_id'));
	var query = $('#tab_results'+tab_id).find('.qry').val();
	var timelog = new Date();

	xhr = $.ajax( {
        "url": '/bigquery/index/ajax/2',
        "data": {
        	'qry':query,
        	'is_cache':is_cache,
        	'limit':1000,
        	'offset':offset,
        	'tabId':tab_id
        },
        "dataType": "json",
        "method": "POST",
        "beforeSend": function(){
            App.blockUI({
                target: '#tab_results'+tab_id,
                boxed: true,
                cenrerY: true,
                message: 'Processing...'
            });
        },
        "complete": function(res){
            App.unblockUI('#tab_results'+tab_id);
        },
        "success": function(json){
        	if(json.transaction){
        		oTable[tab_id].fnClearTable();
        		oTable[tab_id].fnAddData(json.parseData.aaData);

        		if($(me).hasClass('btn-prev')){
        			$(me).data('page',(page-1));
        			$(me).parent().find('.btn-next').data('page',(page+1));
        		} else if($(me).hasClass('btn-next')){
        			$(me).data('page',(page+1));
        			$(me).parent().find('.btn-prev').data('page',(page-1));
        		}

        		// console.log(page);

        		$(me).parent().find('.btn-page-info span:first').html(offset);
        		$(me).parent().find('.btn-page-info span:last').html(max);

        		if(page>1){
        			$(me).parent().find('.btn-prev').removeClass('disabled');
        		} else {
        			$(me).parent().find('.btn-prev').addClass('disabled');
        		}

        		var totalOfRecords = $('#tab_results'+tab_id+' .caption').html();
        		totalOfRecords = parseInt(totalOfRecords.replace(/\,/g, ''));

        		if(max > totalOfRecords){
        			$(me).next().find('span:last').html(totalOfRecords);
        			$(me).parent().find('.btn-next').addClass('disabled');
        		} else {
        			$(me).parent().find('.btn-next').removeClass('disabled');
        		}
        	}

        	var divlog = $('.results .tab-content #tab_logs #divlog');
    		var trfirst = divlog.find('.table tbody tr:first');
			trfirst.before(
				'<tr>'+
					'<td>'+timelog.toLocaleString()+'</td>'+
					'<td>'+query+'</td>'+
					'<td>'+offset+'</td>'+
					'<td>'+json.parsing_data_time+'</td>'+
					'<td>'+(json.transaction?'<label class="label label-success">SUCCESS</label>':'<label class="label label-danger">FAIL</label>')+'</td>'+
					'<td>'+json.message+'</td>'+
				'</tr>'
			);
        },
    });

	return false;
}

var editor;
var oTable = [];
var oTable1 = [];
var tabId = 0;
var btn; 
var timeout;
var start = 1; 
var end = 1000;
var timeout;
var is_cache = true;
var is_rowcount = false;
jQuery(document).ready(function(){

    $('.tr-schemas a.toggle').on('click', function(){
    	// if($('#searchtbx').val().trim()=='' && $('#searchtb').val().trim()==''){
    	if($('#searchtb').val().trim()==''){
        	var database = $(this).data('database');
        	$('.tr-tables.'+database).toggleClass('hidden');

        	var close = $('.tr-tables.'+database).hasClass('hidden');
        	if(close){
        		$('.tr-columns.'+database).addClass('hidden');
        	}
    	}
    });
    $('.tr-tables a.toggle').on('click', function(){
    	// if($('#searchtbx').val()=='' && $('#searchtb').val()==''){
    	if($('#searchtb').val()==''){
        	// console.log($(this).data('database'));
        	var database = $(this).data('database');
        	var table = $(this).data('table');
        	// console.log(database);
        	$('.tr-columns.'+database+'.'+table).toggleClass('hidden');
    	}
    });

    var ctdbd = [];
    $.each($('.tr-schemas'),function(k,v){
    	var x = $(v).find('a').data('database');
    	var y = parseInt($(v).find('.ttb').text());
    	ctdbd[x] = y;
    });

    $('#searchtb').on('keyup',function(){
    	var val = $(this).val();
    	if(val==null||val==''){
    		$('.tr-schemas').removeClass('hidden');
    		$('.tr-tables').addClass('hidden');
    		$('.tr-columns').addClass('hidden');
	        $.each($('.tr-schemas'),function(k,v){
	        	var x = $(v).find('a').text().trim();
	        	$(v).find('.ttb').text(ctdbd[x]);
	        });
    	} else {
    		$('.tr-schemas').addClass('hidden');
    		$('.tr-tables').addClass('hidden');
    		$('.tr-columns').addClass('hidden');
    		var str = $(this).val();
    		// console.log(str);

    		$.each($('#schemas_tablex tbody tr'), function(k,v){
    			var found = false;
    			if($(v).prop('class').indexOf(str)>-1){
    				found = true;
    			}

    			if(found){
    				var str2 = $(v).prop('class').split(' ');
    				// console.log(str2);

    				if(str2[0]=='tr-schemas' && str2[1].indexOf(str)>-1){
    					$(v).removeClass('hidden');
    				} else if(str2[0]=='tr-tables' && str2[2].indexOf(str)>-1){
    					$('.tr-schemas.'+str2[1]).removeClass('hidden');
    					$(v).removeClass('hidden');
    				} else if(str2[0]=='tr-columns' && str2[3].indexOf(str)>-1){
    					// console.log(str2);
    					$('.tr-schemas.'+str2[1]).removeClass('hidden');
    					$('.tr-tables.'+str2[1]+'.'+str2[2]).removeClass('hidden');
    					$(v).removeClass('hidden');
    				}
    			}
    		});
    	}
    });

    editor1 = CodeMirror.fromTextArea(document.getElementById("sql"), {
		lineNumbers: true,
		mode: 'text/x-hive',
		matchBrackets: true,
		styleActiveLine: true,
		lineNumbers: true,
		lineWrapping: true,
		extraKeys: {
			"Ctrl-Space": "autocomplete", // To invoke the auto complete
			"Ctrl-Enter": function(){

				oTable = [];
				oTable1 = [];
				tabId = 0;

				$('.btn-execute').click();
			},
			"Shift-Ctrl-Enter": function(){

				if($('#tab_results0').find('.dataTables_wrapper').length==0){
					oTable = [];
					oTable1 = [];
					tabId = 0;
					
					$('.btn-execute').click();

				} else {

					tabId++;

					if(editor1.getSelection()!=''){
						qry = editor1.getSelection();
					} else {
						qry = editor1.getValue();
					}

					var lastTab1 = $('.tabbale-line .nav-pills li.tabresults:last');
					var cloneTab1 = lastTab1.clone();
					$('.tabbale-line .nav-pills li.tabresults.active').removeClass('active');
					$('.tabbale-line .nav-pills li:first').removeClass('active');
					cloneTab1.addClass('active').find('a').attr('href','#tab_results'+tabId).html('<i class="fa fa-file-text-o"></i> RESULTS('+tabId+')').addC;
					$('.tabbale-line .nav-pills').append(cloneTab1);

					$('.results .tab-content .tabresults.active').removeClass('active');
					$('.results .tab-content #tab_logs').removeClass('active');
					var tabpane = $('.results .tab-content .tab-pane.tabclone').clone();
					tabpane.attr('id','tab_results'+tabId).removeClass('tabclone').addClass('tabresults active');
					$('.results .tab-content').append(tabpane);

					execute();
				}
			}
		}, 
        hint: CodeMirror.hint.sql,
		hintOptions : hintOptions,
    });

    editor1.on("keyup", function(cm, event) {

        var kcode = event.keyCode;
	    if(
	    	(
	    		(kcode==16) ||
	    		(kcode==190) ||
	    		(kcode>=48 && kcode<=57) ||
	    		(kcode>=65 && kcode<=90)
	    	)
	    	&& !editor1.state.completionActive)
	    {
	        if(timeout) clearTimeout(timeout);
	        timeout = setTimeout(function() {
	            CodeMirror.showHint(cm, CodeMirror.hint.sql, {completeSingle: false});
	        }, 150);
	    }
    });

    $('.btn-is_cache, .btn-is_rowcount').on('click', function(){
    	$(this).toggleClass('btn-success');
    	// console.log($(this).hasClass('btn-success'));

    	if($(this).hasClass('btn-is_cache')){
    		is_cache = $(this).hasClass('btn-success');
    	} else if($(this).hasClass('btn-is_rowcount')){
    		is_rowcount = $(this).hasClass('btn-success');
    	}
    });

    $('.btn-execute').on('click', function(){
		if(editor1.getSelection()!=''){
			qry = editor1.getSelection();
		} else {
			qry = editor1.getValue();
		}

		$('.tabbale-line .nav-pills li:first').removeClass('active');
		$('.results > .portlet-body > .tab-content > div#tab_logs').removeClass('active');
		var firstTab1 = $('.tabbale-line .nav-pills li.tabresults:first');
		var cloneTab1 = firstTab1.clone().addClass('active');

		$('.tabbale-line .nav-pills li.tabresults').remove();
		$('.tabbale-line .nav-pills').append(cloneTab1);

		$('.results .tab-content .tabresults').remove();
		var tabpane = $('.results .tab-content .tab-pane.tabclone').clone();
		tabpane.attr('id','tab_results'+tabId).removeClass('tabclone').addClass('tabresults active');
		$('.results .tab-content').append(tabpane);

		execute();
    });

    $('.btn-execute-stop').on('click',function(){
    	stopTimer();
		if(xhr!=undefined){
			xhr.abort();
		}

		$('.btn-execute').removeClass('hidden');
		$(this).addClass('hidden');

		App.unblockUI(jQuery('#tab_results'));
		$('.spin-loader').addClass('hidden');
    });

    $('.modal-header button.close').on('click',function(){
    	if(xhrS!=undefined){
			xhrS.abort();
		}
    });
});