//== Class definition


var Datatable_listpage = function() {
  //== Private functions

  // basic demo
  var demo = function() {

    var datatable_2 = $('#listpage').mDatatable({
      // datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            // sample GET method
            method: 'GET',
            url: '/bigbox/bigspider/listaction2',
            map: function(raw) {
              // sample data mapping
              var dataSet = raw;
              if (typeof raw.data !== 'undefined') {
                dataSet = raw.data;
              }
              return dataSet;
            },
          },
        },
        pageSize: 10,
        serverPaging: true,
        serverFiltering: true,
        serverSorting: true,
      },

      // layout definition
      layout: {
        scroll: false,
        footer: false
      },

      // column sorting
      sortable: true,

      pagination: true,

      toolbar: {
        // toolbar items
        items: {
          // pagination
          pagination: {
            // page size select
            pageSizeSelect: [10, 20, 30, 50, 100],
          },
        },
      },

      search: {
        input: $('#generalSearch'),
      },

      // columns definition
      columns: [
        {
          field: 'No',
          title: 'No',
          sortable: true,
          width: 40,
          selector: false,
          textAlign: 'center',
        },
		{
          field: 'id',
          title: 'ID',
          sortable: true,
          width: 40,
          selector: false,
          textAlign: 'center',
        },{
          field: 'action_name',
          title: 'Action Name',
          width: 250,
          selector: false,
          textAlign: 'center',
        }, {
          field: 'action',
          title: 'Actions',
          // width: 200,
          template: function(row) {
            // callback function support for column rendering
            return row.action;
          },
        }],
    });
	$('#reload').on('click', function() {
		datatable_2.reload();
    });

  };

  
  return {
    // public functions
    init: function() {
      demo();
    },
  };
}();
jQuery(document).ready(function() {
  Datatable_listpage.init();
});