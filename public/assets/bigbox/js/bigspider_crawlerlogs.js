//List Portlet Javascript
var Datatable_crawlerlogs = function() {
  //== Private functions

  // basic demo
  var crawlerlogs = function() {

    var datatable_1 = $('#crawlogs').mDatatable({
      // datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            // sample GET method
            method: 'GET',
            url: '/bigbox/bigspider/ajaxlogscrawl/app/',
            map: function(raw) {
              // sample data mapping
              var dataSet = raw;
              if (typeof raw.data !== 'undefined') {
                dataSet = raw.data;
              }
              return dataSet;
            },
          },
        },
        pageSize: 10,
        serverPaging: true,
        serverFiltering: true,
        serverSorting: true,
      },

      // layout definition
      layout: {
        scroll: false,
        footer: false
      },

      // column sorting
      sortable: true,

      pagination: true,

      toolbar: {
        // toolbar items
        items: {
          // pagination
          pagination: {
            // page size select
            pageSizeSelect: [10, 20, 30, 50, 100],
          },
        },
      },
	  search: {
        input: $('#generalSearch'),
      },
      // columns definition
      columns: [
        {
          field: 'No',
          title: 'No',
		  width: 40,
          sortable: true,
          selector: false,
          textAlign: 'center'
        },
		{
          field: 'id',
          title: 'ID',
		   width: 60,
          sortable: true,
          selector: false,
          textAlign: 'center'
        },{
          field: 'index_url',
          title: 'Url', 
		  width: 200,
		  sortable: true,
          selector: false,
          textAlign: 'center'
        },{
          field: 'sub_domain',
          title: 'Sub Domain', 
		  width: 60,
		  sortable: true,
          selector: false,
          textAlign: 'center'
        },{
          field: 'action',
          title: 'Status',
			width: 60,		  
		  sortable: true,
          selector: false,
          textAlign: 'center'
        }],
    });
	// $('#sample_2_column_toggler input[type="checkbox"]').on('change', function() {
		// /* Get the DataTables object again - this is not a recreation, just a get of the object */
		
		// var iCol = $(this).attr("data-column");
		// datatable_1.hideColumn(iCol);
		
	// });
	$('#reload').on('click', function() {
		datatable_1.reload();
    });

  };

  
  return {
    init: function() {
      crawlerlogs();
    },
  };
}();

jQuery(document).ready(function() {
	Datatable_crawlerlogs.init();
});