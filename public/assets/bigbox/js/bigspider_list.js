//== Class definition

var DatatableHtmlTableDemo = function() {
	//== Private functions

	// demo initializer
	var demo = function() {

		var datatable = $('.m-datatable').mDatatable({
			data: {
				saveState: {cookie: false},
			},
			search: {
				input: $('#generalSearch'),
			},
			columns: [
				{
					field: 'No',
					width:20,
					type: 'number',
				},
				{
					field: 'Status',
					title: 'Status',
					width:75
					
				},
				{
					field: 'Queue Template Name',
					title: 'Queue Template Name',
				},
				{
					field: 'Interval (second)',
					title: 'Interval (second)',
					width:40
				},
				{
					field: 'Count',
					title: 'Count',
					width:40
				},
				{
					field: 'File',
					title: 'file',
				},
				{
					field: 'Field5',
					title: 'Action',
					width:200
				}
			],
		});

		$('#m_form_status').on('change', function() {
			datatable.search($(this).val().toLowerCase(), 'Status');
		});

		$('#m_form_type').on('change', function() {
			datatable.search($(this).val().toLowerCase(), 'Type');
		});

		$('#m_form_status, #m_form_type').selectpicker();

	};

	return {
		//== Public functions
		init: function() {
			// init dmeo
			demo();
		},
	};
}();

jQuery(document).ready(function() {
	DatatableHtmlTableDemo.init();
});