//== Class definition

var DatatableHtmlTableDemo = function() {
	//== Private functions

	// demo initializer
	var demo = function() {

		var datatable = $('.m-datatable').mDatatable({
			data: {
				saveState: {cookie: false},
			},
			search: {
				input: $('#generalSearch'),
			},
			columns: [
				{
				  field: 'No',
				  title: 'No',
				  sortable: true,
				  width: 20,
				  selector: false,
				  textAlign: 'left',
				},
				{
				  field: 'Nama Media',
				  title: 'Nama Media',
				  sortable: true,
				  width: 150,
				  selector: false,
				  textAlign: 'left',
				},
				{
				  field: 'Url',
				  title: 'Url',
				  sortable: true,
				  width: 250,
				  selector: false,
				  textAlign: 'left',
				},
				{
				  field: 'Atributes',
				  title: 'Atributes',
				  sortable: true,
				  width: 250,
				  selector: false,
				  textAlign: 'left',
				},
				{
					field: 'Tipe Crawling',
					title: 'Tipe Crawling',
					sortable: true,
					width: 40,
					selector: false,
					textAlign: 'left',
				},{
					field: 'Action',
					title: 'Action',
					sortable: true,
					width: 150,
					selector: false,
					textAlign: 'left',
				},
			],
		});

		$('#media_type').on('change', function() {
			datatable.search($(this).val().toLowerCase(), 'Tipe Crawling');
		});
		$('#reload').on('click', function() {
			datatable.reload();
		});

		$('#media_type').selectpicker();

	};

	return {
		//== Public functions
		init: function() {
			// init dmeo
			demo();
		},
	};
}();

jQuery(document).ready(function() {
	DatatableHtmlTableDemo.init();
});