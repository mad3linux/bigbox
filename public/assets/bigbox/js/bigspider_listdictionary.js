//== Class definition

var Datatable_listpage = function() {
  // basic demo
  var demo = function() {
    var datatable_2 = $('#listdictionary').mDatatable({
      // datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            // sample GET method
            method: 'GET',
            url: '/bigbox/bigspider/tabledictionary',
            // url: 'https://keenthemes.com/metronic/preview/inc/api/datatables/demos/default.php',
            map: function(raw) {
              // sample data mapping
              var dataSet = raw;
              if (typeof raw.data !== 'undefined') {
                dataSet = raw.data;
              }
              return dataSet;
            },
          },
        },
        pageSize: 10,
        serverPaging: true,
        serverFiltering: true,
        serverSorting: true,
      },

      // layout definition
      layout: {
        scroll: false,
        footer: false
      },

      // column sorting
      sortable: true,

      pagination: true,

      toolbar: {
        // toolbar items
        items: {
          // pagination
          pagination: {
            // page size select
            pageSizeSelect: [10, 20, 30, 50, 100],
          },
        },
      },

      search: {
        input: $('#generalSearch'),
      },

      // columns definition
      columns: [
        {
          field: 'No',
          title: 'No',
          sortable: true,
          width: 40,
          selector: false,
          textAlign: 'center',
        },
		{
          field: 'kata',
          title: 'Kata',
          width: 150,
          template: function(row) {
            // callback function support for column rendering
            return row.kata;
          },
        },{
          field: 'bahasa',
          title: 'Bahasa',
          width: 140,
          template: function(row) {
            // callback function support for column rendering
            return row.bahasa;
          },
        },{
          field: 'topik',
          title: 'Topik',
		  width: 200,
          template: function(row) {
            // callback function support for column rendering
            return row.topik;
          },
        },{
          field: 'sentiment',
          title: 'Sentimen',
          width: 150,
          template: function(row) {
            // callback function support for column rendering
            return row.sentiment;
          },
        }, {
          field: 'action',
          title: 'Actions',
          width: 200,
          template: function(row) {
            // callback function support for column rendering
            return row.action;
          },
        }],
    });
	$('#reload').on('click', function() {
		datatable_2.reload();
    });
	
	$('#bahasa').on('change', function() {
        datatable_2.search($(this).val().toLowerCase(), 'bahasa');
    });
	
	$('#bahasa').selectpicker();

  };

  
  return {
    // public functions
    init: function() {
      demo();
    },
  };
}();
jQuery(document).ready(function() {
  Datatable_listpage.init();
});