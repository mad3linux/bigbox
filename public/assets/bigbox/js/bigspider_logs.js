//List Portlet Javascript
var Datatable_crawlerlogs = function() {
  //== Private functions

  // basic demo
  var crawlerlogs = function() {

    var datatable_1 = $('#crawlogs').mDatatable({
      // datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            // sample GET method
            method: 'GET',
            url: '/bigbox/bigspider/ajaxlogs/app/',
            map: function(raw) {
              // sample data mapping
              var dataSet = raw;
              if (typeof raw.data !== 'undefined') {
                dataSet = raw.data;
              }
              return dataSet;
            },
          },
        },
        pageSize: 10,
        serverPaging: true,
        serverFiltering: true,
        serverSorting: true,
      },

      // layout definition
      layout: {
        scroll: false,
        footer: false
      },

      // column sorting
      sortable: true,

      pagination: true,

      toolbar: {
        // toolbar items
        items: {
          // pagination
          pagination: {
            // page size select
            pageSizeSelect: [10, 20, 30, 50, 100],
          },
        },
      },
      search: {
        input: $('#generalSearch'),
      },
      // columns definition
      columns: [
        {
          field: 'No',
          title: 'No',
          sortable: true,
          width: 40,
          selector: false,
          textAlign: 'center',
        },
		{
          field: 'log_type',
          title: 'Log Type',
          sortable: true,
          //width: 40,
          selector: false,
          textAlign: 'center',
        },{
          field: 'subject',
          title: 'Subject',
          //width: 120,
          selector: false,
          textAlign: 'center',
        }, {
          field: 'reff_id',
          title: 'Reff ID',
          //width: 120,
          selector: false,
          textAlign: 'center',
        }, {
          field: 'log_date',
          title: 'Log Date',
          //width: 100,
		  selector: false,
          textAlign: 'center',
        }, {
          field: 'description',
          title: 'Description',
          //width: 100,
          selector: false,
          textAlign: 'center',
        }, {
          field: 'x2',
          title: 'By',
          //width: 100,
          selector: false,
          textAlign: 'center',
        }],
    });
	// $('#sample_2_column_toggler input[type="checkbox"]').on('change', function() {
		// /* Get the DataTables object again - this is not a recreation, just a get of the object */
		
		// var iCol = $(this).attr("data-column");
		// datatable_1.hideColumn(iCol);
		
	// });
	// $('#reload').on('click', function() {
		// datatable_1.reload();
    // });

  };

  
  return {
    init: function() {
      crawlerlogs();
    },
  };
}();

jQuery(document).ready(function() {
	Datatable_crawlerlogs.init();
});