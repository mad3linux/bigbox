var Treeview = function () {
    var demo6 = function() {
        $("#tree").jstree({
            "core" : {
                "themes" : {
                    "responsive": false
                }, 
                // so that create works
                "check_callback" : true,
                'data' : {
                    'url' : function (node) {
                      return '/domas/editor/sourcecode/ajax/0';
                    },
                    'data' : function (node) {
                      return { 'parent' : node.key };
                    }
                }
            },
            "types" : {
                "default" : {
                    "icon" : "fa fa-folder m--font-brand"
                },
                "file" : {
                    "icon" : "fa fa-file  m--font-brand"
                }
            },
            "state" : { "key" : "demo3" },
            "plugins" : [ "dnd", "state", "types" ]
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            demo6();
        }
    };
}();

jQuery(document).ready(function() {    
    Treeview.init();
});