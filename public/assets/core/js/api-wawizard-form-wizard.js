var defsuccess='<button class="close" data-dismiss="alert"></button>Your form validation is successful!';
var deferror='<button class="close" data-dismiss="alert"></button>You have some form errors. Please check below.';
var form = $('#submit_form');
var error = $('.alert-danger', form);
var success = $('.alert-success', form);

var gnr_date = null;
var act_date = null;

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();
if(dd<10) {
    dd='0'+dd;
} 

if(mm<10) {
    mm='0'+mm;
} 

var H = '00';
var i = '00';
var s = '00';
var td = yyyy+'-'+mm+'-'+dd+' ';

function currentTime(){	
H = today.getHours();
if(H<10) {
	H='0'+H;
} 

i = today.getMinutes();
if(i<10) {
	i='0'+i;
} 

s = today.getSeconds();
if(s<10) {
	s='0'+H;
} 

return td+H+':'+i+':'+s;
}

function checkPhone(phone,nick,havecode){
$.ajax({
	type: "GET",
	url: '/api/checknumber?phone='+phone,
	contentType: "application/json; charset=utf-8",
	//dataType: "json",
	//async: false,
	beforeSend: function(){
		App.scrollTo(error, -200);
		App.blockUI(el);
		success.hide();
		error.hide();
	},
	complete: function(){
		App.unblockUI(el);
	},
	success: function(data){
		console.log(data);
		App.unblockUI(el);
		if(data.result){
			error.html('<button class="close" data-dismiss="alert"></button>'+phone+' has been used and active.');
			error.show();
			App.scrollTo(error, -200);
			return false;
		}else{
			if(!havecode){
				requestCode(phone,nick);
			}else{
				$('#form_wizard_1').bootstrapWizard('show',1);
			}
		}
	},
	error: function(fnc,msg){
		App.unblockUI(el);
		error.html('<button class="close" data-dismiss="alert"></button>Failed to request code, please try again.');
		error.show();
		App.scrollTo(error, -200);
		return false;
	}
});
}

function requestCode(phone,nick){
$.ajax({
	type: "GET",
	url: 'http://10.62.180.6/prahu/wa/action.php?act=register&phone='+phone+'&cc=62&type=sms&nick='+nick,
	contentType: "application/json; charset=utf-8",
	//dataType: "json",
	//async: false,
	beforeSend: function(){
		App.scrollTo(error, -200);
		App.blockUI(el);
		success.hide();
		error.hide();
	},
	complete: function(){
		App.unblockUI(el);
	},
	success: function(data){
		console.log(data);
		if(data.retCode!=undefined){
			if(data.return==true && data.retCode=="00"){
				success.html('<button class="close" data-dismiss="alert"></button>Please wait for a while, you will be receive a code by sms.');
				success.show();
				App.scrollTo(success, -200);
				gnr_date = currentTime();
				console.log(gnr_date+',GNR');
				$('#gnr_date').attr('value',gnr_date);
				$('#form_wizard_1').bootstrapWizard('show',1);
			}else{
				error.html('<button class="close" data-dismiss="alert"></button>Failed to request code, please try again.');
				error.show();
				App.scrollTo(error, -200);
				return false;
			}
		}else{
			error.html('<button class="close" data-dismiss="alert"></button>Failed to request code, please try again.');
			error.show();
			App.scrollTo(error, -200);
			return false;
		}
	},
	error: function(fnc,msg){
		error.html('<button class="close" data-dismiss="alert"></button>Failed to request code, please try again.');
		error.show();
		App.scrollTo(error, -200);
		return false;
	}
});
}

function requestPass(phone,nick,code){
$.ajax({
	type: "GET",
	url: 'http://10.62.180.6/prahu/wa/action.php?act=activate&phone='+phone+'&nick='+nick+'&cc=62&code='+code,
	contentType: "application/json; charset=utf-8",
	//dataType: "json",
	//async: false,
	beforeSend: function(){
		App.scrollTo(error, -200);
		App.blockUI(el);
		success.hide();
		error.hide();
	},
	complete: function(){
		App.unblockUI(el);
	},
	success: function(data){
		console.log(data);
		if(data.retCode!=undefined){
			if(data.return==true && data.retCode=="00"){
				success.html('<button class="close" data-dismiss="alert"></button>'+data.onCodeRegister.phone+' has been active.');
				success.show();
				App.scrollTo(error, -200);
				act_date = currentTime();
				console.log(act_date+',ACT');
				$('#act_date').attr('value',act_date);
				$('#authpass').attr('value',data.onCodeRegister.pw);
				$('#dispauthpass').html('value',data.onCodeRegister.pw);
				
				//save to db
				saveWA(phone,nick,code,data.onCodeRegister.pw);
				$('#form_wizard_1').bootstrapWizard('show',2);
			}else{
				error.html('<button class="close" data-dismiss="alert"></button>Failed to request code, please try again.');
				error.show();
				App.scrollTo(error, -200);
				return false;
			}
		}else{
			error.html('<button class="close" data-dismiss="alert"></button>Failed to request code, please try again.');
			error.show();
			App.scrollTo(error, -200);
			return false;
		}
	},
	error: function(fnc,msg){
		error.html('<button class="close" data-dismiss="alert"></button>Failed to request code, please try again.');
		error.show();
		App.scrollTo(error, -200);
		return false;
	}
});
}

function saveWA(phone,nick,actcode,pass){
var datax = {phoneno:phone,nickname:nick,code:actcode,password:pass,generate_date:gnr_date,activate_date:act_date};
console.log(datax);
$.ajax({
	type: "POST",
	url: '/api/savewa',
	//contentType: "application/json; charset=utf-8",
	data: datax,
	beforeSend: function(){
	},
	complete: function(){
	},
	success: function(data){
	},
	error: function(fnc,msg){
	}
});
}

var FormWizard = function () {
return {
	//main function to initiate the module
	init: function () {
		if (!jQuery().bootstrapWizard) {
			return;
		}

		function format(state) {
			if (!state.id) return state.text; // optgroup
			return "<img class='flag' src='/assets/core/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
		}

		$("#country_list").select2({
			placeholder: "Select",
			allowClear: true,
			formatResult: format,
			formatSelection: format,
			escapeMarkup: function (m) {
				return m;
			}
		});

		form.validate({
			doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
			errorElement: 'span', //default input error message container
			errorClass: 'help-block', // default input error message class
			focusInvalid: false, // do not focus the last invalid input
			rules: {
				//account
				phonenum: {
					number: true,
					required: true
				},
				nickname: {
					letterswithbasicpunc: true,
					required: true
				},
				actcode: {
					required: true
				}/*,
				password: {
					minlength: 5,
					required: true
				},
				rpassword: {
					minlength: 5,
					required: true,
					equalTo: "#submit_form_password"
				},*/
				//profile
				/*fullname: {
					required: true
				},
				email: {
					required: true,
					email: true
				},
				phone: {
					required: true
				},
				gender: {
					required: true
				},
				address: {
					required: true
				},
				city: {
					required: true
				},
				country: {
					required: true
				},
				//payment
				card_name: {
					required: true
				},
				card_number: {
					minlength: 16,
					maxlength: 16,
					required: true
				},
				card_cvc: {
					digits: true,
					required: true,
					minlength: 3,
					maxlength: 4
				},
				card_expiry_date: {
					required: true
				},
				'payment[]': {
					required: true,
					minlength: 1
				}*/
			},

			messages: { // custom messages for radio buttons and checkboxes
				/*'payment[]': {
					required: "Please select at least one option",
					minlength: jQuery.format("Please select at least one option")
				}*/
			},

			errorPlacement: function (error, element) { // render error placement for each input type
				if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
					error.insertAfter("#form_gender_error");
				} else if (element.attr("name") == "payment[]") { // for uniform radio buttons, insert the after the given container
					error.insertAfter("#form_payment_error");
				} else {
					error.insertAfter(element); // for other inputs, just perform default behavior
				}
			},

			invalidHandler: function (event, validator) { //display error alert on form submit
				success.html(defsuccess);
				error.html(deferror);
				success.hide();
				error.show();
				App.scrollTo(error, -200);
			},

			highlight: function (element) { // hightlight error inputs
				$(element)
					.closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
			},

			unhighlight: function (element) { // revert the change done by hightlight
				$(element)
					.closest('.form-group').removeClass('has-error'); // set error class to the control group
			},

			success: function (label) {
				if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
					label
						.closest('.form-group').removeClass('has-error').addClass('has-success');
					label.remove(); // remove error label here
				} else { // display success icon for other inputs
					label
						.addClass('valid') // mark the current input as valid and display OK icon
					.closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
				}
			},

			submitHandler: function (form) {
				success.html(defsuccess);
				error.html(deferror);
				success.show();
				error.hide();
				//add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
			}

		});

		var displayConfirm = function() {
			$('#tab3 .form-control-static', form).each(function(){
				console.log('[name="'+$(this).attr("data-display")+'"]');
				var input = $('[name="'+$(this).attr("data-display")+'"]', form);
				if(input.length<=0){
					input = $('#'+$(this).attr("data-display"));
				}
				
				if (input.is(":hidden") || input.is(":text") || input.is("textarea")) {
					if(input.val()==''){
						$(this).html(input.attr('value'));
					}else{
						$(this).html(input.val());
					}
				} else if (input.is("select")) {
					$(this).html(input.find('option:selected').text());
				} else if (input.is(":radio") && input.is(":checked")) {
					$(this).html(input.attr("data-title"));
				} else if ($(this).attr("data-display") == 'payment') {
					var payment = [];
					$('[name="payment[]"]').each(function(){
						payment.push($(this).attr('data-title'));
					});
					$(this).html(payment.join("<br>"));
				}else{
					if(input.val()==''){
						$(this).html(input.attr('value'));
					}else{
						$(this).html(input.val());
					}
				}
			});
		}

		var handleTitle = function(tab, navigation, index) {
			var total = navigation.find('li').length;
			var current = index + 1;
			// set wizard title
			$('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
			// set done steps
			jQuery('li', $('#form_wizard_1')).removeClass("done");
			var li_list = navigation.find('li');
			for (var i = 0; i < index; i++) {
				jQuery(li_list[i]).addClass("done");
			}

			if (current == 1) {
				$('#form_wizard_1').find('.button-previous').hide();
			} else {
				$('#form_wizard_1').find('.button-previous').show();
			}

			if (current >= total) {
				$('#form_wizard_1').find('.button-next').hide();
				$('#form_wizard_1').find('.button-previous').hide();
				$('#form_wizard_1').find('.button-new').show();
				$('#form_wizard_1').find('.button-view').show();
				displayConfirm();
			} else {
				$('#form_wizard_1').find('.button-next').show();
				$('#form_wizard_1').find('.button-new').hide();
				$('#form_wizard_1').find('.button-view').hide();
			}
			
			App.scrollTo($('.page-title'));
		}

		// default form wizard
		$('#form_wizard_1').bootstrapWizard({
			'nextSelector': '.button-next',
			'previousSelector': '.button-previous',
			onTabClick: function (tab, navigation, index, clickedIndex) {
				success.hide();
				error.hide();
				if (form.valid() == false) {
					return false;
				}
				
				console.log(index+" tabclick");
				if(index==0 && clickedIndex==1){
					$("#submit_form_phonenum2").val($("#submit_form_phonenum").val());
					$("#submit_form_nickname2").val($("#submit_form_nickname").val());
					
					if($("#submit_form_havecode:checked").length>0){
						console.log('have code');
						checkPhone($("#submit_form_phonenum").val(),$("#submit_form_nickname").val(),true);
						return false;
					}else{
						console.log('do not have code');
						checkPhone($("#submit_form_phonenum").val(),$("#submit_form_nickname").val(),false);
						return false;
					}
				}else if(index==1 && clickedIndex==2){
					requestPass($("#submit_form_phonenum").val(),$("#submit_form_nickname").val(),$("#submit_form_actcode").val());
					return false;
				}
				
				handleTitle(tab, navigation, clickedIndex);
			},
			onNext: function (tab, navigation, index) {
				success.hide();
				error.hide();

				if (form.valid() == false) {
					return false;
				}
				
				console.log(index+" onNext");
				if(index==1){
					$("#submit_form_phonenum2").val($("#submit_form_phonenum").val());
					$("#submit_form_nickname2").val($("#submit_form_nickname").val());
					
					if($("#submit_form_havecode:checked").length>0){
						console.log('have code');
						checkPhone($("#submit_form_phonenum").val(),$("#submit_form_nickname").val(),true);
						return false;
					}else{
						console.log('do not have code');
						checkPhone($("#submit_form_phonenum").val(),$("#submit_form_nickname").val(),false);
						return false;
					}
				}else if(index==2){
					requestPass($("#submit_form_phonenum").val(),$("#submit_form_nickname").val(),$("#submit_form_actcode").val());
					return false;
				}

				handleTitle(tab, navigation, index);
			},
			onPrevious: function (tab, navigation, index) {
				success.hide();
				error.hide();
				console.log(index+" onPrev");

				handleTitle(tab, navigation, index);
			},
			onTabShow: function (tab, navigation, index) {
				var total = navigation.find('li').length;
				var current = index + 1;
				var $percent = (current / total) * 100;
				$('#form_wizard_1').find('.progress-bar').css({
					width: $percent + '%'
				});
				handleTitle(tab, navigation, index);
			}
		});

		$('#form_wizard_1').find('.button-previous').hide();		
		$('#form_wizard_1').find('.button-view').hide();
		$('#form_wizard_1 .button-new').click(function () {
			//alert('Finished! Hope you like it :)');
		}).hide();
	}

};
}();
