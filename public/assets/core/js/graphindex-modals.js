var UIExtendedModals = function () {
    return {
        //main function to initiate the module
        init: function () {
        
            // general settings
            $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner = 
              '<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
                '<div class="progress progress-striped active">' +
                  '<div class="progress-bar" style="width: 100%;"></div>' +
                '</div>' +
              '</div>';

            $.fn.modalmanager.defaults.resize = true;

            //ajax demo:
            var $modal = $('#ajax-modal');

			// handle record edit/remove
            $('body').on('click', '#graph_table_wrapper .btn-graph', function() {
              var graphid =  $(this).attr("data-id");
              var cust =  $(this).attr("data-cust");
              var addr =  $(this).attr("data-addr");
              var sid =  $(this).attr("data-sid");
              var title =  $(this).attr("data-title");
                
              // create the backdrop and wait for next modal to be triggered
              $('body').modalmanager('loading');
              
              //$.get('http://netcare.telkom.co.id/cacti/him_graph2.php?action=zoom&local_graph_id='+graphid+'&rra_id=5&view_type=&bypass=99030f464aa2e1780299fbd5c192ab04');
				//console.log('/mrtg/graph/indexdetail?ajaxview=true&graph='+graphid+'&cust='+cust+'&addr='+addr+'&sid='+sid+'&title='+encodeURIComponent(title));
              setTimeout(function(){
                  $modal.load('/mrtg/graph/indexdetail?ajaxview=true&graph='+graphid+'&cust='+encodeURIComponent(cust)+'&addr='+encodeURIComponent(addr)+'&sid='+encodeURIComponent(sid)+'&title='+encodeURIComponent(title), '', function(){
                  $modal.modal();
                });
              }, 1000);
            });

            $modal.on('click', 'button#graphfilter', function(){
              $modal.modal('loading');
              var start = '';
              var time = '';
              if($modal.find('input#startdate')!=undefined && $modal.find('input#startdate').val()!=''){
                start = $modal.find('input#startdate').val()+':00';
                //alert(start);
                start = Date.parse(start)/1000;
                time += '&graph_start='+start;
              }
              
              var end = '';
              if($modal.find('input#enddate')!=undefined && $modal.find('input#enddate').val()!=''){
                end = $modal.find('input#enddate').val()+':00';
                //alert(end);
                end = Date.parse(end)/1000;
                time += '&graph_end='+end;
              }
              
              var graph = '';
              if($modal.find('input#graph')!=undefined && $modal.find('input#graph').val()!=''){
                var graph = $modal.find('input#graph').val();
                var bypass = $modal.find('input#bypass').val();
                //alert(graph);
              }
              
              //alert('graph='+graph+'&startdate='+start+'&enddate='+end);
              //url = '/mrtg/graph/detail?ajaxview=true&graph='+graph+'&startdate='+start+'&enddate='+end;
              //alert(url);
              var html = '';
              
              if(start!='' && end!=''){
				html = '<img src="http://telkomcare.telkom.co.id/libcacti/graph.php?action=zoom&local_graph_id='+graph+'&rra_id=5&view_type='+time+'&graph_height=120&graph_width=500&title_font_size=10" style="width:75%;padding:0px;margin:10px;"/>';
               //html = '<iframe style="width:100%;display:block;min-height:330px;border:none;" src="http://202.134.2.182/him_graph2.php?action=zoom&local_graph_id='+graph+'&rra_id=5&view_type='+time+'&bypass='+bypass+'" />';
              }else{
				html += '<img src="http://telkomcare.telkom.co.id/libcacti/graph.php?action=zoom&local_graph_id='+graph+'&rra_id=5&view_type='+time+'&graph_height=120&graph_width=500&title_font_size=10" style="width:75%;padding:0px;margin:10px;"/>';
				html += '<img src="http://telkomcare.telkom.co.id/libcacti/graph.php?action=zoom&local_graph_id='+graph+'&rra_id=1&view_type='+time+'&graph_height=120&graph_width=500&title_font_size=10" style="width:75%;padding:0px;margin:10px;"/>';
				html += '<img src="http://telkomcare.telkom.co.id/libcacti/graph.php?action=zoom&local_graph_id='+graph+'&rra_id=2&view_type='+time+'&graph_height=120&graph_width=500&title_font_size=10" style="width:75%;padding:0px;margin:10px;"/>';
				html += '<img src="http://telkomcare.telkom.co.id/libcacti/graph.php?action=zoom&local_graph_id='+graph+'&rra_id=3&view_type='+time+'&graph_height=120&graph_width=500&title_font_size=10" style="width:75%;padding:0px;margin:10px;"/>';
				html += '<img src="http://telkomcare.telkom.co.id/libcacti/graph.php?action=zoom&local_graph_id='+graph+'&rra_id=4&view_type='+time+'&graph_height=120&graph_width=500&title_font_size=10" style="width:75%;padding:0px;margin:10px;"/>';
               //html = '<iframe style="width:100%;display:block;min-height:330px;border:none;" src="http://202.134.2.182/him_graph2.php?action=zoom&local_graph_id='+graph+'&rra_id=5&view_type='+time+'&bypass='+bypass+'" /><iframe style="width:100%;display:block;min-height:323px;border:none;" src="http://10.62.8.126/cacti/him_graph2.php?action=zoom&local_graph_id='+graph+'&rra_id=1&view_type='+time+'&bypass='+bypass+'" /><iframe style="width:100%;display:block;min-height:323px;border:none;" src="http://10.62.8.126/cacti/him_graph2.php?action=zoom&local_graph_id='+graph+'&rra_id=2&view_type='+time+'&bypass='+bypass+'" /><iframe style="width:100%;display:block;min-height:323px;border:none;" src="http://10.62.8.126/cacti/him_graph2.php?action=zoom&local_graph_id='+graph+'&rra_id=3&view_type='+time+'&bypass='+bypass+'" /><iframe style="width:100%;display:block;min-height:323px;border:none;" src="http://10.62.8.126/cacti/him_graph2.php?action=zoom&local_graph_id='+graph+'&rra_id=4&view_type='+time+'&bypass='+bypass+'" />';
              }
              setTimeout(function(){
                $modal
                  .modal('loading')
                  .find('#framecontent').html(html)
                  /*.load(url, '', function(){
                     $modal.modal();
                   });*/
              }, 1000);
            });

            $modal.on('click', 'button#graphclear', function(){
              if($modal.find('input#startdate')!=undefined && $modal.find('input#startdate').val()!=''){
                $modal.find('input#startdate').val('');
              }
              
              if($modal.find('input#enddate')!=undefined && $modal.find('input#enddate').val()!=''){
                $modal.find('input#enddate').val('');
              }
            });

            $modal.on('click', 'button#modalclose', function(){
                $modal.modal('hide');
            });
            
            var $modal2 = $('#static');
            $('body').on('click', '#graphprint', function() {
				var graph = $('input.graphcheck:checked');
				if(graph.length>0){
					$modal2.modal();
				}else{
					alert('Please choose graph to print out.');
				}
            });

            $modal2.on('click', 'button#graphclear2', function(){
              if($modal2.find('input#startdate2')!=undefined && $modal2.find('input#startdate2').val()!=''){
                $modal2.find('input#startdate2').val('');
              }
              
              if($modal2.find('input#enddate2')!=undefined && $modal2.find('input#enddate2').val()!=''){
                $modal2.find('input#enddate2').val('');
              }
            });

            $modal2.on('click', 'button#printconfirm', function(e){
				// console.log($modal2);
				var graph = $('input.graphcheck:checked');
				// console.log($('input.graphcheck:checked'));
				// console.log($('#printform').find("input[type='hidden']"));
				hide = $modal2.find('#hiddeninput');
				//console.log(hide);
				$(hide[0]).html('');
				//console.log(hide);
				var list = {};
				for(var i=0;i<graph.length;i++){
					$(hide[0]).append('<input type="hidden" class="hiddeninput" name="check['+i+']" value="'+$(graph[i]).val()+'"/>');
					$(hide[0]).append('<input type="hidden" class="hiddeninput" name="title['+i+']" value="'+$(graph[i]).data('title')+'"/>');
					$(hide[0]).append('<input type="hidden" class="hiddeninput" name="sid['+i+']" value="'+$(graph[i]).data('sid')+'"/>');
					$(hide[0]).append('<input type="hidden" class="hiddeninput" name="cust['+i+']" value="'+$(graph[i]).data('cust')+'"/>');
					$(hide[0]).append('<input type="hidden" class="hiddeninput" name="addr['+i+']" value="'+$(graph[i]).data('addr')+'"/>');
					//list[i] = {'enc':$(graph[i]).data('id'),'dec':$(graph[i]).val()};
				}
				//console.log(list);
				 form = $modal2.find('form');				
				console.log(form[0]);
				$(form[0]).attr('action','/mrtg/public/printgraphs?random='+Math.random());
				$(form[0]).submit();
				e.preventDefault();
            });
            
        }

    };

}();

var UIExtendedModals2 = function () {
    return {
        //main function to initiate the module
        init: function () {
        
            // general settings
            $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner = 
              '<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
                '<div class="progress progress-striped active">' +
                  '<div class="progress-bar" style="width: 100%;"></div>' +
                '</div>' +
              '</div>';

            $.fn.modalmanager.defaults.resize = true;

            //ajax demo:
            var $modal = $('#ajax-modal');

			// handle record edit/remove
            $('body').on('click', '#detail_content .btn-graph', function() {
              var graphid =  $(this).attr("data-id");
              var cust =  $(this).attr("data-cust");
              var addr =  $(this).attr("data-addr");
              var sid =  $(this).attr("data-sid");
              var title =  $(this).attr("data-title");
              var link =  $(this).attr("data-link");
                
              // create the backdrop and wait for next modal to be triggered
              $('body').modalmanager('loading');
			  
			  // console.log($(this).attr("data-id"));
              
              //$.get('http://netcare.telkom.co.id/cacti/him_graph2.php?action=zoom&local_graph_id='+graphid+'&rra_id=5&view_type=&bypass=99030f464aa2e1780299fbd5c192ab04');
				//console.log('/mrtg/graph/indexdetail?ajaxview=true&graph='+graphid+'&cust='+cust+'&addr='+addr+'&sid='+sid+'&title='+encodeURIComponent(title));
              setTimeout(function(){
                  $modal.load('/mrtg/graph/indexdetailkaa?ajaxview=true&graph='+graphid+'&cust='+encodeURIComponent(cust)+'&addr='+encodeURIComponent(addr)+'&sid='+encodeURIComponent(sid)+'&title='+encodeURIComponent(title)+'&link='+link, '', function(){
                  $modal.modal();
                });
              }, 1000);
            });

            $('body').on('click', '#linkmediacenter .btn-graph', function() {
              var graphid =  $(this).attr("data-id");
              var cust =  $(this).attr("data-cust");
              var addr =  $(this).attr("data-addr");
              var sid =  $(this).attr("data-sid");
              var title =  $(this).attr("data-title");
              var link =  $(this).attr("data-link");
                
              // create the backdrop and wait for next modal to be triggered
              $('body').modalmanager('loading');
			  
			  // console.log($(this).attr("data-id"));
              
              //$.get('http://netcare.telkom.co.id/cacti/him_graph2.php?action=zoom&local_graph_id='+graphid+'&rra_id=5&view_type=&bypass=99030f464aa2e1780299fbd5c192ab04');
				//console.log('/mrtg/graph/indexdetail?ajaxview=true&graph='+graphid+'&cust='+cust+'&addr='+addr+'&sid='+sid+'&title='+encodeURIComponent(title));
              setTimeout(function(){
                  $modal.load('/mrtg/graph/indexdetailkaa?ajaxview=true&graph='+graphid+'&cust='+encodeURIComponent(cust)+'&addr='+encodeURIComponent(addr)+'&sid='+encodeURIComponent(sid)+'&title='+encodeURIComponent(title)+'&link='+link, '', function(){
                  $modal.modal();
                });
              }, 1000);
            });

            $('body').on('click', '#linkmetrocctvbdg .btn-graph', function() {
              var graphid =  $(this).attr("data-id");
              var cust =  $(this).attr("data-cust");
              var addr =  $(this).attr("data-addr");
              var sid =  $(this).attr("data-sid");
              var title =  $(this).attr("data-title");
              var link =  $(this).attr("data-link");
                
              // create the backdrop and wait for next modal to be triggered
              $('body').modalmanager('loading');
			  
			  // console.log($(this).attr("data-id"));
              
              //$.get('http://netcare.telkom.co.id/cacti/him_graph2.php?action=zoom&local_graph_id='+graphid+'&rra_id=5&view_type=&bypass=99030f464aa2e1780299fbd5c192ab04');
				//console.log('/mrtg/graph/indexdetail?ajaxview=true&graph='+graphid+'&cust='+cust+'&addr='+addr+'&sid='+sid+'&title='+encodeURIComponent(title));
              setTimeout(function(){
                  $modal.load('/mrtg/graph/indexdetailkaa?ajaxview=true&graph='+graphid+'&cust='+encodeURIComponent(cust)+'&addr='+encodeURIComponent(addr)+'&sid='+encodeURIComponent(sid)+'&title='+encodeURIComponent(title)+'&link='+link, '', function(){
                  $modal.modal();
                });
              }, 1000);
            });

            $('body').on('click', '#linkdatinsecurity .btn-graph', function() {
              var graphid =  $(this).attr("data-id");
              var cust =  $(this).attr("data-cust");
              var addr =  $(this).attr("data-addr");
              var sid =  $(this).attr("data-sid");
              var title =  $(this).attr("data-title");
              var link =  $(this).attr("data-link");
                
              // create the backdrop and wait for next modal to be triggered
              $('body').modalmanager('loading');
			  
			  // console.log($(this).attr("data-id"));
              
              //$.get('http://netcare.telkom.co.id/cacti/him_graph2.php?action=zoom&local_graph_id='+graphid+'&rra_id=5&view_type=&bypass=99030f464aa2e1780299fbd5c192ab04');
				//console.log('/mrtg/graph/indexdetail?ajaxview=true&graph='+graphid+'&cust='+cust+'&addr='+addr+'&sid='+sid+'&title='+encodeURIComponent(title));
              setTimeout(function(){
                  $modal.load('/mrtg/graph/indexdetailkaa?ajaxview=true&graph='+graphid+'&cust='+encodeURIComponent(cust)+'&addr='+encodeURIComponent(addr)+'&sid='+encodeURIComponent(sid)+'&title='+encodeURIComponent(title)+'&link='+link, '', function(){
                  $modal.modal();
                });
              }, 1000);
            });

            $('body').on('click', '#linkdatinvenue .btn-graph', function() {
              var graphid =  $(this).attr("data-id");
              var cust =  $(this).attr("data-cust");
              var addr =  $(this).attr("data-addr");
              var sid =  $(this).attr("data-sid");
              var title =  $(this).attr("data-title");
              var link =  $(this).attr("data-link");
                
              // create the backdrop and wait for next modal to be triggered
              $('body').modalmanager('loading');
			  
			  // console.log($(this).attr("data-id"));
              
              //$.get('http://netcare.telkom.co.id/cacti/him_graph2.php?action=zoom&local_graph_id='+graphid+'&rra_id=5&view_type=&bypass=99030f464aa2e1780299fbd5c192ab04');
				//console.log('/mrtg/graph/indexdetail?ajaxview=true&graph='+graphid+'&cust='+cust+'&addr='+addr+'&sid='+sid+'&title='+encodeURIComponent(title));
              setTimeout(function(){
                  $modal.load('/mrtg/graph/indexdetailkaa?ajaxview=true&graph='+graphid+'&cust='+encodeURIComponent(cust)+'&addr='+encodeURIComponent(addr)+'&sid='+encodeURIComponent(sid)+'&title='+encodeURIComponent(title)+'&link='+link, '', function(){
                  $modal.modal();
                });
              }, 1000);
            });

            $('body').on('click', '#portlet_ticket .btn-mod_ticket', function() {
				
				var layanan = $(this).attr("data-layanan");
				var id = $(this).attr("data-id");
				// console.log($(this).attr("data-toggle"));
              setTimeout(function(){
                  $modal.load('/ajaxmonitor/changestatusticket?layanan='+layanan+'&id='+id, '', function(){
                  $modal.modal();
                });
              }, 1000);
            });

            $modal.on('click', 'button#graphfilter', function(){
              $modal.modal('loading');
              var start = '';
              var time = '';
              if($modal.find('input#startdate')!=undefined && $modal.find('input#startdate').val()!=''){
                start = $modal.find('input#startdate').val()+':00';
                //alert(start);
                start = Date.parse(start)/1000;
                time += '&graph_start='+start;
              }
              
              var end = '';
              if($modal.find('input#enddate')!=undefined && $modal.find('input#enddate').val()!=''){
                end = $modal.find('input#enddate').val()+':00';
                //alert(end);
                end = Date.parse(end)/1000;
                time += '&graph_end='+end;
              }
              
              var graph = '';
              if($modal.find('input#graph')!=undefined && $modal.find('input#graph').val()!=''){
                var graph = $modal.find('input#graph').val();
                var bypass = $modal.find('input#bypass').val();
                //alert(graph);
              }
              
              //alert('graph='+graph+'&startdate='+start+'&enddate='+end);
              //url = '/mrtg/graph/detail?ajaxview=true&graph='+graph+'&startdate='+start+'&enddate='+end;
              //alert(url);
              var html = '';
              
              if(start!='' && end!=''){
				html = '<img src="http://telkomcare.telkom.co.id/libcacti/graph.php?action=zoom&local_graph_id='+graph+'&rra_id=5&view_type='+time+'&graph_height=120&graph_width=500&title_font_size=10" style="width:75%;padding:0px;margin:10px;"/>';
               //html = '<iframe style="width:100%;display:block;min-height:330px;border:none;" src="http://202.134.2.182/him_graph2.php?action=zoom&local_graph_id='+graph+'&rra_id=5&view_type='+time+'&bypass='+bypass+'" />';
              }else{
				html += '<img src="http://telkomcare.telkom.co.id/libcacti/graph.php?action=zoom&local_graph_id='+graph+'&rra_id=5&view_type='+time+'&graph_height=120&graph_width=500&title_font_size=10" style="width:75%;padding:0px;margin:10px;"/>';
				html += '<img src="http://telkomcare.telkom.co.id/libcacti/graph.php?action=zoom&local_graph_id='+graph+'&rra_id=1&view_type='+time+'&graph_height=120&graph_width=500&title_font_size=10" style="width:75%;padding:0px;margin:10px;"/>';
				html += '<img src="http://telkomcare.telkom.co.id/libcacti/graph.php?action=zoom&local_graph_id='+graph+'&rra_id=2&view_type='+time+'&graph_height=120&graph_width=500&title_font_size=10" style="width:75%;padding:0px;margin:10px;"/>';
				html += '<img src="http://telkomcare.telkom.co.id/libcacti/graph.php?action=zoom&local_graph_id='+graph+'&rra_id=3&view_type='+time+'&graph_height=120&graph_width=500&title_font_size=10" style="width:75%;padding:0px;margin:10px;"/>';
				html += '<img src="http://telkomcare.telkom.co.id/libcacti/graph.php?action=zoom&local_graph_id='+graph+'&rra_id=4&view_type='+time+'&graph_height=120&graph_width=500&title_font_size=10" style="width:75%;padding:0px;margin:10px;"/>';
               //html = '<iframe style="width:100%;display:block;min-height:330px;border:none;" src="http://202.134.2.182/him_graph2.php?action=zoom&local_graph_id='+graph+'&rra_id=5&view_type='+time+'&bypass='+bypass+'" /><iframe style="width:100%;display:block;min-height:323px;border:none;" src="http://10.62.8.126/cacti/him_graph2.php?action=zoom&local_graph_id='+graph+'&rra_id=1&view_type='+time+'&bypass='+bypass+'" /><iframe style="width:100%;display:block;min-height:323px;border:none;" src="http://10.62.8.126/cacti/him_graph2.php?action=zoom&local_graph_id='+graph+'&rra_id=2&view_type='+time+'&bypass='+bypass+'" /><iframe style="width:100%;display:block;min-height:323px;border:none;" src="http://10.62.8.126/cacti/him_graph2.php?action=zoom&local_graph_id='+graph+'&rra_id=3&view_type='+time+'&bypass='+bypass+'" /><iframe style="width:100%;display:block;min-height:323px;border:none;" src="http://10.62.8.126/cacti/him_graph2.php?action=zoom&local_graph_id='+graph+'&rra_id=4&view_type='+time+'&bypass='+bypass+'" />';
              }
              setTimeout(function(){
                $modal
                  .modal('loading')
                  .find('#framecontent').html(html)
                  /*.load(url, '', function(){
                     $modal.modal();
                   });*/
              }, 1000);
            });

            $modal.on('click', 'button#graphclear', function(){
              if($modal.find('input#startdate')!=undefined && $modal.find('input#startdate').val()!=''){
                $modal.find('input#startdate').val('');
              }
              
              if($modal.find('input#enddate')!=undefined && $modal.find('input#enddate').val()!=''){
                $modal.find('input#enddate').val('');
              }
            });

            $modal.on('click', 'button#modalclose', function(){
                $modal.modal('hide');
            });
            
            var $modal2 = $('#static');
            $('body').on('click', '#graphprint', function() {
				var graph = $('input.graphcheck:checked');
				if(graph.length>0){
					$modal2.modal();
				}else{
					alert('Please choose graph to print out.');
				}
            });

            $modal2.on('click', 'button#graphclear2', function(){
              if($modal2.find('input#startdate2')!=undefined && $modal2.find('input#startdate2').val()!=''){
                $modal2.find('input#startdate2').val('');
              }
              
              if($modal2.find('input#enddate2')!=undefined && $modal2.find('input#enddate2').val()!=''){
                $modal2.find('input#enddate2').val('');
              }
            });

            $modal2.on('click', 'button#printconfirm', function(e){
				// console.log($modal2);
				var graph = $('input.graphcheck:checked');
				// console.log($('input.graphcheck:checked'));
				// console.log($('#printform').find("input[type='hidden']"));
				hide = $modal2.find('#hiddeninput');
				//console.log(hide);
				$(hide[0]).html('');
				//console.log(hide);
				var list = {};
				for(var i=0;i<graph.length;i++){
					$(hide[0]).append('<input type="hidden" class="hiddeninput" name="check['+i+']" value="'+$(graph[i]).val()+'"/>');
					$(hide[0]).append('<input type="hidden" class="hiddeninput" name="title['+i+']" value="'+$(graph[i]).data('title')+'"/>');
					$(hide[0]).append('<input type="hidden" class="hiddeninput" name="sid['+i+']" value="'+$(graph[i]).data('sid')+'"/>');
					$(hide[0]).append('<input type="hidden" class="hiddeninput" name="cust['+i+']" value="'+$(graph[i]).data('cust')+'"/>');
					$(hide[0]).append('<input type="hidden" class="hiddeninput" name="addr['+i+']" value="'+$(graph[i]).data('addr')+'"/>');
					//list[i] = {'enc':$(graph[i]).data('id'),'dec':$(graph[i]).val()};
				}
				//console.log(list);
				 form = $modal2.find('form');				
				console.log(form[0]);
				$(form[0]).attr('action','/mrtg/public/printgraphs?random='+Math.random());
				$(form[0]).submit();
				e.preventDefault();
            });
            
			// console.log($(this).attr("data-parent_level"));
			
            $('body').on('click', '#content_chats .comments', function() {
              var parent_level =  $(this).attr("data-parent_level");
                
              // create the backdrop and wait for next modal to be triggered
              $('body').modalmanager('loading');
			  
			  // console.log($(this).attr("data-id"));
              
              //$.get('http://netcare.telkom.co.id/cacti/him_graph2.php?action=zoom&local_graph_id='+graphid+'&rra_id=5&view_type=&bypass=99030f464aa2e1780299fbd5c192ab04');
				//console.log('/mrtg/graph/indexdetail?ajaxview=true&graph='+graphid+'&cust='+cust+'&addr='+addr+'&sid='+sid+'&title='+encodeURIComponent(title));
              setTimeout(function(){
                  $modal.load('/ajaxmonitor/getcomments?parent_level='+parent_level, '', function(){
                  $modal.modal();
                });
              }, 1000);
            });

			
        }

    };

}();
