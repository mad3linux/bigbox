var Charts1 = function () {

return {
	//main function to initiate the module

	initCharts: function () {

		if (!jQuery.plot) {
			return;
		}

		function chart() {
			/*
			{
				color: color or number
				data: rawdata
				label: string
				lines: specific lines options
				bars: specific bars options
				points: specific points options
				xaxis: number
				yaxis: number
				clickable: boolean
				hoverable: boolean
				shadowSize: number
				highlightColor: color or number
			}
			*/
			
			console.log(topps);
			
			var dx1 = [] ;
			var data1 = [];
			var data2 = [];
			var axis = 2;
			var idx = 0;
			var hlabel = [];
			$.each(topps, function( index, value ) {
				data1[idx] = [axis,value.complaint];
				if(value.complaint<value.systemfault){
					data2[idx] = [axis,value.systemfault-value.complaint];
				}else{
					data2[idx] = [axis,value.systemfault];
				}
				hlabel[axis] = {
					systemfault:value.systemfault,
					complaint:value.complaint,
					name:value.name,
					img:"<img src='"+value.img+"' style='width:100%;margin-left:-20px;' alt='"+value.name+"'  title='"+value.name+"' />"
				};
				
				axis+=2;
				idx++;
			});
			
			dx1[0] = { 
				label: "Complaint", 
				data: data1,
				color: "rgb(255, 100, 123)",
				borderColor: "black",
				clickable: true,
				hoverable: true,
				highlightColor: "rgb(255, 100, 123)",
				//xaxes: [ { position: "top" } ],
				//yaxes: [ { }, { position: "right", min: 20 } ],
				/*bars: {
					show: true,
					//fill: false,
					barWidth: 0.5
				},*/
				points: {
					show: true,
					radius: 2
				}
			};
			dx1[1] = { 
				label: "System Fault", 
				data: data2,
				color: "rgb(245, 225, 0)",
				clickable: true,
				hoverable: true,
				highlightColor: "rgb(245, 225, 0)",
				//xaxes: [ { position: "top" } ],
				//yaxes: [ { }, { position: "right", min: 20 } ],
				/*bars: {
					show: true,
					//fill: false,
					barWidth: 0.5
				},*/
				points: {
					show: true,
					radius: 2
				}
			};
			
			$("<div id='tooltip'></div>").css({
				position: "absolute",
				display: "none",
				border: "1px solid #fdd",
				padding: "2px",
				"background-color": "#fee",
				opacity: 0.80
			}).appendTo("body");
			
			function plotWithOptions() {
				$.plot($("#chart"), dx1, {
					series: {
						stack: true,
						bars: {
							show: true,
							fill: true,
							barWidth: 1,
							align:"center"
						},
					},
					grid: {
						hoverable: true,
						clickable: true,
					},						
					legend: {
						show: true,
						labelFormatter: function(label, series) {
							// series is the series object for the label
							return '<a href="#' + label + '">' + label + '</a>';
						},
						//labelBoxBorderColor: color
						//noColumns: number
						//position: "ne" or "nw" or "se" or "sw"
						//margin: 10,
						//backgroundColor: "red",
						//backgroundOpacity: number between 0 and 1
						//container: $('#chartlbl'),
						sorted: "ascending"
					},
					xaxis: {
						//show: false
						//position: "bottom" or "top" or "left" or "right"
						//mode: null or "time" ("time" requires jquery.flot.time.js plugin)
						//timezone: null, "browser" or timezone (only makes sense for mode: "time")

						//color: null or color spec
						//tickColor: null or color spec
						//font: null or font spec object

						min: 1,
						max: 41,
						//autoscaleMargin: null or number

						//transform: null or fn: number -> number
						//inverseTransform: null or fn: number -> number

						/*ticks: [
							'', [ Math.PI/2, "www" ], [ Math.PI, "qqq" ],
							[ Math.PI * 3/2, "fff" ], [ Math.PI * 2, "ddd" ]
						]*/
						tickSize: 2,
						//minTickSize: 0,
						tickFormatter: function formatter(val, axis) {
							if(val>=2 && val<=40){
								console.log(hlabel[val]);
								return hlabel[val].img;//"<span style=''>"+hlabel[val].name+"</span>";
							}else{
								return "";//val.toFixed(axis.tickDecimals);
							}
						},
						//tickDecimals: null or number

						//labelWidth: null or number
						//labelHeight: null or number
						//reserveSpace: null or true

						//tickLength: null or number

						//alignTicksWithAxis: 5
					},
					yaxis: {
						/*tickFormatter: function formatter(val, axis) {
							if(val<0){
								return val-(2*val);
							}else{
								return val.toFixed();
							}
						}*/
					}
				});
			}
			
			$("#chart").bind("plotclick", function (event, pos, item) {
				//alert("You clicked at " + pos.x + ", " + pos.y);
				//console.log(event);
				//console.log(pos);
				//console.log(item);
				// axis coordinates for other axes, if present, are in pos.x2, pos.x3, ...
				// if you need global screen coordinates, they are pos.pageX, pos.pageY

				if (item) {
					//highlight(item.series, item.datapoint);
					//alert("You clicked a point!");
				}
			});
			var previousPoint = null;
			$("#chart").bind("plothover", function (event, pos, item) {
				if (item) {
					var x = item.datapoint[0];//.toFixed(2),
						y = item.datapoint[1];//.toFixed(2);

					$("#tooltip").html("<b>"+hlabel[x].name+"</b><br><b>"+hlabel[x].complaint+"</b> Complaint<br><b>"+hlabel[x].systemfault+"</b> System Fault")
						.css({top: item.pageY-50, left: item.pageX-50})
						.fadeIn(200);
				} else {
					$("#tooltip").hide();
				}
			});

			plotWithOptions();
		}

		//graph
		chart();
	},

};

}();
