var UITree = function () {
	var items = [];
	items.F1 = [
         { name: 'Main Projects<div class="tree-actions"><i class="fa fa-plus"></i><i class="fa fa-trash-o"></i><i class="fa fa-refresh"></i></div>', type: 'folder', additionalParameters: { id: 'F11' } },
         { name: '<i class="fa fa-user"></i> Member <div class="tree-actions"><i class="fa fa-plus"></i><i class="fa fa-trash-o"></i><i class="fa fa-refresh"></i></div><div class="tree-actions"><i class="fa fa-plus"></i><i class="fa fa-trash-o"></i><i class="fa fa-refresh"></i></div>', type: 'item', additionalParameters: { id: 'I11' } },
         { name: '<i class="fa fa-calendar"></i> Events <div class="tree-actions"><i class="fa fa-plus"></i><i class="fa fa-trash-o"></i><i class="fa fa-refresh"></i></div>', type: 'item', additionalParameters: { id: 'I12' } },
         { name: '<i class="fa fa-suitcase"></i> Portfolio <div class="tree-actions"><i class="fa fa-plus"></i><i class="fa fa-trash-o"></i><i class="fa fa-refresh"></i></div>', type: 'item', additionalParameters: { id: 'I12' } }
     ];
	//console.log(items);
	return {
        //main function to initiate the module
        init: function () {

            var DataSourceTree = function (options) {
                this._data  = options.data;
                this._delay = options.delay;
            };

            DataSourceTree.prototype = {

                data: function (options, callback) {
                	console.log(options);
                	//console.log(callback);
                    var self = this;
                    //var data = $.extend(true, [], self._data);

                    setTimeout(function () {
                    	//console.log(self);
                    	if(options.type!=undefined && options.type=='folder'){
                    		var data = $.extend(true, [], items[options.id]);
                    	}else{
                    		var data = $.extend(true, [], self._data);
                    	}
                    	//console.log(data);

                        callback({ data: data });

                    }, this._delay)

                    //callback({ data: data });
                }
            };
            
// INITIALIZING TREE
var treeDataSource = new DataSourceTree({
	data: [
	        { name: 'Test Folder 1', type: 'folder', id: 'F1' },
	        { name: 'Test Folder 2', type: 'folder', id: 'F2' },
	        { name: 'Test Item 1', type: 'item', id: 'I1' },
	        { name: 'Test Item 2', type: 'item', id: 'I2' }
	      ],
    delay: 400
});

            $('#MyTree').tree({
                selectable: false,
                dataSource: treeDataSource,
                loadingHTML: '<img src="/assets/core/img/input-spinner.gif"/>',
            });
            
            $('#MyTree').on('loaded.fu.tree', function (e) {
        		console.log('Loaded');
        	});

        	$('#MyTree').on('selected.fu.tree', function (e, selected) {
        		console.log('Select Event: ', selected);
        		console.log($('#MyTree').tree('selectedItems'));
        	});

        	$('#MyTree').on('updated.fu.tree', function (e, selected) {
        		console.log('Updated Event: ', selected);
        		console.log($('#MyTree').tree('selectedItems'));
        	});

        	$('#MyTree').on('opened.fu.tree', function (e, info) {
        		console.log('Open Event: ', info);
        		return false;
        	});

        	$('#MyTree').on('closed.fu.tree', function (e, info) {
        		console.log('Close Event: ', info);
        	});
        }

    };

}();