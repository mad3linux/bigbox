var map;
var marks = {};
var marks4 = {};
var markarr1 = {};
var markarr3 = {};
var markarr4 = {};
var Gmaps1 = function () {
	
var mapInit = function () {
	
	var slat = -2.504085513748616;
	var slng = 116.32324193750006
	var szoom = 5;
	$.each(ddwitel,function(key,val){
		if(val.witel=="JAWA TENGAH"){			
			slat = val.locn_x;
			slng = val.locn_y;
			szoom = val.zoom;
		}
	});
	
    map = new GMaps({
		div: '#gmap1',
		// indonesia
		// lat: -2.504085513748616,
		// lng: 116.32324193750006,
		// zoom:5,
		
		// jawa tengah
		lat: slat,
		lng: slng,
		zoom: szoom,
		click: function(e) {
			//console.log(e);
		},
		dragend: function(e) {
			//console.log(e);
			//console.log(map.getCenter());
		}
		});
	
	map.addControl({
	  position: 'top_right',
	  content: 'Go to center',
	  style: {
		margin: '5px 3px',
		padding: '1px 6px',
		border: 'solid 1px #717B87',
		background: '#fff',
		'font-size': '11px'
	  },
	  events: {
		click: function(){
			select = $('#controlcenter');
			opt = $('#opt'+select.val());
			map.setCenter(parseFloat(opt.data('lat')),parseFloat(opt.data('lng')));
			map.setZoom(parseInt(opt.data('zoom')));
			map.refresh();
		}
	  }
	});
	
	var ddctn = '<select placeholder="center" id="controlcenter">';
	$.each(ddwitel,function(key,val){
		// console.log(key);
		if(val.witel=="INDONESIA"){
			ddctn += '<option value="'+val.wil_id+key+'" id="opt'+val.wil_id+key+'" data-zoom="5" data-lat="'+val.locn_x+'" data-lng="'+val.locn_y+'">'+val.witel+'</option>';
		}else{
			var slt = ""
			if(val.witel=="JAWA TENGAH"){
				slt = "selected";
			}
			ddctn += '<option value="'+val.wil_id+key+'" id="opt'+val.wil_id+key+'" data-zoom="'+val.zoom+'" data-lat="'+val.locn_x+'" data-lng="'+val.locn_y+'" '+slt+'>'+val.witel+'</option>';
		}
	});
	ddctn += '</select>';
	
	map.addControl({
	  position: 'top_right',
		content: ddctn,
	  style: {
		margin: '6px 3px',
		padding: '0px',
		border: '0px',
		background: '#fff',
		'font-size': '11px'
	  },
	  events: {
		change: function(e){			
			select = $('#controlcenter');
			opt = $('#opt'+select.val());
			
			
			reportfilter();
			
			
			map.setCenter(parseFloat(opt.data('lat')),parseFloat(opt.data('lng')));
			map.setZoom(parseInt(opt.data('zoom')));
			map.refresh();
		}
	  }
	});
	
    }
		
var mapInit2 = function (mark3) {
	
	
	$.each(markarr3, function( index, value ) {
		markarr3[index].setMap(null);
	});
	
	if(mark3!=null && mark3!=""){
		
		for(var i = 0; i<100;i++){
		// markarr3[i].setMap(null);
		if(mark3[i].LATITUDE != undefined && mark3[i].LONGITUDE != undefined){
			markarr3[i] = map.addMarker({
				lat: parseFloat(mark3[i].LATITUDE),
				lng: parseFloat(mark3[i].LONGITUDE),
				//title: mark1[i].desc,
				title: '[@'+ mark3[i].USERID+']\n'+mark3[i].TWEET,
				icon: {
					url: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|F00000',
					scaledSize:new google.maps.Size(10,15)
				},
				details: {
					userid:mark3[i].USERID,
					id: 'm2'+i,
				},
				click: function (e) {
				// console.log(e);
				// console.log(e.details.report_id);			
				}
			});
		}
	}
		
	}
	

	
}
		
var mapInit3 = function (marks) {
	
	// console.log(console.log(marks));
	
	$.each(markarr1, function( index, value ) {
		markarr1[index].setMap(null);
	});
	
	if(marks!=null && marks!=""){
		
		for(var i = 0; i<marks.length;i++){
			// markarr1[i].setMap(null);
			
			if(marks[i].status=="3"){
				var sts_i = "darurat_"+marks[i].disaster_name;
			} else if(marks[i].status=="2"){
				var sts_i = "siaga_"+marks[i].disaster_name;
			} else if(marks[i].status=="1"){
				var sts_i = "aman_"+marks[i].disaster_name;
			} else{
				var sts_i = "white_fgreen";
			}
			
			if(marks[i].lat != undefined && marks[i].lng != undefined){
				markarr1[i] = map.addMarker({
					lat: parseFloat(marks[i].lat),
					lng: parseFloat(marks[i].lng),
					//title: marks[i].desc,
					title: '['+marks[i].witel_name+', '+marks[i].divre_name+']\n'+marks[i].description,
					icon: {
						url: '/images/ICON/BENCANA/'+sts_i+'.png',
						scaledSize:new google.maps.Size(15,15)
					},
					details: {
						userid:marks[i].USERID,
						id: 'm2'+i,
					},
					click: function (e) {
					// console.log(e);
					// console.log(e.details.report_id);			
					}
				});
			}
		}
	}

	
}
		
var mapInit4 = function (marks4) {
	
	// console.log(marks4);
	
	$.each(markarr4, function( index, value ) {
		markarr4[index].setMap(null);
	});
	
	if(marks4!=null && marks4!=""){
		
		for(var i = 0; i<marks4.length;i++){
			// markarr4[i].setMap(null);
			
			if(marks4[i].status=="3"){
				var sts_i = "darurat_"+marks4[i].disaster_name;
			} else if(marks4[i].status=="2"){
				var sts_i = "siaga_"+marks4[i].disaster_name;
			} else if(marks4[i].status=="1"){
				var sts_i = "aman_"+marks4[i].disaster_name;
			} else{
				var sts_i = "white_fgreen";
			}
			
			if(marks4[i].lat != undefined && marks4[i].lng != undefined){
				markarr4[i] = map.addMarker({
					lat: parseFloat(marks4[i].lat),
					lng: parseFloat(marks4[i].lng),
					//title: marks4[i].desc,
					title: '['+marks4[i].witel_name+', '+marks4[i].divre_name+']\n'+marks4[i].description,
					icon: {
						url: '/images/ICON/BENCANA/'+sts_i+'.png',
						scaledSize:new google.maps.Size(15,15)
					},
					details: {
						userid:marks4[i].USERID,
						id: 'm2'+i,
					},
					click: function (e) {
					// console.log(e);
					// console.log(e.details.report_id);			
					}
				});
			}
		}
	}
		
	

	
}


return {
	//main function to initiate map samples
	init: function () {
		$('#gmap1').each(function () {
			this.style.setProperty( 'height', $('#content').height()+"px", 'important' );
		});
		mapInit();
		mainmap = map.map;
		mapcenter = mainmap.getCenter();
		mapzoom = mainmap.getZoom();
	},
	
	init2: function(mark3){
		// console.log(mark3);
		mapInit2(mark3);
		
	},
	
	init3: function(marks){
		// console.log(marks);
		mapInit3(marks);
		
	},
	
	init4: function(marks4){
		// console.log(marks);
		mapInit4(marks4);
		
	},
	
	maprefresh : function(){
		if(map!=undefined && map!={}){
			map.refresh();
		}
	},
	
	maptoggle : function(){
		if(map!=undefined && map!={}){
			var select = $('body').find('#controlcenter');
			var opt = $('#opt'+select.val());
			// console.log(mainmap.getCenter());
			mapzoom = parseInt(opt.data('zoom'));
			mainmap.setZoom(mapzoom);
			var ltd = parseFloat(opt.data('lat'));
			var lnd = parseFloat(opt.data('lng'));
			mainmap.setCenter(new google.maps.LatLng(ltd, lnd));
			mapcenter = mainmap.getCenter();
			mapzoom = mainmap.getZoom();
			// console.log(mapzoom);
			map.refresh();
		}
	}

};

}();

