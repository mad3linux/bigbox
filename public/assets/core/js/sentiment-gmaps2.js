var xhr1 = null;
var markarr1 = {};
var markarr2 = {};
var markarr3 = {};
var layerwitel;
var el1 = jQuery("#rightTable").first();
var infomark = '';

function hideMarker2(witel){
	//console.log(witel);
	//console.log(markarr2);
	$.each(markarr2, function( index, value ) {
	//console.log(index);
	//console.log(value);
		if(index==witel){
			$.each(value, function( index2, value2 ) {
				value2.setVisible(true);
			});
		}else{
			$.each(value, function( index2, value2 ) {
				value2.setVisible(false);
			});
		}
	});
}

function showWitelMarker(witel){
	//console.log(witel);
	//console.log(markarr2);
	$.each(markarr1, function( index, value ) {
	// console.log(index);
	// console.log(value);
	// console.log(value.details);
		if(witel==""){
			value.setVisible(true);
		}else if(value.details.wit_id==witel){
			value.setVisible(true);
		}else{
			value.setVisible(false);
		}
	});
}

function showWitelMarker2(witel,status,dist_id){
	//console.log(witel);
	//console.log(markarr2);
	$.each(markarr1, function( index, value ) {
	// console.log(index);
	// console.log(value);
	// console.log(value.details);
		if(witel=="" && status=="" && dist_id==""){
			value.setVisible(true);
		}else if(witel=="" && status=="" && value.details.dist_id==dist_id){
			value.setVisible(true);
		}else if(witel=="" && value.details.status==status && dist_id==""){
			value.setVisible(true);
		}else if(witel=="" && value.details.status==status && value.details.dist_id==dist_id){
			value.setVisible(true);
		}else if(value.details.wit_id==witel && status=="" && dist_id==""){
			value.setVisible(true);
		}else if(value.details.wit_id==witel && status=="" && value.details.dist_id==dist_id){
			value.setVisible(true);
		}else if(value.details.wit_id==witel && value.details.status==status && dist_id==""){
			value.setVisible(true);
		}else if(value.details.wit_id==witel && value.details.status==status && value.details.dist_id==dist_id){
			value.setVisible(true);
		}else{
			value.setVisible(false);
		}
	});
}

var map;
var map2;

var Gmaps1 = function () {
	
var mapInit = function (mark1,mark2) {
	// console.log(mark3);

	map = new GMaps({
		div: '#gmap1',
		// indonesia
		// lat: -2.504085513748616,
		// lng: 116.32324193750006,
		
		// jawa tengah
		lat: -7.007842,
		lng: 110.460062,
		zoom:11,
		click: function(e) {
			//console.log(e);
		},
		dragend: function(e) {
			//console.log(e);
			//console.log(map.getCenter());
		}
	});
        
	map.addControl({
	  position: 'top_right',
	  content: 'Go to center',
	  style: {
		margin: '5px 3px',
		padding: '1px 6px',
		border: 'solid 1px #717B87',
		background: '#fff',
		'font-size': '11px'
	  },
	  events: {
		click: function(){
			select = $('#controlcenter');
			opt = $('#opt'+select.val());
			map.setCenter(parseFloat(opt.data('lat')),parseFloat(opt.data('lng')));
			map.setZoom(parseInt(opt.data('zoom')));
			map.refresh();
		}
	  }
	});
	
	var ddctn = '<select placeholder="center" id="controlcenter">';
	$.each(ddwitel,function(key,val){
		/*if(val.witel=="INDONESIA"){
			ddctn += '<option value="'+val.wil_id+'" id="opt'+val.wil_id+'" data-zoom="5" data-lat="'+val.locn_x+'" data-lng="'+val.locn_y+'" selected>'+val.witel+'</option>';
		}*/
		if(val.witel=="JAWA TENGAH"){
			ddctn += '<option value="'+val.wil_id+'" id="opt'+val.wil_id+'" data-zoom="11" data-lat="'+val.locn_x+'" data-lng="'+val.locn_y+'" selected>'+val.witel+'</option>';
		}else{
			ddctn += '<option value="'+val.wil_id+'" id="opt'+val.wil_id+'" data-zoom="'+val.zoom+'" data-lat="'+val.locn_x+'" data-lng="'+val.locn_y+'">'+val.witel+'</option>';
		}
	});
	ddctn += '</select>';
	
	map.addControl({
	  position: 'top_right',
	  /*content: '<select placeholder="center" id="controlcenter">'+
					'<option value="1" id="opt1" data-zoom="5" data-lat="-2.504085513748616" data-lng="116.32324193750006">Indonesia</option>'+
					'<option value="2" id="opt2" data-zoom="6" data-lat="-0.26367094433665017" data-lng="102.36373901367188">Sumatera</option>'+
					'<option value="3" id="opt3" data-zoom="7" data-lat="-7.536764322084078" data-lng="109.65866088867188">Jawa</option>'+
					'<option value="4" id="opt4" data-zoom="6" data-lat="1.4394488164139768" data-lng="113.52584838867188">Kalimantan</option>'+
					'<option value="5" id="opt5" data-zoom="7" data-lat="-2.3284603685731593" data-lng="120.95260620117188">Sulawesi</option>'+
					'<option value="6" id="opt6" data-zoom="7" data-lat="-8.7113588754265" data-lng="118.35983276367188">Bali Nusra</option>'+
					'<option value="7" id="opt7" data-zoom="6" data-lat="-3.2743089918452106" data-lng="131.19186401367188">Maluku Papua</option>'+
				'</select>',*/
		content: ddctn,
	  style: {
		margin: '6px 3px',
		padding: '0px',
		border: '0px',
		background: '#fff',
		'font-size': '11px'
	  },
	  events: {
		change: function(e){
			select = $('#controlcenter');
			opt = $('#opt'+select.val());
			map.setCenter(parseFloat(opt.data('lat')),parseFloat(opt.data('lng')));
			map.setZoom(parseInt(opt.data('zoom')));
			map.refresh();
			
			// console.log(select.val());
			// var reportlist = $('body').find('.reportlist');
		var cond = $("#cond1").val();
		var dist = $("#disaster1").val();
		var wit = select.val();
			var reportlist = $('.reportlist');
			// console.log(reportlist);
			if(reportlist.length>0){
				// console.log(reportlist);
				$.each(reportlist,function(k,v){
					// console.log($(v).attr("class"));
					
					if(cond=="" && wit=="" && dist==""){
						$(v).show();
					}else if(cond=="" && wit=="" && dist!="" && $(v).hasClass("report-dist"+dist)){
						$(v).show();
					}else if(cond=="" && wit!="" && dist=="" && $(v).hasClass("report-wit"+wit)){
						$(v).show();
					}else if(cond=="" && wit!="" && dist!="" && $(v).hasClass("report-dist"+dist) && $(v).hasClass("report-wit"+wit)){
						$(v).show();
					}else if(cond!="" && wit=="" && dist=="" && $(v).hasClass("report-cond"+cond)){
						$(v).show();
					}else if(cond!="" && wit=="" && dist!="" && $(v).hasClass("report-cond"+cond) && $(v).hasClass("report-dist"+dist)){
						$(v).show();
					}else if(cond!="" && wit!="" && dist=="" && $(v).hasClass("report-cond"+cond) && $(v).hasClass("report-wit"+wit)){
						$(v).show();
					}else if(cond!="" && wit!="" && dist!="" && $(v).hasClass("report-cond"+cond) && $(v).hasClass("report-wit"+wit) && $(v).hasClass("report-dist"+dist)){
						$(v).show();
					}else{
						$(v).hide();
					}
				});
			}
			
			var jqxhr = $.getJSON( '/sentiment/getmaplegend/wil_id/'+select.val(), function(data) {
				// console.log(data);
				var total = 0;
				$.each(data,function(key,val){
					if(val.STATUS=="AMAN"){
						$("#legend_abc").html(val.jumlah);
					}else if(val.STATUS=="SIAGA"){
						$("#legend_s").html(val.jumlah);
					}else if(val.STATUS=="DARURAT"){
						$("#legend_d").html(val.jumlah);
					}
					total+=val.jumlah;
				});
				$("#legend_bb").html(total);
				showWitelMarker(select.val());
			}).done(function() {
				// console.log( "second success" );
			}).fail(function() {
				// App.unblockUI(el1);
			}).always(function() {
				// console.log( "complete" );
			}).complete(function() {
				// console.log( "second complete" );
			});
		}
	  }
	});
	

	
	for(var i = 0; i<mark1.length;i++){
	//console.log(mark1[i]);
	//console.log(mark1[i].img);
	var bencana = mark1[i].disaster;
	var status = mark1[i].img;
	
	if(status != "aman" && bencana != "aman"){
	
	//console.log(bencana);
	if (bencana==""){
		var url= '/images/ICON/CIRCLE/circle_b'+status+'.png';
	} else {
		var url = '/images/ICON/BENCANA/'+status+'_'+bencana+'.png';		
	} 
	
		markarr1[i] = map.addMarker({
			lat: parseFloat(mark1[i].lat),
			lng: parseFloat(mark1[i].lng),
			//title: mark1[i].desc,
			title: '['+ mark1[i].witel_name+'] ',
			icon: {
				url:url,
				//url: '/images/ICON/CIRCLE/circle_b'+mark1[i].img+'.png',
				scaledSize:new google.maps.Size(15,15)
			},
			/*infoWindow: {
				//content: '<span style="color:#000">'+mark1[i].nama_witel+'</span>'
				content : '<div class="table-responsive"><table class="table table-striped table-bordered table-hover summary" ><thead><tr><th colspan="4">'+mark1[i].nama_witel+'</th></tr><tr><th><img src="/images/ICON/NODE-B/node_bwhite_fgrey.png" style="width:20px"/><br>U | D | Un | Tot.</th><th><img src="/images/ICON/METRO/metro_bwhite_fgrey.png" style="width:20px"/><br>U | D | Un | Tot.</th><th><img src="/images/ICON/LAMBDA/lambda_bwhite_fgrey.png" style="width:20px"/><br>U | D | Un | Tot.</th><th><img src="/images/ICON/TRAFFIC/traffic_bwhite_fgrey.png" style="width:20px"/><br>U | D | Un | Tot.</th></tr></thead></table></div>'
			},*/
			details: {
				report_id:mark1[i].report_id,
				markid:'mark1'+i,
				idx: mark1[i].id,
				lat: parseFloat(mark1[i].lat),
				lng: parseFloat(mark1[i].lng),
				title: mark1[i].disaster+" "+mark1[i].witel_name,
				url: mark1[i].url,
				wit_id: parseInt(mark1[i].wit_id),
				// sto_condition: parseInt(mark1[i].sto_condition),
				// marker: parseInt(mark1[i].marker),
				status: parseInt(mark1[i].status_witel),
				dist_id: parseInt(mark1[i].disaster_id),
				zoom: parseInt(mark1[i].zoom),
				img: mark1[i].img
			},
			click: function (e) {
			//console.log(e);
			// console.log(e.details.report_id);
			if(e.details.report_id!='' && e.details.report_id!=null && e.details.report_id!='null'){
				location.href="/sentiment/infobencana/id/"+e.details.report_id;
			}
			}
		});	
	}
	}
	
	window.onresize = function(event) {
		// $('#gmap1,#gmap2,#rightTable').css({"height":($('#content').height()-100)+"px !important"});
		$('#gmap1,#gmap2,#liiis').each(function () {
			this.style.setProperty( 'height', $('#content').height()+"px", 'important' );
		});
		// mapcenter = map.getCenter();
		// mapzoom = map.getZoom();
		map.refresh();
	};
		
	App.addResponsiveHandler(function () {
		// $('#gmap1,#gmap2,#rightTable').css({"height":($('#content').height()-100)+"px !important"});
		$('#gmap1,#gmap2,#liiis').each(function () {
			this.style.setProperty( 'height', $('#content').height()+"px", 'important' );
		});
		// mapcenter = map.getCenter();
		// mapzoom = map.getZoom();
		map.refresh();
	});
	
	$('#gmap2parent').hide();
	//console.log(markarr1);
	//console.log(markarr2);
	//console.log(mark1);
	//console.log(mark2);
}

	
var mapInit2 = function (mark3) {
	
	// console.log(markarr3);
	
	$.each(markarr3, function( index, value ) {
		markarr3[index].setMap(null);
	});
		
	for(var i = 0; i<100;i++){
		// markarr3[i].setMap(null);
		if(mark3[i].LATITUDE != undefined && mark3[i].LONGITUDE != undefined){
			markarr3[i] = map.addMarker({
				lat: parseFloat(mark3[i].LATITUDE),
				lng: parseFloat(mark3[i].LONGITUDE),
				//title: mark1[i].desc,
				title: '[@'+ mark3[i].USERID+']\n'+mark3[i].TWEET,
				icon: {
					url: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|F00000',
					scaledSize:new google.maps.Size(10,15)
				},
				details: {
					userid:mark3[i].USERID,
					id: 'm2'+i,
				},
				click: function (e) {
				console.log(e);
				// console.log(e.details.report_id);			
				}
			});
		}
	}

	
}

return {
	//main function to initiate map samples
	init: function (mark1,mark2) {
		// $('#gmap1,#gmap2,#rightTable').css({"height":($('#content').height()-100)+"px !important"});
		$('#gmap1,#gmap2,#liiis').each(function () {
			this.style.setProperty( 'height', $('#content').height()+"px", 'important' );
		});
		mapInit(mark1,mark2);
		mainmap = map.map;
		mapcenter = mainmap.getCenter();
		mapzoom = mainmap.getZoom();
	},
	
	init2: function(mark3){
		// console.log(mark3);
		mapInit2(mark3);
		
	},
	
	maprefresh : function(){
		if(map!=undefined && map!={}){
			//select = $('body').find('#controlcenter').first();
			//opt = $('body').find('#opt'+select.val()).first();
			//map.setCenter(parseFloat(opt.data('lat')),parseFloat(opt.data('lng')));
			//map.setZoom(parseInt(opt.data('zoom')));
			map.refresh();
		}
	},
	
	maptoggle : function(){
		if(map!=undefined && map!={}){
			var select = $('body').find('#controlcenter');
			var opt = $('#opt'+select.val());
			// console.log(mainmap.getCenter());
			mapzoom = parseInt(opt.data('zoom'));
			// if(mapzoom<6){
				// mapzoom = 6;
			// }
			// if($("#maaap").hasClass("col-md-7")){
				// mainmap.setZoom(5);
			// }else{
				mainmap.setZoom(mapzoom);
			// }
			// mainmap.setCenter(parseFloat(opt.data('lat')),parseFloat(opt.data('lng')));
			var ltd = parseFloat(opt.data('lat'));
			var lnd = parseFloat(opt.data('lng'));
			mainmap.setCenter(new google.maps.LatLng(ltd, lnd));
			mapcenter = mainmap.getCenter();
			mapzoom = mainmap.getZoom();
			// console.log(mapzoom);
			map.refresh();
		}
	}

};

}();

function showOverlay1(lat,lon,data){
	if(infomark!=''){
		//map2.removeOverlay(infomark);
		map2.hideInfoWindows();
		infomark.setVisible(false);
	}
	
	infomark = map2.addMarker({
			lat: parseFloat(lon),
			lng: parseFloat(lat),
			title: data[0],
			/*icon: {
				url: '/images/ICON/CIRCLE/circle_b'+mark2[i].img+'.png',
				scaledSize:new google.maps.Size(15,15)
			},*/
			infoWindow: {
				//content: '<span style="color:#000">'+mark1[i].nama_witel+'</span>'
				content : '<div class="overlay"><b>Type : </b><br>'+data[0]+'<br><b>Name : </b><br>'+data[1]+'<br><b>Status : </b><br>'+data[2]+'<br><b>Comment : </b><br>'+data[3]+'<br></div>'
			},
			details: data,
			click: function (e) {
				//map2.refresh();
				//removeMarkers
				map2.setZoom(10);
			}
	});
	
	/*infomark = map2.drawOverlay({
	  lat: lon,
	  lng: lat,
	  content: '<div class="overlay">Lima</div>'
	});*/
	
	new google.maps.event.trigger( infomark, 'click' );
	map2.setCenter(lon,lat);
	map2.setZoom(16);
	map2.refresh();
	//map2.refresh();
}

function showOverlay2(lat,lon,data){
	if(infomark!=''){
		//map2.removeOverlay(infomark);
		map2.hideInfoWindows();
		infomark.setVisible(false);
	}
	
	infomark = map2.addMarker({
			lat: parseFloat(lon),
			lng: parseFloat(lat),
			title: data[0],
			/*icon: {
				url: '/images/ICON/CIRCLE/circle_b'+mark2[i].img+'.png',
				scaledSize:new google.maps.Size(15,15)
			},*/
			infoWindow: {
				//content: '<span style="color:#000">'+mark1[i].nama_witel+'</span>'
				content : '<div class="overlay"><b>Name : </b><br>'+data[0]+'<br><b>NIK : </b><br>'+data[1]+'<br><b>Address : </b><br>'+data[2]+'<br><b>Status : </b><br>'+data[3]+'<br><b>Comment : </b><br>'+data[4]+'<br></div>'
			},
			details: data,
			click: function (e) {
				//map2.refresh();
				//removeMarkers
				//if(infomark!=''){
					//map2.removeOverlay(infomark);
					//map2.hideInfoWindows();
					//infomark.setVisible(false);
					map2.setZoom(10);
				//}
			}
	});
	
	/*infomark = map2.drawOverlay({
	  lat: lon,
	  lng: lat,
	  content: '<div class="overlay">Lima</div>'
	});*/
	
	new google.maps.event.trigger( infomark, 'click' );
	map2.setCenter(lon,lat);
	map2.setZoom(16);
	map2.refresh();
	//map2.refresh();
}

function maprefresh(){

map.refresh();

}

