var Gmaps1 = function () {
	
var mapInit = function (mark1,mark2) {
	var map = new GMaps({
		div: '#gmap1',
		lat: -2.504085513748616,
		lng: 116.32324193750006,
		zoom:5,
		click: function(e) {
			//console.log(e);
		},
		dragend: function(e) {
			//console.log(e);
			//console.log(map.getCenter());
		}
	});
        
	map.addControl({
	  position: 'top_right',
	  content: 'Go to center',
	  style: {
		margin: '5px 3px',
		padding: '1px 6px',
		border: 'solid 1px #717B87',
		background: '#fff',
		'font-size': '11px'
	  },
	  events: {
		click: function(){
			select = $('#controlcenter');
			opt = $('#opt'+select.val());
			map.setCenter(parseFloat(opt.data('lat')),parseFloat(opt.data('lng')));
			map.setZoom(parseInt(opt.data('zoom')));
			map.refresh();
		}
	  }
	});
	
	map.addControl({
	  position: 'top_right',
	  content: '<select placeholder="center" id="controlcenter">'+
					'<option value="1" id="opt1" data-zoom="5" data-lat="-2.504085513748616" data-lng="116.32324193750006">Indonesia</option>'+
					'<option value="2" id="opt2" data-zoom="6" data-lat="-0.26367094433665017" data-lng="102.36373901367188">Sumatera</option>'+
					'<option value="3" id="opt3" data-zoom="7" data-lat="-7.536764322084078" data-lng="109.65866088867188">Jawa</option>'+
					'<option value="4" id="opt4" data-zoom="6" data-lat="1.4394488164139768" data-lng="113.52584838867188">Kalimantan</option>'+
					'<option value="5" id="opt5" data-zoom="7" data-lat="-2.3284603685731593" data-lng="120.95260620117188">Sulawesi</option>'+
					'<option value="6" id="opt6" data-zoom="7" data-lat="-8.7113588754265" data-lng="118.35983276367188">Bali Nusra</option>'+
					'<option value="7" id="opt7" data-zoom="6" data-lat="-3.2743089918452106" data-lng="131.19186401367188">Maluku Papua</option>'+
				'</select>',
	  style: {
		margin: '6px 3px',
		padding: '0px',
		border: '0px',
		background: '#fff',
		'font-size': '11px'
	  },
	  events: {
		change: function(e){
			select = $('#controlcenter');
			opt = $('#opt'+select.val());
			map.setCenter(parseFloat(opt.data('lat')),parseFloat(opt.data('lng')));
			map.setZoom(parseInt(opt.data('zoom')));
			map.refresh();
		}
	  }
	});
	
	var map2 = new GMaps({
		div: '#gmap2',
		lat: -2.504085513748616,
		lng: 116.32324193750006,
		zoom:7,
		click: function(e) {
			//console.log(e);
		},
		dragend: function(e) {
			//console.log(e);
			//console.log(map.getCenter());
		}
	});
	
	map2.addControl({
	  position: 'top_right',
	  content: 'Main Map',
	  style: {
		margin: '5px',
		padding: '1px 6px',
		border: 'solid 1px #717B87',
		background: '#fff',
		'font-size': '11px'
	  },
	  events: {
		click: function(){
			currentmark1 = null;
			$('#gmap2').hide();
			$('#gmap1').show();
			map.refresh();
		}
	  }
	});
	
	//console.log(mark1);
	
	for(var i = 0; i<mark1.length;i++){
		map.addMarker({
			lat: parseFloat(mark1[i].lat),
			lng: parseFloat(mark1[i].lng),
			title: mark1[i].desc,
			icon: {
				url: '/images/ICON/CIRCLE/circle_b'+mark1[i].img+'.png',
				scaledSize:new google.maps.Size(15,15)
			},
			/*infoWindow: {
				//content: '<span style="color:#000">'+mark1[i].nama_witel+'</span>'
				content : '<div class="table-responsive"><table class="table table-striped table-bordered table-hover summary" ><thead><tr><th colspan="4">'+mark1[i].nama_witel+'</th></tr><tr><th><img src="/images/ICON/NODE-B/node_bwhite_fgrey.png" style="width:20px"/><br>U | D | Un | Tot.</th><th><img src="/images/ICON/METRO/metro_bwhite_fgrey.png" style="width:20px"/><br>U | D | Un | Tot.</th><th><img src="/images/ICON/LAMBDA/lambda_bwhite_fgrey.png" style="width:20px"/><br>U | D | Un | Tot.</th><th><img src="/images/ICON/TRAFFIC/traffic_bwhite_fgrey.png" style="width:20px"/><br>U | D | Un | Tot.</th></tr></thead></table></div>'
			},*/
			details: {
				markid:'mark1'+i,
				idx: mark1[i].id,
				lat: parseFloat(mark1[i].lat),
				lng: parseFloat(mark1[i].lng),
				title: mark1[i].desc,
				url: mark1[i].url,
				wit_id: parseInt(mark1[i].wit_id),
				sto_condition: parseInt(mark1[i].sto_condition),
				marker: parseInt(mark1[i].marker),
				zoom: parseInt(mark1[i].zoom),
				sto_condition: parseInt(mark1[i].sto_condition),
				img: mark1[i].img
			},
			click: function (e) {
				$('#gmap1').hide();
				$('#gmap2').show();
				map2.refresh();
				map2.setCenter(e.details.lat,e.details.lng);
				map2.setZoom(parseInt(e.details.zoom));
				//var markers = map.gmap('get', 'marker');
				//console.log(markers);
			}
		});
	}
	
	for(var i = 0; i<mark2.length;i++){
		//console.log(mark2[i]);
		map2.addMarker({
			lat: parseFloat(mark2[i].locn_x),
			lng: parseFloat(mark2[i].locn_y),
			title: mark2[i].sto_description,
			icon: {
				url: '/images/ICON/CIRCLE/circle_b'+mark2[i].img+'.png',
				scaledSize:new google.maps.Size(15,15)
			},
			/*infoWindow: {
				//content: '<span style="color:#000">'+mark1[i].nama_witel+'</span>'
				content : '<div class="table-responsive"><table class="table table-striped table-bordered table-hover summary" ><thead><tr><th colspan="4">'+mark1[i].nama_witel+'</th></tr><tr><th><img src="/images/ICON/NODE-B/node_bwhite_fgrey.png" style="width:20px"/><br>U | D | Un | Tot.</th><th><img src="/images/ICON/METRO/metro_bwhite_fgrey.png" style="width:20px"/><br>U | D | Un | Tot.</th><th><img src="/images/ICON/LAMBDA/lambda_bwhite_fgrey.png" style="width:20px"/><br>U | D | Un | Tot.</th><th><img src="/images/ICON/TRAFFIC/traffic_bwhite_fgrey.png" style="width:20px"/><br>U | D | Un | Tot.</th></tr></thead></table></div>'
			},*/
			details: {
				markid:'mark2'+i,
				idx: i,
				lat: parseFloat(mark2[i].locn_x),
				lng: parseFloat(mark2[i].locn_y),
				title: mark2[i].sto_description,
				sto_code: mark2[i].sto_code,
				report_desc: mark2[i].report_desc,
				reported_date: mark2[i].reported_date,
				reported_by: mark2[i].reported_by,
				witel: mark2[i].witel,
				wit_id: parseInt(mark2[i].wil_id),
				sto_condition: parseInt(mark2[i].sto_condition),
				img: mark2[i].img
			},
			click: function (e) {
				map2.refresh();
				map2.setCenter(e.details.lat,e.details.lng);
			}
		});
	}
	
	window.onresize = function(event) {
		$('#gmap1,#gmap2').css({height:$('#content').height()});
		map.refresh();
		map2.refresh();
	};
		
	App.addResponsiveHandler(function () {
		$('#gmap1,#gmap2').css({height:$('#content').height()});
		map.refresh();
		map2.refresh();
	});
	
	$('#gmap2').hide();
}

return {
	//main function to initiate map samples
	init: function (mark1,mark2) {
		mapInit(mark1,mark2);
	}

};

}();
