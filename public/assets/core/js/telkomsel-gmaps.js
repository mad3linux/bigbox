var MapsGoogle1 = function () {
	//var modal1 = $('#modal1');

    var mapMarker = function (mark1) {
        var map = new GMaps({
            div: '#gmap_marker',
            lat: -2.504085513748616,
            lng: 116.32324193750006,
            zoom:5,
            click: function(e) {
				//alert('click');
				//console.log(e);
				map.hideInfoWindows();
			},
			dragend: function(e) {
				//alert('dragend');
				//console.log(e);
				//console.log(map.getCenter());
			}
        });
        
        map.addControl({
		  position: 'top_right',
		  content: 'Go to center',
		  style: {
			margin: '5px 3px',
			padding: '1px 6px',
			border: 'solid 1px #717B87',
			background: '#fff',
			'font-size': '11px'
		  },
		  events: {
			click: function(){
				//console.log(this);
				/*map.setCenter(-2.504085513748616,116.32324193750006);
				map.setZoom(5);
				map.refresh();*/
				select = $('#controlcenter');
				opt = $('#opt'+select.val());
				//console.log(opt.data('lat'));
				map.setCenter(parseFloat(opt.data('lat')),parseFloat(opt.data('lng')));
				map.setZoom(parseInt(opt.data('zoom')));
				map.refresh();
			}
		  }
		});
        
        map.addControl({
		  position: 'top_right',
		  content: '<select placeholder="center" id="controlcenter">'+
						'<option value="1" id="opt1" data-zoom="5" data-lat="-2.504085513748616" data-lng="116.32324193750006">Indonesia</option>'+
						'<option value="2" id="opt2" data-zoom="6" data-lat="-0.26367094433665017" data-lng="102.36373901367188">Sumatera</option>'+
						'<option value="3" id="opt3" data-zoom="7" data-lat="-7.536764322084078" data-lng="109.65866088867188">Jawa</option>'+
						'<option value="4" id="opt4" data-zoom="6" data-lat="1.4394488164139768" data-lng="113.52584838867188">Kalimantan</option>'+
						'<option value="5" id="opt5" data-zoom="7" data-lat="-2.3284603685731593" data-lng="120.95260620117188">Sulawesi</option>'+
						'<option value="6" id="opt6" data-zoom="7" data-lat="-8.7113588754265" data-lng="118.35983276367188">Bali Nusra</option>'+
						'<option value="7" id="opt7" data-zoom="6" data-lat="-3.2743089918452106" data-lng="131.19186401367188">Maluku Papua</option>'+
					'</select>',
		  style: {
			margin: '6px 3px',
			padding: '0px',
			border: '0px',
			background: '#fff',
			'font-size': '11px'
		  },
		  events: {
			change: function(e){
				select = $('#controlcenter');
				opt = $('#opt'+select.val());
				//console.log(opt.data('lat'));
				map.setCenter(parseFloat(opt.data('lat')),parseFloat(opt.data('lng')));
				map.setZoom(parseInt(opt.data('zoom')));
				map.refresh();
			}
		  }
		});
		
		//console.log(mark1);
		
		for(var i = 0; i<mark1.length;i++){
			map.addMarker({
				lat: mark1[i].x,
				lng: mark1[i].y,
				title: mark1[i].nama_witel,
                icon: {
					url: '/images/ICON/CIRCLE/circle_bwhite_fgreen.png',
					scaledSize:new google.maps.Size(15,15)
				},
				infoWindow: {
					//content: '<span style="color:#000">'+mark1[i].nama_witel+'</span>'
					//content : '<div class="table-responsive"><table class="table table-striped table-bordered table-hover summary" ><thead><tr><th colspan="4">'+mark1[i].nama_witel+'</th></tr><tr><th><img src="/images/ICON/NODE-B/node_bwhite_fgrey.png" style="width:20px"/><br>U | D | Un | Tot.</th><th><img src="/images/ICON/METRO/metro_bwhite_fgrey.png" style="width:20px"/><br>U | D | Un | Tot.</th><th><img src="/images/ICON/LAMBDA/lambda_bwhite_fgrey.png" style="width:20px"/><br>U | D | Un | Tot.</th><th><img src="/images/ICON/TRAFFIC/traffic_bwhite_fgrey.png" style="width:20px"/><br>U | D | Un | Tot.</th></tr></thead></table></div>'
					content: mark1[i].html
				},
				details: {
					markid:'mark1'+i,
					idx:i,
					lat: mark1[i].x,
					lng: mark1[i].y,
					title: mark1[i].nama_witel,
					idwitel: mark1[i].id_witel
				},
				click: function (e) {
					//if (console.log) console.log(e);
					//alert('You clicked in this marker');
					//modal1.modal();
				}
			});
		}
		
		
		window.onresize = function(event) {
			$('#gmap_marker').css({height:$('#content').height()});
			map.refresh();
		};
            
        App.addResponsiveHandler(function () {
			$('#gmap_marker').css({height:$('#content').height()});
			map.refresh();
        });
    }

    return {
        //main function to initiate map samples
        init: function (mark1) {
            mapMarker(mark1);
        }

    };

}();
