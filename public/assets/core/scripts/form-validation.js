var FormValidation = function () {

    var handleValidation1 = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form1 = $('#form_sample_1');
            var error1 = $('.alert-danger', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    name: {
                        minlength: 2,
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    url: {
                        required: true,
                        url: true
                    },
                    number: {
                        required: true,
                        number: true
                    },
                    digits: {
                        required: true,
                        digits: true
                    },
                    creditcard: {
                        required: true,
                        creditcard: true
                    },
                    occupation: {
                        minlength: 5,
                    },
                    category: {
                        required: true
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    success1.show();
                    error1.hide();
                }
            });

    }

    var handleValidation2 = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form2 = $('#form_sample_2');
            var error2 = $('.alert-danger', form2);
            var success2 = $('.alert-success', form2);

            form2.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    name: {
                        minlength: 2,
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    url: {
                        required: true,
                        url: true
                    },
                    number: {
                        required: true,
                        number: true
                    },
                    digits: {
                        required: true,
                        digits: true
                    },
                    creditcard: {
                        required: true,
                        creditcard: true
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success2.hide();
                    error2.show();
                    App.scrollTo(error2, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var icon = $(element).parent('.input-icon').children('i');
                    icon.removeClass('fa-check').addClass("fa-warning");  
                    icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group   
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    
                },

                success: function (label, element) {
                    var icon = $(element).parent('.input-icon').children('i');
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    icon.removeClass("fa-warning").addClass("fa-check");
                },

                submitHandler: function (form) {
                    success2.show();
                    error2.hide();
                }
            });


    }

    var handleValidation3 = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

            var form3 = $('#form_sample_3');
            var error3 = $('.alert-danger', form3);
            var success3 = $('.alert-success', form3);

            //IMPORTANT: update CKEDITOR textarea with actual content before submit
            form3.on('submit', function() {
                for(var instanceName in CKEDITOR.instances) {
                    CKEDITOR.instances[instanceName].updateElement();
                }
            })

            form3.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    name: {
                        minlength: 2,
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    category: {
                        required: true
                    },
                    options1: {
                        required: true
                    },
                    options2: {
                        required: true
                    },
                    occupation: {
                        minlength: 5,
                    },
                    membership: {
                        required: true
                    },
                    service: {
                        required: true,
                        minlength: 2
                    },
                    markdown: {
                        required: true
                    },
                    editor1: {
                        required: true
                    },
                    editor2: {
                        required: true
                    }
                },

                messages: { // custom messages for radio buttons and checkboxes
                    membership: {
                        required: "Please select a Membership type"
                    },
                    service: {
                        required: "Please select  at least 2 types of Service",
                        minlength: jQuery.format("Please select  at least {0} types of Service")
                    }
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.parent(".input-group").size() > 0) {
                        error.insertAfter(element.parent(".input-group"));
                    } else if (element.attr("data-error-container")) { 
                        error.appendTo(element.attr("data-error-container"));
                    } else if (element.parents('.radio-list').size() > 0) { 
                        error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                    } else if (element.parents('.radio-inline').size() > 0) { 
                        error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                    } else if (element.parents('.checkbox-list').size() > 0) {
                        error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                    } else if (element.parents('.checkbox-inline').size() > 0) { 
                        error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success3.hide();
                    error3.show();
                    App.scrollTo(error3, -200);
                },

                highlight: function (element) { // hightlight error inputs
                   $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    success3.show();
                    error3.hide();
                }

            });

             //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
            $('.select2me', form3).change(function () {
                form3.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });
    }

    var handleWysihtml5 = function() {
        if (!jQuery().wysihtml5) {
            
            return;
        }

        if ($('.wysihtml5').size() > 0) {
            $('.wysihtml5').wysihtml5({
                "stylesheets": ["assets/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
            });
        }
    }

    return {
        //main function to initiate the module
        init: function () {

            handleWysihtml5();
            handleValidation1();
            handleValidation2();
            handleValidation3();

        }

    };

}();

var AddUser = function () {

	var handleAdd = function() {
		
		$('form#edituser').validate({
	            //errorElement: 'span', //default input error message container
	            //errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	                uname: {
	                    required: true
	                },
	                fullname: {
	                    required: true
	                },
	                pass: {
	                    required: true
	                },
	                email: {
	                    email: true
	                }
	            },

	            messages: {
					
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
	                //$('.alert-danger', $('.login-form')).show();
	                //console.log(event);
	                //console.log(validator);
	                
					//$('span.help-block').show();
					$('span.help-block').hide();
	                var errorlist = validator.errorList;
	                if(errorlist.length>0){
						for(var i = 0; i<errorlist.length; i++){
							//console.log(errorlist[i].element);
							var el = errorlist[i].element; 
							var next = $(el).next('span.help-block'); 
							$(next).slideDown();
						}
						$('.modal-scrollable:visible').animate({
							scrollTop: $(el).offset().top
						}, 800);
					}
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                //label.remove();
	            },

	            errorPlacement: function (error, element) {
					$('span.help-block').hide();
					var next = element.next('span.help-block'); 
					//console.log(error);
	                if(error[0].textContent!=""){
						$(next).children('span.label').html("<i class='fa fa-warning'></i> "+ error[0].textContent);
						$(element).next().slideDown();
						$('.modal-scrollable:visible').animate({
							scrollTop: $(element).offset().top
						}, 800);
					}
	            },

	            submitHandler: function (form) {
						$('#alertinfo').addClass('hidden');
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	                //form.submit(); // form validation success, call ajax form submit
	                submitForm();
	                return false;
	            }
	        });

	        $('form#edituser input').keypress(function (e) {
	            if (e.which == 13) {
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	                if ($('form#edituser').validate().form()) {
	                   //$('form#edituser').submit(); //form validation success, call ajax form submit
	                   submitForm();
	                }
	                return false;
	            }
	        });
	}
	
	var submitForm = function(){
		//console.log($('form#edituser').serialize());
		$.ajax({
			url:"/ajax/adduser",
			content:'json',
			type:'post',
			data:$('form#edituser').serialize(),
			beforeSend:function(){
				$('#alertinfo').addClass('hidden');
				$('#processbar').removeClass('hidden');
				$('.modal-scrollable:visible').animate({
					scrollTop: $('#formtitle').offset().top
				}, 800);
				$('#alertinfo').removeClass('alert-success');
				$('#alertinfo').removeClass('alert-warning');
				$('#alertinfo').removeClass('alert-danger');
				$('#alertinfo').removeClass('alert-info');
			},
			complete:function(result){
				$('#processbar').addClass('hidden');
			},
			success: function(data){
				//console.log(data);
				if(data.result!=undefined){
					if(data.result==true){
						$('#alertinfo').addClass('alert-success');
						$('#alertmsg').html("<strong>Success!</strong> "+data.retMsg);
						resetForm();
					}else{
						$('#alertinfo').addClass('alert-warning');
						$('#alertmsg').html("<strong>Failed!</strong> "+data.retMsg);
					}
				}else{
					$('#alertinfo').addClass('alert-info');
					$('#alertmsg').html("<strong>Error!</strong> System is busy, please try again.");
				}
				$('#alertinfo').removeClass('hidden');
			},
			error:function(){
				$('#alertinfo').addClass('alert-info');
				$('#alertmsg').html("<strong>Error!</strong> System is busy, please try again.");
				$('#alertinfo').removeClass('hidden');
			}
		});
	}
	
	

	var resetForm = function(){
		$('input','form#edituser')
			.not(':button, :submit, :reset, :hidden')
			.val('');
			
		$('select,option','form#edituser')
			.removeAttr('checked')
			.removeAttr('selected');
		
		$('.make-switch').bootstrapSwitch('setState',false);
		
		$('.ms-selectable').find('li').removeClass('ms-selected');
		$('.ms-selection').find('li').removeClass('ms-hover');
		$('.ms-selectable').find('li').removeAttr('style');
		
		$('.ms-selection').find('li').removeClass('ms-selected');
		$('.ms-selection').find('li').removeClass('ms-hover');
		$('.ms-selection').find('li').css({display:'none'});
	}
    
    return {
        //main function to initiate the module
        init: function () {
            handleAdd();
        }

    };

}();

var AddUser2 = function () {

	var handleAdd = function() {
		
		$('form#edituser').validate({
	            //errorElement: 'span', //default input error message container
	            //errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	                uname: {
	                    required: true
	                },
	                fullname: {
	                    required: true
	                },
	                pass: {
	                    required: true
	                },
	                email: {
	                    email: true
	                }
	            },

	            messages: {
					
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
	                //$('.alert-danger', $('.login-form')).show();
	                //console.log(event);
	                //console.log(validator);
	                
					//$('span.help-block').show();
					$('span.help-block').hide();
	                var errorlist = validator.errorList;
	                if(errorlist.length>0){
						for(var i = 0; i<errorlist.length; i++){
							//console.log(errorlist[i].element);
							var el = errorlist[i].element; 
							var next = $(el).next('span.help-block'); 
							$(next).slideDown();
						}
						$('.modal-scrollable:visible').animate({
							scrollTop: $(el).offset().top
						}, 800);
					}
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                //label.remove();
	            },

	            errorPlacement: function (error, element) {
					$('span.help-block').hide();
					var next = element.next('span.help-block'); 
					//console.log(error);
	                if(error[0].textContent!=""){
						$(next).children('span.label').html("<i class='fa fa-warning'></i> "+ error[0].textContent);
						$(element).next().slideDown();
						$('.modal-scrollable:visible').animate({
							scrollTop: $(element).offset().top
						}, 800);
					}
	            },

	            submitHandler: function (form) {
						$('#alertinfo').addClass('hidden');
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	                //form.submit(); // form validation success, call ajax form submit
	                submitForm();
	                return false;
	            }
	        });

	        $('form#edituser input').keypress(function (e) {
	            if (e.which == 13) {
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	                if ($('form#edituser').validate().form()) {
	                   //$('form#edituser').submit(); //form validation success, call ajax form submit
	                   submitForm();
	                }
	                return false;
	            }
	        });
	}
	
	var submitForm = function(){
		console.log($('form#edituser').serialize());
	
		$.ajax({
			url:"/ajax/adduser2",
			content:'json',
			type:'post',
			data:$('form#edituser').serialize(),
			beforeSend:function(){
				$('#alertinfo').addClass('hidden');
				$('#processbar').removeClass('hidden');
				$('.modal-scrollable:visible').animate({
					scrollTop: $('#formtitle').offset().top
				}, 800);
				$('#alertinfo').removeClass('alert-success');
				$('#alertinfo').removeClass('alert-warning');
				$('#alertinfo').removeClass('alert-danger');
				$('#alertinfo').removeClass('alert-info');
			},
			complete:function(result){
				$('#processbar').addClass('hidden');
			},
			success: function(data){
				//console.log(data);
				if(data.result!=undefined){
					if(data.result==true){
						$('#alertinfo').addClass('alert-success');
						$('#alertmsg').html("<strong>Success!</strong> "+data.retMsg);
						resetForm();
					}else{
						$('#alertinfo').addClass('alert-warning');
						$('#alertmsg').html("<strong>Failed!</strong> "+data.retMsg);
					}
				}else{
					$('#alertinfo').addClass('alert-info');
					$('#alertmsg').html("<strong>Error!</strong> System is busy, please try again.");
				}
				$('#alertinfo').removeClass('hidden');
			},
			error:function(){
				$('#alertinfo').addClass('alert-info');
				$('#alertmsg').html("<strong>Error!</strong> System is busy, please try again.");
				$('#alertinfo').removeClass('hidden');
			}
		});
	
	}
	
	

	var resetForm = function(){
		$('input','form#edituser')
			.not(':button, :submit, :reset, :hidden')
			.val('');
			
		$('select,option','form#edituser')
			.removeAttr('checked')
			.removeAttr('selected');
		
		$('.make-switch').bootstrapSwitch('setState',false);
		
		$('.ms-selectable').find('li').removeClass('ms-selected');
		$('.ms-selection').find('li').removeClass('ms-hover');
		$('.ms-selectable').find('li').removeAttr('style');
		
		$('.ms-selection').find('li').removeClass('ms-selected');
		$('.ms-selection').find('li').removeClass('ms-hover');
		$('.ms-selection').find('li').css({display:'none'});
	}
    
    return {
        //main function to initiate the module
        init: function () {
            handleAdd();
        }

    };

}();

var EditUser = function () {

	var handleAdd = function() {

		$('form#edituser').validate({
	            //errorElement: 'span', //default input error message container
	            //errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	                uname: {
	                    required: true
	                },
	                fullname: {
	                    required: true
	                },
	                pass: {
	                    required: true
	                },
	                email: {
	                    email: true
	                }
	            },

	            messages: {
					
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
	                //$('.alert-danger', $('.login-form')).show();
	                //console.log(event);
	                //console.log(validator);
	                
					//$('span.help-block').show();
					$('span.help-block').hide();
	                var errorlist = validator.errorList;
	                if(errorlist.length>0){
						for(var i = 0; i<errorlist.length; i++){
							//console.log(errorlist[i].element);
							var el = errorlist[i].element; 
							var next = $(el).next('span.help-block'); 
							$(next).slideDown();
						}
						$('html,body').animate({
							scrollTop: $(el).offset().top-100
						}, 800);
					}
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                //label.remove();
	            },

	            errorPlacement: function (error, element) {
					$('span.help-block').hide();
					var next = element.next('span.help-block'); 
					//console.log(error);
	                if(error[0].textContent!=""){
						$(next).children('span.label').html("<i class='fa fa-warning'></i> "+ error[0].textContent);
						$(element).next().slideDown();
						$('html,body').animate({
							scrollTop: $(element).offset().top-100
						}, 800);
					}
	            },

	            submitHandler: function (form) {
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	                //form.submit(); // form validation success, call ajax form submit
	                submitForm();
	                return false;
	            }
	        });

	        $('form#edituser input').keypress(function (e) {
	            if (e.which == 13) {
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	                if ($('form#edituser').validate().form()) {
	                   //$('form#edituser').submit(); //form validation success, call ajax form submit
	                   submitForm();
	                }
	                return false;
	            }
	        });
	}
	
	var submitForm = function(){
		//console.log($('form#edituser').serialize());
		$.ajax({
			url:"/ajax/adduser",
			content:'json',
			type:'post',
			data:$('form#edituser').serialize(),
			beforeSend:function(){
				$('#alertinfo').addClass('hidden');
				$('#processbar').removeClass('hidden');
				$('html,body').animate({
					scrollTop: $('h3.page-title').offset().top
				}, 800);
				$('#alertinfo').removeClass('alert-success');
				$('#alertinfo').removeClass('alert-warning');
				$('#alertinfo').removeClass('alert-danger');
				$('#alertinfo').removeClass('alert-info');
			},
			complete:function(result){
				$('#processbar').addClass('hidden');
			},
			success: function(data){
				//console.log(data);
				if(data.result!=undefined){
					if(data.result==true){
						$('#alertinfo').addClass('alert-success');
						$('#alertmsg').html("<strong>Success!</strong> "+data.retMsg);
					}else{
						$('#alertinfo').addClass('alert-warning');
						$('#alertmsg').html("<strong>Failed!</strong> "+data.retMsg);
					}
				}else{
					$('#alertinfo').addClass('alert-info');
					$('#alertmsg').html("<strong>Error!</strong> System is busy, please try again.");
				}
				$('#alertinfo').removeClass('hidden');
			},
			error:function(){
				$('#alertinfo').addClass('alert-info');
				$('#alertmsg').html("<strong>Error!</strong> System is busy, please try again.");
				$('#alertinfo').removeClass('hidden');
			}
		});
	}
	
	

	var resetForm = function(){
		$('input','form#edituser')
			.not(':button, :submit, :reset, :hidden')
			.val('');
			
		$('select,option','form#edituser')
			.removeAttr('checked')
			.removeAttr('selected');
		
		$('.make-switch').bootstrapSwitch('setState',false);
		
		$('.ms-selectable').find('li').removeClass('ms-selected');
		$('.ms-selection').find('li').removeClass('ms-hover');
		$('.ms-selectable').find('li').removeAttr('style');
		
		$('.ms-selection').find('li').removeClass('ms-selected');
		$('.ms-selection').find('li').removeClass('ms-hover');
		$('.ms-selection').find('li').css({display:'none'});
	}
    
    return {
        //main function to initiate the module
        init: function () {
            handleAdd();
        }

    };

}();

var EditUser2 = function () {

	var handleAdd = function() {

		$('form#edituser').validate({
	            //errorElement: 'span', //default input error message container
	            //errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	                uname: {
	                    required: true
	                },
	                fullname: {
	                    required: true
	                },
	                pass: {
	                    required: true
	                },
	                email: {
	                    email: true
	                }
	            },

	            messages: {
					
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
	                //$('.alert-danger', $('.login-form')).show();
	                //console.log(event);
	                //console.log(validator);
	                
					//$('span.help-block').show();
					$('span.help-block').hide();
	                var errorlist = validator.errorList;
	                if(errorlist.length>0){
						for(var i = 0; i<errorlist.length; i++){
							//console.log(errorlist[i].element);
							var el = errorlist[i].element; 
							var next = $(el).next('span.help-block'); 
							$(next).slideDown();
						}
						$('html,body').animate({
							scrollTop: $(el).offset().top-100
						}, 800);
					}
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                //label.remove();
	            },

	            errorPlacement: function (error, element) {
					$('span.help-block').hide();
					var next = element.next('span.help-block'); 
					//console.log(error);
	                if(error[0].textContent!=""){
						$(next).children('span.label').html("<i class='fa fa-warning'></i> "+ error[0].textContent);
						$(element).next().slideDown();
						$('html,body').animate({
							scrollTop: $(element).offset().top-100
						}, 800);
					}
	            },

	            submitHandler: function (form) {
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	                //form.submit(); // form validation success, call ajax form submit
	                submitForm();
	                return false;
	            }
	        });

	        $('form#edituser input').keypress(function (e) {
	            if (e.which == 13) {
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	                if ($('form#edituser').validate().form()) {
	                   //$('form#edituser').submit(); //form validation success, call ajax form submit
	                   submitForm();
	                }
	                return false;
	            }
	        });
	}
	
	var submitForm = function(){
		//console.log($('form#edituser').serialize());
		$.ajax({
			url:"/ajax/adduser2",
			content:'json',
			type:'post',
			data:$('form#edituser').serialize(),
			beforeSend:function(){
				$('#alertinfo').addClass('hidden');
				$('#processbar').removeClass('hidden');
				$('html,body').animate({
					scrollTop: $('h3.page-title').offset().top
				}, 800);
				$('#alertinfo').removeClass('alert-success');
				$('#alertinfo').removeClass('alert-warning');
				$('#alertinfo').removeClass('alert-danger');
				$('#alertinfo').removeClass('alert-info');
			},
			complete:function(result){
				$('#processbar').addClass('hidden');
			},
			success: function(data){
				//console.log(data);
				if(data.result!=undefined){
					if(data.result==true){
						$('#alertinfo').addClass('alert-success');
						$('#alertmsg').html("<strong>Success!</strong> "+data.retMsg);
					}else{
						$('#alertinfo').addClass('alert-warning');
						$('#alertmsg').html("<strong>Failed!</strong> "+data.retMsg);
					}
				}else{
					$('#alertinfo').addClass('alert-info');
					$('#alertmsg').html("<strong>Error!</strong> System is busy, please try again.");
				}
				$('#alertinfo').removeClass('hidden');
			},
			error:function(){
				$('#alertinfo').addClass('alert-info');
				$('#alertmsg').html("<strong>Error!</strong> System is busy, please try again.");
				$('#alertinfo').removeClass('hidden');
			}
		});
	}
	
	

	var resetForm = function(){
		$('input','form#edituser')
			.not(':button, :submit, :reset, :hidden')
			.val('');
			
		$('select,option','form#edituser')
			.removeAttr('checked')
			.removeAttr('selected');
		
		$('.make-switch').bootstrapSwitch('setState',false);
		
		$('.ms-selectable').find('li').removeClass('ms-selected');
		$('.ms-selection').find('li').removeClass('ms-hover');
		$('.ms-selectable').find('li').removeAttr('style');
		
		$('.ms-selection').find('li').removeClass('ms-selected');
		$('.ms-selection').find('li').removeClass('ms-hover');
		$('.ms-selection').find('li').css({display:'none'});
	}
    
    return {
        //main function to initiate the module
        init: function () {
            handleAdd();
        }

    };

}();

var AddMenu = function () {

	var handleAdd = function() {

		$('form#addmenu').validate({
	            //errorElement: 'span', //default input error message container
	            //errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	                name: {
	                    required: true
	                },
	                parent: {
	                    required: true
	                },
	                link: {
	                    required: true
	                },
	                weight: {
	                    required: true
	                },
	                desc: {
	                    required: true
	                }
	            },

	            messages: {
					
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
	                //$('.alert-danger', $('.login-form')).show();
	                //console.log(event);
	                //console.log(validator);
	                
					//$('span.help-block').show();
					$('span.help-block').hide();
	                var errorlist = validator.errorList;
	                if(errorlist.length>0){
						for(var i = 0; i<errorlist.length; i++){
							//console.log(errorlist[i].element);
							var el = errorlist[i].element; 
							var next = $(el).next('span.help-block'); 
							$(next).slideDown();
						}
						$('.modal-scrollable:visible').animate({
							scrollTop: $(el).offset().top
						}, 800);
					}
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                //label.remove();
	            },

	            errorPlacement: function (error, element) {
					$('span.help-block').hide();
					var next = element.next('span.help-block'); 
					//console.log(error);
	                if(error[0].textContent!=""){
						$(next).children('span.label').html("<i class='fa fa-warning'></i> "+ error[0].textContent);
						$(element).next().slideDown();
						$('.modal-scrollable:visible').animate({
							scrollTop: $(element).offset().top
						}, 800);
					}
	            },

	            submitHandler: function (form) {
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	                //form.submit(); // form validation success, call ajax form submit
	                submitForm();
	                return false;
	            }
	        });

	        $('form#edituser input').keypress(function (e) {
	            if (e.which == 13) {
					$('span.help-block').hide();
					$('.form-group').removeClass('has-error');
	                if ($('form#edituser').validate().form()) {
	                   //$('form#edituser').submit(); //form validation success, call ajax form submit
	                   submitForm();
	                }
	                return false;
	            }
	        });
	}
	
	var submitForm = function(){
		//console.log($('form#edituser').serialize());
		$.ajax({
			url:"/ajax/addmenu",
			content:'json',
			type:'post',
			data:$('form#addmenu').serialize(),
			beforeSend:function(){
				$('#alertinfo').addClass('hidden');
				$('#processbar').removeClass('hidden');
				$('.modal-scrollable:visible').animate({
					scrollTop: $('#formtitle').offset().top
				}, 800);
				$('#alertinfo').removeClass('alert-success');
				$('#alertinfo').removeClass('alert-warning');
				$('#alertinfo').removeClass('alert-danger');
				$('#alertinfo').removeClass('alert-info');
			},
			complete:function(result){
				$('#processbar').addClass('hidden');
			},
			success: function(data){
				//console.log(data);
				if(data.result!=undefined){
					if(data.result==true){
						$('#alertinfo').addClass('alert-success');
						$('#alertmsg').html("<strong>Success!</strong> "+data.retMsg+"<br>Please wait page will be refresh...");
						setTimeout(function(){location.href="/admin/managemenus"},2000);
					}else{
						$('#alertinfo').addClass('alert-warning');
						$('#alertmsg').html("<strong>Failed!</strong> "+data.retMsg);
					}
				}else{
					$('#alertinfo').addClass('alert-info');
					$('#alertmsg').html("<strong>Error!</strong> System is busy, please try again.");
				}
				$('#alertinfo').removeClass('hidden');
			},
			error:function(){
				$('#alertinfo').addClass('alert-info');
				$('#alertmsg').html("<strong>Error!</strong> System is busy, please try again.");
				$('#alertinfo').removeClass('hidden');
			}
		});
	}
    
    return {
        //main function to initiate the module
        init: function () {
            handleAdd();
        }

    };

}();
