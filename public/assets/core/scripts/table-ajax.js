var TableAjax = function () {

    return {

        //main function to initiate the module
        init: function () {
            
            if (!jQuery().dataTable) {
                return;
            }

            // begin first table
            $('#sample_1').dataTable({
                "sDom" : "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", //default layout without horizontal scroll(remove this setting to enable horizontal scroll for the table)
                "aLengthMenu": [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "All"] // change per page values here
                ],
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "demo/table_ajax.php",
                // set the initial value
                "iDisplayLength": 10,
                "sPaginationType": "bootstrap_full_number",
                "oLanguage": {
                    "sProcessing": '<i class="fa fa-coffee"></i>&nbsp;Please wait...',
                    "sLengthMenu": "_MENU_ records",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ]
            });

            jQuery('#sample_1_wrapper .dataTables_filter input').addClass("form-control input-medium"); // modify table search input
            jQuery('#sample_1_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown

            // handle record edit/remove
            $('body').on('click', '#sample_1_wrapper .btn-editable', function() {
                alert('Edit record with id:' + $(this).attr("data-id"));
            });

            $('body').on('click', '#sample_1_wrapper .btn-removable', function() {
                alert('Remove record with id:' + $(this).attr("data-id"));
            });

        }

    };

}();

var TableAjax2 = function () {

    return {

        //main function to initiate the module
        init: function () {
            
            if (!jQuery().dataTable) {
                return;
            }

            // begin first table
            var oTable = $('#graph_table').dataTable({
               // "sDom" : "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", //default layout without horizontal scroll(remove this setting to enable horizontal scroll for the table)
                "aLengthMenu": [
                    [10,50, 200, 500 -1],
                    [10,50, 200, 500, "All"] // change per page values here
                ],
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "/escort/ajax/table1",
                // set the initial value
                "iDisplayLength": 50,
                "sPaginationType": "bootstrap_full_number",
                "oLanguage": {
                    "sProcessing": '<i class="fa fa-coffee"></i>&nbsp;Please wait...',
                    "sLengthMenu": "_MENU_ records",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    },
			"sSearch": "Search all columns:"
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0,5]
                    }
                ]
            });

            jQuery('#graph_table_wrapper .dataTables_filter input').addClass("form-control input-medium"); // modify table search input
            jQuery('#graph_table_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown

            // handle record edit/remove
            //$('body').on('click', '#graph_table_wrapper .btn-graph', function() {
            //    alert('Edit record with id:' + $(this).attr("data-id"));
            //});

		
		$("input#sid").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 1 );
		} );
		$("input#address").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 3 );
		} );
		$("a.reload").click( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnDraw();
		} );


        }

    };

}();

var TableAjax3 = function () {

    return {

        //main function to initiate the module
        init: function () {
            
            if (!jQuery().dataTable) {
                return;
            }

            // begin first table
            var oTable = $('#graph_table').dataTable({
               // "sDom" : "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", //default layout without horizontal scroll(remove this setting to enable horizontal scroll for the table)
                "aLengthMenu": [
                    [10,50, 200, 500 -1],
                    [10,50, 200, 500, "All"] // change per page values here
                ],
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "/escort/ajax/table1",
                // set the initial value
                "iDisplayLength": 50,
                "sPaginationType": "bootstrap_full_number",
                "oLanguage": {
                    "sProcessing": '<i class="fa fa-coffee"></i>&nbsp;Please wait...',
                    "sLengthMenu": "_MENU_ records",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    },
			"sSearch": "Search all columns:"
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0,7]
                    }
                ]
            });

            jQuery('#graph_table_wrapper .dataTables_filter input').addClass("form-control input-medium"); // modify table search input
            jQuery('#graph_table_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown

            // handle record edit/remove
            //$('body').on('click', '#graph_table_wrapper .btn-graph', function() {
            //    alert('Edit record with id:' + $(this).attr("data-id"));
            //});

		
		$("input#sid").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 1 );
		} );
		$("input#address").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 3 );
		} );
		$("input#neighbour").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 4 );
		} );
		$("input#port").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 5 );
		} );
		$("a.reload").click( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnDraw();
		} );


        }

    };

}();

var TableAjax2 = function () {

    return {

        //main function to initiate the module
        init: function () {
            
            if (!jQuery().dataTable) {
                return;
            }

            // begin first table
            var oTable = $('#graph_table').dataTable({
               // "sDom" : "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", //default layout without horizontal scroll(remove this setting to enable horizontal scroll for the table)
                "aLengthMenu": [
                    [10,50, 200, 500 -1],
                    [10,50, 200, 500, "All"] // change per page values here
                ],
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "/escort/ajax/table1",
                // set the initial value
                "iDisplayLength": 50,
                "sPaginationType": "bootstrap_full_number",
                "oLanguage": {
                    "sProcessing": '<i class="fa fa-coffee"></i>&nbsp;Please wait...',
                    "sLengthMenu": "_MENU_ records",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    },
			"sSearch": "Search all columns:"
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0,5]
                    }
                ]
            });

            jQuery('#graph_table_wrapper .dataTables_filter input').addClass("form-control input-medium"); // modify table search input
            jQuery('#graph_table_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown

            // handle record edit/remove
            //$('body').on('click', '#graph_table_wrapper .btn-graph', function() {
            //    alert('Edit record with id:' + $(this).attr("data-id"));
            //});

		
		$("input#sid").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 1 );
		} );
		$("input#address").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 3 );
		} );
		$("a.reload").click( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnDraw();
		} );

        $('#sample_2_column_toggler input[type="checkbox"]').change(function(){
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
            oTable.fnSetColumnVis(iCol, (bVis ? false : true));
        });


        }

    };

}();

var TableAjax4 = function () {

    return {

        //main function to initiate the module
        init: function () {
            
            if (!jQuery().dataTable) {
                return;
            }

            // begin first table	
            var oTable = $('#user_table').dataTable({
               // "sDom" : "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", //default layout without horizontal scroll(remove this setting to enable horizontal scroll for the table)
                "aLengthMenu": [
                    [10,50, 200, 500 -1],
                    [10,50, 200, 500, "All"] // change per page values here
                ],
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "/ajax/table1",
                // set the initial value
                "iDisplayLength": 50,
                "sPaginationType": "bootstrap_full_number",
                "oLanguage": {
                    "sProcessing": '<i class="fa fa-coffee"></i>&nbsp;Please wait...',
                    "sLengthMenu": "_MENU_ records",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    },
			"sSearch": "Search all columns:"
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0,7]
                    }
                ]
            });

            jQuery('#user_table_wrapper .dataTables_filter input').addClass("form-control input-medium"); // modify table search input
            jQuery('#user_table_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown

            // handle record edit/remove
            //$('body').on('click', '#graph_table_wrapper .btn-graph', function() {
            //    alert('Edit record with id:' + $(this).attr("data-id"));
            //});

		
		$("input#filterid").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 1 );
		} );
		$("input#filteruname").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 2 );
		} );
		$("input#filterfullname").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 3 );
		} );
		$("input#filteremail").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 4 );
		} );
		$("input#filterubis").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 5 );
		} );
		$("input#filtersub_ubis").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 6 );
		} );
		$("input#filterposition").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 7 );
		} );
		$("a.reload").click( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnDraw();
		} );

        $('#sample_2_column_toggler input[type="checkbox"]').change(function(){
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
            oTable.fnSetColumnVis(iCol, (bVis ? false : true));
        });


        }

    };

}();

var TableAjax5 = function () {

    return {

        //main function to initiate the module
        init: function () {
            
            if (!jQuery().dataTable) {
                return;
            }

            // begin first table	
            var oTable = $('#user_table').dataTable({
               // "sDom" : "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", //default layout without horizontal scroll(remove this setting to enable horizontal scroll for the table)
                "aLengthMenu": [
                    [10,50, 200, 500 -1],
                    [10,50, 200, 500, "All"] // change per page values here
                ],
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "/ajax/tablenits",
                // set the initial value
                "iDisplayLength": 50,
                "sPaginationType": "bootstrap_full_number",
                "oLanguage": {
                    "sProcessing": '<i class="fa fa-coffee"></i>&nbsp;Please wait...',
                    "sLengthMenu": "_MENU_ records",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    },
			"sSearch": "Search all columns:"
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0,7]
                    }
                ]
            });

            jQuery('#user_table_wrapper .dataTables_filter input').addClass("form-control input-medium"); // modify table search input
            jQuery('#user_table_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown

            // handle record edit/remove
            //$('body').on('click', '#graph_table_wrapper .btn-graph', function() {
            //    alert('Edit record with id:' + $(this).attr("data-id"));
            //});

		
		$("input#filterid").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 1 );
		} );
		$("input#filteruname").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 2 );
		} );
		$("input#filterfullname").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 3 );
		} );
		$("input#filteremail").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 4 );
		} );
		$("input#filterubis").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 5 );
		} );
		$("input#filtersub_ubis").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 6 );
		} );
		$("input#filterposition").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 7 );
		} );
		$("a.reload").click( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnDraw();
		} );

        $('#sample_2_column_toggler input[type="checkbox"]').change(function(){
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
            oTable.fnSetColumnVis(iCol, (bVis ? false : true));
        });


        }

    };

}();

var TableAjax6 = function () {

    return {

        //main function to initiate the module
        init: function () {
            
            if (!jQuery().dataTable) {
                return;
            }

            // begin first table	
            var oTable = $('#user_table').dataTable({
               // "sDom" : "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", //default layout without horizontal scroll(remove this setting to enable horizontal scroll for the table)
                "aLengthMenu": [
                    [10,50, 200, 500 -1],
                    [10,50, 200, 500, "All"] // change per page values here
                ],
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "/ajax/tableperformance",
                // set the initial value
                "iDisplayLength": 50,
                "sPaginationType": "bootstrap_full_number",
                "oLanguage": {
                    "sProcessing": '<i class="fa fa-coffee"></i>&nbsp;Please wait...',
                    "sLengthMenu": "_MENU_ records",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    },
			"sSearch": "Search all columns:"
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0,7]
                    }
                ]
            });

            jQuery('#user_table_wrapper .dataTables_filter input').addClass("form-control input-medium"); // modify table search input
            jQuery('#user_table_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown

            // handle record edit/remove
            //$('body').on('click', '#graph_table_wrapper .btn-graph', function() {
            //    alert('Edit record with id:' + $(this).attr("data-id"));
            //});

		
		$("input#filterid").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 1 );
		} );
		$("input#filteruname").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 2 );
		} );
		$("input#filterfullname").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 3 );
		} );
		$("input#filteremail").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 4 );
		} );
		$("input#filterubis").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 5 );
		} );
		$("input#filtersub_ubis").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 6 );
		} );
		$("input#filterposition").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 7 );
		} );
		$("a.reload").click( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnDraw();
		} );

        $('#sample_2_column_toggler input[type="checkbox"]').change(function(){
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
            oTable.fnSetColumnVis(iCol, (bVis ? false : true));
        });


        }

    };

}();

var TableAjax44 = function () {

    return {

        //main function to initiate the module
        init: function () {
            
            if (!jQuery().dataTable) {
                return;
            }

            // begin first table	
            var oTable = $('#user_table').dataTable({
               // "sDom" : "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", //default layout without horizontal scroll(remove this setting to enable horizontal scroll for the table)
                "aLengthMenu": [
                    [10,50, 200, 500 -1],
                    [10,50, 200, 500, "All"] // change per page values here
                ],
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "/ajax/table11",
                // set the initial value
                "iDisplayLength": 50,
                "sPaginationType": "bootstrap_full_number",
                "oLanguage": {
                    "sProcessing": '<i class="fa fa-coffee"></i>&nbsp;Please wait...',
                    "sLengthMenu": "_MENU_ records",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    },
			"sSearch": "Search all columns:"
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0,7]
                    }
                ]
            });

            jQuery('#user_table_wrapper .dataTables_filter input').addClass("form-control input-medium"); // modify table search input
            jQuery('#user_table_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown

            // handle record edit/remove
            //$('body').on('click', '#graph_table_wrapper .btn-graph', function() {
            //    alert('Edit record with id:' + $(this).attr("data-id"));
            //});

		
		$("input#filterid").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 1 );
		} );
		$("input#filteruname").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 2 );
		} );
		$("input#filterfullname").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 3 );
		} );
		$("input#filteremail").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 4 );
		} );
		$("input#filterubis").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 5 );
		} );
		$("input#filtersub_ubis").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 6 );
		} );
		$("input#filterposition").keyup( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnFilter( this.value, 7 );
		} );
		$("a.reload").click( function () {
			/* Filter on the column (the index) of this element */
			oTable.fnDraw();
		} );

        $('#sample_2_column_toggler input[type="checkbox"]').change(function(){
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
            oTable.fnSetColumnVis(iCol, (bVis ? false : true));
        });


        }

    };

}();
