var TableEditable = function () {

    return {

        //main function to initiate the module
        init: function () {
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }

                oTable.fnDraw();
            }

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[0] + '">';
                //jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[2] + '">';
                jqTds[3].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[3] + '">';
                jqTds[4].innerHTML = '<a class="edit" href="">Save</a>';
                jqTds[5].innerHTML = '<a class="cancel" href="">Cancel</a>';
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                //oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
                oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 4, false);
                oTable.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 5, false);
                oTable.fnDraw();
            }

            function cancelEditRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                //oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
                oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 4, false);
                oTable.fnDraw();
            }

            var oTable = $('#sample_editable_1').dataTable({
                "aLengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 5,
                
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ]
            });

            jQuery('#sample_editable_1_wrapper .dataTables_filter input').addClass("form-control input-medium"); // modify table search input
            jQuery('#sample_editable_1_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
            jQuery('#sample_editable_1_wrapper .dataTables_length select').select2({
                showSearchInput : false //hide search box with special css class
            }); // initialize select2 dropdown

            var nEditing = null;

            $('#sample_editable_1_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '', '',
                        '<a class="edit" href="">Edit</a>', '<a class="cancel" data-mode="new" href="">Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow);
                nEditing = nRow;
            });

            $('#sample_editable_1 a.delete').live('click', function (e) {
                e.preventDefault();

                if (confirm("Are you sure to delete this row ?") == false) {
                    return;
                }

                var nRow = $(this).parents('tr')[0];
                oTable.fnDeleteRow(nRow);
                alert("Deleted! Do not forget to do some ajax to sync with backend :)");
            });

            $('#sample_editable_1 a.cancel').live('click', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                } else {
                    restoreRow(oTable, nEditing);
                    nEditing = null;
                }
            });

            $('#sample_editable_1 a.edit').live('click', function (e) {
                e.preventDefault();

                /* Get the row as a parent of the link that was clicked on */
                var nRow = $(this).parents('tr')[0];

                if (nEditing !== null && nEditing != nRow) {
                    /* Currently editing - but not this row - restore the old before continuing to edit mode */
                    restoreRow(oTable, nEditing);
                    editRow(oTable, nRow);
                    nEditing = nRow;
                } else if (nEditing == nRow && this.innerHTML == "Save") {
                    /* Editing this row and want to save it */
                    saveRow(oTable, nEditing);
                    nEditing = null;
                    alert("Updated! Do not forget to do some ajax to sync with backend :)");
                } else {
                    /* No edit in progress - let's start one */
                    editRow(oTable, nRow);
                    nEditing = nRow;
                }
            });
        }

    };

}();

var TableEditable2 = function () {

    return {

        //main function to initiate the module
        init: function () {
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }

                oTable.fnDraw();
            }

            function editRow(oTable, nRow) {
				//console.log(oTable);
				//console.log(nRow);
				//console.log('editrow');
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
				jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[2] + '">';
				jqTds[3].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[3] + '">';
                jqTds[4].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[4] + '">';
				jqTds[5].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[5] + '">';
                jqTds[6].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[6] + '">';
                jqTds[7].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[7] + '" readonly>';
				jqTds[8].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[8] + '" readonly>';
                jqTds[9].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[9] + '">';
                jqTds[10].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[10] + '">';
				jqTds[11].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[11] + '" readonly>';
                jqTds[12].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[12] + '">';
                jqTds[13].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[13] + '">';
				jqTds[14].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[14] + '">';
				jqTds[15].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[15] + '">';
                jqTds[16].innerHTML = '<a class="edit" href="">Save</a>&nbsp|&nbsp<a class="cancel" href="">Cancel</a>';
                //jqTds[16].innerHTML = '<a class="cancel" href="">Cancel</a>';
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
                //console.log(jqInputs);
                var val = {};
                for(var i = 0; i < jqInputs.length; i++){
					val[i] = jqInputs[i].value;
				}
				
				$.ajax({
					url:"/ajax/insertdevicehistory",
					content:'json',
					type:'post',
					data:val,
					beforeSend:function(){
						$('#alertinfo').addClass('hidden');
						$('#processbar').removeClass('hidden');
						$('html,body').animate({
							scrollTop: $('.page-title').offset().top
						}, 800);
						$('#alertinfo').removeClass('alert-success');
						$('#alertinfo').removeClass('alert-warning');
						$('#alertinfo').removeClass('alert-danger');
						$('#alertinfo').removeClass('alert-info');
					},
					complete:function(result){
						$('#processbar').addClass('hidden');
					},
					success: function(data){
						//console.log(data);
						if(data.result!=undefined){
							if(data.result==true){
								$('#alertinfo').addClass('alert-success');
								$('#alertmsg').html("<strong>Success!</strong> "+data.retMsg);
								//oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
								oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
								oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
								oTable.fnUpdate(jqInputs[2].value, nRow, 3, false);
								oTable.fnUpdate(jqInputs[3].value, nRow, 4, false);
								oTable.fnUpdate(jqInputs[4].value, nRow, 5, false);
								oTable.fnUpdate(jqInputs[5].value, nRow, 6, false);
								oTable.fnUpdate(jqInputs[6].value, nRow, 7, false);
								oTable.fnUpdate(jqInputs[7].value, nRow, 8, false);
								oTable.fnUpdate(jqInputs[8].value, nRow, 9, false);
								oTable.fnUpdate(jqInputs[9].value, nRow, 10, false);
								oTable.fnUpdate(jqInputs[10].value, nRow, 11, false);
								oTable.fnUpdate(jqInputs[11].value, nRow, 12, false);
								oTable.fnUpdate(jqInputs[12].value, nRow, 13, false);
								oTable.fnUpdate(jqInputs[13].value, nRow, 14, false);
								oTable.fnUpdate(jqInputs[14].value, nRow, 15, false);
								oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 16, false);
								//oTable.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 16, false);
								oTable.fnDraw();
								nEditing = null;
							}else{
								$('#alertinfo').addClass('alert-warning');
								$('#alertmsg').html("<strong>Failed!</strong> "+data.retMsg);
							}
						}else{
							$('#alertinfo').addClass('alert-info');
							$('#alertmsg').html("<strong>Error!</strong> System is busy, please try again.");
						}
						$('#alertinfo').removeClass('hidden');
					},
					error:function(){
						$('#alertinfo').addClass('alert-info');
						$('#alertmsg').html("<strong>Error!</strong> System is busy, please try again.");
						$('#alertinfo').removeClass('hidden');
					}
				});
				
                //console.log(val);
            }

            function cancelEditRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
                //oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 3, false);
                oTable.fnUpdate(jqInputs[3].value, nRow, 4, false);
                oTable.fnUpdate(jqInputs[4].value, nRow, 5, false);
                oTable.fnUpdate(jqInputs[5].value, nRow, 6, false);
                oTable.fnUpdate(jqInputs[6].value, nRow, 7, false);
                oTable.fnUpdate(jqInputs[7].value, nRow, 8, false);
                oTable.fnUpdate(jqInputs[8].value, nRow, 9, false);
                oTable.fnUpdate(jqInputs[9].value, nRow, 10, false);
                oTable.fnUpdate(jqInputs[10].value, nRow, 11, false);
                oTable.fnUpdate(jqInputs[11].value, nRow, 12, false);
                oTable.fnUpdate(jqInputs[12].value, nRow, 13, false);
                oTable.fnUpdate(jqInputs[13].value, nRow, 14, false);
                oTable.fnUpdate(jqInputs[14].value, nRow, 15, false);
                oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 16, false);
                oTable.fnDraw();
            }

            var oTable = $('#sample_editable_1').dataTable({
                "aLengthMenu": [
                    [10,50, 200, 500 -1],
                    [10,50, 200, 500, "All"] // change per page values here
                ],
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "/ajax/table2",
                // set the initial value
                "iDisplayLength": 50,
                "sPaginationType": "bootstrap_full_number",
                "oLanguage": {
                    "sProcessing": '<i class="fa fa-coffee"></i>&nbsp;Please wait...',
                    "sLengthMenu": "_MENU_ records",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "fnDrawCallback": function( oSettings ) {
				  if ( nEditing!=null) {
					  //console.log(nEditing);
					//	editRow(oTable, nEditing);
					  //console.log("xxx");
				  }
				},
				"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
					if (nEditing !== null){
						var first1 = $(nEditing).children()[0];
						var first2 = $(nRow).children()[0];
						if ($(first1).html() == $(first2).html()) {
							//console.log($(first1).html());
							//console.log($(first2).html());
							editRow(oTable, nRow);
							//console.log(nRow);
							nEditing = nRow;
							//console.log("==================================");
						}
					}
				},
				"sSearch": "Search all columns:",
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0,16]
                    }
                ]
            });

            jQuery('#sample_editable_1_wrapper .dataTables_filter input').addClass("form-control input-medium"); // modify table search input
            jQuery('#sample_editable_1_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
            jQuery('#sample_editable_1_wrapper .dataTables_length select').select2({
                showSearchInput : false //hide search box with special css class
            }); // initialize select2 dropdown

            var nEditing = null;

            $('#sample_editable_1_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '', '',
                        '<a class="edit" href="">Edit</a>', '<a class="cancel" data-mode="new" href="">Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow);
                nEditing = nRow;
            });

            $('#sample_editable_1 a.delete').live('click', function (e) {
                e.preventDefault();

                if (confirm("Are you sure to delete this row ?") == false) {
                    return;
                }

                var nRow = $(this).parents('tr')[0];
                oTable.fnDeleteRow(nRow);
                alert("Deleted! Do not forget to do some ajax to sync with backend :)");
            });

            $('#sample_editable_1 a.cancel').live('click', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                } else {
                    restoreRow(oTable, nEditing);
                    nEditing = null;
                }
            });
            
			$("a.reload").click( function (e) {
				/* Filter on the column (the index) of this element */
				oTable.fnDraw();
				e.preventDefault();
			} );
            
            $('#sample_editable_1 a.edit').live('click', function (e) {
                e.preventDefault();

                /* Get the row as a parent of the link that was clicked on */
                var nRow = $(this).parents('tr')[0];

                if (nEditing !== null && nEditing != nRow) {
                    /* Currently editing - but not this row - restore the old before continuing to edit mode */
                    restoreRow(oTable, nEditing);
                    //editRow(oTable, nRow);
                    nEditing = nRow;
                    //editRow(oTable, nRow);
                } else if (nEditing == nRow && this.innerHTML == "Save") {
                    /* Editing this row and want to save it */
                    saveRow(oTable, nEditing);
                    //alert("Updated! Do not forget to do some ajax to sync with backend :)");
                } else {
                    /* No edit in progress - let's start one */
                    editRow(oTable, nRow);
                    nEditing = nRow;
                }
            });
        }

    };

}();
