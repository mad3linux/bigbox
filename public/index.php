<?php
ini_set('allow_url_fopen', '1');

/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
#ini_set("display_errors", "On");
//die("zzz");

defined('APPLICATION_PATH')|| define('APPLICATION_PATH', realpath(dirname(__FILE__). '/../application'));
defined('APPLICATION_ENV')|| define('APPLICATION_ENV',(getenv('APPLICATION_ENV')? getenv('APPLICATION_ENV'): 'production'));
set_include_path(implode(PATH_SEPARATOR, array(realpath(APPLICATION_PATH . '/../library'), get_include_path(),)));
require_once(APPLICATION_PATH . 
             '/../vendor/autoload.php');
define('PAGE_PARSE_START_TIME', microtime());
define('PAGE_PARSE_RATE', 1 / 200);
require_once 'Zend/Application.php';
#die("xxxx");
$application = new Zend_Application(APPLICATION_ENV, APPLICATION_PATH . 
                                    '/configs/application.ini');
#define('survey_mod', "1");
require_once APPLICATION_PATH . '/configs/config.php';
$application->bootstrap()->run();
