<?php



$db_result["mssql"]="mssql_result_jdbcx";  // this variable for excel maps to tbe below function. If you want to try your own connection method, simply change the variable reference. 

/*

$db_param["type"]='mssql';
$db_param["driver_str"]='';
$db_param["dbname"]='master';
$db_param["host"]='192.168.2.4';
$db_param["port"]='1433';
$db_param["dbuser"]='sa';
$db_param["dbpass"]='grDW1dRuxE9mGpZe1dA96EDHlkQvZ42325lSlmy3F58=';
$db_param["connection_description"]='old_sqlserver_db';


new

$db_param["type"]='jdbc';
$db_param["driver_str"]='';
$db_param["dbname"]='';
$db_param["jdbcdriver"]='net.sourceforge.jtds.jdbc.Driver';
$db_param["jdbcurl"]='jdbc:jtds:sqlserver://localhost:1433/master';
$db_param["dbuser"]='sa';
$db_param["dbpass"]='grDW1dRuxE9mGpZe1dA96EDHlkQvZ42325lSlmy3F58=';
$db_param["connection_description"]='';
*/

function mssql_result_jdbcx($db_param,$sql)
{
 try
 {
 
 xlog(0,"Calling mssql_result_jdbc begin<br>");
 
 $db_param["jdbcdriver"]='net.sourceforge.jtds.jdbc.Driver';
$db_param["jdbcurl"]="jdbc:jtds:sqlserver://{$db_param['host']};port={$db_param['port']}/{$db_param['dbname']}";
$sql=base64_encode($sql);
 $db_param["sql"]=$sql;
 $output=post_url($jdbc,$db_param);
 //xlog(0,"raw output = ".$output);
 $content=json_decode($output,true);


 	//clog(0, $output);
    return $content;

 }
    catch (exception $e)
    {
          clog(0,"connection methodds get_connection exception ".print_r($e,true));
		  $content["legends"]=array("Error");
		  $content["data"]=array(0);
		  return $content;
    } 
}

$db_result["server_flat_csv"]="server_flat_csv_result";  //was pointing to jdbc_result, this variable for excel maps to tbe below function. If you want to try your own connection method, simply change the variable reference. 


function server_flat_csv_result($db_param,$sql)
{
 try
 {
 
 xlog(0,"Custom : Calling server_flat_csv begin<br>");
 xlog(0,"api= ".$_SESSION['xmember']["api_id"]);
//$sql=base64_encode($sql);
// $db_param["sql"]=$sql;
$db_param["sql_obj"]["sql_source"]="none";
$db_param["dbpass"]="none";
xlog(0,"dbparam ".print_r($db_param,true));
$param_str=json_encode($db_param);	
xlog(0,"param_str=".$param_str);
$db_param['param_str']=base64_encode($param_str);
xlog(0,$db_param['param_str']);
	if ($db_param["sql_obj"]["userModel"]["callBack"]=="processDistinctMembers")
	{
		$aggr_engine="http://localhost/cgi-bin/uniques.py?allcols={$db_param['all_columns']}";
	}
	else
	{
		$aggr_engine="http://localhost/cgi-bin/aggregate.py?allcols={$db_param['all_columns']}";
	}


//$aggr_engine="http://localhost/cgi-bin/aggregate.py?allcols={$db_param['all_columns']}&param_str={$param_str}";
//$output=file_get_contents($aggr_engine);
 $output=post_url($aggr_engine,$db_param);
 xlog(0,"raw output = ".$output);
 $content=json_decode($output,true);


 	//xlog(0,"decoded ".print_r($content,true));
	
    return $content;
 }
    catch (exception $e)
    {
          clog(0,"connection methodds get_connection exception ".print_r($e,true));
		  $content["legends"]=array("Error");
		  $content["data"]=array(0);
		  return $content;
    } 
}

$table_result["server_flat_csv"]="get_server_flat_csv_tables";  // this variable for excel maps to tbe below function. If you want to try your own connection method, simply change the variable reference. 


function get_server_flat_csv_tables($db_param)
{
xlog(0,"get csv file list");
$dir    = $db_param["dirpath"];
//$files1 = scandir($dir);
$files = array();
/*
foreach (glob("{$dir}/*.csv") as $file) 
{
	array_push($files,utf8_encode($file)); 
  //$files[] = $file;
}
*/

$it = new DirectoryIterator("glob://{$dir}/*.csv");
foreach($it as $f) {
	array_push($files,utf8_encode($f->getFilename())); 
   // printf("%s: %.1FK\n", $f->getFilename(), $f->getSize()/1024);
}

$table_names=$files; //array();
/*
for ($i=0;$i<count($files1);$i++)
{
	if (is_dir($files1[$i]))
		array_push($table_names,utf8_encode()); 
}	
*/
	$content["tables"]=$table_names;
	$content["error_flag"]="false";
	$content["error_mesg"]="Connection Success";
	clog(0, json_encode($content));
	return $content;
}



$db_result["jdbc_backup"]="direct_jdbc_result";  //was pointing to jdbc_result, this variable for excel maps to tbe below function. If you want to try your own connection method, simply change the variable reference. 


function direct_jdbc_result($db_param,$sql)
{
 try
 {
 
 xlog(0,"Calling direct_jdbc_result Custom begin<br>");


 $db_param["sql"]=$sql;
$last_slash_pos=strrpos($_SERVER["SCRIPT_FILENAME"],"/");

 $class_path=substr($_SERVER["SCRIPT_FILENAME"],0,$last_slash_pos+1);// = C:/xampp/htdocs/infocaptor_dev/base/evaluation_test.php
 $class_path.="system/java/";
 xlog(0,"Classpath={$class_path}");
 //include "system/jdbc.php";
 //$output=post_url($odbc,$db_param);
 	//	  $content["legends"]=array("Col1");
		//  $content["data"][0]=array("tea","coffee");
$data_output = array();

$sql=base64_encode($sql);
$cmd_params=<<<EOD
"{$db_param["jdbcdriver"]}" "{$db_param["jdbcurl"]}" "{$db_param["dbuser"]}" "{$db_param["dbpass"]}" "{$db_param['row_limit']}" "{$sql}"
EOD;
xlog(0,"command line params=".$cmd_params);
$full_cmd=<<<FOD
"{$class_path}jdk1.7.0/jre/bin/java"  -Dfile.encoding=UTF8 -cp "{$class_path}" InfocaptorSQL {$cmd_params} 2>&1
FOD;
xlog(5,"full java command=".$full_cmd);
exec($full_cmd, $data_output);		
	$content=json_decode($data_output[0],true);
	//xlog(0,$data_output[0]);
//xlog(0,"content=". print_r($content,true));
    return $content;
 }
    catch (exception $e)
    {
          xlog(0,"connection methodds get_connection exception ".print_r($e,true));
		  $content["legends"]=array("Error");
		  $content["data"]=array(0);
		  return $content;
    } 
}




$table_result["jdbc_backup"]="get_jdbc_tablesx";  // this variable for excel maps to tbe below function. If you want to try your own connection method, simply change the variable reference. 


function get_jdbc_tablesx($db_param)
{


 try
 {
 
 xlog(0,"Calling get_jdbc_tablesx begin<br>");


 $db_param["sql"]=$sql;
 
$last_slash_pos=strrpos($_SERVER["SCRIPT_FILENAME"],"/");

 $class_path=substr($_SERVER["SCRIPT_FILENAME"],0,$last_slash_pos+1);// = C:/xampp/htdocs/infocaptor_dev/base/evaluation_test.php
 $class_path.="system/java/";
 xlog(0,"Classpath={$class_path}");
 
 xlog(0,print_r($db_param,true));

$cmd_params=<<<EOD
"{$db_param["jdbcdriver"]}" "{$db_param["jdbcurl"]}" "{$db_param["dbuser"]}" "{$db_param["dbpass"]}"
EOD;
xlog(0,"command line params=".$cmd_params);
$full_cmd=<<<FOD
"{$class_path}jdk1.7.0/jre/bin/java"  -Dfile.encoding=UTF8 -cp "{$class_path}" InfocaptorTables {$cmd_params} 2>&1
FOD;
xlog(0,"full java command=".$full_cmd);
exec($full_cmd, $data_output);		 

 $table_meta=json_decode($data_output[0],true);
 	//clog(0, $output);
$content["tables"]=$table_meta["table_list"];
			$content["error_flag"]="false";
			$content["error_mesg"]="none";	
    return $content;
 }
    catch (exception $e)
    {
          xlog(0,"connection meta exception ".print_r($e,true));
			$content["tables"]="Error";
		  	
			$content["error_flag"]="true";
			$content["error_mesg"]="JDBC connection error ";
		  return $content;
    } 
}

$db_result["odbc_backup"]="odbc_result_jdbcx";  // this variable for excel maps to tbe below function. If you want to try your own connection method, simply change the variable reference. 
function odbc_result_jdbcx($db_param,$sql)
{
 try
 {
 
 xlog(0,"Calling odbc_result_jdbcx Custom begin<br>");
 xlog(0,"api= ".$_SESSION['xmember']["api_id"]);
$last_slash_pos=strrpos($_SERVER["SCRIPT_FILENAME"],"/");

 $class_path=substr($_SERVER["SCRIPT_FILENAME"],0,$last_slash_pos+1);// = C:/xampp/htdocs/infocaptor_dev/base/evaluation_test.php
 $class_path.="system/java/";
 xlog(0,"Classpath={$class_path}");
 $db_param["sql"]=$sql;
 
 //include "system/jdbc.php";
 //$output=post_url($odbc,$db_param);
 	//	  $content["legends"]=array("Col1");
		//  $content["data"][0]=array("tea","coffee");
$data_output = array();

$sql=base64_encode($sql);
$cmd_params=<<<EOD
"sun.jdbc.odbc.JdbcOdbcDriver" "jdbc:odbc:{$db_param["dbname"]}" "{$db_param["dbuser"]}" "{$db_param["dbpass"]}" "{$db_param['row_limit']}" "{$sql}"
EOD;
xlog(0,"command line params=".$cmd_params);
$full_cmd=<<<FOD
"{$class_path}jdk1.7.0/jre/bin/java"  -Dfile.encoding=UTF8 -cp "{$class_path}" InfocaptorSQL {$cmd_params} 2>&1
FOD;
xlog(0,"full java command=".$full_cmd);
exec($full_cmd, $data_output);		
	$content=json_decode($data_output[0],true);
	//xlog(0,$data_output[0]);
//xlog(0,"content=". print_r($content,true));
    return $content;
 }
    catch (exception $e)
    {
          xlog(0,"connection methodds get_connection exception ".print_r($e,true));
		  $content["legends"]=array("Error");
		  $content["data"]=array(0);
		  return $content;
    } 
}



$table_result["odbc_backup"]="get_odbc_tablesx";  // this variable for excel maps to tbe below function. If you want to try your own connection method, simply change the variable reference. 


function get_odbc_tablesx($db_param)
{


 try
 {
 
 xlog(0,"Calling get_odbc_tablesx begin<br>");


 $db_param["sql"]=$sql;
 
$last_slash_pos=strrpos($_SERVER["SCRIPT_FILENAME"],"/");

 $class_path=substr($_SERVER["SCRIPT_FILENAME"],0,$last_slash_pos+1);// = C:/xampp/htdocs/infocaptor_dev/base/evaluation_test.php
 $class_path.="system/java/";
 xlog(0,"Classpath={$class_path}");
 
 xlog(0,print_r($db_param,true));

$cmd_params=<<<EOD
"sun.jdbc.odbc.JdbcOdbcDriver" "jdbc:odbc:{$db_param["dbname"]}" "{$db_param["dbuser"]}" "{$db_param["dbpass"]}"
EOD;
xlog(0,"command line params=".$cmd_params);
$full_cmd=<<<FOD
"{$class_path}jdk1.7.0/jre/bin/java"  -Dfile.encoding=UTF8 -cp "{$class_path}" InfocaptorTables {$cmd_params} 2>&1
FOD;
xlog(0,"full java command=".$full_cmd);
exec($full_cmd, $data_output);		 

 $table_meta=json_decode($data_output[0],true);
 	//xlog(5, print_r($data_output,true));
$content["tables"]=$table_meta["table_list"];
			$content["error_flag"]="false";
			$content["error_mesg"]="none";	
    return $content;
 }
    catch (exception $e)
    {
          xlog(0,"connection meta exception ".print_r($e,true));
			$content["tables"]="Error";
		  	
			$content["error_flag"]="true";
			$content["error_mesg"]="JDBC connection error ";
		  return $content;
    } 
}


$db_result["mysql_x"]="my_custom_mysql_adapter";  


function my_custom_mysql_adapter(  $db_param  ,  $sql  )
{
 try
 {
 
	 _log(0,"Calling my_custom_mysql_adapter");
	 $db = mysql_connect($db_param["host"].":".$db_param["port"], $db_param["dbuser"], $db_param["dbpass"]);
	 if ($db) _log(0,"mysql connection success");
	 else _log(0,"mysql connection failed");
		mysql_select_db($db_param["dbname"],$db);
		$result = mysql_query($sql,$db);
		if ($result === false) die("failed"); 
 

	 // Loop to handle all results
	 $row_id=0;
	 $column_names=array();
	 $col_data=array();
	 $row_limit=$db_param['row_limit']; 
	 if (!isset($row_limit) ) $row_limit=10;
	while( ($row = mysql_fetch_array($result)) && $row_id<$row_limit)
	{
	  //$row_data[$row_id]=array();
	   // each key and value can be examined individually as well
	   $col_id=0;
	   //_log(0,print_r($row,true));
	   foreach($row as $key => $value)
	   {
	     if (is_int($key)) continue;
	    // _log(0,"key = {$key} val = {$value}");     
	     $col_data[$col_id][$row_id]=utf8_encode($value); 
		 $col_id++;
	     if ($row_id==0) 
		 {
		   array_push($column_names,$key); //just capture column names only once
		 }  
		 //array_push($row_data[$row_id],$value);
		//  print "<br>Key: " . $key . " Value: " . $value;
	   }
	   $row_id++;
	}

	$content["legends"]=$column_names;
	$content["data"]=$col_data;
	_log(0, json_encode($content));
    return $content;
 }
    catch (exception $e)
    {
          _log(0,"connection methods get_connection exception ".print_r($e,true));
		  $content["legends"]=array("Error");
		  $content["data"]=array(0);
		  return $content;
    } 
}




function _log($level, $msg)
{


global $gs;
    

if (!($gs["logging_enabled"]=='Y' && $gs["log_level"]<=$level)) return;
$dateStamp=date("Y_m_d", time());
//$dataStamp=md5( $dateStamp )."_".$dateStamp;
$fd = fopen("log/x_log_".$dateStamp.".log", "a");
// append date/time to message
$str = "[" . date("Y/m/d h:i:s", time()) . "] " . $msg."     session=".session_id();
// write string
fwrite($fd, $str . "\n");
// close file
fclose($fd);
@chmod("log/x_log_".$dateStamp.".log", 0600);
}


?>