<?php

$etl["database"]="";

$etl["job_name"]="etl_csv_hr_data_csv1";
$etl["source_type"]="csv";

$etl["source_data"]="hr_data_csv.csv";



$etl["last_row_tracker_column"]="RowId";
$etl["last_row_tracker_init_val"]=0;

$etl["skip_top_rows"]=1;
$etl["load_size"]=1000;
$etl["target_table"]="hr_data_csv1";

$etl["discard_row"]="Y";

$etl["table_refresh_rule"]="INSERT_UPDATE_PRIMARY_KEY";
$etl["run_retention_rule"] = "Y";

function xet_delete_old_data_fn()
{

}

function xet_row_discard_fn($source_row) 
{
	  if (empty($source_row[0])) return true;
}


function xet_get_source_csv_handle()
{

}


function xet_get_source_columns()
{
global $etl;
$etl["source_columns"]=array(
'Full Name'
,'Hire Date'
,'Location'
,'State'
,'Termination Date'
,'Employment Type'
,'Year'
,'Base Salary'
,'Bonus'
,'Overtime'
,'Commission'
,'Total Compensation'
,'Department'
,'PTO Days'
,'Sick Days'
,'Performance Score'
);
}



function xet_get_select_cols()
{
$sel_cols=array(
);

return implode(",",$sel_cols);

}

function xet_get_last_value()
{

  global $etl;
  $etvar_file=$etl["base_dir"]."/".$etl["job_name"]."_var.php";
   if (file_exists($etvar_file))
   {
		include "{$etvar_file}";
		$etl["last_val"]=$etl_var[$etl["last_row_tracker_column"]]; 
   }
   else
   {
		$etl["last_val"]=$etl["last_row_tracker_init_val"];
   }
}

function xet_get_where_sql()
{
  global $etl;
  
  xet_get_last_value();
  elog(0, "last value = ".$etl['last_val']."<br>");
  $where =  "where B is not null 
and A>{$etl["last_val"]}
 limit {$etl["load_size"]}";
return $where;
}

function xet_get_encoded_sql()
{
	$select_cols=xet_get_select_cols();
	$where= xet_get_where_sql();

	$final_sql="select ".$select_cols." ".$where;
	$encoded_sql=urlencode($final_sql);
	
	return $encoded_sql;
}

function xet_get_final_url($url)
{

return $url;

  $encoded_sql=xet_get_encoded_sql();
  $final_url=$url."&tq=".$encoded_sql;
  return $final_url;
}





function xet_track_last_value($data)
{
/*an indexed list of values are sent and this function needs to decide which value is being used for last tracking*/
global $etl;
	$etl["last_row_tracker_init_val"]=$data[0];
}

function xet_update_last_value()
{
/*
global $etl;
  $etvar_file=$etl["base_dir"]."/".$etl["job_name"]."_var.php";
	$fd = fopen($etvar_file, "w");
	fwrite($fd,"<?php\n\n");

		   fwrite($fd,"\$etl_var['{$etl["last_row_tracker_column"]}']={$etl["last_row_tracker_init_val"]};\n");
		   

	fwrite($fd,"\n?>");
	fclose($fd);
	*/
}

function xet_get_target_columns()
{
global $etl;
$etl["target_columns"][]=array( 	"column_name" => "full_name" ,	"column_type" => "Char" ,	"column_len"=> "20" ,	"source_column"=> "Full Name" ,	"apply_func" => "N" ,	"date_format"=> "" ,	"update_column" => "N" ,	"index" => "None",	"transpose_flag" => "N",	"transpose_column" => "",	"function_string" => ""	);
$etl["target_columns"][]=array( 	"column_name" => "hire_date" ,	"column_type" => "Char" ,	"column_len"=> "10" ,	"source_column"=> "Hire Date" ,	"apply_func" => "N" ,	"date_format"=> "" ,	"update_column" => "N" ,	"index" => "None",	"transpose_flag" => "N",	"transpose_column" => "",	"function_string" => ""	);
$etl["target_columns"][]=array( 	"column_name" => "location" ,	"column_type" => "Char" ,	"column_len"=> "10" ,	"source_column"=> "Location" ,	"apply_func" => "N" ,	"date_format"=> "" ,	"update_column" => "N" ,	"index" => "None",	"transpose_flag" => "N",	"transpose_column" => "",	"function_string" => ""	);
$etl["target_columns"][]=array( 	"column_name" => "state" ,	"column_type" => "Char" ,	"column_len"=> "10" ,	"source_column"=> "State" ,	"apply_func" => "N" ,	"date_format"=> "" ,	"update_column" => "N" ,	"index" => "None",	"transpose_flag" => "N",	"transpose_column" => "",	"function_string" => ""	);
$etl["target_columns"][]=array( 	"column_name" => "termination_date" ,	"column_type" => "Char" ,	"column_len"=> "0" ,	"source_column"=> "Termination Date" ,	"apply_func" => "N" ,	"date_format"=> "" ,	"update_column" => "N" ,	"index" => "None",	"transpose_flag" => "N",	"transpose_column" => "",	"function_string" => ""	);
$etl["target_columns"][]=array( 	"column_name" => "employment_type" ,	"column_type" => "Char" ,	"column_len"=> "10" ,	"source_column"=> "Employment Type" ,	"apply_func" => "N" ,	"date_format"=> "" ,	"update_column" => "N" ,	"index" => "None",	"transpose_flag" => "N",	"transpose_column" => "",	"function_string" => ""	);
$etl["target_columns"][]=array( 	"column_name" => "year" ,	"column_type" => "Num" ,	"column_len"=> "10,2" ,	"source_column"=> "Year" ,	"apply_func" => "N" ,	"date_format"=> "" ,	"update_column" => "Y" ,	"index" => "None",	"transpose_flag" => "N",	"transpose_column" => "",	"function_string" => ""	);
$etl["target_columns"][]=array( 	"column_name" => "base_salary" ,	"column_type" => "Char" ,	"column_len"=> "10" ,	"source_column"=> "Base Salary" ,	"apply_func" => "N" ,	"date_format"=> "" ,	"update_column" => "N" ,	"index" => "None",	"transpose_flag" => "N",	"transpose_column" => "",	"function_string" => ""	);
$etl["target_columns"][]=array( 	"column_name" => "bonus" ,	"column_type" => "Char" ,	"column_len"=> "10" ,	"source_column"=> "Bonus" ,	"apply_func" => "N" ,	"date_format"=> "" ,	"update_column" => "N" ,	"index" => "None",	"transpose_flag" => "N",	"transpose_column" => "",	"function_string" => ""	);
$etl["target_columns"][]=array( 	"column_name" => "overtime" ,	"column_type" => "Char" ,	"column_len"=> "10" ,	"source_column"=> "Overtime" ,	"apply_func" => "N" ,	"date_format"=> "" ,	"update_column" => "N" ,	"index" => "None",	"transpose_flag" => "N",	"transpose_column" => "",	"function_string" => ""	);
$etl["target_columns"][]=array( 	"column_name" => "commission" ,	"column_type" => "Char" ,	"column_len"=> "10" ,	"source_column"=> "Commission" ,	"apply_func" => "N" ,	"date_format"=> "" ,	"update_column" => "N" ,	"index" => "None",	"transpose_flag" => "N",	"transpose_column" => "",	"function_string" => ""	);
$etl["target_columns"][]=array( 	"column_name" => "total_compensation" ,	"column_type" => "Char" ,	"column_len"=> "10" ,	"source_column"=> "Total Compensation" ,	"apply_func" => "N" ,	"date_format"=> "" ,	"update_column" => "N" ,	"index" => "None",	"transpose_flag" => "N",	"transpose_column" => "",	"function_string" => ""	);
$etl["target_columns"][]=array( 	"column_name" => "department" ,	"column_type" => "Char" ,	"column_len"=> "20" ,	"source_column"=> "Department" ,	"apply_func" => "N" ,	"date_format"=> "" ,	"update_column" => "N" ,	"index" => "None",	"transpose_flag" => "N",	"transpose_column" => "",	"function_string" => ""	);
$etl["target_columns"][]=array( 	"column_name" => "pto_days" ,	"column_type" => "Num" ,	"column_len"=> "10,2" ,	"source_column"=> "PTO Days" ,	"apply_func" => "N" ,	"date_format"=> "" ,	"update_column" => "Y" ,	"index" => "None",	"transpose_flag" => "N",	"transpose_column" => "",	"function_string" => ""	);
$etl["target_columns"][]=array( 	"column_name" => "sick_days" ,	"column_type" => "Num" ,	"column_len"=> "10,2" ,	"source_column"=> "Sick Days" ,	"apply_func" => "N" ,	"date_format"=> "" ,	"update_column" => "Y" ,	"index" => "None",	"transpose_flag" => "N",	"transpose_column" => "",	"function_string" => ""	);
$etl["target_columns"][]=array( 	"column_name" => "performance_score" ,	"column_type" => "Num" ,	"column_len"=> "10,2" ,	"source_column"=> "Performance Score" ,	"apply_func" => "N" ,	"date_format"=> "" ,	"update_column" => "Y" ,	"index" => "None",	"transpose_flag" => "N",	"transpose_column" => "",	"function_string" => ""	);


}

function xet_build_function_map()
{
global $etl;



}

//make sure the function returns the value surrounded by single quote if a static value is sent. For mysql function calls do not surround with quotes



function xet_currentDate_fn($str,$sdata)
{
  return " now() ";
}

function xet_service_date_fn($str,$sdata) 
{
//"Year",
//"Month",
  return "STR_TO_DATE('{$str}','%m-%d-%Y %H:%i:%s')"; 

 //return $sdata["Year"]."-".$sdata["Month"]."-01";
  
}

function xet_format1_date_fn($str,$sdata) 
{
//"Year",
//"Month",
   return "STR_TO_DATE('{$str}','%m/%d/%Y')"; 

 //return $sdata["Year"]."-".$sdata["Month"]."-01";
  
}

function xet_format2_date_fn($str,$sdata) 
{
//"Year",
//"Month",

  return "CAST(DATE_FORMAT(STR_TO_DATE('{$str}','%m/%d/%Y') ,'%Y-%m-01') as DATE)"; 

 //return $sdata["Year"]."-".$sdata["Month"]."-01";
  
}

function xet_format3_date_fn($str,$sdata) 
{
//"Year",
//"Month",
  $dStr="1-{$sdata["month"]}-{$sdata["year"]}";
  return "STR_TO_DATE('{$dStr}','%d-%M-%Y')"; 

 //return $sdata["Year"]."-".$sdata["Month"]."-01";
  
}

function xet_dateTime_fn($datestr,$sdata)
{
  return date_format(date_create_from_format('j/m/Y H:i:s', $datestr), 'Y-m-d H:i:s');
}

function xet_date_fn($datestr,$sdata)
{
  return date_format(date_create_from_format('j/m/Y', $datestr), 'Y-m-d');
}

function xet_CompletedAt_lat_fn($str,$sdata)
{
  if ($str=="Unknown") return $str;
	$ary=explode("-", $str);
  return trim($ary[0]);
}

function xet_CompletedAt_lon_fn($str,$sdata)
{
  if ($str=="Unknown") return $str;
	$ary=explode("-", $str);
  return trim($ary[1]);

}

function xet_str_cleanup_fn($str,$sdata)
{

  return "'".str_replace(array(",","'")," ",$str)."'";
  

}
function xet_pre_task($base_dir,$file)
{

//@@PRE_TASK_SQL@@

}
?>