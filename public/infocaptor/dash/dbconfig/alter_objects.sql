alter table xcategory add column `sort_val` int(11);
alter table xcategory add column `description` varchar(200);
alter table xgroup add column `sort_val` int(11);
alter table xgroup add column `description` varchar(200);
alter table xgroup_category_map add column `sort_val` int(11);
alter table xgroup_category_map add column `description` varchar(200);
alter table xmembership_plan add column `sort_val` int(11);
alter table xmembership_plan add column `description` varchar(200);
alter table xmembership_plan_detail add column `sort_val` int(11);
alter table xmembership_plan_detail add column `description` varchar(200);
alter table xobject_category_map add column `sort_val` int(11);
alter table xobject_category_map add column `description` varchar(200);
alter table xobject_type add column `sort_val` int(11);
alter table xobject_type add column `description` varchar(200);
alter table xobject_share add column `sort_val` int(11);
alter table xobject_share add column `description` varchar(200);
alter table xplan_subscription add column `sort_val` int(11);
alter table xplan_subscription add column `description` varchar(200);
alter table xrole add column `sort_val` int(11);
alter table xrole add column `description` varchar(200);
alter table xrole_privilege_map add column `sort_val` int(11);
alter table xrole_privilege_map add column `description` varchar(200);
alter table xuser add column `sort_val` int(11);
alter table xuser add column `description` varchar(200);
alter table xuser_group_map add column `sort_val` int(11);
alter table xuser_group_map add column `description` varchar(200);
alter table xprivilege add column `sort_val` int(11);
alter table xobject add column `status`  varchar(50);
alter table xobject add column `attrib1` varchar(100);
alter table xobject add column `attrib2` varchar(100);
alter table xobject add column `attrib3` varchar(100);
alter table xobject add column `sort_val` int(11);
alter table xobject add column `description` varchar(200);
CREATE TABLE IF NOT EXISTS `xlogin` (
  `login_track_id` int(11) NOT NULL AUTO_INCREMENT,
  `login_name` varchar(100) NOT NULL,
  `creation_date` datetime NOT NULL,
  PRIMARY KEY (`login_track_id`)
  ) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=123;
alter table xobject_category_map add column `role_id` int(11);
alter table xuser_group_map add column `role_id` int(11);
alter table xgroup_category_map add column `role_id` int(11);
alter table xobject add column `is_public`  varchar(1);
alter table xcategory add column `is_public`  varchar(1);
alter table xobject add column locked_by_name	varchar(100);
alter table xobject add column locked_date	datetime;
CREATE TABLE IF NOT EXISTS `xconnection` (
  `connection_id` int(11) NOT NULL AUTO_INCREMENT,
  `connection_handle` varchar(100) NOT NULL,
  `connection_type` varchar(50) NOT NULL,
  `connection_category` varchar(100) NOT NULL,
  `active` varchar(1) NOT NULL,
  `creation_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `last_update_date` datetime NOT NULL,
  `last_updated_by` int(11) NOT NULL,
  `sort_val` int(11) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `attrib1` varchar(100) DEFAULT NULL,
  `attrib2` varchar(100) DEFAULT NULL,
  `attrib3` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`connection_id`),
  UNIQUE KEY `connection_uk` (`connection_handle`,`created_by`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS `xsummary` (
  `username` varchar(100) NOT NULL,
  `metric_name` varchar(100) NOT NULL,
  `metric_type` varchar(100) NOT NULL,
  `metric_value` int(11) NOT NULL,
  `creation_date` datetime NOT NULL,
  UNIQUE KEY `xsummary_uk` (`username`,`metric_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS `xuser_info` (
  `userinfo_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `tag_name` varchar(150) NOT NULL,
  `tag_value` varchar(150) NOT NULL,
  `creation_date` date NOT NULL,
    `created_by` int(11) NOT NULL,
  `last_update_date` date NOT NULL,
  `last_updated_by` int(11) NOT NULL,
  PRIMARY KEY (`userinfo_id`),
  UNIQUE KEY `connection_uk` (`username`,`tag_name`)  
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS `xjob` (
  `job_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_name` varchar(255) NOT NULL,
  `job_type` varchar(255) NOT NULL,
  `load_type` varchar(100) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `load_status` varchar(100) NOT NULL,
  `incremental_duration` int(11) NOT NULL DEFAULT '1440',
  `last_run_date` datetime NOT NULL,
  `last_run_rows` int(11) NOT NULL,
  `last_run_status` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `creation_date` date NOT NULL,
  `last_update_date` datetime NOT NULL,
  `username` varchar(100) NOT NULL,
  `connection_handler` varchar(200) NOT NULL,
  `tablestore` varchar(150) NOT NULL,
  `extrainfo1` text NOT NULL,
  `extrainfo2` text NOT NULL,
  `active` varchar(10) DEFAULT 'Y',
  PRIMARY KEY (`job_id`),
  UNIQUE KEY `job_name` (`job_name`,`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS `xjob_raw_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `load_status` varchar(100) NOT NULL,
  `raw_file_name` varchar(200) NOT NULL,
  `creation_date` datetime NOT NULL,
  `last_update_date` datetime NOT NULL,
  `username` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS `xlicense` (
  `order_number` varchar(100) NOT NULL DEFAULT '',
  `cust_email` varchar(150) NOT NULL DEFAULT '',
  `first_name` varchar(100) NOT NULL DEFAULT '',
  `last_name` varchar(100) NOT NULL DEFAULT '',
  `product_name` varchar(200) NOT NULL DEFAULT '',
  `database` varchar(30) NOT NULL DEFAULT '',
  `expire_date` varchar(30) DEFAULT 'NEVER',
  `selling_price` double NOT NULL DEFAULT '0',
  `order_date` date NOT NULL DEFAULT '0000-00-00',
  `order_date_hour` int(11) NOT NULL DEFAULT '0',
  `order_date_minute` int(11) NOT NULL DEFAULT '0',
  `order_date_timezone` varchar(5) NOT NULL DEFAULT '',
  `serial_key` text,
  `order_id` int(50) NOT NULL AUTO_INCREMENT,
  `processor` varchar(100) DEFAULT NULL,
  `processor_order_number` varchar(100) DEFAULT NULL,
  `registered_domain` text,
  `desktop_user_key` varchar(30) DEFAULT NULL,
  `key_sent_flag` char(1) DEFAULT NULL,
  `key_sent_count` int(11) DEFAULT NULL,
  `validation_stage` varchar(30) DEFAULT NULL,
  `validation_status` varchar(30) DEFAULT NULL,
  `creation_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `install_id` text,
  `os_browser` text,
  `expiry_date` date DEFAULT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS `xorder` (
  order_id int(11) not null AUTO_INCREMENT,
  token varchar(150) not null,
  cust_email varchar(100) not  null,
  order_amount decimal(10,2) not null,
  product_id varchar(50) not null,
  first_name varchar(100),
  last_name  varchar(100),
  company  varchar(100),
  order_date datetime,
  order_type varchar(20),
  PRIMARY KEY (`order_id`),
  UNIQUE KEY `xorder_tk` (`token`)    
) ENGINE=InnoDB DEFAULT CHARSET=latin1;