<?php

/*
a list of all connection methods. 
Copyright Rudrasoft LLC
*/

error_reporting (E_ALL ^ E_NOTICE);





$db_result["excel"]="excel_result_odbc";  // this variable for excel maps to tbe below function. If you want to try your own connection method, simply change the variable reference. 


function excel_result_odbc($db_param,$sql)
{
 try
 {
 
 clog(0,"Calling excel_result_odbc begin<br>");
      $excelFile=$db_param["name"];
	clog(0,"Excel file orig=".$excelFile."<br>");
      $excelRealFile = realpath($excelFile);
	clog(0, "Excel real file=".$excelRealFile."<br>");
	  $fileinfo=pathinfo($excelRealFile);
	clog(0, "Extension =".$fileinfo['extension']);	  
      $excelDir = dirname($excelFile);
	clog(0,"Excel dir=".$excelDir."<br>");
//'DRIVER={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)};FIL=Excel 12.0;DriverID=1046;Dbq=c:\somedirectory\my.xls;DefaultDir=c:\somedirectory;ImportMixedTypes=Text';
//     $db_url="Driver={Microsoft Excel Driver (*.{$fileinfo['extension']})} ;DriverId=790;Dbq={$excelFile};DefaultDir={$excelDir};ImportMixedTypes=Text";
$db_url=$db_param["driver_str"];
	 clog(0,"Excel DSN URL = ".$db_url);
	 $connection = odbc_connect($db_url , '', '');
	 $result = odbc_exec ($connection, $sql);
	 // Loop to handle all results
	 $row_id=0;
	 $column_names=array();
	 $col_data=array();
	 $row_limit=$db_param['row_limit'];
	 if (!isset($row_limit)) $row_limit=10;
	while( ($row = odbc_fetch_array($result)) && $row_id<$row_limit)
	{
	  //$row_data[$row_id]=array();
	   // each key and value can be examined individually as well
	   clog(0,print_r($row,true));
	   $col_id=0;
	   foreach($row as $key => $value)
	   {
	   	     clog(0,"key = {$key} val = {$value}");    
	     $col_data[$col_id][$row_id]=utf8_encode($value); 
		 $col_id++;
	     if ($row_id==0) array_push($column_names,$key); //just capture column names only once
		 //array_push($row_data[$row_id],$value);
		//  print "<br>Key: " . $key . " Value: " . $value;
	   }
	   $row_id++;
	}

	

	$content["legends"]=$column_names;
	$content["data"]=$col_data;
	clog(0, json_encode($content));
    return $content;
 }
    catch (exception $e)
    {
          clog(0,"connection methodds get_connection exception ".print_r($e,true));
		  $content["legends"]=array("Error");
		  $content["data"]=array(0);
		  return $content;
    } 
}


function excel_result_adodb($db_param,$sql)
{

        $excelFile=$db_param["name"];
		echo "Excel file orig=".$excelFile."<br>";
      $excelRealFile = realpath($excelFile);
	  echo "Excel real file=".$excelRealFile."<br>";
      $excelDir = dirname($excelFile);
		echo "Excel dir=".$excelDir."<br>";

      $db_url="Driver={Microsoft Excel Driver (*.xls)} ;DriverId=790;Dbq={$excelFile};DefaultDir={$excelDir};ImportMixedTypes=Text";
		echo $db_url."<br>";
		$db =NewADOConnection('odbc');	
      $db->Connect($db_url , '', '');
 
 
    if ($db)
    {

      $rs = $db->Execute($sql);
		//$maxRows=$rs->MaxRecordCount();
		//echo "max rows={$maxRows}<br>";
		
		while (!$rs->EOF) {
			print_r($rs->fields);
			$rs->MoveNext();
		}
	


    }
    else
    {
          clog(2,"xmember_api.get_sql_result error connection".$db->ErrorMsg());
           clog(2,"xmember_api.get_sql_result error connection".print_r($db,true));
           return false;
    }
	
	return false;

}


$db_result["mysql"]="mysql_result_x";  // this variable for excel maps to tbe below function. If you want to try your own connection method, simply change the variable reference. 


function mysql_result_x($db_param,$sql)
{
 try
 {
 
 clog(0,"Calling mysql_result begin<br>");

 
 $db = mysql_connect($db_param["host"].":".$db_param["port"], $db_param["dbuser"], $db_param["dbpass"]);
mysql_select_db($db_param["dbname"],$db);
$result = mysql_query($sql,$db);
if ($result === false) die("failed"); 
 

	 // Loop to handle all results
	 $row_id=0;
	 $column_names=array();
	 $col_data=array();
	 $row_limit=$db_param['row_limit'];
	 if (!isset($row_limit) ) $row_limit=10;
	while( ($row = mysql_fetch_array($result)) && $row_id<$row_limit)
	{
	  //$row_data[$row_id]=array();
	   // each key and value can be examined individually as well
	   $col_id=0;
	   //clog(0,print_r($row,true));
	   foreach($row as $key => $value)
	   {
	     if (is_int($key)) continue;
	    // clog(0,"key = {$key} val = {$value}");     
	     $col_data[$col_id][$row_id]=utf8_encode($value); 
		 $col_id++;
	     if ($row_id==0) 
		 {
		   array_push($column_names,$key); //just capture column names only once
		 }  
		 //array_push($row_data[$row_id],$value);
		//  print "<br>Key: " . $key . " Value: " . $value;
	   }
	   $row_id++;
	}

	$content["legends"]=$column_names;
	$content["data"]=$col_data;
	clog(0, json_encode($content));
    return $content;
 }
    catch (exception $e)
    {
          clog(0,"connection methodds get_connection exception ".print_r($e,true));
		  $content["legends"]=array("Error");
		  $content["data"]=array(0);
		  return $content;
    } 
}

/*
function excel_connect($db_param)
{
      $db =NewADOConnection('odbc');



    if (!$db)
    {
        
          elog(2,"report_engine.get_connection excel connect".$db->ErrorMsg());
         return null;
    }
  print_r($db_param);
        $excelFile=$db_param["name"];
		echo "Excel file orig=".$excelFile."<br>";
      $excelRealFile = realpath($excelFile);

	  
	  echo "Excel real file=".$excelRealFile."<br>";
      $excelDir = dirname($excelFile);
	  
echo "Excel dir=".$excelDir."<br>";

      $db_url="Driver={Microsoft Excel Driver (*.xls)} ;DriverId=790;Dbq={$excelFile};DefaultDir={$excelDir};ImportMixedTypes=Text";
echo $db_url."<br>";
      $db->Connect($db_url , '', '');
 
      if (!$db)
	  {
		  $_SESSION["xmember"][$db_param["name"]]['error']=$db->ErrorMsg();
		  return null;
      }
      return $db;

      
}
*/

function clog($level, $msg)
{


global $gs;
    

if (!($gs["logging_enabled"]=='Y' && $gs["log_level"]<=$level)) return;
$dateStamp=date("Y_m_d", time());
//$dataStamp=md5( $dateStamp )."_".$dateStamp;
$fd = fopen("log/clogin_".$dateStamp.".log", "a");
// append date/time to message
$str = "[" . date("Y/m/d h:i:s", time()) . "] " . $msg."     session=".session_id();
// write string
fwrite($fd, $str . "\n");
// close file
fclose($fd);
@chmod("log/clogin_".$dateStamp.".log", 0600);
}

?>