-- phpMyAdmin SQL Dump
-- version 3.1.3.1
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Dec 07, 2010 at 06:34 PM
-- Server version: 5.1.33
-- PHP Version: 5.2.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";







--
-- Database: `xmember`
--

-- --------------------------------------------------------

--
-- Table structure for table `xcategory`
--

DROP TABLE IF EXISTS  xcategory,
 xgroup,
 xgroup_category_map,
 xmembership_plan,
 xmembership_plan_detail,
 xobject,
 xobject_category_map,
 xobject_share,
 xobject_type,
 xplan_subscription,
 xprivilege,
 xrole,
 xrole_privilege_map,
 xuser,
 xuser_group_map,
 xlogin,
 xconnection,
 xjob,
 xjob_raw_data,
 xuser_info;

 

CREATE TABLE IF NOT EXISTS `xcategory` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_code` varchar(100) NOT NULL,
  `category_name` varchar(100) NOT NULL,
  `category_type` varchar(30) NOT NULL,
  `active` varchar(1) NOT NULL,
  `creation_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `last_update_date` datetime NOT NULL,
  `last_updated_by` int(11) NOT NULL,
  `sort_val` int(11) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `is_public` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `category_name` (`category_name`,`created_by`),
  UNIQUE KEY `category_code` (`category_code`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `xgroup` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_code` varchar(100) NOT NULL,
  `group_name` varchar(100) NOT NULL,
  `active` varchar(1) NOT NULL,
  `creation_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `last_update_date` datetime NOT NULL,
  `last_updated_by` int(11) NOT NULL,
  `sort_val` int(11) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`group_id`),
  UNIQUE KEY `group_code` (`group_code`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `xgroup_category_map` (
  `group_category_map_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `active` varchar(1) NOT NULL,
  `creation_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `last_update_date` datetime NOT NULL,
  `last_updated_by` int(11) NOT NULL,
  `sort_val` int(11) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`group_category_map_id`),
  UNIQUE KEY `grp_cat_map_uk` (`group_id`,`category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `xlogin` (
  `login_track_id` int(11) NOT NULL AUTO_INCREMENT,
  `login_name` varchar(100) NOT NULL,
  `creation_date` datetime NOT NULL,
  PRIMARY KEY (`login_track_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `xmembership_plan` (
  `plan_id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_code` varchar(100) NOT NULL,
  `plan_name` varchar(100) NOT NULL,
  `base_price` double DEFAULT NULL,
  `plan_period` varchar(30) DEFAULT NULL,
  `active` varchar(1) NOT NULL,
  `creation_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `last_update_date` datetime NOT NULL,
  `last_updated_by` int(11) NOT NULL,
  `sort_val` int(11) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`plan_id`),
  UNIQUE KEY `plan_name` (`plan_name`),
  UNIQUE KEY `plan_code` (`plan_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `xmembership_plan_detail` (
  `plan_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_id` int(11) NOT NULL,
  `object_type_id` int(11) NOT NULL,
  `object_max_count` int(11) NOT NULL,
  `active` varchar(1) NOT NULL,
  `creation_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `last_update_date` datetime NOT NULL,
  `last_updated_by` int(11) NOT NULL,
  `sort_val` int(11) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`plan_detail_id`),
  UNIQUE KEY `plan_detail_uk` (`plan_id`,`object_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `xobject` (
  `object_id` int(11) NOT NULL AUTO_INCREMENT,
  `object_code` varchar(100) NOT NULL,
  `object_type_id` int(11) NOT NULL,
  `object_name` varchar(100) NOT NULL,
  `active` varchar(1) NOT NULL,
  `creation_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `last_update_date` datetime NOT NULL,
  `last_updated_by` int(11) NOT NULL,
  `sort_val` int(11) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `attrib1` varchar(100) DEFAULT NULL,
  `attrib2` varchar(100) DEFAULT NULL,
  `attrib3` varchar(100) DEFAULT NULL,
  `locked_by_name` varchar(100) DEFAULT NULL,
  `locked_date` datetime DEFAULT NULL,
  `is_public` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`object_id`),
  UNIQUE KEY `object_name` (`object_code`,`created_by`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `xobject_category_map` (
  `object_category_map_id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `active` varchar(1) NOT NULL,
  `creation_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `last_update_date` datetime NOT NULL,
  `last_updated_by` int(11) NOT NULL,
  `sort_val` int(11) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`object_category_map_id`),
  UNIQUE KEY `category_map_uk` (`category_id`,`object_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `xobject_share` (
  `object_share_id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) NOT NULL,
  `shared_by_user_id` int(11) NOT NULL,
  `shared_to_category_id` int(11) NOT NULL,
  `access_role_id` int(11) NOT NULL,
  `creation_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `last_update_date` datetime NOT NULL,
  `last_updated_by` int(11) NOT NULL,
  `active` varchar(1) NOT NULL DEFAULT 'Y',
  `sort_val` int(11) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`object_share_id`),
  UNIQUE KEY `username` (`object_id`,`shared_to_category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `xobject_type` (
  `object_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `object_type_name` varchar(50) NOT NULL,
  `object_type_code` varchar(50) NOT NULL,
  `base_path` varchar(250) DEFAULT NULL,
  `command_template` varchar(200) DEFAULT NULL,
  `active` varchar(1) NOT NULL,
  `creation_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `last_update_date` datetime NOT NULL,
  `last_updated_by` int(11) NOT NULL,
  `sort_val` int(11) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`object_type_id`),
  UNIQUE KEY `object_type_name` (`object_type_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `xplan_subscription` (
  `plan_subscription_id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `creation_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `last_update_date` datetime NOT NULL,
  `last_updated_by` int(11) NOT NULL,
  `sort_val` int(11) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`plan_subscription_id`),
  UNIQUE KEY `plan_user_uk` (`plan_id`,`user_id`,`start_date`,`end_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `xprivilege` (
  `privilege_id` int(11) NOT NULL AUTO_INCREMENT,
  `privilege_code` varchar(100) NOT NULL,
  `privilege_name` varchar(100) NOT NULL,
  `Description` varchar(200) NOT NULL,
  `created_by` int(11) NOT NULL,
  `creation_date` datetime NOT NULL,
  `last_updated_by` int(11) NOT NULL,
  `last_update_date` datetime NOT NULL,
  `sort_val` int(11) DEFAULT NULL,
  PRIMARY KEY (`privilege_id`),
  UNIQUE KEY `privilege_code` (`privilege_code`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `xrole` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_code` varchar(100) NOT NULL,
  `role_name` varchar(100) NOT NULL,
  `level` int(11) NOT NULL,
  `active` varchar(1) NOT NULL DEFAULT 'Y',
  `default_url_after_login` varchar(255) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `last_update_date` datetime NOT NULL,
  `last_updated_by` int(11) NOT NULL,
  `sort_val` int(11) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `role_name` (`role_name`),
  UNIQUE KEY `role_name_2` (`role_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `xrole_privilege_map` (
  `role_privilege_map_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `privilege_id` int(11) NOT NULL,
  `active` varchar(1) NOT NULL,
  `creation_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `last_update_date` datetime NOT NULL,
  `last_updated_by` int(11) NOT NULL,
  `sort_val` int(11) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`role_privilege_map_id`),
  UNIQUE KEY `role_map_uk` (`role_id`,`privilege_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `xuser` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `role_id` int(11) NOT NULL,
  `password` varchar(250) NOT NULL,
  `creation_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `last_update_date` datetime NOT NULL,
  `last_updated_by` int(11) NOT NULL,
  `ip_address` varchar(300) NOT NULL,
  `api_id` varchar(250) DEFAULT NULL,
  `active` varchar(1) NOT NULL DEFAULT 'Y',
  `sort_val` int(11) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`,`email`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username_2` (`username`),
  UNIQUE KEY `username_3` (`username`,`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `xuser_group_map` (
  `user_group_map_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `active` varchar(1) NOT NULL,
  `creation_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `last_update_date` datetime NOT NULL,
  `last_updated_by` int(11) NOT NULL,
  `sort_val` int(11) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_group_map_id`),
  UNIQUE KEY `user_group_map` (`user_id`,`group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `xconnection` (
  `connection_id` int(11) NOT NULL AUTO_INCREMENT,
  `connection_handle` varchar(100) NOT NULL,
  `connection_type` varchar(50) NOT NULL,
  `connection_category` varchar(100) NOT NULL,
  `active` varchar(1) NOT NULL,
  `creation_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `last_update_date` datetime NOT NULL,
  `last_updated_by` int(11) NOT NULL,
  `sort_val` int(11) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `attrib1` varchar(100) DEFAULT NULL,
  `attrib2` varchar(100) DEFAULT NULL,
  `attrib3` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`connection_id`),
  UNIQUE KEY `connection_uk` (`connection_handle`,`created_by`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `xsummary` (
  `username` varchar(100) NOT NULL,
  `metric_name` varchar(100) NOT NULL,
  `metric_type` varchar(100) NOT NULL,
  `metric_value` int(11) NOT NULL,
  `creation_date` datetime NOT NULL,
  UNIQUE KEY `xsummary_uk` (`username`,`metric_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `xjob` (
  `job_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_name` varchar(255) NOT NULL,
  `job_type` varchar(255) NOT NULL,
  `load_type` varchar(100) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `load_status` varchar(100) NOT NULL,
  `incremental_duration` int(11) NOT NULL DEFAULT '1440',
  `last_run_date` datetime NOT NULL,
  `last_run_rows` int(11) NOT NULL,
  `last_run_status` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `creation_date` date NOT NULL,
  `last_update_date` datetime NOT NULL,
  `username` varchar(100) NOT NULL,
  `connection_handler` varchar(200) NOT NULL,
  `tablestore` varchar(150) NOT NULL,
  `extrainfo1` text NOT NULL,
  `extrainfo2` text NOT NULL,
  `active` varchar(10) DEFAULT 'Y',
  PRIMARY KEY (`job_id`),
  UNIQUE KEY `job_name` (`job_name`,`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `xjob_raw_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `load_status` varchar(100) NOT NULL,
  `raw_file_name` varchar(200) NOT NULL,
  `creation_date` datetime NOT NULL,
  `last_update_date` datetime NOT NULL,
  `username` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `xuser_info` (
  `userinfo_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `tag_name` varchar(150) NOT NULL,
  `tag_value` varchar(150) NOT NULL,
  `creation_date` date NOT NULL,
    `created_by` int(11) NOT NULL,
  `last_update_date` date NOT NULL,
  `last_updated_by` int(11) NOT NULL,
  PRIMARY KEY (`userinfo_id`),
  UNIQUE KEY `connection_uk` (`username`,`tag_name`)  
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;