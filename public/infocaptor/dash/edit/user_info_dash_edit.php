<?php
define('xmember_doc',1);

include "xmember_api.php";
init();

$qlet['userinfo_q33']['insert_template']=<<<EOD
INSERT INTO `user_info` (
`user_id`,
 `username`,
 `first_name`,
 `last_name`,
 `email`,
 `role_id`,
 `password`,
 `creation_date`,
 `created_by`,
 `last_update_date`,
 `last_updated_by`,
 `ip_address`,
 `active`) 
 VALUES (
 NULL,
 'UVAL['username']',
 'UVAL['first_name']',
 'UVAL['last_name']',
 'UVAL['email']',
 'UVAL['role_id']',
 'UVAL['password']',
 now() ,
 'UVAL['created_by']',
 now() ,
 'UVAL['last_updated_by']',
 'UVAL['ip_address']',
 ifnull('UVAL['active']','Y')
 
 )

EOD;
 
$qlet['userinfo_q33']['update_template']=<<<EOD
update `user_info`
set 
 `first_name`= 'UVAL['first_name']'
, `last_name`= 'UVAL['last_name']'
, `email`= 'UVAL['email']'
, `role_id`= 'UVAL['role_id']'
, `password`= 'UVAL['password']'
, `last_update_date`= now() 
, `last_updated_by`= 'UVAL['last_updated_by']'
, `ip_address`= 'UVAL['ip_address']'
, `active`= 'UVAL['active']'

where user_id = 'UVAL['user_id']'
EOD;

$qlet['userinfo_q33']['delete_template']=<<<EOD
update user_info
set active='N', 
, `last_update_date`= now() 
where user_id = 'UVAL['user_id']
EOD;


function insert_sql($UVAL,$db)
{

$sql="
INSERT INTO `user_info` (
`user_id`,
 `username`,
 `first_name`,
 `last_name`,
 `email`,
 `role_id`,
 `password`,
 `creation_date`,
 `created_by`,
 `last_update_date`,
 `last_updated_by`,
 `ip_address`,
 `active`) 
 VALUES (
 NULL,
 '{$UVAL['username']}',
 '{$UVAL['first_name']}',
 '{$UVAL['last_name']}',
 '{$UVAL['email']}',
 '{$UVAL['role_id']}',
 '{$UVAL['password']}',
 now() ,
 '{$UVAL['created_by']}',
 now() ,
 '{$UVAL['last_updated_by']}',
 '{$UVAL['ip_address']}',
 ifnull('{$UVAL['active']}','Y')
 
 )
 ";
 
 return $sql;
}

function insert_action($p_values)
{
$_SESSION["insert_action"]="yes";

  $sql=add_user($p_values);
}

function update_action($p_values,$db)
{
}

function delete_action($p_values,$db)
{
}

function post_action($p_values,$db)
{



  
}



?>