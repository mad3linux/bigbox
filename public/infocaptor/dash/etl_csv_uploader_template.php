

$etl["database"]="@@DATABASE@@";

$etl["job_name"]="@@JOB_NAME@@";
$etl["source_type"]="@@SOURCE_TYPE@@";

$etl["source_data"]="@@SOURCE_DATA@@";



$etl["last_row_tracker_column"]="RowId";
$etl["last_row_tracker_init_val"]=0;

$etl["skip_top_rows"]=1;
$etl["load_size"]=@@PROCESSED_ROWS@@;
$etl["target_table"]="@@TARGET_TABLE@@";

$etl["discard_row"]="Y";

$etl["table_refresh_rule"]="@@INSERT_UPDATE_MODE@@";
$etl["run_retention_rule"] = "Y";

function xet_delete_old_data_fn()
{

}

function xet_row_discard_fn($source_row) 
{
	  if (empty($source_row[0])) return true;
}


function xet_get_source_csv_handle()
{

}


function xet_get_source_columns()
{
global $etl;
$etl["source_columns"]=@@SOURCE_COLUMNS@@;
}



function xet_get_select_cols()
{
$sel_cols=array(
);

return implode(",",$sel_cols);

}

function xet_get_last_value()
{

  global $etl;
  $etvar_file=$etl["base_dir"]."/".$etl["job_name"]."_var.php";
   if (file_exists($etvar_file))
   {
		include "{$etvar_file}";
		$etl["last_val"]=$etl_var[$etl["last_row_tracker_column"]]; 
   }
   else
   {
		$etl["last_val"]=$etl["last_row_tracker_init_val"];
   }
}

function xet_get_where_sql()
{
  global $etl;
  
  xet_get_last_value();
  elog(0, "last value = ".$etl['last_val']."<br>");
  $where =  "where B is not null 
and A>{$etl["last_val"]}
 limit {$etl["load_size"]}";
return $where;
}

function xet_get_encoded_sql()
{
	$select_cols=xet_get_select_cols();
	$where= xet_get_where_sql();

	$final_sql="select ".$select_cols." ".$where;
	$encoded_sql=urlencode($final_sql);
	
	return $encoded_sql;
}

function xet_get_final_url($url)
{

return $url;

  $encoded_sql=xet_get_encoded_sql();
  $final_url=$url."&tq=".$encoded_sql;
  return $final_url;
}





function xet_track_last_value($data)
{
/*an indexed list of values are sent and this function needs to decide which value is being used for last tracking*/
global $etl;
	$etl["last_row_tracker_init_val"]=$data[0];
}

function xet_update_last_value()
{
/*
global $etl;
  $etvar_file=$etl["base_dir"]."/".$etl["job_name"]."_var.php";
	$fd = fopen($etvar_file, "w");
	fwrite($fd,"<?php\n\n");

		   fwrite($fd,"\$etl_var['{$etl["last_row_tracker_column"]}']={$etl["last_row_tracker_init_val"]};\n");
		   

	fwrite($fd,"\n?>");
	fclose($fd);
	*/
}

function xet_get_target_columns()
{
global $etl;
@@TARGET_COLUMNS@@

}

function xet_build_function_map()
{
global $etl;

@@FUNCTION_MAPPINGS@@

}

//make sure the function returns the value surrounded by single quote if a static value is sent. For mysql function calls do not surround with quotes

@@LIST_OF_FUNCTIONS@@

function xet_currentDate_fn($str,$sdata)
{
  return " now() ";
}

function xet_service_date_fn($str,$sdata) 
{
//"Year",
//"Month",
  return "STR_TO_DATE('{$str}','%m-%d-%Y %H:%i:%s')"; 

 //return $sdata["Year"]."-".$sdata["Month"]."-01";
  
}

function xet_format1_date_fn($str,$sdata) 
{
//"Year",
//"Month",
   return "STR_TO_DATE('{$str}','%m/%d/%Y')"; 

 //return $sdata["Year"]."-".$sdata["Month"]."-01";
  
}

function xet_format2_date_fn($str,$sdata) 
{
//"Year",
//"Month",

  return "CAST(DATE_FORMAT(STR_TO_DATE('{$str}','%m/%d/%Y') ,'%Y-%m-01') as DATE)"; 

 //return $sdata["Year"]."-".$sdata["Month"]."-01";
  
}

function xet_format3_date_fn($str,$sdata) 
{
//"Year",
//"Month",
  $dStr="1-{$sdata["month"]}-{$sdata["year"]}";
  return "STR_TO_DATE('{$dStr}','%d-%M-%Y')"; 

 //return $sdata["Year"]."-".$sdata["Month"]."-01";
  
}

function xet_dateTime_fn($datestr,$sdata)
{
  return date_format(date_create_from_format('j/m/Y H:i:s', $datestr), 'Y-m-d H:i:s');
}

function xet_date_fn($datestr,$sdata)
{
  return date_format(date_create_from_format('j/m/Y', $datestr), 'Y-m-d');
}

function xet_CompletedAt_lat_fn($str,$sdata)
{
  if ($str=="Unknown") return $str;
	$ary=explode("-", $str);
  return trim($ary[0]);
}

function xet_CompletedAt_lon_fn($str,$sdata)
{
  if ($str=="Unknown") return $str;
	$ary=explode("-", $str);
  return trim($ary[1]);

}

function xet_str_cleanup_fn($str,$sdata)
{

  return "'".str_replace(array(",","'")," ",$str)."'";
  

}
function xet_pre_task($base_dir,$file)
{

//@@PRE_TASK_SQL@@

}
