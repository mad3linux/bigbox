<?php

echo<<<SOD
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 

<html xmlns="http://www.w3.org/1999/xhtml"> 

<head> 

<title>InfoCaptor - License Agreement</title>
</head>

<body>
<pre>
End User License Agreement for InfoCaptor
Copyright, RudraSoft LLC
http://www.InfoCaptor.com

This is a LEGAL document and is an agreement between 'You' as a user of InfoCaptor and 
RudraSoft LLC. 


Please read this 'Agreement' carefully before using this software. Your use of this software
indicates your acceptance of this license agreement and warranty.

If you do not agree to the licensing terms in this document then you should immediately DELETE
all the executable and related files to InfoCaptor.


Evaluation version of this product may be used for only evaluation or Demo purpose only.

   For further licensing options check http://www.InfoCaptor.com

It is ILLEGAL for the evaluator, user, buyer or owner of license for InfoCaptor to attempt to reverse
engineer, disassemble, decompile or change the InfoCaptor executable files in any way.

The evaluation version of InfoCaptor may be freely distributed, provided the distribution package is
not modified. No person or company may charge a fee for the distribution of InfoCaptor without written
permission from the copyright holder.

Rudrasoft.com and the copyright holder of InfoCaptor reserves all rights not expressly granted here.

        Software is a complex piece with lot of dependencies on the user's computer settings, operating
        system settings, personal preferences, current installed software on user's computer including hidden viruses
        and adware programs. This product is tested on a variety of computers and different user configurations.
        Although the owners of this product have a excellent development and quality procedures, because of the complex
        nature of software, the owners and the copyright holders of this product are not responsible for any malfunction or defective behavior.

THE ENTIRE RISK AS TO THE QUALITY, BEHAVIOR, FUNCTIONALITY AND PERFORMANCE OF THE SOFTWARE IS ENTIRELY WITH
YOU AS THE USER OF THIS SOFTWARE. SHOULD THE SOFTWARE PROVE DEFECTIVE, YOU AS THE USER OF THIS SOFTWARE ASSUME
THE COST OF ALL NECESSARY DAMAGES.      

THIS SOFTWARE IS PROVIDED 'AS IS' WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, 
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.


RUDRASOFT LLC AND THE COPYRIGHT OWNER OF InfoCaptor IS NOT RESPONSIBLE FOR ANY DAMAGES WHATSOEVER, 
INCLUDING LOSS OF INFORMATION, INTERRUPTION OF BUSINESS, PERSONAL INJURY AND/OR ANY DAMAGE OR 
CONSEQUENTIAL DAMAGE WITHOUT LIMITATION, INCURRED BEFORE, DURING OR AFTER THE USE OF OUR PRODUCTS.

If you do not agree with the terms of this license or if the terms of this license contradict with 
your local laws, you must remove InfoCaptor files from your computer/server and/or storage devices and cease
to use the product.   



InfoCaptor uses several third party software and an effort is made below to recognize all the software licenses and the 
contributors. We say thank you to all the contributors who provide the following beautiful and immensely useful software without which 
InfoCaptor would not be able to see through completion. 

List of software 
-PHP
-MySQL
-Apache Web Server
-Server2Go
-jQuery
-jQuery plugins
-jQuery UI
-jQuery Themes
-TCPDF 



/*
 * jQuery Hotkeys Plugin
 * Copyright 2010, John Resig
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * Based upon the plugin by Tzury Bar Yochay:
 * http://github.com/tzuryby/hotkeys
 *
 * Original idea by:
 * Binny V A, http://www.openjs.com/scripts/events/keyboard_shortcuts/
*/

/**
 * jscolor, JavaScript Color Picker
 *
 * @version 1.3.1
 * @license GNU Lesser General Public License, http://www.gnu.org/copyleft/lesser.html
 * @author  Jan Odvarko, http://odvarko.cz
 * @created 2008-06-15
 * @updated 2010-01-23
 * @link    http://jscolor.com
 */
 
 /*! Copyright (c) 2008 Brandon Aaron (brandon.aaron@gmail.com || http://brandonaaron.net)
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php) 
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 */

 /**
 * Copyright (c)2005-2009 Matt Kruse (javascripttoolbox.com)
 * 
 * Dual licensed under the MIT and GPL licenses. 
 * This basically means you can use this code however you want for
 * free, but don't claim to have written it yourself!
 * Donations always accepted: http://www.JavascriptToolbox.com/donate/
 * 
 * Please do not link to the .js files on javascripttoolbox.com from
 * your site. Copy the files locally to your server instead.
 * 
 */

 /**
 * @preserve jquery.layout 1.3.0 - Release Candidate 29.12
 * $Date: 2010-11-12 08:00:00 (Thu, 18 Nov 2010) $
 * $Rev: 302912 $
 *
 * Copyright (c) 2010 
 *   Fabrizio Balliano (http://www.fabrizioballiano.net)
 *   Kevin Dalman (http://allpro.net)
 *
 * Dual licensed under the GPL (http://www.gnu.org/licenses/gpl.html)
 * and MIT (http://www.opensource.org/licenses/mit-license.php) licenses.
 *
 * Docs: http://layout.jquery-dev.net/documentation.html
 * Tips: http://layout.jquery-dev.net/tips.html
 * Help: http://groups.google.com/group/jquery-ui-layout
 */

Some of the software above uses either of the following licenses
http://www.gnu.org/licenses/lgpl.txt
http://www.opensource.org/licenses/mit-license.php
http://www.apache.org/licenses/LICENSE-2.0.html

</pre>   
</body>
</html>
SOD;

?>