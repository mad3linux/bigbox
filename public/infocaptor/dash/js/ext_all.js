/*
 * jQuery Hotkeys Plugin
 * Copyright 2010, John Resig
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * Based upon the plugin by Tzury Bar Yochay:
 * http://github.com/tzuryby/hotkeys
 *
 * Original idea by:
 * Binny V A, http://www.openjs.com/scripts/events/keyboard_shortcuts/
*/

(function(jQuery){
	
	jQuery.hotkeys = {
		version: "0.8",

		specialKeys: {
			8: "backspace", 9: "tab", 13: "return", 16: "shift", 17: "ctrl", 18: "alt", 19: "pause",
			20: "capslock", 27: "esc", 32: "space", 33: "pageup", 34: "pagedown", 35: "end", 36: "home",
			37: "left", 38: "up", 39: "right", 40: "down", 45: "insert", 46: "del", 
			96: "0", 97: "1", 98: "2", 99: "3", 100: "4", 101: "5", 102: "6", 103: "7",
			104: "8", 105: "9", 106: "*", 107: "+", 109: "-", 110: ".", 111 : "/", 
			112: "f1", 113: "f2", 114: "f3", 115: "f4", 116: "f5", 117: "f6", 118: "f7", 119: "f8", 
			120: "f9", 121: "f10", 122: "f11", 123: "f12", 144: "numlock", 145: "scroll", 191: "/", 224: "meta"
		},
	
		shiftNums: {
			"`": "~", "1": "!", "2": "@", "3": "#", "4": "$", "5": "%", "6": "^", "7": "&", 
			"8": "*", "9": "(", "0": ")", "-": "_", "=": "+", ";": ": ", "'": "\"", ",": "<", 
			".": ">",  "/": "?",  "\\": "|"
		}
	};

	function keyHandler( handleObj ) {
		// Only care when a possible input has been specified
		if ( typeof handleObj.data !== "string" ) {
			return;
		}
		
		var origHandler = handleObj.handler,
			keys = handleObj.data.toLowerCase().split(" ");
	
		handleObj.handler = function( event ) {
			// Don't fire in text-accepting inputs that we didn't directly bind to
			if ( this !== event.target && (/textarea|select/i.test( event.target.nodeName ) ||
				 event.target.type === "text") ) {
				return;
			}
			
			// Keypress represents characters, not special keys
			var special = event.type !== "keypress" && jQuery.hotkeys.specialKeys[ event.which ],
				character = String.fromCharCode( event.which ).toLowerCase(),
				key, modif = "", possible = {};

			// check combinations (alt|ctrl|shift+anything)
			if ( event.altKey && special !== "alt" ) {
				modif += "alt+";
			}

			if ( event.ctrlKey && special !== "ctrl" ) {
				modif += "ctrl+";
			}
			
			// TODO: Need to make sure this works consistently across platforms
			if ( event.metaKey && !event.ctrlKey && special !== "meta" ) {
				modif += "meta+";
			}

			if ( event.shiftKey && special !== "shift" ) {
				modif += "shift+";
			}

			if ( special ) {
				possible[ modif + special ] = true;

			} else {
				possible[ modif + character ] = true;
				possible[ modif + jQuery.hotkeys.shiftNums[ character ] ] = true;

				// "$" can be triggered as "Shift+4" or "Shift+$" or just "$"
				if ( modif === "shift+" ) {
					possible[ jQuery.hotkeys.shiftNums[ character ] ] = true;
				}
			}

			for ( var i = 0, l = keys.length; i < l; i++ ) {
				if ( possible[ keys[i] ] ) {
					return origHandler.apply( this, arguments );
				}
			}
		};
	}

	jQuery.each([ "keydown", "keyup", "keypress" ], function() {
		jQuery.event.special[ this ] = { add: keyHandler };
	});

})( jQuery );(function($){
	$.ui.resizable.prototype._mouseStart = function(event){
		var num = function(v) {
			return parseInt(v, 10) || 0;
		};
   
		var o = this.options, iniPos = this.element.position(), el = this.element;

		this.resizing = true;
		this.documentScroll = { top: $(document).scrollTop(), left: $(document).scrollLeft() };

		if (el.is('.ui-draggable') || (/absolute/).test(el.css('position'))) {
			el.css({ position: 'absolute', top: el.position.top, left: el.position.left });
		}
/*
		if ($.browser.opera && (/relative/).test(el.css('position')))
			el.css({ position: 'relative', top: 'auto', left: 'auto' });
*/
		this._renderProxy();

		var curleft = num(this.helper.css('left')), curtop = num(this.helper.css('top'));

		if (o.containment) {
			curleft += $(o.containment).scrollLeft() || 0;
			curtop += $(o.containment).scrollTop() || 0;
		}

		this.offset = this.helper.offset();
		this.position = { left: curleft, top: curtop };
		this.size = this._helper ? { width: el.outerWidth(), height: el.outerHeight() } : { width: el.width(), height: el.height() };
		this.originalSize = this._helper ? { width: el.outerWidth(), height: el.outerHeight() } : { width: el.width(), height: el.height() };
		this.originalPosition = { left: curleft, top: curtop };
		this.sizeDiff = { width: el.outerWidth() - el.width(), height: el.outerHeight() - el.height() };
		this.originalMousePosition = { left: event.pageX, top: event.pageY };

		this.aspectRatio = (typeof o.aspectRatio == 'number') ? o.aspectRatio : ((this.originalSize.width / this.originalSize.height) || 1);

	    var cursor = $('.ui-resizable-' + this.axis).css('cursor');
	    $('body').css('cursor', cursor == 'auto' ? this.axis + '-resize' : cursor);

		el.addClass("ui-resizable-resizing");
		this._propagate("start", event);
		return true;
   }
})(jQuery);


/*! Copyright (c) 2008 Brandon Aaron (brandon.aaron@gmail.com || http://brandonaaron.net)
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php) 
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 */

/**
 * Gets the width of the OS scrollbar
 */
(function($) {
	var scrollbarWidth = 0;
	$.getScrollbarWidth = function() {
		if ( !scrollbarWidth ) {
			/* commented on 5/12/14 as $.browser is not supported with jquery anymore
			if ( $.browser.msie ) {
				var $textarea1 = $('<textarea cols="10" rows="2"></textarea>')
						.css({ position: 'absolute', top: -1000, left: -1000 }).appendTo('body'),
					$textarea2 = $('<textarea cols="10" rows="2" style="overflow: hidden;"></textarea>')
						.css({ position: 'absolute', top: -1000, left: -1000 }).appendTo('body');
				scrollbarWidth = $textarea1.width() - $textarea2.width();
				$textarea1.add($textarea2).remove();
			} else */
			{
				var $div = $('<div />')
					.css({ width: 100, height: 100, overflow: 'auto', position: 'absolute', top: -1000, left: -1000 })
					.prependTo('body').append('<div />').find('div')
						.css({ width: '100%', height: 200 });
				scrollbarWidth = 100 - $div.width();
				$div.parent().remove();
			}
		}
		return scrollbarWidth;
	};
})(jQuery);/**
 * Copyright (c)2005-2009 Matt Kruse (javascripttoolbox.com)
 * 
 * Dual licensed under the MIT and GPL licenses. 
 * This basically means you can use this code however you want for
 * free, but don't claim to have written it yourself!
 * Donations always accepted: http://www.JavascriptToolbox.com/donate/
 * 
 * Please do not link to the .js files on javascripttoolbox.com from
 * your site. Copy the files locally to your server instead.
 * 
 */
/**
 * jquery.contextmenu.js
 * jQuery Plugin for Context Menus
 * http://www.JavascriptToolbox.com/lib/contextmenu/
 *
 * Copyright (c) 2008 Matt Kruse (javascripttoolbox.com)
 * Dual licensed under the MIT and GPL licenses. 
 *
 * @version 1.1
 * @history 1.1 2010-01-25 Fixed a problem with 1.4 which caused undesired show/hide animations
 * @history 1.0 2008-10-20 Initial Release
 * @todo slideUp doesn't work in IE - because of iframe?
 * @todo Hide all other menus when contextmenu is shown?
 * @todo More themes
 * @todo Nested context menus
 */
;(function($){
	$.contextMenu = {
		shadow:true,
		shadowOffset:0,
		shadowOffsetX:5,
		shadowOffsetY:5,
		shadowWidthAdjust:-3,
		shadowHeightAdjust:-3,
		shadowOpacity:.2,
		shadowClass:'context-menu-shadow',
		shadowColor:'black',

		offsetX:0,
		offsetY:0,
		appendTo:'body',
		direction:'down',
		constrainToScreen:true,
				
		showTransition:'show',
		hideTransition:'hide',
		showSpeed:null,
		hideSpeed:null,
		showCallback:null,
		hideCallback:null,
		
		disableAll:false,
		className:'context-menu',
		itemClassName:'context-menu-item',
		itemHoverClassName:'context-menu-item-hover',
		disabledItemClassName:'context-menu-item-disabled',
		disabledItemHoverClassName:'context-menu-item-disabled-hover',
		separatorClassName:'context-menu-separator',
		innerDivClassName:'context-menu-item-inner',
		themePrefix:'context-menu-theme-',
		theme:'default',

		separator:'context-menu-separator', // A specific key to identify a separator
		target:null, // The target of the context click, to be populated when triggered
		clickX:0,
		clickY:0,
		menu:null, // The jQuery object containing the HTML object that is the menu itself
		shadowObj:null, // Shadow object
		bgiframe:null, // The iframe object for IE6
		shown:false, // Currently being shown?
		normalMenu:false,
		useIframe:/*@cc_on @*//*@if (@_win32) true, @else @*/false,/*@end @*/ // This is a better check than looking at userAgent!
		menuList:null,
		// Create the menu instance
		create: function(menu,opts) {
			var cmenu = $.extend({},this,opts); // Clone all default properties to created object
			
			// If a selector has been passed in, then use that as the menu
			if (typeof menu=="string") {
				cmenu.menu = $(menu);
			} 
			// If a function has been passed in, call it each time the menu is shown to create the menu
			else if (typeof menu=="function") {
				cmenu.menuFunction = menu;
			}
			// Otherwise parse the Array passed in
			else {
				cmenu.menu = cmenu.createMenu(menu,cmenu);
			}
			if (cmenu.menu) {
				cmenu.menu.css({display:'none'});
				$(cmenu.appendTo).append(cmenu.menu);
			}
			
			// Create the shadow object if shadow is enabled
			if (cmenu.shadow) {
				cmenu.createShadow(cmenu); // Extracted to method for extensibility
				if (cmenu.shadowOffset) { cmenu.shadowOffsetX = cmenu.shadowOffsetY = cmenu.shadowOffset; }
			}
			$('body').bind('contextmenu',function(){cmenu.hide();}); // If right-clicked somewhere else in the document, hide this menu
			return cmenu;
		},
		
		// Create an iframe object to go behind the menu
		createIframe: function() {
		    return $('<iframe frameborder="0" tabindex="-1" src="javascript:false" style="display:block;position:fixed;z-index:-1;filter:Alpha(Opacity=0);"/>');
		},
		
		// Accept an Array representing a menu structure and turn it into HTML
		createMenu: function(menu,cmenu) {
			var className = cmenu.className;
			$.each(cmenu.theme.split(","),function(i,n){className+=' '+cmenu.themePrefix+n});
			var $t = $('<table cellspacing=0 cellpadding=0></table>').click(function(){cmenu.hide(); return false;}); // We wrap a table around it so width can be flexible
			var $tr = $('<tr></tr>');
			var $td = $('<td></td>');
			var $div = $('<div class="'+className+'"></div>');
			//var $button = $('<div><input type="button" name="test"></div');
			// Each menu item is specified as either:
			//     title:function
			// or  title: { property:value ... }
			for (var i=0; i<menu.length; i++) {
				var m = menu[i];
				if (m==$.contextMenu.separator) {
					$div.append(cmenu.createSeparator());
				}
				else {
					for (var opt in menu[i]) {
						$div.append(cmenu.createMenuItem(opt,menu[i][opt])); // Extracted to method for extensibility
					}
				}
			}
			//$div.append($button);
			if ( cmenu.useIframe ) {
				$td.append(cmenu.createIframe());
			}
			$t.append($tr.append($td.append($div)))
			return $t;
		},
		hideAll : function()
		{
		  //alert("total menus - " + $.contextMenu.menuList.length);
		  for (var i=0;i<$.contextMenu.menuList.length;i++)
		  {
		    $.contextMenu.menuList[i].hide();
		  }
		  
		},
		// Create an individual menu item
		createMenuItem: function(label,obj) {
			var cmenu = this;
			if (typeof obj=="function") { obj={onclick:obj}; } // If passed a simple function, turn it into a property of an object
			// Default properties, extended in case properties are passed
			var o = $.extend({
				onclick:function() { },
				className:'',
				hoverClassName:cmenu.itemHoverClassName,
				icon:'',
				disabled:false,
				title:'',
				hoverItem:cmenu.hoverItem,
				hoverItemOut:cmenu.hoverItemOut
			},obj);
			// If an icon is specified, hard-code the background-image style. Themes that don't show images should take this into account in their CSS
			var iconStyle = (o.icon)?'background-image:url('+o.icon+');':'';
			var $div = $('<div class="'+cmenu.itemClassName+' '+o.className+((o.disabled)?' '+cmenu.disabledItemClassName:'')+'" title="'+o.title+'"></div>')
							// If the item is disabled, don't do anything when it is clicked
							.click(function(e){if(cmenu.isItemDisabled(this)){return false;}else{return o.onclick.call(cmenu.target,this,cmenu,e)}})
							// Change the class of the item when hovered over
							.hover( function(){ o.hoverItem.call(this,(cmenu.isItemDisabled(this))?cmenu.disabledItemHoverClassName:o.hoverClassName); }
									,function(){ o.hoverItemOut.call(this,(cmenu.isItemDisabled(this))?cmenu.disabledItemHoverClassName:o.hoverClassName); }
							);
			var $idiv = $('<div class="'+cmenu.innerDivClassName+'" style="'+iconStyle+'">'+label+'</div>');
			$div.append($idiv);
			return $div;
		},
		
		// Create a separator row
		createSeparator: function() {
			return $('<div class="'+this.separatorClassName+'"></div>');
		},
		
		// Determine if an individual item is currently disabled. This is called each time the item is hovered or clicked because the disabled status may change at any time
		isItemDisabled: function(item) { return $(item).is('.'+this.disabledItemClassName); },
		
		// Functions to fire on hover. Extracted to methods for extensibility
		hoverItem: function(c) { $(this).addClass(c); },
		hoverItemOut: function(c) { $(this).removeClass(c); },
		
		// Create the shadow object
		createShadow: function(cmenu) {
			cmenu.shadowObj = $('<div class="'+cmenu.shadowClass+'"></div>').css( {display:'none',position:"fixed", zIndex:9998, opacity:cmenu.shadowOpacity, backgroundColor:cmenu.shadowColor } );
			$(cmenu.appendTo).append(cmenu.shadowObj);
		},
		
		// Display the shadow object, given the position of the menu itself
		showShadow: function(x,y,e) {
			var cmenu = this;
			if (cmenu.shadow) {
				cmenu.shadowObj.css( {
					width:(cmenu.menu.width()+cmenu.shadowWidthAdjust)+"px", 
					height:(cmenu.menu.height()+cmenu.shadowHeightAdjust)+"px", 
					top:(y+cmenu.shadowOffsetY)+"px", 
					left:(x+cmenu.shadowOffsetX)+"px"
				}).addClass(cmenu.shadowClass)[cmenu.showTransition](cmenu.showSpeed);
			}
		},
		
		// A hook to call before the menu is shown, in case special processing needs to be done.
		// Return false to cancel the default show operation
		beforeShow: function() 
		{ 
		if ($.contextMenu.disableAll)
			return false;
		else return true; 
		},
		
		// Show the context menu
		show: function(t,e) {
		//hide if any other menus popped and showing
		$.contextMenu.hideAll();
			var cmenu=this, x=e.pageX, y=e.pageY;
			
			//adjust scrollheight
			//var centerBox=$('html'); commented on 4/23/2015, returns zero value for scrolltop y position in chrome
			var centerBox=$(window);
			var st=centerBox.scrollTop();
			var sl=centerBox.scrollLeft();
			x-=sl;
			y-=st;
			
			cmenu.target = t; // Preserve the object that triggered this context menu so menu item click methods can see it
			$.contextMenu.target=t;	
			$.contextMenu.clickX=x;
			$.contextMenu.clickY=y;
			
			
			if (cmenu.beforeShow()!==false) {
				// If the menu content is a function, call it to populate the menu each time it is displayed
				if (cmenu.menuFunction) {
					if (cmenu.menu) { $(cmenu.menu).remove(); }
					cmenu.menu = cmenu.createMenu(cmenu.menuFunction(cmenu,t),cmenu);
					cmenu.menu.css({display:'none'});
					$(cmenu.appendTo).append(cmenu.menu);
				}
				var $c = cmenu.menu;
				x+=cmenu.offsetX; y+=cmenu.offsetY;
				var pos = cmenu.getPosition(x,y,cmenu,e); // Extracted to method for extensibility
				cmenu.showShadow(pos.x,pos.y,e);
				// Resize the iframe if needed
				if (cmenu.useIframe) {
					$c.find('iframe').css({width:$c.width()+cmenu.shadowOffsetX+cmenu.shadowWidthAdjust,height:$c.height()+cmenu.shadowOffsetY+cmenu.shadowHeightAdjust});
				}
				/*NJ: changed the position to fixed from absolute to prevent the scrolling of menu when the body scrolls */
				$c.css( {top:pos.y+"px", left:pos.x+"px", position:"fixed",zIndex:9999} )[cmenu.showTransition](cmenu.showSpeed,((cmenu.showCallback)?function(){cmenu.showCallback.call(cmenu);}:null));
				cmenu.shown=true;
				$(document).one('click',null,function(){cmenu.hide()}); // Handle a single click to the document to hide the menu
			}
		},
		
		// Find the position where the menu should appear, given an x,y of the click event
		getPosition: function(clickX,clickY,cmenu,e) {
			var x = clickX+cmenu.offsetX;
			var y = clickY+cmenu.offsetY;
			if (cmenu.normalMenu)
			{
			  var el= e.currentTarget;
			  var tHeight=$(el).height();
			  var tPosition=$(el).position();
			  x= tPosition.left+cmenu.offsetX;
			  y= tPosition.top + tHeight+cmenu.offsetY;
			}
			var h = $(cmenu.menu).height();
			var w = $(cmenu.menu).width();
			var dir = cmenu.direction;
			if (cmenu.constrainToScreen) {
				var $w = $(window);
				var wh = $w.height();
				var ww = $w.width();
				if (dir=="down" && (y+h-$w.scrollTop() > wh)) { dir = "up"; }
				var maxRight = x+w-$w.scrollLeft();
				if (maxRight > ww) { x -= (maxRight-ww); }
			}
			if (dir=="up") { y -= h; }
			return {'x':x,'y':y};
		},
		
		// Hide the menu, of course
		hide: function() {
			var cmenu=this;
			if (cmenu.shown) {
				if (cmenu.iframe) { $(cmenu.iframe).hide(); }
				if (cmenu.menu) { cmenu.menu[cmenu.hideTransition](cmenu.hideSpeed,((cmenu.hideCallback)?function(){cmenu.hideCallback.call(cmenu);}:null)); }
				if (cmenu.shadow) { cmenu.shadowObj[cmenu.hideTransition](cmenu.hideSpeed); }
			}
			cmenu.shown = false;
		}
	};
	
	// This actually adds the .contextMenu() function to the jQuery namespace
	$.fn.contextMenu = function(menu,options,eventName) {
		var cmenu = $.contextMenu.create(menu,options);
		if ($.contextMenu.menuList==null) $.contextMenu.menuList = new Array();
		$.contextMenu.menuList.push(cmenu);
		var cEventName="contextmenu";
		if (typeof eventName=="string") cEventName=eventName;
		
		return this.each(function(){
			$(this).bind(cEventName,function(e){cmenu.show(this,e);return false;});
		});
	};
})(jQuery);
jQuery.fn.extend({
insertAtCaret: function(myValue){
  return this.each(function(i) {
    if (document.selection) {
      this.focus();
      sel = document.selection.createRange();
      sel.text = myValue;
      this.focus();
    }
    else if (this.selectionStart || this.selectionStart == '0') {
      var startPos = this.selectionStart;
      var endPos = this.selectionEnd;
      var scrollTop = this.scrollTop;
      this.value = this.value.substring(0, startPos)+myValue+this.value.substring(endPos,this.value.length);
      this.focus();
      this.selectionStart = startPos + myValue.length;
      this.selectionEnd = startPos + myValue.length;
      this.scrollTop = scrollTop;
    } else {
      this.value += myValue;
      this.focus();
    }
  })
}
});!function($,window,document){$.fn.dataTableSettings=[];var _aoSettings=$.fn.dataTableSettings;$.fn.dataTableExt={};var _oExt=$.fn.dataTableExt;_oExt.sVersion="1.8.2",_oExt.sErrMode="alert",_oExt.iApiIndex=0,_oExt.oApi={},_oExt.afnFiltering=[],_oExt.aoFeatures=[],_oExt.ofnSearch={},_oExt.afnSortData=[],_oExt.oStdClasses={sPagePrevEnabled:"paginate_enabled_previous",sPagePrevDisabled:"paginate_disabled_previous",sPageNextEnabled:"paginate_enabled_next",sPageNextDisabled:"paginate_disabled_next",sPageJUINext:"",sPageJUIPrev:"",sPageButton:"paginate_button",sPageButtonActive:"paginate_active",sPageButtonStaticDisabled:"paginate_button paginate_button_disabled",sPageFirst:"first",sPagePrevious:"previous",sPageNext:"next",sPageLast:"last",sStripeOdd:"odd",sStripeEven:"even",sRowEmpty:"dataTables_empty",sWrapper:"dataTables_wrapper",sFilter:"dataTables_filter",sInfo:"dataTables_info",sPaging:"dataTables_paginate paging_",sLength:"dataTables_length",sProcessing:"dataTables_processing",sSortAsc:"sorting_asc",sSortDesc:"sorting_desc",sSortable:"sorting",sSortableAsc:"sorting_asc_disabled",sSortableDesc:"sorting_desc_disabled",sSortableNone:"sorting_disabled",sSortColumn:"sorting_",sSortJUIAsc:"",sSortJUIDesc:"",sSortJUI:"",sSortJUIAscAllowed:"",sSortJUIDescAllowed:"",sSortJUIWrapper:"",sSortIcon:"",sScrollWrapper:"dataTables_scroll",sScrollHead:"dataTables_scrollHead",sScrollHeadInner:"dataTables_scrollHeadInner",sScrollBody:"dataTables_scrollBody",sScrollFoot:"dataTables_scrollFoot",sScrollFootInner:"dataTables_scrollFootInner",sFooterTH:""},_oExt.oJUIClasses={sPagePrevEnabled:"fg-button ui-button ui-state-default ui-corner-left",sPagePrevDisabled:"fg-button ui-button ui-state-default ui-corner-left ui-state-disabled",sPageNextEnabled:"fg-button ui-button ui-state-default ui-corner-right",sPageNextDisabled:"fg-button ui-button ui-state-default ui-corner-right ui-state-disabled",sPageJUINext:"ui-icon ui-icon-circle-arrow-e",sPageJUIPrev:"ui-icon ui-icon-circle-arrow-w",sPageButton:"fg-button ui-button ui-state-default",sPageButtonActive:"fg-button ui-button ui-state-default ui-state-disabled",sPageButtonStaticDisabled:"fg-button ui-button ui-state-default ui-state-disabled",sPageFirst:"first ui-corner-tl ui-corner-bl",sPagePrevious:"previous",sPageNext:"next",sPageLast:"last ui-corner-tr ui-corner-br",sStripeOdd:"odd",sStripeEven:"even",sRowEmpty:"dataTables_empty",sWrapper:"dataTables_wrapper",sFilter:"dataTables_filter",sInfo:"dataTables_info",sPaging:"dataTables_paginate fg-buttonset ui-buttonset fg-buttonset-multi ui-buttonset-multi paging_",sLength:"dataTables_length",sProcessing:"dataTables_processing",sSortAsc:"ui-state-default",sSortDesc:"ui-state-default",sSortable:"ui-state-default",sSortableAsc:"ui-state-default",sSortableDesc:"ui-state-default",sSortableNone:"ui-state-default",sSortColumn:"sorting_",sSortJUIAsc:"css_right ui-icon ui-icon-triangle-1-n",sSortJUIDesc:"css_right ui-icon ui-icon-triangle-1-s",sSortJUI:"css_right ui-icon ui-icon-carat-2-n-s",sSortJUIAscAllowed:"css_right ui-icon ui-icon-carat-1-n",sSortJUIDescAllowed:"css_right ui-icon ui-icon-carat-1-s",sSortJUIWrapper:"DataTables_sort_wrapper",sSortIcon:"DataTables_sort_icon",sScrollWrapper:"dataTables_scroll",sScrollHead:"dataTables_scrollHead ui-state-default",sScrollHeadInner:"dataTables_scrollHeadInner",sScrollBody:"dataTables_scrollBody",sScrollFoot:"dataTables_scrollFoot ui-state-default",sScrollFootInner:"dataTables_scrollFootInner",sFooterTH:"ui-state-default"},_oExt.oPagination={two_button:{fnInit:function(e,t,a){var n,o,s,i;e.bJUI?(n=document.createElement("a"),o=document.createElement("a"),i=document.createElement("span"),i.className=e.oClasses.sPageJUINext,o.appendChild(i),s=document.createElement("span"),s.className=e.oClasses.sPageJUIPrev,n.appendChild(s)):(n=document.createElement("div"),o=document.createElement("div")),n.className=e.oClasses.sPagePrevDisabled,o.className=e.oClasses.sPageNextDisabled,n.title=e.oLanguage.oPaginate.sPrevious,o.title=e.oLanguage.oPaginate.sNext,t.appendChild(n),t.appendChild(o),$(n).bind("click.DT",function(){e.oApi._fnPageChange(e,"previous")&&a(e)}),$(o).bind("click.DT",function(){e.oApi._fnPageChange(e,"next")&&a(e)}),$(n).bind("selectstart.DT",function(){return!1}),$(o).bind("selectstart.DT",function(){return!1}),""!==e.sTableId&&"undefined"==typeof e.aanFeatures.p&&(t.setAttribute("id",e.sTableId+"_paginate"),n.setAttribute("id",e.sTableId+"_previous"),o.setAttribute("id",e.sTableId+"_next"))},fnUpdate:function(e,t){if(e.aanFeatures.p)for(var a=e.aanFeatures.p,n=0,o=a.length;o>n;n++)0!==a[n].childNodes.length&&(a[n].childNodes[0].className=0===e._iDisplayStart?e.oClasses.sPagePrevDisabled:e.oClasses.sPagePrevEnabled,a[n].childNodes[1].className=e.fnDisplayEnd()==e.fnRecordsDisplay()?e.oClasses.sPageNextDisabled:e.oClasses.sPageNextEnabled)}},iFullNumbersShowPages:5,full_numbers:{fnInit:function(e,t,a){var n=document.createElement("span"),o=document.createElement("span"),s=document.createElement("span"),i=document.createElement("span"),r=document.createElement("span");n.innerHTML=e.oLanguage.oPaginate.sFirst,o.innerHTML=e.oLanguage.oPaginate.sPrevious,i.innerHTML=e.oLanguage.oPaginate.sNext,r.innerHTML=e.oLanguage.oPaginate.sLast;var l=e.oClasses;n.className=l.sPageButton+" "+l.sPageFirst,o.className=l.sPageButton+" "+l.sPagePrevious,i.className=l.sPageButton+" "+l.sPageNext,r.className=l.sPageButton+" "+l.sPageLast,t.appendChild(n),t.appendChild(o),t.appendChild(s),t.appendChild(i),t.appendChild(r),$(n).bind("click.DT",function(){e.oApi._fnPageChange(e,"first")&&a(e)}),$(o).bind("click.DT",function(){e.oApi._fnPageChange(e,"previous")&&a(e)}),$(i).bind("click.DT",function(){e.oApi._fnPageChange(e,"next")&&a(e)}),$(r).bind("click.DT",function(){e.oApi._fnPageChange(e,"last")&&a(e)}),$("span",t).bind("mousedown.DT",function(){return!1}).bind("selectstart.DT",function(){return!1}),""!==e.sTableId&&"undefined"==typeof e.aanFeatures.p&&(t.setAttribute("id",e.sTableId+"_paginate"),n.setAttribute("id",e.sTableId+"_first"),o.setAttribute("id",e.sTableId+"_previous"),i.setAttribute("id",e.sTableId+"_next"),r.setAttribute("id",e.sTableId+"_last"))},fnUpdate:function(e,t){if(e.aanFeatures.p){var a,n,o,s,i=_oExt.oPagination.iFullNumbersShowPages,r=Math.floor(i/2),l=Math.ceil(e.fnRecordsDisplay()/e._iDisplayLength),f=Math.ceil(e._iDisplayStart/e._iDisplayLength)+1,d="",u=e.oClasses;for(i>l?(a=1,n=l):r>=f?(a=1,n=i):f>=l-r?(a=l-i+1,n=l):(a=f-Math.ceil(i/2)+1,n=a+i-1),o=a;n>=o;o++)d+=f!=o?'<span class="'+u.sPageButton+'">'+o+"</span>":'<span class="'+u.sPageButtonActive+'">'+o+"</span>";var p,c,h=e.aanFeatures.p,g=function(a){var n=1*this.innerHTML-1;e._iDisplayStart=n*e._iDisplayLength,t(e),a.preventDefault()},_=function(){return!1};for(o=0,s=h.length;s>o;o++)if(0!==h[o].childNodes.length){var S=$("span:eq(2)",h[o]);S.html(d),$("span",S).bind("click.DT",g).bind("mousedown.DT",_).bind("selectstart.DT",_),p=h[o].getElementsByTagName("span"),c=[p[0],p[1],p[p.length-2],p[p.length-1]],$(c).removeClass(u.sPageButton+" "+u.sPageButtonActive+" "+u.sPageButtonStaticDisabled),1==f?(c[0].className+=" "+u.sPageButtonStaticDisabled,c[1].className+=" "+u.sPageButtonStaticDisabled):(c[0].className+=" "+u.sPageButton,c[1].className+=" "+u.sPageButton),0===l||f==l||-1==e._iDisplayLength?(c[2].className+=" "+u.sPageButtonStaticDisabled,c[3].className+=" "+u.sPageButtonStaticDisabled):(c[2].className+=" "+u.sPageButton,c[3].className+=" "+u.sPageButton)}}}}},_oExt.oSort={"string-asc":function(e,t){"string"!=typeof e&&(e=""),"string"!=typeof t&&(t="");var a=e.toLowerCase(),n=t.toLowerCase();return n>a?-1:a>n?1:0},"string-desc":function(e,t){"string"!=typeof e&&(e=""),"string"!=typeof t&&(t="");var a=e.toLowerCase(),n=t.toLowerCase();return n>a?1:a>n?-1:0},"html-asc":function(e,t){var a=e.replace(/<.*?>/g,"").toLowerCase(),n=t.replace(/<.*?>/g,"").toLowerCase();return n>a?-1:a>n?1:0},"html-desc":function(e,t){var a=e.replace(/<.*?>/g,"").toLowerCase(),n=t.replace(/<.*?>/g,"").toLowerCase();return n>a?1:a>n?-1:0},"date-asc":function(e,t){var a=Date.parse(e),n=Date.parse(t);return(isNaN(a)||""===a)&&(a=Date.parse("01/01/1970 00:00:00")),(isNaN(n)||""===n)&&(n=Date.parse("01/01/1970 00:00:00")),a-n},"date-desc":function(e,t){var a=Date.parse(e),n=Date.parse(t);return(isNaN(a)||""===a)&&(a=Date.parse("01/01/1970 00:00:00")),(isNaN(n)||""===n)&&(n=Date.parse("01/01/1970 00:00:00")),n-a},"numeric-asc":function(e,t){try{var a=/(-?\d*\.?\d+)/;if("string"==typeof e){e=e.replace(/[,]+/g,"").replace(/[\(]+/g,"-");var n=a.exec(e);e=n[0]}if("string"==typeof t){t=t.replace(/[,]+/g,"").replace(/[\(]+/g,"-");var o=a.exec(t);t=o[0]}var s="-"==e||""===e?0:1*e,i="-"==t||""===t?0:1*t;return s-i}catch(r){return console.log("error in sorting"+e+" - "+t),0}},"numeric-desc":function(e,t){try{var a=/(-?\d*\.?\d+)/;if("string"==typeof e){e=e.replace(/[,]+/g,"").replace(/[\(]+/g,"-");var n=a.exec(e);e=n[0]}if("string"==typeof t){t=t.replace(/[,]+/g,"").replace(/[\(]+/g,"-");var o=a.exec(t);t=o[0]}var s="-"==e||""===e?0:1*e,i="-"==t||""===t?0:1*t;return i-s}catch(r){return 0}}},_oExt.aTypes=[function(e){if("number"==typeof e)return"numeric";if(isNaN(e)){var t=/(-?\d*\.?\d+)/;e=e.replace(/[,]+/g,"").replace(/[\(]+/g,"-");var a=t.test(e);return a?"numeric":null}return"numeric"},function(e){var t=Date.parse(e);if("Invalid Date"==t||null==t||e.length<5){var a=e.replace(/^[^0-9]/g,"").replace(/[,\.\(\)]+/g,""),n="string";return isNaN(a)||(n="numeric"),n}n="Date"},function(e){return"string"==typeof e&&-1!=e.indexOf("<")&&-1!=e.indexOf(">")?"html":null}],_oExt.fnVersionCheck=function(e){for(var t=function(e,t){for(;e.length<t;)e+="0";return e},a=_oExt.sVersion.split("."),n=e.split("."),o="",s="",i=0,r=n.length;r>i;i++)o+=t(a[i],3),s+=t(n[i],3);return parseInt(o,10)>=parseInt(s,10)},_oExt._oExternConfig={iNextUnique:0},$.fn.dataTable=function(oInit){function classSettings(){this.fnRecordsTotal=function(){return this.oFeatures.bServerSide?parseInt(this._iRecordsTotal,10):this.aiDisplayMaster.length},this.fnRecordsDisplay=function(){return this.oFeatures.bServerSide?parseInt(this._iRecordsDisplay,10):this.aiDisplay.length},this.fnDisplayEnd=function(){return this.oFeatures.bServerSide?this.oFeatures.bPaginate===!1||-1==this._iDisplayLength?this._iDisplayStart+this.aiDisplay.length:Math.min(this._iDisplayStart+this._iDisplayLength,this._iRecordsDisplay):this._iDisplayEnd},this.oInstance=null,this.sInstance=null,this.oFeatures={bPaginate:!0,bLengthChange:!0,bFilter:!0,bSort:!0,bInfo:!0,bAutoWidth:!0,bProcessing:!1,bSortClasses:!0,bStateSave:!1,bServerSide:!1,bDeferRender:!1},this.oScroll={sX:"",sXInner:"",sY:"",bCollapse:!1,bInfinite:!1,iLoadGap:100,iBarWidth:0,bAutoCss:!0},this.aanFeatures=[],this.oLanguage={sProcessing:"Processing...",sLengthMenu:"Show _MENU_ entries",sZeroRecords:"No matching records found",sEmptyTable:"No data available in table",sLoadingRecords:"Loading...",sInfo:"Showing _START_ to _END_ of _TOTAL_ entries",sInfoEmpty:"Showing 0 to 0 of 0 entries",sInfoFiltered:"(filtered from _MAX_ total entries)",sInfoPostFix:"",sInfoThousands:",",sSearch:"Search:",sUrl:"",oPaginate:{sFirst:"First",sPrevious:"Previous",sNext:"Next",sLast:"Last"},fnInfoCallback:null},this.aoData=[],this.aiDisplay=[],this.aiDisplayMaster=[],this.aoColumns=[],this.aoHeader=[],this.aoFooter=[],this.iNextId=0,this.asDataSearch=[],this.oPreviousSearch={sSearch:"",bRegex:!1,bSmart:!0},this.aoPreSearchCols=[],this.aaSorting=[[0,"asc",0]],this.aaSortingFixed=null,this.asStripeClasses=[],this.asDestroyStripes=[],this.sDestroyWidth=0,this.fnRowCallback=null,this.fnHeaderCallback=null,this.fnFooterCallback=null,this.aoDrawCallback=[],this.fnPreDrawCallback=null,this.fnInitComplete=null,this.sTableId="",this.nTable=null,this.nTHead=null,this.nTFoot=null,this.nTBody=null,this.nTableWrapper=null,this.bDeferLoading=!1,this.bInitialised=!1,this.aoOpenRows=[],this.sDom="lfrtip",this.sPaginationType="two_button",this.iCookieDuration=7200,this.sCookiePrefix="SpryMedia_DataTables_",this.fnCookieCallback=null,this.aoStateSave=[],this.aoStateLoad=[],this.oLoadedState=null,this.sAjaxSource=null,this.sAjaxDataProp="aaData",this.bAjaxDataGet=!0,this.jqXHR=null,this.fnServerData=function(e,t,a,n){n.jqXHR=$.ajax({url:e,data:t,success:function(e){$(n.oInstance).trigger("xhr",n),a(e)},dataType:"json",cache:!1,error:function(e,t,a){"parsererror"==t&&alert("DataTables warning: JSON data from server could not be parsed. This is caused by a JSON formatting error.")}})},this.aoServerParams=[],this.fnFormatNumber=function(e){if(1e3>e)return e;for(var t=e+"",a=t.split(""),n="",o=t.length,s=0;o>s;s++)s%3===0&&0!==s&&(n=this.oLanguage.sInfoThousands+n),n=a[o-s-1]+n;return n},this.aLengthMenu=[10,25,50,100],this.iDraw=0,this.bDrawing=0,this.iDrawError=-1,this._iDisplayLength=10,this._iDisplayStart=0,this._iDisplayEnd=10,this._iRecordsTotal=0,this._iRecordsDisplay=0,this.bJUI=!1,this.oClasses=_oExt.oStdClasses,this.bFiltered=!1,this.bSorted=!1,this.bSortCellsTop=!1,this.oInit=null,this.aoDestroyCallback=[]}function _fnExternApiFunc(e){return function(){var t=[_fnSettingsFromNode(this[_oExt.iApiIndex])].concat(Array.prototype.slice.call(arguments));return _oExt.oApi[e].apply(this,t)}}function _fnInitialise(e){var t,a,n=e.iInitDisplayStart;if(e.bInitialised===!1)return void setTimeout(function(){_fnInitialise(e)},200);for(_fnAddOptionsHtml(e),_fnBuildHead(e),_fnDrawHead(e,e.aoHeader),e.nTFoot&&_fnDrawHead(e,e.aoFooter),_fnProcessingDisplay(e,!0),e.oFeatures.bAutoWidth&&_fnCalculateColumnWidths(e),t=0,a=e.aoColumns.length;a>t;t++)null!==e.aoColumns[t].sWidth&&(e.aoColumns[t].nTh.style.width=_fnStringToCss(e.aoColumns[t].sWidth));if(e.oFeatures.bSort?_fnSort(e):e.oFeatures.bFilter?_fnFilterComplete(e,e.oPreviousSearch):(e.aiDisplay=e.aiDisplayMaster.slice(),_fnCalculateEnd(e),_fnDraw(e)),null!==e.sAjaxSource&&!e.oFeatures.bServerSide){var o=[];return _fnServerParams(e,o),void e.fnServerData.call(e.oInstance,e.sAjaxSource,o,function(a){var o=a;if(""!==e.sAjaxDataProp){var s=_fnGetObjectDataFn(e.sAjaxDataProp);o=s(a)}for(t=0;t<o.length;t++)_fnAddData(e,o[t]);e.iInitDisplayStart=n,e.oFeatures.bSort?_fnSort(e):(e.aiDisplay=e.aiDisplayMaster.slice(),_fnCalculateEnd(e),_fnDraw(e)),_fnProcessingDisplay(e,!1),_fnInitComplete(e,a)},e)}e.oFeatures.bServerSide||(_fnProcessingDisplay(e,!1),_fnInitComplete(e))}function _fnInitComplete(e,t){e._bInitComplete=!0,"function"==typeof e.fnInitComplete&&("undefined"!=typeof t?e.fnInitComplete.call(e.oInstance,e,t):e.fnInitComplete.call(e.oInstance,e))}function _fnLanguageProcess(e,t,a){e.oLanguage=$.extend(!0,e.oLanguage,t),"undefined"==typeof t.sEmptyTable&&"undefined"!=typeof t.sZeroRecords&&_fnMap(e.oLanguage,t,"sZeroRecords","sEmptyTable"),"undefined"==typeof t.sLoadingRecords&&"undefined"!=typeof t.sZeroRecords&&_fnMap(e.oLanguage,t,"sZeroRecords","sLoadingRecords"),a&&_fnInitialise(e)}function _fnAddColumn(e,t){var a=e.aoColumns.length,n={sType:null,_bAutoType:!0,bVisible:!0,bSearchable:!0,bSortable:!0,asSorting:["asc","desc"],sSortingClass:e.oClasses.sSortable,sSortingClassJUI:e.oClasses.sSortJUI,sTitle:t?t.innerHTML:"",sName:"",sWidth:null,sWidthOrig:null,sClass:null,fnRender:null,bUseRendered:!0,iDataSort:a,mDataProp:a,fnGetData:null,fnSetData:null,sSortDataType:"std",sDefaultContent:null,sContentPadding:"",nTh:t?t:document.createElement("th"),nTf:null};e.aoColumns.push(n),"undefined"==typeof e.aoPreSearchCols[a]||null===e.aoPreSearchCols[a]?e.aoPreSearchCols[a]={sSearch:"",bRegex:!1,bSmart:!0}:("undefined"==typeof e.aoPreSearchCols[a].bRegex&&(e.aoPreSearchCols[a].bRegex=!0),"undefined"==typeof e.aoPreSearchCols[a].bSmart&&(e.aoPreSearchCols[a].bSmart=!0)),_fnColumnOptions(e,a,null)}function _fnColumnOptions(e,t,a){var n=e.aoColumns[t];"undefined"!=typeof a&&null!==a&&("undefined"!=typeof a.sType&&(n.sType=a.sType,n._bAutoType=!1),_fnMap(n,a,"bVisible"),_fnMap(n,a,"bSearchable"),_fnMap(n,a,"bSortable"),_fnMap(n,a,"sTitle"),_fnMap(n,a,"sName"),_fnMap(n,a,"sWidth"),_fnMap(n,a,"sWidth","sWidthOrig"),_fnMap(n,a,"sClass"),_fnMap(n,a,"fnRender"),_fnMap(n,a,"bUseRendered"),_fnMap(n,a,"iDataSort"),_fnMap(n,a,"mDataProp"),_fnMap(n,a,"asSorting"),_fnMap(n,a,"sSortDataType"),_fnMap(n,a,"sDefaultContent"),_fnMap(n,a,"sContentPadding")),n.fnGetData=_fnGetObjectDataFn(n.mDataProp),n.fnSetData=_fnSetObjectDataFn(n.mDataProp),e.oFeatures.bSort||(n.bSortable=!1),!n.bSortable||-1==$.inArray("asc",n.asSorting)&&-1==$.inArray("desc",n.asSorting)?(n.sSortingClass=e.oClasses.sSortableNone,n.sSortingClassJUI=""):n.bSortable||-1==$.inArray("asc",n.asSorting)&&-1==$.inArray("desc",n.asSorting)?(n.sSortingClass=e.oClasses.sSortable,n.sSortingClassJUI=e.oClasses.sSortJUI):-1!=$.inArray("asc",n.asSorting)&&-1==$.inArray("desc",n.asSorting)?(n.sSortingClass=e.oClasses.sSortableAsc,n.sSortingClassJUI=e.oClasses.sSortJUIAscAllowed):-1==$.inArray("asc",n.asSorting)&&-1!=$.inArray("desc",n.asSorting)&&(n.sSortingClass=e.oClasses.sSortableDesc,n.sSortingClassJUI=e.oClasses.sSortJUIDescAllowed)}function _fnAddData(e,t){var a,n=$.isArray(t)?t.slice():$.extend(!0,{},t),o=e.aoData.length,s={nTr:null,_iId:e.iNextId++,_aData:n,_anHidden:[],_sRowStripe:""};e.aoData.push(s);for(var i,r=0,l=e.aoColumns.length;l>r;r++)if(a=e.aoColumns[r],"function"==typeof a.fnRender&&a.bUseRendered&&null!==a.mDataProp&&_fnSetCellData(e,o,r,a.fnRender({iDataRow:o,iDataColumn:r,aData:s._aData,oSettings:e})),a._bAutoType&&"string"!=a.sType){var f=_fnGetCellData(e,o,r,"type");null!==f&&""!==f&&(i=_fnDetectType(f),null===a.sType?a.sType=i:a.sType!=i&&"html"!=a.sType&&(a.sType="string"))}return e.aiDisplayMaster.push(o),e.oFeatures.bDeferRender||_fnCreateTr(e,o),o}function _fnCreateTr(e,t){var a,n=e.aoData[t];if(null===n.nTr){n.nTr=document.createElement("tr"),"undefined"!=typeof n._aData.DT_RowId&&n.nTr.setAttribute("id",n._aData.DT_RowId),"undefined"!=typeof n._aData.DT_RowClass&&$(n.nTr).addClass(n._aData.DT_RowClass);for(var o=0,s=e.aoColumns.length;s>o;o++){var i=e.aoColumns[o];a=document.createElement("td"),"function"!=typeof i.fnRender||i.bUseRendered&&null!==i.mDataProp?a.innerHTML=_fnGetCellData(e,t,o,"display"):a.innerHTML=i.fnRender({iDataRow:t,iDataColumn:o,aData:n._aData,oSettings:e}),null!==i.sClass&&(a.className=i.sClass),i.bVisible?(n.nTr.appendChild(a),n._anHidden[o]=null):n._anHidden[o]=a}}}function _fnGatherData(e){var t,a,n,o,s,i,r,l,f,d,u,p,c,h;if(e.bDeferLoading||null===e.sAjaxSource)for(r=e.nTBody.childNodes,t=0,a=r.length;a>t;t++)if("TR"==r[t].nodeName.toUpperCase())for(f=e.aoData.length,e.aoData.push({nTr:r[t],_iId:e.iNextId++,_aData:[],_anHidden:[],_sRowStripe:""}),e.aiDisplayMaster.push(f),i=r[t].childNodes,s=0,n=0,o=i.length;o>n;n++)h=i[n].nodeName.toUpperCase(),("TD"==h||"TH"==h)&&(_fnSetCellData(e,f,s,$.trim(i[n].innerHTML)),s++);for(r=_fnGetTrNodes(e),i=[],t=0,a=r.length;a>t;t++)for(n=0,o=r[t].childNodes.length;o>n;n++)l=r[t].childNodes[n],h=l.nodeName.toUpperCase(),("TD"==h||"TH"==h)&&i.push(l);for(i.length!=r.length*e.aoColumns.length&&_fnLog(e,1,"Unexpected number of TD elements. Expected "+r.length*e.aoColumns.length+" and got "+i.length+". DataTables does not support rowspan / colspan in the table body, and there must be one cell for each row/column combination."),p=0,c=e.aoColumns.length;c>p;p++){null===e.aoColumns[p].sTitle&&(e.aoColumns[p].sTitle=e.aoColumns[p].nTh.innerHTML);var g,_,S,C,m=e.aoColumns[p]._bAutoType,b="function"==typeof e.aoColumns[p].fnRender,D=null!==e.aoColumns[p].sClass,y=e.aoColumns[p].bVisible;if(m||b||D||!y)for(d=0,u=e.aoData.length;u>d;d++)g=i[d*c+p],m&&"string"!=e.aoColumns[p].sType&&(C=_fnGetCellData(e,d,p,"type"),""!==C&&(_=_fnDetectType(C),null===e.aoColumns[p].sType?e.aoColumns[p].sType=_:e.aoColumns[p].sType!=_&&"html"!=e.aoColumns[p].sType&&(e.aoColumns[p].sType="string"))),b&&(S=e.aoColumns[p].fnRender({iDataRow:d,iDataColumn:p,aData:e.aoData[d]._aData,oSettings:e}),g.innerHTML=S,e.aoColumns[p].bUseRendered&&_fnSetCellData(e,d,p,S)),D&&(g.className+=" "+e.aoColumns[p].sClass),y?e.aoData[d]._anHidden[p]=null:(e.aoData[d]._anHidden[p]=g,g.parentNode.removeChild(g))}}function _fnBuildHead(e){var t,a,n,o=(e.nTHead.getElementsByTagName("tr"),e.nTHead.getElementsByTagName("th").length);if(0!==o)for(t=0,n=e.aoColumns.length;n>t;t++)a=e.aoColumns[t].nTh,null!==e.aoColumns[t].sClass&&$(a).addClass(e.aoColumns[t].sClass),e.aoColumns[t].sTitle!=a.innerHTML&&(a.innerHTML=e.aoColumns[t].sTitle);else{var s=document.createElement("tr");for(t=0,n=e.aoColumns.length;n>t;t++)a=e.aoColumns[t].nTh,a.innerHTML=e.aoColumns[t].sTitle,null!==e.aoColumns[t].sClass&&$(a).addClass(e.aoColumns[t].sClass),s.appendChild(a);$(e.nTHead).html("")[0].appendChild(s),_fnDetectHeader(e.aoHeader,e.nTHead)}if(e.bJUI)for(t=0,n=e.aoColumns.length;n>t;t++){a=e.aoColumns[t].nTh;var i=document.createElement("div");i.className=e.oClasses.sSortJUIWrapper,$(a).contents().appendTo(i);var r=document.createElement("span");r.className=e.oClasses.sSortIcon,i.appendChild(r),a.appendChild(i)}var l=function(e){return this.onselectstart=function(){return!1},!1};if(e.oFeatures.bSort)for(t=0;t<e.aoColumns.length;t++)e.aoColumns[t].bSortable!==!1?(_fnSortAttachListener(e,e.aoColumns[t].nTh,t),$(e.aoColumns[t].nTh).bind("mousedown.DT",l)):$(e.aoColumns[t].nTh).addClass(e.oClasses.sSortableNone);if(""!==e.oClasses.sFooterTH&&$(e.nTFoot).children("tr").children("th").addClass(e.oClasses.sFooterTH),null!==e.nTFoot){var f=_fnGetUniqueThs(e,null,e.aoFooter);for(t=0,n=e.aoColumns.length;n>t;t++)"undefined"!=typeof f[t]&&(e.aoColumns[t].nTf=f[t])}}function _fnDrawHead(e,t,a){var n,o,s,i,r,l,f,d,u=[],p=[],c=e.aoColumns.length;for("undefined"==typeof a&&(a=!1),n=0,o=t.length;o>n;n++){for(u[n]=t[n].slice(),u[n].nTr=t[n].nTr,s=c-1;s>=0;s--)e.aoColumns[s].bVisible||a||u[n].splice(s,1);p.push([])}for(n=0,o=u.length;o>n;n++){if(u[n].nTr)for(r=0,l=u[n].nTr.childNodes.length;l>r;r++)u[n].nTr.removeChild(u[n].nTr.childNodes[0]);for(s=0,i=u[n].length;i>s;s++)if(f=1,d=1,"undefined"==typeof p[n][s]){for(u[n].nTr.appendChild(u[n][s].cell),p[n][s]=1;"undefined"!=typeof u[n+f]&&u[n][s].cell==u[n+f][s].cell;)p[n+f][s]=1,f++;for(;"undefined"!=typeof u[n][s+d]&&u[n][s].cell==u[n][s+d].cell;){for(r=0;f>r;r++)p[n+r][s+d]=1;d++}u[n][s].cell.rowSpan=f,u[n][s].cell.colSpan=d}}}function _fnDraw(e){var t,a,n=[],o=0,s=!1,i=e.asStripeClasses.length,r=e.aoOpenRows.length;if(null===e.fnPreDrawCallback||e.fnPreDrawCallback.call(e.oInstance,e)!==!1){if(e.bDrawing=!0,"undefined"!=typeof e.iInitDisplayStart&&-1!=e.iInitDisplayStart&&(e.oFeatures.bServerSide?e._iDisplayStart=e.iInitDisplayStart:e._iDisplayStart=e.iInitDisplayStart>=e.fnRecordsDisplay()?0:e.iInitDisplayStart,e.iInitDisplayStart=-1,_fnCalculateEnd(e)),e.bDeferLoading)e.bDeferLoading=!1,e.iDraw++;else if(e.oFeatures.bServerSide){if(!e.bDestroying&&!_fnAjaxUpdate(e))return}else e.iDraw++;if(0!==e.aiDisplay.length){var l=e._iDisplayStart,f=e._iDisplayEnd;e.oFeatures.bServerSide&&(l=0,f=e.aoData.length);for(var d=l;f>d;d++){var u=e.aoData[e.aiDisplay[d]];null===u.nTr&&_fnCreateTr(e,e.aiDisplay[d]);var p=u.nTr;if(0!==i){var c=e.asStripeClasses[o%i];u._sRowStripe!=c&&($(p).removeClass(u._sRowStripe).addClass(c),u._sRowStripe=c)}if("function"==typeof e.fnRowCallback&&(p=e.fnRowCallback.call(e.oInstance,p,e.aoData[e.aiDisplay[d]]._aData,o,d),p||s||(_fnLog(e,0,"A node was not returned by fnRowCallback"),s=!0)),n.push(p),o++,0!==r)for(var h=0;r>h;h++)p==e.aoOpenRows[h].nParent&&n.push(e.aoOpenRows[h].nTr)}}else{n[0]=document.createElement("tr"),"undefined"!=typeof e.asStripeClasses[0]&&(n[0].className=e.asStripeClasses[0]);var g=e.oLanguage.sZeroRecords.replace("_MAX_",e.fnFormatNumber(e.fnRecordsTotal()));1!=e.iDraw||null===e.sAjaxSource||e.oFeatures.bServerSide?"undefined"!=typeof e.oLanguage.sEmptyTable&&0===e.fnRecordsTotal()&&(g=e.oLanguage.sEmptyTable):g=e.oLanguage.sLoadingRecords;var _=document.createElement("td");_.setAttribute("valign","top"),_.colSpan=_fnVisbleColumns(e),_.className=e.oClasses.sRowEmpty,_.innerHTML=g,n[o].appendChild(_)}"function"==typeof e.fnHeaderCallback&&e.fnHeaderCallback.call(e.oInstance,$(e.nTHead).children("tr")[0],_fnGetDataMaster(e),e._iDisplayStart,e.fnDisplayEnd(),e.aiDisplay),"function"==typeof e.fnFooterCallback&&e.fnFooterCallback.call(e.oInstance,$(e.nTFoot).children("tr")[0],_fnGetDataMaster(e),e._iDisplayStart,e.fnDisplayEnd(),e.aiDisplay);var S,C,m=document.createDocumentFragment(),b=document.createDocumentFragment();if(e.nTBody){if(S=e.nTBody.parentNode,b.appendChild(e.nTBody),!e.oScroll.bInfinite||!e._bInitComplete||e.bSorted||e.bFiltered)for(C=e.nTBody.childNodes,t=C.length-1;t>=0;t--)C[t].parentNode.removeChild(C[t]);for(t=0,a=n.length;a>t;t++)m.appendChild(n[t]);e.nTBody.appendChild(m),null!==S&&S.appendChild(e.nTBody)}for(t=e.aoDrawCallback.length-1;t>=0;t--)e.aoDrawCallback[t].fn.call(e.oInstance,e);$(e.oInstance).trigger("draw",e),e.bSorted=!1,e.bFiltered=!1,e.bDrawing=!1,e.oFeatures.bServerSide&&(_fnProcessingDisplay(e,!1),"undefined"==typeof e._bInitComplete&&_fnInitComplete(e))}}function _fnReDraw(e){e.oFeatures.bSort?_fnSort(e,e.oPreviousSearch):e.oFeatures.bFilter?_fnFilterComplete(e,e.oPreviousSearch):(_fnCalculateEnd(e),_fnDraw(e))}function _fnAjaxUpdate(e){if(e.bAjaxDataGet){e.iDraw++,_fnProcessingDisplay(e,!0);var t=(e.aoColumns.length,_fnAjaxParameters(e));return _fnServerParams(e,t),e.fnServerData.call(e.oInstance,e.sAjaxSource,t,function(t){_fnAjaxUpdateDraw(e,t)},e),!1}return!0}function _fnAjaxParameters(e){var t,a,n=e.aoColumns.length,o=[];for(o.push({name:"sEcho",value:e.iDraw}),o.push({name:"iColumns",value:n}),o.push({name:"sColumns",value:_fnColumnOrdering(e)}),o.push({name:"iDisplayStart",value:e._iDisplayStart}),o.push({name:"iDisplayLength",value:e.oFeatures.bPaginate!==!1?e._iDisplayLength:-1}),a=0;n>a;a++)t=e.aoColumns[a].mDataProp,o.push({name:"mDataProp_"+a,value:"function"==typeof t?"function":t});if(e.oFeatures.bFilter!==!1)for(o.push({name:"sSearch",value:e.oPreviousSearch.sSearch}),o.push({name:"bRegex",value:e.oPreviousSearch.bRegex}),a=0;n>a;a++)o.push({name:"sSearch_"+a,value:e.aoPreSearchCols[a].sSearch}),o.push({name:"bRegex_"+a,value:e.aoPreSearchCols[a].bRegex}),o.push({name:"bSearchable_"+a,value:e.aoColumns[a].bSearchable});if(e.oFeatures.bSort!==!1){var s=null!==e.aaSortingFixed?e.aaSortingFixed.length:0,i=e.aaSorting.length;for(o.push({name:"iSortingCols",value:s+i}),a=0;s>a;a++)o.push({name:"iSortCol_"+a,value:e.aaSortingFixed[a][0]}),o.push({name:"sSortDir_"+a,value:e.aaSortingFixed[a][1]});for(a=0;i>a;a++)o.push({name:"iSortCol_"+(a+s),value:e.aaSorting[a][0]}),o.push({name:"sSortDir_"+(a+s),value:e.aaSorting[a][1]});for(a=0;n>a;a++)o.push({name:"bSortable_"+a,value:e.aoColumns[a].bSortable})}return o}function _fnServerParams(e,t){for(var a=0,n=e.aoServerParams.length;n>a;a++)e.aoServerParams[a].fn.call(e.oInstance,t)}function _fnAjaxUpdateDraw(e,t){if("undefined"!=typeof t.sEcho){if(1*t.sEcho<e.iDraw)return;e.iDraw=1*t.sEcho}(!e.oScroll.bInfinite||e.oScroll.bInfinite&&(e.bSorted||e.bFiltered))&&_fnClearTable(e),e._iRecordsTotal=t.iTotalRecords,e._iRecordsDisplay=t.iTotalDisplayRecords;var a=_fnColumnOrdering(e),n="undefined"!=typeof t.sColumns&&""!==a&&t.sColumns!=a;if(n)var o=_fnReOrderIndex(e,t.sColumns);for(var s=_fnGetObjectDataFn(e.sAjaxDataProp),i=s(t),r=0,l=i.length;l>r;r++)if(n){for(var f=[],d=0,u=e.aoColumns.length;u>d;d++)f.push(i[r][o[d]]);_fnAddData(e,f)}else _fnAddData(e,i[r]);e.aiDisplay=e.aiDisplayMaster.slice(),e.bAjaxDataGet=!1,_fnDraw(e),e.bAjaxDataGet=!0,_fnProcessingDisplay(e,!1)}function _fnAddOptionsHtml(e){var t=document.createElement("div");e.nTable.parentNode.insertBefore(t,e.nTable),e.nTableWrapper=document.createElement("div"),e.nTableWrapper.className=e.oClasses.sWrapper,""!==e.sTableId&&e.nTableWrapper.setAttribute("id",e.sTableId+"_wrapper"),e.nTableReinsertBefore=e.nTable.nextSibling;for(var a,n,o,s,i,r,l,f=e.nTableWrapper,d=e.sDom.split(""),u=0;u<d.length;u++){if(n=0,o=d[u],"<"==o){if(s=document.createElement("div"),i=d[u+1],"'"==i||'"'==i){for(r="",l=2;d[u+l]!=i;)r+=d[u+l],l++;if("H"==r?r="fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix":"F"==r&&(r="fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix"),-1!=r.indexOf(".")){var p=r.split(".");s.setAttribute("id",p[0].substr(1,p[0].length-1)),s.className=p[1]}else"#"==r.charAt(0)?s.setAttribute("id",r.substr(1,r.length-1)):s.className=r;u+=l}f.appendChild(s),f=s}else if(">"==o)f=f.parentNode;else if("l"==o&&e.oFeatures.bPaginate&&e.oFeatures.bLengthChange)a=_fnFeatureHtmlLength(e),n=1;else if("f"==o&&e.oFeatures.bFilter)a=_fnFeatureHtmlFilter(e),n=1;else if("r"==o&&e.oFeatures.bProcessing)a=_fnFeatureHtmlProcessing(e),n=1;else if("t"==o)a=_fnFeatureHtmlTable(e),n=1;else if("i"==o&&e.oFeatures.bInfo)a=_fnFeatureHtmlInfo(e),n=1;else if("p"==o&&e.oFeatures.bPaginate)a=_fnFeatureHtmlPaginate(e),n=1;else if(0!==_oExt.aoFeatures.length)for(var c=_oExt.aoFeatures,h=0,g=c.length;g>h;h++)if(o==c[h].cFeature){a=c[h].fnInit(e),a&&(n=1);break}1==n&&null!==a&&("object"!=typeof e.aanFeatures[o]&&(e.aanFeatures[o]=[]),e.aanFeatures[o].push(a),f.appendChild(a))}t.parentNode.replaceChild(e.nTableWrapper,t)}function _fnFeatureHtmlTable(e){if(""===e.oScroll.sX&&""===e.oScroll.sY)return e.nTable;var t=document.createElement("div"),a=document.createElement("div"),n=document.createElement("div"),o=document.createElement("div"),s=document.createElement("div"),i=document.createElement("div"),r=e.nTable.cloneNode(!1),l=e.nTable.cloneNode(!1),f=e.nTable.getElementsByTagName("thead")[0],d=0===e.nTable.getElementsByTagName("tfoot").length?null:e.nTable.getElementsByTagName("tfoot")[0],u="undefined"!=typeof oInit.bJQueryUI&&oInit.bJQueryUI?_oExt.oJUIClasses:_oExt.oStdClasses;a.appendChild(n),s.appendChild(i),o.appendChild(e.nTable),t.appendChild(a),t.appendChild(o),n.appendChild(r),r.appendChild(f),null!==d&&(t.appendChild(s),i.appendChild(l),l.appendChild(d)),t.className=u.sScrollWrapper,a.className=u.sScrollHead,n.className=u.sScrollHeadInner,o.className=u.sScrollBody,s.className=u.sScrollFoot,i.className=u.sScrollFootInner,e.oScroll.bAutoCss&&(a.style.overflow="hidden",a.style.position="relative",s.style.overflow="hidden",o.style.overflow="auto"),a.style.border="0",a.style.width="100%",s.style.border="0",n.style.width="150%",r.removeAttribute("id"),r.style.marginLeft="0",e.nTable.style.marginLeft="0",null!==d&&(l.removeAttribute("id"),l.style.marginLeft="0");for(var p=$(e.nTable).children("caption"),c=0,h=p.length;h>c;c++)r.appendChild(p[c]);return""!==e.oScroll.sX&&(a.style.width=_fnStringToCss(e.oScroll.sX),o.style.width=_fnStringToCss(e.oScroll.sX),null!==d&&(s.style.width=_fnStringToCss(e.oScroll.sX)),$(o).scroll(function(e){a.scrollLeft=this.scrollLeft,null!==d&&(s.scrollLeft=this.scrollLeft)})),""!==e.oScroll.sY&&(o.style.height=_fnStringToCss(e.oScroll.sY)),e.aoDrawCallback.push({fn:_fnScrollDraw,sName:"scrolling"}),e.oScroll.bInfinite&&$(o).scroll(function(){e.bDrawing||$(this).scrollTop()+$(this).height()>$(e.nTable).height()-e.oScroll.iLoadGap&&e.fnDisplayEnd()<e.fnRecordsDisplay()&&(_fnPageChange(e,"next"),_fnCalculateEnd(e),_fnDraw(e))}),e.nScrollHead=a,e.nScrollFoot=s,t}function _fnScrollDraw(e){var t,a,n,o,s,i,r,l,f,d,u=e.nScrollHead.getElementsByTagName("div")[0],p=u.getElementsByTagName("table")[0],c=e.nTable.parentNode,h=[],g=null!==e.nTFoot?e.nScrollFoot.getElementsByTagName("div")[0]:null,_=null!==e.nTFoot?g.getElementsByTagName("table")[0]:null,S=$.browser.msie&&$.browser.version<=7,C=e.nTable.getElementsByTagName("thead");if(C.length>0&&e.nTable.removeChild(C[0]),null!==e.nTFoot){var m=e.nTable.getElementsByTagName("tfoot");m.length>0&&e.nTable.removeChild(m[0])}C=e.nTHead.cloneNode(!0),e.nTable.insertBefore(C,e.nTable.childNodes[0]),null!==e.nTFoot&&(m=e.nTFoot.cloneNode(!0),e.nTable.insertBefore(m,e.nTable.childNodes[1])),""===e.oScroll.sX&&(c.style.width="100%",u.parentNode.style.width="100%");var b=_fnGetUniqueThs(e,C);for(t=0,a=b.length;a>t;t++)l=_fnVisibleToColumnIndex(e,t),
b[t].style.width=e.aoColumns[l].sWidth;if(null!==e.nTFoot&&_fnApplyToChildren(function(e){e.style.width=""},m.getElementsByTagName("tr")),d=$(e.nTable).outerWidth(),""===e.oScroll.sX?(e.nTable.style.width="100%",S&&(c.scrollHeight>c.offsetHeight||"scroll"==$(c).css("overflow-y"))&&(e.nTable.style.width=_fnStringToCss($(e.nTable).outerWidth()-e.oScroll.iBarWidth))):""!==e.oScroll.sXInner?e.nTable.style.width=_fnStringToCss(e.oScroll.sXInner):d==$(c).width()&&$(c).height()<$(e.nTable).height()?(e.nTable.style.width=_fnStringToCss(d-e.oScroll.iBarWidth),$(e.nTable).outerWidth()>d-e.oScroll.iBarWidth&&(e.nTable.style.width=_fnStringToCss(d))):e.nTable.style.width=_fnStringToCss(d),d=$(e.nTable).outerWidth(),n=e.nTHead.getElementsByTagName("tr"),o=C.getElementsByTagName("tr"),_fnApplyToChildren(function(e,t){r=e.style,r.paddingTop="0",r.paddingBottom="0",r.borderTopWidth="0",r.borderBottomWidth="0",r.height=0,f=$(e).width(),t.style.width=_fnStringToCss(f),h.push(f)},o,n),$(o).height(0),null!==e.nTFoot&&(s=m.getElementsByTagName("tr"),i=e.nTFoot.getElementsByTagName("tr"),_fnApplyToChildren(function(e,t){r=e.style,r.paddingTop="0",r.paddingBottom="0",r.borderTopWidth="0",r.borderBottomWidth="0",r.height=0,f=$(e).width(),t.style.width=_fnStringToCss(f),h.push(f)},s,i),$(s).height(0)),_fnApplyToChildren(function(e){e.innerHTML="",e.style.width=_fnStringToCss(h.shift())},o),null!==e.nTFoot&&_fnApplyToChildren(function(e){e.innerHTML="",e.style.width=_fnStringToCss(h.shift())},s),$(e.nTable).outerWidth()<d){var D=c.scrollHeight>c.offsetHeight||"scroll"==$(c).css("overflow-y")?d+e.oScroll.iBarWidth:d;S&&(c.scrollHeight>c.offsetHeight||"scroll"==$(c).css("overflow-y"))&&(e.nTable.style.width=_fnStringToCss(D-e.oScroll.iBarWidth)),c.style.width=_fnStringToCss(D),u.parentNode.style.width=_fnStringToCss(D),null!==e.nTFoot&&(g.parentNode.style.width=_fnStringToCss(D)),""===e.oScroll.sX?_fnLog(e,1,"The table cannot fit into the current element which will cause column misalignment. The table has been drawn at its minimum possible width."):""!==e.oScroll.sXInner&&_fnLog(e,1,"The table cannot fit into the current element which will cause column misalignment. Increase the sScrollXInner value or remove it to allow automatic calculation")}else c.style.width=_fnStringToCss("100%"),u.parentNode.style.width=_fnStringToCss("100%"),null!==e.nTFoot&&(g.parentNode.style.width=_fnStringToCss("100%"));if(""===e.oScroll.sY&&S&&(c.style.height=_fnStringToCss(e.nTable.offsetHeight+e.oScroll.iBarWidth)),""!==e.oScroll.sY&&e.oScroll.bCollapse){c.style.height=_fnStringToCss(e.oScroll.sY);var y=""!==e.oScroll.sX&&e.nTable.offsetWidth>c.offsetWidth?e.oScroll.iBarWidth:0;e.nTable.offsetHeight<c.offsetHeight&&(c.style.height=_fnStringToCss($(e.nTable).height()+y))}var T=$(e.nTable).outerWidth();p.style.width=_fnStringToCss(T),u.style.width=_fnStringToCss(T+e.oScroll.iBarWidth),null!==e.nTFoot&&(g.style.width=_fnStringToCss(e.nTable.offsetWidth+e.oScroll.iBarWidth),_.style.width=_fnStringToCss(e.nTable.offsetWidth)),(e.bSorted||e.bFiltered)&&(c.scrollTop=0)}function _fnAdjustColumnSizing(e){if(e.oFeatures.bAutoWidth===!1)return!1;_fnCalculateColumnWidths(e);for(var t=0,a=e.aoColumns.length;a>t;t++)e.aoColumns[t].nTh.style.width=e.aoColumns[t].sWidth}function _fnFeatureHtmlFilter(e){var t=e.oLanguage.sSearch;t=-1!==t.indexOf("_INPUT_")?t.replace("_INPUT_",'<input type="text" />'):""===t?'<input type="text" />':t+' <input type="text" />';var a=document.createElement("div");a.className=e.oClasses.sFilter,a.innerHTML="<label>"+t+"</label>",""!==e.sTableId&&"undefined"==typeof e.aanFeatures.f&&a.setAttribute("id",e.sTableId+"_filter");var n=$("input",a);return n.val(e.oPreviousSearch.sSearch.replace('"',"&quot;")),n.bind("keyup.DT",function(t){for(var a=e.aanFeatures.f,n=0,o=a.length;o>n;n++)a[n]!=$(this).parents("div.dataTables_filter")[0]&&$("input",a[n]).val(this.value);this.value!=e.oPreviousSearch.sSearch&&_fnFilterComplete(e,{sSearch:this.value,bRegex:e.oPreviousSearch.bRegex,bSmart:e.oPreviousSearch.bSmart})}),n.bind("keypress.DT",function(e){return 13==e.keyCode?!1:void 0}),a}function _fnFilterComplete(e,t,a){_fnFilter(e,t.sSearch,a,t.bRegex,t.bSmart);for(var n=0;n<e.aoPreSearchCols.length;n++)_fnFilterColumn(e,e.aoPreSearchCols[n].sSearch,n,e.aoPreSearchCols[n].bRegex,e.aoPreSearchCols[n].bSmart);0!==_oExt.afnFiltering.length&&_fnFilterCustom(e),e.bFiltered=!0,$(e.oInstance).trigger("filter",e),e._iDisplayStart=0,_fnCalculateEnd(e),_fnDraw(e),_fnBuildSearchArray(e,0)}function _fnFilterCustom(e){for(var t=_oExt.afnFiltering,a=0,n=t.length;n>a;a++)for(var o=0,s=0,i=e.aiDisplay.length;i>s;s++){var r=e.aiDisplay[s-o];t[a](e,_fnGetRowData(e,r,"filter"),r)||(e.aiDisplay.splice(s-o,1),o++)}}function _fnFilterColumn(e,t,a,n,o){if(""!==t)for(var s=0,i=_fnFilterCreateSearch(t,n,o),r=e.aiDisplay.length-1;r>=0;r--){var l=_fnDataToSearch(_fnGetCellData(e,e.aiDisplay[r],a,"filter"),e.aoColumns[a].sType);i.test(l)||(e.aiDisplay.splice(r,1),s++)}}function _fnFilter(e,t,a,n,o){var s,i=_fnFilterCreateSearch(t,n,o);if(("undefined"==typeof a||null===a)&&(a=0),0!==_oExt.afnFiltering.length&&(a=1),t.length<=0)e.aiDisplay.splice(0,e.aiDisplay.length),e.aiDisplay=e.aiDisplayMaster.slice();else if(e.aiDisplay.length==e.aiDisplayMaster.length||e.oPreviousSearch.sSearch.length>t.length||1==a||0!==t.indexOf(e.oPreviousSearch.sSearch))for(e.aiDisplay.splice(0,e.aiDisplay.length),_fnBuildSearchArray(e,1),s=0;s<e.aiDisplayMaster.length;s++)i.test(e.asDataSearch[s])&&e.aiDisplay.push(e.aiDisplayMaster[s]);else{var r=0;for(s=0;s<e.asDataSearch.length;s++)i.test(e.asDataSearch[s])||(e.aiDisplay.splice(s-r,1),r++)}e.oPreviousSearch.sSearch=t,e.oPreviousSearch.bRegex=n,e.oPreviousSearch.bSmart=o}function _fnBuildSearchArray(e,t){if(!e.oFeatures.bServerSide){e.asDataSearch.splice(0,e.asDataSearch.length);for(var a="undefined"!=typeof t&&1==t?e.aiDisplayMaster:e.aiDisplay,n=0,o=a.length;o>n;n++)e.asDataSearch[n]=_fnBuildSearchRow(e,_fnGetRowData(e,a[n],"filter"))}}function _fnBuildSearchRow(e,t){var a="";"undefined"==typeof e.__nTmpFilter&&(e.__nTmpFilter=document.createElement("div"));for(var n=e.__nTmpFilter,o=0,s=e.aoColumns.length;s>o;o++)if(e.aoColumns[o].bSearchable){var i=t[o];a+=_fnDataToSearch(i,e.aoColumns[o].sType)+"  "}return-1!==a.indexOf("&")&&(n.innerHTML=a,a=n.textContent?n.textContent:n.innerText,a=a.replace(/\n/g," ").replace(/\r/g,"")),a}function _fnFilterCreateSearch(e,t,a){var n,o;return a?(n=t?e.split(" "):_fnEscapeRegex(e).split(" "),o="^(?=.*?"+n.join(")(?=.*?")+").*$",new RegExp(o,"i")):(e=t?e:_fnEscapeRegex(e),new RegExp(e,"i"))}function _fnDataToSearch(e,t){return"function"==typeof _oExt.ofnSearch[t]?_oExt.ofnSearch[t](e):"html"==t?e.replace(/\n/g," ").replace(/<.*?>/g,""):"string"==typeof e?e.replace(/\n/g," "):null===e?"":e}function _fnSort(e,t){var a,n,o,s,i=[],r=[],l=_oExt.oSort,f=e.aoData,d=e.aoColumns;if(!e.oFeatures.bServerSide&&(0!==e.aaSorting.length||null!==e.aaSortingFixed)){for(i=null!==e.aaSortingFixed?e.aaSortingFixed.concat(e.aaSorting):e.aaSorting.slice(),a=0;a<i.length;a++){var u=i[a][0],p=_fnColumnIndexToVisible(e,u),c=e.aoColumns[u].sSortDataType;if("undefined"!=typeof _oExt.afnSortData[c]){var h=_oExt.afnSortData[c](e,u,p);for(o=0,s=f.length;s>o;o++)_fnSetCellData(e,o,u,h[o])}}for(a=0,n=e.aiDisplayMaster.length;n>a;a++)r[e.aiDisplayMaster[a]]=a;var g=i.length;e.aiDisplayMaster.sort(function(t,n){var o,s,f;for(a=0;g>a;a++)if(s=d[i[a][0]].iDataSort,f=d[s].sType,o=l[(f?f:"string")+"-"+i[a][1]](_fnGetCellData(e,t,s,"sort"),_fnGetCellData(e,n,s,"sort")),0!==o)return o;return l["numeric-asc"](r[t],r[n])})}"undefined"!=typeof t&&!t||e.oFeatures.bDeferRender||_fnSortingClasses(e),e.bSorted=!0,$(e.oInstance).trigger("sort",e),e.oFeatures.bFilter?_fnFilterComplete(e,e.oPreviousSearch,1):(e.aiDisplay=e.aiDisplayMaster.slice(),e._iDisplayStart=0,_fnCalculateEnd(e),_fnDraw(e))}function _fnSortAttachListener(e,t,a,n){$(t).bind("click.DT",function(t){if(e.aoColumns[a].bSortable!==!1){var o=function(){var n,o;if(t.shiftKey){for(var s=!1,i=0;i<e.aaSorting.length;i++)if(e.aaSorting[i][0]==a){s=!0,n=e.aaSorting[i][0],o=e.aaSorting[i][2]+1,"undefined"==typeof e.aoColumns[n].asSorting[o]?e.aaSorting.splice(i,1):(e.aaSorting[i][1]=e.aoColumns[n].asSorting[o],e.aaSorting[i][2]=o);break}s===!1&&e.aaSorting.push([a,e.aoColumns[a].asSorting[0],0])}else 1==e.aaSorting.length&&e.aaSorting[0][0]==a?(n=e.aaSorting[0][0],o=e.aaSorting[0][2]+1,"undefined"==typeof e.aoColumns[n].asSorting[o]&&(o=0),e.aaSorting[0][1]=e.aoColumns[n].asSorting[o],e.aaSorting[0][2]=o):(e.aaSorting.splice(0,e.aaSorting.length),e.aaSorting.push([a,e.aoColumns[a].asSorting[0],0]));_fnSort(e)};e.oFeatures.bProcessing?(_fnProcessingDisplay(e,!0),setTimeout(function(){o(),e.oFeatures.bServerSide||_fnProcessingDisplay(e,!1)},0)):o(),"function"==typeof n&&n(e)}})}function _fnSortingClasses(e){var t,a,n,o,s,i,r=e.aoColumns.length,l=e.oClasses;for(t=0;r>t;t++)e.aoColumns[t].bSortable&&$(e.aoColumns[t].nTh).removeClass(l.sSortAsc+" "+l.sSortDesc+" "+e.aoColumns[t].sSortingClass);for(s=null!==e.aaSortingFixed?e.aaSortingFixed.concat(e.aaSorting):e.aaSorting.slice(),t=0;t<e.aoColumns.length;t++)if(e.aoColumns[t].bSortable){for(i=e.aoColumns[t].sSortingClass,o=-1,a=0;a<s.length;a++)if(s[a][0]==t){i="asc"==s[a][1]?l.sSortAsc:l.sSortDesc,o=a;break}if($(e.aoColumns[t].nTh).addClass(i),e.bJUI){var f=$("span",e.aoColumns[t].nTh);f.removeClass(l.sSortJUIAsc+" "+l.sSortJUIDesc+" "+l.sSortJUI+" "+l.sSortJUIAscAllowed+" "+l.sSortJUIDescAllowed);var d;d=-1==o?e.aoColumns[t].sSortingClassJUI:"asc"==s[o][1]?l.sSortJUIAsc:l.sSortJUIDesc,f.addClass(d)}}else $(e.aoColumns[t].nTh).addClass(e.aoColumns[t].sSortingClass);if(i=l.sSortColumn,e.oFeatures.bSort&&e.oFeatures.bSortClasses){var u=_fnGetTdNodes(e);if(e.oFeatures.bDeferRender)$(u).removeClass(i+"1 "+i+"2 "+i+"3");else if(u.length>=r)for(t=0;r>t;t++)if(-1!=u[t].className.indexOf(i+"1"))for(a=0,n=u.length/r;n>a;a++)u[r*a+t].className=$.trim(u[r*a+t].className.replace(i+"1",""));else if(-1!=u[t].className.indexOf(i+"2"))for(a=0,n=u.length/r;n>a;a++)u[r*a+t].className=$.trim(u[r*a+t].className.replace(i+"2",""));else if(-1!=u[t].className.indexOf(i+"3"))for(a=0,n=u.length/r;n>a;a++)u[r*a+t].className=$.trim(u[r*a+t].className.replace(" "+i+"3",""));var p,c=1;for(t=0;t<s.length;t++){for(p=parseInt(s[t][0],10),a=0,n=u.length/r;n>a;a++)u[r*a+p].className+=" "+i+c;3>c&&c++}}}function _fnFeatureHtmlPaginate(e){if(e.oScroll.bInfinite)return null;var t=document.createElement("div");return t.className=e.oClasses.sPaging+e.sPaginationType,_oExt.oPagination[e.sPaginationType].fnInit(e,t,function(e){_fnCalculateEnd(e),_fnDraw(e)}),"undefined"==typeof e.aanFeatures.p&&e.aoDrawCallback.push({fn:function(e){_oExt.oPagination[e.sPaginationType].fnUpdate(e,function(e){_fnCalculateEnd(e),_fnDraw(e)})},sName:"pagination"}),t}function _fnPageChange(e,t){var a=e._iDisplayStart;if("first"==t)e._iDisplayStart=0;else if("previous"==t)e._iDisplayStart=e._iDisplayLength>=0?e._iDisplayStart-e._iDisplayLength:0,e._iDisplayStart<0&&(e._iDisplayStart=0);else if("next"==t)e._iDisplayLength>=0?e._iDisplayStart+e._iDisplayLength<e.fnRecordsDisplay()&&(e._iDisplayStart+=e._iDisplayLength):e._iDisplayStart=0;else if("last"==t)if(e._iDisplayLength>=0){var n=parseInt((e.fnRecordsDisplay()-1)/e._iDisplayLength,10)+1;e._iDisplayStart=(n-1)*e._iDisplayLength}else e._iDisplayStart=0;else _fnLog(e,0,"Unknown paging action: "+t);return $(e.oInstance).trigger("page",e),a!=e._iDisplayStart}function _fnFeatureHtmlInfo(e){var t=document.createElement("div");return t.className=e.oClasses.sInfo,"undefined"==typeof e.aanFeatures.i&&(e.aoDrawCallback.push({fn:_fnUpdateInfo,sName:"information"}),""!==e.sTableId&&t.setAttribute("id",e.sTableId+"_info")),t}function _fnUpdateInfo(e){if(e.oFeatures.bInfo&&0!==e.aanFeatures.i.length){var t,a=e._iDisplayStart+1,n=e.fnDisplayEnd(),o=e.fnRecordsTotal(),s=e.fnRecordsDisplay(),i=e.fnFormatNumber(a),r=e.fnFormatNumber(n),l=e.fnFormatNumber(o),f=e.fnFormatNumber(s);e.oScroll.bInfinite&&(i=e.fnFormatNumber(1)),t=0===e.fnRecordsDisplay()&&e.fnRecordsDisplay()==e.fnRecordsTotal()?e.oLanguage.sInfoEmpty+e.oLanguage.sInfoPostFix:0===e.fnRecordsDisplay()?e.oLanguage.sInfoEmpty+" "+e.oLanguage.sInfoFiltered.replace("_MAX_",l)+e.oLanguage.sInfoPostFix:e.fnRecordsDisplay()==e.fnRecordsTotal()?e.oLanguage.sInfo.replace("_START_",i).replace("_END_",r).replace("_TOTAL_",f)+e.oLanguage.sInfoPostFix:e.oLanguage.sInfo.replace("_START_",i).replace("_END_",r).replace("_TOTAL_",f)+" "+e.oLanguage.sInfoFiltered.replace("_MAX_",e.fnFormatNumber(e.fnRecordsTotal()))+e.oLanguage.sInfoPostFix,null!==e.oLanguage.fnInfoCallback&&(t=e.oLanguage.fnInfoCallback(e,a,n,o,s,t));for(var d=e.aanFeatures.i,u=0,p=d.length;p>u;u++)$(d[u]).html(t)}}function _fnFeatureHtmlLength(e){if(e.oScroll.bInfinite)return null;var t,a,n=""===e.sTableId?"":'name="'+e.sTableId+'_length"',o='<select size="1" '+n+">";if(2==e.aLengthMenu.length&&"object"==typeof e.aLengthMenu[0]&&"object"==typeof e.aLengthMenu[1])for(t=0,a=e.aLengthMenu[0].length;a>t;t++)o+='<option value="'+e.aLengthMenu[0][t]+'">'+e.aLengthMenu[1][t]+"</option>";else for(t=0,a=e.aLengthMenu.length;a>t;t++)o+='<option value="'+e.aLengthMenu[t]+'">'+e.aLengthMenu[t]+"</option>";o+="</select>";var s=document.createElement("div");return""!==e.sTableId&&"undefined"==typeof e.aanFeatures.l&&s.setAttribute("id",e.sTableId+"_length"),s.className=e.oClasses.sLength,s.innerHTML="<label>"+e.oLanguage.sLengthMenu.replace("_MENU_",o)+"</label>",$('select option[value="'+e._iDisplayLength+'"]',s).attr("selected",!0),$("select",s).bind("change.DT",function(n){var o=$(this).val(),s=e.aanFeatures.l;for(t=0,a=s.length;a>t;t++)s[t]!=this.parentNode&&$("select",s[t]).val(o);e._iDisplayLength=parseInt(o,10),_fnCalculateEnd(e),e.fnDisplayEnd()==e.fnRecordsDisplay()&&(e._iDisplayStart=e.fnDisplayEnd()-e._iDisplayLength,e._iDisplayStart<0&&(e._iDisplayStart=0)),-1==e._iDisplayLength&&(e._iDisplayStart=0),_fnDraw(e)}),s}function _fnFeatureHtmlProcessing(e){var t=document.createElement("div");return""!==e.sTableId&&"undefined"==typeof e.aanFeatures.r&&t.setAttribute("id",e.sTableId+"_processing"),t.innerHTML=e.oLanguage.sProcessing,t.className=e.oClasses.sProcessing,e.nTable.parentNode.insertBefore(t,e.nTable),t}function _fnProcessingDisplay(e,t){if(e.oFeatures.bProcessing)for(var a=e.aanFeatures.r,n=0,o=a.length;o>n;n++)a[n].style.visibility=t?"visible":"hidden"}function _fnVisibleToColumnIndex(e,t){for(var a=-1,n=0;n<e.aoColumns.length;n++)if(e.aoColumns[n].bVisible===!0&&a++,a==t)return n;return null}function _fnColumnIndexToVisible(e,t){for(var a=-1,n=0;n<e.aoColumns.length;n++)if(e.aoColumns[n].bVisible===!0&&a++,n==t)return e.aoColumns[n].bVisible===!0?a:null;return null}function _fnNodeToDataIndex(e,t){var a,n;for(a=e._iDisplayStart,n=e._iDisplayEnd;n>a;a++)if(e.aoData[e.aiDisplay[a]].nTr==t)return e.aiDisplay[a];for(a=0,n=e.aoData.length;n>a;a++)if(e.aoData[a].nTr==t)return a;return null}function _fnVisbleColumns(e){for(var t=0,a=0;a<e.aoColumns.length;a++)e.aoColumns[a].bVisible===!0&&t++;return t}function _fnCalculateEnd(e){e.oFeatures.bPaginate===!1?e._iDisplayEnd=e.aiDisplay.length:e._iDisplayStart+e._iDisplayLength>e.aiDisplay.length||-1==e._iDisplayLength?e._iDisplayEnd=e.aiDisplay.length:e._iDisplayEnd=e._iDisplayStart+e._iDisplayLength}function _fnConvertToWidth(e,t){if(!e||null===e||""===e)return 0;"undefined"==typeof t&&(t=document.getElementsByTagName("body")[0]);var a,n=document.createElement("div");return n.style.width=_fnStringToCss(e),t.appendChild(n),a=n.offsetWidth,t.removeChild(n),a}function _fnCalculateColumnWidths(e){var t,a,n,o,s=(e.nTable.offsetWidth,0),i=0,r=e.aoColumns.length,l=$("th",e.nTHead);for(a=0;r>a;a++)e.aoColumns[a].bVisible&&(i++,null!==e.aoColumns[a].sWidth&&(t=_fnConvertToWidth(e.aoColumns[a].sWidthOrig,e.nTable.parentNode),null!==t&&(e.aoColumns[a].sWidth=_fnStringToCss(t)),s++));if(r==l.length&&0===s&&i==r&&""===e.oScroll.sX&&""===e.oScroll.sY)for(a=0;a<e.aoColumns.length;a++)t=$(l[a]).width(),null!==t&&(e.aoColumns[a].sWidth=_fnStringToCss(t));else{var f=e.nTable.cloneNode(!1),d=e.nTHead.cloneNode(!0),u=document.createElement("tbody"),p=document.createElement("tr");f.removeAttribute("id"),f.appendChild(d),null!==e.nTFoot&&(f.appendChild(e.nTFoot.cloneNode(!0)),_fnApplyToChildren(function(e){e.style.width=""},f.getElementsByTagName("tr"))),f.appendChild(u),u.appendChild(p);var c=$("thead th",f);0===c.length&&(c=$("tbody tr:eq(0)>td",f));var h=_fnGetUniqueThs(e,d);for(n=0,a=0;r>a;a++){var g=e.aoColumns[a];g.bVisible&&null!==g.sWidthOrig&&""!==g.sWidthOrig?h[a-n].style.width=_fnStringToCss(g.sWidthOrig):g.bVisible?h[a-n].style.width="":n++}for(a=0;r>a;a++)if(e.aoColumns[a].bVisible){var _=_fnGetWidestNode(e,a);null!==_&&(_=_.cloneNode(!0),""!==e.aoColumns[a].sContentPadding&&(_.innerHTML+=e.aoColumns[a].sContentPadding),p.appendChild(_))}var S=e.nTable.parentNode;S.appendChild(f),""!==e.oScroll.sX&&""!==e.oScroll.sXInner?f.style.width=_fnStringToCss(e.oScroll.sXInner):""!==e.oScroll.sX?(f.style.width="",$(f).width()<S.offsetWidth&&(f.style.width=_fnStringToCss(S.offsetWidth))):""!==e.oScroll.sY&&(f.style.width=_fnStringToCss(S.offsetWidth)),f.style.visibility="hidden",_fnScrollingWidthAdjust(e,f);var C=$("tbody tr:eq(0)",f).children();if(0===C.length&&(C=_fnGetUniqueThs(e,$("thead",f)[0])),""!==e.oScroll.sX){var m=0;for(n=0,a=0;a<e.aoColumns.length;a++)e.aoColumns[a].bVisible&&(m+=null===e.aoColumns[a].sWidthOrig?$(C[n]).outerWidth():parseInt(e.aoColumns[a].sWidth.replace("px",""),10)+($(C[n]).outerWidth()-$(C[n]).width()),n++);f.style.width=_fnStringToCss(m),e.nTable.style.width=_fnStringToCss(m)}for(n=0,a=0;a<e.aoColumns.length;a++)e.aoColumns[a].bVisible&&(o=$(C[n]).width(),null!==o&&o>0&&(e.aoColumns[a].sWidth=_fnStringToCss(o)),n++);e.nTable.style.width=_fnStringToCss($(f).outerWidth()),f.parentNode.removeChild(f)}}function _fnScrollingWidthAdjust(e,t){if(""===e.oScroll.sX&&""!==e.oScroll.sY){$(t).width();t.style.width=_fnStringToCss($(t).outerWidth()-e.oScroll.iBarWidth)}else""!==e.oScroll.sX&&(t.style.width=_fnStringToCss($(t).outerWidth()))}function _fnGetWidestNode(e,t){var a=_fnGetMaxLenString(e,t);if(0>a)return null;if(null===e.aoData[a].nTr){var n=document.createElement("td");return n.innerHTML=_fnGetCellData(e,a,t,""),n}return _fnGetTdNodes(e,a)[t]}function _fnGetMaxLenString(e,t){for(var a=-1,n=-1,o=0;o<e.aoData.length;o++){var s=_fnGetCellData(e,o,t,"display")+"";s=s.replace(/<.*?>/g,""),s.length>a&&(a=s.length,n=o)}return n}function _fnStringToCss(e){if(null===e)return"0px";if("number"==typeof e)return 0>e?"0px":e+"px";var t=e.charCodeAt(e.length-1);return 48>t||t>57?e:e+"px"}function _fnArrayCmp(e,t){if(e.length!=t.length)return 1;for(var a=0;a<e.length;a++)if(e[a]!=t[a])return 2;return 0}function _fnDetectType(e){for(var t=_oExt.aTypes,a=t.length,n=0;a>n;n++){var o=t[n](e);if(null!==o)return o}return"string"}function _fnSettingsFromNode(e){for(var t=0;t<_aoSettings.length;t++)if(_aoSettings[t].nTable==e)return _aoSettings[t];return null}function _fnGetDataMaster(e){for(var t=[],a=e.aoData.length,n=0;a>n;n++)t.push(e.aoData[n]._aData);return t}function _fnGetTrNodes(e){for(var t=[],a=0,n=e.aoData.length;n>a;a++)null!==e.aoData[a].nTr&&t.push(e.aoData[a].nTr);return t}function _fnGetTdNodes(e,t){var a,n,o,s,i,r,l,f=[],d=e.aoData.length,u=0,p=d;for("undefined"!=typeof t&&(u=t,p=t+1),o=u;p>o;o++)if(r=e.aoData[o],null!==r.nTr){for(n=[],s=0,i=r.nTr.childNodes.length;i>s;s++)l=r.nTr.childNodes[s].nodeName.toLowerCase(),("td"==l||"th"==l)&&n.push(r.nTr.childNodes[s]);for(a=0,s=0,i=e.aoColumns.length;i>s;s++)e.aoColumns[s].bVisible?f.push(n[s-a]):(f.push(r._anHidden[s]),a++)}return f}function _fnEscapeRegex(e){var t=["/",".","*","+","?","|","(",")","[","]","{","}","\\","$","^"],a=new RegExp("(\\"+t.join("|\\")+")","g");return e.replace(a,"\\$1")}function _fnDeleteIndex(e,t){for(var a=-1,n=0,o=e.length;o>n;n++)e[n]==t?a=n:e[n]>t&&e[n]--;-1!=a&&e.splice(a,1)}function _fnReOrderIndex(e,t){for(var a=t.split(","),n=[],o=0,s=e.aoColumns.length;s>o;o++)for(var i=0;s>i;i++)if(e.aoColumns[o].sName==a[i]){n.push(i);break}return n}function _fnColumnOrdering(e){for(var t="",a=0,n=e.aoColumns.length;n>a;a++)t+=e.aoColumns[a].sName+",";return t.length==n?"":t.slice(0,-1)}function _fnLog(e,t,a){var n=""===e.sTableId?"DataTables warning: "+a:"DataTables warning (table id = '"+e.sTableId+"'): "+a;if(0===t){if("alert"!=_oExt.sErrMode)throw n;return void alert(n)}"undefined"!=typeof console&&"undefined"!=typeof console.log&&console.log(n)}function _fnClearTable(e){e.aoData.splice(0,e.aoData.length),e.aiDisplayMaster.splice(0,e.aiDisplayMaster.length),e.aiDisplay.splice(0,e.aiDisplay.length),_fnCalculateEnd(e)}function _fnSaveState(e){if(e.oFeatures.bStateSave&&"undefined"==typeof e.bDestroying){var t,a,n,o="{";for(o+='"iCreate":'+(new Date).getTime()+",",o+='"iStart":'+(e.oScroll.bInfinite?0:e._iDisplayStart)+",",o+='"iEnd":'+(e.oScroll.bInfinite?e._iDisplayLength:e._iDisplayEnd)+",",o+='"iLength":'+e._iDisplayLength+",",o+='"sFilter":"'+encodeURIComponent(e.oPreviousSearch.sSearch)+'",',o+='"sFilterEsc":'+!e.oPreviousSearch.bRegex+",",o+='"aaSorting":[ ',t=0;t<e.aaSorting.length;t++)o+="["+e.aaSorting[t][0]+',"'+e.aaSorting[t][1]+'"],';for(o=o.substring(0,o.length-1),o+="],",o+='"aaSearchCols":[ ',t=0;t<e.aoPreSearchCols.length;t++)o+='["'+encodeURIComponent(e.aoPreSearchCols[t].sSearch)+'",'+!e.aoPreSearchCols[t].bRegex+"],";for(o=o.substring(0,o.length-1),o+="],",o+='"abVisCols":[ ',t=0;t<e.aoColumns.length;t++)o+=e.aoColumns[t].bVisible+",";for(o=o.substring(0,o.length-1),o+="]",t=0,a=e.aoStateSave.length;a>t;t++)n=e.aoStateSave[t].fn(e,o),""!==n&&(o=n);o+="}",_fnCreateCookie(e.sCookiePrefix+e.sInstance,o,e.iCookieDuration,e.sCookiePrefix,e.fnCookieCallback)}}function _fnLoadState(oSettings,oInit){if(oSettings.oFeatures.bStateSave){var oData,i,iLen,sData=_fnReadCookie(oSettings.sCookiePrefix+oSettings.sInstance);if(null!==sData&&""!==sData){try{oData="function"==typeof $.parseJSON?$.parseJSON(sData.replace(/'/g,'"')):eval("("+sData+")")}catch(e){return}for(i=0,iLen=oSettings.aoStateLoad.length;iLen>i;i++)if(!oSettings.aoStateLoad[i].fn(oSettings,oData))return;if(oSettings.oLoadedState=$.extend(!0,{},oData),oSettings._iDisplayStart=oData.iStart,oSettings.iInitDisplayStart=oData.iStart,oSettings._iDisplayEnd=oData.iEnd,oSettings._iDisplayLength=oData.iLength,oSettings.oPreviousSearch.sSearch=decodeURIComponent(oData.sFilter),oSettings.aaSorting=oData.aaSorting.slice(),oSettings.saved_aaSorting=oData.aaSorting.slice(),"undefined"!=typeof oData.sFilterEsc&&(oSettings.oPreviousSearch.bRegex=!oData.sFilterEsc),"undefined"!=typeof oData.aaSearchCols)for(i=0;i<oData.aaSearchCols.length;i++)oSettings.aoPreSearchCols[i]={sSearch:decodeURIComponent(oData.aaSearchCols[i][0]),bRegex:!oData.aaSearchCols[i][1]};if("undefined"!=typeof oData.abVisCols)for(oInit.saved_aoColumns=[],i=0;i<oData.abVisCols.length;i++)oInit.saved_aoColumns[i]={},oInit.saved_aoColumns[i].bVisible=oData.abVisCols[i]}}}function _fnCreateCookie(sName,sValue,iSecs,sBaseName,fnCallback){var date=new Date;date.setTime(date.getTime()+1e3*iSecs);var aParts=window.location.pathname.split("/"),sNameFile=sName+"_"+aParts.pop().replace(/[\/:]/g,"").toLowerCase(),sFullCookie,oData;null!==fnCallback?(oData="function"==typeof $.parseJSON?$.parseJSON(sValue):eval("("+sValue+")"),sFullCookie=fnCallback(sNameFile,oData,date.toGMTString(),aParts.join("/")+"/")):sFullCookie=sNameFile+"="+encodeURIComponent(sValue)+"; expires="+date.toGMTString()+"; path="+aParts.join("/")+"/";var sOldName="",iOldTime=9999999999999,iLength=null!==_fnReadCookie(sNameFile)?document.cookie.length:sFullCookie.length+document.cookie.length;if(iLength+10>4096){for(var aCookies=document.cookie.split(";"),i=0,iLen=aCookies.length;iLen>i;i++)if(-1!=aCookies[i].indexOf(sBaseName)){var aSplitCookie=aCookies[i].split("=");try{oData=eval("("+decodeURIComponent(aSplitCookie[1])+")")}catch(e){continue}"undefined"!=typeof oData.iCreate&&oData.iCreate<iOldTime&&(sOldName=aSplitCookie[0],iOldTime=oData.iCreate)}""!==sOldName&&(document.cookie=sOldName+"=; expires=Thu, 01-Jan-1970 00:00:01 GMT; path="+aParts.join("/")+"/")}document.cookie=sFullCookie}function _fnReadCookie(e){for(var t=window.location.pathname.split("/"),a=e+"_"+t[t.length-1].replace(/[\/:]/g,"").toLowerCase()+"=",n=document.cookie.split(";"),o=0;o<n.length;o++){for(var s=n[o];" "==s.charAt(0);)s=s.substring(1,s.length);if(0===s.indexOf(a))return decodeURIComponent(s.substring(a.length,s.length))}return null}function _fnDetectHeader(e,t){var a,n,o,s,i,r,l,f,d=$(t).children("tr"),u=function(e,t,a){for(;"undefined"!=typeof e[t][a];)a++;return a};for(e.splice(0,e.length),n=0,r=d.length;r>n;n++)e.push([]);for(n=0,r=d.length;r>n;n++){var p=0;for(o=0,l=d[n].childNodes.length;l>o;o++)if(a=d[n].childNodes[o],"TD"==a.nodeName.toUpperCase()||"TH"==a.nodeName.toUpperCase()){var c=1*a.getAttribute("colspan"),h=1*a.getAttribute("rowspan");for(c=c&&0!==c&&1!==c?c:1,h=h&&0!==h&&1!==h?h:1,f=u(e,n,p),i=0;c>i;i++)for(s=0;h>s;s++)e[n+s][f+i]={cell:a,unique:1==c?!0:!1},e[n+s].nTr=d[n]}}}function _fnGetUniqueThs(e,t,a){var n=[];"undefined"==typeof a&&(a=e.aoHeader,"undefined"!=typeof t&&(a=[],_fnDetectHeader(a,t)));for(var o=0,s=a.length;s>o;o++)for(var i=0,r=a[o].length;r>i;i++)!a[o][i].unique||"undefined"!=typeof n[i]&&e.bSortCellsTop||(n[i]=a[o][i].cell);return n}function _fnScrollBarWidth(){var e=document.createElement("p"),t=e.style;t.width="100%",t.height="200px",t.padding="0px";var a=document.createElement("div");t=a.style,t.position="absolute",t.top="0px",t.left="0px",t.visibility="hidden",t.width="200px",t.height="150px",t.padding="0px",t.overflow="hidden",a.appendChild(e),document.body.appendChild(a);var n=e.offsetWidth;a.style.overflow="scroll";var o=e.offsetWidth;return n==o&&(o=a.clientWidth),document.body.removeChild(a),n-o}function _fnApplyToChildren(e,t,a){for(var n=0,o=t.length;o>n;n++)for(var s=0,i=t[n].childNodes.length;i>s;s++)1==t[n].childNodes[s].nodeType&&("undefined"!=typeof a?e(t[n].childNodes[s],a[n].childNodes[s]):e(t[n].childNodes[s]))}function _fnMap(e,t,a,n){"undefined"==typeof n&&(n=a),"undefined"!=typeof t[a]&&(e[n]=t[a])}function _fnGetRowData(e,t,a){for(var n=[],o=0,s=e.aoColumns.length;s>o;o++)n.push(_fnGetCellData(e,t,o,a));return n}function _fnGetCellData(e,t,a,n){var o,s=e.aoColumns[a],i=e.aoData[t]._aData;if(void 0===(o=s.fnGetData(i)))return e.iDrawError!=e.iDraw&&null===s.sDefaultContent&&(_fnLog(e,0,"Requested unknown parameter '"+s.mDataProp+"' from the data source for row "+t),e.iDrawError=e.iDraw),s.sDefaultContent;if(null===o&&null!==s.sDefaultContent)o=s.sDefaultContent;else if("function"==typeof o)return o();return"display"==n&&null===o?"":o}function _fnSetCellData(e,t,a,n){var o=e.aoColumns[a],s=e.aoData[t]._aData;o.fnSetData(s,n)}function _fnGetObjectDataFn(e){if(null===e)return function(e){return null};if("function"==typeof e)return function(t){return e(t)};if("string"==typeof e&&-1!=e.indexOf(".")){var t=e.split(".");return 2==t.length?function(e){return e[t[0]][t[1]]}:3==t.length?function(e){return e[t[0]][t[1]][t[2]]}:function(e){for(var a=0,n=t.length;n>a;a++)e=e[t[a]];return e}}return function(t){return t[e]}}function _fnSetObjectDataFn(e){if(null===e)return function(e,t){};if("function"==typeof e)return function(t,a){return e(t,a)};if("string"==typeof e&&-1!=e.indexOf(".")){var t=e.split(".");return 2==t.length?function(e,a){e[t[0]][t[1]]=a}:3==t.length?function(e,a){e[t[0]][t[1]][t[2]]=a}:function(e,a){for(var n=0,o=t.length-1;o>n;n++)e=e[t[n]];e[t[t.length-1]]=a}}return function(t,a){t[e]=a}}this.oApi={},this.fnDraw=function(e){var t=_fnSettingsFromNode(this[_oExt.iApiIndex]);"undefined"!=typeof e&&e===!1?(_fnCalculateEnd(t),_fnDraw(t)):_fnReDraw(t)},this.fnFilter=function(e,t,a,n,o){var s=_fnSettingsFromNode(this[_oExt.iApiIndex]);if(s.oFeatures.bFilter)if("undefined"==typeof a&&(a=!1),"undefined"==typeof n&&(n=!0),"undefined"==typeof o&&(o=!0),"undefined"==typeof t||null===t){if(_fnFilterComplete(s,{sSearch:e,bRegex:a,bSmart:n},1),o&&"undefined"!=typeof s.aanFeatures.f)for(var i=s.aanFeatures.f,r=0,l=i.length;l>r;r++)$("input",i[r]).val(e)}else s.aoPreSearchCols[t].sSearch=e,s.aoPreSearchCols[t].bRegex=a,s.aoPreSearchCols[t].bSmart=n,_fnFilterComplete(s,s.oPreviousSearch,1)},this.fnSettings=function(e){return _fnSettingsFromNode(this[_oExt.iApiIndex])},this.fnVersionCheck=_oExt.fnVersionCheck,this.fnSort=function(e){var t=_fnSettingsFromNode(this[_oExt.iApiIndex]);t.aaSorting=e,_fnSort(t)},this.fnSortListener=function(e,t,a){_fnSortAttachListener(_fnSettingsFromNode(this[_oExt.iApiIndex]),e,t,a)},this.fnAddData=function(e,t){if(0===e.length)return[];var a,n=[],o=_fnSettingsFromNode(this[_oExt.iApiIndex]);if("object"==typeof e[0])for(var s=0;s<e.length;s++){if(a=_fnAddData(o,e[s]),-1==a)return n;n.push(a)}else{if(a=_fnAddData(o,e),-1==a)return n;n.push(a)}return o.aiDisplay=o.aiDisplayMaster.slice(),("undefined"==typeof t||t)&&_fnReDraw(o),n},this.fnDeleteRow=function(e,t,a){var n,o=_fnSettingsFromNode(this[_oExt.iApiIndex]);n="object"==typeof e?_fnNodeToDataIndex(o,e):e;var s=o.aoData.splice(n,1),i=$.inArray(n,o.aiDisplay);return o.asDataSearch.splice(i,1),_fnDeleteIndex(o.aiDisplayMaster,n),_fnDeleteIndex(o.aiDisplay,n),"function"==typeof t&&t.call(this,o,s),o._iDisplayStart>=o.aiDisplay.length&&(o._iDisplayStart-=o._iDisplayLength,o._iDisplayStart<0&&(o._iDisplayStart=0)),("undefined"==typeof a||a)&&(_fnCalculateEnd(o),_fnDraw(o)),s},this.fnClearTable=function(e){var t=_fnSettingsFromNode(this[_oExt.iApiIndex]);_fnClearTable(t),("undefined"==typeof e||e)&&_fnDraw(t)},this.fnOpen=function(e,t,a){var n=_fnSettingsFromNode(this[_oExt.iApiIndex]);this.fnClose(e);var o=document.createElement("tr"),s=document.createElement("td");o.appendChild(s),s.className=a,s.colSpan=_fnVisbleColumns(n),"undefined"!=typeof t.jquery||"object"==typeof t?s.appendChild(t):s.innerHTML=t;var i=$("tr",n.nTBody);return-1!=$.inArray(e,i)&&$(o).insertAfter(e),n.aoOpenRows.push({nTr:o,nParent:e}),o},this.fnClose=function(e){for(var t=_fnSettingsFromNode(this[_oExt.iApiIndex]),a=0;a<t.aoOpenRows.length;a++)if(t.aoOpenRows[a].nParent==e){var n=t.aoOpenRows[a].nTr.parentNode;return n&&n.removeChild(t.aoOpenRows[a].nTr),t.aoOpenRows.splice(a,1),0}return 1},this.fnGetData=function(e,t){var a=_fnSettingsFromNode(this[_oExt.iApiIndex]);if("undefined"!=typeof e){var n="object"==typeof e?_fnNodeToDataIndex(a,e):e;return"undefined"!=typeof t?_fnGetCellData(a,n,t,""):"undefined"!=typeof a.aoData[n]?a.aoData[n]._aData:null}return _fnGetDataMaster(a)},this.fnGetNodes=function(e){var t=_fnSettingsFromNode(this[_oExt.iApiIndex]);return"undefined"!=typeof e?"undefined"!=typeof t.aoData[e]?t.aoData[e].nTr:null:_fnGetTrNodes(t)},this.fnGetPosition=function(e){var t=_fnSettingsFromNode(this[_oExt.iApiIndex]),a=e.nodeName.toUpperCase();if("TR"==a)return _fnNodeToDataIndex(t,e);if("TD"==a||"TH"==a)for(var n=_fnNodeToDataIndex(t,e.parentNode),o=_fnGetTdNodes(t,n),s=0;s<t.aoColumns.length;s++)if(o[s]==e)return[n,_fnColumnIndexToVisible(t,s),s];return null},this.fnUpdate=function(e,t,a,n,o){var s,i,r=_fnSettingsFromNode(this[_oExt.iApiIndex]),l="object"==typeof t?_fnNodeToDataIndex(r,t):t;if($.isArray(e)&&"object"==typeof e)for(r.aoData[l]._aData=e.slice(),s=0;s<r.aoColumns.length;s++)this.fnUpdate(_fnGetCellData(r,l,s),l,s,!1,!1);else if(null!==e&&"object"==typeof e)for(r.aoData[l]._aData=$.extend(!0,{},e),s=0;s<r.aoColumns.length;s++)this.fnUpdate(_fnGetCellData(r,l,s),l,s,!1,!1);else i=e,_fnSetCellData(r,l,a,i),null!==r.aoColumns[a].fnRender&&(i=r.aoColumns[a].fnRender({iDataRow:l,iDataColumn:a,aData:r.aoData[l]._aData,oSettings:r}),r.aoColumns[a].bUseRendered&&_fnSetCellData(r,l,a,i)),null!==r.aoData[l].nTr&&(_fnGetTdNodes(r,l)[a].innerHTML=i);var f=$.inArray(l,r.aiDisplay);return r.asDataSearch[f]=_fnBuildSearchRow(r,_fnGetRowData(r,l,"filter")),("undefined"==typeof o||o)&&_fnAdjustColumnSizing(r),
("undefined"==typeof n||n)&&_fnReDraw(r),0},this.fnSetColumnVis=function(e,t,a){var n,o,s,i,r,l=_fnSettingsFromNode(this[_oExt.iApiIndex]),f=l.aoColumns.length;if(l.aoColumns[e].bVisible!=t){if(t){var d=0;for(n=0;e>n;n++)l.aoColumns[n].bVisible&&d++;if(i=d>=_fnVisbleColumns(l),!i)for(n=e;f>n;n++)if(l.aoColumns[n].bVisible){r=n;break}for(n=0,o=l.aoData.length;o>n;n++)null!==l.aoData[n].nTr&&(i?l.aoData[n].nTr.appendChild(l.aoData[n]._anHidden[e]):l.aoData[n].nTr.insertBefore(l.aoData[n]._anHidden[e],_fnGetTdNodes(l,n)[r]))}else for(n=0,o=l.aoData.length;o>n;n++)null!==l.aoData[n].nTr&&(s=_fnGetTdNodes(l,n)[e],l.aoData[n]._anHidden[e]=s,s.parentNode.removeChild(s));for(l.aoColumns[e].bVisible=t,_fnDrawHead(l,l.aoHeader),l.nTFoot&&_fnDrawHead(l,l.aoFooter),n=0,o=l.aoOpenRows.length;o>n;n++)l.aoOpenRows[n].nTr.colSpan=_fnVisbleColumns(l);("undefined"==typeof a||a)&&(_fnAdjustColumnSizing(l),_fnDraw(l)),_fnSaveState(l)}},this.fnPageChange=function(e,t){var a=_fnSettingsFromNode(this[_oExt.iApiIndex]);_fnPageChange(a,e),_fnCalculateEnd(a),("undefined"==typeof t||t)&&_fnDraw(a)},this.fnDestroy=function(){var e,t,a=_fnSettingsFromNode(this[_oExt.iApiIndex]),n=a.nTableWrapper.parentNode,o=a.nTBody;for(a.bDestroying=!0,e=0,t=a.aoDestroyCallback.length;t>e;e++)a.aoDestroyCallback[e].fn();for(e=0,t=a.aoColumns.length;t>e;e++)a.aoColumns[e].bVisible===!1&&this.fnSetColumnVis(e,!0);for($(a.nTableWrapper).find("*").andSelf().unbind(".DT"),$("tbody>tr>td."+a.oClasses.sRowEmpty,a.nTable).parent().remove(),a.nTable!=a.nTHead.parentNode&&($(a.nTable).children("thead").remove(),a.nTable.appendChild(a.nTHead)),a.nTFoot&&a.nTable!=a.nTFoot.parentNode&&($(a.nTable).children("tfoot").remove(),a.nTable.appendChild(a.nTFoot)),a.nTable.parentNode.removeChild(a.nTable),$(a.nTableWrapper).remove(),a.aaSorting=[],a.aaSortingFixed=[],_fnSortingClasses(a),$(_fnGetTrNodes(a)).removeClass(a.asStripeClasses.join(" ")),a.bJUI?($("th",a.nTHead).removeClass([_oExt.oStdClasses.sSortable,_oExt.oJUIClasses.sSortableAsc,_oExt.oJUIClasses.sSortableDesc,_oExt.oJUIClasses.sSortableNone].join(" ")),$("th span."+_oExt.oJUIClasses.sSortIcon,a.nTHead).remove(),$("th",a.nTHead).each(function(){var e=$("div."+_oExt.oJUIClasses.sSortJUIWrapper,this),t=e.contents();$(this).append(t),e.remove()})):$("th",a.nTHead).removeClass([_oExt.oStdClasses.sSortable,_oExt.oStdClasses.sSortableAsc,_oExt.oStdClasses.sSortableDesc,_oExt.oStdClasses.sSortableNone].join(" ")),a.nTableReinsertBefore?n.insertBefore(a.nTable,a.nTableReinsertBefore):n.appendChild(a.nTable),e=0,t=a.aoData.length;t>e;e++)null!==a.aoData[e].nTr&&o.appendChild(a.aoData[e].nTr);for(a.oFeatures.bAutoWidth===!0&&(a.nTable.style.width=_fnStringToCss(a.sDestroyWidth)),$(o).children("tr:even").addClass(a.asDestroyStripes[0]),$(o).children("tr:odd").addClass(a.asDestroyStripes[1]),e=0,t=_aoSettings.length;t>e;e++)_aoSettings[e]==a&&_aoSettings.splice(e,1);a=null},this.fnAdjustColumnSizing=function(e){var t=_fnSettingsFromNode(this[_oExt.iApiIndex]);_fnAdjustColumnSizing(t),"undefined"==typeof e||e?this.fnDraw(!1):(""!==t.oScroll.sX||""!==t.oScroll.sY)&&this.oApi._fnScrollDraw(t)};for(var sFunc in _oExt.oApi)sFunc&&(this[sFunc]=_fnExternApiFunc(sFunc));this.oApi._fnExternApiFunc=_fnExternApiFunc,this.oApi._fnInitialise=_fnInitialise,this.oApi._fnInitComplete=_fnInitComplete,this.oApi._fnLanguageProcess=_fnLanguageProcess,this.oApi._fnAddColumn=_fnAddColumn,this.oApi._fnColumnOptions=_fnColumnOptions,this.oApi._fnAddData=_fnAddData,this.oApi._fnCreateTr=_fnCreateTr,this.oApi._fnGatherData=_fnGatherData,this.oApi._fnBuildHead=_fnBuildHead,this.oApi._fnDrawHead=_fnDrawHead,this.oApi._fnDraw=_fnDraw,this.oApi._fnReDraw=_fnReDraw,this.oApi._fnAjaxUpdate=_fnAjaxUpdate,this.oApi._fnAjaxParameters=_fnAjaxParameters,this.oApi._fnAjaxUpdateDraw=_fnAjaxUpdateDraw,this.oApi._fnServerParams=_fnServerParams,this.oApi._fnAddOptionsHtml=_fnAddOptionsHtml,this.oApi._fnFeatureHtmlTable=_fnFeatureHtmlTable,this.oApi._fnScrollDraw=_fnScrollDraw,this.oApi._fnAdjustColumnSizing=_fnAdjustColumnSizing,this.oApi._fnFeatureHtmlFilter=_fnFeatureHtmlFilter,this.oApi._fnFilterComplete=_fnFilterComplete,this.oApi._fnFilterCustom=_fnFilterCustom,this.oApi._fnFilterColumn=_fnFilterColumn,this.oApi._fnFilter=_fnFilter,this.oApi._fnBuildSearchArray=_fnBuildSearchArray,this.oApi._fnBuildSearchRow=_fnBuildSearchRow,this.oApi._fnFilterCreateSearch=_fnFilterCreateSearch,this.oApi._fnDataToSearch=_fnDataToSearch,this.oApi._fnSort=_fnSort,this.oApi._fnSortAttachListener=_fnSortAttachListener,this.oApi._fnSortingClasses=_fnSortingClasses,this.oApi._fnFeatureHtmlPaginate=_fnFeatureHtmlPaginate,this.oApi._fnPageChange=_fnPageChange,this.oApi._fnFeatureHtmlInfo=_fnFeatureHtmlInfo,this.oApi._fnUpdateInfo=_fnUpdateInfo,this.oApi._fnFeatureHtmlLength=_fnFeatureHtmlLength,this.oApi._fnFeatureHtmlProcessing=_fnFeatureHtmlProcessing,this.oApi._fnProcessingDisplay=_fnProcessingDisplay,this.oApi._fnVisibleToColumnIndex=_fnVisibleToColumnIndex,this.oApi._fnColumnIndexToVisible=_fnColumnIndexToVisible,this.oApi._fnNodeToDataIndex=_fnNodeToDataIndex,this.oApi._fnVisbleColumns=_fnVisbleColumns,this.oApi._fnCalculateEnd=_fnCalculateEnd,this.oApi._fnConvertToWidth=_fnConvertToWidth,this.oApi._fnCalculateColumnWidths=_fnCalculateColumnWidths,this.oApi._fnScrollingWidthAdjust=_fnScrollingWidthAdjust,this.oApi._fnGetWidestNode=_fnGetWidestNode,this.oApi._fnGetMaxLenString=_fnGetMaxLenString,this.oApi._fnStringToCss=_fnStringToCss,this.oApi._fnArrayCmp=_fnArrayCmp,this.oApi._fnDetectType=_fnDetectType,this.oApi._fnSettingsFromNode=_fnSettingsFromNode,this.oApi._fnGetDataMaster=_fnGetDataMaster,this.oApi._fnGetTrNodes=_fnGetTrNodes,this.oApi._fnGetTdNodes=_fnGetTdNodes,this.oApi._fnEscapeRegex=_fnEscapeRegex,this.oApi._fnDeleteIndex=_fnDeleteIndex,this.oApi._fnReOrderIndex=_fnReOrderIndex,this.oApi._fnColumnOrdering=_fnColumnOrdering,this.oApi._fnLog=_fnLog,this.oApi._fnClearTable=_fnClearTable,this.oApi._fnSaveState=_fnSaveState,this.oApi._fnLoadState=_fnLoadState,this.oApi._fnCreateCookie=_fnCreateCookie,this.oApi._fnReadCookie=_fnReadCookie,this.oApi._fnDetectHeader=_fnDetectHeader,this.oApi._fnGetUniqueThs=_fnGetUniqueThs,this.oApi._fnScrollBarWidth=_fnScrollBarWidth,this.oApi._fnApplyToChildren=_fnApplyToChildren,this.oApi._fnMap=_fnMap,this.oApi._fnGetRowData=_fnGetRowData,this.oApi._fnGetCellData=_fnGetCellData,this.oApi._fnSetCellData=_fnSetCellData,this.oApi._fnGetObjectDataFn=_fnGetObjectDataFn,this.oApi._fnSetObjectDataFn=_fnSetObjectDataFn;var _that=this;return this.each(function(){var e,t,a,n,o,s=0;for(s=0,e=_aoSettings.length;e>s;s++){if(_aoSettings[s].nTable==this){if("undefined"==typeof oInit||"undefined"!=typeof oInit.bRetrieve&&oInit.bRetrieve===!0)return _aoSettings[s].oInstance;if("undefined"!=typeof oInit.bDestroy&&oInit.bDestroy===!0){_aoSettings[s].oInstance.fnDestroy();break}return void _fnLog(_aoSettings[s],0,"Cannot reinitialise DataTable.\n\nTo retrieve the DataTables object for this table, please pass either no arguments to the dataTable() function, or set bRetrieve to true. Alternatively, to destory the old table and create a new one, set bDestroy to true (note that a lot of changes to the configuration can be made through the API which is usually much faster).")}if(""!==_aoSettings[s].sTableId&&_aoSettings[s].sTableId==this.getAttribute("id")){_aoSettings.splice(s,1);break}}var i=new classSettings;_aoSettings.push(i);var r=!1,l=!1,f=this.getAttribute("id");if(null!==f?(i.sTableId=f,i.sInstance=f):i.sInstance=_oExt._oExternConfig.iNextUnique++,"table"!=this.nodeName.toLowerCase())return void _fnLog(i,0,"Attempted to initialise DataTables on a node which is not a table: "+this.nodeName);i.nTable=this,i.oInstance=1==_that.length?_that:$(this).dataTable(),i.oApi=_that.oApi,i.sDestroyWidth=$(this).width(),"undefined"!=typeof oInit&&null!==oInit?(i.oInit=oInit,_fnMap(i.oFeatures,oInit,"bPaginate"),_fnMap(i.oFeatures,oInit,"bLengthChange"),_fnMap(i.oFeatures,oInit,"bFilter"),_fnMap(i.oFeatures,oInit,"bSort"),_fnMap(i.oFeatures,oInit,"bInfo"),_fnMap(i.oFeatures,oInit,"bProcessing"),_fnMap(i.oFeatures,oInit,"bAutoWidth"),_fnMap(i.oFeatures,oInit,"bSortClasses"),_fnMap(i.oFeatures,oInit,"bServerSide"),_fnMap(i.oFeatures,oInit,"bDeferRender"),_fnMap(i.oScroll,oInit,"sScrollX","sX"),_fnMap(i.oScroll,oInit,"sScrollXInner","sXInner"),_fnMap(i.oScroll,oInit,"sScrollY","sY"),_fnMap(i.oScroll,oInit,"bScrollCollapse","bCollapse"),_fnMap(i.oScroll,oInit,"bScrollInfinite","bInfinite"),_fnMap(i.oScroll,oInit,"iScrollLoadGap","iLoadGap"),_fnMap(i.oScroll,oInit,"bScrollAutoCss","bAutoCss"),_fnMap(i,oInit,"asStripClasses","asStripeClasses"),_fnMap(i,oInit,"asStripeClasses"),_fnMap(i,oInit,"fnPreDrawCallback"),_fnMap(i,oInit,"fnRowCallback"),_fnMap(i,oInit,"fnHeaderCallback"),_fnMap(i,oInit,"fnFooterCallback"),_fnMap(i,oInit,"fnCookieCallback"),_fnMap(i,oInit,"fnInitComplete"),_fnMap(i,oInit,"fnServerData"),_fnMap(i,oInit,"fnFormatNumber"),_fnMap(i,oInit,"aaSorting"),_fnMap(i,oInit,"aaSortingFixed"),_fnMap(i,oInit,"aLengthMenu"),_fnMap(i,oInit,"sPaginationType"),_fnMap(i,oInit,"sAjaxSource"),_fnMap(i,oInit,"sAjaxDataProp"),_fnMap(i,oInit,"iCookieDuration"),_fnMap(i,oInit,"sCookiePrefix"),_fnMap(i,oInit,"sDom"),_fnMap(i,oInit,"bSortCellsTop"),_fnMap(i,oInit,"oSearch","oPreviousSearch"),_fnMap(i,oInit,"aoSearchCols","aoPreSearchCols"),_fnMap(i,oInit,"iDisplayLength","_iDisplayLength"),_fnMap(i,oInit,"bJQueryUI","bJUI"),_fnMap(i.oLanguage,oInit,"fnInfoCallback"),"function"==typeof oInit.fnDrawCallback&&i.aoDrawCallback.push({fn:oInit.fnDrawCallback,sName:"user"}),"function"==typeof oInit.fnServerParams&&i.aoServerParams.push({fn:oInit.fnServerParams,sName:"user"}),"function"==typeof oInit.fnStateSaveCallback&&i.aoStateSave.push({fn:oInit.fnStateSaveCallback,sName:"user"}),"function"==typeof oInit.fnStateLoadCallback&&i.aoStateLoad.push({fn:oInit.fnStateLoadCallback,sName:"user"}),i.oFeatures.bServerSide&&i.oFeatures.bSort&&i.oFeatures.bSortClasses?i.aoDrawCallback.push({fn:_fnSortingClasses,sName:"server_side_sort_classes"}):i.oFeatures.bDeferRender&&i.aoDrawCallback.push({fn:_fnSortingClasses,sName:"defer_sort_classes"}),"undefined"!=typeof oInit.bJQueryUI&&oInit.bJQueryUI&&(i.oClasses=_oExt.oJUIClasses,"undefined"==typeof oInit.sDom&&(i.sDom='<"H"lfr>t<"F"ip>')),(""!==i.oScroll.sX||""!==i.oScroll.sY)&&(i.oScroll.iBarWidth=_fnScrollBarWidth()),"undefined"!=typeof oInit.iDisplayStart&&"undefined"==typeof i.iInitDisplayStart&&(i.iInitDisplayStart=oInit.iDisplayStart,i._iDisplayStart=oInit.iDisplayStart),"undefined"!=typeof oInit.bStateSave&&(i.oFeatures.bStateSave=oInit.bStateSave,_fnLoadState(i,oInit),i.aoDrawCallback.push({fn:_fnSaveState,sName:"state_save"})),"undefined"!=typeof oInit.iDeferLoading&&(i.bDeferLoading=!0,i._iRecordsTotal=oInit.iDeferLoading,i._iRecordsDisplay=oInit.iDeferLoading),"undefined"!=typeof oInit.aaData&&(l=!0),"undefined"!=typeof oInit&&"undefined"!=typeof oInit.aoData&&(oInit.aoColumns=oInit.aoData),"undefined"!=typeof oInit.oLanguage&&("undefined"!=typeof oInit.oLanguage.sUrl&&""!==oInit.oLanguage.sUrl?(i.oLanguage.sUrl=oInit.oLanguage.sUrl,$.getJSON(i.oLanguage.sUrl,null,function(e){_fnLanguageProcess(i,e,!0)}),r=!0):_fnLanguageProcess(i,oInit.oLanguage,!1))):oInit={},"undefined"==typeof oInit.asStripClasses&&"undefined"==typeof oInit.asStripeClasses&&(i.asStripeClasses.push(i.oClasses.sStripeOdd),i.asStripeClasses.push(i.oClasses.sStripeEven));var d=!1,u=$(this).children("tbody").children("tr");for(s=0,e=i.asStripeClasses.length;e>s;s++)if(u.filter(":lt(2)").hasClass(i.asStripeClasses[s])){d=!0;break}d&&(i.asDestroyStripes=["",""],$(u[0]).hasClass(i.oClasses.sStripeOdd)&&(i.asDestroyStripes[0]+=i.oClasses.sStripeOdd+" "),$(u[0]).hasClass(i.oClasses.sStripeEven)&&(i.asDestroyStripes[0]+=i.oClasses.sStripeEven),$(u[1]).hasClass(i.oClasses.sStripeOdd)&&(i.asDestroyStripes[1]+=i.oClasses.sStripeOdd+" "),$(u[1]).hasClass(i.oClasses.sStripeEven)&&(i.asDestroyStripes[1]+=i.oClasses.sStripeEven),u.removeClass(i.asStripeClasses.join(" ")));var p,c=[],h=this.getElementsByTagName("thead");if(0!==h.length&&(_fnDetectHeader(i.aoHeader,h[0]),c=_fnGetUniqueThs(i)),"undefined"==typeof oInit.aoColumns)for(p=[],s=0,e=c.length;e>s;s++)p.push(null);else p=oInit.aoColumns;for(s=0,e=p.length;e>s;s++)"undefined"!=typeof oInit.saved_aoColumns&&oInit.saved_aoColumns.length==e&&(null===p[s]&&(p[s]={}),p[s].bVisible=oInit.saved_aoColumns[s].bVisible),_fnAddColumn(i,c?c[s]:null);if("undefined"!=typeof oInit.aoColumnDefs)for(s=oInit.aoColumnDefs.length-1;s>=0;s--){var g=oInit.aoColumnDefs[s].aTargets;for($.isArray(g)||_fnLog(i,1,"aTargets must be an array of targets, not a "+typeof g),t=0,a=g.length;a>t;t++)if("number"==typeof g[t]&&g[t]>=0){for(;i.aoColumns.length<=g[t];)_fnAddColumn(i);_fnColumnOptions(i,g[t],oInit.aoColumnDefs[s])}else if("number"==typeof g[t]&&g[t]<0)_fnColumnOptions(i,i.aoColumns.length+g[t],oInit.aoColumnDefs[s]);else if("string"==typeof g[t])for(n=0,o=i.aoColumns.length;o>n;n++)("_all"==g[t]||$(i.aoColumns[n].nTh).hasClass(g[t]))&&_fnColumnOptions(i,n,oInit.aoColumnDefs[s])}if("undefined"!=typeof p)for(s=0,e=p.length;e>s;s++)_fnColumnOptions(i,s,p[s]);for(s=0,e=i.aaSorting.length;e>s;s++){i.aaSorting[s][0]>=i.aoColumns.length&&(i.aaSorting[s][0]=0);var _=i.aoColumns[i.aaSorting[s][0]];for("undefined"==typeof i.aaSorting[s][2]&&(i.aaSorting[s][2]=0),"undefined"==typeof oInit.aaSorting&&"undefined"==typeof i.saved_aaSorting&&(i.aaSorting[s][1]=_.asSorting[0]),t=0,a=_.asSorting.length;a>t;t++)if(i.aaSorting[s][1]==_.asSorting[t]){i.aaSorting[s][2]=t;break}}_fnSortingClasses(i);var S=$(this).children("thead");0===S.length&&(S=[document.createElement("thead")],this.appendChild(S[0])),i.nTHead=S[0];var C=$(this).children("tbody");0===C.length&&(C=[document.createElement("tbody")],this.appendChild(C[0])),i.nTBody=C[0];var m=$(this).children("tfoot");if(m.length>0&&(i.nTFoot=m[0],_fnDetectHeader(i.aoFooter,i.nTFoot)),l)for(s=0;s<oInit.aaData.length;s++)_fnAddData(i,oInit.aaData[s]);else _fnGatherData(i);i.aiDisplay=i.aiDisplayMaster.slice(),i.bInitialised=!0,r===!1&&_fnInitialise(i)})}}(jQuery,window,document);
RegExp.escape=function(s){return s.replace(/[-\/\\^$*+?.()|[\]{}]/g,'\\$&');};(function($){'use strict'
$.csv={defaults:{separator:',',delimiter:'"',headers:true},hooks:{castToScalar:function(value,state){var hasDot=/\./;if(isNaN(value)){return value;}else{if(hasDot.test(value)){return parseFloat(value);}else{var integer=parseInt(value);if(isNaN(integer)){return null;}else{return integer;}}}}},parsers:{parse:function(csv,options){var separator=options.separator;var delimiter=options.delimiter;if(!options.state.rowNum){options.state.rowNum=1;}
if(!options.state.colNum){options.state.colNum=1;}
var data=[];var entry=[];var state=0;var value=''
var exit=false;function endOfEntry(){state=0;value='';if(options.start&&options.state.rowNum<options.start){entry=[];options.state.rowNum++;options.state.colNum=1;return;}
if(options.onParseEntry===undefined){data.push(entry);}else{var hookVal=options.onParseEntry(entry,options.state);if(hookVal!==false){data.push(hookVal);}}
entry=[];if(options.end&&options.state.rowNum>=options.end){exit=true;}
options.state.rowNum++;options.state.colNum=1;}
function endOfValue(){if(options.onParseValue===undefined){entry.push(value);}else{var hook=options.onParseValue(value,options.state);if(hook!==false){entry.push(hook);}}
value='';state=0;options.state.colNum++;}
var escSeparator=RegExp.escape(separator);var escDelimiter=RegExp.escape(delimiter);var match=/(D|S|\n|\r|[^DS\r\n]+)/;var matchSrc=match.source;matchSrc=matchSrc.replace(/S/g,escSeparator);matchSrc=matchSrc.replace(/D/g,escDelimiter);match=RegExp(matchSrc,'gm');csv.replace(match,function(m0){if(exit){return;}
switch(state){case 0:if(m0===separator){value+='';endOfValue();break;}
if(m0===delimiter){state=1;break;}
if(m0==='\n'){endOfValue();endOfEntry();break;}
if(/^\r$/.test(m0)){break;}
value+=m0;state=3;break;case 1:if(m0===delimiter){state=2;break;}
value+=m0;state=1;break;case 2:if(m0===delimiter){value+=m0;state=1;break;}
if(m0===separator){endOfValue();break;}
if(m0==='\n'){endOfValue();endOfEntry();break;}
if(/^\r$/.test(m0)){break;}
throw new Error('CSVDataError: Illegal State [Row:'+options.state.rowNum+'][Col:'+options.state.colNum+']');case 3:if(m0===separator){endOfValue();break;}
if(m0==='\n'){endOfValue();endOfEntry();break;}
if(/^\r$/.test(m0)){break;}
if(m0===delimiter){throw new Error('CSVDataError: Illegal Quote [Row:'+options.state.rowNum+'][Col:'+options.state.colNum+']');}
throw new Error('CSVDataError: Illegal Data [Row:'+options.state.rowNum+'][Col:'+options.state.colNum+']');default:throw new Error('CSVDataError: Unknown State [Row:'+options.state.rowNum+'][Col:'+options.state.colNum+']');}});if(entry.length!==0){endOfValue();endOfEntry();}
return data;},splitLines:function(csv,options){var separator=options.separator;var delimiter=options.delimiter;if(!options.state.rowNum){options.state.rowNum=1;}
var entries=[];var state=0;var entry='';var exit=false;function endOfLine(){state=0;if(options.start&&options.state.rowNum<options.start){entry='';options.state.rowNum++;return;}
if(options.onParseEntry===undefined){entries.push(entry);}else{var hookVal=options.onParseEntry(entry,options.state);if(hookVal!==false){entries.push(hookVal);}}
entry='';if(options.end&&options.state.rowNum>=options.end){exit=true;}
options.state.rowNum++;}
var escSeparator=RegExp.escape(separator);var escDelimiter=RegExp.escape(delimiter);var match=/(D|S|\n|\r|[^DS\r\n]+)/;var matchSrc=match.source;matchSrc=matchSrc.replace(/S/g,escSeparator);matchSrc=matchSrc.replace(/D/g,escDelimiter);match=RegExp(matchSrc,'gm');csv.replace(match,function(m0){if(exit){return;}
switch(state){case 0:if(m0===separator){entry+=m0;state=0;break;}
if(m0===delimiter){entry+=m0;state=1;break;}
if(m0==='\n'){endOfLine();break;}
if(/^\r$/.test(m0)){break;}
entry+=m0;state=3;break;case 1:if(m0===delimiter){entry+=m0;state=2;break;}
entry+=m0;state=1;break;case 2:var prevChar=entry.substr(entry.length-1);if(m0===delimiter&&prevChar===delimiter){entry+=m0;state=1;break;}
if(m0===separator){entry+=m0;state=0;break;}
if(m0==='\n'){endOfLine();break;}
if(m0==='\r'){break;}
throw new Error('CSVDataError: Illegal state [Row:'+options.state.rowNum+']');case 3:if(m0===separator){entry+=m0;state=0;break;}
if(m0==='\n'){endOfLine();break;}
if(m0==='\r'){break;}
if(m0===delimiter){throw new Error('CSVDataError: Illegal quote [Row:'+options.state.rowNum+']');}
throw new Error('CSVDataError: Illegal state [Row:'+options.state.rowNum+']');default:throw new Error('CSVDataError: Unknown state [Row:'+options.state.rowNum+']');}});if(entry!==''){endOfLine();}
return entries;},parseEntry:function(csv,options){var separator=options.separator;var delimiter=options.delimiter;if(!options.state.rowNum){options.state.rowNum=1;}
if(!options.state.colNum){options.state.colNum=1;}
var entry=[];var state=0;var value='';function endOfValue(){if(options.onParseValue===undefined){entry.push(value);}else{var hook=options.onParseValue(value,options.state);if(hook!==false){entry.push(hook);}}
value='';state=0;options.state.colNum++;}
if(!options.match){var escSeparator=RegExp.escape(separator);var escDelimiter=RegExp.escape(delimiter);var match=/(D|S|\n|\r|[^DS\r\n]+)/;var matchSrc=match.source;matchSrc=matchSrc.replace(/S/g,escSeparator);matchSrc=matchSrc.replace(/D/g,escDelimiter);options.match=RegExp(matchSrc,'gm');}
csv.replace(options.match,function(m0){switch(state){case 0:if(m0===separator){value+='';endOfValue();break;}
if(m0===delimiter){state=1;break;}
if(m0==='\n'||m0==='\r'){break;}
value+=m0;state=3;break;case 1:if(m0===delimiter){state=2;break;}
value+=m0;state=1;break;case 2:if(m0===delimiter){value+=m0;state=1;break;}
if(m0===separator){endOfValue();break;}
if(m0==='\n'||m0==='\r'){break;}
throw new Error('CSVDataError: Illegal State [Row:'+options.state.rowNum+'][Col:'+options.state.colNum+']');case 3:if(m0===separator){endOfValue();break;}
if(m0==='\n'||m0==='\r'){break;}
if(m0===delimiter){throw new Error('CSVDataError: Illegal Quote [Row:'+options.state.rowNum+'][Col:'+options.state.colNum+']');}
throw new Error('CSVDataError: Illegal Data [Row:'+options.state.rowNum+'][Col:'+options.state.colNum+']');default:throw new Error('CSVDataError: Unknown State [Row:'+options.state.rowNum+'][Col:'+options.state.colNum+']');}});endOfValue();return entry;}},toArray:function(csv,options,callback){var options=(options!==undefined?options:{});var config={};config.callback=((callback!==undefined&&typeof(callback)==='function')?callback:false);config.separator='separator'in options?options.separator:$.csv.defaults.separator;config.delimiter='delimiter'in options?options.delimiter:$.csv.defaults.delimiter;var state=(options.state!==undefined?options.state:{});var options={delimiter:config.delimiter,separator:config.separator,onParseEntry:options.onParseEntry,onParseValue:options.onParseValue,state:state}
var entry=$.csv.parsers.parseEntry(csv,options);if(!config.callback){return entry;}else{config.callback('',entry);}},toArrays:function(csv,options,callback){var options=(options!==undefined?options:{});var config={};config.callback=((callback!==undefined&&typeof(callback)==='function')?callback:false);config.separator='separator'in options?options.separator:$.csv.defaults.separator;config.delimiter='delimiter'in options?options.delimiter:$.csv.defaults.delimiter;var data=[];var options={delimiter:config.delimiter,separator:config.separator,onParseEntry:options.onParseEntry,onParseValue:options.onParseValue,start:options.start,end:options.end,state:{rowNum:1,colNum:1}};data=$.csv.parsers.parse(csv,options);if(!config.callback){return data;}else{config.callback('',data);}},toObjects:function(csv,options,callback){var options=(options!==undefined?options:{});var config={};config.callback=((callback!==undefined&&typeof(callback)==='function')?callback:false);config.separator='separator'in options?options.separator:$.csv.defaults.separator;config.delimiter='delimiter'in options?options.delimiter:$.csv.defaults.delimiter;config.headers='headers'in options?options.headers:$.csv.defaults.headers;options.start='start'in options?options.start:1;if(config.headers){options.start++;}
if(options.end&&config.headers){options.end++;}
var lines=[];var data=[];var options={delimiter:config.delimiter,separator:config.separator,onParseEntry:options.onParseEntry,onParseValue:options.onParseValue,start:options.start,end:options.end,state:{rowNum:1,colNum:1},match:false};var headerOptions={delimiter:config.delimiter,separator:config.separator,start:1,end:1,state:{rowNum:1,colNum:1}}
var headerLine=$.csv.parsers.splitLines(csv,headerOptions);var headers=$.csv.toArray(headerLine[0],options);var lines=$.csv.parsers.splitLines(csv,options);options.state.colNum=1;if(headers){options.state.rowNum=2;}else{options.state.rowNum=1;}
for(var i=0,len=lines.length;i<len;i++){var entry=$.csv.toArray(lines[i],options);var object={};for(var j in headers){object[headers[j]]=entry[j];}
data.push(object);options.state.rowNum++;}
if(!config.callback){return data;}else{config.callback('',data);}},fromArrays:function(arrays,options,callback){var options=(options!==undefined?options:{});var config={};config.callback=((callback!==undefined&&typeof(callback)==='function')?callback:false);config.separator='separator'in options?options.separator:$.csv.defaults.separator;config.delimiter='delimiter'in options?options.delimiter:$.csv.defaults.delimiter;config.escaper='escaper'in options?options.escaper:$.csv.defaults.escaper;config.experimental='experimental'in options?options.experimental:false;if(!config.experimental){throw new Error('not implemented');}
var output=[];for(i in arrays){output.push(arrays[i]);}
if(!config.callback){return output;}else{config.callback('',output);}},fromObjects2CSV:function(objects,options,callback){var options=(options!==undefined?options:{});var config={};config.callback=((callback!==undefined&&typeof(callback)==='function')?callback:false);config.separator='separator'in options?options.separator:$.csv.defaults.separator;config.delimiter='delimiter'in options?options.delimiter:$.csv.defaults.delimiter;config.experimental='experimental'in options?options.experimental:false;if(!config.experimental){throw new Error('not implemented');}
var output=[];for(i in objects){output.push(arrays[i]);}
if(!config.callback){return output;}else{config.callback('',output);}}};$.csvEntry2Array=$.csv.toArray;$.csv2Array=$.csv.toArrays;$.csv2Dictionary=$.csv.toObjects;})(jQuery);var colorbrewer = [
['#9edae5','#17becf','#dbdb8d','#bcbd22','#c7c7c7','#7f7f7f','#f7b6d2','#e377c2','#c49c94','#8c564b','#c5b0d5','#9467bd','#ff9896','#d62728','#98df8a','#2ca02c','#ffbb78','#ff7f0e','#aec7e8',	'#1f77b4'],
/*['#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c', '#98df8a', '#d62728', '#ff9896', '#9467bd', '#c5b0d5', '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f', '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5'],*/
['#393b79', '#5254a3', '#6b6ecf', '#9c9ede', '#637939', '#8ca252', '#b5cf6b', '#cedb9c', '#8c6d31', '#bd9e39', '#e7ba52', '#e7cb94', '#843c39', '#ad494a', '#d6616b', '#e7969c', '#7b4173', '#a55194', '#ce6dbd', '#de9ed6'],
['#3182bd', '#6baed6', '#9ecae1', '#c6dbef', '#e6550d', '#fd8d3c', '#fdae6b', '#fdd0a2', '#31a354', '#74c476', '#a1d99b', '#c7e9c0', '#756bb1', '#9e9ac8', '#bcbddc', '#dadaeb', '#636363', '#969696', '#bdbdbd', '#d9d9d9'],
['#1f77b4','#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf'  ],
["#ffffe5","#f7fcb9","#d9f0a3","#addd8e","#78c679","#41ab5d","#238443","#006837","#004529"],
["#ffffd9","#edf8b1","#c7e9b4","#7fcdbb","#41b6c4","#1d91c0","#225ea8","#253494","#081d58"],
["#f7fcf0","#e0f3db","#ccebc5","#a8ddb5","#7bccc4","#4eb3d3","#2b8cbe","#0868ac","#084081"],
["#f7fcfd","#e5f5f9","#ccece6","#99d8c9","#66c2a4","#41ae76","#238b45","#006d2c","#00441b"],
["#fff7fb","#ece2f0","#d0d1e6","#a6bddb","#67a9cf","#3690c0","#02818a","#016c59","#014636"],
["#fff7fb","#ece7f2","#d0d1e6","#a6bddb","#74a9cf","#3690c0","#0570b0","#045a8d","#023858"],
["#f7fcfd","#e0ecf4","#bfd3e6","#9ebcda","#8c96c6","#8c6bb1","#88419d","#810f7c","#4d004b"],
["#fff7f3","#fde0dd","#fcc5c0","#fa9fb5","#f768a1","#dd3497","#ae017e","#7a0177","#49006a"],
["#f7f4f9","#e7e1ef","#d4b9da","#c994c7","#df65b0","#e7298a","#ce1256","#980043","#67001f"],
["#fff7ec","#fee8c8","#fdd49e","#fdbb84","#fc8d59","#ef6548","#d7301f","#b30000","#7f0000"],
["#ffffcc","#ffeda0","#fed976","#feb24c","#fd8d3c","#fc4e2a","#e31a1c","#bd0026","#800026"],
["#ffffe5","#fff7bc","#fee391","#fec44f","#fe9929","#ec7014","#cc4c02","#993404","#662506"],
["#fcfbfd","#efedf5","#dadaeb","#bcbddc","#9e9ac8","#807dba","#6a51a3","#54278f","#3f007d"],
["#f7fbff","#deebf7","#c6dbef","#9ecae1","#6baed6","#4292c6","#2171b5","#08519c","#08306b"],
["#f7fcf5","#e5f5e0","#c7e9c0","#a1d99b","#74c476","#41ab5d","#238b45","#006d2c","#00441b"],
["#fff5eb","#fee6ce","#fdd0a2","#fdae6b","#fd8d3c","#f16913","#d94801","#a63603","#7f2704"],
["#fff5f0","#fee0d2","#fcbba1","#fc9272","#fb6a4a","#ef3b2c","#cb181d","#a50f15","#67000d","#fff5eb","#fee6ce","#fdd0a2","#fdae6b","#fd8d3c","#f16913","#d94801","#a63603","#7f2704"],
["#ffffff","#f0f0f0","#d9d9d9","#bdbdbd","#969696","#737373","#525252","#252525","#000000"],
["#7f3b08","#b35806","#e08214","#fdb863","#fee0b6","#f7f7f7","#d8daeb","#b2abd2","#8073ac","#542788","#2d004b"],
["#543005","#8c510a","#bf812d","#dfc27d","#f6e8c3","#f5f5f5","#c7eae5","#80cdc1","#35978f","#01665e","#003c30"],
["#40004b","#762a83","#9970ab","#c2a5cf","#e7d4e8","#f7f7f7","#d9f0d3","#a6dba0","#5aae61","#1b7837","#00441b"],
["#8e0152","#c51b7d","#de77ae","#f1b6da","#fde0ef","#f7f7f7","#e6f5d0","#b8e186","#7fbc41","#4d9221","#276419"],
["#67001f","#b2182b","#d6604d","#f4a582","#fddbc7","#f7f7f7","#d1e5f0","#92c5de","#4393c3","#2166ac","#053061"],
["#67001f","#b2182b","#d6604d","#f4a582","#fddbc7","#ffffff","#e0e0e0","#bababa","#878787","#4d4d4d","#1a1a1a"],
["#a50026","#d73027","#f46d43","#fdae61","#fee090","#ffffbf","#e0f3f8","#abd9e9","#74add1","#4575b4","#313695"],
["#9e0142","#d53e4f","#f46d43","#fdae61","#fee08b","#ffffbf","#e6f598","#abdda4","#66c2a5","#3288bd","#5e4fa2"],
["#a50026","#d73027","#f46d43","#fdae61","#fee08b","#ffffbf","#d9ef8b","#a6d96a","#66bd63","#1a9850","#006837"],
["#7fc97f","#beaed4","#fdc086","#ffff99","#386cb0","#f0027f","#bf5b17","#666666"],
["#1b9e77","#d95f02","#7570b3","#e7298a","#66a61e","#e6ab02","#a6761d","#666666"],
["#a6cee3","#1f78b4","#b2df8a","#33a02c","#fb9a99","#e31a1c","#fdbf6f","#ff7f00","#cab2d6","#6a3d9a","#ffff99","#b15928"],
["#fbb4ae","#b3cde3","#ccebc5","#decbe4","#fed9a6","#ffffcc","#e5d8bd","#fddaec","#f2f2f2"],
["#b3e2cd","#fdcdac","#cbd5e8","#f4cae4","#e6f5c9","#fff2ae","#f1e2cc","#cccccc"],
["#e41a1c","#377eb8","#4daf4a","#984ea3","#ff7f00","#ffff33","#a65628","#f781bf","#999999"],
["#66c2a5","#fc8d62","#8da0cb","#e78ac3","#a6d854","#ffd92f","#e5c494","#b3b3b3"],
["#8dd3c7","#ffffb3","#bebada","#fb8072","#80b1d3","#fdb462","#b3de69","#fccde5","#d9d9d9","#bc80bd","#ccebc5","#ffed6f"],
["#ffffff","#ffffff","#ffffff","#ffffff","#ffffff","#ffffff","#ffffff","#ffffff","#ffffff","#ffffff","#ffffff","#ffffff"]
];


var cbIndex = {};
cbIndex['category20']=0;
cbIndex['category20b']=1;
cbIndex['category20c']=2;
cbIndex['d3Color1']=3;
cbIndex['YlGn']=4;
cbIndex['YlGnBu']=5;
cbIndex['GnBu']=6;
cbIndex['BuGn']=7;
cbIndex['PuBuGn']=8;
cbIndex['PuBu']=9;
cbIndex['BuPu']=10;
cbIndex['RdPu']=11;
cbIndex['PuRd']=12;
cbIndex['OrRd']=13;
cbIndex['YlOrRd']=14;
cbIndex['YlOrBr']=15;
cbIndex['Purples']=16;
cbIndex['Blues']=17;
cbIndex['Greens']=18;
cbIndex['Oranges']=19;
cbIndex['Reds']=20;
cbIndex['Greys']=21;
cbIndex['PuOr']=22;
cbIndex['BrBG']=23;
cbIndex['PRGn']=24;
cbIndex['PiYG']= 25;
cbIndex['RdBu']=26;
cbIndex['RdGy']=27;
cbIndex['RdYlBu']=28;
cbIndex['Spectral']=29;
cbIndex['RdYlGn']=30;
cbIndex['Accent']=31;
cbIndex['Dark2']=32;
cbIndex['Paired']=33;
cbIndex['Pastel1']=34;
cbIndex['Pastel2']=35;
cbIndex['Set1']=36;
cbIndex['Set2']=37;
cbIndex['Set3']=38;
cbIndex['blank']=39;