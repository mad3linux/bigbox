function mtFill(pCtx,px,py,pR,pG,pB,pTopx,pTopy,pBottomx,pBottomy)
{
mtFill.v_ctx=pCtx;
mtFill.v_stack=[];
mtFill.v_topx=pTopx;
mtFill.v_topy=pTopy;
mtFill.v_bottomx=pBottomx;
mtFill.v_bottomy=pBottomy;

mtFill.r=pR;
mtFill.g=pG;
mtFill.b=pB;

mtFill.floodFill(30,30);

}


mtFill.floodFill = function(x, y)
{
    
	mtFill.fillPixel(x, y);

	while(mtFill.v_stack.length>0)
	{
		toFill = mtFill.v_stack.pop();
		mtFill.fillPixel(toFill[0], toFill[1]);
	}
}

mtFill.fillPixel= function(x, y)
{
	if(mtFill.canFill(x, y)) mtFill.fill(x, y);

	if(mtFill.canFill(x,   y-1)) mtFill.v_stack.push([x,   y-1]);
	if(mtFill.canFill(x+1, y  )) mtFill.v_stack.push([x+1, y  ]);
	if(mtFill.canFill(x,   y+1)) mtFill.v_stack.push([x,   y+1]);
	if(mtFill.canFill(x-1, y  )) mtFill.v_stack.push([x-1, y  ]);
	if(mtFill.canFill(x+1, y+1  )) mtFill.v_stack.push([x+1, y+1  ]);
	if(mtFill.canFill(x+1, y-1  )) mtFill.v_stack.push([x+1, y-1  ]);
	if(mtFill.canFill(x-1, y+1  )) mtFill.v_stack.push([x-1, y+1  ]);
	if(mtFill.canFill(x-1, y-1  )) mtFill.v_stack.push([x-1, y-1  ]);
}

mtFill.fill= function(x, y)
{


var data = mtFill.v_ctx.getImageData(x+10, y+10, 1, 1).data;
var r=data[0];
var g =data[1];
var b=data[2];
var a=data[3];

r+=10;
data[0]=r;
mtFill.v_ctx.putImageData(data, x+10, y+10);

}

mtFill.canFill = function(x, y)
{

//if the point goes beyond bounds then don't fill
if (x<=mtFill.v_topx || x>=mtFill.v_bottomx) return false;

if (y<=mtFill.v_topy || y>=mtFill.v_bottomy) return false;



var v_data = mtFill.v_ctx.getImageData(x+10, y+10, 1, 1).data;

if ((v_data[0]==0&& v_data[1]==0 && v_data[2]==0)||(v_data[0]==255&& v_data[1]==255 && v_data[2]==255)) return true;
else return false;

//color = new Color([data[0], data[1], data[2]]);
	// this functions checks to see if our square has been filled already
}