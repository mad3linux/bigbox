		function updateTips( t ) {
			$(".validateTips")
				.text( t )
				.addClass( "ui-state-highlight" );
			setTimeout(function() {
				$(".validateTips").removeClass( "ui-state-highlight", 1500 );
			}, 500 );
		}
		
		function checkLength( o, n, min, max ) {
		   var x=$("#"+o).val();
			if (  x.length < min ) {
				$("#"+o).addClass( "ui-state-error" );
				updateTips( "Length of " + n + " must be atleast " +
					min  );
				return false;
			} else {
				return true;
			}
		}

		function checkRegexp( o, regexp, n ) {
			if ( !( regexp.test( o.val() ) ) ) {
				o.addClass( "ui-state-error" );
				updateTips( n );
				return false;
			} else {
				return true;
			}
		} 

function validateInput()
{
	var errorText="";
	var handle = $("#connection_handle");
	var bflag=checkRegexp( handle, /^[a-z]([0-9a-z_])+$/i, "Connection Handle may consist of a-z, 0-9, underscores, begin with a letter." );
	if (!bflag)
	{
	  //errorText="Connection handle should start with a character and cannot contain space or special characters. It should also be a minimum of 10 characters";
	  //updateTips( errorText);
	  return false;
	}
/*
	$("input:text").each(function()
     	 {
		   if (!checkLength(this.id,this.id,5,300)) return false;
		 }
		 );
		 */
return true;
}

  $(document).ready(function(){
    
	
	$("#connection_form").submit(function() {


				if (!validateInput()) 
				{
				  $("#status p").html("Form Input Error... Please correct and submit again");
				  return false;
				}
				else
				{
				  
				}
				//the post call below calls the process param funciton in the script by passing the serialized form elements
				var dataStr=$("#add_connection_fields"+" :input").serialize(); 
				$.post("connection_handler.php"
				, 
				  dataStr
				, function(data) {
					$("#status p").html(data['message']);
						
						if (data['status']=="SUCCESS")
						{
						  
						}
					if (data['url_redirect']=="TRUE")
					{
					  var url = data['url'];
                      $(location).attr('href',url);
					}  
					//drawPortlets();
					
				}
				, "json"
				);
				return false;
			}); 
	
	$("#test_connect_button").click(function() {


				if (!validateInput()) 
				{
				  $("#status p").html("Form Input Error... Please correct and Test again");
				  return false;
				}
				else
				{
				  
				}
				//the post call below calls the process param funciton in the script by passing the serialized form elements
				var dataStr=$("#add_connection_fields"+" :input").serialize(); 
				$.post("test_database.php"
				, 
				  dataStr
				, function(data) {
				   if (data.error_flag=="false")
				   {
					 $("#connect_status").html("<strong>"+data.error_mesg+"</strong><br><br>Table List<br>"+data.tables.join("<br>"));
				   }
                   else 
					{
					$("#connect_status").html("<strong>"+data.error_mesg);
					}
						
  
					//drawPortlets();
					
				}
				, "json"
				);
				return false;
			}); 
		
	
  });
 

 
