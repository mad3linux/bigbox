$(document).ready(function(){
	

if (typeof(MT) == 'undefined') MT = {};

MT.propertyDialogHandle=null;
MT.propertyDialogID="property_dialog";
MT.propertyDialogDetailID="property_details";
MT.propertyDialogStatusID="property_status";
MT.currentPropertyObj=null;
MT.alertDialogID="alert_dialog";
MT.editDialogDivID="edit_dialog";
MT.editTextAreaID="obj_text_box";


MT.jobDetails = function()
{

var tableId="table_jobs";
var tablePagerId=tableId+"_pager";
var str='<table id="'+tableId+'"></table><div id="'+tablePagerId+'"></div>';

$("div#job_details_container").append(str);
//var userval=$("#form_username").val();
jQuery("#"+tableId).jqGrid({
   	url:'jobs_handler.php?mode=GET_JOBS_DATA',
	datatype: "json",
    colModel:[
   {name:'job_id',label:'Job ID', width:40,editable:true,editoptions:{readonly:true}},
   {name:'job_name',label:'Job Name',width:120,editable:true},
   {name:'active',label:'Active', width:30,editable:true,edittype:"select",editoptions:{value:"Y:Active;N:Disabled"}},
   {name:'job_type',label:'Job Type', width:110,editable:true},
   {name:'tablestore',label:'Table', width:110,editable:true},
   {name:'connection_handler',label:'Connection Handle', width:110,editable:false},
   {name:'load_type',label:'Load Type', width:90,editable:true},
   {name:'start_date',label:'Start Date', width:95,editable:true,edittype: 'text',
    editoptions: {
      dataInit: function(element) {
        $(element).datepicker({dateFormat: 'yy-mm-dd'})
      }
    }},
   {name:'end_date',label:'End Date', width:95,editable:true,edittype: 'text',
    editoptions: {
      dataInit: function(element) {
        $(element).datepicker({dateFormat: 'yy-mm-dd'})
      }
    }},
   {name:'load_status',label:'Load Status', width:75,editable:true},
   {name:'incremental_duration',label:'Incremental Duration', width:75,editable:true},
   {name:'last_run_date',label:'Last Run Date', width:130,editable:true,edittype: 'text',
    editoptions: {
      dataInit: function(element) {
        $(element).datepicker({dateFormat: 'yy-mm-dd'})
      }
    }},
   {name:'last_run_rows',label:'Last Run Rows', width:50,editable:true},
   {name:'last_run_status',label:'Last Run Status', width:50,editable:true},
   {name:'description',label:'Description', width:120,editable:true},
   {name:'creation_date',label:'Creation Date', width:90,editable:true,edittype: 'text',
    editoptions: {
      dataInit: function(element) {
        $(element).datepicker({dateFormat: 'yy-mm-dd'})
      }
    }},
   {name:'last_update_date',label:'Last Update Date', width:130,editable:true,edittype: 'text',
    editoptions: {
      dataInit: function(element) {
        $(element).datepicker({dateFormat: 'yy-mm-dd'})
      }
    }},
   {name:'username',label:'Username', width:50,editable:false},
 ],
   	rowNum:15,
   	rowList:[15,30,100],
   	pager: tablePagerId,
   	sortname: 'job_id',
    viewrecords: true,
    sortorder: "desc",
	loadonce: true,
	height:"100%",
	width:"100%",
    caption:"My Jobs",
	postData: { form_filter: function() { return $("#form_filter").val(); } },
	jsonReader: {
		root: "rows",
		page: "page",
		total: "total",
		records: "records",
		repeatitems: false,
		id: "job_id"
	},
	mtype:'POST',
	loadComplete: function() 
	{
    var iCol = MT.getColumnIndexByName($(this),'active'),
        cRows = this.rows.length, iRow, row, className;
		//var jCol=MT.getColumnIndexByName($(this),'project_active');

    for (iRow=0; iRow<cRows; iRow++) {
        row = this.rows[iRow];
        className = row.className;
        if ($.inArray('jqgrow', className.split(' ')) > 0) {
            var x = $(row.cells[iCol]).text();
			//var y = $(row.cells[jCol]).text();
			if (x=="Disabled" ) row.className = className + " ui-state-error";
			if (x=="Archived" ) row.className = className + " ui-state-error";
        }
    }
	},	
	editurl:'jobs_handler.php?mode=FORM_SAVE&type=JOB_EDIT_FS',
		edit : {
		addCaption: "Add Record",
		editCaption: "Edit Record",
		bSubmit: "Submit",
		bCancel: "Cancel",
		bClose: "Close",
		saveData: "Data has been changed! Save changes?",
		bYes : "Yes",
		bNo : "No",
		bExit : "Cancel",
		width:500

		},
		//subGrid: true,
		//subGridRowExpanded: function(subgrid_id, row_id) {MT.projectGroupMembers(subgrid_id, row_id);}


});
	jQuery("#"+tableId).jqGrid('navGrid','#'+tablePagerId,{add:true,edit:true,del:false,refresh:true,

					beforeRefresh: function() {
					   $("#"+tableId).jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
					}},
					{
					  afterSubmit: processAddEdit,
						   closeAfterAdd:true,
						   closeAfterEdit:true
					},
					{
					  afterSubmit: processAdd,
						   closeAfterAdd:true,
						   closeAfterEdit:true
					}

					);
					
/*
function processAddEdit() {
  $("#"+tableId).jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
  return[true,'']; //no error
}
*/
		function processAddEdit(response, postdata) 
		{
			$("#"+tableId).jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
        	var success = true;

        	var new_id = "1";
        	return [true,"great",1];
        }


		function processAdd(response, postdata) 
		{
			$("#"+tableId).jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
        	var success = true;

        	var new_id = "1";
        	return [true,"great",1];
        }
}

MT.jobDetails();
MT.getColumnIndexByName = function(grid, columnName) {
        var cm = grid.jqGrid('getGridParam','colModel'),i=0,l=cm.length;
        for (; i<l; i++) {
            if (cm[i].name===columnName) {
                return i; // return the index
            }
        }
        return -1;
    };



MT.rawjobDetails = function()
{

var tableId="table_rawjobs";
var tablePagerId=tableId+"_pager";
var str='<table id="'+tableId+'"></table><div id="'+tablePagerId+'"></div>';

$("div#rawjob_details_container").append(str);
//var userval=$("#form_username").val();
jQuery("#"+tableId).jqGrid({
   	url:'jobs_handler.php?mode=GET_RAWJOBS_DATA',
	datatype: "json",
    colModel:[
   {name:'id',label:'ID', width:40,editable:true,editoptions:{readonly:true}},
   {name:'job_id',label:'Job ID',width:120,editable:true,editoptions:{readonly:true}},
   {name:'load_status',label:'Load Status',width:120,editable:true,edittype:"select",editoptions:{value:"processing:Processing;pending:Pending"}},
   {name:'raw_file_name',label:'Raw File Name', width:120,editable:true},
   {name:'creation_date',label:'Creation Date', width:110,editable:false,hidden:true},
   {name:'username',label:'username', width:110,editable:true,editoptions:{readonly:true}}
 ],
   	rowNum:15,
   	rowList:[15,30,100],
   	pager: tablePagerId,
   	sortname: 'id',
    viewrecords: true,
    sortorder: "desc",
	loadonce: true,
	height:"100%",
	width:"100%",
    caption:"Raw Jobs",
	postData: { form_filter: function() { return $("#form_filter").val(); } },
	jsonReader: {
		root: "rows",
		page: "page",
		total: "total",
		records: "records",
		repeatitems: false,
		id: "id"
	},
	mtype:'POST',
	loadComplete: function() 
	{
    var iCol = MT.getColumnIndexByName($(this),'active'),
        cRows = this.rows.length, iRow, row, className;
		//var jCol=MT.getColumnIndexByName($(this),'project_active');

    for (iRow=0; iRow<cRows; iRow++) {
        row = this.rows[iRow];
        className = row.className;
        if ($.inArray('jqgrow', className.split(' ')) > 0) {
            var x = $(row.cells[iCol]).text();
			//var y = $(row.cells[jCol]).text();
			if (x=="Disabled" ) row.className = className + " ui-state-error";
			if (x=="Archived" ) row.className = className + " ui-state-error";
        }
    }
	},	
	editurl:'jobs_handler.php?mode=FORM_SAVE&type=RAWJOB_EDIT_FS',
		edit : {
		addCaption: "Add Record",
		editCaption: "Edit Record",
		bSubmit: "Submit",
		bCancel: "Cancel",
		bClose: "Close",
		saveData: "Data has been changed! Save changes?",
		bYes : "Yes",
		bNo : "No",
		bExit : "Cancel",
		width:500

		},
		//subGrid: true,
		//subGridRowExpanded: function(subgrid_id, row_id) {MT.projectGroupMembers(subgrid_id, row_id);}


});
	jQuery("#"+tableId).jqGrid('navGrid','#'+tablePagerId,{add:false,edit:true,del:true,refresh:true,

					beforeRefresh: function() {
					   $("#"+tableId).jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
					}},
					{
					  afterSubmit: processAddEdit,
						   closeAfterAdd:true,
						   closeAfterEdit:true
					},
					{
					  afterSubmit: processAdd,
						   closeAfterAdd:true,
						   closeAfterEdit:true
					},
					{
							afterSubmit: processAdd,
						   closeAfterAdd:true,
						   closeAfterEdit:true
					}
					
					);
					
/*
function processAddEdit() {
  $("#"+tableId).jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
  return[true,'']; //no error
}
*/
		function processAddEdit(response, postdata) 
		{
			$("#"+tableId).jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
        	var success = true;

        	var new_id = "1";
        	return [true,"great",1];
        }


		function processAdd(response, postdata) 
		{
			$("#"+tableId).jqGrid('setGridParam',{datatype:'json'}).trigger('reloadGrid');
        	var success = true;

        	var new_id = "1";
        	return [true,"great",1];
        }
}

MT.rawjobDetails();
});