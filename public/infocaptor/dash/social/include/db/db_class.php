<?php
//error_reporting(E_ALL);
include_once("MysqlTable.php");
include_once("Db_connection.php");
class Randomizer {
 
    public function process( $text ) {
        return preg_replace_callback('/\{(((?>[^\{\}]+)|(?R))*)\}/x', array( $this, 'replace' ), $text );
    	
	}
 
    public function replace( $text ) {
        $text = $this->process( $text[ 1 ] );
    	return $text;
	}
	public function spintax_val( $text )
	{
		
		$content = $this->process($text);
		$parts = explode('|',$content);
		$content = $parts[array_rand($parts)];
		return $content;
	}
}

$randomizer = new Randomizer( );
?>