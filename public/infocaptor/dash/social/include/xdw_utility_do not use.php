<?php

//require_once '../infocaptor_api.php';

include_once("db/MysqlTable.php");
//require_once 'xdw_config.php';

/*
//basic database functions
function connect(){



	$link = new PDO('mysql:host='. $GLOBALS['db_host'] .';dbname='. $GLOBALS['db_name'], $GLOBALS['db_user'], $GLOBALS['db_password']);

	$link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	return $link;
}
function disconnect(){
	$link = "";
	unset($link);
};
*/

function xdw_add_job($data)//$job_name,$job_type,$start_date,$end_date,$incremental_duration
{
//create unique job_id, add the record to table and return the job_id
	$link = xi_getConnection();
	$s1 = new MySqlTable('',$link);
	
	$fields='';
	$fields_values='';
	if(count($data)>0) {
		foreach($data as $ind => $value) {
			$fields .= $s1->escape($ind).',';
			$fields_values .= "'".$s1->escape($value)."',";
		}
	}
	
	$fields = substr($fields,0,-1);
	$fields_values = substr($fields_values,0,-1);

	$sql = "INSERT INTO xjob ($fields) VALUES ($fields_values)";
	$s1->executeQuery($sql);
	//return mysqli_insert_id($s1->link);
	$lastid =  $s1->link->lastInsertId();
	//disconnect();
	return $lastid;
	
}

function xdw_initial_load_list($job_type=null)
{
	$link = xi_getConnection();
 //send all the list of pending initial load list for the given job type, if no job_type provided then send all job
	$m1 = new MySqlTable('',$link);
	$sql = "SELECT * FROM xjob WHERE load_type='initial' AND load_status = 'pending'";
	if($job_type != null)
	$sql .= " AND job_type = '" . $job_type . "'";

$result = $m1->customQuery($sql);
	//disconnect();
	return $result;
}

function xdw_incremental_load_list($job_type=null)
{
 //send all the list of pending incremental load list for the given job type, if no job_type provided then send all job
	$link = xi_getConnection();//connect();
	$m1 = new MySqlTable('',$link);
	$sql = "SELECT * FROM xjob WHERE load_type='incremental' and load_status = 'pending'";
	if($job_type != null)
	$sql .= " AND job_type = '" . $job_type . "'";

	$result = $m1->customQuery($sql);
	//disconnect();
	return $result;
}

function xdw_update_job($job_id,$data)
{
	//update the job table with status and next start and end date
	$link = xi_getConnection();//connect();
	$s1 = new MySqlTable('',$link);
	
	$fields='';
	$fields_values='';
	if(count($data)>0) {
		foreach($data as $ind => $value) {
			$fields .= $s1->escape($ind)."='".$s1->escape($value)."',";
			$fields_values .= "'".$s1->escape($value)."',";
		}
	}
	
	$fields = substr($fields,0,-1);
	$fields_values = substr($fields_values,0,-1);
	
	$sql = "UPDATE xjob SET $fields WHERE job_id='".$s1->escape($job_id)."'";
	//axlog(0,$sql);
	$s1->executeQuery($sql);
	//disconnect();
}

function xdw_get_job_details($job_id)
{
//return all details for that row - requested by the processor
	$link = xi_getConnection();
	$m1 = new MySqlTable('',$link);
	$sql = "SELECT * FROM xjob WHERE job_id=" . $job_id;
	
	$result = $m1->customQuery($sql);
	//disconnect();
	return $result;
}

function xdw_add_raw_job_data($data){
	//create unique job_id, add the record to table and return the job_id
	$link = xi_getConnection();
	$s1 = new MySqlTable('',$link);
	
	$fields='';
	$fields_values='';
	if(count($data)>0) {
		foreach($data as $ind => $value) {
			$fields .= $s1->escape($ind).',';
			$fields_values .= "'".$s1->escape($value)."',";
		}
	}
	
	$fields = substr($fields,0,-1);
	$fields_values = substr($fields_values,0,-1);

	$sql = "INSERT INTO xjob_raw_data ($fields) VALUES ($fields_values)";
	$s1->executeQuery($sql);
	//return mysqli_insert_id($s1->link);
	$lastid =  $s1->link->lastInsertId();
	//disconnect();
	return $lastid;

}

function xdw_get_job_by_connection($conn){
	$link = xi_getConnection();
	$m1 = new MySqlTable('',$link);
	$sql = "SELECT * FROM xjob WHERE connection_handler=" . $conn;
	
	$result = $m1->customQuery($sql);
	//disconnect();
	return $result;
}

?>