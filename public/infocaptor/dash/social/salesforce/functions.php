<?php 

function fetch_segments($analytic){
	$all = array();
	try {
	  $segments = $analytic->management_segments->listManagementSegments();
	  if(!empty($segments['items'])){
		  foreach($segments['items'] as $key=>$value){
			$all[$value['segmentId']] = $value['name'];
		  }
		}
		return $all;
	} catch (apiServiceException $e) {
	  return 'There was an Analytics API service error '
		  . $e->getCode() . ':' . $e->getMessage();

	} catch (apiException $e) {
	  return 'There was a general API error '
		  . $e->getCode() . ':' . $e->getMessage();
	}
}

function fetch_accounts($analytic){
		$user_accounts = array();
		try{
		$accounts = $analytic->management_accounts->listManagementAccounts();
		if(!empty($accounts['items'])){
		  foreach($accounts['items'] as $key=>$value){
			$user_accounts[$value['id']] = $value['name'];
		  }
		}
		return $user_accounts;
		}
		catch (apiServiceException $e) {
		  return 'There was an Analytics API service error '
			  . $e->getCode() . ':' . $e->getMessage();

		} catch (apiException $e) {
		  return 'There was a general API error '
			  . $e->getCode() . ':' . $e->getMessage();
		}
}
function fetch_web_properties($analytic, $account=null){
	$all = array();
	if($account == ""){
		return false;
	}
	try {
	  $properties = $analytic->management_webproperties
		  ->listManagementWebproperties($account);
	  if(!empty($properties['items'])){
		  foreach($properties['items'] as $key=>$val){
			  $all[$val['id']] = $val['name'];
		  }
	  }
		
		return $all;

	} catch (apiServiceException $e) {
	  return 'There was an Analytics API service error '
		  . $e->getCode() . ':' . $e->getMessage();

	} catch (apiException $e) {
	  return 'There was a general API error '
		  . $e->getCode() . ':' . $e->getMessage();
	}
}

function fetch_user_profiles($analytic, $account, $property=null){
	$all = array();
	if($property == ""){
		return;; 
	}
	try {
	  $profiles = $analytic->management_profiles->listManagementProfiles($account,$property);
	  if(!empty($profiles['items'])){
		  foreach($profiles['items'] as $key=>$val){
			  $all[$val['id']] = $val['name'];
		  }
	  }
		
		return $all;

	} catch (apiServiceException $e) {
	  return 'There was an Analytics API service error '
		  . $e->getCode() . ':' . $e->getMessage();

	} catch (apiException $e) {
	  return 'There was a general API error '
		  . $e->getCode() . ':' . $e->getMessage();
	}
}

function  fetch_metadata(){
	
	$response = file_get_contents("https://www.googleapis.com/analytics/v3/metadata/ga/columns"); 
	return $response;
	
}

?>