<?php




function ilog($level, $msg)
{

global $gs;
    

if (!($gs["logging_enabled"]=='Y' && $gs["log_level"]>=$level)) return;
$dateStamp=date("Y_m_d", time());


$fd = fopen(dirname(__FILE__)."/log/login_social_{$_SESSION['xmember']["username"]}_".$dateStamp.".log", "a");
// append date/time to message
$str = "[" . date("Y/m/d h:i:s", time()) . "] " . $msg."     session=".session_id();
// write string
fwrite($fd, $str . "\r\n");
// close file
fclose($fd);

}

function xlog_new($level, $msg)
{
ilog($level,$msg);

}

function zlog($level,$user,$msg)
{
global $gs;
//social_log_level will decide what messages to log. if social_log_level is 5 then all $level values 5 or below will be displayed


	if ($gs["social_logging_enabled"]!='Y') return;
	
	if ( $gs["social_log_level"]<$level) return;
	//$dateStamp=date("Y_m_d_H", time()); //log files will have date and hour stamp
	$dateStamp=date("Y_m_d", time()); //log files will have date and hour stamp
	if (empty($user)) $user="nouser";
	//$level will be -1 for errors to display them in a separate file in addtion to normal log file
	$logfile=__DIR__."/log/scl_{$user}_".$dateStamp.".log";
	$fd = fopen($logfile, "a");
	$str = "[" . date("Y/m/d h:i:s", time()) . "] " . $msg;
	// write string
	fwrite($fd, $str . "\r\n");
	// close file
	fclose($fd);
	@chmod($logfile, 0666);
	if ($level<0) //write errors to a separate file in addition to above logging
	{
		$errorfile=__DIR__."/log/error_{$user}_".$dateStamp.".log";
		$fd = fopen($errorfile, "a");
			// write string
		fwrite($fd, $str . "\r\n");
		// close file
		fclose($fd);
		@chmod($errorfile, 0666);
	}
}


?>