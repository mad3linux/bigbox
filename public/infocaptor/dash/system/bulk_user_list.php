<?php

//You can use this to add bulk users for e.g as list of ldap users  that need to be added to infocaptor system
/*
Authentication is still done at LDAP so you can add a default password for each user. 
The password you set here is not used when LDAP is on. 
If you turn off LDAP (system/infocaptor_config.php file contains LDAP settings) then the user can use the
password set here.
*/

//each row represent the user record, 
//Provide username, email, first name, last name, password
//username should be unique
//email should be unique
//the password is raw text and it is encrypted when registered with infocaptor. Again, if LDAP is on then this password is just a dummy value
//if the record already exists in infocaptor it will be skipped
//your license restricts the amount of users you can add to infocaptor
//make sure the last row in the array does not have the ending comma.

$ldap_users = array(
 
array("rjsmith", "rjsmith@somecompany.com" ,"First","Last","password"),
array("user02", "user02@somecompany.com" ,"First","Last","password"),
array("user11", "user11@somecompany.com" ,"First","Last","password"),
array("user022", "user022@somecompany.com" ,"First","Last","password"),
array("user03", "user03@somecompany.com" ,"First","Last","password")

); 

?>