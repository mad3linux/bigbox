<?php



//$db_property['database type']['driver name'] 

$db_property['default']['default']= array(
								 'table_delimitter'=>'"'
								,'column_delimitter'=>'"'
								,'table_alias'=>'as'								
								,'column_alias'=>'as'
								,'aggregations'=>array("count","sum","min","max","avg")
								,'limit_clause'=>'none'
								,'date_prompt_format'=>'mm/dd/yy'  //this is the format used in date picker. decided based on database type
								,'date_fn'=>'none'
								,'date_filter_fn'=>"'#date_value#'"
								,'date_hrchy'=>'none'
									  );

$db_property['excel']['default']= array(
								 'table_delimitter'=>'"'
								,'column_delimitter'=>'"'
								,'table_alias'=>'as'								
								,'column_alias'=>'as'
								,'aggregations'=>array("count","sum","min","max","avg")
								,'limit_clause'=>'none'
								,'date_prompt_format'=>'mm/dd/yy'
								,'date_fn'=>"format(#dbcolumn#,'yyyy-mm-dd')"
								,'date_filter_fn'=>"##date_value##"
								,'date_hrchy'=>array(
													'year' => "year(#dbcolumn#)"
													,'mth_num' => "format(#dbcolumn#,'mm')"
													,'mth_name' => "format(#dbcolumn#,'mm# mmm')"
													,'mth_yr' => "format(#dbcolumn#,'mmm yyyy')"
													,'qtr' => "'Qtr-'&format(#dbcolumn#,'q')"
													,'yr_qtr' => "'Qtr-'&format(#dbcolumn#,'yyyy-q')"
													,'day_of_month' => "format(#dbcolumn#,'dd')"
													,'week' => "format(#dbcolumn#,'yyyy-ww') "
													,'day_of_year' => "datepart('y',#dbcolumn#) "
								                   )									
									  );

$db_property['access']['default']= array(
								 'table_delimitter'=>'"'
								,'column_delimitter'=>'"'
								,'table_alias'=>'as'								
								,'column_alias'=>'as'
								,'aggregations'=>array("count","sum","min","max","avg")
								,'limit_clause'=>'none'
								,'date_prompt_format'=>'mm/dd/yy'
								,'date_fn'=>"format(#dbcolumn#,'yyyy-mm-dd')"
								,'date_filter_fn'=>"##date_value##"
								,'date_hrchy'=>array(
													'year' => "year(#dbcolumn#)"
													,'mth_num' => "format(#dbcolumn#,'mm')"
													,'mth_name' => "format(#dbcolumn#,'mm# mmm')"
													,'mth_yr' => "format(#dbcolumn#,'mmm yyyy')"
													,'qtr' => "'Qtr-'&format(#dbcolumn#,'q')"
													,'yr_qtr' => "'Qtr-'&format(#dbcolumn#,'yyyy-q')"
													,'day_of_month' => "format(#dbcolumn#,'dd')"
													,'week' => "format(#dbcolumn#,'yyyy-ww') "
													,'day_of_year' => "datepart('y',#dbcolumn#) "
								                   )							
									  );

$db_property['mysql']['default']= array(
								 'table_delimitter'=>'`'
								,'column_delimitter'=>'`'
								,'table_alias'=>'as'								
								,'column_alias'=>'as'
								,'aggregations'=>array("count","sum","min","max","avg")
								,'limit_clause'=>'limit'
								,'date_prompt_format'=>'mm/dd/yy'
								,'date_fn'=>"DATE_FORMAT(#dbcolumn#,'%Y-%m-%d')"
								,'date_filter_fn'=>"STR_TO_DATE('#date_value#','%m/%d/%Y')"
								,'date_hrchy'=>array(
													'year' => "DATE_FORMAT(#dbcolumn#,'%Y')"
													,'mth_num' => "DATE_FORMAT(#dbcolumn#,'%m')"
													,'month_yr' => "DATE_FORMAT(#dbcolumn#,'%b %Y')"
													,'mth_name' => "concat('[',date_format(#dbcolumn#,'%m'),'] ',date_format(#dbcolumn#,'%b'))"
													,'mth_name_yr' => "concat('[',date_format(#dbcolumn#,'%m'),'] ',date_format(#dbcolumn#,'%b %y'))"
													,'qtr' => "concat('Qtr-',quarter(#dbcolumn#))"
													,'yr_qtr' => "concat(year(#dbcolumn#),'- Qtr ',quarter(#dbcolumn#))"
													,'day_of_month' => "DATE_FORMAT(#dbcolumn#,'%d')"
													,'day_of_week' => "concat('[',date_format(#dbcolumn#,'%w'),'] ',date_format(#dbcolumn#,'%a'))"
													,'day_of_year' => "DATE_FORMAT(#dbcolumn#,'%j')"
								                   )
								
									  );				

$db_property['sqlite']['default']= array(
								 'table_delimitter'=>''
								,'column_delimitter'=>'"'
								,'table_alias'=>'as'								
								,'column_alias'=>'as'
								,'aggregations'=>array("count","sum","min","max","avg")
								,'limit_clause'=>'limit'
								,'date_prompt_format'=>'yy-mm-dd'
								,'date_fn'=>"strftime('%Y-%m-%d')"
								,'date_filter_fn'=>"'#date_value#'"
								,'date_hrchy'=>array(
													'year' => "strftime('%Y',#dbcolumn#)"
													,'mth_num' => "strftime('%m',#dbcolumn#)"
													,'mth_name' => "case strftime('%m', #dbcolumn#) when '01' then '[01] Jan' when '02' then '[02] Feb' when '03' then '[03] Mar' when '04' then '[04] Apr' when '05' then '[05] May' when '06' then '[06] Jun' when '07' then '[07] Jul' when '08' then '[08] Aug' when '09' then '[09] Sep' when '10' then '[10] Oct' when '11' then '[11] Nov' when '12' then '[12] Dec' else '' end"
													,'mth_yr' => "case strftime('%m', #dbcolumn#) when '01' then 'Jan' when '02' then 'Feb' when '03' then 'Mar' when '04' then 'Apr' when '05' then 'May' when '06' then 'Jun' when '07' then 'Jul' when '08' then 'Aug' when '09' then 'Sep' when '10' then 'Oct' when '11' then 'Nov' when '12' then 'Dec' else '' end ||strftime(' %Y',#dbcolumn#) "
													,'qtr' => "'Qtr - '||( (cast(strftime('%m', SurveyDate) as integer) + 2) / 3)"
													,'yr_qtr' => "strftime('%Y',#dbcolumn#)||'- Qtr '||( (cast(strftime('%m', SurveyDate) as integer) + 2) / 3)"
													,'day_of_month' => "strftime('%d',#dbcolumn#)"
													
													,' day_of_week' => "strftime('%w',#dbcolumn#)"
													,' day_of_yr' => "strftime('%j',#dbcolumn#)"
													
													,' week' => "strftime('%W',#dbcolumn#)"
								                   )
								
									  );									  
									  
$db_property['jdbc']['com.mysql.jdbc.Driver']= 
							array(
								 'table_delimitter'=>'`'
								,'column_delimitter'=>'`'
								,'table_alias'=>'as'								
								,'column_alias'=>'as'
								,'aggregations'=>array("count","sum","min","max","avg")
								,'limit_clause'=>'limit'
								,'date_prompt_format'=>'mm/dd/yy'
								,'date_fn'=>"DATE_FORMAT(#dbcolumn#,'%Y-%m-%d')"
								,'date_filter_fn'=>"STR_TO_DATE('#date_value#','%m/%d/%Y')"
								,'date_hrchy'=>array(
													'year' => "DATE_FORMAT(#dbcolumn#,'%Y')"
													,'mth_num' => "DATE_FORMAT(#dbcolumn#,'%m')"
													,'mth_name' => "concat('[',date_format(#dbcolumn#,'%m'),'] ',date_format(#dbcolumn#,'%b'))"
													,'qtr' => "concat('Qtr-',quarter(#dbcolumn#))"
													,'yr_qtr' => "concat(year(#dbcolumn#),'- Qtr ',quarter(#dbcolumn#))"
													,'day_of_month' => "DATE_FORMAT(#dbcolumn#,'%d')"
													,'day_of_week' => "concat('[',date_format(#dbcolumn#,'%w'),'] ',date_format(#dbcolumn#,'%a'))"
													,'day_of_year' => "DATE_FORMAT(#dbcolumn#,'%j')"
								                   )								
									  );									  


$db_property['jdbc']['oracle.jdbc.OracleDriver']= 
							array(
								 'table_delimitter'=>'"'
								,'column_delimitter'=>'"'
								,'table_alias'=>' '								
								,'column_alias'=>'as'
								,'aggregations'=>array("count","sum","min","max","avg")
								,'limit_clause'=>'none'
								,'date_prompt_format'=>'mm/dd/yy'
								,'date_fn'=>"to_char(#dbcolumn#,'YYYY-MM-DD')"
								,'date_filter_fn'=>"TO_DATE('#date_value#','mm/dd/YYYY')"
								,'date_hrchy'=>array(
													'year' => "to_char(#dbcolumn#,'YYYY')"
													,'mth_num' => "to_char(#dbcolumn#,'MM')"
													,'mth_name' => "to_char(#dbcolumn#,'[mm] Mon')"
													,'qtr' => "'Qtr '||to_char(#dbcolumn#,'Q')"
													,'yr_qtr' => "to_char(#dbcolumn#,'YYYY-Q')"
													,'day_of_month' => "to_char(#dbcolumn#,'DD')"
													,'week' => "to_char(#dbcolumn#,'YYYY-WW')"
													,'day_of_year' => "to_char(#dbcolumn#,'DDD')"
								                   )								
									  );

$db_property['jdbc']['net.sourceforge.jtds.jdbc.Driver']= 
							array(
								 'table_delimitter'=>'"'
								,'column_delimitter'=>'"'
								,'table_alias'=>'as'								
								,'column_alias'=>'as'
								,'aggregations'=>array("count","sum","min","max","avg")
								,'limit_clause'=>'none'
								,'date_prompt_format'=>'mm/dd/yy'
								,'date_fn'=>"CONVERT(char(10), #dbcolumn#,126)"
								,'date_filter_fn'=>"Convert(varchar(30),'#date_value#',102)"
								,'date_hrchy'=>array(
													'year' => "datename(yyyy,#dbcolumn#)"
													,'mth_num' => "datepart(mm,#dbcolumn#)"
													,'mth_name' => "'['+right('00'+cast(datepart(mm,#dbcolumn#)  as varchar),2)+'] '+left(datename(month,#dbcolumn#),3) "
													,'qtr' => "'Qtr - '+datename(yy,#dbcolumn#) + '-' + datename(q,#dbcolumn#)"
													,'yr_qtr' => "datename(yy,#dbcolumn#) + '-' + datename(q,#dbcolumn#)"
													,'day_of_month' => "datename(d,#dbcolumn#)"
													,'week' => "datename(ww,#dbcolumn#)"
													,'day_of_year' => "datename(dayofyear,#dbcolumn#)"
								                   )								
									  );

$db_property['jdbc']['org.postgresql.Driver']= 
							array(
								 'table_delimitter'=>'"'
								,'column_delimitter'=>'"'
								,'table_alias'=>'as'								
								,'column_alias'=>'as'
								,'aggregations'=>array("count","sum","min","max","avg")
								,'limit_clause'=>'none'
								,'date_prompt_format'=>'mm/dd/yy'
								,'date_fn'=>"to_char(#dbcolumn#,'YYYY-MM-DD')"
								,'date_filter_fn'=>"TO_DATE('#date_value#','mm/dd/YYYY')"
								,'date_hrchy'=>array(
													'year' => "to_char(#dbcolumn#,'YYYY')"
													,'mth_num' => "to_char(#dbcolumn#,'MM')"
													,'mth_name' => "to_char(#dbcolumn#,'[mm] Mon')"
													,'qtr' => "'Qtr '||to_char(#dbcolumn#,'Q')"
													,'yr_qtr' => "to_char(#dbcolumn#,'YYYY-Q')"
													,'day_of_month' => "to_char(#dbcolumn#,'DD')"
													,'week' => "to_char(#dbcolumn#,'YYYY-WW')"
													,'day_of_year' => "to_char(#dbcolumn#,'DDD')"
								                   )
									  );

$db_property['jdbc']['com.cloudera.hive.jdbc4.HS2Driver']= 
							array(
								 'table_delimitter'=>'`'
								,'column_delimitter'=>'`'
								,'table_alias'=>'as'								
								,'column_alias'=>'as'
								,'aggregations'=>array("count","sum","min","max","avg")
								,'limit_clause'=>'limit'
								,'date_prompt_format'=>'mm/dd/yy'
								,'date_fn'=>"none"
								,'date_filter_fn'=>"'#date_value#'"
								,'date_hrchy'=>"none"								
									  );	
$db_property['jdbc']['com.cloudera.impala.jdbc4.Driver']= 
							array(
								 'table_delimitter'=>'"'
								,'column_delimitter'=>'"'
								,'table_alias'=>'as'								
								,'column_alias'=>'as'
								,'aggregations'=>array("count","sum","min","max","avg")
								,'limit_clause'=>'limit'
								,'date_prompt_format'=>'mm/dd/yy'
								,'date_fn'=>"none"
								,'date_filter_fn'=>"'#date_value#'"
								,'date_hrchy'=>"none"								
									  );										  
									  
$db_property['jdbc']['com.pervasive.jdbc.v2.Driver']= 
							array(
								 'table_delimitter'=>'"'
								,'column_delimitter'=>'"'
								,'table_alias'=>'as'								
								,'column_alias'=>'as'
								,'aggregations'=>array("count","sum","min","max","avg")
								,'limit_clause'=>'none'
								,'date_prompt_format'=>'mm/dd/yy'
								,'date_fn'=> 'none'
								,'date_filter_fn'=>"'#date_value#'"
								,'date_hrchy'=>'none'
									  );

$db_property['bigodbc']['default']= array(
								 'table_delimitter'=>'"'
								,'column_delimitter'=>'"'
								,'table_alias'=>'as'								
								,'column_alias'=>'as'
								,'aggregations'=>array("count","sum","min","max","avg")
								,'limit_clause'=>'limit'
								,'date_prompt_format'=>'yy-mm-dd'
								,'date_fn'=>"none"
								,'date_filter_fn'=>"none"
								,'date_hrchy'=>'none'
								
									  );									  

$db_property['bigodbc']['impala']= array(
								 'table_delimitter'=>'`'
								,'column_delimitter'=>'`'
								,'table_alias'=>'as'								
								,'column_alias'=>'as'
								,'aggregations'=>array("count","sum","min","max","avg")
								,'limit_clause'=>'limit'
								,'date_prompt_format'=>'yy-mm-dd'
								,'date_fn'=>"to_date(#dbcolumn#)"
								,'date_filter_fn'=>"cast('#date_value#' as timestamp)"
								,'date_hrchy'=>array(
													'year' => "year(#dbcolumn#)"
													,'mth_num' => "month(#dbcolumn#)"
													//,'mth_name' => "concat('[',date_format(#dbcolumn#,'%m'),'] ',date_format(#dbcolumn#,'%b'))"
													,'qtr' => "round(month(#dbcolumn#)/3 + 0.3 ,0)"
													//,'yr_qtr' => "concat(year(#dbcolumn#),'- Qtr ',quarter(#dbcolumn#))"
													,'day_of_month' => "dayofmonth(#dbcolumn#)"
													,'day_of_week' => "dayofweek(#dbcolumn#)"
													,'day_of_year' => "dayofyear(#dbcolumn#)"
								                   )
								
									  );

$db_property['bigodbc']['hive']= array(
								 'table_delimitter'=>'`'
								,'column_delimitter'=>'`'
								,'table_alias'=>'as'								
								,'column_alias'=>'as'
								,'aggregations'=>array("count","sum","min","max","avg")
								,'limit_clause'=>'limit'
								,'date_prompt_format'=>'yy-mm-dd'
								,'date_fn'=>"to_date(#dbcolumn#)"
								,'date_filter_fn'=>"cast('#date_value#' as timestamp)"
								,'date_hrchy'=>array(
													'year' => "year(#dbcolumn#)"
													,'mth_num' => "month(#dbcolumn#)"
													//,'mth_name' => "concat('[',date_format(#dbcolumn#,'%m'),'] ',date_format(#dbcolumn#,'%b'))"
													,'qtr' => "round(month(#dbcolumn#)/3 + 0.3 ,0)"
													//,'yr_qtr' => "concat(year(#dbcolumn#),'- Qtr ',quarter(#dbcolumn#))"
													,'day_of_month' => "dayofmonth(#dbcolumn#)"
													,'day_of_week' => "dayofweek(#dbcolumn#)"
													,'day_of_year' => "dayofyear(#dbcolumn#)"
								                   )
								
									  );									  
?>