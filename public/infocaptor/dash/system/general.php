<?php
//Last modified by: admin

$gs['logging_enabled']='Y';
$gs['log_level']=5;
$gs['log_error']='Y';
$gs['social_logging_enabled']='Y';
$gs['user_logging_enabled']='N';
$gs['logging_username']='none';
$gs['social_log_level']=-1;
$gs['font_size']=20;
$gs['snap_grid_size']=20;
$gs['css_theme']='jqueryui/css/redmond/jquery-ui.css';
$gs['ga_tracking_id']='212620-23';
$gs['registration']='INSTANT';
$gs['registration_from_email']='support@yourdomain.com';
$gs['registration_from_name']='Admin';
$gs['system_from_email']='contact@';
$gs['top_button_message']='Manage Projects';
$gs['top_button_link']='dashboard.php';
$gs['share_connections']='shareallx';
$gs['export_width_correction']=0;
$gs['export_height_correction']=0;

?>