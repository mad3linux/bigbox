<?php
//Last modified by: admin

$icprop["common_area_name"]="Important Stuff";
/*
$icprop["dash_code"] = array("http://infocaptor.com/online_pricing.php","http://infocaptor.com/tos.php","http://infocaptor.com/privacy_policy.php");
$icprop["dash_name"] = array("Subscription Pricing (Jan 2013)","Terms of Service","Privacy Policy");
$icprop["dash_img"] = array("icons/64/emoticon-money-mouth.png","icons/64/book-open.png","icons/64/infomation.png");
*/
//add free flow html text for any other informational boxes
$icprop["html_box_name"]=array("Policies","Tutorial");
$icprop["html_box_text"]=array(
"<ul style='font-size:14px;'><li><a href='http://infocaptor.com/privacy_policy.php'>Privacy Policy</a><li><a href='http://infocaptor.com/tos.php'>Terms of service </a></ul>",
"<ul><li><a href='http://www.infocaptor.com/dashboard/hr-dashboard-build-excel-based-human-resource-dashboard-with-filters-parameters-and-drill-down'>Excel Dashboard with Parameters (video)</a><li><a href='http://www.infocaptor.com/help/useful_tutorials.htm?mw=MzAw&st=MQ==&sct=MA==&ms=AAAA'>How to - Tutorials</a></ul>"
);

$icprop["show_google_signon"]="Y";

$icprop["query_timeout"]=15;

$icprop["front_message"]="";

$icprop["ldap"]=array(
		"enable" => "N",
		"server" => "rudrasoft_nj",
		"port" => 389,
		"basedn" => "dc=maxcrc,dc=com",
		"protocol_version" => 3
		);


$icprop["baseDir"]=dirname($_SERVER["SCRIPT_FILENAME"]);
$icprop["phantomjs"]=dirname($_SERVER["SCRIPT_FILENAME"])."/system/phantomjs/phantomjs";

function getICBaseURL()
{
$httpStr="http";
$port="";
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') $httpStr="https";
if (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT']!="80") $port=":".$_SERVER['SERVER_PORT'];

return "{$httpStr}://{$_SERVER['SERVER_NAME']}{$port}{$_SERVER['REQUEST_URI']}";
}

$icprop["baseURL"]=dirname(getICBaseURL());

function setupPython()
{
  xlog(0,"inside setupPython");
	$base= dirname($_SERVER["SCRIPT_FILENAME"]);
	$baseP=explode("htdocs",$base);
	
	$cgi_path=$baseP[0]."cgi-bin/";
	$python_path=$baseP[0]."cgi-bin/python_64bit_2794/wp/python";
	xlog(0,"cgi={$cgi_path}  , python = {$python_path}");
	$file_list=array("aggregate.py","tablelist.py","allodbc.py");
	for ($fi=0; $fi < count($file_list); $fi++)
	{
		addPythonPath($cgi_path.$file_list[$fi],$python_path);
	}	
}

function addPythonPath($file,$path)
{
  try
  {
	// read into array
	$arr = file($file);

	// edit first line
	$arr[0] = "#!".$path."\n";

	// write back to file
	file_put_contents($file, implode($arr));
  }
   catch (Exception $e) 
   {
     xlog(0,"Error... cannot setup python path, please perform manually");
	 xlog(0,"file = {$file} , path = {$path}");
   }  
}
		
?>