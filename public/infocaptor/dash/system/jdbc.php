<?php

$oracle="http://{$_SERVER['SERVER_NAME']}:8081/examples/oracle_jdbc.jsp";

$mssql="http://{$_SERVER['SERVER_NAME']}:8081/examples/mssql_jdbc.jsp";

$odbc="http://{$_SERVER['SERVER_NAME']}:8081/examples/odbc_jdbc.jsp";

$jdbc="http://{$_SERVER['SERVER_NAME']}:8081/examples/all_jdbc.jsp";

$mssql_tables="http://{$_SERVER['SERVER_NAME']}:8081/examples/mssql_list.jsp";

$oracle_tables="http://{$_SERVER['SERVER_NAME']}:8081/examples/oracle_list.jsp";

$odbc_tables="http://{$_SERVER['SERVER_NAME']}:8081/examples/odbc_jdbc_list.jsp";

$jdbc_tables="http://{$_SERVER['SERVER_NAME']}:8081/examples/all_jdbc_list.jsp";

$httpStr="http";
$port="";
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') $httpStr="https";
if (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT']!="80") $port=":".$_SERVER['SERVER_PORT'];
$base_server="{$httpStr}://{$_SERVER['SERVER_NAME']}{$port}";

$python_odbc_result="{$base_server}/cgi-bin/allodbc.py";
$python_odbc_tables="{$base_server}/cgi-bin/tablelist.py";

/*
$python_odbc_result="http://{$_SERVER['SERVER_NAME']}/cgi-bin/allodbc.py";
$python_odbc_tables="http://{$_SERVER['SERVER_NAME']}/cgi-bin/tablelist.py";
*/
/* JDBC notes 
Pervasive : download three jar files from pervasive jdbc driver package jpscs.jar, pvjdbc2.jar, pvjdbc2x.jar
jdbc driver: com.pervasive.jdbc.v2.Driver, jdbc url= jdbc:pervasive://localhost:1583/DEMODATA


*/

?>