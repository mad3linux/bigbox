<?php
error_reporting (0);
function validate_ldap_user($user,$password,$server,$port,$basedn,$protocol_version)
{

  try
  {
        //$user = "admin";
		//$password="admin123";
        $filter = "(|(uid=" . $user . ")" . "(mail=" . $user ."))";
		//$server = "rudra_xyz";
		//$port = 389;
		//$basedn="dc=infocaptor,dc=com";
		

        // Connect to the LDAP server.
        $ldapconn = ldap_connect($server, $port) ;
        if (!$ldapconn)
		{
			$_SESSION['current_message']="Cannot connect to LDAP {$server} - port {$port}";
		   ldaplog(0,$_SESSION['current_message']);
		   return false;
		}
		
		ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, $protocol_version);
		
        // Bind anonymously to the LDAP server to search and retrieve DN.
        $ldapbind = ldap_bind($ldapconn);
		if (!$ldapbind)
		{
			$_SESSION['current_message']="Cannot BIND anonymously to LDAP {$server} - port {$port}";
		   ldaplog(0,$_SESSION['current_message']);
		   return false;
		
		}
		
        $result = ldap_search($ldapconn,$basedn,$filter);// or die ("Search error.");
		if (!$result)
		{
			$_SESSION['current_message']="Cannot search LDAP";
		   ldaplog(0,$_SESSION['current_message']);
		   return false;
		
		}
				
        $entries = ldap_get_entries($ldapconn, $result);
		//print_r($entries);
        $binddn = $entries[0]["dn"];

        // Bind again using the DN retrieved. If this bind is successful,
        // then the user has managed to authenticate.
        $ldapbind = ldap_bind($ldapconn, $binddn, $password);
		ldap_close($ldapconn);
		
        if ($ldapbind) 
		{
				return $entries[0]['uid'][0];
        } else 
		{
			$_SESSION['current_message']="cannot authenticate";
		   ldaplog(0,$_SESSION['current_message']);
		   return false;
            
        }

   }
    catch (Exception $err)
		{
		  return false;
		}

}

function ldaplog($level, $msg)
{
    
$dateStamp=date("Y_m_d", time());
$fd = fopen("log/ldap_".session_id().$dateStamp.".log", "a");
// append date/time to message
$str = "[" . date("Y/m/d h:i:s", time()) . "] " . $msg;
// write string
fwrite($fd, $str . "\n");
// close file
fclose($fd);
}

?>