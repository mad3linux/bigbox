<?php

error_reporting(E_ALL & ~E_DEPRECATED & ~E_NOTICE);

define("DEEP_LEVEL", 9); //debug level for logging purpose

//$property['name']= property_label, edit form field type, editable/hidden, min_role_level,default_value, array of selectable lov
//e.g
//logging_enabled, Turn on logging?, TEXT/SELECT/radio etc

$property['general']['form']= array(
								 'label'=>"General Settings"
								,'description'=>"some desc"
								,'save_directory'=>"system/"								
								,'width'=>400
								,'height'=>400
								,'modal'=>"n"
								,'min_role_level'=>"ADMIN"
								,'action' =>"sayHi"
									  );
									  

									  
$property['general']['logging_enabled']= array(
                                   'label'=>"Turn Loggin On?"
								  ,'description'=>"Select Yes for troubleshooting purpose. The logs are created for each session and stored in the log directory"
	                              ,'field_control'=>"SELECT" 
								  ,'field_type'=>"CHAR"
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"N"
								  ,'lov'=> array('Yes:Y','No:N')
							       );

$property['general']['log_level']= array(
                                   'label'=>"Log Level"
								  ,'description'=>"A value of 0 will dump detail information in the log file. A higher value such as 5 will just dump errors in the log file" 
	                              ,'field_control'=>"SELECT" 
								  ,'field_type'=>"NUMBER" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>5
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );

$property['general']['log_error']= array(
                                   'label'=>"Log Error"
								  ,'description'=>"Yes will dump all error details" 
	                              ,'field_control'=>"SELECT" 
								  ,'field_type'=>"NUMBER" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>'Y'
								  ,'lov'=> array('Yes:Y','No:N')
							       );								   

$property['general']['social_logging_enabled']= array(
                                   'label'=>"Social logging Enabled?"
								  ,'description'=>"Select Yes for troubleshooting purpose. The logs are created for each session and stored in the log directory"
	                              ,'field_control'=>"SELECT" 
								  ,'field_type'=>"CHAR"
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"Y"
								  ,'lov'=> array('Yes:Y','No:N')
							       );								   
$property['general']['user_logging_enabled']= array(
                                   'label'=>"Specific User loggin?"
								  ,'description'=>"Select Yes for troubleshooting purpose. The logs are created for each session and stored in the log directory"
	                              ,'field_control'=>"SELECT" 
								  ,'field_type'=>"CHAR"
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"N"
								  ,'lov'=> array('Yes:Y','No:N')
							       );

$property['general']['logging_username']= array(
                                   'label'=>"Username for Logging"
								  ,'description'=>"Pick your font" 
	                              ,'field_control'=>"text" 
								  ,'field_type'=>"CHAR" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"none"
								  ,'lov'=> array(
													'10'

											  )
							       );
								   
$property['general']['social_log_level']= array(
                                   'label'=>"Social Log Level"
								  ,'description'=>"A value of 0 will dump detail information in the log file. A higher value such as 5 will just dump errors in the log file" 
	                              ,'field_control'=>"SELECT" 
								  ,'field_type'=>"NUMBER" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>5
								  ,'lov'=> array('Error:-1','Summary:0','Basic:1','Detail:9')
							       );

								   
$property['general']['font_size']= array(
                                   'label'=>"Default Font Size"
								  ,'description'=>"Pick your font" 
	                              ,'field_control'=>"text" 
								  ,'field_type'=>"CHAR" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"10"
								  ,'lov'=> array(
													'10'

											  )
							       );								   

$property['general']['snap_grid_size']= array(
                                   'label'=>"Grid/Step size"
								  ,'description'=>"Snap Tolerance" 
	                              ,'field_control'=>"text" 
								  ,'field_type'=>"CHAR" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"30"
								  ,'lov'=> array(
													'10'

											  )
							       );									   
								   
$property['general']['css_theme']= array(
                                   'label'=>"Color Theme"
								  ,'description'=>"Layout and Color Theme. Themes are installed in the jqueryui/css subdirectory" 
	                              ,'field_control'=>"SELECT" 
								  ,'field_type'=>"CHAR" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"redmond:jqueryui/css/redmond/jquery-ui.css"
								  ,'lov'=> array(
												"ui-darkness:jqueryui/css/ui-darkness/jquery-ui-1.8.6.custom.css"
											   ,'redmond:jqueryui/css/redmond/jquery-ui.css'
											   , 'blue:jqueryui/css/blue/jquery-ui-1.9.2.custom.css'
											   , 'sunny:jqueryui/css/sunny/jquery-ui-1.8.8.custom.css'
											   , 'flick:jqueryui/css/flick/jquery-ui.css'

											  )
							       );
/*								   
$property['general']['default_color_option']= array(
                                   'label'=>"Default Draw Color"
								  ,'description'=>"How will users gain access to the system upon registration" 
	                              ,'field_control'=>"SELECT" 
								  ,'field_type'=>"NUMBER" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"CUSTOM"
								  ,'lov'=> array('Use Theme Color Default:THEME','Use Custom Color as below:CUSTOM')
							       );	


								   
$property['general']['draw_color']= array(
                                   'label'=>"Custom Draw Color"
								  ,'description'=>"A value of 0 will dump detail information in the log file. A higher value such as 5 will just dump errors in the log file" 
	                              ,'field_control'=>"color" 
								  ,'field_type'=>"char" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"110947"
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );									   
*/
$property['general']['ga_tracking_id']= array(
                                   'label'=>"Google Analytics ID"
								  ,'description'=>"A google account number" 
	                              ,'field_control'=>"text" 
								  ,'field_type'=>"char" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"UA-212620-20"
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );									   
								   
$property['general']['User Registration']= array(
                                   'label'=>"User Registration"
								  ,'description'=>"How users register into the system" 
	                              ,'field_control'=>"PARENT" 
								  ,'field_type'=>"NUMBER" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>5
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );
								   
$property['general']['registration']= array(
                                   'label'=>"Registration Confirmation"
								  ,'description'=>"How will users gain access to the system upon registration" 
	                              ,'field_control'=>"SELECT" 
								  ,'field_type'=>"NUMBER" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>5
								  ,'lov'=> array('Immediate upon registration:INSTANT','Send Activation Link:EMAIL')
							       );

$property['general']['registration_from_email']= array(
                                   'label'=>"From Email Confirmation"
								  ,'description'=>"What is the email users will receive notification from?" 
	                              ,'field_control'=>"text" 
								  ,'field_type'=>"CHAR" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"support@yourdomain.com"
								  ,'lov'=> array('Immediate upon registration:INSTANT','Send Activation Link:EMAIL')
							       );

$property['general']['registration_from_name']= array(
                                   'label'=>"From Name Confirmation"
								  ,'description'=>"What is the name users will receive notification from?" 
	                              ,'field_control'=>"text" 
								  ,'field_type'=>"CHAR" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"Admin"
								  ,'lov'=> array('Immediate upon registration:INSTANT','Send Activation Link:EMAIL')
							       );

$property['general']['system_from_email']= array(
                                   'label'=>"System Email Address"
								  ,'description'=>"What is the email users will receive notification from?" 
	                              ,'field_control'=>"text" 
								  ,'field_type'=>"CHAR" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"contact@infocaptor.com"
								  ,'lov'=> array('Immediate upon registration:INSTANT','Send Activation Link:EMAIL')
							       );
								   
$property['general']['top_button_message']= array(
                                   'label'=>"Top Button Message"
								  ,'description'=>"" 
	                              ,'field_control'=>"text" 
								  ,'field_type'=>"CHAR" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"Manage Projects"
								  ,'lov'=> array('Immediate upon registration:INSTANT','Send Activation Link:EMAIL')
							       );
								   
								   
$property['general']['top_button_link']= array(
                                   'label'=>"Top Button Link"
								  ,'description'=>"" 
	                              ,'field_control'=>"text" 
								  ,'field_type'=>"CHAR" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"dashboard.php"
								  ,'lov'=> array('Immediate upon registration:INSTANT','Send Activation Link:EMAIL')
							       );


$property['general']['share_connections']= array(
'label'=>"Share Connections"
,'description'=>"" 
,'field_control'=>"text" 
,'field_type'=>"CHAR" 
,'field_status'=>"EDIT"
,'min_role_level'=>"ADMIN"
,'default_value'=>"ignore"
,'lov'=> array('Immediate upon registration:INSTANT','Send Activation Link:EMAIL')
);


$property['general']['export_width_correction']= array(
'label'=>"Export Width Correction"
,'description'=>"" 
,'field_control'=>"text" 
,'field_type'=>"NUMBER" 
,'field_status'=>"EDIT"
,'min_role_level'=>"ADMIN"
,'default_value'=>0
,'lov'=> array('Immediate upon registration:INSTANT','Send Activation Link:EMAIL')
);


$property['general']['export_height_correction']= array(
'label'=>"Export Height Correction"
,'description'=>"" 
,'field_control'=>"text" 
,'field_type'=>"NUMBER" 
,'field_status'=>"EDIT"
,'min_role_level'=>"ADMIN"
,'default_value'=>0
,'lov'=> array('Immediate upon registration:INSTANT','Send Activation Link:EMAIL')
);


$property['general']['share_connections']= array(
'label'=>"Share Connections"
,'description'=>"" 
,'field_control'=>"text" 
,'field_type'=>"CHAR" 
,'field_status'=>"EDIT"
,'min_role_level'=>"ADMIN"
,'default_value'=>"ignore"
,'lov'=> array('Immediate upon registration:INSTANT','Send Activation Link:EMAIL')
);

//'shareall'

$property['personal']['form']= array(
								 'label'=>"Personal Settings"
								,'description'=>"some desc"
								,'save_directory'=>"data/"
								,'width'=>-1
								,'height'=>350
								,'modal'=>"n"
								,'min_role_level'=>"ADMIN"
								
									  );
/*									  
$property['personal']['personal_css_theme']= array(
                                   'label'=>"Color Theme"
								  ,'description'=>"Layout and Color Theme. Themes are installed in the jqueryui/css subdirectory" 
	                              ,'field_control'=>"SELECT" 
								  ,'field_type'=>"CHAR" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"jqueryui/css/blue/jquery-ui-1.8.16.custom.css"
								  ,'lov'=> array(
								                
											   'redmond:jqueryui/css/redmond/jquery-ui-1.8.8.custom.css'
											   , 'blue:jqueryui/css/blue/jquery-ui-1.9.2.custom.css'
											   , 'ui-lightness:jqueryui/css/ui-lightness/jquery-ui-1.8.6.custom.css'
											   , 'sunny:jqueryui/css/sunny/jquery-ui-1.8.8.custom.css'
											  ,'le-frog:jqueryui/css/le-frog/jquery-ui-1.8.6.custom.css' 
											  , 'ui-lightness:jqueryui/css/ui-lightness/jquery-ui-1.8.6.custom.css'
											   , 'ui-darkness:jqueryui/css/ui-darkness/jquery-ui-1.8.6.custom.css' 
											   , 'blitzer:jqueryui/css/blitzer/jquery-ui-1.8.6.custom.css' 
											   , 'sunny:jqueryui/css/sunny/jquery-ui-1.8.8.custom.css'
											   , 'dot-luv:jqueryui/css/dot-luv/jquery-ui-1.8.8.custom.css'
											   , 'purple:jqueryui/css/pink/jquery-ui-1.8.9.custom.css'
											   , 'blue:jqueryui/css/blue/jquery-ui-1.9.2.custom.css'
											  )
							       );
*/
$property['personal']['font_name']= array(
                                   'label'=>"Font"
								  ,'description'=>"Pick your font" 
	                              ,'field_control'=>"SELECT" 
								  ,'field_type'=>"CHAR" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"N"
								  ,'lov'=> array(
													'Arial:Arial'
												,	'Verdana:Verdana'
												, 	'Comic Sans MS:Comic Sans MS'	
												,   'Helvetica:Helvetica'
												,   'Cooper Black:Cooper Black'
											  )
							       );

$property['personal']['font_size']= array(
                                   'label'=>"Default Font Size"
								  ,'description'=>"Pick your font" 
	                              ,'field_control'=>"text" 
								  ,'field_type'=>"CHAR" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"10"
								  ,'lov'=> array(
													'10'

											  )
							       );
/*								   
$property['personal']['mockfactor']= array(
                                   'label'=>"Draw Style"
								  ,'description'=>"How will users gain access to the system upon registration" 
	                              ,'field_control'=>"SELECT" 
								  ,'field_type'=>"NUMBER" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>0
								  ,'lov'=> array('Normal - Straight lines:0','Sketch Like:1')
							       );		

$property['personal']['default_color_option']= array(
                                   'label'=>"Default Draw Color"
								  ,'description'=>"How will users gain access to the system upon registration" 
	                              ,'field_control'=>"SELECT" 
								  ,'field_type'=>"NUMBER" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"CUSTOM"
								  ,'lov'=> array('Use Theme Color Default:THEME','Use Custom Color as below:CUSTOM')
							       );	
								   
$property['personal']['draw_color']= array(
                                   'label'=>"Custom Draw Color"
								  ,'description'=>"A value of 0 will dump detail information in the log file. A higher value such as 5 will just dump errors in the log file" 
	                              ,'field_control'=>"color" 
								  ,'field_type'=>"char" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"110947"
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );
*/								   

$property['personal']['Snapping']= array(
                                   'label'=>"Snapping"
								  ,'description'=>"Define snapping behavior" 
	                              ,'field_control'=>"PARENT" 
								  ,'field_type'=>"NUMBER" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>5
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );
/*
$property['personal']['show_grid']= array(
                                   'label'=>"Show Grid"
								  ,'description'=>"Show Grid" 
	                              ,'field_control'=>"SELECT" 
								  ,'field_type'=>"NUMBER" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>0
								  ,'lov'=> array('Yes:Y','No:N')
							       );									   

$property['personal']['grid_cell_width']= array(
                                   'label'=>"Grid Cell Width"
								  ,'description'=>"Snap Tolerance" 
	                              ,'field_control'=>"text" 
								  ,'field_type'=>"CHAR" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"20"
								  ,'lov'=> array(
													'10'

											  )
							       );															   
*/								   
								   
								   
$property['personal']['snap_grid']= array(
                                   'label'=>"Snap to Grid/Steps"
								  ,'description'=>"Snap Objects" 
	                              ,'field_control'=>"SELECT" 
								  ,'field_type'=>"NUMBER" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"Y"
								  ,'lov'=> array('Yes:Y','No:N')
							       );									   

$property['personal']['snap_grid_size']= array(
                                   'label'=>"Grid/Step size"
								  ,'description'=>"Snap Tolerance" 
	                              ,'field_control'=>"text" 
								  ,'field_type'=>"CHAR" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"30"
								  ,'lov'=> array(
													'10'

											  )
							       );															   
//////////////
$property['test']['form']= array(
								 'label'=>"Chart Settings"
								,'description'=>"some desc"
								,'save_directory'=>"data/"
								
								,'width'=>-1
								,'height'=>400
								,'modal'=>"n"
								,'min_role_level'=>"ADMIN"
								
									  );
									  
$property['test']['mockuptiger']= array(
                                   'label'=>"Mockup General"
								  ,'description'=>"A value of 0 will dump detail information in the log file. A higher value such as 5 will just dump errors in the log file" 
	                              ,'field_control'=>"PARENT" 
								  ,'field_type'=>"NUMBER" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>5
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );

$property['test']['name']= array(
                                   'label'=>"Chart Name"
								  ,'description'=>"Give a name" 
	                              ,'field_control'=>"text" 
								  ,'field_type'=>"CHAR" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"name1"
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );
$property['test']['chart_type']= array(
                                   'label'=>"Chart Type"
								  ,'description'=>"A value of 0 will dump detail information in the log file. A higher value such as 5 will just dump errors in the log file" 
	                              ,'field_control'=>"select" 
								  ,'field_type'=>"char" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"area"
								  ,'lov'=> array('bar:bar','pie:pie','line:line','area:area','bubble:bubble','funnel:funnel')
							       );
$property['test']['Axes']= array(
                                   'label'=>"Axis Details"
								  ,'description'=>"A value of 0 will dump detail information in the log file. A higher value such as 5 will just dump errors in the log file" 
	                              ,'field_control'=>"PARENT" 
								  ,'field_type'=>"NUMBER" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>5
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );
$property['test']['x_axis_name']= array(
                                   'label'=>"X Axis Label"
								  ,'description'=>"A value of 0 will dump detail information in the log file. A higher value such as 5 will just dump errors in the log file" 
	                              ,'field_control'=>"text" 
								  ,'field_type'=>"char" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"x"
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );
$property['test']['y_axis_name']= array(
                                   'label'=>"Y Axis Label"
								  ,'description'=>"A value of 0 will dump detail information in the log file. A higher value such as 5 will just dump errors in the log file" 
	                              ,'field_control'=>"text" 
								  ,'field_type'=>"char" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"y"
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );
$property['test']['colors']= array(
                                   'label'=>"Color Details"
								  ,'description'=>"A value of 0 will dump detail information in the log file. A higher value such as 5 will just dump errors in the log file" 
	                              ,'field_control'=>"PARENT" 
								  ,'field_type'=>"NUMBER" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>5
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );
								   
$property['test']['font_color']= array(
                                   'label'=>"Font Color"
								  ,'description'=>"A value of 0 will dump detail information in the log file. A higher value such as 5 will just dump errors in the log file" 
	                              ,'field_control'=>"color" 
								  ,'field_type'=>"char" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"ABCFFF"
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );

$property['test']['series1_color']= array(
                                   'label'=>"Series1 Color"
								  ,'description'=>"A value of 0 will dump detail information in the log file. A higher value such as 5 will just dump errors in the log file" 
	                              ,'field_control'=>"color" 
								  ,'field_type'=>"char" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"ABCDEF"
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );

$property['test']['series2_color']= array(
                                   'label'=>"Series2 Color"
								  ,'description'=>"A value of 0 will dump detail information in the log file. A higher value such as 5 will just dump errors in the log file" 
	                              ,'field_control'=>"color" 
								  ,'field_type'=>"char" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"FFFFFF"
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );	
$property['test']['series3_color']= array(
                                   'label'=>"Series3 Color"
								  ,'description'=>"A value of 0 will dump detail information in the log file. A higher value such as 5 will just dump errors in the log file" 
	                              ,'field_control'=>"color" 
								  ,'field_type'=>"char" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"47FF9D"
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );	
$property['test']['background_color']= array(
                                   'label'=>"Background Color"
								  ,'description'=>"A value of 0 will dump detail information in the log file. A higher value such as 5 will just dump errors in the log file" 
	                              ,'field_control'=>"color" 
								  ,'field_type'=>"char" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"FFEEEF"
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );								   

$property['test']['bounds']= array(
                                   'label'=>"Min Max Details"
								  ,'description'=>"A value of 0 will dump detail information in the log file. A higher value such as 5 will just dump errors in the log file" 
	                              ,'field_control'=>"PARENT" 
								  ,'field_type'=>"NUMBER" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>5
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );	

$property['test']['minimum_value']= array(
                                   'label'=>"Minimum Value"
								  ,'description'=>"A value of 0 will dump detail information in the log file. A higher value such as 5 will just dump errors in the log file" 
	                              ,'field_control'=>"slider" 
								  ,'field_type'=>"number" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>30
								  ,'lov'=> array(0,50)
							       );									   
$property['test']['max_value']= array(
                                   'label'=>"Max Value"
								  ,'description'=>"A value of 0 will dump detail information in the log file. A higher value such as 5 will just dump errors in the log file" 
	                              ,'field_control'=>"slider" 
								  ,'field_type'=>"number" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>140
								  ,'lov'=> array(100,500)
							       );		

$property['test']['some_date']= array(
                                   'label'=>"Some Date"
								  ,'description'=>"A value of 0 will dump detail information in the log file. A higher value such as 5 will just dump errors in the log file" 
	                              ,'field_control'=>"date" 
								  ,'field_type'=>"number" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>100
								  ,'lov'=> array(100,500)
							       );	

			   
								   
$property['project']['form']= array(
								 'label'=>"Project"
								,'description'=>"some desc"
								,'save_directory'=>"data/#USER#/#PROJECT#/"
								
								,'width'=>-1
								,'height'=>200
								,'modal'=>"n"
								,'min_role_level'=>"ADMIN"
								
									  );		

/*
$property['project']['project_details']= array(
                                   'label'=>"Project Details"
								  ,'description'=>"A value of 0 will dump detail information in the log file. A higher value such as 5 will just dump errors in the log file" 
	                              ,'field_control'=>"PARENT" 
								  ,'field_type'=>"NUMBER" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>5
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );
	*/								  
$property['project']['name']= array(
                                   'label'=>"Project Name"
								  ,'description'=>"Give a name" 
	                              ,'field_control'=>"text" 
								  ,'field_type'=>"CHAR" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"name1"
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );									  
/*
$property['project']['mockup_details']= array(
                                   'label'=>"What would like to name your Mockup?"
								  ,'description'=>"A value of 0 will dump detail information in the log file. A higher value such as 5 will just dump errors in the log file" 
	                              ,'field_control'=>"PARENT" 
								  ,'field_type'=>"NUMBER" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>5
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );
								   */
								   /*
$property['project']['default_page']= array(
                                   'label'=>"Mockup Title?"
								  ,'description'=>"Give a name" 
	                              ,'field_control'=>"text" 
								  ,'field_type'=>"CHAR" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"Mockup1"
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );
*/								   
								   
$property['mockup']['form']= array(
								 'label'=>"Page Details"
								,'description'=>"some desc"
								,'save_directory'=>"data/#USER#/#PROJECT#/"
								
								,'width'=>-1
								,'height'=>250
								,'modal'=>"n"
								,'min_role_level'=>"ADMIN"
								
									  );		
$property['mockup']['name']= array(
                                   'label'=>"Page Name"
								  ,'description'=>"Give a name" 
	                              ,'field_control'=>"text" 
								  ,'field_type'=>"CHAR" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"name1"
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );

$property['mockup']['description']= array(
                                   'label'=>"Description"
								  ,'description'=>"Give a name" 
	                              ,'field_control'=>"text" 
								  ,'field_type'=>"CHAR" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"Describe it"
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );								   
								   
$property['sharegallery']['form']= array(
								 'label'=>"Share your Page to public Gallery"
								,'description'=>"some desc"
								,'save_directory'=>"data/#USER#/#PROJECT#/"
								
								,'width'=>-1
								,'height'=>250
								,'modal'=>"n"
								,'min_role_level'=>"ADMIN"
								
									  );		
$property['sharegallery']['title']= array(
                                   'label'=>"Name/Title of the Dashboard Page"
								  ,'description'=>"Give a name" 
	                              ,'field_control'=>"text" 
								  ,'field_type'=>"CHAR" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"Give a name"
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );								   

$property['sharegallery']['author']= array(
                                   'label'=>"Your name will appear in the gallery"
								  ,'description'=>"Give a name" 
	                              ,'field_control'=>"text" 
								  ,'field_type'=>"CHAR" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"Give a name"
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );								   

$property['sharegallery']['tags']= array(
                                   'label'=>"Add Tags comma seperated"
								  ,'description'=>"Give Tags" 
	                              ,'field_control'=>"text" 
								  ,'field_type'=>"CHAR" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"Give a name"
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );									   

								   
$property['dashboard_assign_group']['form']= array(
								 'label'=>"Assign project to Group"
								,'description'=>"some desc"
								,'save_directory'=>"system/"								
								,'width'=>-1
								,'height'=>200
								,'modal'=>"n"
								,'min_role_level'=>"ADMIN"
								,'action' =>"sayHi"
									  );
									  

									  
$property['dashboard_assign_group']['group_code']= array(
                                   'label'=>"Group"
								  ,'description'=>""
	                              ,'field_control'=>"SELECT" 
								  ,'field_type'=>"CHAR"
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"N"
								  ,'lov'=> array('Yes:Y','No:N')
							       );								   

$property['dashboard_assign_group']['role_code']= array(
                                   'label'=>"Role"
								  ,'description'=>""
	                              ,'field_control'=>"SELECT" 
								  ,'field_type'=>"CHAR"
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"N"
								  ,'lov'=> array('DEVELOPER:DEVELOPER','VIEWER:VIEWER')
							       );														   

$property['dashboard_assign_group']['project_code']= array(
                                   'label'=>"Project that is being Shared"
								  ,'description'=>""
	                              ,'field_control'=>"text" 
								  ,'field_type'=>"CHAR"
								  ,'field_status'=>"readonly"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"N"
								  ,'lov'=> array('Delete:N','Active:Y','Archive:A')
							       );

$property['image_upload']['size_limit']= array(
                                   'label'=>"Add Tags comma seperated"
								  ,'description'=>"Give Tags" 
	                              ,'field_control'=>"text" 
								  ,'field_type'=>"CHAR" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>1.5*1024*1024
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );									   



/////////////clone dashboards
$property['clone_dashboard']['form']= array(
								 'label'=>"Create copy of current Dashboard"
								,'description'=>"some desc"
								,'save_directory'=>"data/#USER#/#PROJECT#/"
								
								,'width'=>650
								,'height'=>350
								,'modal'=>"n"
								,'min_role_level'=>"ADMIN"
								
									  );		
$property['clone_dashboard']['source_page_name']= array(
                                   'label'=>"Source Page Name"
								  ,'description'=>"Give a name" 
	                              ,'field_control'=>"text" 
								  ,'field_type'=>"CHAR" 
								  ,'field_status'=>"NOEDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>""
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );

$property['clone_dashboard']['source_page_code']= array(
                                   'label'=>"Source Page Code"
								  ,'description'=>"Give a name" 
	                              ,'field_control'=>"text" 
								  ,'field_type'=>"CHAR" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>""
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );

$property['clone_dashboard']['target_project_code']= array(
                                   'label'=>"Target Project"
								  ,'description'=>"Give a name" 
	                              ,'field_control'=>"SELECT" 
								  ,'field_type'=>"CHAR"
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"N"
								  ,'lov'=> array('None:None')
							       );
								   
$property['clone_dashboard']['target_page_name']= array(
                                   'label'=>"Target Page Name"
								  ,'description'=>"Give a name" 
	                              ,'field_control'=>"text" 
								  ,'field_type'=>"CHAR" 
								  ,'field_status'=>"EDIT"
								  ,'min_role_level'=>"ADMIN"
								  ,'default_value'=>"Describe it"
								  ,'lov'=> array('0:0','1:1','2:2','3:3','4:4','5:5')
							       );										  

									  

								   
$property['ss']='N';
$property['ss_default_plan']='MEDIUM';
	
$property['save_threshold']=20;//obsolete - do not use - do not delete

$property['table_prefix']='ic'; //used for data uploads and social tables, value of 'user' will prefix it with userid_

$property['email_send_alert']="contact@infocaptor.com";
$property['from_name_send_alert']="InfoCaptor Data Notification";
$property['xxp']="xxp";
$property['xxl']="xxl";
$property['upload_log_size']="200000000";
$property['upload_csv_size']="100000000";
//$property['exclude_menu']=array("Export to PDF" => "Export to PDF","Export to PNG" => "Export to PNG","Export to JPEG" => "Export to JPEG","About"=>"About","Help/Documentation"=>"Help/Documentation","Register"=>"Register");
$property['exclude_menu']=array("Ab out"=>"Ab out","He lp/Documentation"=>"Help/Doc umentation","R egister"=>"Regis ter");
$property['connection_template_mesg']="";
$property['page_title']="Business Intelligence Dashboards, Reports and Metrics";


?>