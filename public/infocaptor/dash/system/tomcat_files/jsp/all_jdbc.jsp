<%@page contentType="text/html; charset=UTF-8" %><%@ page import = "java.io.*" %><%@ page import = "org.apache.commons.codec.binary.Base64" %><%@ page import = "java.sql.*" %><%@ page import = "java.lang.*" %><%
			
			 String method=request.getMethod();
             String dbuser = request.getParameter("dbuser");
             String dbpass = request.getParameter("dbpass");
			 String dbname = request.getParameter("dbname");
			 String jdbcdriver = request.getParameter("jdbcdriver");
			 String jdbcurl = request.getParameter("jdbcurl");
			 String encodeSql = request.getParameter("sql");
			 String rowlimit= request.getParameter("row_limit");
			 if (rowlimit==null || rowlimit.length()<=0) rowlimit="10";
			 int row_limit=Integer.parseInt(rowlimit);
			// out.println("user="+dbuser);
			// out.println("pass="+dbpass);
			 

               //String db_url = jdbcurl;
               //String Driver = jdbcdriver;
               //jdbcurl = "jdbc:odbc:"+"northwind_system";
               //jdbcdriver = "sun.jdbc.odbc.JdbcOdbcDriver";			   

         try
          {  
		    if (method.equals("GET"))
			{
			  out.println("Invalid values");
			  return;
			}
			/*
				URL infocaptor = new URL("http://localhost/infocaptor_dev/base/whoisit.php");
				BufferedReader in = new BufferedReader(
				new InputStreamReader(infocaptor.openStream()));

				String inputLine;
				inputLine = in.readLine();
					out.println("whoisit : "+inputLine);
				in.close();
				*/
		
			String encodeRefer= request.getHeader("referer");
			String refer=new String(Base64.decodeBase64(encodeRefer.getBytes()));
			if (!(refer.equals("INf0C@pt*r#%@@!__===")) )
			{
			  out.println("Invalid values");
			  return;
			}
			String sql=new String(Base64.decodeBase64(encodeSql.getBytes()));
               Class.forName(jdbcdriver);   
				Connection con = DriverManager.getConnection(jdbcurl,dbuser,dbpass);

             Statement st = con.createStatement();
			//out.print("connected<br>");
			if (sql==null || sql.length()<=0) sql="select 'no sql' from dual";
			//sql="select * from customers";
			//out.print("now executing<br>");
               ResultSet rs = st.executeQuery(sql);
			  // out.print("executed<br>");
			   ResultSetMetaData   metaData=rs.getMetaData();
			   ///////////////
			   int numberOfColumns =  metaData.getColumnCount();
				String           columnNames = "";
			   
			   // Get the column names and cache them.
			   // Then we can close the connection.
			   for(int column = 0; column < numberOfColumns; column++) 
			   {
				 try
				 {
				   columnNames+="\""+metaData.getColumnLabel(column+1)+"\"";
				   if (column < numberOfColumns-1) columnNames+=",";

				     
				 }
				 catch (SQLException eix)
				 {
				   
				   eix.printStackTrace();
				 }
			   }
				//out.print("all columns collected<br>");
			   // Get all rows.

			   
			   String[]            columnData = {};
			   columnData = new String[numberOfColumns];
			   int rowCount=0;
			   String cellValue="";
			   String utf8Val="";
			   ResultSetMetaData rsmd = rs.getMetaData();
			   int columnType;// = rsmd.getColumnType(i);
			   int[] colTypes= new int[numberOfColumns];
			   String colTypeNames= "";
			   for (int i = 1; i <= numberOfColumns ; i++) 
				   {
				     colTypes[i-1]=rsmd.getColumnType(i);
					 //colTypeNames[i-1]=rsmd.getColumnTypeName(i);
					 colTypeNames+="\""+rsmd.getColumnTypeName(i)+"\"";
					 if (i < numberOfColumns) colTypeNames+=",";
				   }
			   while (rs.next() && rowCount<row_limit) 
			   {
			     //out.print("row# "+rowCount);
				 try
				 {

				   rowCount++;
				   for (int i = 1; i <= numberOfColumns ; i++) 
				   {
				      cellValue=rs.getString(i);
					  if (rowCount==1) columnData[i-1]="";
					  
		
					//cellValue=cellValue;
					//now replace new line from string values
					if (cellValue!=null && ((colTypes[i-1] == Types.VARCHAR || colTypes[i-1] == Types.CHAR) ))
					{
						cellValue=cellValue.replaceAll("\r\n", "<br>").replaceAll("\n", "<br>").replaceAll("\"", "\\\\\"");
					}
						
					  columnData[i-1]+="\""+cellValue+"\",";
					  
						//out.println(cellValue);
					  //newRow.add(resultSet.getObject(i));
				   }

				   
				 }
				 catch (SQLException erx)
				 {
				      
						out.print("{");
						out.print("\"legends\":");
						out.print("[\""+"No Data"+"\"],");
						out.print("\"data\":");
						out.print("[");
						out.print("[\""+"No Data Found"+"\"]");
						out.print("],");
						out.print("\"error_flag\":\"true\",");
						out.print("\"error_mesg\":\""+erx.getMessage()+"\"");
						out.print("}");
					 return;
				 }
				}		

				
				//display the JSON
				out.print("{");
				out.print("\"legends\":");
				out.print("["+columnNames+"],");
				out.print("\"columnTypes\":");
				out.print("["+colTypeNames+"],");				
				out.print("\"data\":");
				out.print("[");
				
				try
				{
					for (int i = 0; i < numberOfColumns ; i++) 
					{
					  
						out.print("["+columnData[i].substring(0, columnData[i].length() - 1)  +"]");
					  
					  if (i<numberOfColumns-1) 	out.print(",");
					}
				}
				catch (Exception e3)
				{
					for (int i = 0; i < numberOfColumns ; i++) 
					{
					  
						out.print("[\"no data\"]");
					  
					  if (i<numberOfColumns-1) 	out.print(",");
					}

				}
				out.print("]");
				
				out.print("}");
			   ///////////////
			   
			   /*
                   while(rs.next())
                   {
                     out.println(rs.getString(1));
                      out.println("<br/>");                          
                    }
					*/
           st.close ();
           con.close();            
         }             
        catch(Exception e)
         { 
				out.print("{");
				out.print("\"legends\":");
				out.print("[\""+"No Data"+"\"],");
				out.print("\"data\":");
				out.print("[");
				out.print("[\""+"No Data Found"+"\"]");
				out.print("],");
				out.print("\"error_flag\":\"true\",");
				out.print("\"error_mesg\":\""+e.getMessage()+"\"");
				out.print("}");
		     return;
			 /*
		 out.println("\n msg" + e.getMessage());
            out.println("\n str" + e.toString());
           //out.println(e);
			StackTraceElement[] traceElements = e.getStackTrace();
			out.println(" leng"+traceElements.length);
            for(int i=0 ; i<traceElements.length ; i++){
                out.println("<br>" + traceElements[i].getMethodName());
                out.println("("  + traceElements[i].getClassName() + ":");
                out.println(traceElements[i].getLineNumber() + ")");
            }
			*/
          }
%>