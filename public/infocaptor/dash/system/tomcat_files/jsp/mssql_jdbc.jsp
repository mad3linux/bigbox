<%@ page import = "java.io.*" %><%@ page import = "java.sql.*" %><%@ page import = "java.lang.*" %><%
             String dbuser = request.getParameter("dbuser");
             String dbpass = request.getParameter("dbpass");
			 String dbname = request.getParameter("dbname");
			 String host = request.getParameter("host");
			 String port = request.getParameter("port");
			 String sql = request.getParameter("sql");
			 String rowlimit= request.getParameter("row_limit");
			 if (rowlimit.length()<=0) rowlimit="10";
			 int row_limit=Integer.parseInt(rowlimit);
			// out.println("user="+dbuser);
			// out.println("pass="+dbpass);
			 

               String db_url = "jdbc:jtds:sqlserver://"+host+":"+port+"/"+dbname;
               String Driver = "net.sourceforge.jtds.jdbc.Driver";

         try
          {  
               Class.forName(Driver);   
   Connection con = DriverManager.getConnection(db_url,dbuser,dbpass);

             Statement st = con.createStatement();

			if (sql.length()<=0) sql="select 'no sql' from dual";
			
               ResultSet rs = st.executeQuery(sql);
			   ResultSetMetaData   metaData=rs.getMetaData();
			   ///////////////
			   int numberOfColumns =  metaData.getColumnCount();
				String           columnNames = "";
			   
			   // Get the column names and cache them.
			   // Then we can close the connection.
			   for(int column = 0; column < numberOfColumns; column++) 
			   {
				 try
				 {
				   columnNames+="\""+metaData.getColumnLabel(column+1)+"\"";
				   if (column < numberOfColumns-1) columnNames+=",";

				     
				 }
				 catch (SQLException eix)
				 {
				   
				   eix.printStackTrace();
				 }
			   }
				
			   // Get all rows.

			   
			   String[]            columnData = {};
			   columnData = new String[numberOfColumns];
			   int rowCount=0;
			   String cellValue="";
			   while (rs.next() && rowCount<row_limit) 
			   {
				 try
				 {

				   rowCount++;
				   for (int i = 1; i <= numberOfColumns ; i++) 
				   {
				      cellValue=rs.getString(i);
					  if (rowCount==1) columnData[i-1]="";
					  
					  columnData[i-1]+="\""+cellValue+"\",";
						//out.println(cellValue);
					  //newRow.add(resultSet.getObject(i));
				   }

				   
				 }
				 catch (SQLException erx)
				 {
				     
				   erx.printStackTrace();
				 }
				}		

				
				//display the JSON
				out.print("{");
				out.print("\"legends\":");
				out.print("["+columnNames+"],");
				out.print("\"data\":");
				out.print("[");
				
				for (int i = 0; i < numberOfColumns ; i++) 
				{
				  
					out.print("["+columnData[i].substring(0, columnData[i].length() - 1)  +"]");
				  
				  if (i<numberOfColumns-1) 	out.print(",");
				}
				out.print("]");
				
				out.print("}");
			   ///////////////
			   
			   /*
                   while(rs.next())
                   {
                     out.println(rs.getString(1));
                      out.println("<br/>");                          
                    }
					*/
           st.close ();
           con.close();            
         }             
        catch(Exception e)
         { 
           out.println(e);
          e.printStackTrace();  
          }
%>