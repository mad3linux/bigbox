<?php


  $ss['service:twitter_all']="Twitter - All metrics";
  $ss['service:twitter_specific']="Twitter - specific metric (e.g followers etc)";
  $ss['service:facebook_all']="Facebook - All metrics";
  $ss['service:facebook_specific']="Facebook - specific metric (e.g likes etc)";
  $ss['service:feedburner_all']="Feedburner - All metrics";
  $ss['service:feedburner_specific']="Feedburner - specific metric (e.g circulation etc)";  
  $ss['service:alexa_all']="Alexa - All metrics";
  $ss['service:alexa_specific']="Alexa - specific metric (e.g links,reach,rank etc)";    
  $ss['service:backlinks']="Google Backlink Count";  
  $ss['service:pagerank']="Google PageRank ";  
  $ss['service:pagespeed_all']="Google PageSpeed - All Metrics ";  
  $ss['service:pagespeed_specific']="Google PageSpeed - specific metric (e.g score,bytes) ";  
  $ss['service:github_all']="Github - All Metrics ";  
  $ss['service:gitbub_specific']="Github - specific metric (e.g open_issues,watchers,forks) ";    
  $ss['service:rss']="RSS - Title,Link,Date";
  $ss['service:http_html']="HTTP Output as HTML - Any URL that is public ";
  $ss['service:http_xml']="HTTP Output as XML - Any URL that is public ";
  $ss['service:http_json']="HTTP Output as JSON - Any URL that is public ";
  $ss['service:http_text']="HTTP Output as text - Any URL that is public ";
  $ss['service:http_post']="HTTP Secure - Username/Password via POST ";
  $ss['service:googlespreadsheet']="Google Spreadsheet - Public ";
  $ss['service:simulate']="Simulation ";	
 $ss['service:countdown']="Count Down ";


?>