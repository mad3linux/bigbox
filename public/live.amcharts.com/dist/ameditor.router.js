jQuery.router.add('/pivottable/amchart',function(){
	jQuery.router.go('/pivottable/amchart/new/');
	// AmCharts.Editor.log('info','route landing');
	// AmCharts.Editor.updateSection('landing');
	// AmCharts.Editor.modal('hide');
});
jQuery.router.add('/pivottable/amchart/pid/:chart_id',function(){
	// AmCharts.Editor.log('info','route new chart');
	// AmCharts.Editor.updateSection('editor');
	// // GET USER DATA
	// AmCharts.Editor.getUserCharts();
	// var chartTPL= {};
	// try{chartTPL= AmCharts.Editor.parseJSON(stripslashes(PRABAC_cfg.template));}catch(e){chartTPL=PRABAC_cfg.template}
	// // console.log(chartTPL);
	// // console.log(typeof chartTPL);
	// var data = chartTPL;
	// // console.log(data);
	// data.loaded = false;
	// // console.log(data);

	// // RESET; REMEMBER
	// AmCharts.Editor.backToDefaults();
	// AmCharts.Editor.current.tpl = data;
	// // console.log(AmCharts.Editor.current.tpl);
	// // return false;
	// // // CHANGE ROUTE
	// jQuery.router.go('/pivottable/amchart/new/edit/');
	// jQuery.router.go('/pivottable/amchart/new/');
	if(AmCharts.Editor.current.user.loading){return false;}
	// console.log(data);
	AmCharts.Editor.updateSection('editor');
	AmCharts.Editor.current.user.loading=true;
	AmCharts.Editor.current.user.loading=false;
	// console.log(PRABAC_cfg.code);
	var chartCFG= PRABAC_cfg.code;
	if(chartCFG.legend!=undefined){
		delete chartCFG.legend;
	}
	_chart = JSON.stringify(chartCFG);
	_chart = JSON.parse(_chart);
	if(_chart.categoryAxis==undefined){
		_chart.categoryAxis = {};
	}
	_chart.categoryAxis.labelFunction = {};
	_chart.dataProvider = [];
	_chart = JSON.stringify(_chart);
	_chart = _chart.replace('"labelFunction":{}','"labelFunction":function( label, item ) {var lbl="";if(item!=undefined){lbl = String(item.dataContext.columns);}return lbl;}');
	var chartTPL= PRABAC_cfg.template;
	var chartCSS= PRABAC_cfg.css;
	var chartData= grpdata;
	jQuery.extend(AmCharts.Editor.current.cfg,chartCFG||{});
	jQuery.extend(AmCharts.Editor.current.tpl,chartTPL||{});
	jQuery.extend(AmCharts.Editor.current.css,chartCSS||{});
	jQuery('.nav-themes li').each(function(){
		jQuery(this)[jQuery(this).data('theme')==chartCFG.theme?'addClass':'removeClass']('active');
	});
	AmCharts.Editor.init(chartCFG);
	AmCharts.Editor.CONSTANT.DATASOURCE = PRABAC_api;
	AmCharts.Editor.CONSTANT.ISHTTP = true;
	AmCharts.Editor.CONSTANT.PRABACAPI = PRABAC_pid;
	AmCharts.Editor.current.chart.chart_id = PRABAC_pid;
	AmCharts.Editor.current.chart.title = formsave.title;
	AmCharts.Editor.current.chart.description = formsave.description;
	if(chartData.length>0){
		jQuery('.am-tab-data').show().find('a').tab('show');
		AmCharts.Editor.processData(chartData);
	}else{
		jQuery('.am-tab-data').hide();
		jQuery('.am-tab-code a').tab('show');
	}
	if($("li.menu-item.menu-item-class.menu-item-switchable a span").length>0){
		$("li.menu-item.menu-item-class.menu-item-switchable a span").each(function(k,v){
			// console.log(k);
			// console.log($(v).html());
			if($(v).html()=="Legend"){
				// console.log($(v).next();
				$(v).next().click();
			}
		
		});
	}
	if(AmCharts.Editor.current.chart.chart_id!=PRABAC_pid){
		AmCharts.Editor.current.chart.chart_id = PRABAC_pid;
	}
});

jQuery.router.add('/pivottable/amchart/new/','/pivottable/amchart/new/',function(data){
	// console.log("NEW");
	AmCharts.Editor.log('info','route new chart');
	// if(AmCharts.Editor.current.user.loading){return false;}
	// AmCharts.Editor.backToDefaults();
	AmCharts.Editor.updateSection('editor');
	// AmCharts.Editor.updateSection('editor');
	AmCharts.Editor.modal({
		remote:AmCharts.Editor.CONSTANT.URL_BASE+'static/tpl/modal-chart-new.tpl?__='+ AmCharts.Editor.CONSTANT.RND_KEY
	});
	
	// AmCharts.Editor.current.user.loading=true;
	// AmCharts.Editor.current.user.loading=false;
	// AmCharts.Editor.current.tpl.loaded=true;
	// jQuery('.nav-themes li').each(function(){
		// jQuery(this)[jQuery(this).data('theme')=='default'?'addClass':'removeClass']('active');
	// });
	// AmCharts.Editor.init(chartconfig);
	// AmCharts.Editor.modal('hide');
	// jQuery('.nav-themes li').each(function(){
		// jQuery(this)[jQuery(this).data('theme')=='default'?'addClass':'removeClass']('active');
	// });
	// // console.log(chartconfig);
	// if(!chartconfig.dataProvider){
		// jQuery('.am-tab-data').hide();
		// jQuery('.am-tab-code a').tab('show');
	// }else if(grpdata.length>0){
		// // console.log(grpdata);
		// jQuery('.am-tab-data').show().find('a').tab('show');
		// AmCharts.Editor.processData(grpdata);
	// }else{
		// jQuery('.am-tab-data').show().find('a').tab('show');
		// // console.log(chartCFG.dataProvider);
		// AmCharts.Editor.processData(chartconfig.dataProvider);
	// }
	
	// if($("li.menu-item.menu-item-class.menu-item-switchable a span").length>0){
		// $("li.menu-item.menu-item-class.menu-item-switchable a span").each(function(k,v){
			// // console.log(k);
			// // console.log($(v).html());
			// if($(v).html()=="Legend"){
				// // console.log($(v).next();
				// $(v).next().click();
			// }
		
		// });
	// }
	// jQuery.router.go('/pivottable/amchart/new/edit/');
});

jQuery.router.add('/pivottable/amchart/new/edit/','/pivottable/amchart/new/edit/',function(data){
	AmCharts.Editor.log('info','route new chart edit');
	// console.log(AmCharts.Editor.current.tpl);
	if(AmCharts.Editor.current.user.loading){return false;}
	if(AmCharts.Editor.current.stranger){
		AmCharts.Editor.current.chart.chart_id='';
		AmCharts.Editor.current.stranger=false;
		AmCharts.Editor.updateUserNav();
	}else if(AmCharts.Editor.current.tpl.name && !AmCharts.Editor.current.tpl.loaded){
		AmCharts.Editor.updateSection('editor');
		AmCharts.Editor.current.user.loading=true;
		// console.log(AmCharts.Editor.current.tpl.tpl);
		jQuery.ajax({
			dataType:'json',
			url:AmCharts.Editor.current.tpl.tpl,
			complete:function(transport){
				var response=transport.responseJSON||{error:true};
				AmCharts.Editor.current.user.loading=false;
				AmCharts.Editor.current.tpl.loaded=true;
				// console.log(transport);
				// console.log(response);
				if(!response.error){
					// jQuery.extend(response,chartconfig||{});
					// console.log(response);
					// console.log(chartconfig);
					response.categoryField = chartconfig.categoryField;
					var grp = chartconfig.graphs;
					if(response.graphs[0]!=undefined){
						$.each(grp,function(k,v){
							chartconfig.graphs[k].type = response.graphs[0].type;
							chartconfig.graphs[k].fillAlphas = response.graphs[0].fillAlphas;
						});
					}
					response.graphs = chartconfig.graphs;
					response.dataProvider = (chartconfig.dataProvider.length>0)?chartconfig.dataProvider:grpdata;
					response.titles = chartconfig.titles;
					response.legend = chartconfig.legend;
					response['export'] = chartconfig['export'];
					response.valueAxes = chartconfig.valueAxes;
					response.categoryAxis = chartconfig.categoryAxis;
					_chart = JSON.stringify(response);
					_chart = JSON.parse(_chart);
					_chart.categoryAxis.labelFunction = {};
					_chart.dataProvider = [];
					_chart = JSON.stringify(_chart);
					_chart = _chart.replace('"labelFunction":{}','"labelFunction":function( label, item ) {var lbl="";if(item!=undefined){lbl = String(item.dataContext.columns);}return lbl;}');
					// console.log(_chart);
					var lblfunct = {labelFunction : function( label, item ) {
						var lbl="";if(item!=undefined){lbl = String(item.dataContext.columns);}
						// var lbls = lbl.split("::");
						// // console.log(lbls);
						// var tmp = "";
						// for(i=0;i<lbls.length;i++){
							// tmp += lbls[i]+"<br>";
						// }
						return lbl;
					}};
					jQuery.extend(chartconfig.categoryAxis,lblfunct||{});
					response.categoryAxis = chartconfig.categoryAxis;
					// console.log(JSON.stringify(response));
					// response.categoryAxis.add({labelFunction : function( label, item ) {console.log(item.dataContext);return item.dataContext.section;}});
					// console.log(response);
					jQuery('.nav-themes li').each(function(){
						jQuery(this)[jQuery(this).data('theme')=='default'?'addClass':'removeClass']('active');
					});
					// console.log(response);
					AmCharts.Editor.init(response);
					AmCharts.Editor.CONSTANT.DATASOURCE = PRABAC_api;
					AmCharts.Editor.CONSTANT.ISHTTP = true;
					AmCharts.Editor.CONSTANT.PRABACAPI = PRABAC_pid;
					AmCharts.Editor.current.chart.chart_id = PRABAC_pid;
					AmCharts.Editor.modal('hide');
				}else{
					jQuery.router.go('/pivottable/amchart');
					AmCharts.Editor.log('error','receiving chart: '+ data.chart_id);
				}
			}
		});
	}else if(AmCharts.Editor.current.tpl.loaded!=true){
		AmCharts.Editor.log('info','new edit redirect');
		// console.log(AmCharts.Editor.current.tpl.loaded);
		jQuery.router.go('/pivottable/amchart/new/');
	}
});

// jQuery.router.add('/pivottable/amchart/edit/:chart_id','/pivottable/amchart/chart/edit/',function(data){
	// jQuery.router.go('/pivottable/amchart/new/edit/');
	// AmCharts.Editor.log('info','route edit chart');
	// if(AmCharts.Editor.current.user.loading){return false;}
	// // console.log(data);
	// AmCharts.Editor.updateSection('editor');
	// if(AmCharts.Editor.current.chart.chart_id){
		// AmCharts.Editor.modal('hide');
	// }else{
		// AmCharts.Editor.current.user.loading=true;
		// AmCharts.Editor.ajax({
			// data:{action:'amcharts_editor_get_chart',chart_id:data.chart_id},
			// complete:function(transport){
				// var response=transport.responseJSON||{error:true};
				// // var response = {error: false, message: "", id: 0, login_url: "https://www.amcharts.com/sign-in/editor/?skinny=1&…%2F%2Flive.amcharts.com%2Fauth%2F%3Faction%3Ddone"};
				// AmCharts.Editor.current.user.loading=false;
				// // console.log(response);
				// // console.log(!response.error);
				// // var response = {error:false};
				// jQuery.extend(AmCharts.Editor.current.chart,response);
				// if(!response.error){
					// // var chartCFG=PRABAC_config.chartCFG;
					// // var chartTPL=PRABAC_config.chartTPL;
					// // var chartCSS=PRABAC_config.chartCSS;
					// // console.log(response.code);
					// // console.log(typeof response.code);
					// var chartCFG= {};
					// try{chartCFG= AmCharts.Editor.parseJSON(stripslashes(response.code));}catch(e){chartCFG=response.code}
					// var chartTPL= {};
					// try{chartTPL= AmCharts.Editor.parseJSON(stripslashes(response.template));}catch(e){chartTPL=response.template}
					// var chartCSS= {};
					// try{chartCSS= AmCharts.Editor.parseJSON(stripslashes(response.css));}catch(e){chartCSS=response.css}
					// var chartData= {};
					// try{chartData= AmCharts.Editor.parseJSON(stripslashes(response.provdata));}catch(e){chartData=response.provdata}
					// // console.log(response);
					// // console.log(chartCFG);
					// jQuery.extend(AmCharts.Editor.current.cfg,chartCFG||{});
					// jQuery.extend(AmCharts.Editor.current.tpl,chartTPL||{});
					// jQuery.extend(AmCharts.Editor.current.css,chartCSS||{});
					// jQuery('.nav-themes li').each(function(){
						// jQuery(this)[jQuery(this).data('theme')==chartCFG.theme?'addClass':'removeClass']('active');
					// });
					// try{PRABAC_config= AmCharts.Editor.parseJSON(stripslashes(response));}catch(e){PRABAC_config=response}
					// try{PRABAC_data= AmCharts.Editor.parseJSON(stripslashes(response.provdata));}catch(e){PRABAC_data=response.provdata}
					// // PRABAC_config = response;
					// // PRABAC_data = response;
					// // console.log(chartCFG);
					// // console.log(chartTPL);
					// // console.log(chartCSS);
					// // console.log(response);
					// // console.log(PRABAC_config);
					// // console.log(PRABAC_data);
					// // AmCharts.Editor.log('info','init config: '+ chartCFG);
					// // if(chartCFG){
						// AmCharts.Editor.init(chartCFG);
					// // }
					// if(!chartCFG.dataProvider){
						// jQuery('.am-tab-data').hide();
						// jQuery('.am-tab-code a').tab('show');
					// }else{
						// jQuery('.am-tab-data').show().find('a').tab('show');
						// // console.log(chartCFG.dataProvider);
						// AmCharts.Editor.processData(chartCFG.dataProvider);
					// }

					// if(PRABAC_data.length>0){
						// jQuery('.am-tab-data').show().find('a').tab('show');
						// AmCharts.Editor.processData(chartData);
					// }else{
						// jQuery('.am-tab-data').hide();
						// jQuery('.am-tab-code a').tab('show');
					// }
					// // console.log($("li.menu-item.menu-item-class.menu-item-switchable a span"));
					// if($("li.menu-item.menu-item-class.menu-item-switchable a span").length>0){
						// $("li.menu-item.menu-item-class.menu-item-switchable a span").each(function(k,v){
							// // console.log(k);
							// // console.log($(v).html());
							// if($(v).html()=="Legend"){
								// // console.log($(v).next();
								// $(v).next().click();
							// }
						
						// });
					// }
					// if(AmCharts.Editor.current.chart.chart_id!=PRABAC_pid){
						// AmCharts.Editor.current.chart.chart_id = PRABAC_pid;
					// }

					// var form= {};
					// try{form= AmCharts.Editor.parseJSON(stripslashes(response.form));}catch(e){form=response.form}
					// // console.log(form);
					// // console.log(form.length);
					// if(!jQuery.isEmptyObject(form) || form.length>0){
						// AmCharts.Editor.CONSTANT.DATASOURCE = "Prabac API";
						// AmCharts.Editor.CONSTANT.PRABACAPI = form;
						// AmCharts.Editor.CONSTANT.ISHTTP = true;
					// }

					// var form2= {};
					// try{form2= AmCharts.Editor.parseJSON(stripslashes(response.form2));}catch(e){form=response.form2}
					// // console.log(form);
					// // console.log(form.length);
					// if(!jQuery.isEmptyObject(form2) || form2.length>0){
						// AmCharts.Editor.CONSTANT.PRABACAPI2 = form2;
					// }
					// AmCharts.Editor.current.chart.chart_id = PRABAC_pid;
					// AmCharts.Editor.modal('hide');
					// if(!AmCharts.Editor.current.chart.owner){
						// AmCharts.Editor.current.stranger=true;
						// AmCharts.Editor.log('info','chart doesnt belong to this user ');
						// jQuery.router.go('/pivottable/amchart/new/edit/');
					// }
				// }else{
					// jQuery.router.go('/prabacontent/prabaeditor/listportlet');
					// AmCharts.Editor.log('error','receiving chart: '+ data.chart_id);
				// }
			// }
		// });
	// }
	// // return false;
// });

// jQuery.router.add('/pivottable/amchart/edit/:chart_id/draft/','/pivottable/amchart/chart/edit/draft/',function(data){
	// jQuery.router.go('/pivottable/amchart/new/edit/');
	// AmCharts.Editor.log('info','route edit draft');
	// if(AmCharts.Editor.current.user.loading){return false;}
	// AmCharts.Editor.updateSection('editor');
	// if(AmCharts.Editor.current.chart.chart_id){
		// AmCharts.Editor.modal('hide');
	// }else{
		// AmCharts.Editor.current.user.loading=true;
		// AmCharts.Editor.ajax({
			// data:{action:'amcharts_editor_get_chart',chart_id:data.chart_id,draft:'1'},
			// complete:function(transport){
				// var response=transport.responseJSON||{error:true};
				// AmCharts.Editor.current.user.loading=false;
				// jQuery.extend(AmCharts.Editor.current.chart,response);
				// if(!response.error){
					// var chartCFG=AmCharts.Editor.parseJSON(stripslashes(response.code));
					// var chartTPL=AmCharts.Editor.parseJSON(stripslashes(response.template));
					// var chartCSS=AmCharts.Editor.parseJSON(stripslashes((response.css||'{}')));
					// jQuery.extend(AmCharts.Editor.current.cfg,chartCFG||{});
					// jQuery.extend(AmCharts.Editor.current.tpl,chartTPL||{});
					// jQuery.extend(AmCharts.Editor.current.css,chartCSS||{});
					// jQuery('.nav-themes li').each(function(){
						// jQuery(this)[jQuery(this).data('theme')==chartCFG.theme?'addClass':'removeClass']('active');
					// });
					// AmCharts.Editor.init(chartCFG);
					// AmCharts.Editor.modal('hide');
				// }else{
					// jQuery.router.go('/pivottable/amchart/new');
					// AmCharts.Editor.log('error','receiving chart: '+ data.chart_id);
				// }
			// }
		// });
	// }
// });

routeCallback=function(data){
	AmCharts.Editor.log('info','route share chart');
	if(AmCharts.Editor.current.user.loading){return false;}
	if(AmCharts.Editor.current.chart.chart_id){
		AmCharts.Editor.updateSection('share');
		AmCharts.Editor.modal('hide');
	}else{
		AmCharts.Editor.log('info','LOAD share chart');
		AmCharts.Editor.current.user.loading=true;
		AmCharts.Editor.ajax({
			data:{action:'amcharts_editor_get_chart',chart_id:data.chart_id},
			complete:function(transport){
				var response=transport.responseJSON||{error:true};
				console.log(response);
				AmCharts.Editor.current.user.loading=false;
				jQuery.extend(AmCharts.Editor.current.chart,response);
				console.log(response.error);
				if(!response.error){
					var chartCFG=AmCharts.Editor.parseJSON(stripslashes(response.code));
					var chartTPL=AmCharts.Editor.parseJSON(stripslashes(response.template));
					var chartCSS=AmCharts.Editor.parseJSON(stripslashes((response.css||'{}')));
					jQuery.extend(AmCharts.Editor.current.cfg,chartCFG||{});
					jQuery.extend(AmCharts.Editor.current.tpl,chartTPL||{});
					jQuery.extend(AmCharts.Editor.current.css,chartCSS||{});
					jQuery('.nav-themes li').each(function(){
						jQuery(this)[jQuery(this).data('theme')==chartCFG.theme?'addClass':'removeClass']('active');
					});
					AmCharts.Editor.dataProvider=chartCFG.dataProvider||[];
					AmCharts.Editor.updateSection('share');
					AmCharts.Editor.modal('hide');
				}else{
					jQuery.router.go('/pivottable/amchart/new');
					AmCharts.Editor.log('error','receiving chart: '+ data.chart_id);
				}
			}
		});
	}
}

// jQuery.router.add('/pivottable/amchart/:chart_id/','/pivottable/amchart/chart/',routeCallback);
// jQuery.router.add('/pivottable/amchart/:chart_id/share/','/pivottable/amchart/chart/share/',routeCallback);
// jQuery.router.add('/pivottable/amchart/:chart_id/404/',function(data){
// 	AmCharts.Editor.log('info','not found');
// 	jQuery.router.go('/pivottable/amchart/new');
// });
// jQuery.router.add('/pivottable/amchart/:chart_id/:action/','/pivottable/amchart/chart/action/',function(data){
// 	AmCharts.Editor.log('info',data);
// 	jQuery.router.go('/pivottable/amchart/new');
// });

jQuery(document).ready(function(){
	// console.log(location.hash.slice(1));
	jQuery.router.go(location.hash.slice(1));
	AmCharts.Editor.getUser();
	setTimeout(function(){
		jQuery('body').removeClass('glamorize');
		jQuery('.modal-backdrop-init').removeClass('in').one($.support.transition.end,function(){
			jQuery(this).remove();
		}).emulateTransitionEnd(300);
	},1000);
});