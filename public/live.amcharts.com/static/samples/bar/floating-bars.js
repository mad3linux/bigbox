{
	"categoryField":"col-0","rotate":true,"startDuration":1,"categoryAxis":{"gridPosition":"start"},"trendLines":[],
	"graphs":[{"balloonText":"open:[[open]] close:[[close]]","closeField":"col-2","fillAlphas":1,"id":"AmGraph-1","openField":"col-1","title":"graph 1","type":"column","valueField":1}],
	"guides":[],"valueAxes":[{"id":"ValueAxis-1","stackType":"regular","position":"left","title":"Axis title"}],"allLabels":[],"balloon":{},"titles":[{"id":"Title-1","size":15,"text":"Chart Title"}],
	"type":"serial",
	"dataProvider":[
		{"col-0":"category 1","col-1":8,"col-2":5},
		{"col-0":"category 2","col-1":6,"col-2":7},
		{"col-0":"category 3","col-1":2,"col-2":3}
	]
}