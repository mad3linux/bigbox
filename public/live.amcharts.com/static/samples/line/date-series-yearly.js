{"type":"serial","categoryField":"col-0","dataDateFormat":"YYYY","theme":"default","categoryAxis":{"minPeriod":"YYYY","parseDates":true},
"chartCursor":{"animationDuration":0,"categoryBalloonDateFormat":"YYYY"},"chartScrollbar":{},"trendLines":[],
"graphs":[
{"bullet":"round","id":"AmGraph-1","title":"graph 1","valueField":"col-1"},
{"bullet":"square","id":"AmGraph-2","title":"graph 2","valueField":"col-2"}],"guides":[],
"valueAxes":[{"id":"ValueAxis-1","position":"left","title":"Axis title"}],"allLabels":[],"balloon":{},"legend":{"useGraphSettings":true},
"titles":[{"id":"Title-1","size":15,"text":"Chart Title"}],
"dataProvider":[
{"col-0":"2010","col-1":8,"col-2":5},
{"col-0":"2011","col-1":6,"col-2":7},
{"col-0":"2012","col-1":2,"col-2":3},
{"col-0":"2013","col-1":1,"col-2":3},
{"col-0":"2014","col-1":2,"col-2":1},
{"col-0":"2015","col-1":3,"col-2":2},
{"col-0":"2016","col-1":6,"col-2":8}
]}