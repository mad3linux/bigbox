{"type":"pie","angle":12,"balloonText":"[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>","depth3D":15,"innerRadius":"40%",
"titleField":"col-0","valueField":"col-1",
"allLabels":[],"balloon":{},"legend":{"align":"center","markerType":"circle"},"titles":[],
"dataProvider":[
{"col-0":"category 1","col-1":8},
{"col-0":"category 2","col-1":6},
{"col-0":"category 3","col-1":2}
]}