<h2 class="am-charts-legend">
<span class="pull-left">MORE TEMPLATES TO START FROM</span>
<span class="pull-right" style="overflow:hidden;">
<iframe style="overflow:hidden;width:85px !important;height:21px !important;" class="social-iframe social-iframe-google" src="https://plusone.google.com/_/+1/fastbutton?url=http%3A%2F%2Flive.amcharts.com%2F&amp;size=medium&amp;count=true&amp;lang=en" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
<iframe style="overflow:hidden;width:100px !important;height:21px !important;" class="social-iframe social-iframe-facebook" src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Flive.amcharts.com%2F&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21&amp;appId=563198717101351" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
<iframe style="overflow:hidden;width:100px !important;height:21px !important;" class="social-iframe social-iframe-twitter" src="//platform.twitter.com/widgets/tweet_button.html?url=http%3A%2F%2Flive.amcharts.com%2F" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
</span>
</h2>
<ul class="am-charts-list"></ul>
<div class="clearfix"></div>
<script type="text/javascript">
    (function() {
        var response  = {"error":false,"charts":[{"title":"","chart_id":"NkMTE"},{"title":"","chart_id":"NjEwN"},{"title":"","chart_id":"c1MGE"},{"title":"","chart_id":"hlOWI"},{"title":"","chart_id":"TY1Yj"},{"title":"","chart_id":"zkyZm"},{"title":"","chart_id":"YjRlZ"},{"title":"","chart_id":"GIyNz"},{"title":"","chart_id":"DdmMD"}]};
        var container = jQuery('.am-charts-list').data('data',{
            o: 0,
            a: 16,
            action: 'receive'
        });

        function drawItems(charts) {
            var data = jQuery(container).data('data');

            jQuery(charts).each(function() {
                var tmpItem   = jQuery('<li>').appendTo(container);
                var tmpLink   = jQuery('<a>').appendTo(tmpItem);
                var tmpFolder = this.chart_id.slice(0,2);
                var tmpImage  = AmCharts.Editor.CONSTANT.URL_BASE + 'static/export/' + tmpFolder + '/' + this.chart_id + '.png';
                var tmpHref   = AmCharts.Editor.CONSTANT.URL_BASE + this.chart_id;

                tmpLink.attr({
                    href: tmpHref + '/edit/'
                }).css({
                    backgroundImage: 'url('+tmpImage+')'
                });
            });

            jQuery(container).data('data',{
                o: data.o + data.a,
                a: data.a
            });
        }
        drawItems(response.charts);

        jQuery('.am-charts-list a').off().on('click',function(e) {
            e.preventDefault();
            // RESET
            AmCharts.Editor.current.chart = {};

            // SOME VISUAL
            AmCharts.Editor.modal('show');

            // CHANGE ROUTE
            jQuery.router.go(this.href);
        });

        var msg    = AmCharts.Editor.getMSG('title_landing_counter');
        var number = String(AmCharts.Editor.CONSTANT.CHARTS_CREATED);
        var letter  = '';
        for(var i = 0; i < number.length; i++) {
            letter += jQuery('<div>').append(jQuery('<span>').text(number[i])).html();
        }

        jQuery('.am-charts-counter').html(msg.replace('[[number]]',letter));
    })(jQuery);
</script>
