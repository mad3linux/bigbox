<i class="am am-remove" data-dismiss="modal"></i>
<div class="modal-body modal-body-templates less-padding">
	<div class="app-menu app-menu-dark">
		<ul class="nav nav-stacked"></ul>
	</div>
	<div class="tab-content" ></div>
</div>

<script type="text/javascript">
	(function() {
		var containerTabs = jQuery(AmCharts.Editor.SELECTORS.modal + ' .nav');
		var containerHTML = jQuery(AmCharts.Editor.SELECTORS.modal + ' .tab-content');

		// APPLY WIDTH
		jQuery(AmCharts.Editor.SELECTORS.modalContent).removeClass(AmCharts.Editor.SELECTORS.modalClasses);
		jQuery(AmCharts.Editor.SELECTORS.modalContent).addClass('modal-lg');
		
		// STICKY MENU
		jQuery(AmCharts.Editor.SELECTORS.modal + ' .modal-body').off().on('scroll',function() {
			jQuery(AmCharts.Editor.SELECTORS.modal + ' .app-menu').css({
				top: this.scrollTop
			});
		});

		// GET USER DATA
		AmCharts.Editor.getUserCharts();

		// WALK THROUGH TEMPLATES
		// console.log(AmCharts.Editor.MAPPING.samples);
		var chr = ["Column","Bar","Line","Area"];
		jQuery(AmCharts.Editor.MAPPING.samples).each(function(gid) {
			// console.log(this);
			if(chr.indexOf(this.name)<0){return true}
			var groupObj		= this;
			var groupID			= 'am-group-' + AmCharts.Editor.variableizeName(this.name);
			var itemLi			= jQuery('<li>').appendTo(containerTabs);
			var itemLink		= jQuery('<a href="#'+groupID+'">').addClass(groupID).appendTo(itemLi);
			var itemIcon		= jQuery('<i class="am pull-left">').appendTo(itemLink);
			var itemTitle		= jQuery('<span>').text(this.name).appendTo(itemLink);
			var itemContainer	= jQuery('<div id="'+groupID+'">').addClass('tab-pane').appendTo(containerHTML);
			var itemList		= jQuery('<ul>').addClass('am-grid').appendTo(itemContainer);
			
			itemIcon.attr('style','background-image: url('+this.img+');');

			// USER EXCEPTION
			if ( gid == 0 ) {
				itemIcon.remove();
				// itemIcon	= jQuery('<span>').addClass('btn btn-flat btn-lightblue pull-right').appendTo(itemLink);
				// itemLabel	= jQuery('<span>').addClass('am-user-charts').appendTo(itemIcon);
				// itemLi.addClass('highlighted');
				// if ( AmCharts.Editor.current.user.name ) {
				// 	itemIcon.addClass('btn-usercharts');
				// 	itemLabel.text(AmCharts.Editor.current.list.total_count || '0').appendTo(itemIcon);
				// } else {
				// 	itemLabel.text(AmCharts.Editor.getMSG('title_sign_in'));
				// 	itemIcon.on('click',function() {
				// 		AmCharts.Editor.modal("hide");
				// 		setTimeout(function() {
				// 			if ( AmCharts.Editor.current.tpl.name ) {
				// 				jQuery('.app-section-editor .am-menu-signin a').trigger('click');
				// 			} else {
				// 				jQuery('.app-section-landing .am-menu-signin a').trigger('click');
				// 			}
				// 		},300);
				// 	});
				// }

			// AUTO SELECT
			} else if ( gid == 1 ) {
				itemLi.addClass('active');
				itemContainer.addClass('active');
			}

			// UNSELECTABLE
			if ( this.disabled ) {
				itemLi.addClass('disabled');
			} else {
				itemLink.attr('data-toggle','tab');
			}

			// UNBIND DEFAULT
			itemLink.on('click',function(e) {
				e.preventDefault();
			});

			// CREATE ITEMS
			// console.log(this.items);
			jQuery(this.items).each(function(tid) {
				var tplLi		= jQuery('<li></li>');
				var tplTitle	= jQuery('<span></span>').appendTo(tplLi).text(this.name);
				this.group		= {
					name: groupObj.name,
					img: groupObj.img
				};

				// UNSELECTABLE
				if ( this.disabled ) {
					itemLi.addClass('disabled');
				}

				// APPLY IMG; APPEND TO LI; OBSERVE CLICK
				tplLi.data('cfg',this).css({
					backgroundImage: 'url('+this.img+')'
				}).appendTo(itemList).on('click',function() {
					var data = jQuery(this).data('cfg');
					//console.log(data);
					data.loaded = false;

					// VISUAL STUFF
					jQuery(this).addClass('loading');

					// RESET; REMEMBER
					AmCharts.Editor.backToDefaults();
					AmCharts.Editor.current.tpl = data;

					// CHANGE ROUTE
					jQuery.router.go('/pivottable/amchart/new/edit/');
				});
			});
		});
	})(jQuery);

</script>