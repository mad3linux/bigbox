<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title">Save HTML</h4>
</div>
<div class="modal-body less-padding" style="height: 600px;">
			<textarea id="html_save" style="display: block; border: none; background: none; width: 100%; height: 100%; padding: 20px;" disabled>
				&lt;meta name=&quot;description&quot; content=&quot;[[description]]&quot; /&gt;[[font]]

				<!-- amCharts javascript sources -->
				[[sources]]

				<!-- amCharts javascript code -->
				&lt;script type=&quot;text/javascript&quot;&gt;
					(function() {
					AmCharts.makeChart(&quot;chartdiv&quot;,
		[[config]]
					);
					})(jQuery);
				&lt;/script&gt;
				&lt;style type="text/css"&gt;
				.chartwrapper {
				  margin: 0px auto;
				  position: relative;
				  padding-bottom: 50%;
				  box-sizing: border-box;
				  background-color: [[css]];
				}

				.chartdiv {
				  position: absolute;
				  width: 100%;
				  height: 100%;
				  background-color: [[css]];
				}
				&lt;/style&gt;
				&lt;div class="chartwrapper"&gt;
				  &lt;div id="chartdiv" class="chartdiv"&gt;&lt;/div&gt;
				&lt;/div&gt;</textarea>

			<textarea id="html_save2" style="display: block; border: none; background: none; width: 100%; height: 100%; padding: 20px;" disabled>
				&lt;meta name=&quot;description&quot; content=&quot;[[description]]&quot; /&gt;[[font]]

				<!-- amCharts javascript sources -->
				[[sources]]

				<!-- amCharts javascript code -->
				&lt;script type=&quot;text/javascript&quot;&gt;
					(function() {
					var chartData = [];
					var chart = AmCharts.makeChart(&quot;chartdiv&quot;,
		[[config]]
					);

					var onprocess = false;
					var url = "[[ds]]";
					var tint = [[tint]];

					function process_objectiveData(data){
						if(data instanceof Array){
							jQuery.each(data,function(key,val){
								chartData.push(val);
							});
							// that.results=data;
						}else if(data instanceof Object){
							jQuery.each(data,function(key,val){
								chartData.push(val);
							});
						}else{
							console.log("UNKNOWN DATA TYPE");
						}
						if (chartData.length > (1*data.length)) {
							chartData.splice(0, chartData.length - (1*data.length));
						}
						chart.validateData();
					}

					function process_plainData(data){
						var results = [];
						var row_start = 0;
						var row_end = "";
						var row=0;var matches=null;
						var delimiter = 'x2C';
						var next={continue:false,break:false}
						var crop={start:false,end:false}
						var objPattern=new RegExp(("(\\"+ delimiter+"|\\r?\\n|\\r|^)"+"(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|"+"([^\"\\"+ delimiter+"\\r\\n]*))"),"gi");
						var buffer=[];results.push([]);
						var resultsType="array";
						while(matches=objPattern.exec(data)){
							var tmpDelimiter=matches[1];var tmpCharCode=String(tmpDelimiter).charCodeAt(0);
							var tmpValue=null;
							if(matches[2]){tmpValue=jQuery.trim(matches[2].replace(/\"\"/g,"\""));}
							else{tmpValue=jQuery.trim(matches[3]);}
							next.continue=Boolean(row_start&&(Number(row_start)>row));
							next.break=Boolean(row_end&&(Number(row_end)<=row));
							if(jQuery.inArray(tmpCharCode,[10,11,13])!=-1){
								if(next.break)break;results.push([]);row++;
							}
							results[results.length-1].push(tmpValue);
							if(crop.start===false&&row_start.length&&!Boolean(Number(row_start)+ 1)&&matches[0].indexOf(row_start)!=-1){crop.start=row;}
							else if(crop.end===false&&row_end.length&&!Boolean(Number(row_end)+ 1)&&matches[0].indexOf(row_end)!=-1){crop.end=row;}
							if(row>2000){break;}
						}
						results=results.slice(crop.start||row_start||0,crop.end||row_end||results.length);
						var totfield = 0;
						var fields = [];
						jQuery(results).each(function(rID,items){
							if(rID==0){	
								totfield=items.length;
							};
							if(totfield==items.length){
								var tmp={};
								chartData.push(items);
							}
						});
						if (chartData.length > (1*results.length)) {
							chartData.splice(0, chartData.length - (1*results.length));
						}
						chart.validateData();
					}

					setInterval(function(){
						if(!onprocess){
							jQuery.ajax({type:"get",url:url,
								beforeSend:function(transport,status){onprocess = true;},
								complete:function(transport,status){
									if(status=="success"){
										var results=transport.responseJSON||transport.responseText;
										try{results=eval('('+ results+')');}catch(e){onprocess = false;}
										if(typeof results==="object"){
											process_objectiveData(results);
										}else if(typeof results==="string" && results!=""){
											process_plainData(results);
										}else{
											console.log("UNKNOWN DATA TYPE");
										}
									}
									onprocess = false;
								}
							});
						}
					}, tint);

					})(jQuery);
				&lt;/script&gt;
				&lt;style type="text/css"&gt;
				.chartwrapper {
				  margin: 0px auto;
				  position: relative;
				  padding-bottom: 50%;
				  box-sizing: border-box;
				  background-color: [[css]];
				}

				.chartdiv {
				  position: absolute;
				  width: 100%;
				  height: 100%;
				  background-color: [[css]];
				}
				&lt;/style&gt;
				&lt;div class="chartwrapper"&gt;
				  &lt;div id="chartdiv" class="chartdiv"&gt;&lt;/div&gt;
				&lt;/div&gt;</textarea>

			<textarea id="html_save3" style="display: block; border: none; background: none; width: 100%; height: 100%; padding: 20px;" disabled>
				&lt;meta name=&quot;description&quot; content=&quot;[[description]]&quot; /&gt;[[font]]

				<!-- amCharts javascript sources -->
				[[sources]]

				<!-- amCharts javascript code -->
				&lt;script type=&quot;text/javascript&quot;&gt;
					(function() {
					var chartData = [];
					var chart = AmCharts.makeChart(&quot;chartdiv&quot;,
		[[config]]
					);

					var url = "[[ds]]";
					var tint = [[tint]];

					function process_objectiveData(data){
						if(data instanceof Array){
							jQuery.each(data,function(key,val){
								chartData.push(val);
							});
							// that.results=data;
						}else if(data instanceof Object){
							jQuery.each(data,function(key,val){
								chartData.push(val);
							});
						}else{
							console.log("UNKNOWN DATA TYPE");
						}
						if (chartData.length > (1*data.length)) {
							chartData.splice(0, chartData.length - (1*data.length));
						}
						chart.validateData();
					}

					function process_plainData(data){
						var results = [];
						var row_start = 0;
						var row_end = "";
						var row=0;var matches=null;
						var delimiter = 'x2C';
						var next={continue:false,break:false}
						var crop={start:false,end:false}
						var objPattern=new RegExp(("(\\"+ delimiter+"|\\r?\\n|\\r|^)"+"(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|"+"([^\"\\"+ delimiter+"\\r\\n]*))"),"gi");
						var buffer=[];results.push([]);
						var resultsType="array";
						while(matches=objPattern.exec(data)){
							var tmpDelimiter=matches[1];var tmpCharCode=String(tmpDelimiter).charCodeAt(0);
							var tmpValue=null;
							if(matches[2]){tmpValue=jQuery.trim(matches[2].replace(/\"\"/g,"\""));}
							else{tmpValue=jQuery.trim(matches[3]);}
							next.continue=Boolean(row_start&&(Number(row_start)>row));
							next.break=Boolean(row_end&&(Number(row_end)<=row));
							if(jQuery.inArray(tmpCharCode,[10,11,13])!=-1){
								if(next.break)break;results.push([]);row++;
							}
							results[results.length-1].push(tmpValue);
							if(crop.start===false&&row_start.length&&!Boolean(Number(row_start)+ 1)&&matches[0].indexOf(row_start)!=-1){crop.start=row;}
							else if(crop.end===false&&row_end.length&&!Boolean(Number(row_end)+ 1)&&matches[0].indexOf(row_end)!=-1){crop.end=row;}
							if(row>2000){break;}
						}
						results=results.slice(crop.start||row_start||0,crop.end||row_end||results.length);
						var totfield = 0;
						var fields = [];
						jQuery(results).each(function(rID,items){
							if(rID==0){	
								totfield=items.length;
							};
							if(totfield==items.length){
								var tmp={};
								chartData.push(items);
							}
						});
						if (chartData.length > (1*results.length)) {
							chartData.splice(0, chartData.length - (1*results.length));
						}
						chart.validateData();
					}

					jQuery.ajax({type:"get",url:url,
						complete:function(transport,status){
							if(status=="success"){
								var results=transport.responseJSON||transport.responseText;
								try{results=eval('('+ results+')');}catch(e){onprocess = false;}
								if(typeof results==="object"){
									process_objectiveData(results);
								}else if(typeof results==="string" && results!=""){
									process_plainData(results);
								}else{
									console.log("UNKNOWN DATA TYPE");
								}
							}
						}
					});

					})(jQuery);
				&lt;/script&gt;
				&lt;style type="text/css"&gt;
				.chartwrapper {
				  margin: 0px auto;
				  position: relative;
				  padding-bottom: 50%;
				  box-sizing: border-box;
				  background-color: [[css]];
				}

				.chartdiv {
				  position: absolute;
				  width: 100%;
				  height: 100%;
				  background-color: [[css]];
				}
				&lt;/style&gt;
				&lt;div class="chartwrapper"&gt;
				  &lt;div id="chartdiv" class="chartdiv"&gt;&lt;/div&gt;
				&lt;/div&gt;</textarea>
</div>
<div class="modal-footer less-margin">
	<!--a href="#am-data-export-copy" class="btn btn-default" data-clipboard-target="am-data-export-html">Copy to clipboard</a-->
	<a href="#am-data-export-save" target="_blank" type="button" class="btn btn-primary" download="amcharts.editor.html">Save to filesystem</a>
</div>
<script type="text/javascript">
	(function() {
		// console.log(this);
		var input		= jQuery(AmCharts.Editor.SELECTORS.modal).find('textarea#html_save');
		var input2		= jQuery(AmCharts.Editor.SELECTORS.modal).find('textarea#html_save2');
		var input3		= jQuery(AmCharts.Editor.SELECTORS.modal).find('textarea#html_save3');
		var tpl			= input.val();
		var cfg = JSON.stringify(AmCharts.Editor.current.cfg,undefined,'\t');
		var srcs		= ['http://localhost/live.amcharts.com/vendor/amcharts/amcharts.js'];
		var font		= "";
		var title		= AmCharts.Editor.current.chart.title || AmCharts.Editor.current.tpl.name;
		var description	= AmCharts.Editor.current.chart.description || '';
		var plugins		= [];

		// ADD CORRECT TAB INTEND
		cfg			= '\t\t\t\t' + cfg.replace(/\n/g, '\n\t\t\t\t');
		cfg = JSON.parse(cfg);
		// console.log(cfg);

		var isloop = jQuery(AmCharts.Editor.SELECTORS.table).parent().find("#am-datasource").find("#loop-ds").is(":checked");
		// console.log(isloop);
		if(AmCharts.Editor.CONSTANT.ISHTTP){
			cfg.dataProvider = '[[dprov]]';
			if(!isloop){
				tpl	= input3.val();
			}else{
				tpl	= input2.val();
			}
		}
		// console.log(cfg);
		cfg = JSON.stringify(cfg,undefined,'\t');
		cfg = cfg.replace('"[[dprov]]"',"chartData");
		// console.log(cfg);

		// APPLY THEME FONT AND TRY TO SIMPLY GATHER IT FROM GOOGLE
		if ( AmCharts.Editor.chart.theme && AmCharts.Editor.chart.theme.AmChart.fontFamily ) {
			font = "\n\n\t\t<!-- amCharts custom font -->\n\t\t<link href='http://fonts.googleapis.com/css?family=[[fontfamily]]' rel='stylesheet' type='text/css'>".replace('[[fontfamily]]',AmCharts.Editor.chart.theme.AmChart.fontFamily.replace(/ /g,'+'));
		}

		// GATHER SOURCES
			// console.log(AmCharts.Editor.current.tpl.srcs);
		jQuery.merge(srcs,AmCharts.Editor.current.tpl.srcs || []);

		// ADD THEME IF APPLIED
		if ( AmCharts.Editor.chartConfig.theme ) {
			srcs.push('http://localhost/live.amcharts.com/vendor/amcharts/themes/'+ AmCharts.Editor.chartConfig.theme.themeName +'.js')
		}
		if ( AmCharts.Editor.chart["export"] ) {
			srcs.push('http://localhost/live.amcharts.com/vendor/amcharts/plugins/export/export.js');
			srcs.push('http://localhost/live.amcharts.com/vendor/amcharts/plugins/export/export.css');
		}

		// CREATE SCRIPT TAGS AND INTERPOLATE WITH TPL
		var js = srcs;
		srcs = jQuery(srcs).map(function() {
			var resource = this;
			// console.log(resource);
			if ( resource.indexOf('http:') == -1 ) {
				resource = "http:" + resource;
			}

			// SCRIPT
			if ( resource.indexOf(".js") != -1 ) {
				var script = jQuery('<script>').attr('type','text/javascript').attr('src',resource);
				return jQuery('<div>').append(script).html();

			// CSS
			} else if ( resource.indexOf(".css") != -1 ) {
				var script = jQuery('<link>').attr('rel','stylesheet').attr('href',resource);
				return jQuery('<div>').append(script).html();
			}
			
		//}).get().reverse().join('\n\t\t');
		});
		srcs = srcs.get();
		srcs = srcs.join('\n\t\t');
		tpl = tpl.replace('[[title]]',title);
		tpl = tpl.replace('[[description]]',description);
		tpl = tpl.replace('[[sources]]',srcs);
		tpl = tpl.replace('[[config]]',cfg);
		tpl = tpl.replace('[[css]]',AmCharts.Editor.chart.backgroundColor);
		tpl = tpl.replace('[[font]]',font);
		if(AmCharts.Editor.CONSTANT.ISHTTP){
			tpl = tpl.replace('[[ds]]',AmCharts.Editor.CONSTANT.DATASOURCE);
			tpl = tpl.replace('[[tint]]',"10000");
			if(!isloop){
				input3.val(tpl);
				input3.removeClass("hidden");
				input2.addClass("hidden");
			}else{
				input2.val(tpl);
				input2.removeClass("hidden");
				input3.addClass("hidden");
			}
			input.addClass("hidden");
		}else{
			input.val(tpl);
			input.removeClass("hidden");
			input2.addClass("hidden");
			input3.addClass("hidden");
		}

		// input.val(tpl);

		// CLIPBOARD; THROWS AN ERROR BUT IT WORKS
		// new ZeroClipboard(jQuery('.modal .btn-default')).on('mouseover mouseout',function(e) {
		// 	jQuery(e.target).attr('aria-label',AmCharts.Editor.getMSG('tooltip_clipboard_copy'))[e.type=='mouseover'?'addClass':'removeClass']('cssTooltip');
		// }).on('copy',function(e) {
		// 	jQuery(e.target).attr('aria-label',AmCharts.Editor.getMSG('tooltip_clipboard_copied'));
		// });

		// APPLY CONTENT TO DOWNLOAD LINK
		jQuery(AmCharts.Editor.SELECTORS.modal).find('.btn-primary').attr('href','data:text/html;charset=utf-8,' + encodeURIComponent(tpl));
	})(jQuery);
</script>
