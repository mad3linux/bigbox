<header class="app-header app-header-site">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<nav class="app-nav navbar navbar-amcharts" role="navigation">
					<div class="app-nav-brand">
						<div class="app-nav-brand-inner">
							<a href="https://www.amcharts.com">
								<img src="//live.amcharts.com/static/img/page/logo_site.png" title="JavaScript charts and maps" alt="">
							</a>
						</div>
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<ul class="nav navbar-nav navbar-right navbar-signin">
						<li class="am-login-label">
							<span>Want to see your saved charts?</span>
						</li>
						<li class="am-menu-signin dropdown">
							<a href="/signin/" class="dropdown-toggle" data-toggle="dropdown">
								<span class="btn btn-flat btn-lightblue">
									<span>Sign In</span>
								</span>
							</a>
							<ul class="dropdown-menu dropdown-menu-right"></ul>
						</li>
					</ul>
					<ul class="nav navbar-nav navbar-right navbar-user hidden">
						<li class="am-login-label disabled">
							<span>Logged in as</span>
						</li>
						<li class="am-menu-user dropdown">
							<a href="/user/" class="dropdown-toggle" data-toggle="dropdown">
								<span class="am-user-name">Mr. Who</span>
								<span class="btn btn-flat btn-lightblue btn-usercharts">
									<span class="am-user-charts">0</span>
								</span>
							</a>
							<ul class="dropdown-menu dropdown-menu-right"></ul>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</header>
<div class="app-landing">
	<div class="app-landing-header">
		<div class="container full-height">
			<div class="row full-height">
				<div class="col-sm-12 full-height text-center">
					<h1>Live Editor - Make your charts online</h1>
					<span class="am-charts-counter"></span>
					<div>
						<a href="#" class="btn btn-flat btn-lightblue btn-makechart">
							<span>Make a <strong>Chart</strong></span>
						</a>
						<a href="http://pixelmap.amcharts.com" target="pixelmap" class="btn btn-pixelmap" title="Pixel Map Generator | amCharts">
							<span>Need a map?<br />Try <strong>Pixel Map Generator</strong></span>
						</a>
						<a href="#" class="btn btn-flat btn-lightblue btn-upgrade hidden">
							<span>We are sorry, but you need a modern browser to use the editor</span>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="app-landing-cover"></div>
	</div>
	<div class="app-landing-body">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="app-landing-usercharts"></div>
					<div class="app-landing-publiccharts"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="app-landing-footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div>
					<div>
						<h3>Products</h3>
						<ul>
							<li><a href="//www.amcharts.com/javascript-charts/">JavaScript Charts</a></li>
							<li><a href="//www.amcharts.com/stock-chart/">Javascript Stock Chart</a></li>
							<li><a href="//www.amcharts.com/javascript-maps/">Interactive Javascript Maps</a></li>
						</ul>
					</div>
				</div>
				<div>
					<div>
						<h3>Related</h3>
						<ul>
							<li><a href="//www.amcharts.com/visited_countries/">Visited Countries Map</a></li>
							<li><a href="//www.amcharts.com/visited_states/">Visited States Map</a></li>
							<li><a href="//www.amcharts.com/about/">About</a></li>
						</ul>
					</div>
				</div>
				<div>
					<div>
						<h3>Get Social</h3>
						<ul>
							<li><a href="//www.facebook.com/amcharts"><i class="fa fa-facebook-square color-gray"></i> Facebook</a></li>
							<li><a href="//www.twitter.com/amcharts"><i class="fa fa-twitter color-gray"></i>  Twitter</a></li>
							<li><a href="//plus.google.com/+amcharts/"><i class="fa fa-google-plus-square color-gray"></i> Google+</a></li>
						</ul>
					</div>
				</div>
				<div>
					<div>
						<h3>Resources</h3>
						<ul>
							<li><a href="//www.amcharts.com/inspiration/">Inspiration Gallery</a></li>
							<li><a href="//www.amcharts.com/svg-maps/">Free SVG Maps</a></li>
							<li><a href="//feeds.feedburner.com/amcharts">RSS feed of our blog</a></li>
							<li><a href="//www.amcharts.com/terms-conditions/">Terms &amp; Conditions</a></li>
						</ul>
					</div>
				</div>
				<div>
					<div>
						<h3>Contact Us</h3>
						<ul>
							<li><a href="mailto:contact@amcharts.com">contact@amcharts.com</a></li>
							<li><a href="//www.amcharts.com/support/">Get Support</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="app-fieldset text-center">
					<div class="app-fieldset-legend">Copyright © 2014, amcharts.com. All rights reserved.</div>
				</div>
			</div>
		</div>
	</div>
	<div class="app-wave wave-light"></div>
</div>