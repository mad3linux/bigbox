{"type":"serial","categoryField":"col-0","startDuration":1,"categoryAxis":{"gridPosition":"start"},"trendLines":[],
"graphs":[
{"balloonText":"[[title]] of [[category]]:[[value]]","fillAlphas":0.7,"id":"AmGraph-1","lineAlpha":0,"title":"graph 1","valueField":"col-1"},
{"balloonText":"[[title]] of [[category]]:[[value]]","fillAlphas":0.7,"id":"AmGraph-2","lineAlpha":0,"title":"graph 2","valueField":"col-2"}],"guides":[],
"valueAxes":[{"id":"ValueAxis-1","title":"Axis title"}],"allLabels":[],"balloon":{},"legend":{},"titles":[{"id":"Title-1","size":15,"text":"Chart Title"}],
"dataProvider":[
{"col-0":"category 1","col-1":8,"col-2":5},
{"col-0":"category 2","col-1":6,"col-2":7},
{"col-0":"category 3","col-1":2,"col-2":3},
{"col-0":"category 4","col-1":1,"col-2":3},
{"col-0":"category 5","col-1":2,"col-2":1},
{"col-0":"category 6","col-1":3,"col-2":2},
{"col-0":"category 7","col-1":6,"col-2":8}
]}