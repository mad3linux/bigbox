{"type":"serial","categoryField":"col-0","dataDateFormat":"YYYY-MM-DD HH:NN","categoryAxis":{"minPeriod":"mm","parseDates":true},
"chartCursor":{"categoryBalloonDateFormat":"JJ:NN"},"chartScrollbar":{},"trendLines":[],
"graphs":[
{"fillAlphas":0.7,"id":"AmGraph-1","lineAlpha":0,"title":"graph 1","valueField":"col-1"},
{"fillAlphas":0.7,"id":"AmGraph-2","lineAlpha":0,"title":"graph 2","valueField":"col-2"}],"guides":[],
"valueAxes":[{"id":"ValueAxis-1","title":"Axis title"}],"allLabels":[],"balloon":{},"legend":{},
"titles":[{"id":"Title-1","size":15,"text":"Chart Title"}],
"dataProvider":[
{"col-1":8,"col-2":5,"col-0":"2014-03-01 07:57"},
{"col-1":6,"col-2":7,"col-0":"2014-03-01 07:58"},
{"col-1":2,"col-2":3,"col-0":"2014-03-01 07:59"},
{"col-1":1,"col-2":3,"col-0":"2014-03-01 08:00"},
{"col-1":2,"col-2":1,"col-0":"2014-03-01 08:01"},
{"col-1":3,"col-2":2,"col-0":"2014-03-01 08:02"},
{"col-1":6,"col-2":8,"col-0":"2014-03-01 08:03"}
]}