{"categoryField":"col-0","rotate":true,"startDuration":1,"categoryAxis":{"gridPosition":"start"},"trendLines":[],
"graphs":[
{"balloonText":"[[title]] of [[category]]:[[value]]","fillAlphas":1,"id":"AmGraph-1","labelText":"[[value]]","title":"graph 1","type":"column","valueField":"col-1"},
{"balloonText":"[[title]] of [[category]]:[[value]]","bullet":"round","id":"AmGraph-2","labelText":"[[value]]","lineThickness":2,"title":"graph 2","valueField":"col-2"}
],"guides":[],"valueAxes":[{"id":"ValueAxis-1","position":"left","title":""}],"allLabels":[],"balloon":{},"legend":{"useGraphSettings":true},
"titles":[{"id":"Title-1","size":15,"text":"Chart Title"}],"type":"serial",
"dataProvider":[
	{"col-0":"category 1","col-1":8,"col-2":5},
	{"col-0":"category 2","col-1":6,"col-2":7},
	{"col-0":"category 3","col-1":2,"col-2":3}
]}