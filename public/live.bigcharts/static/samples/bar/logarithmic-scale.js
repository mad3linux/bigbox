{"categoryField":"col-0","rotate":true,"startDuration":1,"categoryAxis":{"gridPosition":"start"},"trendLines":[],
"graphs":[{"fillAlphas":1,"id":"AmGraph-1","title":"graph 1","type":"column","valueField":"col-1"}],"guides":[],
"valueAxes":[{"id":"ValueAxis-1","logarithmic":true,"position":"left","title":"Axis title"}],"allLabels":[],"balloon":{},
"titles":[{"id":"Title-1","size":15,"text":"Chart Title"}],"type":"serial",
"dataProvider":[
{"col-0":"category 1","col-1":8},{"col-0":"category 2","col-1":160},{"col-0":"category 3","col-1":989},
{"col-0":"category 4","col-1":1560},{"col-0":"category 5","col-1":5006},{"col-0":"category 6","col-1":9012},{"col-0":"category 7","col-1":40124},
{"col-0":"category 8","col-1":150200}
]}