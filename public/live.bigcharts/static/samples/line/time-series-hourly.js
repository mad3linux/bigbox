{"type":"serial","categoryField":"col-0","dataDateFormat":"YYYY-MM-DD HH","categoryAxis":{"minPeriod":"hh","parseDates":true},
"chartCursor":{"categoryBalloonDateFormat":"JJ:NN"},"chartScrollbar":{},"trendLines":[],
"graphs":[
{"bullet":"round","id":"AmGraph-1","title":"graph 1","valueField":"col-1"},
{"bullet":"square","id":"AmGraph-2","title":"graph 2","valueField":"col-2"}],"guides":[],"valueAxes":[{"id":"ValueAxis-1","position":"left","title":"Axis title"}],
"allLabels":[],"balloon":{},"legend":{"useGraphSettings":true},"titles":[{"id":"Title-1","size":15,"text":"Chart Title"}],
"dataProvider":[
{"col-1":8,"col-2":5,"col-0":"2014-03-01 08"},
{"col-1":6,"col-2":7,"col-0":"2014-03-01 09"},
{"col-1":2,"col-2":3,"col-0":"2014-03-01 10"},
{"col-1":1,"col-2":3,"col-0":"2014-03-01 11"},
{"col-1":2,"col-2":1,"col-0":"2014-03-01 12"},
{"col-1":3,"col-2":2,"col-0":"2014-03-01 13"},
{"col-1":6,"col-2":8,"col-0":"2014-03-01 14"}
]}