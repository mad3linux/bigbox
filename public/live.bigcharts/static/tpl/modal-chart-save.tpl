<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title">Save Chart</h4>
</div>
<div class="modal-body">
	<form role="form">
		<fieldset>
			<div class="form-group">
				<label class="form-input">TITLE</label>
				<input type="text" class="form-control" name="title" placeholder="Title">
			</div>
			<div class="form-group">
				<label class="form-input">DESCRIPTION</label>
				<textarea class="form-control" name="description" rows="3" placeholder="Description"></textarea>
			</div>
			<div class="form-group">
				<input id="save_ds" type="text" class="form-control hidden" name="save_ds" placeholder="Datasource" disabled>
				<input id="save_ds2" type="hidden" class="form-control hidden" name="save_ds2">
			</div>
			<div class="form-group">
				<label class="form-input">INTERVAL RELOAD (ms)</label>
				<input id="int_save" type="number" class="form-control hidden" name="int_save" placeholder="Auto Reload">
			</div>
			<!-- <div class="checkbox"> -->
				<!-- <label> -->
				  <!-- <input type="checkbox" name="public" value="1" checked> Public -->
				<!-- </label> -->
			<!-- </div> -->
			<textarea id="htmldiv" style="border: none; background: none; width: 0px; height: 0px; padding: 0px;">
				&lt;meta name=&quot;description&quot; content=&quot;[[description]]&quot; /&gt;[[font]]
				&lt;style type="text/css"&gt;
				.chartwrapper {
				  margin: 0px auto;
				  position: relative;
				  padding-bottom: 50%;
				  box-sizing: border-box;
				  background-color: [[css1]];
				}

				.chartdiv {
				  position: absolute;
				  width: 100%;
				  height: 100%;
				  background-color: [[css2]];
				}
				&lt;/style&gt;
				&lt;div class="chartwrapper"&gt;
				  &lt;div id="chartdiv{{pid}}" class="chartdiv"&gt;&lt;/div&gt;
				&lt;/div&gt;</textarea>
			<textarea id="html_save" style="border: none; background: none; width: 0px; height: 0px; padding: 0px;">(function() {
					var amchart{{pid}} = AmCharts.makeChart(&quot;chartdiv{{pid}}&quot;,
		[[config]]
					);
					})(jQuery);</textarea>

			<textarea id="html_save2" style="border: none; background: none; width: 0px; height: 0px; padding: 0px;">(function() {
					var chartData = [];
					var amchart{{pid}} = AmCharts.makeChart(&quot;chartdiv{{pid}}&quot;,
		[[config]]
					);

					var onprocess = false;
					var url = "[[ds]]";
					var tint = [[tint]];

					function process_objectiveData(data){
						if(data instanceof Array){
							jQuery.each(data,function(key,val){
								chartData.push(val);
							});
							// that.results=data;
						}else if(data instanceof Object){
							jQuery.each(data,function(key,val){
								chartData.push(val);
							});
						}else{
							console.log("UNKNOWN DATA TYPE");
						}
						if (chartData.length > (1*data.length)) {
							chartData.splice(0, chartData.length - (1*data.length));
						}
						amchart{{pid}}.validateData();
					}

					function process_plainData(data){
						var results = [];
						var row_start = 0;
						var row_end = "";
						var row=0;var matches=null;
						var delimiter = 'x2C';
						var next={continue:false,break:false}
						var crop={start:false,end:false}
						var objPattern=new RegExp(("(\\"+ delimiter+"|\\r?\\n|\\r|^)"+"(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|"+"([^\"\\"+ delimiter+"\\r\\n]*))"),"gi");
						var buffer=[];results.push([]);
						var resultsType="array";
						while(matches=objPattern.exec(data)){
							var tmpDelimiter=matches[1];var tmpCharCode=String(tmpDelimiter).charCodeAt(0);
							var tmpValue=null;
							if(matches[2]){tmpValue=jQuery.trim(matches[2].replace(/\"\"/g,"\""));}
							else{tmpValue=jQuery.trim(matches[3]);}
							next.continue=Boolean(row_start&&(Number(row_start)>row));
							next.break=Boolean(row_end&&(Number(row_end)<=row));
							if(jQuery.inArray(tmpCharCode,[10,11,13])!=-1){
								if(next.break)break;results.push([]);row++;
							}
							results[results.length-1].push(tmpValue);
							if(crop.start===false&&row_start.length&&!Boolean(Number(row_start)+ 1)&&matches[0].indexOf(row_start)!=-1){crop.start=row;}
							else if(crop.end===false&&row_end.length&&!Boolean(Number(row_end)+ 1)&&matches[0].indexOf(row_end)!=-1){crop.end=row;}
							if(row>2000){break;}
						}
						results=results.slice(crop.start||row_start||0,crop.end||row_end||results.length);
						var totfield = 0;
						var fields = [];
						jQuery(results).each(function(rID,items){
							if(rID==0){	
								totfield=items.length;
							};
							if(totfield==items.length){
								var tmp={};
								chartData.push(items);
							}
						});
						if (chartData.length > (1*results.length)) {
							chartData.splice(0, chartData.length - (1*results.length));
						}
						amchart{{pid}}.validateData();
					}

					jQuery.ajax({type:"get",url:url,
						beforeSend:function(transport,status){onprocess = true;},
						complete:function(transport,status){
							if(status=="success"){
								var results=transport.responseJSON||transport.responseText;
								try{results=eval('('+ results+')');}catch(e){onprocess = false;}
								if(typeof results==="object"){
									process_objectiveData(results);
								}else if(typeof results==="string" && results!=""){
									process_plainData(results);
								}else{
									console.log("UNKNOWN DATA TYPE");
								}
							}
							onprocess = false;
						}
					});
					
					setInterval(function(){
						if(!onprocess){
							jQuery.ajax({type:"get",url:url,
								beforeSend:function(transport,status){onprocess = true;},
								complete:function(transport,status){
									if(status=="success"){
										var results=transport.responseJSON||transport.responseText;
										try{results=eval('('+ results+')');}catch(e){onprocess = false;}
										if(typeof results==="object"){
											process_objectiveData(results);
										}else if(typeof results==="string" && results!=""){
											process_plainData(results);
										}else{
											console.log("UNKNOWN DATA TYPE");
										}
									}
									onprocess = false;
								}
							});
						}
					}, tint);

					})(jQuery);</textarea>

			<textarea id="html_save3" style="border: none; background: none; width: 0px; height: 0px; padding: 0px;">(function() {
					var chartData = [];
					var amchart{{pid}} = AmCharts.makeChart(&quot;chartdiv{{pid}}&quot;,
		[[config]]
					);

					var url = "[[ds]]";
					var tint = [[tint]];

					function process_objectiveData(data){
						if(data instanceof Array){
							jQuery.each(data,function(key,val){
								chartData.push(val);
							});
							// that.results=data;
						}else if(data instanceof Object){
							jQuery.each(data,function(key,val){
								chartData.push(val);
							});
						}else{
							console.log("UNKNOWN DATA TYPE");
						}
						if (chartData.length > (1*data.length)) {
							chartData.splice(0, chartData.length - (1*data.length));
						}
						amchart{{pid}}.validateData();
					}

					function process_plainData(data){
						var results = [];
						var row_start = 0;
						var row_end = "";
						var row=0;var matches=null;
						var delimiter = 'x2C';
						var next={continue:false,break:false}
						var crop={start:false,end:false}
						var objPattern=new RegExp(("(\\"+ delimiter+"|\\r?\\n|\\r|^)"+"(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|"+"([^\"\\"+ delimiter+"\\r\\n]*))"),"gi");
						var buffer=[];results.push([]);
						var resultsType="array";
						while(matches=objPattern.exec(data)){
							var tmpDelimiter=matches[1];var tmpCharCode=String(tmpDelimiter).charCodeAt(0);
							var tmpValue=null;
							if(matches[2]){tmpValue=jQuery.trim(matches[2].replace(/\"\"/g,"\""));}
							else{tmpValue=jQuery.trim(matches[3]);}
							next.continue=Boolean(row_start&&(Number(row_start)>row));
							next.break=Boolean(row_end&&(Number(row_end)<=row));
							if(jQuery.inArray(tmpCharCode,[10,11,13])!=-1){
								if(next.break)break;results.push([]);row++;
							}
							results[results.length-1].push(tmpValue);
							if(crop.start===false&&row_start.length&&!Boolean(Number(row_start)+ 1)&&matches[0].indexOf(row_start)!=-1){crop.start=row;}
							else if(crop.end===false&&row_end.length&&!Boolean(Number(row_end)+ 1)&&matches[0].indexOf(row_end)!=-1){crop.end=row;}
							if(row>2000){break;}
						}
						results=results.slice(crop.start||row_start||0,crop.end||row_end||results.length);
						var totfield = 0;
						var fields = [];
						jQuery(results).each(function(rID,items){
							if(rID==0){	
								totfield=items.length;
							};
							if(totfield==items.length){
								var tmp={};
								chartData.push(items);
							}
						});
						if (chartData.length > (1*results.length)) {
							chartData.splice(0, chartData.length - (1*results.length));
						}
						amchart{{pid}}.validateData();
					}

					jQuery.ajax({type:"get",url:url,
						complete:function(transport,status){
							if(status=="success"){
								var results=transport.responseJSON||transport.responseText;
								try{results=eval('('+ results+')');}catch(e){onprocess = false;}
								if(typeof results==="object"){
									process_objectiveData(results);
								}else if(typeof results==="string" && results!=""){
									process_plainData(results);
								}else{
									console.log("UNKNOWN DATA TYPE");
								}
							}
						}
					});

					})(jQuery);</textarea>
		</fieldset>
	</form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-primary">Save</button>
</div>

<script type="text/javascript">
	(function () {
		var form = jQuery(AmCharts.Editor.SELECTORS.modal).find('form')[0];

		// APPLY WIDTH
		jQuery(AmCharts.Editor.SELECTORS.modalContent).removeClass(AmCharts.Editor.SELECTORS.modalClasses);
		jQuery(AmCharts.Editor.SELECTORS.modalContent).addClass('modal-sm');
		
		// POLYFILL FORM
		form.title.value		= AmCharts.Editor.current.chart.title || AmCharts.Editor.current.tpl.name || formsave.title;
		form.description.value	= AmCharts.Editor.current.chart.description || formsave.description || '';
		form.int_save.value	= formsave.int_save || '10000';

		//if ( AmCharts.Editor.current.chart.chart_id ) {
		//	form.public.checked		= AmCharts.Editor.current.chart.public;
		//}
		
		if(AmCharts.Editor.CONSTANT.DATASOURCE!=""){
			// $("#save_ds").removeClass("hidden");
			$("#save_ds").val(AmCharts.Editor.CONSTANT.DATASOURCE);
			$("#save_ds2").val(AmCharts.Editor.CONSTANT.DATASOURCE);
		}else{
			$("#save_ds").addClass("hidden");
			$("#save_ds").val("");
		}

		if(AmCharts.Editor.CONSTANT.ISHTTP){
			$("#int_save").removeClass("hidden");
		}else{
			$("#int_save").addClass("hidden");
			$("#int_save").val("");
		}
		// console.log(js);
		// console.log(plugins);
		// console.log(cfg);

		// SAVE
		jQuery(AmCharts.Editor.SELECTORS.modal).find('.btn-primary').on('click',function() {
			var htmldiv		= jQuery(AmCharts.Editor.SELECTORS.modal).find('textarea#htmldiv');
			var input		= jQuery(AmCharts.Editor.SELECTORS.modal).find('textarea#html_save');
			var input2		= jQuery(AmCharts.Editor.SELECTORS.modal).find('textarea#html_save2');
			var input3		= jQuery(AmCharts.Editor.SELECTORS.modal).find('textarea#html_save3');
			var tpl			= input.val();
			var tpl2		= htmldiv.val();
			var cfg = JSON.stringify(AmCharts.Editor.current.cfg,undefined,'\t');
			var srcs		= ['/live.bigcharts/vendor/amcharts/amcharts.js'];
			var font		= "";
			var title		= AmCharts.Editor.current.chart.title || AmCharts.Editor.current.tpl.name;
			var description	= AmCharts.Editor.current.chart.description || '';
			var plugins		= [];

			// ADD CORRECT TAB INTEND
			cfg	= '\t\t\t\t' + cfg.replace(/\n/g, '\n\t\t\t\t');
			cfg = JSON.parse(cfg);
			// console.log(cfg);

			if(AmCharts.Editor.CONSTANT.ISHTTP){
				cfg.dataProvider = '[[dprov]]';
				if(form.int_save.value<3000){
					tpl	= input3.val();
				}else{
					tpl	= input2.val();
				}
			}
			// console.log(cfg);
			cfg = JSON.stringify(cfg,undefined,'\t');
			cfg = cfg.replace('"[[dprov]]"',"chartData");
			// console.log(cfg);

			// APPLY THEME FONT AND TRY TO SIMPLY GATHER IT FROM GOOGLE
			if ( AmCharts.Editor.chart.theme && AmCharts.Editor.chart.theme.AmChart.fontFamily ) {
				font = "\n\n\t\t<!-- amCharts custom font -->\n\t\t<link href='http://fonts.googleapis.com/css?family=[[fontfamily]]' rel='stylesheet' type='text/css'>".replace('[[fontfamily]]',AmCharts.Editor.chart.theme.AmChart.fontFamily.replace(/ /g,'+'));
			}

			// GATHER SOURCES
				// console.log(AmCharts.Editor.current.tpl.srcs);
			jQuery.merge(srcs,AmCharts.Editor.current.tpl.srcs || []);

			// ADD THEME IF APPLIED
			if ( AmCharts.Editor.chartConfig.theme ) {
				srcs.push('/live.bigcharts/vendor/amcharts/themes/'+ AmCharts.Editor.chartConfig.theme.themeName +'.js')
			}
			if ( AmCharts.Editor.chart["export"] ) {
				srcs.push('/live.bigcharts/vendor/amcharts/plugins/export/export.js');
				srcs.push('/live.bigcharts/vendor/amcharts/plugins/export/export.css');
			}

			// CREATE SCRIPT TAGS AND INTERPOLATE WITH TPL
			var js = srcs;
			srcs = jQuery(srcs).map(function() {
				var resource = this;
				// console.log(resource);
				if ( resource.indexOf('http:') == -1 ) {
					resource = "http:" + resource;
				}

				// SCRIPT
				if ( resource.indexOf(".js") != -1 ) {
					var script = jQuery('<script>').attr('type','text/javascript').attr('src',resource);
					return jQuery('<div>').append(script).html();

				// CSS
				} else if ( resource.indexOf(".css") != -1 ) {
					var script = jQuery('<link>').attr('rel','stylesheet').attr('href',resource);
					return jQuery('<div>').append(script).html();
				}
				
			//}).get().reverse().join('\n\t\t');
			});
			srcs = srcs.get();
			srcs = srcs.join('\n\t\t');
			tpl = tpl.replace('[[title]]',title);
			tpl = tpl.replace('[[description]]',description);
			tpl2 = tpl2.replace('[[description]]',description);
			tpl = tpl.replace('[[sources]]',srcs);
			tpl = tpl.replace('[[config]]',cfg);
			// console.log(AmCharts.Editor.chart.backgroundColor);
			tpl = tpl.replace('[[css1]]',AmCharts.Editor.chart.backgroundColor);
			tpl = tpl.replace('[[css2]]',AmCharts.Editor.chart.backgroundColor);
			tpl = tpl.replace('[[font]]',font);
			tpl2 = tpl2.replace('[[css1]]',AmCharts.Editor.chart.backgroundColor);
			tpl2 = tpl2.replace('[[css2]]',AmCharts.Editor.chart.backgroundColor);
			tpl2 = tpl2.replace('[[font]]',font);
			var chartid = AmCharts.Editor.current.chart.chart_id || PRABAC_pid || '{{pid}}';
			if(AmCharts.Editor.CONSTANT.ISHTTP){
				tpl = tpl.replace('[[ds]]',"/bigchart/getamchart/id/"+chartid);
				// tpl = tpl.replace('[[ds]]',form.save_ds.value);
				tpl = tpl.replace('[[tint]]',form.int_save.value);
				if(form.int_save.value<3000){
					input3.val(tpl);
				}else{
					input2.val(tpl);
				}
			}else{
				input.val(tpl);
			}

			// console.log(tpl);
			
			_chart = JSON.stringify(AmCharts.Editor.current.cfg);
			_chart = JSON.parse(_chart);
			// _chart.categoryAxis.labelFunction = {};
			_chart.dataProvider = [];
			_chart = JSON.stringify(_chart);
			// _chart = _chart.replace('"labelFunction":{}','"labelFunction":function( label, item ) {var lbl="";if(item!=undefined){lbl = String(item.dataContext.columns);}return lbl;}');
			//console.log(_chart);
			
			var parameters	= {
				action		: 'amcharts_editor_save_chart',
				chart_id	: chartid,
				type		: AmCharts.Editor.chartType,
				code		: _chart,
				js			: JSON.stringify(js),
				plugins		: JSON.stringify(plugins),
				template	: JSON.stringify(AmCharts.Editor.current.tpl),
				css	: JSON.stringify(AmCharts.Editor.current.css),
				form_api	: JSON.stringify(formapi),
				tpl : tpl,
				tpl2 : tpl2
			}

			// MERGE FORM FIELDS
			jQuery.each(jQuery(form).serializeArray(), function() {
				parameters[this.name] = this.value;
			});

			//console.log(parameters);
			//return false;
			// DISABLE FORM AND PUSH IT OUT
			jQuery(AmCharts.Editor.SELECTORS.modal).find('fieldset').attr('disabled',true);
			jQuery(AmCharts.Editor.SELECTORS.modal).find('.btn').addClass('disabled');
			//console.log(parameters);
			//return false;
			AmCharts.Editor.ajax({
				data: parameters,
				complete: function(transport) {
					//console.log(transport);
					//return false;
					var response = transport.responseJSON || { error: true };

					response.public = parameters.public=='1';
					// jQuery.extend(AmCharts.Editor.current.chart,response);

					if ( !response.error ) {
						AmCharts.Editor.changedConfig(true); // Recache
						AmCharts.Editor.current.chart.title = form.title.value;
						AmCharts.Editor.current.chart.description = form.description.value;

						// GET USER CHARTS
						// AmCharts.Editor.getUserCharts();
						
						// SOME VISUALS
						jQuery(AmCharts.Editor.SELECTORS.modal).find('.btn-primary').addClass('btn-success');
						setTimeout(function() {
							// CHANGE ROUTE
							//jQuery.router.go('/bigchart/editor/pid/'+response.id);
							window.location.replace('/bigchart/editor/pid/'+response.id);
							AmCharts.Editor.modal("hide");
						},1000);
					} else {
						var msg = response.message || 'unexpected response';
						AmCharts.Editor.log('error',msg);
						jQuery(AmCharts.Editor.SELECTORS.modal).find('.btn-primary').addClass('btn-danger').tooltip({
							title: msg,
						}).tooltip('show');
					}
				}
			});
		});
	})(jQuery);

</script>