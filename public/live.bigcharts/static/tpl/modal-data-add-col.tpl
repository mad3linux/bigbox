<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title">Create data column</h4>
</div>
<div class="modal-body">
	<form role="form">
		<fieldset>
			<div class="form-group">
				<input type="text" class="form-control" name="title" placeholder="Title">
			</div>
			<div class="checkbox">
				<label>
				  <input type="checkbox" name="data_rnd" value="1" checked> Generate random values
				</label>
			</div>
			<div class="form-group">
				<input type="number" class="form-control" name="data_min" value="0" placeholder="Min">
			</div>
			<div class="form-group">
				<input type="number" class="form-control" name="data_max" value="100" placeholder="Max">
			</div>
		</fieldset>
	</form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default">Add</button>
	<button type="button" class="btn btn-primary hidden">Add with graph</button>
</div>

<script type="text/javascript">

	(function() {
		var form = jQuery(AmCharts.Editor.SELECTORS.modal).find('form');

		// APPLY WIDTH
		jQuery(AmCharts.Editor.SELECTORS.modalContent).removeClass(AmCharts.Editor.SELECTORS.modalClasses);
		jQuery(AmCharts.Editor.SELECTORS.modalContent).addClass('modal-sm');

		function randomizeData(columnName) {
			var min = Number(form[0].data_min.value);
			var max = Number(form[0].data_max.value);
			if ( form[0].data_rnd.checked ) {
				for(var i = 0; i < AmCharts.Editor.dataProvider.length; i++){
					AmCharts.Editor.dataProvider[i][columnName] = Math.floor(Math.random() * (max - min + 1)) + min;
				}
			}

			return columnName;
		}

		/*
		** BIND FORM
		*/
		if ( jQuery(AmCharts.Editor.SELECTORS.modal).data('tmp') ) {
			form[0].title.value = jQuery(AmCharts.Editor.SELECTORS.modal).data('tmp').fieldname;
			jQuery(AmCharts.Editor.SELECTORS.modal).data('tmp',false);
		} else {
			form[0].title.value = AmCharts.Editor.getMSG('prefix_new_column') + AmCharts.Editor.dataFields.length;
		}
		jQuery(form[0].data_rnd).on('change',function() {
			jQuery(form[0].data_min).prop('disabled',!this.checked);
			jQuery(form[0].data_max).prop('disabled',!this.checked);
		});

		/*
		** BIND BUTTONS
		*/
		jQuery(AmCharts.Editor.SELECTORS.modal).find('.btn-default').on('click',function() {
			randomizeData(form[0].title.value);
			AmCharts.Editor.processData(AmCharts.Editor.dataProvider);
			AmCharts.Editor.modal("hide");
		});

		if ( jQuery.inArray(AmCharts.Editor.chartType,['serial','xy','radar']) != -1 ) {
			jQuery(AmCharts.Editor.SELECTORS.modal).find('.btn-primary').on('click',function() {
				AmCharts.Editor.createGraph(randomizeData(form[0].title.value));

				AmCharts.Editor.processData(AmCharts.Editor.dataProvider);
				AmCharts.Editor.modal("hide");
			}).removeClass('hidden');
		}

	})(jQuery)
</script>
