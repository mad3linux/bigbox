<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title">Save HTML</h4>
</div>
<div class="modal-body less-padding" style="height: 600px;">
	<textarea id="am-data-export-html" style="display: block; border: none; background: none; width: 100%; height: 100%; padding: 20px;" disabled>&lt;!DOCTYPE html&gt;
&lt;html&gt;
	&lt;head&gt;
		&lt;title&gt;[[title]] | amCharts&lt;/title&gt;
		&lt;meta name=&quot;description&quot; content=&quot;[[description]]&quot; /&gt;[[font]]

		<!-- amCharts javascript sources -->
		[[sources]]

		<!-- amCharts javascript code -->
		&lt;script type=&quot;text/javascript&quot;&gt;
			AmCharts.makeChart(&quot;chartdiv&quot;,
[[config]]
			);
		&lt;/script&gt;
	&lt;/head&gt;
	&lt;body&gt;
		&lt;div id=&quot;chartdiv&quot; style=&quot;width: 100%; height: 400px; background-color: [[css]];&quot; &gt;&lt;/div&gt;
	&lt;/body&gt;
&lt;/html&gt;</textarea>
</div>
<div class="modal-footer less-margin">
	<a href="#am-data-export-copy" class="btn btn-default" data-clipboard-target="am-data-export-html">Copy to clipboard</a>
	<a href="#am-data-export-save" target="_blank" type="button" class="btn btn-primary" download="amcharts.editor.html">Save to filesystem</a>
</div>
<script type="text/javascript">
	(function() {
		var input		= jQuery(AmCharts.Editor.SELECTORS.modal).find('textarea');
		var tpl			= input.val();
		var cfg			= JSON.stringify(AmCharts.Editor.current.cfg,undefined,'\t');
		var srcs		= ['http://www.amcharts.com/lib/3/amcharts.js'];
		var font		= "";
		var title		= AmCharts.Editor.current.chart.title || 'chart created with amCharts';
		var description	= AmCharts.Editor.current.chart.description || 'chart created using amCharts live editor';
		var plugins		= [];

		// ADD CORRECT TAB INTEND
		cfg			= '\t\t\t\t' + cfg.replace(/\n/g, '\n\t\t\t\t');

		// APPLY THEME FONT AND TRY TO SIMPLY GATHER IT FROM GOOGLE
		if ( AmCharts.Editor.chart.theme && AmCharts.Editor.chart.theme.AmChart.fontFamily ) {
			font = "\n\n\t\t<!-- amCharts custom font -->\n\t\t<link href='http://fonts.googleapis.com/css?family=[[fontfamily]]' rel='stylesheet' type='text/css'>".replace('[[fontfamily]]',AmCharts.Editor.chart.theme.AmChart.fontFamily.replace(/ /g,'+'));
		}

		// APPLY WIDTH
		jQuery(AmCharts.Editor.SELECTORS.modalContent).removeClass(AmCharts.Editor.SELECTORS.modalClasses);
		jQuery(AmCharts.Editor.SELECTORS.modalContent).addClass('modal-lg');

		// GATHER SOURCES
		jQuery.merge(srcs,AmCharts.Editor.current.tpl.srcs || []);

		// ADD THEME IF APPLIED
		if ( AmCharts.Editor.chartConfig.theme ) {
			srcs.push('http://www.amcharts.com/lib/3/themes/'+ AmCharts.Editor.chartConfig.theme.themeName +'.js')
		}

		if ( AmCharts.Editor.chart["export"] ) {
			srcs.push('http://www.amcharts.com/lib/3/plugins/export/export.js');
			srcs.push('http://www.amcharts.com/lib/3/plugins/export/export.css');
		}

		// CREATE SCRIPT TAGS AND INTERPOLATE WITH TPL
		srcs = jQuery(srcs).map(function() {
			var resource = this;
			if ( resource.indexOf('http:') == -1 ) {
				resource = "http:" + resource;
			}

			// SCRIPT
			if ( resource.indexOf(".js") != -1 ) {
				var script = jQuery('<script>').attr('type','text/javascript').attr('src',resource);
				return jQuery('<div>').append(script).html();

			// CSS
			} else if ( resource.indexOf(".css") != -1 ) {
				var script = jQuery('<link>').attr('rel','stylesheet').attr('href',resource);
				return jQuery('<div>').append(script).html();
			}
			
		//}).get().reverse().join('\n\t\t');
		});
		srcs = srcs.get();
		srcs = srcs.join('\n\t\t');
		tpl = tpl.replace('[[title]]',title);
		tpl = tpl.replace('[[description]]',description);
		tpl = tpl.replace('[[sources]]',srcs);
		tpl = tpl.replace('[[config]]',cfg);
		tpl = tpl.replace('[[css]]',AmCharts.Editor.chart.backgroundColor);
		tpl = tpl.replace('[[font]]',font);

		input.val(tpl);

		// CLIPBOARD; THROWS AN ERROR BUT IT WORKS
		new ZeroClipboard(jQuery('.modal .btn-default')).on('mouseover mouseout',function(e) {
			jQuery(e.target).attr('aria-label',AmCharts.Editor.getMSG('tooltip_clipboard_copy'))[e.type=='mouseover'?'addClass':'removeClass']('cssTooltip');
		}).on('copy',function(e) {
			jQuery(e.target).attr('aria-label',AmCharts.Editor.getMSG('tooltip_clipboard_copied'));
		});

		// APPLY CONTENT TO DOWNLOAD LINK
		jQuery(AmCharts.Editor.SELECTORS.modal).find('.btn-primary').attr('href','data:text/html;charset=utf-8,' + encodeURIComponent(tpl));
	})(jQuery);
</script>
