<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title">Import Data</h4>
</div>
<div class="modal-body">
	<div class="importer-upload" style="padding-bottom: 10px;">
		<div class="alert alert-danger alert-error error-ajax error-ajax-general hidden">
			Ohh something went wrong, we were not able to receive that file.
		</div>
		<div class="alert alert-danger alert-error error-ajax error-ajax-parseerror hidden">
			Ohh something went wrong, we were not able to parse your data due bad formating or similar.
		</div>
		<div class="alert alert-danger alert-error error-general hidden">
			Ohh something went wrong, we were not able to process this file for any reason.
		</div>
		<div class="alert alert-info alert-desc">
			<p>Simply drag and drop your file, copy and paste your plain data or use any URL within the input field below and define some specifics - we do the rest.</p>
			<p>Import is limited to a <strong>maximum of 2000 records</strong>.</p>
		</div>
		<form>
	  		<fieldset>
				<div class="form-group">
					<div class="input-group">
						<textarea class="form-control input-source" type="text" placeholder="Data, URL or browse" style="height: 34px;resize:none;"></textarea>
						<div class="input-group-btn">
							<button class="btn btn-default btn-browse">Browse</button>
						</div>
					</div>
				</div>
				<div class="alert alert-warning alert-cors hidden">
					Ensure your resource accepts cross origin requests and uses the https protocol, otherwise pre-download and drop it here.
				</div>
				<div class="form-group row">
					<div class="col-sm-3">
						<label class="control-label am-tooltip" title="Indicator to split the rows">Column Delimiter</label>
						<div class="input-group">
							<input class="form-control input-delimiter " type="text" name="delimiter" value=",">
							<div class="input-group-btn">
								<div class="btn-group">
									<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
										<span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu">
										<li><a href="#" data-value="Tab">Tab</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-3">
						<label class="control-label am-tooltip" title="Indicator to start capturing">Start row</label>
						<div class="input-group">
							<input class="form-control input-row-start " type="text" name="row_start" value="0">
							<div class="input-group-btn">
								<div class="btn-group">
									<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
										<span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu">
										<li><a href="#" data-value="&amp;">&amp;</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<label class="control-label am-tooltip" title="Indicator to stop capturing">End Row</label>
						<div class="input-group">
							<input class="form-control input-row-end " type="text" name="row_end" value="" placeholder="unlimited">
							<div class="input-group-btn">
								<div class="btn-group">
									<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
										<span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu">
										<li><a href="#" data-value="&amp;">&amp;</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="hidden">
					<input type="file" class="input-file">
				</div>
			</fieldset>
		</form>
	</div>
	<div class="importer-settings hidden">
		<div class="alert alert-info data-array">
			<p>Great, now just check if everything is fine - each row represents one field.</p>
			<p>If everything seems to be ok, click on `finished` to overtake the data.</p>
		</div>
		<div class="alert alert-info data-object">
			<p>Great, now just check if everything is fine - each row represents one sheet.</p>
			<p>If everything seems to be ok, select one and click on `finished` to overtake the data.</p>
		</div>
		<table class="table table-condensed table-fields" style="border-bottom: 1px solid #DDD;">
			<tbody>
				<tr class="table-tpl hidden">
					<td class="name"></td>
					<td class="button">
						<button class="btn btn-default btn-xs btn-select pull-right">Select</button>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<div class="modal-footer importer-upload">
	<button type="button" class="btn btn-primary btn-import disabled">Import</button>
</div>
<div class="modal-footer importer-settings hidden">
	<button type="button" class="btn btn-default btn-previous pull-left">Back</button>
	<button type="button" class="btn btn-primary btn-adapt">Finished</button>
</div>
<div class="modal-dropzone hidden" style="background-color: rgba(255,255,255,.9);position: absolute; top:0; bottom:0; width: 100%;z-index:1337;border-radius: 6px;border: 4px dashed #EFEFEF;">
	<table style="height: 100%; width: 100%;">
		<tr>
			<td style="text-align:center;">
				<p class="lead">Simply drop it around here</p>
			</td>
		</tr>
	</table>
</div>
<script type="text/javascript">
	(function() {
		var modal			= jQuery(AmCharts.Editor.SELECTORS.modalContent);
		AmCharts.Editor.CONSTANT.DATASOURCE = "";
		AmCharts.Editor.CONSTANT.ISHTTP = false;
		var input_file		= jQuery(modal).find('.input-file');
		
		// APPLY WIDTH
		jQuery(modal).removeClass(AmCharts.Editor.SELECTORS.modalClasses);
		jQuery(modal).addClass('modal-md');

		var alert_desc		= jQuery(modal).find('.alert-desc');
		var alert_cors		= jQuery(modal).find('.alert-cors');

		var btn_browse		= jQuery(modal).find('.btn-browse');
		var btn_previous	= jQuery(modal).find('.btn-previous');
		var btn_adapt		= jQuery(modal).find('.btn-adapt');
		var btn_import		= jQuery(modal).find('.btn-import');

		var input_form		= jQuery(modal).find('form');
		var input_fieldset	= jQuery(modal).find('fieldset');
		var input_file		= jQuery(modal).find('.input-file');
		var input_source	= jQuery(modal).find('.input-source');
		var input_delimiter = jQuery(modal).find('.input-delimiter ');
		var input_start		= jQuery(modal).find('.input-row-start');
		var input_end		= jQuery(modal).find('.input-row-end');

		var drop_zone		= jQuery(modal).find('.modal-dropzone');

		var table_fields	= jQuery(modal).find('.table-fields');

		var body_upload		= jQuery(modal).find('.importer-upload');
		var body_settings	= jQuery(modal).find('.importer-settings');

		// TOOLTIP
		jQuery(AmCharts.Editor.SELECTORS.modal).find('.am-tooltip').tooltip();

		// OBSERVE DROPZONE
		jQuery(AmCharts.Editor.SELECTORS.modal).off('dragover').on('dragover',function(e) {
			e.preventDefault();
			e.stopPropagation();
			jQuery(drop_zone).removeClass('hidden');
		});
		jQuery(drop_zone).on('drop dropleave',function(e) {
			e.preventDefault();
			e.stopPropagation();
			jQuery(drop_zone).addClass('hidden');
			jQuery(alert_desc).addClass('hidden');
			jQuery(alert_cors).addClass('hidden');
			input_file[0].files = e.originalEvent.dataTransfer.files;
			jQuery(input_file).trigger('change');
		});

		// OBSERVE BROWSE
		jQuery(btn_browse).on('click',function(e) {
			e.preventDefault();
			jQuery(input_file).trigger('click');
		});

		// OBSERVE FILE INPUT
		jQuery(input_file).on('change',function(e) {
			e.preventDefault();
			var name = this.files[0].name;

			jQuery(alert_desc).addClass('hidden');
			jQuery(input_source).val(name);
			jQuery(input_source).trigger('change');
		});
		jQuery(input_source).on('keyup change blur',function(e) {
			var value	= jQuery(input_source).val().trim();

			jQuery(alert_desc).addClass('hidden');
			if ( (value.indexOf('http://') != -1 || value.indexOf('http://') != -1) && e.type != 'blur' ) {
				jQuery(alert_cors).removeClass('hidden');
			} else {
				jQuery(alert_cors).addClass('hidden');
			}

			// RESET ON CHANGE
			if ( input_file[0].files.length && input_file[0].files[0].name != value ) {
				this.value = '';
				input_file.val('');
				jQuery(btn_import).addClass('disabled');
			} else if ( value.length ) {
				jQuery(btn_import).removeClass('disabled');
			} else {
				jQuery(btn_import).addClass('disabled');
			}
		});

		// OBSERVE DROPDOWN
		jQuery(modal).find('.input-group .dropdown-menu a').on('click',function() {
			jQuery(this).parents('.input-group').find('input').val(jQuery(this).data('value'));
		});

		// OBSERVE IMPORT BUTTON
		jQuery(btn_import).on('click',function() {
			var config		= {};
			var source		= input_file[0].files[0] || jQuery(input_source).val();
			AmCharts.Editor.CONSTANT.DATASOURCE = source.name || source;
			var _source = AmCharts.Editor.CONSTANT.DATASOURCE;
			if ( (_source.indexOf('http://') != -1 || _source.indexOf('http://') != -1)) {
				AmCharts.Editor.CONSTANT.ISHTTP = true;
			}

			var importer	= undefined;
			jQuery(input_form).serializeArray().map(function(item) {
				config[item.name] = item.value;
			});

			// CREATE IMPORT AND STUFF
			importer = new AmCharts.Editor.importer(source,jQuery.extend(config,{
				loadstart: function() {
					jQuery(input_fieldset).attr('disabled',true);
					jQuery(btn_import).addClass('disabled');
				},
				error: function(event,eventName,eventObject) {
					jQuery('.modal .alert-error').addClass('hidden');
					if ( this.readerType == 'AJAX' ) {
						if ( this.readerResponse.statusText == 'parseerror' ) {
							jQuery('.modal .alert-error.error-ajax-parseerror').removeClass('hidden');	
						} else {
							jQuery('.modal .alert-error.error-ajax-general').removeClass('hidden');
						}
					} else {
						jQuery('.modal .alert-error.error-general').removeClass('hidden');
					}
					jQuery(input_fieldset).attr('disabled',false);
					jQuery(btn_import).removeClass('disabled');
				},
				success: function() {
					var that	= this;
					// console.log(that);
					var tpl		= jQuery(table_fields).find('.table-tpl');
					var tbody	= jQuery(table_fields).find('tbody');
					var fields	= that.fields;

					that.selectedResults = that.results;
					// console.log(that.selectedResults);
					// HIDE ALERTS
					jQuery('.modal .alert-error').addClass('hidden');

					// OBJECT EXCEPTION
					jQuery(tbody).find('.btn-select')[that.resultsType!='object'?'addClass':'removeClass']('hidden');
					jQuery(btn_adapt)[that.resultsType=='object'?'addClass':'removeClass']('disabled');

					jQuery('.modal .alert.data-array')[that.resultsType=='object'?'addClass':'removeClass']('hidden');
					jQuery('.modal .alert.data-object')[that.resultsType!='object'?'addClass':'removeClass']('hidden');
					
					// SWITCH VIEW
					jQuery(input_fieldset).attr('disabled',false);
					jQuery(btn_import).removeClass('disabled');
					jQuery(body_upload).addClass('hidden');
					jQuery(body_settings).removeClass('hidden');

					// FULFILL TABLE
					jQuery(tbody).find('.table-field').remove();
					jQuery(fields).each(function() {
						var tmp		= jQuery(tpl).clone().appendTo(tbody);
						var cols	= jQuery(tmp).find('td');
						var field	= this;

						tmp.removeClass('table-tpl hidden');
						tmp.addClass('table-field');

						cols.first().text(field);
						cols.last().find('.btn-select').on('click',function() {
							that.selectedResults = that.results[field];
							that.fields = [];
							jQuery.each(that.results[field][0],function(key,value) {
								that.fields.push(key);
							});
							jQuery(tbody).find('tr').removeClass('btn-success');
							jQuery(this).parents('tr').addClass('btn-success');
							jQuery(btn_adapt).removeClass('disabled');
						});
					});
					// console.log(that.selectedResults);
				}
			}));

			// OBSERVE ADAPT
			jQuery(btn_adapt).off().on('click',function() {
				// console.log(importer.selectedResults);
				AmCharts.Editor.processData(importer.selectedResults);

				// HIDE DONE
				AmCharts.Editor.modal("hide");

				if(AmCharts.Editor.CONSTANT.ISHTTP && AmCharts.Editor.CONSTANT.DATASOURCE!=""){
					jQuery(AmCharts.Editor.SELECTORS.table).parent().find("#am-datasource").find("span").html(AmCharts.Editor.CONSTANT.DATASOURCE);
					jQuery(AmCharts.Editor.SELECTORS.table).parent().find("#am-datasource").removeClass("hidden");
				}else{
					jQuery(AmCharts.Editor.SELECTORS.table).parent().find("#am-datasource").find("span").html(AmCharts.Editor.CONSTANT.DATASOURCE);
					jQuery(AmCharts.Editor.SELECTORS.table).parent().find("#am-datasource").addClass("hidden");
				}
				// console.log(jQuery(AmCharts.Editor.SELECTORS.table));
				// console.log(AmCharts.Editor.CONSTANT.DATASOURCE);
				// console.log(AmCharts.Editor.CONSTANT.ISHTTP);
			});
		});

		// OBSERVE PREVIOUS
		jQuery(btn_previous).on('click',function() {
			jQuery(body_upload).removeClass('hidden');
			jQuery(body_settings).addClass('hidden');
		});
	})(jQuery);

	// LOAD ASYNC.
	(function() {
		var resources = [];
		if ( AmCharts.Editor.importer === undefined ) resources.push(AmCharts.Editor.CONSTANT.URL_BASE + 'dist/ameditor.import.js');
		if ( window.JSZip === undefined ) resources.push(AmCharts.Editor.CONSTANT.URL_BASE + 'vendor/import/jszip.js');
		if ( window.XLSX === undefined ) resources.push(AmCharts.Editor.CONSTANT.URL_BASE + 'vendor/import/xlsx.js');

		// POSTLOAD RESOURCES
		jQuery(resources).each(function() {
			var script = jQuery('<script>').attr({
				src: this,
				type: 'text/javascript'
			}).appendTo(document.body).on('load',function() {
				jQuery(this).remove();
			});
		});
	})(jQuery);
</script>