<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title">ERROR</h4>
</div>
<div class="modal-body">
	<h3></h3>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-primary" onclick='AmCharts.Editor.modal("hide");'>OK</button>
</div>

<script type="text/javascript">
	(function () {
		var h3 = jQuery(AmCharts.Editor.SELECTORS.modal).find('h3')[0];
		h3.html(errormsg);
	})(jQuery);

</script>