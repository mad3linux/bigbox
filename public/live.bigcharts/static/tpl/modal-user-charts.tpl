<div class="am-charts">
	<table class="am-charts-table table table-hover">
		<colgroup>
			<col width="" data-key="title"></col>
			<col width="205" data-key="buttons"></col>
		</colgroup>
		<tbody></tbody>
	</table>
	<div class="am-charts-alert alert alert-warning">
		<p>No saved charts have been found yet, feel free to start with one of our templates.</p>
	</div>
    <ul class="am-charts-pagination pagination"></ul>
</div>

<script type="text/javascript">
	(function() {
		var table		= jQuery(AmCharts.Editor.SELECTORS.modal + ' table');
		var tableBody	= jQuery(table).find('tbody');
		var tableCols	= jQuery(table).find('colgroup col');
		var buttons		= [{
			name: 'edit',
			icon: 'am am-edit',
			link: '/edit/',
			hint: AmCharts.Editor.getMSG('tooltip_edit_chart')
		},{
			name: 'edit-draft',
			icon: 'am am-edit-draft',
			link: '/edit/draft/',
			hint: AmCharts.Editor.getMSG('tooltip_edit_draft')
		},{
			name: 'share',
			icon: 'am am-share',
			link: '/',
			hint: AmCharts.Editor.getMSG('tooltip_share_chart')
		},{
			name: 'public',
			icon: 'am am-public',
			link: '/publish/',
			hint: ''
		},{
			name: 'delete',
			icon: 'am am-delete',
			link: '/delete/',
			hint: AmCharts.Editor.getMSG('tooltip_delete_chart')
		}];

		// STICKY HEADER
		jQuery(AmCharts.Editor.SELECTORS.modal + ' .modal-body').on('scroll',function() {
			jQuery(AmCharts.Editor.SELECTORS.modal + ' .am-charts-headline').css({
				top: this.scrollTop
			});
		});

		jQuery('.am-charts-headline span').text(AmCharts.Editor.current.list.total_count || 0);

		if ( AmCharts.Editor.current.list.charts && AmCharts.Editor.current.list.charts.length ) {
			jQuery(AmCharts.Editor.SELECTORS.modal).find('table').show();
			jQuery(AmCharts.Editor.SELECTORS.modal).find('.alert').hide();

			// CREATE ITEMS
			jQuery(AmCharts.Editor.current.list.charts).each(function(rid) {
				var data	= this;
				var tplRow	= jQuery('<tr></tr>').appendTo(tableBody);

				jQuery(tableCols).each(function(cid) {
					var key		= jQuery(this).data('key');
					var value	= '';
					var tplCol	= jQuery('<td></td>');

					// TITLE
					if ( key == 'title' ) {
						var tmpIcon		= jQuery('<i></i>').appendTo(tplCol);
						var tmpTitle	= jQuery('<a></a>').appendTo(tplCol);
						var tmpTime		= jQuery('<span></span>').appendTo(tplCol);
                        var title       = data['title']||'http://live.amcharts.com/'+data['chart_id']+ '/';
                        var tpl         = JSON.parse(data['template'].replace(/\\/g,''))

                        tmpIcon.addClass('am-charts-icon am-charts-icon-' + data['type']);
                        if ( tpl.group ) {
                        	tmpIcon.css({
	                            backgroundImage: 'url('+ tpl.group.img +')'
	                        });
	                    }

						tmpTitle.attr('href','/'+data['chart_id']+ '/edit/').text(title).addClass('am-charts-title').tooltip({
							title: AmCharts.Editor.getMSG('tooltip_edit_chart')
						}).on('click',function(e) {
							e.preventDefault();
							// RESET
							AmCharts.Editor.current.chart	= {};

							// CHANGE ROUTE
							jQuery.router.go(this.href);
						});
						tmpTime.text(data['chart_updated']).addClass('am-charts-time');

					// BUTTONS
					} else if ( key == 'buttons' ) {
						var tmpList = jQuery('<ul></ul>').addClass('am-list').appendTo(tplCol);
						jQuery(buttons).each(function() {
							var callback = function(e) {
								e.preventDefault();

								// RESET
								AmCharts.Editor.current.chart	= {};

								// CHANGE ROUTE
								jQuery.router.go(tmpHref);
							}

							// DRAFT
							if ( this.name == 'edit-draft' && !data['draft_updated'] ) {
								return;

							// PUBLIC
							} else if ( this.name == 'public' ) {
								if ( data['public'] ) {
                                    this.icon = 'am am-public active';
									this.hint = AmCharts.Editor.getMSG('tooltip_privatize_chart')
									this.link = '/private/';
								} else {
                                    this.icon = 'am am-public';
									this.hint = AmCharts.Editor.getMSG('tooltip_publish_chart');
									this.link = '/publish/';
								}

								callback = function(e) {
									e.preventDefault();
									AmCharts.Editor.saveState(data['chart_id'],Number(data['public'])?0:1);
								}

                            // DELETE
                            } else if ( this.name == 'delete' ) {
                                callback = function(e) {
                                    e.preventDefault();
                                    AmCharts.Editor.deleteChart(data['chart_id']);
                                }
                            }
                            
							var tmpItem = jQuery('<li></li>').appendTo(tmpList);
							var tmpLink = jQuery('<a></a>').data('cfg',this).appendTo(tmpItem);
							var tmpIcon	= jQuery('<i></i>').appendTo(tmpLink);
							var tmpHref = '/' + data['chart_id'] + this.link;

							tmpIcon.tooltip({
								title: this.hint
							});
							tmpIcon.addClass(this.icon);
							tmpLink.attr('href',tmpHref).on('click',callback);
						});

					}

					tplCol.appendTo(tplRow);
				});
			});

            // UPDATE PAGINATION
            AmCharts.Editor.updatePagination();
		} else {
			jQuery(AmCharts.Editor.SELECTORS.modal).find('.am-charts-table').hide();
			jQuery(AmCharts.Editor.SELECTORS.modal).find('.am-charts-alert').show();
			AmCharts.Editor.log('info','No user charts available');
		}

	})(jQuery);

</script>