<header class="app-header">
	<nav class="app-nav navbar navbar-amcharts" role="navigation">
		<a class="navbar-header navbar-header-title" href="/amchart-dialog.php">
			<span>BigChart Editor</span>
			<i class="fa fa-reply"></i>
		</a>
		<ul class="nav navbar-nav navbar-left collapse2 navbar-collapse2">
			<li class="am-menu-save">
				<a href="/amchart-dialog.php/save/"><span>Save</span><i class="am am-save"></i></a>
			</li>
			<!--li class="am-menu-save-draft">
				<a href="/amchart-dialog.php/save/draft/"><span>Save Draft</span><i class="am am-save-draft"></i></a>
			</li>
			<li class="am-menu-fork">
				<a href="/amchart-dialog.php/fork/"><span>Fork</span><i class="am am-fork"></i></a>
			</li-->
			<li class="am-menu-new">
				<a href="/amchart-dialog.php/new"><span>New</span><i class="am am-new"></i></a>
			</li>
			<!--li class="am-menu-share">
				<a href="/amchart-dialog.php/share/"><span>Publish &amp; Share</span><i class="am am-share"></i></a>
			</li-->
		</ul>
		<ul class="nav navbar-nav navbar-right navbar-signin" style="margin: 0 26px 0 0;">
			<li class="am-login-label hidden">
				<span>Want to see your saved charts?</span>
			</li>
			<!--li class="am-menu-signin dropdown">
				<a href="/amchart-dialog.php/signin/" class="dropdown-toggle" data-toggle="dropdown">
					<span class="btn btn-flat btn-lightblue">
						<span>Sign In</span>
					</span>
				</a>
				<ul class="dropdown-menu dropdown-menu-left"></ul>
			</li-->
		</ul>
		<ul class="nav navbar-nav navbar-right navbar-user hidden">
			<li class="am-menu-user dropdown">
				<a href="/amchart-dialog.php/user/" class="dropdown-toggle" data-toggle="dropdown">
					<i class="am am-login"></i>
					<span class="am-user-name">Unknown</span>
					<span class="btn btn-flat btn-lightblue btn-usercharts">
						<span class="am-user-charts">0</span>
					</span>
				</a>
				<ul class="dropdown-menu dropdown-menu-left"></ul>
			</li>
		</ul>
		<!--a class="navbar-header navbar-header-brand" href="https://www.amcharts.com">
			<img src="//localhost/live.amcharts.com/static/img/page/logo_light.png" title="JavaScript charts and maps" alt="" />
		</a-->
	</nav>
</header>
<div class="app-body app-editor fx-all-250">
	<div class="app-editor-1">
		<div class="app-menu app-menu-dark">
			<ul class="am-editor-groups nav nav-stacked"></ul>
		</div>
	</div>
	<div class="app-editor-2">
		<div class="app-editor-2-1">
			<div class="app-menu app-menu-light">
				<ul class="am-editor-props nav nav-stacked"></ul>
			</div>
		</div>
		<div class="app-editor-2-2">
			<div class="app-editor-2-2-1">
				<div class="am-editor-chart" id="am-editor-chart"></div>
			</div>
			<div class="app-editor-2-2-2">
				<div class="tab-header clearfix">
					<ul class="nav nav-tabs navbar-left">
						<li class="am-tab-data active">
							<a href="#am-editor-data" data-toggle="tab"><i class="am am-data"></i><span>Data</span></a>
						</li>
						<li class="am-tab-code">
							<a href="#am-editor-code" data-toggle="tab"><i class="am am-code"></i><span>Code</span></a>
						</li>
					</ul>
					<!--ul class="nav navbar-nav navbar-buttons navbar-left">
						<li>
							<a class="am-data-export" href="#"><i class="am am-export"></i><span>Save Graphic Portlet</span></a>
						</li>
					</ul-->
					<ul class="nav nav-pills nav-themes navbar-right am-editor-themes">
						<li class="am-theme-default" data-theme="default">
							<a href="#"></a>
						</li>
						<li class="am-theme-patterns" data-theme="patterns">
							<a href="#"></a>
						</li>
						<li class="am-theme-chalk" data-theme="chalk">
							<a href="#"></a>
						</li>
						<li class="am-theme-dark" data-theme="dark">
							<a href="#"></a>
						</li>
						<li class="am-theme-light" data-theme="light">
							<a href="#"></a>
						</li>
						<li class="am-theme-black" data-theme="black">
							<a href="#"></a>
						</li>
					</ul>
				</div>
				<div class="tab-content">
					<div class="tab-pane active" id="am-editor-data">
						<ul class="nav navbar-nav navbar-buttons navbar-left">
							<li>
								<a class="am-data-add-row" href="#"><span>Add Row</span><i class="am am-add-row"></i></a>
							</li>
							<li class="">
								<a class="am-data-add-col" href="#"><span>Add Column</span><i class="am am-add-col"></i></a>
							</li>
						</ul>
						<ul class="nav navbar-nav navbar-buttons navbar-right">
							<li class="">
								<a class="am-data-import" href="#"><span>Set Data from API</span><i class="am am-import"></i></a>
							</li>
						</ul>
						
						<ul class="nav navbar-nav navbar-buttons navbar-right hidden" id="am-datasource">
							<li style="color: black;font-size: 12px;margin-top: 12px;margin-right: 5px;">
								<input type="checkbox" name="loop-ds" id="loop-ds"></input>
							</li>
							<li style="color: black;font-size: 12px;margin-top: 10px;">
								<span></span><i class="am am-data"></i>
							</li>
						</ul>
						<div class="am-editor-table" id="am-editor-table"></div>
					</div>
					<div class="tab-pane" id="am-editor-code">
						<textarea class="am-editor-config" id="am-editor-config" rows="3"></textarea>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
