<?php

/*
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class CMS_Controller_Plugin_Acl extends Zend_Controller_Plugin_Abstract {

    public function preDispatch(Zend_Controller_Request_Abstract $request) {
       
         if(constant("survey_mod")=="1"){
        //if($request->module == 'surveyext') {
        //if($request->module == 'surveyext') {
            $nl = new Model_ZPrabacgen();
            $kky = $nl->vg_gen();
            try {
                Zend_Session::start();
            }
            catch(Zend_Session_Exception $e) {
                session_start();
            }
            if($request->module == 'api') {
                return;
            }
           ($request->module == 'default')? $appCur = 'core' : $appCur = $request->module;
            $mdl_sys = new Model_System();
            $acl = new Zend_Acl();
            $controller = $mdl_sys->get_controllers(true);
            $roles = $mdl_sys->get_roles();
            $acl->addRole(new Zend_Acl_Role('guest'));
            
            /*
             if($appCur!='core'){
             $acl->addResource(new Zend_Acl_Resource($appCur.':public'));
             $acl->addResource(new Zend_Acl_Resource($appCur.':error'));
             }
             */
            $cons = array();

            foreach($controller as $k => $v) {

                foreach($v as $k2 => $v2) {
                    if($k == 'core') {
                        array_push($cons, $v2);
                    } else {
                        array_push($cons, $k . 
                                   ':' . 
                                   $v2);
                    }
                }
            }
            //Zend_Debug::dump($cons);die();
            $cons = array_unique($cons);

            foreach($cons as $k => $v) {
                $acl->add(new Zend_Acl_Resource($v));
            }

            foreach($roles as $role) {
                $acl->addRole(new Zend_Acl_Role($role['group_name']));
                $acl->allow($role['group_name'], 'public');
                $acl->allow($role['group_name'], 'error');
                $acl->allow($role['group_name'], 'index');
                $acl->allow($role['group_name'], 'ajax');
                $acl->allow($role['group_name'], 'prabagen');
                $acl->allow($role['group_name'], 'prabaeditor');
                $acl->allow($role['group_name'], 'prabajax');
                $acl->allow($role['group_name'], 'prabapublish');
                $acl->allow($role['group_name'], 'z');
                $acl->allow($role['group_name'], 'prabawf');
                $acl->allow($role['group_name'], 'api');
                $acl->allow($role['group_name'], 'inbox');
                $acl->allow($role['group_name'], 'prabaqueue');
                $acl->allow($role['group_name'], 'prabaesb');
                $acl->allow($role['group_name'], 'r');
                if($appCur != 'core') {
                    $acl->allow($role['group_name'], $appCur . 
                                ':public');
                    $acl->allow($role['group_name'], $appCur . 
                                ':error');
                    $acl->allow($role['group_name'], $appCur . 
                                ':ajax');
                    $acl->allow($role['group_name'], $appCur . 
                                ':general');
                    $acl->allow($role['group_name'], $appCur . 
                                ':index');
                    $acl->allow($role['group_name'], $appCur . 
                                ':prabagen');
                    $acl->allow($role['group_name'], $appCur . 
                                ':z');
                }
            }
            try {
                //$vdat = explode();
            }
            catch(Exception $e) {
            }
            $acl->allow('guest', 'public');
            $acl->allow('guest', 'error');
            $acl->allow('guest', 'prabapublish');
            $acl->allow('guest', 'z');
            $acl->allow('guest', 'prabagen');
            if($appCur != 'core') {
                $acl->allow('guest', $appCur . 
                            ':public');
                $acl->allow('guest', $appCur . 
                            ':error');
                $acl->allow('guest', $appCur . 
                            ':prabagen');
                $acl->allow('guest', $appCur . 
                            ':z');
            }
            try {
                $auth = Zend_Auth::getInstance();
                $identity = $auth->getIdentity();
            }
            catch(Zend_Session_Exception $e) {
            }
            $roleIden = array();
            if($auth->hasIdentity()) {
                $perms = $mdl_sys->get_permissions_by_uid($identity->uid);
                $arrperms = array();

                foreach($perms as $p) {
                    $roleIden[] = $p['group_name'];
                    $arrperms =($p['perms'] != "")? unserialize($p['perms']): array();
                    $arrpages =($p['pages'] != "")? unserialize($p['pages']): array();

                    foreach($arrpages as $key => $val) {
                        $rolpages[] = $val;
                    }

                    foreach($arrperms as $key => $arp) {
                        //Zend_Debug::dump($key);echo '(PERM)';
                        if($key == $appCur) {

                            foreach($arp as $arpp) {
                                $module =($appCur != 'core')? $appCur . ':' : '';
                                //Zend_Debug::dump($module.$arpp);
                                if(in_array($module . $arpp, $cons)) {
                                    //Zend_Debug::dump($module.$arpp);echo '(IN)';
                                    $acl->allow($p['group_name'], $module . 
                                                $arpp);
                                }
                            }
                        }
                    }
                }
                //die();
                $role = $roleIden;
            } else {
                $role = array('guest');
            }
            $req =(($request->module == 'default')? $request->controller : $request->module . 
                    ':' . 
                    $request->controller);
            if(!in_array($req, $cons)) {
                if($appCur != 'core') {
                    $request->setModuleName($appCur);
                } else {
                    $request->setModuleName('default');
                }
                $request->setControllerName('error');
                $request->setActionName('error404');
                $request->setParam('error_handler', '404');
            }
            $per = array();
            $per[$req] = "";

            foreach($role as $rol) {
                try {
                    if(!$acl->isAllowed($rol, $req)) {
                        $per[$req][] = 0;
                    } else {
                        $per[$req][] = 1;
                    }
                }
                catch(Zend_Acl_Exception $e) {
                    if($appCur != 'core') {
                        $request->setModuleName($appCur);
                    } else {
                        $request->setModuleName('default');
                    }
                    $request->setControllerName('error');
                    $request->setActionName('error404');
                    $request->setParam('error_handler', '404');
                }
            }
            //Zend_Debug::dump($appCur);die();
            if(in_array($req, $cons)) {
                //Zend_Debug::dump($per);die();
                if(!in_array('1', $per[$req])) {
                    if(!$auth->hasIdentity()) {
                        if($appCur != 'core') {
                            $request->setModuleName($appCur);
                        } else {
                            $request->setModuleName('default');
                        }
                        $request->setControllerName('public');
                        $request->setActionName('login');
                        $CMS_Himfunc = new CMS_Himfunctions();
                        $request->setParam('redirect', $CMS_Himfunc->curPageURL(true));
                    } else {
                        if($appCur != 'core') {
                            $request->setModuleName($appCur);
                        } else {
                            $request->setModuleName('default');
                        }
                        $request->setControllerName('error');
                        $request->setActionName('error403');
                        $request->setParam('error_handler', '403');
                    }
                }
            }
            if($request->controller == 'prabagen' && isset($request->page)&& $request->page != "") {
                if(!in_array($request->page, $arrpages)) {
                    if($appCur != 'core') {
                        $request->setModuleName($appCur);
                    } else {
                        $request->setModuleName('default');
                    }
                    $request->setControllerName('error');
                    $request->setActionName('error403');
                    $request->setParam('error_handler', '403');
                }
            }
            //Zend_Debug::dump($request);
            if($request->error_handler != null && isset($request->error_handler->type)&& $request->error_handler->type == 'EXCEPTION_NO_ACTION') {
                if($appCur != 'core') {
                    $request->setModuleName($appCur);
                } else {
                    $request->setModuleName('default');
                }
                $request->setControllerName('error');
                $request->setActionName('error404');
                $request->setParam('error_handler', '404');
            }
            //echo $request->controller;
            if($request->module != 'default') {
                //define("temp_path_file", '/assets/'.$request->module.'/skins/');
            } else {
                //define("temp_path_file", '/assets/core/skins/');
            }
            if(!$this->getRequest()->isSecure()) {
                //$this->_response->setRedirect("https://apec2013.telkom.co.id".$_SERVER['REQUEST_URI']."");
            }
        } else {
            $nl = new Model_ZPrabacgen();
            $kky = $nl->vg_gen();
            try {
                Zend_Session::start();
            }
            catch(Zend_Session_Exception $e) {
                session_start();
            }
            if($request->module == 'api') {
                return;
            }
           ($request->module == 'default')? $appCur = 'core' : $appCur = $request->module;
            $mdl_sys = new Model_System();
            $sys = new Satcuti_Model_Login();
            
            
            $acl = new Zend_Acl();
            $controller = $mdl_sys->get_controllers(true);
            $roles = $sys->get_roles();
            #Zend_Debug::dump($roles); die();
            $acl->addRole(new Zend_Acl_Role('guest'));
            
            /*
             if($appCur!='core'){
             $acl->addResource(new Zend_Acl_Resource($appCur.':public'));
             $acl->addResource(new Zend_Acl_Resource($appCur.':error'));
             }
             */
            $cons = array();

            foreach($controller as $k => $v) {

                foreach($v as $k2 => $v2) {
                    if($k == 'core') {
                        array_push($cons, $v2);
                    } else {
                        array_push($cons, $k . 
                                   ':' . 
                                   $v2);
                    }
                }
            }
            //Zend_Debug::dump($cons);die();
            $cons = array_unique($cons);

            foreach($cons as $k => $v) {
                $acl->add(new Zend_Acl_Resource($v));
            }

            foreach($roles as $role) {
                $acl->addRole(new Zend_Acl_Role(strtolower($role['GROUP_NAME'])));
                $acl->allow(strtolower($role['GROUP_NAME']), 'public');
                $acl->allow(strtolower($role['GROUP_NAME']), 'error');
                $acl->allow(strtolower($role['GROUP_NAME']), 'index');
                $acl->allow(strtolower($role['GROUP_NAME']), 'ajax');
                $acl->allow(strtolower($role['GROUP_NAME']), 'prabagen');
                $acl->allow(strtolower($role['GROUP_NAME']), 'prabaeditor');
                $acl->allow(strtolower($role['GROUP_NAME']), 'prabajax');
                $acl->allow(strtolower($role['GROUP_NAME']), 'generalora');
                $acl->allow(strtolower($role['GROUP_NAME']), 'prabapublish');
                $acl->allow(strtolower($role['GROUP_NAME']), 'z');
                $acl->allow(strtolower($role['GROUP_NAME']), 'prabawf');
                $acl->allow(strtolower($role['GROUP_NAME']), 'api');
                $acl->allow(strtolower($role['GROUP_NAME']), 'inbox');
                $acl->allow(strtolower($role['GROUP_NAME']), 'prabaqueue');
                $acl->allow(strtolower($role['GROUP_NAME']), 'prabaesb');
                $acl->allow(strtolower($role['GROUP_NAME']), 'r');
               # $acl->allow(strtolower($role['GROUP_NAME']), 'front:admin');
                if($appCur != 'core') {
                    $acl->allow(strtolower($role['GROUP_NAME']), $appCur . 
                                ':public');
                    $acl->allow(strtolower($role['GROUP_NAME']), $appCur . 
                                ':error');
                    $acl->allow(strtolower($role['GROUP_NAME']), $appCur . 
                                ':ajax');
                    $acl->allow(strtolower($role['GROUP_NAME']), $appCur . 
                                ':general');
                    $acl->allow(strtolower($role['GROUP_NAME']), $appCur . 
                                ':index');
                    $acl->allow(strtolower($role['GROUP_NAME']), $appCur . 
                                ':prabagen');
                    $acl->allow(strtolower($role['GROUP_NAME']), $appCur . 
                                ':z');
                }
            }
            try {
                //$vdat = explode();
            }
            catch(Exception $e) {
            }
            
            $acl->allow('guest', 'public');
            $acl->allow('guest', 'error');
            $acl->allow('guest', 'prabapublish');
            $acl->allow('guest', 'z');
            $acl->allow('guest', 'prabagen');
            if($appCur != 'core') {
                $acl->allow('guest', $appCur . 
                            ':public');
                $acl->allow('guest', $appCur . 
                            ':error');
                $acl->allow('guest', $appCur . 
                            ':prabagen');
                $acl->allow('guest', $appCur . 
                            ':z');
            }
            try {
                $auth = Zend_Auth::getInstance();
                $identity = $auth->getIdentity();
            }
            catch(Zend_Session_Exception $e) {
            }
           
            $roleIden = array();
            if($auth->hasIdentity()) {
               
                $perms = $sys->get_permissions_by_uid_ora($identity->uid);
               // Zend_Debug::dump($perms); die();
                $arrperms = array();

                foreach($perms as $p) {
                    $roleIden[] = $p['GROUP_NAME'];
                    $arrperms =($p['PERMS'] != "")? unserialize($p['PERMS']): array();
                    $arrpages =($p['PAGES'] != "")? unserialize($p['PAGES']): array();

                    foreach($arrpages as $key => $val) {
                        $rolpages[] = $val;
                    }
                    

                    foreach($arrperms as $key => $arp) {
                        //Zend_Debug::dump($key);echo '(PERM)';
                        if($key == $appCur) {

                            foreach($arp as $arpp) {
                                $module =($appCur != 'core')? $appCur . ':' : '';
                                //Zend_Debug::dump($module.$arpp);
                                if(in_array($module . $arpp, $cons)) {
                                    //Zend_Debug::dump($module.$arpp);echo '(IN)';
                                    $acl->allow($p['group_name'], $module . 
                                                $arpp);
                                }
                            }
                        }
                    }
                }
                //die();
                $role = $roleIden;
            } else {
                $role = array('guest');
            }
            $req =(($request->module == 'default')? $request->controller : $request->module . 
                    ':' . 
                    $request->controller);
            if(!in_array($req, $cons)) {
                if($appCur != 'core') {
                    $request->setModuleName($appCur);
                } else {
                    $request->setModuleName('default');
                }
                $request->setControllerName('error');
                $request->setActionName('error404');
                $request->setParam('error_handler', '404');
            }
            $per = array();
            //$per[$req] = "";
            #Zend_Debug::dump($role); die();
            foreach($role as $rol) {
                try {
                    if(!$acl->isAllowed(strtolower($rol), $req)) {
                        $per[$req][] = 0;
                    } else {
                        $per[$req][] = 1;
                    }
                }
                catch(Zend_Acl_Exception $e) {
                    if($appCur != 'core') {
                        $request->setModuleName($appCur);
                    } else {
                        $request->setModuleName('default');
                    }
                    $request->setControllerName('error');
                    $request->setActionName('error404');
                    $request->setParam('error_handler', '404');
                }
            }
            //Zend_Debug::dump($appCur);die();
            if(in_array($req, $cons)) {
                //Zend_Debug::dump($per);die();
                if(!in_array('1', $per[$req])) {
                    if(!$auth->hasIdentity()) {
                        if($appCur != 'core') {
                            $request->setModuleName($appCur);
                        } else {
                            $request->setModuleName('default');
                        }
                        $request->setControllerName('public');
                        $request->setActionName('login');
                        $CMS_Himfunc = new CMS_Himfunctions();
                        $request->setParam('redirect', $CMS_Himfunc->curPageURL(true));
                    } else {
                        if($appCur != 'core') {
                            $request->setModuleName($appCur);
                        } else {
                            $request->setModuleName('default');
                        }
                        $request->setControllerName('error');
                        $request->setActionName('error403');
                        $request->setParam('error_handler', '403');
                    }
                }
            }
            if($request->controller == 'prabagen' && isset($request->page)&& $request->page != "") {
                if(!in_array($request->page, $arrpages)) {
                    if($appCur != 'core') {
                        $request->setModuleName($appCur);
                    } else {
                        $request->setModuleName('default');
                    }
                    $request->setControllerName('error');
                    $request->setActionName('error403');
                    $request->setParam('error_handler', '403');
                }
            }
            //Zend_Debug::dump($request);
            if($request->error_handler != null && isset($request->error_handler->type)&& $request->error_handler->type == 'EXCEPTION_NO_ACTION') {
                if($appCur != 'core') {
                    $request->setModuleName($appCur);
                } else {
                    $request->setModuleName('default');
                }
                $request->setControllerName('error');
                $request->setActionName('error404');
                $request->setParam('error_handler', '404');
            }
            //echo $request->controller;
            if($request->module != 'default') {
                //define("temp_path_file", '/assets/'.$request->module.'/skins/');
            } else {
                //define("temp_path_file", '/assets/core/skins/');
            }
            if(!$this->getRequest()->isSecure()) {
                //$this->_response->setRedirect("https://apec2013.telkom.co.id".$_SERVER['REQUEST_URI']."");
            }
        }
    }
}
