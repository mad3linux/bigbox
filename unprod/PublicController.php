<?php

/*
 * Copyright (C) Prabtech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */

class PublicController extends Zend_Controller_Action {

    public function init() {
        
        /*
         * $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger'); $this->_cache = Zend_Registry::get('cache'); $this->initView();
         */
    }

    public function cobaAction() {
    }

    public function getAction() {
        $cc = new Pmas_Model_Pubmediacrawler();
        $vdata = $cc->processing_date("19 Sep 2016, 15:17 WIB", 6);
        Zend_Debug::dump($vdata);
        die();
        $var = unserialize(base64_decode("YToxOntzOjc6InJzc2l0ZW0iO2E6NDp7czo0OiJpY29uIjtzOjExOiJkZXNjcmlwdGlvbiI7czo1OiJ0aXRsZSI7czo1OiJ0aXRsZSI7czo0OiJsaW5rIjtzOjQ6ImxpbmsiO3M6NDoiZGF0ZSI7czo3OiJwdWJEYXRlIjt9fQ=="));
        Zend_Debug::dump($var);
        die();
    }

    public function index() {
    }

    public function loginAction() {
        //Zend_Debug::dump($_SERVER); die();
        if($_SERVER['HTTP_HOST'] == 'hcis.jasamarga.co.id') {
            # DEFINE("survey_mod", "1");
            $this->_redirect("/surveyext");
        } else {
            # DEFINE("survey_mod", "0");
        }
        $theme = Zend_Registry::get('theme');
        $this->view->headLink()->appendStylesheet("/assets/$theme/pages/css/login.min.css");
        $params = $this->getRequest()->getParams();
        try {
            $authAdapter = Zend_Auth::getInstance();
            $usr = $authAdapter->getIdentity();
        }
        catch(Exception $e) {
        }
        if(isset($usr->uid)&& isset($usr->uname)) {
            $this->redirect('/');
        }
        $captcha = new Zend_Form_Element_Captcha('captcha', // This is the name of the input field
        array('label' => 'Write the chars to the field', 'captcha' => array(// Here comes the magic...
        // First the type...
        'captcha' => 'Image', // Length of the word...
        'wordLen' => 3, //'height'=>30,
        // Captcha timeout, 5 mins
        'timeout' => 300, 'LineNoiseLevel' => 0, 'DotNoiseLevel' => 5, // What font to use...
        'font' => APPLICATION_PATH . '/../public/assets/core/fonts/elephant.ttf', // Where to put the image
        'imgDir' => APPLICATION_PATH . '/../public/tmp/captcha/', // URL to the images
        // This was bogus, here's how it should be... Sorry again :S
        'imgUrl' => '/tmp/captcha/',)));
        $form = new Form_Login();
        $form->setAction('/public/login');
        $form->addElement($captcha);
        $this->view->form = $form;
        $this->view->captcha = $captcha;
        if($this->_request->isPost()&& isset($_POST)) {
            $cms_enc = new CMS_Enc();
            $data = $_POST;
            $raws = $_POST;
            $captcha = $data['captcha'];
            $captchaId = $captcha['id'];
            $captchaInput = $captcha['input'];
            $captchaSession = new Zend_Session_Namespace('Zend_Form_Captcha_' . 
                                                         $captchaId);
            $captchaIterator = $captchaSession->getIterator();
            if(!isset($captchaIterator['word'])) {
                $this->redirect('/');
            }
            $captchaWord = $captchaIterator['word'];
            if($captchaInput == $captchaWord) {
                $u = $data['uname'];
                $p = $data['passw'];
                //$mdl_zusr = new Model_Zusers();
                $sys = new Satcuti_Model_Login();
                $data = $sys->getuser($u);
                // Zend_Debug::dump($data); die();
                $arr1 = array();
                if(isset($data['PERSON_NUMBER'])) {
                    $rols = $sys->get_roles_by_uid($data['PERSON_NUMBER']);
                    //Zend_Debug::dump($rols);
                    //die();

                    foreach($rols as $v) {
                        $arr[] = $v['GID'];
                        if($data['MAIN_ROLE'] != '' && $data['MAIN_ROLE'] != null) {
                            if($v['LANDING_PAGE'] != "" && $v['LANDING_PAGE'] != null && ( int ) $v['GID'] == ( int ) $data['MAIN_ROLE']) {
                                $redirect_role = $v['LANDING_PAGE'];
                            }
                        }
                    }
                }
                // Zend_Debug::dump($redirect_role ); die();
                #die("x");
                $auth = false;
                if(!isset($data['PERSON_NUMBER'])|| $data['PERSON_NUMBER'] == "") {
                    $cms_ldp = new CMS_LDAP();
                    $auth = $cms_ldp->auth($u, $p);
                    if($auth) {
                        $data['id'] = "999999999";
                        $arr[0] = "108";
                        $uid = "999999999";
                        $redirect_role = '/';
                    } else {
                        $uid = $u;
                    }
                } else {
                    $uid = $data['PERSON_NUMBER'];
                }
                $row = array();
                $msg = "Username dan Password anda tidak dikenal\nSilahkan gunakan user LDAP anda.";
                $ret = array('success' => 0,
                             'msg' =>$msg);
                if($u == '') {
                    $ret['success'] = 0;
                    $ret['msg'] = " Username is required";
                }
                if($p == '') {
                    $ret['success'] = 0;
                    $ret['msg'] = "Password is required";
                }
                $cms_ldp = new CMS_LDAP();
                if($data['ISLDAP'] == 1) {
                    $auth = $cms_ldp->auth($u, $p);
                }
                if($auth && $data['id'] != "") {
                    $info = $cms_ldp->bind($u);
                    $ret['success'] = $auth;
                    $authsession = Zend_Auth::getInstance();
                    $storage = $authsession->getStorage();
                    $obj2 = new stdClass();
                    $obj2->uid = $uid;
                    $obj2->uname = $u;
                    $obj2->fullname = $info[0]['cn'][0];
                    $obj2->isldap = 1;
                    $obj2->mainrole = 1;
                    $obj2->mainrole = $data['main_role'];
                    $obj2->auth = 1;
                    $obj2->currentevent = $event;
                    $obj2->roles = $arr;
                    $obj2->ubis = $data['ubis_id'];
                    $obj2->sububis = $data['sub_ubis_id'];
                    $obj2->sububisname = $data['sub_ubis'];
                    $storage->write($obj2);
                    $ret['msg'] = "";
                } elseif(($data['ISLDAP'] != 1 or $data['ISLDAP'] == "" or !isset($data['ISLDAP']))&& $uid != "") {
                    $params3 = array('username' => constant("SAT_SCHEMA"),
                                     'password' => constant("SAT_PASSWORD"),
                                     'host' => constant("SAT_HOST"),
                                     'dbname' => constant("SAT_DB"));
                    $adapter = Zend_Db::factory('Oracle', $params3);
                    $db = Zend_Db_Table::setDefaultAdapter($adapter);
                    //Zend_Debug::dump($bd)
                    if(count($data)> 0) {
                        try {
                            $authAdapter = new CMS_OraAuth($db, 'JM_EMPLOYEE', 'EMPLOYEE_NUMBER', 'PASSWORD', '', 0);
                            $authAdapter->setIdentity($u);
                            $authAdapter->setCredential(md5($p));
                            $result = $authAdapter->authenticate();
                        }
                        catch(Exception $e) {
                            Zend_Debug::dump($e->getMessage());
                            die();
                        }
                        // Zend_Debug::dump($result);
                        //die();
                        if($result->isValid()) {
                            $cekz = false;
                            if($cekz) {
                                $ret['success'] = 0;
                                $ret['msg'] = "Username has been used in other place.";
                                // var_dump($ret);die();
                            } else {
                                try {
                                    $authsession = Zend_Auth::getInstance();
                                    $storage = $authsession->getStorage();
                                }
                                catch(Exception $e) {
                                }
                                $ret['success'] = 1;
                                $obj2 = new stdClass();
                                $obj2->uid = $data['PERSON_NUMBER'];
                                $obj2->uname = $data['EMPLOYEE_NUMBER'];
                                $obj2->roles = $arr;
                                $obj2->fullname = $data['PERSON_NAME'];
                                $obj2->fullname = $data['PERSON_NAME'];
                                $obj2->email = $data['EMAIL_ADDRESS'];
                                $obj2->isldap = 0;
                                $obj2->currentevent = $event;
                                $obj2->auth = 1;
                                $obj2->ubis = $data['UBIS_ID'];
                                $obj2->sububis =(isset($data['SUB_UBIS_ID']))? $data['SUB_UBIS_ID'] : null;
                                $obj2->sububisname =(isset($data['SUB_UBIS']))? $data['SUB_UBIS'] : null;
                                $storage->write($obj2);
                                $ret['msg'] = "";
                            }
                        } else {
                            $ret['msg'] = "Wrong User Name/Password";
                        }
                    } else {
                        $ret['success'] = 0;
                        $ret['msg'] = "User tersebut tidak terdaftar.";
                    }
                } else {
                    $ret['success'] = 0;
                    $ret['msg'] = "LDAP Error";
                }
                // Zend_Debug::dump($ret); die();
                if($ret['success'] == 1) {
                    try {
                        $auth = Zend_Auth::getInstance();
                        $usr = $auth->getIdentity();
                    }
                    catch(Zend_Session_Exception $e) {
                    }
                    // Zend_Debug::dump($usr); die();
                    if(isset($usr->uid)&& isset($usr->uname)) {
                        #$mdl_sys->update_log($usr->uname);
                        #$res2 = $mdl_zusr->update_c_act($usr->uname);
                        if(isset($_GET['redirect'])&&($_GET['redirect'] != '/')&& $_GET['redirect'] != '') {
                            $this->_redirect($_GET['redirect']);
                        } else if(isset($_POST['redirect'])&& $_POST['redirect'] != '' && $_POST['redirect'] != '/') {
                            $this->_redirect($_POST['redirect']);
                        } else if(isset($params['redirect'])&& $params['redirect'] != '' && $params['redirect'] != '/') {
                            $this->_redirect($params['redirect']);
                        } else {
                            $module = Zend_Controller_Front::getInstance()->getRequest()->getModuleName();
                            if(isset($redirect_role)&& $redirect_role != "") {
                                $this->_redirect($redirect_role);
                            } else if($module != 'default') {
                                $this->_redirect("/" . 
                                                 $control);
                            } else {
                                $this->_redirect("/");
                            }
                        }
                    }
                } else {
                }
            } else {
                $ret['success'] = 0;
                $ret['msg'] = "Please enter valid captcha.";
            }
        }
        if(isset($params['redirect'])&& $params['redirect'] != '') {
            $this->view->redirect = $params['redirect'];
        }
        if(isset($ret)) {
            $this->_redirect("http://" . 
                             $_SERVER['HTTP_HOST'] . 
                             '' . 
                             $_SERVER['REQUEST_URI'] . 
                             '?' . 
                             http_build_query($ret));
        }
    }

    public function logoutAction() {
        try {
            $authAdapter = Zend_Auth::getInstance();
            $identity = $authAdapter->getIdentity();
            // /clear
            $cc = new Model_Zusers();
            $cc->clear_logout($identity->uname);
            // / end
            $authAdapter->clearIdentity();
            Zend_Session::destroy();
        }
        catch(Exception $e) {
        }
        $this->_redirect('/');
    }
}
