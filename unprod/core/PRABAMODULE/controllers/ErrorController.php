<?php
/*
 * Copyright (C) Prabtech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>,
*/
class PRABAMODULE_ErrorController extends Zend_Controller_Action{

public function error404Action (){
   $this->view->headLink()->appendStylesheet('/assets/core/pages/css/error.min.css');
   $errors = $this->_getParam('error_handler');
   //Zend_Debug::dump($errors); die();
   switch ($errors) {
      case '404':
         // 404 error -- controller or action not found
         $this->getResponse()->setHttpResponseCode(404);
         $this->view->message = 'Page not found';
      break;
      default:
         // application error 
         $this->getResponse()->setHttpResponseCode(500);
         $this->view->message = 'Application error';
      break;
   }
   $this->view->request = $errors;
}

public function errorAction (){
  $this->view->headLink()->appendStylesheet('/assets/core/pages/css/error.min.css');
   //$errors = $this->_getParam('error_handler');
   //Zend_Debug::dump($errors); die();
   $this->getResponse()->setHttpResponseCode(500);
   //$this->view->request = $errors;
}
    
public function error403Action (){
  $this->view->headLink()->appendStylesheet('/assets/core/pages/css/error.min.css');
   $errors = $this->_getParam('error_handler');
   //Zend_Debug::dump($errors); die();

   switch ($errors) {
      case '403':
         // 404 error -- controller or action not found
         $this->getResponse()->setHttpResponseCode(403);
         $this->view->message = 'You Dont Have Authorize to Access This Page';
      break;
      default:
         // application error 
        $this->getResponse()->setHttpResponseCode(500);
         $this->view->message = 'Application error';
      break;
  }
   $this->view->request = $errors;
}
    
}

