<?php
/*
 * Copyright (C) Prabtech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 08.07.2014
 */
class PRABAMODULE_PrabagenController extends Zend_Controller_Action {
	
	
	public function init() {
	}
	
	public function callappiAction() {
		
		$this->_helper->layout->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ( true );
		$params = $this->getRequest ()->getParams ();
		$mm = new Model_Zpraba4apipublish ();
		$dd = new Model_Zprabapage ();
		
		($_GET['xin'])? $xin = $_GET['xin']  : $xin = $params['xin']; 
		$xin = urldecode($xin);
        $xin = str_replace('garing', '/', $xin);
        $xin = str_replace('tambah', '+', $xin);
        $xin = str_replace('prosen', '%', $xin);
        
			if($xin!=NULL){
			$pos = strpos($xin, '~');
			$p=array();
			$ti=array();
			if($pos !== false){
			$qar = explode('~', $xin);
			foreach($qar as $q)
			{
			$p[]=explode(':',$q);
			foreach($p as $pa)
			{
			$ti[$pa[0]]=$pa[1];
			}
			}

			} else {
			$qarin = explode(':', $xin);
			$ti[$qarin[0]]=$qarin[1];

			}
			}
		
		
		

		$xin = array ();
	
		$exe = new Model_Zpraba4api ();
		$varC = $dd->get_api ( $params ['id'] );
		$conn = unserialize ( $varC ['conn_params'] );
		//Zend_Debug::dump($varC);die();
		$exedata = $exe->execute_api ($conn, $varC, $ti);
			
		
		if ($params ['format'] == 'xml') {
			header ( "Content-type: application/xml" );
			
			// $dataxx=array('data'=>$Arr, "api"=>$sqlChart);
			$dataxx = array (
					'data' => $exedata 
			);
			$xml = CMS_Arraytoxml::createXML ( 'root', $dataxx );
			
			// General::debug($Arr);
			
			echo $xml->saveXML ();
			
			die ();
		} else {
			echo json_encode ( $exedata );
			die ();
		}
	
		
	}
	
	public function listcronAction() {
		$auth = Zend_Auth::getInstance ();
		$identity = $auth->getIdentity ();
		$params = $this->getRequest ()->getParams ();
		$callert = new Model_Zprabacrons ();
		$allert = $callert->get_all_allerts_v1 ();
		if ($params ['act'] == 'del') {
			$callert->deleteallert ( $params ['ID'] );
			$this->_redirect ( '/pm/parentwf/listallerts' );
		}
		$this->view->data = $allert;
		$this->view->cat = $cat;
	}
	public function getscriptAction() {
		$this->_helper->layout->disableLayout ();
		$params = $this->getRequest ()->getParams ();
		//Zend_Debug::dump($_SERVER); die();
		// $this->view->headScript()->appendFile('/assets/core/plugins/select2/select2.min.js');
		echo '

			<html>
			<head>	
			<link rel="stylesheet" href="/assets/core/plugins/jqueryhighlight/jquery.highlight.css">
			<script src="/assets/core/plugins/jqueryhighlight/jquery.highlight.js"></script>
			<script>
			$(document).ready(function(){
						$(\'pre.code\').highlight({source:1, zebra:1, indent:\'space\', list:\'ol\'});
					});
			</script>
			</head>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				
						
						<pre class="code" lang="php">
						'.file_get_contents('http://'.$_SERVER['HTTP_HOST'].'/prabapublish/viewportlet/id/'.$params['id'].'/modal/1').'
							
						</code></pre>
						
						
					
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
			</div>
			</html>
';
		die ();
	}
	public function indexAction() {
		$auth = Zend_Auth::getInstance ();
		$identity = $auth->getIdentity ();
		
		//Zend_Debug::dump($identity); die();
		
		Zend_Session::namespaceUnset ( 'api' );
		$ns = new Zend_Session_Namespace ( 'api' );
		$ns->api = array ();
		$params = $this->getRequest ()->getParams ();
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery.numeric.js' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-datepicker/css/datepicker.css' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/select2/select2_metro.css' );
		
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/jquery-multi-select/css/multi-select.css' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js' );
		
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/scripts/form-components.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/select2/select2.min.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/data-tables/jquery.dataTables.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/data-tables/DT_bootstrap.js' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/data-tables/DT_bootstrap.css' );
		
	$this->view->headLink()->appendStylesheet('/assets/core/plugins/jquery-tags-input/jquery.tagsinput.css');
	$this->view->headScript()->appendFile('/assets/core/plugins/jquery-tags-input/jquery.tagsinput.min.js');
		
		$dd = new Model_Zprabapage ();
		$data = $dd->get_page ( $params ['page'] );
		// Zend_Debug::dump(unserialize($data['element'])); die();
		// Zend_Debug::dump($params); die();
			
		if (isset ( $params['debug'] ) && $params['debug'] != "") {
		echo "<br>	<p>Zend_Debug::dump(\$this->data) </p>";
		Zend_Debug::dump($data); 
		
		echo "</div>";
		}
		$port = unserialize ( $data ['element'] );
		//Zend_Debug::dump(implode(",", $port));die();
		$gt = new Model_Prabasystem ();
		$modules = $gt->get_modules ();
		$data = $dd->get_page ( $params ['page'] );
		// Zend_Debug::dump($port); die();
		$this->view->element = $port;
		$this->view->data = $data;
	
		$this->view->modules = $modules;
		$this->view->varams = $params;
	}
	public function viewportletAction() {
		
		Zend_Session::namespaceUnset ( 'api' );
		$ns = new Zend_Session_Namespace ( 'api' );
		$ns->api = array ();
		$params = $this->getRequest ()->getParams ();
		if(isset($params['modal'])&&$params['modal']==1){
		$this->_helper->layout->disableLayout();
		}
		
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery.numeric.js' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-modal/css/bootstrap-modal.css' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-datepicker/css/datepicker.css' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-timepicker/compiled/timepicker.css' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/select2/select2_metro.css' );
		
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/jquery-multi-select/css/multi-select.css' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js' );
		
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/jquery-multi-select/js/jquery.multi-select.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/scripts/form-components.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-modal/js/bootstrap-modalmanager.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-modal/js/bootstrap-modal.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/global/plugins/jquery-validation/js/jquery.validate.min.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/select2/select2.min.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/data-tables/jquery.dataTables.js' );
		$this->view->headScript ()->appendFile ( '/assets/core/plugins/data-tables/DT_bootstrap.js' );
		$this->view->headLink ()->appendStylesheet ( '/assets/core/plugins/data-tables/DT_bootstrap.css' );
		$this->view->element = $params['id'];
		$this->view->varams = $params;
	
	}
	function scripturalist($params) {
		
		
	
	}
}
