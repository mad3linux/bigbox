<?php
/*
 * Copyright (C) Prabtech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>, 24.01.2016
 */
class PRABAMODULE_PublicController extends Zend_Controller_Action {
	
	
	public function init() {
		
	}
	public function index() {
	
	
	}
	
	
	public function loginAction() {
		$theme = Zend_Registry::get('theme');
		$this->view->headLink ()->appendStylesheet ( "/assets/$theme/pages/css/login.min.css" );
		$params = $this->getRequest ()->getParams ();
		
		try {
			$authAdapter = Zend_Auth::getInstance ();
			$usr = $authAdapter->getIdentity ();
		} catch ( Exception $e ) {
		}
		
		if (isset ( $usr->uid ) && isset ( $usr->uname )) {
			$this->redirect ( '/' );
		}
		
		$captcha = new Zend_Form_Element_Captcha(
			'captcha', // This is the name of the input field
    			array(
				'label' => 'Write the chars to the field',
				'captcha' => array( // Here comes the magic...
					// First the type...
					'captcha' => 'Image',
					// Length of the word...
					'wordLen' => 3,
					//'height'=>30,
					// Captcha timeout, 5 mins
					'timeout' => 300,
					'LineNoiseLevel'=>0,
					'DotNoiseLevel'=>5,
					// What font to use...
					'font' => APPLICATION_PATH . '/../public/assets/core/fonts/elephant.ttf',
					// Where to put the image
					'imgDir' => APPLICATION_PATH . '/../public/tmp/captcha/',
					// URL to the images
					// This was bogus, here's how it should be... Sorry again :S
					'imgUrl' => '/tmp/captcha/',
				)
			)
	);
	
	$form = new Form_Login();
	$form->setAction('/public/login'); 
	$form->addElement($captcha) ; 
	$this->view->form = $form;

	$this->view->captcha = $captcha;

		
		if ($this->_request->isPost () && isset ( $_POST )) {
	
			$cms_enc = new CMS_Enc ();
			$data = $_POST;
			$raws = $_POST;
			$captcha = $data ['captcha'];
			$captchaId = $captcha ['id'];
			$captchaInput = $captcha ['input'];
			$captchaSession = new Zend_Session_Namespace ( 'Zend_Form_Captcha_' . $captchaId );
			$captchaIterator = $captchaSession->getIterator ();
			if (! isset ( $captchaIterator ['word'] )) {
				$this->redirect ( '/' );
			}
			$captchaWord = $captchaIterator ['word'];
			if ($captchaInput == $captchaWord) {
				$u = $data ['uname'];
				$p = $data ['passw'];
				$mdl_zusr = new Model_Zusers ();
				$mdl_sys = new Model_System ();
				$data = $mdl_zusr->getuser ( $u );
				$arr1 = array ();
				if (isset ( $data ['id'] )) {
					$rols = $mdl_sys->get_roles_by_uid ( $data ['id'] );
					foreach ( $rols as $v ) {
							
							$arr [] = $v ['gid'];
						if ($data ['main_role'] != '' && $data ['main_role'] != null) {
							
							if ($v ['landing_page'] != "" && $v ['landing_page'] != null && ( int ) $v ['gid'] == ( int ) $data ['main_role']) {
							
							
								$redirect_role = $v ['landing_page'];
							}
						}
						
						if ($v ['exec'] != "") {
							$exec [] = $v ['exec'];
						}
					}
				}
				
		
							
				$auth = false;
				if (! isset ( $data ['id'] ) || $data ['id'] == "") {
					$cms_ldp = new CMS_LDAP ();
					$auth = $cms_ldp->auth ( $u, $p );
					if ($auth) {
						$data ['id'] = "999999999";
						$arr [0] = "108";
						$uid = "999999999";
						$redirect_role = '/';
					} else {
						$uid = $u;
					}
				} else {
					$uid = $data ['id'];
				}
				
				$row = array ();
				$msg = "Username dan Password anda tidak dikenal\nSilahkan gunakan user LDAP anda.";
				$ret = array (
						'success' => 0,
						'msg' => $msg 
				);
				
				if ($u == '') {
					$ret ['success'] = 0;
					$ret ['msg'] = " Username is required";
				}
				
				if ($p == '') {
					$ret ['success'] = 0;
					$ret ['msg'] = "Password is required";
				}
				
				$cms_ldp = new CMS_LDAP ();
				
			
				if ($data ['isldap'] == 1) {
					$auth = $cms_ldp->auth ( $u, $p );
				}
			
				
				if ($auth && $data ['id'] != "") {
					$info = $cms_ldp->bind ( $u );
					$ret ['success'] = $auth;
					$authsession = Zend_Auth::getInstance ();
					$storage = $authsession->getStorage ();
					$obj2 = new stdClass ();
					$obj2->uid = $uid;
					$obj2->uname = $u;
					$obj2->fullname = $info [0] ['cn'] [0];
					$obj2->isldap = 1;
					$obj2->mainrole = 1;
					$obj2->mainrole = $data ['main_role'];
					$obj2->auth = 1;
					$obj2->currentevent = $event;
					$obj2->roles = $arr;
					$obj2->ubis = $data ['ubis_id'];
					$obj2->sububis = $data ['sub_ubis_id'];
					$obj2->sububisname = $data ['sub_ubis'];
					$storage->write ( $obj2 );
					$ret ['msg'] = "";
				} elseif (($data ['isldap'] != 1 or $data ['isldap'] == "" or ! isset ( $data ['isldap'] )) && $uid != "") {
					$db = Zend_Db_Table::getDefaultAdapter ();
					
					if (count ( $data ) > 0) {
						$authAdapter = new CMS_TmaAuth ( $db, 'z_users', 'uname', 'upassword', 'MD5(?)', 0 );
						$authAdapter->setIdentity ( $u );
						$authAdapter->setCredential ( $p );
						$result = $authAdapter->authenticate ();
						
						if ($result->isValid ()) {
						
							$cekz = false;
							
							if ($cekz) {
								$ret ['success'] = 0;
								$ret ['msg'] = "Username has been used in other place.";
								// var_dump($ret);die();
							} else {
								try {
									$authsession = Zend_Auth::getInstance ();
									$storage = $authsession->getStorage ();
								} catch ( Exception $e ) {
								}
								$ret ['success'] = 1;
								$obj2 = new stdClass ();
								$obj2->uid = $data ['id'];
								$obj2->uname = $data ['uname'];
								$obj2->roles = $arr;
								$obj2->fullname = $data ['fullname'];
								$obj2->mainrole = $data ['main_role'];
								$obj2->isldap = 0;
								$obj2->currentevent = $event;
								$obj2->auth = 1;
								$obj2->ubis = $data ['ubis_id'];
								$obj2->sububis = (isset ( $data ['sub_ubis_id'] )) ? $data ['sub_ubis_id'] : null;
								$obj2->sububisname = (isset ( $data ['sub_ubis'] )) ? $data ['sub_ubis'] : null;
								$storage->write ( $obj2 );
								$ret ['msg'] = "";
							}
						} else {
							$ret ['msg'] = "Wrong User Name/Password";
						}
					} else {
						$ret ['success'] = 0;
						$ret ['msg'] = "User tersebut tidak terdaftar.";
					}
				} else {
					$ret ['success'] = 0;
					$ret ['msg'] = "LDAP Error";
				}
				
				if ($ret ['success'] == 1) {
					try {
						$auth = Zend_Auth::getInstance ();
						$usr = $auth->getIdentity ();
					} catch ( Zend_Session_Exception $e ) {
					}
				
					if (isset ( $usr->uid ) && isset ( $usr->uname )) {
						$mdl_sys->update_log ( $usr->uname );
						
					
						$res2 = $mdl_zusr->update_c_act ( $usr->uname );
					
						if (isset ( $_GET ['redirect'] ) && ($_GET ['redirect'] != '/') &&$_GET ['redirect'] != '') {
							$this->_redirect ( $_GET ['redirect'] );
						} else if (isset ( $_POST ['redirect'] ) && $_POST ['redirect'] != ''&&$_POST ['redirect'] != '/') {
							$this->_redirect ( $_POST ['redirect'] );
						} else if (isset ( $params ['redirect'] ) && $params ['redirect'] != ''&& $params ['redirect'] != '/') {
							
							$this->_redirect ( $params ['redirect'] );
						} else {
							$module = Zend_Controller_Front::getInstance ()->getRequest ()->getModuleName ();
							
							if (isset ( $redirect_role ) && $redirect_role != "") {
								$this->_redirect ( $redirect_role );
							} else if ($module != 'default') {
								$this->_redirect ( "/" . $control );
							} else {
								$this->_redirect ( "/" );
							}
						}
					}
				} else {
				}
			} else {
				$ret ['success'] = 0;
				$ret ['msg'] = "Please enter valid captcha.";
			}
		}
		
		if (isset ( $params ['redirect'] ) && $params ['redirect'] != '') {
			$this->view->redirect = $params ['redirect'];
		}
		(isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS'])) ? $ser='https':$ser='http';
		if (isset ( $ret )) {
			
			$this->_redirect ( "$ser://".$_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI'].'?'.http_build_query($ret));
		}
	}
	public function logoutAction() {
		try {
			$authAdapter = Zend_Auth::getInstance ();
			$identity = $authAdapter->getIdentity ();
			
			// /clear
			$cc = new Model_Zusers ();
			$cc->clear_logout ( $identity->uname );
			// / end
			
			$authAdapter->clearIdentity ();
			Zend_Session::destroy ();
		} catch ( Exception $e ) {
		}
		$this->_redirect ( '/PRABAMODULE' );
	}
}

