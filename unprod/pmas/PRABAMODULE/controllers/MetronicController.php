<?php

/**
 * Copyright (C) Prabatech.com, Inc - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited Proprietary and confidential Written by himawijaya <info@prabatech.com>,24.01.2016
 *
 */
class PRABAMODULE_MetronicController extends Zend_Controller_Action {
	public function init() {
		
	}
	
	public function headerAction() {
		$params = $this->getRequest ()->getParams ();
		
		$ns = Zend_Session::namespaceGet ( 'api' );
		$this->view->apid = $params ['apid'];
		$this->view->session = $ns;
	}
	
	public function topbarAction() {
		try {
			$authAdapter = Zend_Auth::getInstance ();
			$identity = $authAdapter->getIdentity ();
		} catch ( Exception $e ) {
		}
		$rids = implode(",", $identity->roles); 
		
		$this->view->me = $identity;
		$mdl_sys = new Model_System ();
		$listmenu = $mdl_sys->get_menus_by_roles ($rids);
		// Zend_Debug::dump($listmenu);die();
		$menus = array ();
		$menu1 = array ();
		$listmenu1 = array ();
		$tmp1 = array ();
		$menu2 = array ();
		$listmenu2 = array ();
		$tmp2 = array ();
		
		/*
		 * foreach($listmenu as $k=>$v){ if(substr($v['menu_name'],0,3)=='new'){ $menus = $v; } }
		 */
		
		foreach ( $listmenu as $k => $v ) {
			if ($v ['parent_id'] == '0') {
				if (substr ( $v ['menu_name'], 0, 4 ) == 'new ') {
					$v ['child'] = array ();
					$v ['menu_name'] = substr ( $v ['menu_name'], 4 );
					$menu1 [$v ['id']] = $v;
					$listmenu1 [] = $v ['id'];
				}
			} else {
				$tmp1 [] = $v;
			}
		}
		// Zend_Debug::dump($menu1);die();
		
		foreach ( $tmp1 as $k => $v ) {
			if (in_array ( $v ['parent_id'], $listmenu1 )) {
				$v ['child'] = array ();
				$menu2 [$v ['id']] = $v;
				$listmenu2 [] = $v ['id'];
			} else {
				$tmp2 [] = $v;
			}
		}
		
		foreach ( $tmp2 as $k => $v ) {
			if (in_array ( $v ['parent_id'], $listmenu2 )) {
				$v ['child'] = array ();
				$menu2 [$v ['parent_id']] ['child'] [] = $v;
			}
		}
		
		foreach ( $menu2 as $k => $v ) {
			$menu1 [$v ['parent_id']] ['child'] [] = $v;
		}
		// Zend_Debug::dump($menu1);die();
		
		$this->view->menu = $menu1;
	}
	public function sidebarAction() {
		try {
			$authAdapter = Zend_Auth::getInstance ();
			$identity = $authAdapter->getIdentity ();
		} catch ( Exception $e ) {
		}
		$rids = implode(",", $identity->roles); 
		
		$this->view->me = $identity;
		$mdl_sys = new Model_System ();
		$listmenu = $mdl_sys->get_menus_by_roles ($rids);
		// Zend_Debug::dump($listmenu);die();
		$menus = array ();
		$menu1 = array ();
		$listmenu1 = array ();
		$tmp1 = array ();
		$menu2 = array ();
		$listmenu2 = array ();
		$tmp2 = array ();
		
		/*
		 * foreach($listmenu as $k=>$v){ if(substr($v['menu_name'],0,3)=='new'){ $menus = $v; } }
		 */
		
		foreach ( $listmenu as $k => $v ) {
			if ($v ['parent_id'] == '0') {
				if (substr ( $v ['menu_name'], 0, 4 ) == 'new ') {
					$v ['child'] = array ();
					$v ['menu_name'] = substr ( $v ['menu_name'], 4 );
					$menu1 [$v ['id']] = $v;
					$listmenu1 [] = $v ['id'];
				}
			} else {
				$tmp1 [] = $v;
			}
		}
		// Zend_Debug::dump($menu1);die();
		
		foreach ( $tmp1 as $k => $v ) {
			if (in_array ( $v ['parent_id'], $listmenu1 )) {
				$v ['child'] = array ();
				$menu2 [$v ['id']] = $v;
				$listmenu2 [] = $v ['id'];
			} else {
				$tmp2 [] = $v;
			}
		}
		
		foreach ( $tmp2 as $k => $v ) {
			if (in_array ( $v ['parent_id'], $listmenu2 )) {
				$v ['child'] = array ();
				$menu2 [$v ['parent_id']] ['child'] [] = $v;
			}
		}
		
		foreach ( $menu2 as $k => $v ) {
			$menu1 [$v ['parent_id']] ['child'] [] = $v;
		}
		// Zend_Debug::dump($menu1);die();
		
		$this->view->menu = $menu1;
	}
	
}
